EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr C 22000 17000
encoding utf-8
Sheet 1 1
Title "noname.sch"
Date "10 feb 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L old_nonsystem_library:CONN_1 P2
U 1 1 5117DE89
P 8700 9950
F 0 "P2" H 8780 9950 40  0000 C CNN
F 1 "CONN_1" H 8650 9990 30  0001 C CNN
F 2 "PGA15X15" H 8650 10090 30  0001 C CNN
F 3 "" H 8700 9950 50  0001 C CNN
	1    8700 9950
	1    0    0    -1  
$EndComp
$Comp
L old_nonsystem_library:CONN_1 P4
U 1 1 5117DE83
P 8700 10150
F 0 "P4" H 8780 10150 40  0000 C CNN
F 1 "CONN_1" H 8650 10190 30  0001 C CNN
F 2 "PGA15X15" H 8650 10290 30  0001 C CNN
F 3 "" H 8700 10150 50  0001 C CNN
	1    8700 10150
	1    0    0    -1  
$EndComp
$Comp
L old_nonsystem_library:CONN_1 P7
U 1 1 5117DE7D
P 8700 10450
F 0 "P7" H 8780 10450 40  0000 C CNN
F 1 "CONN_1" H 8650 10490 30  0001 C CNN
F 2 "PGA15X15" H 8650 10590 30  0001 C CNN
F 3 "" H 8700 10450 50  0001 C CNN
	1    8700 10450
	1    0    0    -1  
$EndComp
$Comp
L old_nonsystem_library:CONN_1 P11
U 1 1 5117DE77
P 9100 10250
F 0 "P11" H 9180 10250 40  0000 C CNN
F 1 "CONN_1" H 9050 10290 30  0001 C CNN
F 2 "PGA15X15" H 9050 10390 30  0001 C CNN
F 3 "" H 9100 10250 50  0001 C CNN
	1    9100 10250
	1    0    0    -1  
$EndComp
$Comp
L old_nonsystem_library:CONN_1 P3
U 1 1 5117DE65
P 8700 10050
F 0 "P3" H 8780 10050 40  0000 C CNN
F 1 "CONN_1" H 8650 10090 30  0001 C CNN
F 2 "PGA15X15" H 8650 10190 30  0001 C CNN
F 3 "" H 8700 10050 50  0001 C CNN
	1    8700 10050
	1    0    0    -1  
$EndComp
$Comp
L old_nonsystem_library:CONN_1 P5
U 1 1 5117DE5F
P 8700 10250
F 0 "P5" H 8780 10250 40  0000 C CNN
F 1 "CONN_1" H 8650 10290 30  0001 C CNN
F 2 "PGA15X15" H 8650 10390 30  0001 C CNN
F 3 "" H 8700 10250 50  0001 C CNN
	1    8700 10250
	1    0    0    -1  
$EndComp
$Comp
L old_nonsystem_library:CONN_1 P8
U 1 1 5117DE59
P 9100 9950
F 0 "P8" H 9180 9950 40  0000 C CNN
F 1 "CONN_1" H 9050 9990 30  0001 C CNN
F 2 "PGA15X15" H 9050 10090 30  0001 C CNN
F 3 "" H 9100 9950 50  0001 C CNN
	1    9100 9950
	1    0    0    -1  
$EndComp
$Comp
L old_nonsystem_library:CONN_1 P12
U 1 1 5117DE53
P 9100 10350
F 0 "P12" H 9180 10350 40  0000 C CNN
F 1 "CONN_1" H 9050 10390 30  0001 C CNN
F 2 "PGA15X15" H 9050 10490 30  0001 C CNN
F 3 "" H 9100 10350 50  0001 C CNN
	1    9100 10350
	1    0    0    -1  
$EndComp
$Comp
L old_nonsystem_library:CONN_1 P10
U 1 1 5117DE41
P 9100 10150
F 0 "P10" H 9180 10150 40  0000 C CNN
F 1 "CONN_1" H 9050 10190 30  0001 C CNN
F 2 "PGA15X15" H 9050 10290 30  0001 C CNN
F 3 "" H 9100 10150 50  0001 C CNN
	1    9100 10150
	1    0    0    -1  
$EndComp
$Comp
L old_nonsystem_library:CONN_1 P6
U 1 1 5117DE3B
P 8700 10350
F 0 "P6" H 8780 10350 40  0000 C CNN
F 1 "CONN_1" H 8650 10390 30  0001 C CNN
F 2 "PGA15X15" H 8650 10490 30  0001 C CNN
F 3 "" H 8700 10350 50  0001 C CNN
	1    8700 10350
	1    0    0    -1  
$EndComp
$Comp
L old_nonsystem_library:CONN_1 P9
U 1 1 5117DE35
P 9100 10050
F 0 "P9" H 9180 10050 40  0000 C CNN
F 1 "CONN_1" H 9050 10090 30  0001 C CNN
F 2 "PGA15X15" H 9050 10190 30  0001 C CNN
F 3 "" H 9100 10050 50  0001 C CNN
	1    9100 10050
	1    0    0    -1  
$EndComp
$Comp
L old_nonsystem_library:CONN_1 P13
U 1 1 5117DE2F
P 9100 10450
F 0 "P13" H 9180 10450 40  0000 C CNN
F 1 "CONN_1" H 9050 10490 30  0001 C CNN
F 2 "PGA15X15" H 9050 10590 30  0001 C CNN
F 3 "" H 9100 10450 50  0001 C CNN
	1    9100 10450
	1    0    0    -1  
$EndComp
$Comp
L old_nonsystem_library:CONN_1 P14
U 1 1 5117DE23
P 9500 9950
F 0 "P14" H 9580 9950 40  0000 C CNN
F 1 "CONN_1" H 9450 9990 30  0001 C CNN
F 2 "PGA15X15" H 9450 10090 30  0001 C CNN
F 3 "" H 9500 9950 50  0001 C CNN
	1    9500 9950
	1    0    0    -1  
$EndComp
NoConn ~ 8550 9950
NoConn ~ 8550 10150
NoConn ~ 8550 10450
NoConn ~ 8950 10250
NoConn ~ 8550 10050
NoConn ~ 8550 10250
NoConn ~ 8950 9950
NoConn ~ 9350 9950
NoConn ~ 8950 10450
NoConn ~ 8950 10050
NoConn ~ 8550 10350
NoConn ~ 8950 10150
NoConn ~ 8950 10350
Text Notes 8100 9800 0    60   ~ 0
Prototyping Board Grid Patterns
Wire Wire Line
	11150 8650 11150 7900
Text Label 11150 8600 1    60   ~ 0
/SSW_DSBL
Text Label 7850 8600 1    60   ~ 0
UNPROT(T5)
NoConn ~ 6050 8650
NoConn ~ 11050 8650
Wire Wire Line
	13150 7900 13150 8650
Wire Wire Line
	15750 8650 15750 7900
Wire Wire Line
	15650 8650 15650 7900
Wire Wire Line
	15550 8650 15550 7900
Wire Wire Line
	15450 8650 15450 7900
Wire Wire Line
	15350 8650 15350 7900
Wire Wire Line
	15250 8650 15250 7900
Wire Wire Line
	15150 8650 15150 7900
Wire Wire Line
	14950 8650 14950 7900
Wire Wire Line
	14850 8650 14850 7900
Wire Wire Line
	14750 8650 14750 7900
Wire Wire Line
	14650 8650 14650 7900
Wire Wire Line
	14550 8650 14550 7900
Wire Wire Line
	14450 8650 14450 7900
Wire Wire Line
	14350 8650 14350 7900
Wire Wire Line
	14250 8650 14250 7900
Wire Wire Line
	14150 8650 14150 7900
Wire Wire Line
	14050 8650 14050 7900
Wire Wire Line
	13950 8650 13950 7900
Wire Wire Line
	13850 8650 13850 7900
Wire Wire Line
	13750 8650 13750 7900
Wire Wire Line
	13650 8650 13650 7900
Wire Wire Line
	13550 8650 13550 7900
Wire Wire Line
	13450 8650 13450 7900
Wire Wire Line
	13350 8650 13350 7900
Wire Wire Line
	13250 8650 13250 7900
Wire Wire Line
	13050 8650 13050 7900
Wire Wire Line
	12950 8650 12950 7900
Wire Wire Line
	12750 8650 12750 7900
Wire Wire Line
	12650 8650 12650 7900
Wire Wire Line
	12550 8650 12550 7900
Wire Wire Line
	12450 8650 12450 7900
Wire Wire Line
	12350 8650 12350 7900
Wire Wire Line
	12250 8650 12250 7900
Wire Wire Line
	12150 8650 12150 7900
Wire Wire Line
	12050 8650 12050 7900
Wire Wire Line
	11950 8650 11950 7900
Wire Wire Line
	11850 8650 11850 7900
Wire Wire Line
	11750 8650 11750 7900
Wire Wire Line
	11650 8650 11650 7900
Wire Wire Line
	11550 8650 11550 7900
Wire Wire Line
	11450 8650 11450 7900
Wire Wire Line
	11350 8650 11350 7900
Wire Wire Line
	11250 8650 11250 7900
Wire Wire Line
	11050 8650 11050 7900
Wire Wire Line
	10950 8650 10950 7900
Wire Wire Line
	10850 8650 10850 7900
Wire Wire Line
	10750 8650 10750 7900
Wire Wire Line
	10650 8650 10650 7900
Wire Wire Line
	10550 8650 10550 7900
Wire Wire Line
	10450 8650 10450 7900
Wire Wire Line
	10350 8650 10350 7900
Wire Wire Line
	10250 8650 10250 7900
Wire Wire Line
	10150 8650 10150 7900
Wire Wire Line
	10050 8650 10050 7900
Wire Wire Line
	9950 8650 9950 7900
Wire Wire Line
	9850 8650 9850 7900
Wire Wire Line
	9750 8650 9750 7900
Wire Wire Line
	9650 8650 9650 7900
Wire Wire Line
	9550 8650 9550 7900
Wire Wire Line
	9450 8650 9450 7900
Wire Wire Line
	9350 8650 9350 7900
Wire Wire Line
	9250 8650 9250 7900
Wire Wire Line
	9150 8650 9150 7900
Wire Wire Line
	9050 8650 9050 7900
Wire Wire Line
	8950 8650 8950 7900
Wire Wire Line
	8850 8650 8850 7900
Wire Wire Line
	8750 8650 8750 7900
Wire Wire Line
	8650 8650 8650 7900
Wire Wire Line
	8550 8650 8550 7900
Wire Wire Line
	8450 8650 8450 7900
Wire Wire Line
	8350 8650 8350 7900
Wire Wire Line
	8250 8650 8250 7900
Wire Wire Line
	8150 8650 8150 7900
Wire Wire Line
	8050 8650 8050 7900
Wire Wire Line
	7950 8650 7950 7900
Wire Wire Line
	7750 8650 7750 7900
Wire Wire Line
	7650 8650 7650 7900
Wire Wire Line
	7550 8650 7550 7900
Wire Wire Line
	7450 8650 7450 7900
Wire Wire Line
	7350 8650 7350 7900
Wire Wire Line
	7250 8650 7250 7900
Wire Wire Line
	7150 8650 7150 7900
Wire Wire Line
	7050 8650 7050 7900
Wire Wire Line
	6950 8650 6950 7900
Wire Wire Line
	6850 8650 6850 7900
Wire Wire Line
	6750 8650 6750 7900
Wire Wire Line
	6650 8650 6650 7900
Wire Wire Line
	6550 8650 6550 7900
Wire Wire Line
	6450 8650 6450 7900
Wire Wire Line
	6350 8650 6350 7900
Wire Wire Line
	6250 8650 6250 7900
Wire Wire Line
	6150 8650 6150 7900
Wire Wire Line
	6050 8650 6050 7900
Text Label 15850 8600 1    60   ~ 0
0_V
Text Label 15750 8600 1    60   ~ 0
/POC
Text Label 15650 8600 1    60   ~ 0
SSTACK
Text Label 15550 8600 1    60   ~ 0
/SWO
Text Label 15450 8600 1    60   ~ 0
SINTA
Text Label 15350 8600 1    60   ~ 0
DI0
Text Label 15250 8600 1    60   ~ 0
DI1
Text Label 15150 8600 1    60   ~ 0
DI6
Text Label 15050 8600 1    60   ~ 0
DI5
Text Label 14950 8600 1    60   ~ 0
DI4
Text Label 14850 8600 1    60   ~ 0
DO7
Text Label 14750 8600 1    60   ~ 0
DO3
Text Label 14650 8600 1    60   ~ 0
DO2
Text Label 14550 8600 1    60   ~ 0
A11
Text Label 14450 8600 1    60   ~ 0
A14
Text Label 14350 8600 1    60   ~ 0
A13
Text Label 14250 8600 1    60   ~ 0
A8
Text Label 14150 8600 1    60   ~ 0
A7
Text Label 14050 8600 1    60   ~ 0
A6
Text Label 13950 8600 1    60   ~ 0
A2
Text Label 13850 8600 1    60   ~ 0
A1
Text Label 13750 8600 1    60   ~ 0
A0
Text Label 13650 8600 1    60   ~ 0
PDBIN
Text Label 13550 8600 1    60   ~ 0
/PWR
Text Label 13450 8600 1    60   ~ 0
PSYNC
Text Label 13350 8600 1    60   ~ 0
/PRESET
Text Label 13250 8600 1    60   ~ 0
/PHOLD
Text Label 13150 8600 1    60   ~ 0
/PINT
Text Label 13050 8600 1    60   ~ 0
PRDY
Text Label 12950 8600 1    60   ~ 0
RUN
Text Label 12850 8600 1    60   ~ 0
PROT(GND)
Text Label 12750 8600 1    60   ~ 0
/PS(---)
Text Label 12650 8600 1    60   ~ 0
MWRT
Text Label 12550 8600 1    60   ~ 0
---(/PHANT)
Text Label 12450 8600 1    60   ~ 0
pin_66
Text Label 12350 8600 1    60   ~ 0
pin_65
Text Label 12250 8600 1    60   ~ 0
pin_64
Text Label 12150 8600 1    60   ~ 0
pin_63
Text Label 12050 8600 1    60   ~ 0
pin_62
Text Label 11950 8600 1    60   ~ 0
pin_61
Text Label 11850 8600 1    60   ~ 0
pin_60
Text Label 11750 8600 1    60   ~ 0
pin_59
Text Label 11650 8600 1    60   ~ 0
FRDY(---)
Text Label 11550 8600 1    60   ~ 0
DIG1(---)
Text Label 11450 8600 1    60   ~ 0
/STSTB(---)
Text Label 11350 8600 1    60   ~ 0
RTC(---)
Text Label 11250 8600 1    60   ~ 0
/EXTCLR
Text Label 11050 8600 1    60   ~ 0
-16_V
Text Label 10950 8600 1    60   ~ 0
+8_V
Text Label 10850 8600 1    60   ~ 0
GND
Text Label 10750 8600 1    60   ~ 0
/CLOCK
Text Label 10650 8600 1    60   ~ 0
SHLTA
Text Label 10550 8600 1    60   ~ 0
SMEMR
Text Label 10450 8600 1    60   ~ 0
SINP
Text Label 10350 8600 1    60   ~ 0
SOUT
Text Label 10250 8600 1    60   ~ 0
SM1
Text Label 10150 8600 1    60   ~ 0
DI7
Text Label 10050 8600 1    60   ~ 0
DI3
Text Label 9950 8600 1    60   ~ 0
DI2
Text Label 9850 8600 1    60   ~ 0
DO6
Text Label 9750 8600 1    60   ~ 0
DO5
Text Label 9650 8600 1    60   ~ 0
DO4
Text Label 9550 8600 1    60   ~ 0
A10
Text Label 9450 8600 1    60   ~ 0
DO0
Text Label 9350 8600 1    60   ~ 0
DO1
Text Label 9250 8600 1    60   ~ 0
A9
Text Label 9150 8600 1    60   ~ 0
A12
Text Label 9050 8600 1    60   ~ 0
A15
Text Label 8950 8600 1    60   ~ 0
A3
Text Label 8850 8600 1    60   ~ 0
A4
Text Label 8750 8600 1    60   ~ 0
A5
Text Label 8650 8600 1    60   ~ 0
PINTE
Text Label 8550 8600 1    60   ~ 0
PWAIT
Text Label 8450 8600 1    60   ~ 0
PHLDA
Text Label 8350 8600 1    60   ~ 0
phi1
Text Label 8250 8600 1    60   ~ 0
phi2
Text Label 8150 8600 1    60   ~ 0
/DODSB
Text Label 8050 8600 1    60   ~ 0
/ADDDSB
Text Label 7950 8600 1    60   ~ 0
SS
Text Label 7750 8600 1    60   ~ 0
/CCDSB
Text Label 7650 8600 1    60   ~ 0
/STADSB
Text Label 7550 8600 1    60   ~ 0
pin_17
Text Label 7450 8600 1    60   ~ 0
pin_16
Text Label 7350 8600 1    60   ~ 0
pin_15
Text Label 7250 8600 1    60   ~ 0
pin_14
Text Label 7150 8600 1    60   ~ 0
pin_13
Text Label 7050 8600 1    60   ~ 0
pin_12
Text Label 6950 8600 1    60   ~ 0
VI_7
Text Label 6850 8600 1    60   ~ 0
VI_6
Text Label 6750 8600 1    60   ~ 0
VI_5
Text Label 6650 8600 1    60   ~ 0
VI_4
Text Label 6550 8600 1    60   ~ 0
VI_3
Text Label 6450 8600 1    60   ~ 0
VI_2
Text Label 6350 8600 1    60   ~ 0
VI_1
Text Label 6250 8600 1    60   ~ 0
VI_0
Text Label 6150 8600 1    60   ~ 0
XRDY
Text Label 6050 8600 1    60   ~ 0
+18_V
Text Label 5950 8600 1    60   ~ 0
+8_V
$Comp
L old_nonsystem_library:S100_MALE P1
U 1 1 4A06E881
P 10900 7250
F 0 "P1" H 10900 12300 60  0000 C CNN
F 1 "S100_MALE" V 11300 7250 60  0000 C CNN
F 2 "Sockets:S100_MALE" V 11400 7250 60  0001 C CNN
F 3 "" H 10900 7250 50  0001 C CNN
	1    10900 7250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7850 8650 7850 7900
Wire Wire Line
	12850 8650 12850 7900
$Comp
L old_nonsystem_library:CONN_1 P15
U 1 1 61F7CD47
P 9500 10050
F 0 "P15" H 9580 10050 40  0000 C CNN
F 1 "CONN_1" H 9450 10090 30  0001 C CNN
F 2 "PGA15X15" H 9450 10190 30  0001 C CNN
F 3 "" H 9500 10050 50  0001 C CNN
	1    9500 10050
	1    0    0    -1  
$EndComp
NoConn ~ 9350 10050
$Comp
L Connector:Conn_01x01_Female J1
U 1 1 6202E70D
P 5950 8850
F 0 "J1" H 6000 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 5842 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 5950 8850 50  0001 C CNN
F 3 "~" H 5950 8850 50  0001 C CNN
	1    5950 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J2
U 1 1 6204279A
P 6150 8850
F 0 "J2" H 6200 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 6042 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 6150 8850 50  0001 C CNN
F 3 "~" H 6150 8850 50  0001 C CNN
	1    6150 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J3
U 1 1 62042DE5
P 6250 8850
F 0 "J3" H 6300 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 6142 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 6250 8850 50  0001 C CNN
F 3 "~" H 6250 8850 50  0001 C CNN
	1    6250 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J4
U 1 1 620433D4
P 6350 8850
F 0 "J4" H 6400 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 6242 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 6350 8850 50  0001 C CNN
F 3 "~" H 6350 8850 50  0001 C CNN
	1    6350 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J5
U 1 1 620439AF
P 6450 8850
F 0 "J5" H 6500 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 6342 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 6450 8850 50  0001 C CNN
F 3 "~" H 6450 8850 50  0001 C CNN
	1    6450 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J6
U 1 1 620450BF
P 6550 8850
F 0 "J6" H 6600 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 6442 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 6550 8850 50  0001 C CNN
F 3 "~" H 6550 8850 50  0001 C CNN
	1    6550 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J7
U 1 1 620454E6
P 6650 8850
F 0 "J7" H 6700 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 6542 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 6650 8850 50  0001 C CNN
F 3 "~" H 6650 8850 50  0001 C CNN
	1    6650 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J8
U 1 1 620458D6
P 6750 8850
F 0 "J8" H 6800 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 6642 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 6750 8850 50  0001 C CNN
F 3 "~" H 6750 8850 50  0001 C CNN
	1    6750 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J9
U 1 1 62046FF3
P 6850 8850
F 0 "J9" H 6900 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 6742 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 6850 8850 50  0001 C CNN
F 3 "~" H 6850 8850 50  0001 C CNN
	1    6850 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J10
U 1 1 62047455
P 6950 8850
F 0 "J10" H 7000 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 6842 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 6950 8850 50  0001 C CNN
F 3 "~" H 6950 8850 50  0001 C CNN
	1    6950 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J11
U 1 1 620478A6
P 7050 8850
F 0 "J11" H 7100 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 6942 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 7050 8850 50  0001 C CNN
F 3 "~" H 7050 8850 50  0001 C CNN
	1    7050 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J12
U 1 1 62048BD7
P 7150 8850
F 0 "J12" H 7200 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 7042 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 7150 8850 50  0001 C CNN
F 3 "~" H 7150 8850 50  0001 C CNN
	1    7150 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J13
U 1 1 6204902E
P 7250 8850
F 0 "J13" H 7300 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 7142 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 7250 8850 50  0001 C CNN
F 3 "~" H 7250 8850 50  0001 C CNN
	1    7250 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J14
U 1 1 620494F6
P 7350 8850
F 0 "J14" H 7400 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 7242 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 7350 8850 50  0001 C CNN
F 3 "~" H 7350 8850 50  0001 C CNN
	1    7350 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J15
U 1 1 62049963
P 7450 8850
F 0 "J15" H 7500 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 7342 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 7450 8850 50  0001 C CNN
F 3 "~" H 7450 8850 50  0001 C CNN
	1    7450 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J16
U 1 1 62049EA8
P 7550 8850
F 0 "J16" H 7600 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 7442 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 7550 8850 50  0001 C CNN
F 3 "~" H 7550 8850 50  0001 C CNN
	1    7550 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J17
U 1 1 6204A42D
P 7650 8850
F 0 "J17" H 7700 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 7542 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 7650 8850 50  0001 C CNN
F 3 "~" H 7650 8850 50  0001 C CNN
	1    7650 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J18
U 1 1 6204A919
P 7750 8850
F 0 "J18" H 7800 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 7642 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 7750 8850 50  0001 C CNN
F 3 "~" H 7750 8850 50  0001 C CNN
	1    7750 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J19
U 1 1 62061FEC
P 7850 8850
F 0 "J19" H 7900 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 7742 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 7850 8850 50  0001 C CNN
F 3 "~" H 7850 8850 50  0001 C CNN
	1    7850 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J20
U 1 1 62062478
P 7950 8850
F 0 "J20" H 8000 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 7842 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 7950 8850 50  0001 C CNN
F 3 "~" H 7950 8850 50  0001 C CNN
	1    7950 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J21
U 1 1 6206293C
P 8050 8850
F 0 "J21" H 8100 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 7942 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 8050 8850 50  0001 C CNN
F 3 "~" H 8050 8850 50  0001 C CNN
	1    8050 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J22
U 1 1 62062E17
P 8150 8850
F 0 "J22" H 8200 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 8042 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 8150 8850 50  0001 C CNN
F 3 "~" H 8150 8850 50  0001 C CNN
	1    8150 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J23
U 1 1 620632A1
P 8250 8850
F 0 "J23" H 8300 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 8142 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 8250 8850 50  0001 C CNN
F 3 "~" H 8250 8850 50  0001 C CNN
	1    8250 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J24
U 1 1 620637C5
P 8350 8850
F 0 "J24" H 8400 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 8242 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 8350 8850 50  0001 C CNN
F 3 "~" H 8350 8850 50  0001 C CNN
	1    8350 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J25
U 1 1 620688A9
P 8450 8850
F 0 "J25" H 8500 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 8342 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 8450 8850 50  0001 C CNN
F 3 "~" H 8450 8850 50  0001 C CNN
	1    8450 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J26
U 1 1 62068C9C
P 8550 8850
F 0 "J26" H 8600 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 8442 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 8550 8850 50  0001 C CNN
F 3 "~" H 8550 8850 50  0001 C CNN
	1    8550 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J27
U 1 1 6206911A
P 8650 8850
F 0 "J27" H 8700 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 8542 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 8650 8850 50  0001 C CNN
F 3 "~" H 8650 8850 50  0001 C CNN
	1    8650 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J28
U 1 1 6206973C
P 8750 8850
F 0 "J28" H 8800 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 8642 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 8750 8850 50  0001 C CNN
F 3 "~" H 8750 8850 50  0001 C CNN
	1    8750 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J29
U 1 1 62069C0B
P 8850 8850
F 0 "J29" H 8900 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 8742 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 8850 8850 50  0001 C CNN
F 3 "~" H 8850 8850 50  0001 C CNN
	1    8850 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J30
U 1 1 6206A269
P 8950 8850
F 0 "J30" H 9000 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 8842 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 8950 8850 50  0001 C CNN
F 3 "~" H 8950 8850 50  0001 C CNN
	1    8950 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J31
U 1 1 6206A6DA
P 9050 8850
F 0 "J31" H 9100 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 8942 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 9050 8850 50  0001 C CNN
F 3 "~" H 9050 8850 50  0001 C CNN
	1    9050 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J32
U 1 1 6206D642
P 9150 8850
F 0 "J32" H 9200 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 9042 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 9150 8850 50  0001 C CNN
F 3 "~" H 9150 8850 50  0001 C CNN
	1    9150 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J33
U 1 1 6206DACE
P 9250 8850
F 0 "J33" H 9300 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 9142 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 9250 8850 50  0001 C CNN
F 3 "~" H 9250 8850 50  0001 C CNN
	1    9250 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J34
U 1 1 6206DF48
P 9350 8850
F 0 "J34" H 9400 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 9242 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 9350 8850 50  0001 C CNN
F 3 "~" H 9350 8850 50  0001 C CNN
	1    9350 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J35
U 1 1 6206E694
P 9450 8850
F 0 "J35" H 9500 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 9342 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 9450 8850 50  0001 C CNN
F 3 "~" H 9450 8850 50  0001 C CNN
	1    9450 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J36
U 1 1 6206EC7F
P 9550 8850
F 0 "J36" H 9600 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 9442 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 9550 8850 50  0001 C CNN
F 3 "~" H 9550 8850 50  0001 C CNN
	1    9550 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J37
U 1 1 6206F193
P 9650 8850
F 0 "J37" H 9700 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 9542 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 9650 8850 50  0001 C CNN
F 3 "~" H 9650 8850 50  0001 C CNN
	1    9650 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J38
U 1 1 6206F6D8
P 9750 8850
F 0 "J38" H 9800 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 9642 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 9750 8850 50  0001 C CNN
F 3 "~" H 9750 8850 50  0001 C CNN
	1    9750 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J39
U 1 1 62070E93
P 9850 8850
F 0 "J39" H 9900 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 9742 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 9850 8850 50  0001 C CNN
F 3 "~" H 9850 8850 50  0001 C CNN
	1    9850 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J40
U 1 1 6207131E
P 9950 8850
F 0 "J40" H 10000 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 9842 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 9950 8850 50  0001 C CNN
F 3 "~" H 9950 8850 50  0001 C CNN
	1    9950 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J41
U 1 1 62071782
P 10050 8850
F 0 "J41" H 10100 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 9942 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 10050 8850 50  0001 C CNN
F 3 "~" H 10050 8850 50  0001 C CNN
	1    10050 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J42
U 1 1 62071C56
P 10150 8850
F 0 "J42" H 10200 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 10042 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 10150 8850 50  0001 C CNN
F 3 "~" H 10150 8850 50  0001 C CNN
	1    10150 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J43
U 1 1 620721FD
P 10250 8850
F 0 "J43" H 10300 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 10142 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 10250 8850 50  0001 C CNN
F 3 "~" H 10250 8850 50  0001 C CNN
	1    10250 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J44
U 1 1 6207352C
P 10350 8850
F 0 "J44" H 10400 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 10242 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 10350 8850 50  0001 C CNN
F 3 "~" H 10350 8850 50  0001 C CNN
	1    10350 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J45
U 1 1 620739FE
P 10450 8850
F 0 "J45" H 10500 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 10342 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 10450 8850 50  0001 C CNN
F 3 "~" H 10450 8850 50  0001 C CNN
	1    10450 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J46
U 1 1 62073ED8
P 10550 8850
F 0 "J46" H 10600 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 10442 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 10550 8850 50  0001 C CNN
F 3 "~" H 10550 8850 50  0001 C CNN
	1    10550 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J47
U 1 1 620780F7
P 10650 8850
F 0 "J47" H 10700 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 10542 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 10650 8850 50  0001 C CNN
F 3 "~" H 10650 8850 50  0001 C CNN
	1    10650 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J48
U 1 1 62078686
P 10750 8850
F 0 "J48" H 10800 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 10642 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 10750 8850 50  0001 C CNN
F 3 "~" H 10750 8850 50  0001 C CNN
	1    10750 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J49
U 1 1 62078BA6
P 10850 8850
F 0 "J49" H 10900 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 10742 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 10850 8850 50  0001 C CNN
F 3 "~" H 10850 8850 50  0001 C CNN
	1    10850 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J50
U 1 1 620790DD
P 10950 8850
F 0 "J50" H 11000 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 10842 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 10950 8850 50  0001 C CNN
F 3 "~" H 10950 8850 50  0001 C CNN
	1    10950 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J51
U 1 1 62079A73
P 11150 8850
F 0 "J51" H 11200 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 11042 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 11150 8850 50  0001 C CNN
F 3 "~" H 11150 8850 50  0001 C CNN
	1    11150 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J52
U 1 1 6207A13C
P 11250 8850
F 0 "J52" H 11300 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 11142 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 11250 8850 50  0001 C CNN
F 3 "~" H 11250 8850 50  0001 C CNN
	1    11250 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J53
U 1 1 6207A5E9
P 11350 8850
F 0 "J53" H 11400 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 11242 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 11350 8850 50  0001 C CNN
F 3 "~" H 11350 8850 50  0001 C CNN
	1    11350 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J54
U 1 1 6207AC4F
P 11450 8850
F 0 "J54" H 11500 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 11342 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 11450 8850 50  0001 C CNN
F 3 "~" H 11450 8850 50  0001 C CNN
	1    11450 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J55
U 1 1 6207B103
P 11550 8850
F 0 "J55" H 11600 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 11442 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 11550 8850 50  0001 C CNN
F 3 "~" H 11550 8850 50  0001 C CNN
	1    11550 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J56
U 1 1 6207B7F9
P 11650 8850
F 0 "J56" H 11700 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 11542 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 11650 8850 50  0001 C CNN
F 3 "~" H 11650 8850 50  0001 C CNN
	1    11650 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J57
U 1 1 6207BD3A
P 11750 8850
F 0 "J57" H 11800 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 11642 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 11750 8850 50  0001 C CNN
F 3 "~" H 11750 8850 50  0001 C CNN
	1    11750 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J58
U 1 1 6207C45F
P 11850 8850
F 0 "J58" H 11900 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 11742 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 11850 8850 50  0001 C CNN
F 3 "~" H 11850 8850 50  0001 C CNN
	1    11850 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J59
U 1 1 6207C9BD
P 11950 8850
F 0 "J59" H 12000 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 11842 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 11950 8850 50  0001 C CNN
F 3 "~" H 11950 8850 50  0001 C CNN
	1    11950 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J60
U 1 1 6207D267
P 12050 8850
F 0 "J60" H 12100 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 11942 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 12050 8850 50  0001 C CNN
F 3 "~" H 12050 8850 50  0001 C CNN
	1    12050 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J61
U 1 1 6207D81A
P 12150 8850
F 0 "J61" H 12200 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 12042 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 12150 8850 50  0001 C CNN
F 3 "~" H 12150 8850 50  0001 C CNN
	1    12150 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J62
U 1 1 6207DDBA
P 12250 8850
F 0 "J62" H 12300 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 12142 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 12250 8850 50  0001 C CNN
F 3 "~" H 12250 8850 50  0001 C CNN
	1    12250 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J63
U 1 1 6207E2FF
P 12350 8850
F 0 "J63" H 12400 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 12242 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 12350 8850 50  0001 C CNN
F 3 "~" H 12350 8850 50  0001 C CNN
	1    12350 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J64
U 1 1 6207E818
P 12450 8850
F 0 "J64" H 12500 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 12342 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 12450 8850 50  0001 C CNN
F 3 "~" H 12450 8850 50  0001 C CNN
	1    12450 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J65
U 1 1 6207F6F8
P 12550 8850
F 0 "J65" H 12600 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 12442 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 12550 8850 50  0001 C CNN
F 3 "~" H 12550 8850 50  0001 C CNN
	1    12550 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J66
U 1 1 6207FC56
P 12650 8850
F 0 "J66" H 12700 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 12542 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 12650 8850 50  0001 C CNN
F 3 "~" H 12650 8850 50  0001 C CNN
	1    12650 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J67
U 1 1 620801A7
P 12750 8850
F 0 "J67" H 12800 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 12642 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 12750 8850 50  0001 C CNN
F 3 "~" H 12750 8850 50  0001 C CNN
	1    12750 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J68
U 1 1 62080715
P 12850 8850
F 0 "J68" H 12900 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 12742 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 12850 8850 50  0001 C CNN
F 3 "~" H 12850 8850 50  0001 C CNN
	1    12850 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J69
U 1 1 62080D68
P 12950 8850
F 0 "J69" H 13000 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 12842 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 12950 8850 50  0001 C CNN
F 3 "~" H 12950 8850 50  0001 C CNN
	1    12950 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J70
U 1 1 620812F6
P 13050 8850
F 0 "J70" H 13100 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 12942 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 13050 8850 50  0001 C CNN
F 3 "~" H 13050 8850 50  0001 C CNN
	1    13050 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J71
U 1 1 62081AF6
P 13150 8850
F 0 "J71" H 13200 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 13042 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 13150 8850 50  0001 C CNN
F 3 "~" H 13150 8850 50  0001 C CNN
	1    13150 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J72
U 1 1 6208205A
P 13250 8850
F 0 "J72" H 13300 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 13142 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 13250 8850 50  0001 C CNN
F 3 "~" H 13250 8850 50  0001 C CNN
	1    13250 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J73
U 1 1 62083279
P 13350 8850
F 0 "J73" H 13400 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 13242 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 13350 8850 50  0001 C CNN
F 3 "~" H 13350 8850 50  0001 C CNN
	1    13350 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J74
U 1 1 620837F0
P 13450 8850
F 0 "J74" H 13500 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 13342 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 13450 8850 50  0001 C CNN
F 3 "~" H 13450 8850 50  0001 C CNN
	1    13450 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J75
U 1 1 62083DB4
P 13550 8850
F 0 "J75" H 13600 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 13442 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 13550 8850 50  0001 C CNN
F 3 "~" H 13550 8850 50  0001 C CNN
	1    13550 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J76
U 1 1 62086FA0
P 13650 8850
F 0 "J76" H 13700 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 13542 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 13650 8850 50  0001 C CNN
F 3 "~" H 13650 8850 50  0001 C CNN
	1    13650 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J77
U 1 1 620873BF
P 13750 8850
F 0 "J77" H 13800 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 13642 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 13750 8850 50  0001 C CNN
F 3 "~" H 13750 8850 50  0001 C CNN
	1    13750 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J78
U 1 1 620877C8
P 13850 8850
F 0 "J78" H 13900 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 13742 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 13850 8850 50  0001 C CNN
F 3 "~" H 13850 8850 50  0001 C CNN
	1    13850 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J79
U 1 1 620880DD
P 13950 8850
F 0 "J79" H 14000 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 13842 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 13950 8850 50  0001 C CNN
F 3 "~" H 13950 8850 50  0001 C CNN
	1    13950 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J80
U 1 1 6208866C
P 14050 8850
F 0 "J80" H 14100 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 13942 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 14050 8850 50  0001 C CNN
F 3 "~" H 14050 8850 50  0001 C CNN
	1    14050 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J81
U 1 1 62088C81
P 14150 8850
F 0 "J81" H 14200 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 14042 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 14150 8850 50  0001 C CNN
F 3 "~" H 14150 8850 50  0001 C CNN
	1    14150 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J82
U 1 1 62089254
P 14250 8850
F 0 "J82" H 14300 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 14142 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 14250 8850 50  0001 C CNN
F 3 "~" H 14250 8850 50  0001 C CNN
	1    14250 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J83
U 1 1 6208AD1C
P 14350 8850
F 0 "J83" H 14400 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 14242 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 14350 8850 50  0001 C CNN
F 3 "~" H 14350 8850 50  0001 C CNN
	1    14350 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J84
U 1 1 6208B2E4
P 14450 8850
F 0 "J84" H 14500 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 14342 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 14450 8850 50  0001 C CNN
F 3 "~" H 14450 8850 50  0001 C CNN
	1    14450 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J85
U 1 1 6208B88A
P 14550 8850
F 0 "J85" H 14600 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 14442 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 14550 8850 50  0001 C CNN
F 3 "~" H 14550 8850 50  0001 C CNN
	1    14550 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J86
U 1 1 6208BDF5
P 14650 8850
F 0 "J86" H 14700 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 14542 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 14650 8850 50  0001 C CNN
F 3 "~" H 14650 8850 50  0001 C CNN
	1    14650 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J87
U 1 1 6208D75C
P 14750 8850
F 0 "J87" H 14800 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 14642 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 14750 8850 50  0001 C CNN
F 3 "~" H 14750 8850 50  0001 C CNN
	1    14750 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J88
U 1 1 6208DD25
P 14850 8850
F 0 "J88" H 14900 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 14742 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 14850 8850 50  0001 C CNN
F 3 "~" H 14850 8850 50  0001 C CNN
	1    14850 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J89
U 1 1 6208E819
P 14950 8850
F 0 "J89" H 15000 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 14842 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 14950 8850 50  0001 C CNN
F 3 "~" H 14950 8850 50  0001 C CNN
	1    14950 8850
	0    1    1    0   
$EndComp
Wire Wire Line
	15050 8650 15050 7900
$Comp
L Connector:Conn_01x01_Female J90
U 1 1 6208F7A7
P 15050 8850
F 0 "J90" H 15100 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 14942 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 15050 8850 50  0001 C CNN
F 3 "~" H 15050 8850 50  0001 C CNN
	1    15050 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J91
U 1 1 62090177
P 15150 8850
F 0 "J91" H 15200 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 15042 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 15150 8850 50  0001 C CNN
F 3 "~" H 15150 8850 50  0001 C CNN
	1    15150 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J92
U 1 1 6209074E
P 15250 8850
F 0 "J92" H 15300 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 15142 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 15250 8850 50  0001 C CNN
F 3 "~" H 15250 8850 50  0001 C CNN
	1    15250 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J93
U 1 1 62090D18
P 15350 8850
F 0 "J93" H 15400 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 15242 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 15350 8850 50  0001 C CNN
F 3 "~" H 15350 8850 50  0001 C CNN
	1    15350 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J94
U 1 1 62091AF7
P 15450 8850
F 0 "J94" H 15500 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 15342 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 15450 8850 50  0001 C CNN
F 3 "~" H 15450 8850 50  0001 C CNN
	1    15450 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J95
U 1 1 62092089
P 15550 8850
F 0 "J95" H 15600 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 15442 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 15550 8850 50  0001 C CNN
F 3 "~" H 15550 8850 50  0001 C CNN
	1    15550 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J96
U 1 1 620925CB
P 15650 8850
F 0 "J96" H 15700 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 15542 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 15650 8850 50  0001 C CNN
F 3 "~" H 15650 8850 50  0001 C CNN
	1    15650 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J97
U 1 1 62092BFF
P 15750 8850
F 0 "J97" H 15800 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 15642 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 15750 8850 50  0001 C CNN
F 3 "~" H 15750 8850 50  0001 C CNN
	1    15750 8850
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J98
U 1 1 6209327B
P 15850 8850
F 0 "J98" H 15900 8850 50  0000 C CNN
F 1 "Conn_01x01_Female" H 15742 8716 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 15850 8850 50  0001 C CNN
F 3 "~" H 15850 8850 50  0001 C CNN
	1    15850 8850
	0    1    1    0   
$EndComp
$Comp
L old_nonsystem_library:CONN_1 P16
U 1 1 61FA2D75
P 9500 10150
F 0 "P16" H 9580 10150 40  0000 C CNN
F 1 "CONN_1" H 9450 10190 30  0001 C CNN
F 2 "PGA15X15" H 9450 10290 30  0001 C CNN
F 3 "" H 9500 10150 50  0001 C CNN
	1    9500 10150
	1    0    0    -1  
$EndComp
NoConn ~ 9350 10150
$Comp
L old_nonsystem_library:CONN_1 P17
U 1 1 61FADE1C
P 9500 10250
F 0 "P17" H 9580 10250 40  0000 C CNN
F 1 "CONN_1" H 9450 10290 30  0001 C CNN
F 2 "PGA15X15" H 9450 10390 30  0001 C CNN
F 3 "" H 9500 10250 50  0001 C CNN
	1    9500 10250
	1    0    0    -1  
$EndComp
NoConn ~ 9350 10250
$Comp
L Regulator_Linear:LM7805_TO220 U1
U 1 1 61FB9D16
P 11250 9950
F 0 "U1" H 11250 10192 50  0000 C CNN
F 1 "LM7805_TO220" H 11250 10101 50  0000 C CNN
F 2 "TO_SOT_Packages_THT:TO-3" H 11250 10175 50  0001 C CIN
F 3 "http://www.fairchildsemi.com/ds/LM/LM7805.pdf" H 11250 9900 50  0001 C CNN
	1    11250 9950
	1    0    0    -1  
$EndComp
Wire Wire Line
	11250 10450 11250 10250
$Comp
L old_nonsystem_library:CONN_1 P18
U 1 1 620C9DA6
P 9500 10350
F 0 "P18" H 9580 10350 40  0000 C CNN
F 1 "CONN_1" H 9450 10390 30  0001 C CNN
F 2 "PGA15X15" H 9450 10490 30  0001 C CNN
F 3 "" H 9500 10350 50  0001 C CNN
	1    9500 10350
	1    0    0    -1  
$EndComp
NoConn ~ 9350 10350
Wire Wire Line
	11550 9950 11750 9950
$Comp
L pspice:CAP C1
U 1 1 61F3D48E
P 10550 10200
F 0 "C1" H 10728 10246 50  0000 L CNN
F 1 "CAP" H 10728 10155 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D4.0mm_P2.00mm" H 10550 10200 50  0001 C CNN
F 3 "~" H 10550 10200 50  0001 C CNN
	1    10550 10200
	1    0    0    -1  
$EndComp
Wire Wire Line
	11250 10450 11750 10450
$Comp
L pspice:CAP C2
U 1 1 61F61CAD
P 11750 10200
F 0 "C2" H 11928 10246 50  0000 L CNN
F 1 "CAP" H 11928 10155 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D4.0mm_P2.00mm" H 11750 10200 50  0001 C CNN
F 3 "~" H 11750 10200 50  0001 C CNN
	1    11750 10200
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 61FC6DF6
P 10250 9950
F 0 "#FLG0101" H 10250 10025 50  0001 C CNN
F 1 "PWR_FLAG" H 10250 10123 50  0000 C CNN
F 2 "" H 10250 9950 50  0001 C CNN
F 3 "~" H 10250 9950 50  0001 C CNN
	1    10250 9950
	1    0    0    -1  
$EndComp
Connection ~ 11750 9950
Wire Wire Line
	5950 8650 5950 7900
Wire Wire Line
	15850 8650 15850 7900
Text Label 10300 9950 0    50   ~ 0
+8_V
Text Label 12000 9950 0    50   ~ 0
VCC
Wire Wire Line
	11750 10450 12300 10450
Connection ~ 11750 10450
Text Label 12100 10450 2    50   ~ 0
0_V
$Comp
L power:GND #PWR02
U 1 1 61FA448E
P 12300 10450
F 0 "#PWR02" H 12300 10200 50  0001 C CNN
F 1 "GND" H 12305 10277 50  0000 C CNN
F 2 "" H 12300 10450 50  0001 C CNN
F 3 "" H 12300 10450 50  0001 C CNN
	1    12300 10450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 61F3142A
P 9100 10750
F 0 "H1" H 9200 10799 50  0000 L CNN
F 1 "MountingHole_Pad" H 9200 10708 50  0000 L CNN
F 2 "Sockets:1pin" H 9100 10750 50  0001 C CNN
F 3 "~" H 9100 10750 50  0001 C CNN
	1    9100 10750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 61F320FE
P 9100 10850
F 0 "#PWR01" H 9100 10600 50  0001 C CNN
F 1 "GND" H 9105 10677 50  0000 C CNN
F 2 "" H 9100 10850 50  0001 C CNN
F 3 "" H 9100 10850 50  0001 C CNN
	1    9100 10850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 61F579FB
P 10200 10800
F 0 "H2" H 10300 10849 50  0000 L CNN
F 1 "MountingHole_Pad" H 10300 10758 50  0000 L CNN
F 2 "Sockets:1pin" H 10200 10800 50  0001 C CNN
F 3 "~" H 10200 10800 50  0001 C CNN
	1    10200 10800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 61F57A05
P 10200 10900
F 0 "#PWR03" H 10200 10650 50  0001 C CNN
F 1 "GND" H 10205 10727 50  0000 C CNN
F 2 "" H 10200 10900 50  0001 C CNN
F 3 "" H 10200 10900 50  0001 C CNN
	1    10200 10900
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 62198753
P 12300 9950
F 0 "#FLG0102" H 12300 10025 50  0001 C CNN
F 1 "PWR_FLAG" H 12300 10123 50  0000 C CNN
F 2 "" H 12300 9950 50  0001 C CNN
F 3 "~" H 12300 9950 50  0001 C CNN
	1    12300 9950
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 62199722
P 12300 10450
F 0 "#FLG0103" H 12300 10525 50  0001 C CNN
F 1 "PWR_FLAG" H 12300 10623 50  0000 C CNN
F 2 "" H 12300 10450 50  0001 C CNN
F 3 "~" H 12300 10450 50  0001 C CNN
	1    12300 10450
	1    0    0    -1  
$EndComp
Connection ~ 12300 10450
Wire Wire Line
	11750 9950 12300 9950
Wire Wire Line
	10250 9950 10550 9950
Connection ~ 10550 9950
Wire Wire Line
	10550 9950 10950 9950
Wire Wire Line
	10550 10450 11250 10450
Connection ~ 11250 10450
$Comp
L Connector:Conn_01x01_Female J99
U 1 1 62205B41
P 12500 9950
F 0 "J99" H 12528 9976 50  0000 L CNN
F 1 "VCC" H 12528 9885 50  0000 L CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 12500 9950 50  0001 C CNN
F 3 "~" H 12500 9950 50  0001 C CNN
	1    12500 9950
	1    0    0    -1  
$EndComp
Connection ~ 12300 9950
$EndSCHEMATC
