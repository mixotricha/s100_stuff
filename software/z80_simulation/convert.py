import binascii
import sys
fname = str(sys.argv[1])
file = open(fname, "rb")
byte = file.read(1)
str = "v2.0 raw\n"
while byte:
    str += binascii.hexlify(byte)+" "
    byte = file.read(1)
file.close()
print(str)
