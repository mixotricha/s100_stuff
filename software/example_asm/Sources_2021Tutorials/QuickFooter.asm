

	
	ifdef BuildSxx
TestRam equ &D800
	endif 
	ifdef BuildGBx
TestRam equ &D800
	endif 
	
	
	ifndef TestRam
	align16
TestRam:
		ds 32			;Some space for testing
	endif
	
	read "\SrcALL\Multiplatform_Monitor_RomVer.asm"
	read "\SrcALL\Multiplatform_MonitorMemdump.asm"
	read "\SrcALL\Multiplatform_ShowHex.asm"
	
	read "\SrcALL\V1_Functions.asm"
	
	ifdef BuildGbx
		read "\SrcALL\V1_VdpMemory.asm"
	endif
	ifdef BuildSxx
		read "\SrcALL\V1_VdpMemory.asm"
	endif
	
	ifndef BitmapFont
BitmapFont:
	ifdef BMP_UppercaseOnlyFont
		incbin "\ResALL\Font64.FNT"			;Font bitmap, this is common to all systems
	else
		incbin "\ResALL\Font96.FNT"			;Font bitmap, this is common to all systems
	endif
BitmapFontEnd:
	endif
	
	read "\SrcALL\V1_Footer.asm"
