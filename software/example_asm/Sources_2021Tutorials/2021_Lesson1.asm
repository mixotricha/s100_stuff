;BuildCPC equ 1	; Build for Amstrad CPC on WinApe

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	ifdef vasm
		include "\SrcALL\VasmBuildCompat.asm"	;Vasm <> Winape Compat
	else
		read "\SrcALL\WinApeBuildCompat.asm"	;Vasm <> Winape Compat
	endif
	
	read "QuickHeader.asm"
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			Start Your Program Here
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	LD HL,0
	LD DE,0
	LD BC,0
	LD A,0

	;jp SubsAndLabels		;Subs Labels Jumps
	;jp RegisterLoads		;Load immediates
	;jp MemoryWrites		;Work with addresses
	;jp IncDecNeg			;Inc Dec
	jp AddSub				;Add Sub

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This is a comment - it starts with a semicolon

SubsAndLabels:

	call ShowHello			;This is a subroutine - it runs and comes back

	jp Finish				;Jump to label Finish
	

ShowHello:					;This is a label

	ld hl,Message			;Source is on RIGHT (Message)
							;Destination is on LEFT (Registers HL)
	
	call PrintString		;Print Message
	
	ret						;Return to calling line

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

RegisterLoads:

	ld a,&69 				;Load A with Hex value 69
	
	call ShowAHLBC			;Show Registers
	
	call NewLine

;8 Bit Loads
	
	ld a,-1					;Load A with decimal value -1
	ld b,69 				;Load B with decimal value 69
	ld c,%11110000			;Load C with Binary %11110000 (&F0)
	ld d,255				;Load D with decimal value 255
	call ShowAHLBC			;Show Registers
	
	call NewLine

;16 bit pair loads
	
	ld BC,&1234				;Load BC with 16 bit Hex value &1234
	ld DE,&5678				;Load DE with 16 bit Hex value &5678
	ld HL,16383				;Load DE with 16 bit decimal value 16383 (&3FFF)
	
	call ShowAHLBC			;Show Registers

	call NewLine
	
;Transfer Registers	
	ld a,b					;Load accumulator A with reg B
	
	ld c,d					;Load C with reg D
	
	ld h,d					;Transfer HL to DE 
	ld l,e					;(Have to do this in two parts)

	call ShowAHLBC			;Show Registers

	
	jp Finish

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MemoryWrites:
	call NewLine
	
	ld hl,&C000				;Address to dump
	ld c,16					;Bytes to dump
	call Monitor_MemDumpDirect
	call NewLine
	call NewLine
	
	
	ld hl,&C000				;Load HL with immediate &C000
	
	ld de,(&C000)			;Load DE from address &C000
	
	call ShowAHLBC			;Show Registers
	call NewLine
	
	ld a,&69				;Load A with immediate 69
	ld (&C000),a			;Load A to address &C000
	
	ld bc,(&C002)			;Load BC from address &C002
	
	ld de,&0666				;Load DE with immedaite &0666
	ld (&C004),de			;Store DE to &C004

	call ShowAHLBC			;Show Registers
	call NewLine
	
	ld hl,IAmALabel			;Load HL with address of label IAmALabel
	
	ld a,(hl)				;load a from address in register HL
	
	ld e,(hl)				;load E from address in register HL
	inc hl					;Add 1 to HL
	ld d,(hl)				;load D from address in register HL
	inc hl					;Add 1 to HL
	
	call ShowAHLBC			;Show Registers
	call NewLine
	
	ld a,&69				;Load A with immediate &69
	ld bc,&C006				;Load BC with immediate &C006
	
	ld (bc),a				;Load address in BC with value in A

	call ShowAHLBC			;Show Registers
	call NewLine
	
	ld hl,&C008				;Destination address
	
	ld de,&9876				;Test Value
	
	ld (hl),e				;Load address HL with value in register E
	inc hl					;Add 1 to HL
	ld (hl),d				;Load address HL with value in register D
	inc hl					;Add 1 to HL
	
	;ld (hl),de				;This is impossible - only 1 byte a time
	
	;ld (de),h				;This is impossible - (de) (bc) can only work with a
	
	call ShowAHLBC			;Show Registers
	call NewLine	
	
	
	ld hl,&C000				;Address to dump
	ld c,16					;Bytes to dump
	call Monitor_MemDumpDirect
	call NewLine	
	
	ld hl,IAmALabel				;Address to dump
	ld c,16					;Bytes to dump
	call Monitor_MemDumpDirect
	
	jp Finish
	

IAmALabel:
	db &FE,&DC				;DB defines a byte of data

	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

IncDecNeg:	
	ld hl,&00FE

	call ShowAHLBC			;Show Registers
	inc hl
	call ShowAHLBC			;Show Registers
	inc hl
	call ShowAHLBC			;Show Registers
	
	call NewLine	
	
	call ShowAHLBC			;Show Registers
	dec hl
	call ShowAHLBC			;Show Registers
	dec hl
	call ShowAHLBC			;Show Registers
	
	call NewLine	
	
	
	call ShowAHLBC			;Show Registers
	inc l
	call ShowAHLBC			;Show Registers
	inc l
	call ShowAHLBC			;Show Registers
	
	
	call NewLine	
	
	call ShowAHLBC			;Show Registers
	dec l
	call ShowAHLBC			;Show Registers
	dec l
	call ShowAHLBC			;Show Registers
	
	call NewLine	
	
	ld a,1
	
	neg						;Negate A
	call ShowAHLBC			;Show Registers
	
	neg						;Negate A
	call ShowAHLBC			;Show Registers
	
	neg						;Negate A
	call ShowAHLBC			;Show Registers
	
	
	call NewLine	
	
	
	jp Finish
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
AddSub:
	ld a,&F0
	ld b,2
	ld c,&10
	
	call ShowAHLBC			;Show Registers
	call NewLine	
	
	add a,b					;A=A+B
	add b					;A=A+B (Same meaning as ADD A,B)
	
	call ShowAHLBC			;Show Registers
	call NewLine	
	
	add 1 					;A=A+1 (immediate value)
	
	call ShowAHLBC			;Show Registers
	call NewLine	
	
	;These are impossible
	;add b,a				;Destination must be Accumulator
	;add b,c
	
	sub c
	sub #1
	
	call ShowAHLBC			;Show Registers
	call NewLine
	call NewLine
	
	ld hl,&0666
	ld bc,&1001
	
	call ShowAHLBC			;Show Registers
	call NewLine
	
	add hl,bc
	
	call ShowAHLBC			;Show Registers
	call NewLine
	
	;sub hl,bc				;This doesn't exist
	
	or a					;Carry=0
	sbc hl,bc				;Subtract with Carry HL=HL-(BC + Carry)
	
	call ShowAHLBC			;Show Registers
	call NewLine
	
	ld bc,-&100
	add hl,bc
	
	call ShowAHLBC			;Show Registers
	
	jp Finish
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	; ld hl,Message	
	; call PrintString		;Print Message

	; call newline
	
	; ld hl,&1234
	; ld BC,&ABCD
	; ld a,&10
	; ld de,&9876
	; call ShowAHLBC			;Show Registers
	; call ShowAHLBC			;Show Registers

	; call newline
	
	; ld hl,TestRam			;Address to dump
	; ld c,16					;Bytes to dump 
	; call Monitor_MemDumpDirect
	
	; ld hl,TestRam	
	; ld a,255
	; ld (hl),a
	
	; ld hl,TestRam			;Address to dump
	; ld c,16					;Bytes to dump 
	; call Monitor_MemDumpDirect
	
	
	
	
	
	
	;jp Shutdown
Finish:
	di
	halt					;Lock the CPU


Message: db 'Hello World 323!',255		;255 terminated string

ShowAHLBC:
	push af
	push hl
	push bc
		push hl
			ld l,a
			ld a,'A'
			call ShowHexOne
		
			ld h,b
			ld l,c
			ld bc,'CB'
		
			call ShowHexPair
		
			ifndef BuildGBx			;Can't Show this on the GameBoy
				ld bc,'ED'
				ld h,d
				ld l,e
				call ShowHexPair
			endif
		pop hl
		ld bc,'LH'
		call ShowHexPair
			
		call NewLine
	pop bc
	pop hl
	pop af
	ret

ShowHexOne:		;A:L
	call Printchar
	ld a,':'
	call Printchar
	jr ShowHexOneB
	
ShowHexPair:	;BC:HL
	ld a,b
	call Printchar
	ld a,c
	call Printchar
	ld a,':'
	call Printchar
	ld a,h
	call ShowHex
ShowHexOneB:
	ld a,L
	call ShowHex
	ld a,' '
	call Printchar
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			End of Your program
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	read "QuickFooter.asm"