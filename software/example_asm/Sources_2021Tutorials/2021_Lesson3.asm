;BuildCPC equ 1	; Build for Amstrad CPC on WinApe

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	ifdef vasm
		include "\SrcALL\VasmBuildCompat.asm"	;Vasm <> Winape Compat
	else
		read "\SrcALL\WinApeBuildCompat.asm"	;Vasm <> Winape Compat
	endif
	
	read "QuickHeader.asm"
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			Start Your Program Here
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	LD HL,0
	LD DE,0
	LD BC,0
	LD A,0
	
	;jp Zero
	;jp Carry
	;jp OverFlow
	;jp Parity
	;jp Compare
	jp BitTest
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Zero:
	ld a,&03
	
RepeatUntilZero:	
	call ShowAF
	
	dec a					;Decrease A (sets Z flag)
	
	jr nz,RepeatUntilZero	;Jump if Not Zero

	call NewLines
	call ShowAF
	call NewLines
	
	
;DJNZ is good for loop counting!
	ld b,&04
RepeatUntilZeroB:	
	call ShowBF				;DJNZ does not use flags!
	djnz RepeatUntilZeroB	;Decrease B and Jump if B>0

	call NewLines
	call ShowBF
	call NewLines


;Not all commands change the flags,
;Check the Cheetsheet to se which commands do!
	
	
	ld a,1
	dec a					;Zero flag set
	call ShowAF
		
	ld a,32					;This does not alter the Zero flag
	
	call ShowAF
	
	;sub 32					;This alters the Zero flag! (Z=True)
	;or
	sub 31					;This alters the Zero flag! (Z=False)

	call ShowAF
	
	
	call Z,ZeroString		;Call only if Z flag is set
	call NZ,NonZeroString	;Call only if Z flag is clear
	
	jr z,ShowZero			;Jump only if Z flag is set
	jr nz,ShowNonZero		;Jump only if Z flag is clear
	

	jp Finish
	
ZeroString:
	push af					;Back up A + Flags
		ld hl,strZero
		call PrintString
		call newline
	pop af					;Restore A + Flags
	ret
NonZeroString:
	push af					;Back up A + Flags
		ld hl,strNonZero
		call PrintString
		call newline
	pop af					;Restore A + Flags
	ret

	
strNonZero:
	db "NonZero",255	
strZero:
	db "Zero",255
	
ShowNonZero:
	ld a,'N'
	call PrintChar
ShowZero:	
	ld a,'Z'
	call PrintChar
	jp Finish
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Carry:
	ld a,253
	ld b,4
CarryTestA:
	add 1
	
	call c,ShowCarry
	call nc,ShowNoCarry
	
	call ShowAF
	djnz CarryTestA
	
	call NewLine
	
	
	ld hl,&0001
	ld (TestRam+2),hl
	ld hl,&FFFE
	ld (TestRam),hl

;We can use Carry to extend values - lets use HL to work at 32 bit
	ld b,3
CarryTest32:	
	call ShowTestRam
	
	
	ld hl,(TestRam)
	ld de,&0001
	add hl,de			;HL=HL+DE
	ld (TestRam),hl
	
	ld hl,(TestRam+2)
	ld de,&0000
	adc hl,de			;HL=HL+DE+Carry
	ld (TestRam+2),hl
	
	djnz CarryTest32
	
	call ShowTestRam
	
	call NewLine

;SBC is Subtract with Carry - Carry acts as a borrow in this case	
	ld b,4
CarryTest32b:	
	call ShowTestRam
	
	ld hl,(TestRam)
	ld de,&0001
	or a				;OR A clears carry (no SUB for 16 bit)
	sbc hl,de			;HL=HL-DE
	ld (TestRam),hl
	
	ld hl,(TestRam+2)
	ld de,&0000
	sbc hl,de			;HL=HL-(DE+Carry)
	ld (TestRam+2),hl
	
	djnz CarryTest32b	
	
	jp Finish
	
ShowCarry:
	push af
		ld hl,strCarry
		call PrintString
	pop af
	ret

ShowNoCarry:
	push af
		ld hl,strNoCarry
		call PrintString
	pop af
	ret
	
	
strCarry:
	db "Carry ",255
strNoCarry:
	db " NoC  ",255

	
ShowTestRam:
	push bc
	push de
		ld hl,TestRam			;Address to dump
		ld c,8					;Bytes to dump
		call Monitor_MemDumpDirect
	pop de
	pop bc
	ret	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
OverFlow:

	ld a,&78
	ld b,3

	call AddAndTestOverflow
	call ShowAF
	call AddAndTestOverflow
	call ShowAF
	call AddAndTestOverflow
	call ShowAF
	call AddAndTestOverflow
	call ShowAF
	call AddAndTestOverflow
	call ShowAF
	
	
	call NewLines


	call SubAndTestOverflow
	call ShowAF
	call SubAndTestOverflow
	call ShowAF
	call SubAndTestOverflow
	call ShowAF
	call SubAndTestOverflow
	call ShowAF
	call SubAndTestOverflow
	call ShowAF
	
	jp Finish

AddAndTestOverflow:
	add b
	
	call M,ShowMinus		;Minus S=1
	call P,ShowPlus			;Plus S=0
	
	ret po		;PO=No overflow / PE=Overflow

	push af
		ld hl,strOverflow
		call PrintString
	pop af
	ret
	
SubAndTestOverflow:
	sub b
	
	call M,ShowMinus		;Minus S=1
	call P,ShowPlus			;Plus S=0
	
	ret po		;PO=No overflow / PE=Overflow

	push af
		ld hl,strOverflow
		call PrintString
	pop af
	ret
	
ShowMinus:
	push af
		ld a,'-'
		call PrintChar
		ld a,' '
		call PrintChar
	pop af
	ret
	
ShowPlus:
	push af
		ld a,'+'
		call PrintChar
		ld a,' '
		call PrintChar
	pop af
	ret
		
strOverflow:
	db "OverFlow ",255	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; oVerflow flag is also a Parity flag (P/V)

;Some commands (Add/Sub) use it as oVerflow
;Some commands (bit logic) use it as Parity

Parity:
	ld b,8

	ld a,%11001101
	call po,ShowOdd			;V/P = 0
	call pe,ShowEven		;V/P = 1
	call ShowAbitsF
	
	call NewLines
	
ParityTestAgain:

	srl a					;Bit shift
	
	call po,ShowOdd			;V/P = 0
	call pe,ShowEven		;V/P = 1
	
	call ShowAbitsF
	
	djnz ParityTestAgain
	
	jp Finish
	
ShowOdd:
	push af
		ld hl,strOdd
		call PrintString
	pop af
	ret
	
ShowEven:
	push af
		ld hl,strEven
		call PrintString
	pop af
	ret
	

strOdd:	
	db "Odd  ",255	
strEven:	
	db "Even ",255	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
		
Compare:
	ld a,32
	
	cp 32		;Set flags like SUB 32
	call ShowAF
	
	call Z,ZeroString		;Call only if Z flag is set
	call NZ,NonZeroString	;Call only if Z flag is clear
	
	call NewLines
	
	ld b,31
	;ld b,32
	
	cp b		;Set flags like SUB B
	call ShowAF
	
	call Z,ZeroString		;Call only if Z flag is set
	call NZ,NonZeroString	;Call only if Z flag is clear
	
	call NewLines
	
	;ld D,32
	ld D,48
	;ld D,16
	
	cp D		;Set flags like SUB D
	call ShowAF
	
	call Z,ZeroString		; A =  D
	call NZ,NonZeroString	; A != D
	call C,ShowCarry		; A <  D
	call NC,ShowNoCarry		; A >= D
	
	jp Finish
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BitTest:
	ld a,%01110010

	bit 0,a
	call ShowAF
	
	bit 1,a
	call ShowAF
	
	bit 7,a
	call ShowAF
	
	bit 6,a
	call ShowAF
	
	jp Finish
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	
Finish:
	di
	halt					;Lock the CPU

NewLines:
	push af
		call NewLine
	pop af
	ret
	
	


ShowBF:
	push af
	push hl
	push bc
		push af
		
		
			ld l,b
			ld a,'B'
			call ShowHexOne
	jp ShowBFB
	
	
	
ShowAbitsF:
	push af
	push hl
	push bc
		push af
		
		
			ld l,a
			ld a,'A'
			call ShowHexOne
			
			
			ld b,8			
ShowAbit:			
			ld a,'0'
			sla l
			jr nc,ShowAbit0
			ld a,'1'
ShowAbit0:
			call PrintChar
			djnz ShowAbit
			
			ld a,' '
			call PrintChar
			
	jp ShowBFB
			
ShowAF:
	push af
	push hl
	push bc
		push af
		
		
			ld l,a
			ld a,'A'
			call ShowHexOne
ShowBFB:			
			ld a,'F'
			call PrintChar
			ld a,':'
			call PrintChar
			
		pop af
		push af
			ld a,'C'
			jr c,ShowAF_C
			ld a,'-'
ShowAF_C:			
			call PrintChar
		pop af
		push af
			ld a,'V'
			jp pe,ShowAF_V
			ld a,'-'
ShowAF_V:	
			call PrintChar
		pop af
		push af
			ld a,'Z'
			jp z,ShowAF_Z
			ld a,'-'
ShowAF_Z:	
			call PrintChar
		pop af
		push af
			ld a,'S'
			jp m,ShowAF_S
			ld a,'-'
ShowAF_S:	
			call PrintChar
		pop af
		
		call NewLine
	pop bc
	pop hl
	pop af
	ret	
	

ShowAHLIX:
	push af
	push hl
	push bc
		push hl
			ld l,a
			ld a,'A'
			call ShowHexOne
		pop hl
		
		ld bc,'LH'
		call ShowHexPair
		
		ld bc,'XI'
		ld a,IXL
		ld l,a
		ld a,IXH
		ld h,a
		call ShowHexPair
		
		ld bc,'YI'
		ld a,IYL
		ld l,a
		ld a,IYH
		ld h,a
		call ShowHexPair
			
		call NewLine
	pop bc
	pop hl
	pop af
	ret

ShowAHLBC:
	push af
	push hl
	push bc
		push hl
			ld l,a
			ld a,'A'
			call ShowHexOne
		
			ld h,b
			ld l,c
			ld bc,'CB'
		
			call ShowHexPair
		
			ifndef BuildGBx			;Can't Show this on the GameBoy
				ld bc,'ED'
				ld h,d
				ld l,e
				call ShowHexPair
			endif
		pop hl
		ld bc,'LH'
		call ShowHexPair
			
		call NewLine
	pop bc
	pop hl
	pop af
	ret

ShowHexOne:		;A:L
	call Printchar
	ld a,':'
	call Printchar
	jr ShowHexOneB
	
ShowHexPair:	;BC:HL
	ld a,b
	call Printchar
	ld a,c
	call Printchar
	ld a,':'
	call Printchar
	ld a,h
	call ShowHex
ShowHexOneB:
	ld a,L
	call ShowHex
	ld a,' '
	call Printchar
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			End of Your program
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	read "QuickFooter.asm"