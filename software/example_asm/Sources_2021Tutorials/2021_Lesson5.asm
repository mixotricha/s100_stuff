;BuildCPC equ 1	; Build for Amstrad CPC on WinApe

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	ifdef vasm
		include "\SrcALL\VasmBuildCompat.asm"	;Vasm <> Winape Compat
	else
		read "\SrcALL\WinApeBuildCompat.asm"	;Vasm <> Winape Compat
	endif
	
	read "QuickHeader.asm"
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			Start Your Program Here
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	LD HL,0
	LD DE,0
	LD BC,0
	LD A,0

	;jp RLC_RRC				;RLC/RRC
	;jp SLA_SRA				;SLA/SRA
	;jp SLL_SRL				;SLL/SRL
	;jp RL_RR				;RL/RR
	jp BitwiseOps				;AND OR XOR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;RLC Rotate bits in register r Left and Copy the top bit to the Carry. 
;RRC Rotate bits in register r Right and Copy the bottom bit to the Carry. 

RLC_RRC:	
	ld a,%01010111
	
	call ShowAbitsF
	call NewLine
	
	ld b,8
Example1A:	
	rlc a 					;Rotate Left with Wrap
	call ShowAbitsF
	djnz Example1A
	
	call NewLine
	
	ld a,%01010111
	
	call ShowAbitsF
	call NewLine
	
	ld b,8
Example1B:	
	rrc a 					;Rotate Right with Wrap
	call ShowAbitsF
	djnz Example1B
	
	jp finish

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;SLA Shift the bits register r Left for Arithmetic.
;SRA Shift the bits in register r Right for Arithmetic.



SLA_SRA:	
	ld a,%01010111
	;ld a,%11010110
	
	call ShowAbitsF
	call NewLine
	
	ld b,8
Example2A:	
	sla a 					;Shift Left for Arithematic
	call ShowAbitsF
	djnz Example2A
	
	call NewLine
	
	ld a,%11010111
	;ld a,%11010110
	
	call ShowAbitsF
	call NewLine
	
	ld b,8
Example2B:	
	sra a 					;Shift Right for Arithematic
	call ShowAbitsF
	djnz Example2B
	
	jp finish
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;SLL Shift the bits in register r Left Logically (for unsigned numbers).
;SRL Shift the bits in register r Right Logically.

SLL_SRL:	
	ld a,%01010111
	;ld a,%11010110
	
	call ShowAbitsF
	call NewLine
	
	ld b,8
Example3A:	
	sll a 			;Undocumented Op (Mot available on eZ80)
	call ShowAbitsF
	djnz Example3A
	
	call NewLine
	
	ld a,%01010111
	;ld a,%11010110
	
	call ShowAbitsF
	call NewLine
	
	ld b,8
Example3B:	
	srl a 					;Shift Right for Logic
	call ShowAbitsF
	djnz Example3B
	
	jp finish
	
	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
RL_RR:	
	ld de,&0057
	ld de,&00D6
	call ShowDEbits
	call NewLine
	
	ld b,8
Example4A:	
	sla e 				;Shift Left for Arithematic
	rl d				;Shift the Carry left into the high byte
	call ShowDEbits
	djnz Example4A
	
	call NewLine
	
	ld de,&5700
	ld de,&D600
	
	call ShowDEbits
	call NewLine
	
	ld b,8
Example4B:	
	sra d 				;Shift Right for Arithematic
	rr e				;Shift the Carry right into the low byte
	call ShowDEbits
	djnz Example4B
	
	jp finish
			
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
BitwiseOps:
	ld a,%01010111
	call ShowAbitsF
	AND &F0					;Keep bits where mask bit is 1
	call ShowAbitsF
	
	call NewLine
	
	ld a,%01010111
	call ShowAbitsF
	OR &F0					;set bits where mask bit is 1
	call ShowAbitsF
	
	call NewLine
	
	ld a,%01010111
	ld b,&F0
	call ShowAbitsF
	XOR b					;Flip bits where mask bit is 1
	call ShowAbitsF
	
	call NewLine
	
	call ShowAbitsF
	cpl						;Equivalent of XOR &FF
	call ShowAbitsF
	cpl						;Equivalent of XOR &FF
	call ShowAbitsF
	
	
	
	jp finish
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	
	;jp Shutdown
Finish:
	di
	halt					;Lock the CPU

	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	

	
ShowDEbits:
	push af
	push hl
	push bc
		push af
			ld l,D
			ld a,'D'
			call ShowHexOne
			
			
			ld b,8			
ShowBbit:			
			ld a,'0'
			sla l
			jr nc,ShowBbit0
			ld a,'1'
ShowBbit0:
			call PrintChar
			djnz ShowBbit
			
			ld a,' '
			call PrintChar
		pop af	
	
		ld l,e
		ld a,'E'
		call ShowHexOne
		
		ld b,8			
ShowBbit2:			
		ld a,'0'
		sla l
		jr nc,ShowBbit02
		ld a,'1'
ShowBbit02:
		call PrintChar
		djnz ShowBbit2
		
		ld a,' '
		call PrintChar
		
		call NewLine
	pop bc
	pop hl
	pop af
	ret	
		
		

	
ShowAbitsF:
	push af
	push hl
	push bc
ShowAbitsFb:
		push af
		
		
			ld l,a
			ld a,'A'
			call ShowHexOne
			
			
			ld b,8			
ShowAbit:			
			ld a,'0'
			sla l
			jr nc,ShowAbit0
			ld a,'1'
ShowAbit0:
			call PrintChar
			djnz ShowAbit
			
			ld a,' '
			call PrintChar
			
	jp ShowBFB
			
ShowAF:
	push af
	push hl
	push bc
		push af
		
		
			ld l,a
			ld a,'A'
			call ShowHexOne
ShowBFB:			
			ld a,'F'
			call PrintChar
			ld a,':'
			call PrintChar
			
		pop af
		push af
			ld a,'C'
			jr c,ShowAF_C
			ld a,'-'
ShowAF_C:			
			call PrintChar
		pop af
		push af
			ld a,'V'
			jp pe,ShowAF_V
			ld a,'-'
ShowAF_V:	
			call PrintChar
		pop af
		push af
			ld a,'Z'
			jp z,ShowAF_Z
			ld a,'-'
ShowAF_Z:	
			call PrintChar
		pop af
		push af
			ld a,'S'
			jp m,ShowAF_S
			ld a,'-'
ShowAF_S:	
			call PrintChar
		pop af
		
		call NewLine
	pop bc
	pop hl
	pop af
	ret	
	
ShowHexOne:		;A:L
	call Printchar
	ld a,':'
	call Printchar
	jr ShowHexOneB
	
ShowHexPair:	;BC:HL
	ld a,b
	call Printchar
	ld a,c
	call Printchar
	ld a,':'
	call Printchar
	ld a,h
	call ShowHex
ShowHexOneB:
	ld a,L
	call ShowHex
	ld a,' '
	call Printchar
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			End of Your program
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	read "QuickFooter.asm"