;BuildCPC equ 1	; Build for Amstrad CPC on WinApe

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	ifdef vasm
		include "\SrcALL\VasmBuildCompat.asm"	;Vasm <> Winape Compat
	else
		read "\SrcALL\WinApeBuildCompat.asm"	;Vasm <> Winape Compat
	endif
	
	read "QuickHeader.asm"
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			Start Your Program Here
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	ld ix,0
	add ix,sp
	ld a,ixl
	and %11110000		;Round Stack pointer to a multiple of 16
	ld ixl,a
	
	ld sp,ix
	
	ld bc,-8
	add ix,bc			;IX points 16 bytes before our test stack
	
	LD HL,0
	LD DE,0
	LD BC,0
	LD A,0

	;jp StackTest1		;Simple Backup / Restore of values
	;jp StackTest2		;The Stack and Subs
	;jp StackTest3		;Special Stack Commands
	jp ShadowRegs		;Shadow Registers and Swaps

	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

StackTest1:	
	ld hl,&FEDC

	call ShowTestStackB
	call NewLine
	
	push hl					;Backup HL
		ld hl,&9876
		
		call ShowTestStackB
		call NewLine
	
		push hl				;Back up 2nd HL
			call ShowTestStackB
			call NewLine
		pop hl				;Restore 2nd HL
		call ShowTestStackB
		call NewLine
	pop bc					;Restore 1st 'HL' ... into BC
	call ShowTestStackB
	call NewLine		
		
	push af					;Can also Push pop AF/BC/DE/IX/IY
	pop af
	
	jp Finish

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;Subroutines also use the stack... they push the return address!
	
StackTest2:
	ld bc,RetTest
	
	call ShowTestStackB		;Show Current stack
	
	Call Subroutine1
RetTest:	
	
	call ShowTestStackB		;Show Current stack

	jp Finish
	
Subroutine1:
	ld hl,&1234
	push hl	
		call ShowTestStackB		;Show Current stack
		
		call Subroutine2
		
		call ShowTestStackB		;Show Current stack
	pop hl
	ret

Subroutine2:
	ld hl,&5678
	push hl	
		call NewLine
		call ShowTestStackB		;Show Current stack
		call NewLine
	pop hl
	ret		
		
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;There are a small, but varied range of commands we can use with SP
;See the cheatsheet for more details
	
;Can INC and DEC SP	

StackTest3:			
	call ShowTestStackB
	ld hl,&FFEE
	push hl				;2 bytes onto stack
		push hl				;2 bytes onto stack
			call ShowTestStackB		;Show Current stack
		inc sp				;Shift SP 1 byte
		inc sp				;Shift SP 1 byte
	inc sp				;Shift SP 1 byte
	inc sp				;Shift SP 1 byte
	
	call ShowTestStackB
	call NewLine
	
	call ShowAHLBCSP
	
	ld hl,0				;There's no LD HL,SP command :-(
	add hl,sp			;Get the current stack pointer in HL
	
	call ShowAHLBCSP
	
	dec hl				;Reduce HL
	ld sp,hl			;Store back to the stack pointer
	
	call ShowAHLBCSP
	
	ld (TestRam),sp		;Backup Stack Pointer to address TestRam
	ld sp,(TestRam)		;Restore Stack Pointer from address TestRam
	
	call NewLine
	
	call ShowTestStackB		;Show Current stack
	
	ld hl,&FFEE
	push hl					;Put a value on the stack
	ld hl,&1234
	
	call ShowTestStackB		;Show Current stack
	
	ex (sp),hl				;Swap HL and the value on the stack (SP)
	
	call ShowTestStackB		;Show Current stack
	
	jp Finish
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;There is a 'Spare' AF BC DE and HL 
;These are intended for use by interrupt handlers, but if we know what
;we're doing we can use them!
;This will crash on some systems where the firmware uses them though!
ShadowRegs:	
	ld hl,&1234
	ld bc,&5678
	ld de,&9ABC
	ld a,&FF
	
	call ShowRegsPlus
	
	ex af,af'				;Swap AF and shadow AF
		call ShowRegsPlus

		exx					;Swap BC,DE,HL and shadown versions
			call ShowRegsPlus
			call NewLines
			
			ld a,&01
			ld bc,&0202
			ld de,&0303
			ld hl,&0404
			
			call ShowRegsPlus
			call NewLines
		exx					;Swap BC,DE,HL and shadown versions
	ex af,af'				;Swap AF and shadow AF
	
	call ShowRegsPlus
	
	call NewLines
	
;We can also swap DE,HL a lot of commands use HL, so this is a 
; quick way to use those commands with de

	call ShowRegsPlusB

	ex de,hl			
					
	call ShowRegsPlusB		
	
	ex de,hl			
					
	call ShowRegsPlusB
	
	jp Finish
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

	
	jp Shutdown
Finish:
	di
	halt					;Lock the CPU

ShowTestStackB:	
	push af
	push af
	push hl
	push bc
		call ShowTestStack
	pop bc
	pop hl
	pop af
	pop af
	jp ShowAHLBCSP
	
	
ShowTestStack:				;IX must point to our stack range
	push ix
	pop hl					;Address to dump
	ld c,8					;Bytes to dump
	jp Monitor_MemDumpDirect




	
ShowRegsPlus:
	ld ixl,':'
	call ShowRegsPlusB
	ld ixl,39		; '= ASC 39
	ex af,af'
	exx
		call ShowRegsPlusB
		call NewLines
	ex af,af'
	exx
	ret
	
ShowRegsPlusB:	
	push af
	push hl
	push bc
		push hl
			push bc
				push af
				pop hl
				ld bc,'FA'
				call ShowHexPairs
			pop hl
			ld bc,'CB'
			call ShowHexPairs
			
			ld l,e
			ld h,d
			ld bc,'ED'
			call ShowHexPairs
		pop hl
		ld bc,'LH'
		call ShowHexPairs
		
		ifndef BuildSxx
			call NewLine
		endif
	pop bc
	pop hl
	pop af
	ret

NewLines:
	push af
		call NewLine
	pop af
	ret
	

ShowHexPairs:	;BC:HL
	ld a,b
	call Printchar
	ld a,c
	call Printchar
	ld a,ixl
	call Printchar
	ld a,h
	call ShowHex
	ld a,L
	call ShowHex
	ld a,' '
	call Printchar
	ret
	
	


	
ShowAHLBCSP:
	push af
	push hl
	push bc
		push hl
			ld l,a
			ld a,'A'
			call ShowHexOne
		
			ld h,b
			ld l,c
			ld bc,'CB'
		
			call ShowHexPair
		
			
		pop hl
		ld bc,'LH'
		call ShowHexPair
		
		ifndef BuildGBx			;Can't Show this on the GameBoy
			ld bc,'PS'
			ld hl,8
			add hl,sp
			call ShowHexPair
		endif
			
		call NewLine
	pop bc
	pop hl
	pop af
	ret

ShowHexOne:		;A:L
	call Printchar
	ld a,':'
	call Printchar
	jr ShowHexOneB
	
ShowHexPair:	;BC:HL
	ld a,b
	call Printchar
	ld a,c
	call Printchar
	ld a,':'
	call Printchar
	ld a,h
	call ShowHex
ShowHexOneB:
	ld a,L
	call ShowHex
	ld a,' '
	call Printchar
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			End of Your program
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	read "QuickFooter.asm"