;BuildCPC equ 1	; Build for Amstrad CPC on WinApe

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	ifdef vasm
		include "\SrcALL\VasmBuildCompat.asm"	;Vasm <> Winape Compat
	else
		read "\SrcALL\WinApeBuildCompat.asm"	;Vasm <> Winape Compat
	endif
	
	read "QuickHeader.asm"
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			Start Your Program Here
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	
	
	ld hl,Message			
	
	call PrintString		;Print Message

	call newline
	
	call Monitor			;Show Registers

	call newline
	call newline
	
	ld hl,Message			;Address to dump
	ld c,16					;Bytes to dump 
	call Monitor_MemDumpDirect
	
	
	;jp Shutdown
	di
	halt					;Lock the CPU


Message: db 'Hello World 323!',255		;255 terminated string


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			End of Your program
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	read "QuickFooter.asm"