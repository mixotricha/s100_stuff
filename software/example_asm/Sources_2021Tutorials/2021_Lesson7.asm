;BuildCPC equ 1	; Build for Amstrad CPC on WinApe

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	ifdef vasm
		include "\SrcALL\VasmBuildCompat.asm"	;Vasm <> Winape Compat
	else
		read "\SrcALL\WinApeBuildCompat.asm"	;Vasm <> Winape Compat
	endif
	
	read "QuickHeader.asm"
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			Start Your Program Here
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	LD HL,0
	LD DE,0
	LD BC,0
	LD A,0
	
	;jp TestLdirLddr
	;jp TestOutIn_SMS
	;jp TestOutIn_ZX
	;jp TestOtir_SMS
	;jp TestOtir_MSX1
	;jp TestInterrupt_CPC
	jp TestInterrupt_SAM
	
TestLdirLddr:	
	call ShowTestRam
	
	ld hl,TestData		;Source
	ld de,TestRam		;Destination
	ld bc,6				;ByteCount
	
	ldir				;Copy BC bytes from HL to DE
	
	call ShowTestRam
	
	ld hl,TestDataEnd-1	;Source
	ld de,TestRam+15	;Destination
	ld bc,6				;ByteCount
	
	lddr				;Copy BC bytes from HL to DE Desc
	
	call ShowTestRam
	
	call NewLine
	
	call ShowTestRam
	
	ld hl,TestData2		;Source
	ld de,TestRam		;Destination
	ldi					;Copy 1 byte from HL to DE
	ldi					;Copy 1 byte from HL to DE
	ldi					;Copy 1 byte from HL to DE
	
	call ShowTestRam
	
	ld hl,TestData2End-1;Source
	ld de,TestRam+15	;Destination
	ldd					;Copy 1 byte from HL to DE Desc
	ldd					;Copy 1 byte from HL to DE Desc
	ldd					;Copy 1 byte from HL to DE Desc
	
	call ShowTestRam
	
	jp Finish
	
TestData:	
	db &01,&02,&03,&04,&05,&06
TestDataEnd:	
	
TestData2:	
	db &FF,&EE,&DD,&CC,&AA,&99
TestData2End:		
	
ShowTestRam:
	ld hl,TestRam			;Address to dump
	ld c,16					;Bytes to dump
	call Monitor_MemDumpDirect
	
	ret	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
TestOutIn_SMS:
;Lets change the color of the background!
	ld hl,&C000			;VRAM address to change (&C000=Color 0)
	ld c,&BF			;VDP control port 
	
	out (c),l			;Send L to Port C
	nop					;No Operation (Short Delay)
	out (c),h			;Send H to Port C
	nop					;No Operation (Short Delay)
	
	ld a,%00000000		;%--BBGGRR
	
	out (&BE),a			;Send A to VDP Data Port
	
;Lets Read in from the Joypad
TestOutIn_SMSb:

	in a,(&DC)			;Get A from the Joypad Port
	
	call ShowA
	;UD are in Player1's port	---FRLDU 0=Down 1=up
	
	ld bc,&4000
	call PauseBC

	jp TestOutIn_SMSb
	
	
	
TestOutIn_ZX:
	ld a,3				;Border Color
	out (&fe),a			;Set border color (&xxFE)
	
TestOutIn_ZXb:
	ld bc,&7FFE 
	in a,(c)			;Get A from port BC 
						;(Speccy uses 16 bit ports)
	call ShowA
	;&7F = %01111111 B, N, M, Sym, Space

	ld bc,&4000
	call PauseBC

	jp TestOutIn_ZXb
	
	

PauseBC:
	dec bc
	ld a,b
	or c
	jr nz,PauseBC
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
TestOtir_SMS:
	ld hl,Message	
	call PrintString		;Print Message

; Like LDIR...	OTDR, OUTD, OUTI are also available
	ld hl,&C000
	ld c,&BF
	out (c),l			;Send L to Port C
	nop					;No Operation (Short Delay)
	out (c),h			;Send H to Port C
	nop
	
    ld hl,PaletteDataSMS	;Source of data
	ld b,16			;Byte count (16 on SMS)
	ld c,&BE		;Destination port
	otir				;Out (c),(hl).. inc HL... dec B, djnz  

	jp Finish
	
Message: db 'Hello World 323!',255		;255 terminated string

PaletteDataSMS:
	db %00000000; ;0  --BBGGRR
	db %00001100; ;1  --BBGGRR
	db %00010101; ;2  --BBGGRR
	db %00101010; ;3  --BBGGRR
	db %00111111; ;4  --BBGGRR
	db %00011000; ;5  --BBGGRR
	db %00001100; ;6  --BBGGRR
	db %00000011; ;7  --BBGGRR
	db %00010111; ;8  --BBGGRR
	db %00011011; ;9  --BBGGRR
	db %00011111; ;10  --BBGGRR
	db %00100010; ;11  --BBGGRR
	db %00110011; ;12  --BBGGRR
	db %00110000; ;13  --BBGGRR
	db %00100100; ;14  --BBGGRR
	db %00111100; ;15  --BBGGRR

; Like LDIR...	OTDR, OUTD, OUTI are also available
		
TestOtir_MSX1:
	ld hl,&5800			;Tilemap VRAM
	ld c,&99			;VDP Control
	out (c),l			;Send L to Port C
	nop					;No Operation (Short Delay)
	out (c),h			;Send H to Port C
	nop
	
    ld hl,TileDataMSX1	;Source of data
	ld b,31				;Byte count (16)
	ld c,&98			;Destination port (VDP Data)
	otir				;Out (c),(hl).. inc HL... dec B, djnz  
	
	jp Finish
	
TileDataMSX1:
	db 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16
	db 17,18,19,20,21,22,23,24,25,26,27,28,29,30,31
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
TestInterrupt_CPC:	
	di					;Disable Interrupts
	ld hl,&0038			;IM1 interrupt address (RST7)
	ld a,&C3			;JP
	ld (hl),a
	inc hl
	ld de,InterruptHanlder_CPC
	ld (hl),e			;Address of interrupt handler
	inc hl
	ld (hl),d			

	exx
	ld hl,&5458			;Two colors in the shadow register
	exx
	
	ei					;Turn on interrupts
	ld b,255			;Delay count
InfLoop_CPC:	
	halt				;Wait For Interrupt
	djnz InfLoop_CPC

	di
InfLoop_CPC2:	
		rst &38 		;Force interrupt (RST 7 on winape)
	jp InfLoop_CPC2
	

InterruptHanlder_CPC:
	di	;Only needed because we're calling directly with RST 7
	
	ex af,af'			;Swap in shadow registers
	exx					
		ld bc,&7f00		;&7Fxx = Palette  0 = Color 0
		out (c),c
		out (c),l		;Set Color
		
		ld a,h			;Swap H and L
		ld h,l
		ld l,a
	exx					;Swap in shadow registers
	ex af,af'
	ei					;Turn interrupts back on
	ret


	
TestInterrupt_SAM:	
	di
		; WRrBBBBB
	ld a,%00101110  
	out (250),a		;Page in RAM to area &0000-&FFFF
	
	ld hl,&0038		;IM1 interrupt address (RST7)
	ld a,&C3		;JP
	ld (hl),a
	inc hl
	ld de,InterruptHanlder_SAM
	ld (hl),e
	inc hl
	ld (hl),d

	ei
InfLoop_SAM:	
	halt				;Wait For Interrupt
	djnz InfLoop_SAM

	di
InfLoop_SAM2:	
		rst &38 		;Force interrupt (RST 7 on winape)
	jp InfLoop_SAM2
	
	

InterruptHanlder_SAM:
	di	;Only needed because we're calling directly with RST 7
	
	ex af,af'
	exx
	
		ld c,254		;Port 254= Border & Sound
		inc b
		out (c),b		
		
	exx
	ex af,af'
	
	ei
	ret

	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
	;jp Shutdown
Finish:
	di
	halt					;Lock the CPU


ShowAHLBC:
	push af
	push hl
	push bc
		push hl
			ld l,a
			ld a,'A'
			call ShowHexOne
		
			ld h,b
			ld l,c
			ld bc,'CB'
		
			call ShowHexPair
		
			ifndef BuildGBx			;Can't Show this on the GameBoy
				ld bc,'ED'
				ld h,d
				ld l,e
				call ShowHexPair
			endif
		pop hl
		ld bc,'LH'
		call ShowHexPair
			
		call NewLine
	pop bc
	pop hl
	pop af
	ret

ShowA:
	push af
	push hl
		ld l,a
		ld a,'A'
		call ShowHexOne	
	pop hl
	pop af
	ret
	
ShowHexOne:		;A:L
	call Printchar
	ld a,':'
	call Printchar
	jr ShowHexOneB
	
ShowHexPair:	;BC:HL
	ld a,b
	call Printchar
	ld a,c
	call Printchar
	ld a,':'
	call Printchar
	ld a,h
	call ShowHex
ShowHexOneB:
	ld a,L
	call ShowHex
	ld a,' '
	call Printchar
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			End of Your program
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	read "QuickFooter.asm"