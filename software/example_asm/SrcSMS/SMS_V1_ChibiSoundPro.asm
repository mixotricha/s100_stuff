

ChibiOctive:
	;   E     F     G      A     B     C     D   
	dw &0043,&0043,&0043,&0043,&0043,&0043,&0043 ;0
	dw &0043,&0043,&0043,&0043,&1D5E,&2A6E,&4161 ;1
	dw &565D,&6024,&71A2,&80DB,&8EA4,&94EA,&A0A5;2
	dw &AB56,&AFC3,&B882,&C06D,&C76A,&D078,&D54E ;3 -Line E4-D5 of XLS
	dw &D548,&D7DE,&DC17,&E01A,&E3AF,&E520,&E801 ;4
	dw &EAAE,&EBEB,&EE11,&F003,&F18E,&F2B2,&F3D2;5
	dw &F577,&F5FA,&F71D,&F824,&F8DC,&F9B0,&FA68 ;6
	dw &FB00,&FBBB,&FC00,&FCCC,&FDDD,&FEEE,&FFFF ;EXTRA
	   



ChibiTone:	;E=Note (Bit 1-6=Note  Bit0=Flat)
	;E-F-G-A-B-C-D-... Octive 0-6
	push hl
			ld hl,ChibiToneGotNote
			push de
				res 0,e
				add hl,de
			pop de
			ld a,e
			ld e,(hl)
			inc hl
			ld d,(hl)
			bit 0,a
			jp z,ChibiToneGotNote
			inc hl
			srl d
			rr e

			ld a,(hl)		;Half note
			inc hl
			ld h,(hl)
			ld l,a
			srl h
			rr l
			add hl,de		;add two halves
			ex de,hl 
ChibiToneGotNote:
	pop hl

;H=Volume (0-255) 
;L=Channel Num (0-127 unused channels will wrap around) / Top Bit=Noise
;DE=Pitch (0-65535)

ChibiSoundPro:			

	bit 7,l
	jr nz,ChibiSoundPro_Noise
	
	ld bc,ChannelMask
	
	ld a,l
	and %00000011
	add c
	ld c,a
	ld a,(bc)
	ld b,a
	jr ChibiSoundPro_NotNoise
ChibiSoundPro_Noise:
	ld b,%01000000		;Noise Channel uses tone 2
	
ChibiSoundPro_NotNoise:	
	ld a,h
	or a
	jr z,silent
	
	
	
	ld a,d
	cpl
	ld d,a
	ld a,e
	cpl
	ld e,a
	
	srl d
	rr e
	srl d
	rr e
	
	srl e
	srl e
	srl e
	srl e
	ld a,e
         ;1CCTLLLL	(Latch - Channel Type DataL
	or   %10000000	; Low Tone
	or b	;Channelnum
	out (&7F),a
	ld a,d
		;0-HHHHHH
	;and %00111111	;High Tone
	out (&7F),a

	ld a,h
	srl a
	srl a
	srl a
	srl a
	ld c,a
	xor %10011111	;Set Volume
	or b	;Channelnum
	out (&7F),a
	ld a,%11111111	;Mute noise
	out (&7F),a

	bit 7,l		;We're done if there is no noise
	ret z

	ld a,%11011111	;1CCTVVVV	(Latch - Channel Type Volume)	
	out (&7F),a	;Mute Tone 2

	ld a,%11100111	;1CCT-MRR	(Latch - Channel Type... noise Mode (1=white) Rate (Rate 11= use Tone Channel 2)
	out (&7F),a

	ld a,c
	xor %11111111	;Set Volume of noise
	out (&7F),a

	ret

silent:	;Mute both channels
		
		 ;1CCTVVVV
	
	ld a,%10011111	
	or b	;Channelnum
	out (&7F),a
	ret
	align 4
ChannelMask:	db %00000000,%00100000,%01000000,%00000000