

ChibiOctive:
	;   E     F     G      A     B     C     D   
	dw &3000,&4000,&5000,&6000,&7000,&8000,&9000 ;0
	dw &A1A6,&A6CB,&B096,&B8F4,&C097,&C446,&CAD4 ;1
	dw &D09A,&D32A ,&D816,&DC76,&E058,&E218,&E55B;2
	dw &E843,&E9A7,&EC11,&EE31,&F029,&F10D,&F2AA ;3
	dw &F414,&F4CC,&F60C,&F71C,&F819,&F889,&F95A ;4
	dw &FA04,&FA6C,&FAF3,&FB8E,&FC0C,&FC3E,&FCAC;5
	dw &FCF8,&FD2E,&FD79,&FDCA,&FDFC,&FE1A,&FE4D ;6
	dw &FE40,&FEC0,&FF00,&FF40,&FF80,&FFC0,&FFFF
	   



ChibiTone:	;E=Note (Bit 1-6=Note  Bit0=Flat)
	;E-F-G-A-B-C-D-... Octive 0-6
	push hl
			ld hl,ChibiToneGotNote
			push de
				res 0,e
				add hl,de
			pop de
			ld a,e
			ld e,(hl)
			inc hl
			ld d,(hl)
			bit 0,a
			jp z,ChibiToneGotNote
			inc hl
			srl d
			rr e

			ld a,(hl)		;Half note
			inc hl
			ld h,(hl)
			ld l,a
			srl h
			rr l
			add hl,de		;add two halves
			ex de,hl 
ChibiToneGotNote:
	pop hl

;H=Volume (0-255) 
;L=Channel Num (0-127 unused channels will wrap around) / Top Bit=Noise
;DE=Pitch (0-65535)

ChibiSoundPro:			
	ld a,h
	or a
	jp z,silent		;Zero turns off sound
	
	ld a,ixl
	bit 7,a
	jr z,ChibiSoundProNoEnv

	and %00001111
	ld c,a
	ld a,13
	call AYRegWrite	
	ld c,0
	ld a,12
	call AYRegWrite
	ld a,ixh
	ld c,a
	ld a,11
	call AYRegWrite
	

	ld l,2
	jr ChibiSoundProChannelOK
	
	
ChibiSoundProNoEnv:				
	
	ld a,l
	and %00000011
	cp 3
	jr c,ChibiSoundProChannelOK
	dec l			;Only 3 channels!
ChibiSoundProChannelOK:
	ld a,d 
	cpl
	ld d,a
	ld a,e
	cpl
 	ld e,a
	inc de


	srl d		;Ditch bottom 4 bits
	rr e
	srl d
	rr e
	srl d
	rr e
	srl d
	rr e


	ld c,e
	ld a,l			;TTTTTTTT Tone Lower 8 bits	B
	and %00000011
	rlca
	push af
		call AYRegWrite

		ld c,d
	pop af
				;----TTTT Tone Upper 4 bits
	inc a
	call AYRegWrite
	
	bit 7,l			;Noise bit N-------
	jr z,AYNoNoise

	call DoChannelMask
	and %00111111
	cpl
	ld d,a
	ld a,(bc)
	and d
	ld (bc),a
	ld c,a
	ld a,7
	call AYRegWrite

	ld a,6			;Noise ---NNNNN
	ld c,%00011111
	call AYRegWrite

	jr AYMakeTone
AYNoNoise:
	call DoChannelMask
	and %00000111
	cpl
	ld d,a

	ld a,(bc)
	and d
	ld (bc),a
	ld c,a

	ld a,7			;Mixer  --NNNTTT (1=off) --CBACBA

;	ld c,%10111101
	call AYRegWrite
;	jr AYMakeTone
AYMakeTone:
;*** these are incorrectly marked as Amplitude Control (Reg R10,R11,R12) in some documentation! ***
	ld c,h	;VVVVVVVV = Volume bits
	srl c
	srl c
	srl c
	srl c
	
	ld a,ixl
	bit 7,a
	jr z,AYMakeTone_EnvOff
	set 4,c			;Turn Envelope on
AYMakeTone_EnvOff:
	
	ld a,l			;4-bit Volume / 2-bit Envelope Select for channel A ---EVVVV
	and %00000011
	add 8
	call AYRegWrite
	ret

DoChannelMask:
	ld bc,ChannelMask
	ld a,l
	and %00000011
	push af
		add c
		ld c,a
		ld a,(bc)
		ld d,a
		ld bc,ChannelStatus
	pop af
	add c
	ld c,a
	ld a,d
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
silent:
	call DoChannelMask
	and %00111111
	ld d,a

	ld a,(bc)
	or d
	ld (bc),a
	ld c,a

	ld a,7			;Mixer  --NNNTTT (1=off) --CBACBA
			
	call AYRegWrite				

	
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
AYRegWrite:
	ifdef BuildCPC
		push bc
			ld b,&f4
			ld c,a
			out (c),c	;#f4 Regnum

			ld bc,&F6C0	;Select REG
			out (c),c	

			ld bc,&f600	;Inactive
			out (c),c

			ld bc,&F680	;Write VALUE
			out (c),c	
		pop bc

		ld b,&F4		;#f4 value
		out (c),c

		ld bc,&f600		;inactive
		out (c),c
		ret
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	ifdef BuildZXS
		push bc
			ld bc,&FFFD
			out (c),a	;Regnum
		pop bc
AYRegWriteQuick:
		ld a,c
		ld bc,&BFFD
		out (c),a	;Value
		ret
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef BuildMSX
		push bc
			out (#a0),a	;regnum
		pop bc
AYRegWriteQuick:
		ld a,c
		out (#a1),a	;value
		ret
	endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	align 4
ChannelMask:	db %00001001,%00010010,%00100100,0
ChannelStatus:	db %10111111,%10111111,%10111111,0