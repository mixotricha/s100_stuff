	
	
Player_ReadControlsDual:	

Read_Keyboard:
KeyboardScanner_Read:
	;Key input on the CPC goes through the 8255 PPI an AY soundchip, we need to use the PPI to talk to the AY, 
	;and tell it to read input through reg 14

	;Port F6C0 = Select Regsiter
	;Port F600 = Inactive
	;Port F6xx = Recieve Data
	;Port F4xx = Send Data to selected Register

	ld bc,#f782     ;Select PPI port direction... A out /C out 
;	ld hl,KeyboardScanner_KeyPresses    ;Destination Mem for the Keypresses
	xor a
	di              
	out (c),c       


	ld bc,#f40e     ; Select Ay reg 14 on ppi port A 
	ld e,b          
	out (c),c       
	ld bc,#f6c0     ;This value is an AY index (R14) 
	ld d,b          
	out (c),c        
	out (c),a   	; F600 - set to inactive (needed)
	ld bc,#f792     ; Set PPI port direction A in/C out 
	out (c),c       
	ld a,#49        ;Line 49
	ld c,#4a        
KeyboardScanner_Loop
	ld b,d    	;d=#f6     
	out (c),a       ;select line &F640-F64A
	ld b,e    	;e=#f4   
	in h,(c)
	ld l,255
	ld bc,#f782     
	out (c),c       ; reet PPI port direction - PPI port A out / C out 
	ei 
KeyboardScanner_AllowJoysticks:             
	ret



;Line 	7 	6 	5 	4 	3 	2 	1 	0
;&40 	F Dot 	ENTER 	F3 	F6 	F9 	CUR D 	CUR R 	CUR U
;&41 	F0 	F2 	F1 	F5 	F8 	F7 	COPY 	CUR L
;&42 	CTRL 	\ 	SHIFT 	F4 	] 	RETN 	[ 	CLR
;&43 	. 	/ 	 : 	 ; 	P 	@ 	- 	^
;&44 	, 	M 	K 	L 	I 	O 	9 	0
;&45 	SPACE 	N 	J 	H 	Y 	U 	7 	8
;&46 	V 	B 	F 	G/J2 F1	T/ J2 R	R/ J2 L	5/ J2 D 6/ J2 U
;&47 	X 	C 	D 	S 	W 	E 	3 	4
;&48 	Z 	CAPSLK 	A 	TAB 	Q 	ESC 	2 	1
;&49 	DEL 	J1 F3 	J1 F2 	J1 F 1 	J1 R 	J1 L 	J1 D 	J1 U

