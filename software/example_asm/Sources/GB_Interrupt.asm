GBSpriteCache equ 1	;Enable VBLANK
UseLcdStatInterruptHandler equ 1 ;Enable LCD Stat

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef vasm
		include "..\SrcALL\VasmBuildCompat.asm"
	else
		read "..\SrcALL\WinApeBuildCompat.asm"
	endif
	read "..\SrcALL\CPU_Compatability.asm"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


	read "..\SrcALL\V1_Header.asm"
	read "..\SrcALL\V1_BitmapMemory_Header.asm"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			Start Your Program Here
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	

	Call DOINIT		;Get ready

	call ScreenINIT		;Enable the Bitmap Screen

	ld	hl, SpriteData1		;240 Tiles
	ld	bc, SpriteData1End-SpriteData1
	ld  de,&8000 			;Tiles 0-255 (Bank 1)
	call DefineTiles
	
	ld	hl, SpriteData2		;120 tiles
	ld	bc, SpriteData2End-SpriteData2
	ld  de,&9000 			;Tiles -128 to 127 (Bank 2)
	call DefineTiles
	
	;1st tile bank (Lines 0-95)
	ld bc,&0000 ;xy pos
	ld hl,&140C ;WH
	ld de,0		;Tile
	call FillAreaWithTiles		;Fill a grid area 
	
	;2nd tile bank (Lines 96-144)
	ld bc,&000C ;xy pos
	ld hl,&1406 ;WH
	ld de,0		;Tile
	call FillAreaWithTiles		;Fill a grid area 
	
	
		; ---JSTLV	J=Joypad / S=Serial / T=Timer / L=Lcd stat / V=vblank
	ld a,%00000011	;VBlank On / LCD Stat On (LYC)
	ld (&FFFF),a	;IE - Interrupt Enable (R/W)
	
	ld a,96			;Line 96 (12 tiles down... 12x20=240 Tiles)
	ld (&FF45),a	;LYC - LY Compare Line number
	
		; -LOVHCMM
	ld a,%01000000
	ld (&FF41),a	;Turn on LYC
	
	
		; -LOVHCMM	LY Coincidence interrupt on, M2 OAM Interrupt on, M1 Vblank interrupt on, M0 Hblank interrupt on, Coincidence flag, MM=video mode (0/1 =Vram available)
		
	ei
infloop:
	jp InfLoop


VblankInterruptHandler:
	push hl
		ld hl,&FF40		;DwWBbOoC	B=Background Tile Patterns
		set 4,(hl)		;Background Tiles @ &8000
	pop hl
	reti
LcdStatInterruptHandler:	
	push hl
		ld hl,&FF40		;DwWBbOoC	B=Background Tile Patterns
		res 4,(hl) 		;Background Tiles @ &8800
	pop hl
	reti
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	

Palette:
	;   -grb
	dw &0000	;0 - Background
	dw &0099	;1
	dw &0E0F	;2
	dw &0FFF	;3 - Last color in 4 color modes
	dw &000F	;4
	dw &004F	;5
	dw &008F	;6
	dw &00AF	;7
	dw &00FF	;8
	dw &04FF	;9
	dw &08FF	;10
	dw &0AFF	;11
	dw &0CCC	;12
	dw &0AAA	;13
	dw &0888	;14
	dw &0444	;15
	dw &0000	;Border


	;Sprite Data of our Chibiko character, this needs to be in the native format of the graphics system we're working with
SpriteData2:
		incbin "Z:\ResALL\sprites\GBTileScreen2.raw"  
SpriteData2End:
SpriteData1:
		incbin "Z:\ResALL\sprites\GBTileScreen1.raw"  
SpriteData1End:

	;2 bit bitmap font, this is generic for all systems
BitmapFont:
	db 0
BitmapFontEnd:
	read "..\SrcALL\V1_VdpMemory.asm"
	read "..\SrcALL\V1_HardwareTileArray.asm"
	read "..\SrcAll\V1_Palette.asm"	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			End of Your program
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	read "..\SrcALL\V1_BitmapMemory.asm"

	read "..\SrcALL\V1_Functions.asm"
	read "..\SrcALL\V1_Footer.asm"
