	;Unrem this if building with vasm
	include "\SrcALL\VasmBuildCompat.asm"

	include "\SrcZXN\ZXN_V1_Z80_Extensions.asm"
	include "\SrcZX\ZX_V1_header.asm"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;			Classic Spectrum Screen
	
	ld hl,SampleScreen		;Source 
	ld de,&4000				;Destination VRAM
	ld bc,SampleScreen_End-SampleScreen	;Length
	;ldir
	
	
	; ld a,0
	; ld bc,&303B
	; out (c),a
	
		
		
	
	ld c,&5b
	ld b,128
	ld hl,MySprite
	otir
				;76543210
	nextreg $15,%00000001	;Turn on sprite
	;nextreg $09,%00010000	;SpriteLockstep
	

	ld a,0
	ld bc,&303B
	out (c),a
	
	
	ld a,64
	out (&57),a		;xpos 0 
	ld a,64
	out (&57),a		;ypos 1
	 
	;	  PPPPXYRX 2
	ld a,%00000000
	out (&57),a		;P P P P XM YM R X8/PR
		;P = 4-bit Palette Offset
		;XM = 1 to mirror the sprite image horizontally
		;YM = 1 to mirror the sprite image vertically
		;R = 1 to rotate the sprite image 90 degrees clockwise
		;X8 = Ninth bit of the sprite’s X coordinate
		;PR = 1 to indicate P is relative to the anchor’s palette offset (relative sprites only) 
		; VENNNNNN 3
	ld a,%11000000
	out (&57),a		;V E N5 N4 N3 N2 N1 N0
		;V = 1 to make the sprite visible
		;E = 1 to enable attribute byte 4
		;N = Sprite pattern to use 0-63 
		;If E=0, the sprite is fully described by sprite attributes 0-3. The sprite pattern is an 8-bit one identified by pattern N=0-63. The sprite is an anchor and cannot be made relative. The sprite is displayed as if sprite attribute 4 is zero. 
		;If E=1, the sprite is further described by sprite attribute 4. 

		; HNTXXYYy 4
	ld a,%10000000
	out (&57),a	;H N6 T X X Y Y Y8
		;H = 1 if the sprite pattern is 4-bit
		;N6 = 7th pattern bit if the sprite pattern is 4-bit
		;T = 0 if relative sprites are composite type else 1 for unified type
		;XX = Magnification in the X direction (00 = 1x, 01 = 2x, 10 = 4x, 11 = 8x)
		;YY = Magnification in the Y direction (00 = 1x, 01 = 2x, 10 = 4x, 11 = 8x)
		;Y8 = Ninth bit of the sprite’s Y coordinate 
		;{H,N6} must not equal {0,1} as this combination is used to indicate a relative sprite. 

	;ld a,0
	;out (&57),a	
	;out (&57),a	

	
	
	
	ld a,1
	ld bc,&303B
	out (c),a
	
	
	ld a,128
	out (&57),a		;xpos 0 
	ld a,128
	out (&57),a		;ypos 1
	 
	;	  PPPPXYRX 2
	ld a,%00000000
	out (&57),a		;P P P P XM YM R X8/PR
		;P = 4-bit Palette Offset
		;XM = 1 to mirror the sprite image horizontally
		;YM = 1 to mirror the sprite image vertically
		;R = 1 to rotate the sprite image 90 degrees clockwise
		;X8 = Ninth bit of the sprite’s X coordinate
		;PR = 1 to indicate P is relative to the anchor’s palette offset (relative sprites only) 
		; VENNNNNN 3
	ld a,%11000000
	out (&57),a		;V E N5 N4 N3 N2 N1 N0
		;V = 1 to make the sprite visible
		;E = 1 to enable attribute byte 4
		;N = Sprite pattern to use 0-63 
		;If E=0, the sprite is fully described by sprite attributes 0-3. The sprite pattern is an 8-bit one identified by pattern N=0-63. The sprite is an anchor and cannot be made relative. The sprite is displayed as if sprite attribute 4 is zero. 
		;If E=1, the sprite is further described by sprite attribute 4. 

		; HNTXXYYy 4
	ld a,%10000000
	out (&57),a	;H N6 T X X Y Y Y8
		;H = 1 if the sprite pattern is 4-bit
		;N6 = 7th pattern bit if the sprite pattern is 4-bit
		;T = 0 if relative sprites are composite type else 1 for unified type
		;XX = Magnification in the X direction (00 = 1x, 01 = 2x, 10 = 4x, 11 = 8x)
		;YY = Magnification in the Y direction (00 = 1x, 01 = 2x, 10 = 4x, 11 = 8x)
		;Y8 = Ninth bit of the sprite’s Y coordinate 
		;{H,N6} must not equal {0,1} as this combination is used to indicate a relative sprite. 

	
	nextreg $34,3
	nextreg $75,96
	nextreg $76,96
	nextreg $77,0
	nextreg $78,%11000000
	;nextreg $39,%10000000
	
	
	nextreg &43,%00110000
		;Load ULA 16 color Palette
    nextreg &40,0           ;palette index 0 (ULA Fore)
	ld b,16					;Entries
	ld hl,MyPalette			;Source Definition
	call DefinePalettes	
	
	;Tilemap

		; 76543210
	ld a,%10100000	;TileMap on
	NextRegA &6B
	
	
	; --HHHHHH
	ld a,&40		;Baseaddr	-Tilemap Base Address
	NextRegA &6E
	
	ld a,&50  ;baseaddr	- Tile Definitions Base Address
	NextRegA &6F
	
	
	ld hl,&5020
	ld a,0
	
	ld b,31
Tagain	
	ld (hl),a
	inc a
	;xor 255
	inc hl
	djnz Tagain
	
	ld a,1
	ld hl,&4140
	ld (hl),a
	ld de,&4140+1
	ld bc,80-3
	ldir
	
TileLoop:
	push af
		ld hl,&4000
		inc (hl)
	pop af
	ei
	halt
	ei
	halt
	ei
	halt
	ei
	halt
	inc a
	nextrega &30
	
	jp TileLoop
	
	di
	halt
	
	xor a
	out (&fe),a				;border=0
	
	
	call DoPause
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;			ULA PLUS Recolor Test
	
	
	nextreg &14,%11101110	;Transparency Color
	
	;Load ULA 16 color Palette
    nextreg &40,0           ;palette index 0 (ULA Fore)
	ld b,16					;Entries
	ld hl,MyPalette			;Source Definition
	call DefinePalettes	
	
    nextreg &40,16          ;palette index 16 (ULA Back)
	ld b,16					;Entries
	ld hl,MyPalette			;Source Definition
	call DefinePalettes	
	
	call DoPause
	
	nextreg &1A,32         ;Window - X1
	nextreg &1A,128        ;Window - X2
	nextreg &1A,32         ;Window - Y1
	nextreg &1A,128        ;Window - Y2
	
	ld a,0
ScrollAgain:; According to documentation this should scroll
	ei			; the ULA, but it doesn't seem to work!
	halt
	inc a
	nextregA &32			;Offset X
	nextregA &33			;Offset Y
	jp ScrollAgain
	
DoPause:
	ld b,100
PauseAgain:	
	ei
	halt
	djnz PauseAgain			;Wait a while before we enable the ULA+ Palette
	
	ret	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;		
	;8 bit palette definition

DefinePalettes:				
	ld a,(hl)				;get color (RRRGGGBB)
	inc hl
    nextregA &41           	;Send the colour 
    djnz DefinePalettes    	;Repeat for next color	
	ret
	
MySprite:
	;incbin "c:\4bitTest.raw"
	incbin "C:\8bitTest.RAW"
MySprite_End:

MyPalette:		
	   ;RRRGGGBB
	db %00000000	;0-0  Black
	db %00000001	;0-1  Blue
	db %10000000	;0-2  Red
	db %01100001	;0-3  Magenta
	db %00001001	;0-4  Green
	db %00010011	;0-5  Cyan
	db %01110100	;0-6  Yellow
	db %01101101	;0-7  White
	
	db %00000000	;B-0  Bright Black (!)
	db %00100010	;B-1  Bright Blue
	db %11100000	;B-2  Bright Red
	db %11100011	;B-3  Bright Magenta
	db %00011100	;B-4  Bright Green
	db %00011111	;B-5  Bright Cyan
	db %11110100	;B-6  Bright Yellow
	db %11111111	;B-7  Bright White
	
SampleScreen:					;Spectrum 1bpp Screen Data
	incbin "\ResALL\Sprites\ZXTestScr.scr"	
SampleScreen_End:	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	