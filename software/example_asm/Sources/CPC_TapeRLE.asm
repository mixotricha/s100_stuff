;Use Tape ResALL\5K_Demo\5K_Demo.cdt



;Script for Adding 5k Demo files to a tape... 

;-n   = start New tape
;-s 1 = Speed 2000 Baud (fast)
;-m 0 = Add Block data
;-t 1 = Turbo Loading blocks (default)

;utils\2cdt -n -r "CINEMA.RLE" "\ResALL\5K_Demo\Cinema.SCR.RLE" RelCPC\tape.cdt -t 1 -m 0  -s 1 
;utils\2cdt -r "CINEMA2.RLE" "\ResALL\5K_Demo\Cinema2.SCR.RLE" RelCPC\tape.cdt -t 1 -m 0  -s 1 

;-m 1 = Add Headerless

;utils\2cdt -r "CINEMA3.SCR" "\ResALL\5K_Demo\Cinema3.SCR" RelCPC\tape.cdt -t 1 -m 1 -s 1



;Unrem this if building with vasm
;	include "\SrcALL\VasmBuildCompat.asm"

BuildCPC equ 1		
SimpleBuild equ 1

PrintChar 	equ &BB5A	


	org &4000
	

	;Switch to tape via |TAPE basic command
	rst 3		;Far Call (To Rom)
	dw bartape	;Pointer to BarTape command


	ld bc,&0000	;Black
	call &bc38	;SCR SET BORDER... Flash between B / C
	
	ld a,0		;Color 0
	ld bc,&0000	;Black
	call &bc32	;SCR SET INK... Color A... Flash between B / C

	ld a,1
	ld bc,&0303	;Dk Red
	call &bc32	;SCR SET INK... Color A... Flash between B / C
	ld a,2
	ld bc,&0606	;Lt Red
	call &bc32	;SCR SET INK... Color A... Flash between B / C
	ld a,3
	ld bc,&1717	;Cyan
	call &bc32	;SCR SET INK... Color A... Flash between B / C
	
;	call &BB57 ; VDU Disable

	ld a,1		;(0=Messages ... <>0 No Messages)
	call &BC6B 	;CAS NOISY... Enable / Disable Casette Messages

	;ld a,255
	;ld (&BE78),a ;bios_set_message (255=Disable)

	ld a,1		;Screen Mode 1
	call &BC0E	;SCR SET MODE ... Set the Screen Mode A

;Cinema


	;ld HL,FileName	;HL=Filename (legth in B - 10 for this example)
	ld de,&A600	; address of 2k buffer (&800), 
				;Casette loads first 2k to here when reading the file header
	ld b,0		;Length of filename in HL (0=Next File on tape)
	call &BC77	;CAS IN OPEN - Open File on tape carry true if sucess

;If we want to specify a filename, this is the correct format:
;FileName db "CINEMA.RLE" ;(10 byte length)


	ld de,&C000	;Screen Ram
	call RLEDecompress	;Decompress bytes from tape to screen

	call &BC7A	;CAS IN CLOSE (Close Tape File)

;5K Message
	;ld HL,FileName	;HL=Filename (legth in B)
	ld de,&A600	;; address of 2k buffer (&800), Casette loads first 2k to here when reading the file header
	ld b,0		;Length of filename in HL (0=Next File on tape)
	call &BC77	;cas_in_open - Open File on tape... carry=True if sucess

	ld de,&C000	;Screen Ram
	call RLEDecompress	;Decompress bytes from tape to screen

	call &bc7a	;Cas_IN_Close (Close Tape File)



	call &BB18	;Wait for key

	call Fireworks	;Fireworks routines

	
;Thanks Message	
	
	ld a,1		;Screen Mode 1
	call &BC0E	;SCR SET MODE ... Set the Screen Mode A

	ld a,1
	ld bc,&0404	;Dk Red
	call &bc32	;SCR SET INK... Color A... Flash between B / C
	ld a,2
	ld bc,&0E0E	;Lt Red
	call &bc32	;SCR SET INK... Color A... Flash between B / C
	ld a,3
	ld bc,&1A1A	;Cyan
	call &bc32	;SCR SET INK... Color A... Flash between B / C
	


	ld hl,&C000	;Destination 	(Screen Ram)
	ld de,16384	;SIZE 			(1 screen worth)
	ld a,&16	;HEADER Byte 	(&16=default)
	call &BCA1	;CAS READ 		(Read headerless data)

	;NOTE... I had trouble when the first byte in the file was lots of zeros

	call &BB18	;Wait for key
	ret



		
NewLine:
	ld a,13		;Carriage return
	call PrintChar
	ld a,10		;Line Feed 
	jp PrintChar


bartape: 		;For RST3 command
	dw &ccfd	;Address of |TAPE command in ROM
	db &7		;Rom 7 for upper rom

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




RLEDecompress:
	call &BC80	;CAS IN CHAR... Read a byte (A= Result IX= Corrupt)

	or a		;check header byte	
	ret z		;Zero byte=Sequence done - return
	jp m,RLEDecompress_RLE 
			;m=Jump if sign negative (top bit 1)

;Process Uncompressed Data

RLEDecompress_LINEAR:	;Top bit = 0 
	ld b,a		;byte count (1-127)
RLEDecompress_LinearAgain:
	call &BC80	;CAS IN CHAR... Read a byte (A= Result IX= Corrupt)

	or a		;0=Bytes are transparent so we can 
	jr z,RleSkip1	;load the image in multiple layers

	ld (de),a	;Save B bytes to Destination
RleSkip1:
	inc de
	djnz RLEDecompress_LinearAgain	;Repeat until B=0
	jr RLEDecompress ;Get Next Header byte

;Process Compressed Data Data (=1 repeated byte

RLEDecompress_RLE:	;Top bit = 1
	and %01111111	
	ld b,a		;byte count (1-127)
	call &BC80	;CAS IN CHAR... Read a byte (A= Result IX= Corrupt)
RLEDecompress_RLEAgain:

	or a		;0=Bytes are transparent so we can 
	jr z,RleSkip2	;load the image in multiple layers

	ld (de),a	;Fill B times to Destination (RLE) 
RleSkip2:
	inc de
	djnz RLEDecompress_RLEAgain	;Repeat until B=0
	jr RLEDecompress ;Get Next Header byte




;Bonus! Monitor/Memdump

;These are needed only for the Monitor/Memdump


	read "\SrcALL\Multiplatform_Monitor_RomVer.asm"
	read "\SrcALL\Multiplatform_ShowHex.asm"
	read "\SrcALL\Multiplatform_MonitorMemdump.asm"


	read "\sources\CPC_Fireworks.asm"