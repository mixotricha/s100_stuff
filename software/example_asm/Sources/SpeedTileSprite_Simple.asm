NoStartRelocate equ 1 	;For New CPC Header


;Uncomment one of the lines below to select your compilation target

;BuildCPC equ 1	; Build for Amstrad CPC
;BuildMSX equ 1 ; Build for MSX
;BuildTI8 equ 1 ; Build for TI-83+
;BuildZXS equ 1 ; Build for ZX Spectrum
;BuildENT equ 1 ; Build for Enterprise
;BuildSAM equ 1 ; Build for SamCoupe
;BuildSMS equ 1 ; Build for Sega Mastersystem
;BuildSGG equ 1 ; Build for Sega GameGear
;BuildGMB equ 1 ; Build for GameBoy Regular
;BuildGBC equ 1 ; Build for GameBoyColor 
;BuildMSX_MSX1VDP equ 1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef vasm
		include "..\SrcALL\VasmBuildCompat.asm"
	else
		read "..\SrcALL\WinApeBuildCompat.asm"
	endif
	read "..\SrcALL\CPU_Compatability.asm"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;read "..\SrcALL\V1_Header.asm"
	read "..\SrcALL\V1_Header.asm"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			Start Your Program Here
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	
;Tile Settings for Hardware Tilemap Systems

	ifdef BuildSxx			;Hardware sprite system.
	
TileBitmaps equ 0			;Tile offset for bitmap Tile Data
FillData equ (FillDataT-TileData)/32			;Tile offset for 'fill data'
TileByteWidth equ 1
Tsize equ 1					;These are unused on TileSprite systems
Fsize equ 1
Fmarker equ 0
Tmarker equ 0

TranspT equ &FFFF			;Unused Tile Word

TileForeGround equ 4096	;Tile in foreground over sprites on SMS
	endif	

	
;Tile Settings for Bitmap Systems	
	
	ifdef BuildSAM
TileByteWidth equ 4		;4bpp
	endif
	ifdef BuildCPC	
TileByteWidth equ 2		;2bpp
	endif
	ifdef BuildZXS
TileByteWidth equ 1		;1bppp
	endif

	ifndef Tsize
Tsize equ 8*TileByteWidth	;Each Tile uses this many bytes
Fsize equ 4					;Each SolidFill definition uses 4 bytes

Tmarker equ 3				;Add this to a tile to make it Transparent
Fmarker equ 1				;Add this to a tile to make it solidfilled
TranspT equ 0+1				;Set a tile to this to make it totally empty (trasparent)

TileForeGround equ 0		;Unused on bitmap systems
	endif

 
;Tilemap Size settings 		World / Logical / Onscreen	

STilemapWidth equ 16		;Supertilemap size (Game world - in 4x4 blocks)
STilemapHeight equ 16

LTilemapWidth equ 40		;Logical tilemap size (in blocks) (precalculated viewable)
LTilemapHeight equ 32	

VTilemapWidth equ 32		;Visible tilemap size (in blocks) 
	ifdef BuildSxx
VTilemapHeight equ 25
	else
VTilemapHeight equ 24				  		
	endif

VTileMapXoff equ 0		;X offset in BYTES (try 8)
HTileMapYoffH equ &0		;Screen base offset (try 01)
HTileMapYoffL equ &0		;Screen base offset (try &40)



	;4 spare tiles on all sides
	;supertile = 4x4
	
	ifdef BuildSMS
		ld sp, &dff0		;Default stack pointer
		di
		call ScreenInit
	endif
	ifdef BuildCPC
		ld sp,&8000 ;SP usually around &BF00 - this area used by second Screen buffer
		di
		call ScreenInit
	endif
	ifdef BuildZXS
		di
	endif
	ifdef BuildSAM
		di
		ld sp,&0000
		
		ld a,%01101110	;	Bit 0 R/W  BCD 1 of video page control.
		ld bc,252 	;VMPR - Video Memory Page Register (252 dec)
		out (c),a
	
		ld a,%00101110  ;Bank for Screen
		out (250),a		;Page in &0000-7FFF
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Main Loop

	ld a,0				;Xoffset (0-3)
	ld c,0				;Yoffset (0-3)
	ld de,TileBuffer	;Source Logical Tilemap
	call DrawTileMap	;Draw To screen

	di
	halt


;SuperTilemap Vars

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Bitmap data

	align 2	;Bottom 2 bits must be 0
TileBuffer:
	dw TileBitmaps+Tsize,TileBitmaps+Tsize*2,TileBitmaps+Tsize*3,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps+Tsize,TileBitmaps+Tsize,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,FillData+FSize+Fmarker,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TranspT,TranspT,TranspT,TileBitmaps,TileBitmaps
	dw TileBitmaps+Tsize,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps

	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps


;unneeded	
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps
	dw TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps,TileBitmaps

	ifdef BuildZXS

	align 4
TileBitmaps:			;Tile Data
	incbin "\ResALL\Yquest\ZXS_YQuest.RAW"

	align 4
FillData:			;Solid fills
	db %00000000,%00000000	;Tile 0		
	dw 0	;Spacer
	db %10101010,%01010101	;Tile 1
	dw 0	;Spacer

ActiveBank:	db &40
	endif


	ifdef BuildSAM

	align 4
TileBitmaps:			;Tile Data
	incbin "\ResALL\Yquest\MSX2_Yquest.RAW"

	align 4
FillData:			;Solid fills
	db %00000000,%00000000	;Tile 0		
	dw 0	;Spacer
	db %10101010,%01010101	;Tile 1
	dw 0	;Spacer

ActiveBank:	db &00
	endif



	ifdef BuildCPC

	align 4
TileBitmaps:			;Tile Data
	incbin "\ResALL\SpeedTiles\TileTestCPC.RAW"

	align 4
FillData:			;Solid fills
	db %00000000,%00000000	;Tile 0		
	dw 0	;Spacer
	db %10100000,%01010000	;Tile 1
	dw 0	;Spacer

ActiveBank:	db &C0
	endif


	ifdef BuildSMS

TileData:
	incbin "\ResALL\Yquest\SMS_YQuest.RAW"
FillDataT:
	db &FF,&00,&00,&00
	db &FF,&00,&00,&00
	db &FF,&00,&00,&00
	db &FF,&00,&00,&00
	db &FF,&00,&00,&00
	db &FF,&00,&00,&00
	db &FF,&00,&00,&00
	db &FF,&00,&00,&00
	
	db &00,&FF,&00,&00
	db &00,&FF,&00,&00
	db &00,&FF,&00,&00
	db &00,&FF,&00,&00
	db &00,&FF,&00,&00
	db &00,&FF,&00,&00
	db &00,&FF,&00,&00
	db &00,&FF,&00,&00
	
	db &00,&00,&FF,&00
	db &00,&00,&FF,&00
	db &00,&00,&FF,&00
	db &00,&00,&FF,&00
	db &00,&00,&FF,&00
	db &00,&00,&FF,&00
	db &00,&00,&FF,&00
	db &00,&00,&FF,&00
	
	db &00,&00,&00,&FF
	db &00,&00,&00,&FF
	db &00,&00,&00,&FF
	db &00,&00,&00,&FF
	db &00,&00,&00,&FF
	db &00,&00,&00,&FF
	db &00,&00,&00,&FF
	db &00,&00,&00,&FF
	
TileDataEnd:	
	
sprestoreB_Plus2 equ &D442
DeRestore_plus2 equ &D444
TileYUnused_Plus2 equ &D446

ActiveBank: db &38
VisibleBankReg equ &D417
HSpriteNum equ &D418
HSpriteLimit equ 64

XShift equ &D430
YShift equ &D431


	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Speed Tile Modules

;Minimal version - no Transparency, and no shifts
DisableShifts equ 1		;Minimalist Version
DisableTranspTiles equ 1
	
	ifdef BuildSMS
		read "\SrcSMS\SMS_V1_SpeedTile_ScreenDriver.asm"
		read "\SrcSMS\SMS_V1_SpeedTile_NormalTile.asm"
	endif
	ifdef BuildSAM
		read "\SrcSAM\SAM_V1_SpeedTile_NormalTile.asm"
	endif
	
	ifdef BuildCPC
		read "\SrcCPC\CPC_V1_SpeedTile_NormalTile.asm"
		read "\SrcCPC\CPC_V1_SpeedTile_ScreenDriver.asm"
	endif
	
	ifdef BuildZXS
	read "\SrcZX\ZX_V1_SpeedTile_NormalTile.asm"
	endif
	
	
		read "..\SrcALL\V1_Footer.asm"