
;mode4 equ 1
;mode3 equ 1
mode2 equ 1
;mode1 equ 1





	Org &8000		;Code Origin

	di		;The Sam Screen is 24k, and we need to move it in at &0000-&7FFF
			;we must keep interrupts disabled the whole time
		
;Select Screen Mode
		
; MSSBBBBB	- M=Midi io / S=Screen mode / B=video Bank
	ifdef mode4
		ld a,%01101110
mode43 equ 1
linewidth equ 24
	endif
	
	ifdef mode3
		ld a,%01001110
mode43 equ 1
linewidth equ 12
	endif
	
	ifdef mode2
		ld a,%00101110
mode21 equ 1
linewidth equ 6
	endif
	
	ifdef mode1
		ld a,%00001110
mode21 equ 1
linewidth equ 6
	endif
	
;Select the Screen Mode
	out (252),a		;VMPR - Video Memory Page Register (252 dec)

;Page In Vram
	
		; WRrBBBBB W=Write protect Bank (&0000-&3FFF) / r=ram in low area 
	ld a,%00101110  			;/ R=rom in high area / B=low ram Bank
	out (250),a		;LMPR - Low Memory Page Register (250 dec) 
	
	ld sp,&BFFF		;Define a stack pointer
			

;Mode 4321 - Set Pixels
	
		ld bc,&1010			;XY position
		call GetScreenPos	;Calculate ram position
		
		ld ixl,48			;Height of bitmap
		
		ld hl,sprite		;Source bitmap data
drawnextline:
		push de
			ld bc,linewidth		;Width of bitmap in bytes
			ldir
		pop de
		call GetNextLine	;Move down a line
		
		dec ixl				;Repeat for next line
		jr nz,drawnextline
	

	
;Mode 1 - Set Colors
		
	ifdef Mode1
		ld h,%00001011
		ld bc,&1010			;XY position)
		push bc
			call GetColMemPos;Get color address
			ld c,linewidth	;Height
SpriteNextColorLine:	
				
			ld b,6			;Width
			push de
SpriteNextColorByte:	
			ld a,h
			xor %00111000; BLBBBFFF - Fl / Bright / Back / Front
			ld h,a
				ld (de),a	;Color a byte
				inc de
				djnz SpriteNextColorByte
			pop de
			ld a,h
			xor %00111000
			ld h,a
			call GetNextColLine		;Move down a ColorMem line
			dec c
			jr nz,SpriteNextColorLine
		pop bc
	endif
	
;Mode 2 - Set Colors
	
	ifdef Mode2
		ld bc,&1010			;XY position
		call GetScreenPos	;Get Screen Memory pos
		ld a,d 
		or &20				;Color table starts from &4000
		ld d,a
		ld hl,TestPalette		;Sprite Source
		ld ixh,48			;Lines
PalNextLine:
		ld bc,6				;Bytes per line (Width)
		push de
PalNextByte:
			ldir 			;Copy BC bytes from HL to De	
		pop de
		call GetNextLine	;Scr Next Line (Alter HL to move down a line)
		dec ixh
		jr nz,PalNextLine	;Repeat for next line
	endif
	
;Turn the Rom back on	
		; WRrBBBBB W=Write protect Bank (&0000-&3FFF) / r=ram in low area 
	ld a,%00011111  					;/ R=rom in high area / B=low ram Bank
	out (250),a		;Turn on Rom 0 again

;Color Test 		
	ld a,%1001000	;Dark Color 1
	ld bc,&01F8
	;out (c),a
	ld a,%1101000	;Bright Color 1
	ld bc,&09F8
	;out (c),a

	DI		
	halt			;Stop processor so we can see result
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
	ifdef mode43
GetScreenPos:	;return mempos in HL of screen co-ord B,C (X,Y)
		ld d,c	
		xor a	
		rr d		;Effectively Multiply Ypos by 128
		rra 
		or b		;Add Xpos
		ld e,a	
		ret
	
GetNextLine:	;Move DE down 1 line 
		ld a,&80	
		add e		;Add 128 to mem position 
		ld e,a
		ret nc
		inc d		;Add any carry
		ret
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

	ifdef Mode2
GetScreenPos:		;return memory pos in DE of screen co-ord B,C (X,Y)
		ld d,c		;B=Xpos (0-31), C=Ypos (0-23)
		xor a		
		srl d		;32 bytes per line, so shift EL left 5 times, 
		rr a			;and push any overflow into D
		srl d
		rr a
		srl d
		rr a
		or b		;Or in the X co-ordinate
		ld e,a
		ret
		
GetNextLine:
		ld a,e
		add 32				;Add 32 to line DE
		ld e,a
		ret nc
		inc d				;Add any carry to DE
		ret
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

	ifdef Mode1
GetScreenPos:	;return memory pos in DE of screen co-ord B,C (X,Y)
		ld a,c
		and %00111000
		rlca
		rlca
		or b
		ld e,a
		ld a,c
		and %00000111
		ld d,a
		ld a,c
		and %11000000
		rrca
		rrca
		rrca
		or d
		;or  &00			;&0000 screen base
		ld d,a
		ret

GetNextLine:			;Move HL down one line
		inc d
		ld a,d
		and   %00000111		;See if we're over the first 3rd
		ret nz
		ld a,e
		add a,%00100000
		ld e,a
		ret c				;See if we're over the 2'nd 3rd
		ld a,d
		sub   %00001000
		ld d,a
		ret

	; Input  BC= XY (x=bytes - so 32 across)
	; output HL= screen mem pos
GetColMemPos:			;YYYYYyyy 	Color ram is in 8x8 tiles 
		ld a,C							;so low three Y bits are ignored
			and %11000000	;YY------
			rlca
			rlca			;------YY
			add &18 		;1800 =color ram base
			ld d,a
		ld a,C
		and %00111000		;--YYY---
		rlca
		rlca				;YYY-----
		add b				;Add Xpos
		ld e,a
		ret

GetNextColLine:			;Move down a color line
		ld a,e
		add 32				;Add 32 to line DE
		ld e,a
		ret nc
		inc d				;Add any carry to DE
		ret		
	endif

Sprite:
	ifdef mode4
		incbin "\ResALL\Sprites\RawMSX.RAW"
	endif	
	ifdef mode3
		incbin "\ResALL\Sprites\RawMSX_4color.RAW"
	endif	
	ifdef mode21
		incbin "\ResALL\Sprites\RawZX.RAW"
	endif
	
	ifdef mode2
TestPalette:
	ds 48,%00000001
	ds 48,%00001000
	
	db 1,2,3,4,5,6,7,8
	db 1,2,3,4,5,6,7,8
	db 1,2,3,4,5,6,7,8
	db 1,2,3,4,5,6,7,8
	db 1,2,3,4,5,6,7,8
	db 1,2,3,4,5,6,7,8
	
	db 1,2,3,4,5,6,7,8
	db 1,2,3,4,5,6,7,8
	db 1,2,3,4,5,6,7,8
	db 1,2,3,4,5,6,7,8
	db 1,2,3,4,5,6,7,8
	db 1,2,3,4,5,6,7,8
		
	ds 48,%01000001
	ds 48,%01001000
TestPalette_End:	
	endif
	