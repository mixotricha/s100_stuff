
vdpControl equ &BF
vdpData    equ &BE

NextCharX equ &C000
NextCharY equ &C001

	;Unrem this if building with vasm
	include "\SrcALL\VasmBuildCompat.asm"

	org &0000
	jr ProgramStart		;&0000 - RST 0
	ds 6,&C9			;&0002 - RST 0
	ds 8,&C9			;&0008 - RST 1
	ds 8,&C9			;&0010 - RST 2
	ds 8,&C9			;&0018 - RST 3
	ds 8,&C9			;&0020 - RST 4
	ds 8,&C9			;&0028 - RST 5
	ds 8,&C9			;&0030 - RST 6
	ds 8,&C9			;&0038 - RST 7
	ds 38,&C9			;&0066 - NMI
	ds 26,&C9			;&0080
						
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	; effective Start address &0080
ProgramStart:	
  	di      			;Disable interrupts
    im 1    			;Interrupt mode 1
    ld sp, &dff0		;Default stack pointer

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;									Init the screen 												

	ld hl,VdpInitData	;Source of data
    ld b,VdpInitDataEnd-VdpInitData		;Byte count
    ld c,vdpControl		;Destination port
    otir				;Out (c),(hl).. inc HL... dec B, djnz 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;									Define Palette 												
	
    ld hl, &c000	    ; set VRAM write address to CRAM (palette) address 0
		; note &C0-- is a set palette command... it's not a literal memory address 
    call prepareVram

    ld hl,PaletteData	;Source of data
	ifdef BuildSGG
		ld b,16*2 		;Byte count (32 on SGG)
	else
		ld b,16			;Byte count (16 on SMS)
	endif
	ld c,vdpData		;Destination port
	otir				;Out (c),(hl).. inc HL... dec B, djnz  
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;		Define Font by copying 2 bit font (b/w) to all 4 bitplanes of Tile definitions		
	
    ld hl, &4000		;Any value above &4000 wraps round because there IS only 16k
    call prepareVram
    ld hl,BitmapFont             ; Location of tile data
    ld bc,BitmapFontEnd-BitmapFont ;96*8  ; Counter for number of bytes to write
writeToVramx4:
	ld a,(hl)
    out (vdpData),a		;1		Bitplanes
    out (vdpData),a		;2
    out (vdpData),a		;4
    out (vdpData),a		;8
    inc hl
    dec bc
    ld a,c
    or b
    jr nz, writeToVramx4

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;					Initilize the Cursor position												

	ifdef BuildSGG
		ld a,6				;Left 6 lines not visible on SGG
		ld (NextCharX),a
		ld a,3				;Top 3 lines not visible on SGG
		ld (NextCharY),a
	else
		xor a
		ld (NextCharX),a
		ld (NextCharY),a
	endif 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;					Start of the Test Code														
	
	ld hl,Message			;Address of string
	Call PrintString		;Show String to screen
	
	di
	halt

	
PrintString:
	ld a,(hl)				;Print a '255' terminated string 
	cp 255
	ret z
	inc hl
	call PrintChar
	jr PrintString

Message: db 'Hello World 323!',255

NewLine:
	push af
	ifdef BuildSGG
		ld a,6			;SGG - left 6 lines not visible
	else
		xor a			;SMS 
	endif
		ld (NextCharX),a
		ld a,(NextCharY)
		inc a
		ld (NextCharY),a
	pop af
	ret

	

PrintChar:
	push bc
	push hl
		push af
			ld a,(NextCharY)
			ld b,a				;Ypos (32 tiles per line)
			ld a,(NextCharX)
			ld c,a				;Xpos (2 bytes per tile)
			
			xor a				;YYYYYYYY --------
			rr b		
			rra					;-YYYYYYY Y-------
			rr b
			rra					;--YYYYYY YY------
			
			rlc c				;			XXXXX-
			or c				;--YYYYYY YYXXXXX-
			ld c,a
			
			ld hl,&3800			;Base of Tilemap
			add hl,bc
			call prepareVram
		pop af
		
		push af
			sub 32				;No characters below 32 in our font
			out (vdpData),a
			xor a				;Top byte is palette/top bit of tile num
			out (vdpData),a
			
			ld a,(NextCharX)	;Increase Xpos and see if at end of line
			inc a
			ld (NextCharX),a	
			ifdef BuildSGG
				cp 20+6			;SGG 	20x18
			else
				cp 32			;SMS	32x24
			endif
			call nc,NewLine
		pop af
	pop hl
	pop bc
	ret
	
prepareVram:				;Set vdpData to write to memory address HL in vram
	    ld a,l
	    out (vdpControl),a
	    ld a,h
	    or &40				;we set bit 6 to define that we want to Write data...
	    out (vdpControl),a	;As the VDP ram only goes from &0000-&3FFF 
    ret							;this does not cause a problem
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;					VDP Register settings (needed to turn on screen)							
	
VdpInitData:
	db %00000110 ; reg. 0, display and interrupt mode.
	db 128+0
	db %11100001 ; reg. 1, display and interrupt mode.
	db 128+1
	db &ff 		; reg. 2, name table address. &ff = name table at &3800
	db 128+2
	db &ff 		; reg. 3, Name Table Base Address  (no function) &0000
	db 128+3
	db &ff 		; reg. 4, Color Table Base Address (no function) &0000
	db 128+4
	db &ff 		; reg. 5, sprite attribute table. -DCBA98- = bits of address $3f00
	db 128+5
	db &00		;&ff ; reg. 6, sprite tile address. -----D-- = bit 13 of address $2000
	db 128+6
	db &00 		; reg. 7, border color. 			----CCCC = Color
	db 128+7
	db &00 		; reg. 8, horizontal scroll value = 0.
	db 128+8
	db &00 		; reg. 9, vertical scroll value = 0.
	db 128+9
	db &ff 		; reg. 10, raster line interrupt. Turn off line int. requests.
	db 128+10
VdpInitDataEnd:


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;								Basic palette in native format									
	
PaletteData:
	ifdef BuildSGG					;SGG
		   ;GGGGRRRR, ----BBBB
		db %00000000,%00001111;0
		db %11111111,%00000000;1
		db %11110000,%00001111;2
		db %00001111,%00000000;3
		db %00001111,%00001111;4
		db %00001111,%00001111;5
		db %00001111,%00001111;6
		db %00001111,%00001111;7
		db %00001111,%00001111;8
		db %00001111,%00001111;9
		db %00001111,%00001111;10
		db %00001111,%00001111;11
		db %00001111,%00001111;12
		db %00001111,%00001111;13
		db %00001111,%00001111;14
		db %11111111,%00000000;15
	else 							;SMS
		;   --BBGGRR
		db %00110000	;0
		db %00001111	;1
		db %00111100	;2
		db %00000011	;3
		db %00001111	;4
		db %00001111	;5
		db %00001111	;6
		db %00001111	;7
		db %00001111	;8
		db %00001111	;9
		db %00001111	;A
		db %00001111	;B
		db %00001111	;C
		db %00001111	;D
		db %00001111	;E
		db %00001111	;F
	endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;								Font (1bpp / Black & White)										
BitmapFont:
		incbin "\ResALL\Font96.FNT"			;Font bitmap,
BitmapFontEnd:									; this is common to all systems

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Footer													

	
	org &7FF0
	db "TMR SEGA"	;Fixed data (needed by some SGG)
	db 0,0			;Reserved
	db &69,&69		;16 bit Checksum (sum of bytes $0000-$7FEF... Little endian)
					;Only needed for 'Export SMS', not checked by emulator without bios
	db 0,0,0 		;BCD Product Code & Version
	
	ifdef BuildSGG	;Region & Rom size (see below) - only checked by SMS export bios
		db &6C		;GG Export - 32k
	else
		db &4C		;SMS Export - 32k
	endif

;&3- SMS Japan 
;&4- SMS Export 
;&5- GG 	Japan 
;&6- GG 	Export 
;&7- GG 	International 
;&-C 32KB   
;&-F 128KB   
;&-0 256KB   
;&-1 512KB

 