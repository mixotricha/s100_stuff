NextCharX equ &C000
NextCharY equ &C001


	org &0000				;RST 0-7
	
	org &0040				;Interrupt: Vblank
		reti				
	org &0048				;Interrupt:	LCD-Stat
		reti
	org &0050				;Interrupt: Timer
		reti
	org &0058				;Interrupt: Serial
		reti
	org &0060				;Interrupt:Joypad
		reti

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Init routine												
sstart:
	org &0100
	nop			;0100	0103	Entry point (start of program)
	jp	begin

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Header															

	;0104-0133	Nintendo logo (must match rom logo)	
	db &CE,&ED,&66,&66,&CC,&0D,&00,&0B,&03,&73,&00,&83,&00,&0C,&00,&0D
	db &00,&08,&11,&1F,&88,&89,&00,&0E,&DC,&CC,&6E,&E6,&DD,&DD,&D9,&99
	db &BB,&BB,&67,&63,&6E,&0E,&EC,&CC,&DD,&DC,&99,&9F,&BB,&B9,&33,&3E

	db "CHIBIAKUMAS.COM" ;0134-0142	Game Name (UCASE)
	
	ifdef BuildGBC 
		db &80		;0143 		Color gameboy flag (&80 = GB+CGB,&C0 = CGB only)
	else 
		db &00
	endif
	db 0,0       	;0144-0145	Game Manufacturer code
	db 0         	;0146		Super GameBoy flag (&00=normal, &03=SGB)
	db 2	  	  	;0147		Cartridge type (special upgrade hardware) 
						;(0=normal ROM , 1/2=MBC1(max 2MByte ROM and/or 32KByte RAM)
			
						;0 - ROM ONLY                12 - ROM+MBC3+RAM
						;1 - ROM+MBC1                13 - ROM+MBC3+RAM+BATT
						;2 - ROM+MBC1+RAM            19 - ROM+MBC5 (max 8MByte ROM and/or 128KByte RAM)
						;3 - ROM+MBC1+RAM+BATT       1A - ROM+MBC5+RAM
						;5 - ROM+MBC2                1B - ROM+MBC5+RAM+BATT
						;6 - ROM+MBC2+BATTERY        1C - ROM+MBC5+RUMBLE
						;8 - ROM+RAM                 1D - ROM+MBC5+RUMBLE+SRAM
						;9 - ROM+RAM+BATTERY         1E - ROM+MBC5+RUMBLE+SRAM+BATT
						;B - ROM+MMM01               1F - Pocket Camera
						;C - ROM+MMM01+SRAM          FD - Bandai TAMA5
						;D - ROM+MMM01+SRAM+BATT     FE - Hudson HuC-3
						;F - ROM+MBC3+TIMER+BATT     FF - Hudson HuC-1
						;10 - ROM+MBC3+TIMER+RAM+BATT
						;11 - ROM+MBC3
			
	db 2        	;0148		Rom size (0=32k, 1=64k,2=128k etc)
	db 3         	;0149		Cart Ram size (0=none,1=2k 2=8k, 3=32k)
	db 1         	;014A		Destination Code (0=JPN 1=EU/US)
	db &33       	;014B		Old Licensee code (must be &33 for SGB)
	db 0         	;014C		Rom Version Number (usually 0)
	db 0         	;014D		Header Checksum - ‘ones complement' checksum of bytes 0134-014C… (not needed for emulators)
	dw 0         	;014E-014F	Global Checksum – 16 bit sum of all rom bytes (except 014E-014F)… unused by gameboy

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Start of code												
	 
begin:
	nop
	di
	ld	sp, &ffff		; set the stack pointer to highest mem location + 1
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Position tilemap											

	xor a					
	ld hl,&FF42				
	ldi	(hl), a				;FF42: SCY - Tile Scroll Y
	ld	(hl), a				;FF43: SCX - Tile Scroll X
	
	ld (NextCharX),a		;Set cursor to pos 0,0
	ld (NextCharY),a

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Turn off screen												
	
StopLCD_wait:				;Turn off the screen so we can define our patterns
	ld      a,(&FF44)		;Loop until we are in VBlank
	cp      145             ;Is display on scan line 145 yet?
	jr      nz,StopLCD_wait ;no? keep waiting!
	
	ld      hl,&FF40		;LCDC - LCD Control (R/W)
	res     7,(hl)      	;Turn off the screen
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Define bitmap font											
	
	ld	de, BitmapFont		;Source bitmaps
	ld	hl, &8000 			;Dest in Vram
	ld	bc, BitmapFontEnd-BitmapFont	;Bytes of font

Copy2Bitloop	
	ld	a,(de)		;Read in a byte and INC HL
	inc de
	ldi	(hl),a		;Fill Bitplane 1
	ldi	(hl),a		;Fill Bitplane 2
	dec	bc
	ld a,b
	or c
	jr	nz,Copy2Bitloop

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Define palette												
		
	ifdef BuildGBC
		ld c,0*8		;palette no 0 (back)
		call SetGBCPalettes
		
		ld c,7*8		;palette no 7 (used by font)
		call SetGBCPalettes
	else
		ld a,%00011011		;DDCCBBAA .... A=Background 0=Black, 3=White
		ld hl,&FF47
		ldi (hl),a			;FF47 	BGP	BG & Window Palette Data  (R/W)	= &FC
		ldi (hl),a			;FF48  	OBP0	Object Palette 0 Data (R/W)	= &FF
		cpl					;Set sprite Palette 2 to the opposite
		ldi (hl),a			;FF49  	OBP1	Object Palette 1 Data (R/W)	= &FF
	endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Turn on screen												
		
	ld      hl,&FF40		;LCDC - LCD Control (R/W)	EWwBbOoC 
    set     7,(hl)          ;Turn on the screen
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Our program													
			
	ld hl,Message			;Address of string
	Call PrintString		;Show String to screen
	
	di
	halt
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PrintString:
	ld a,(hl)		;Print a '255' terminated string 
	cp 255
	ret z
	inc hl
	call PrintChar
	jr PrintString

Message: db 'Hello World 323!',255

	
PrintChar:
	push hl
	push bc
		push af
			ld a,(NextCharY)
			ld b,a			;YYYYYYYY --------
			ld hl,NextCharX
			ld a,(hl)
			ld c,a			;-------- ---XXXXX
			inc (hl)
			cp 20-1
			call z,NewLine
			xor a
			rr b			;-YYYYYYY Y-------
			rra
			rr b			;--YYYYYY YY------
			rra
			rr b			;---YYYYY YYY-----
			rra
			or c			;---YYYYY YYYXXXXX
			ld c,a
			ld	hl, &9800	;Tilemap base
			add hl,bc	
		pop af
		push af
			sub 32			;no char <32!
			call LCDWait	;Wait for VDP Sync
			ld (hl),a
			ifdef BuildGBC
				ld bc,&FF4F	;VBK - CGB Mode Only - VRAM Bank
				
				ld a,1		;Turn on GBC extras
				ld (bc),a	
				
				ld (hl),7	;Palette 7
				
				xor a		;Turn off GBC extras
				ld (bc),a			
			endif
		pop af
	pop bc
	pop hl
	ret
	
NewLine:
	push hl
		ld hl,NextCharY		;Inc Ypos
		inc (hl)
		ld hl,NextCharX
		ld (hl),0			;Reset Xpos
	pop hl
	ret	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
LCDWait:
	push af
        di
LCDWaitAgain
        ld a,(&FF41)  		;STAT - LCD Status (R/W)
			;-LOVHCMM
        and %00000010		;MM=video mode (0/1 =Vram available)  		
        jr nz,LCDWaitAgain 
    pop af	
	ret
 

SetGBCPalettes:
	ifdef BuildGBC
		ld hl,GBPal		
SetGBCPalettesb:
		ldi a,(hl)  	;GGGRRRRR
		ld e,a
		ldi a,(hl)  	;xBBBBBGG
		ld d,a
		inc a 			;cp 255
		ret z
		push hl
			call lcdwait ;Wait for VDP Sync
			ld hl,&ff68	
			ld (hl),c	;FF68 - BCPS/BGPI - CGB Mode Only - Background Palette Index
			inc hl			
			ld (hl),e	;FF69 - BCPD/BGPD - CGB Mode Only - Background Palette Data
			dec hl		
			inc	c		;Increase palette address
			ld (hl),c	;FF68 - BCPS/BGPI - CGB Mode Only - Background Palette Index
			inc hl		
			ld (hl),d	;FF69 - BCPD/BGPD - CGB Mode Only - Background Palette Data
			inc c		;Increase palette address
		pop hl
		jr SetGBCPalettesb
	endif

;		 	xBBBBBGGGGGRRRRR
GBPal:	dw %0111110000000000	;col 0
		dw %0111111111100000	;col 1
		dw %0000000000011111	;col 2
		dw %0000001111111111	;col 3
		dw %1111111111111111	;End of list
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;					Font (1bpp / Black & White)										
BitmapFont:
		incbin "\ResALL\Font96.FNT"	;Font bitmap,
BitmapFontEnd:						; this is common to all systems


	
