PrintChar 	equ &BB5A

	nolist


BuildCPC equ 1	; Build for Amstrad CPC
;buildCPC_Tap equ 1

BuildBasicDisk equ 1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef vasm
		include "..\SrcALL\VasmBuildCompat.asm"
	else
		read "..\SrcALL\WinApeBuildCompat.asm"
	endif
	read "..\SrcALL\CPU_Compatability.asm"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	read "..\SrcALL\V1_Header.asm"
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	
	call Monitor_MemDump
		db 16
		dw Akuyou_MusicPos
		
	
	ld de,Akuyou_MusicPos
	ld hl,MusicFile
	call DiskDriver_LoadDirect		;Load file from disk
	

	call Monitor_MemDump
		db 16
		dw Akuyou_MusicPos
		
	ld hl,TestFile
	ld de,&8000
	ld bc,&0064
	call DiskDriver_Save			;Save File to disk
	
	ld a,&C9
	ld (&0038),a		;Quick dummy interrupt handler

	call Music_Restart
	call PLY_SFX_Init

	ld d,20
Repeatr:
	
	di
	push iy
	call PLY_Play	;Play music
	pop iy
	ei


	ei	;Wait for an interrupt
	halt
	ei	;Interrupts happen 6x faster on CPC so wait 5 more times
	halt
	ei
	halt
	ei
	halt
	ei
	halt
	ei
	halt


	ld a,40 ;<-- SM ***
SfxTime_plus1:	;We're going to play a sfx beep one time in 40 
	dec a
	jr nz,DontPlaySFX

	ld a,70 		;frequency
	ld l,1 			;Sfx_Note_Plus1
	call PLY_SFX_Play	;Play the sound 

	ld a,40
DontPlaySFX:
	ld (SfxTime_plus1-1),a

	jp Repeatr
WaitChar:
	ret

;We need to align our Music and SFX files to the locations we entered when we exported them from arkostracker, using a different destination
;will not work






MusicFile:
	db "MUSIC   .BIN"
	   ;12345678.123
TestFile:
	db "Test    .Tst"
	   ;12345678.123


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


cas_noisy equ &bc6b
cas_in_open equ &bc77
cas_in_direct equ &bc83
cas_in_close equ &bc7a
txt_set_cursor equ &bb75
bios_set_message equ &c033
cas_out_open equ &bc8c
cas_out_direct equ &bc98
cas_out_close equ &bc8f


DiskDriver_Save:
	push bc
	push de

		call &BB57 ;VDU Disable


		ld a,255
		ld (&BE78),a ;bios_set_message

		ld b,12 ;; B = length of the filename in characters
		ld de,&C000 ; Address of Buffer in DE ... Filename in HL 
		call cas_out_open ;; firmware function to open a file for writing

	pop hl  ;Load address in HL
	pop de	;File length isn BC
	ld bc,&0000;; BC = execution address

	ld a,2 ;; A = file type (2 = binary)

	call cas_out_direct	;; write file
	call cas_out_close 	;; firmware function to close a file opened for writing

	ld bc,&FA7E         ; FLOPPY MOTOR OFF
    out (c),c      
	

	call &BB54 ; VDU enable
	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

DiskDriver_LoadDirect:
DiskDriver_Load:
;LoadDiskFileFromHL	; Load a file from HL memory loc
	push hl	
		push de
			;push hl
		
			push hl
				ld hl,&0A0F		; Move cursor so errors dont wrap I don't hide them
				call txt_set_cursor	; so we can see if a problem happened
				ld a,255
				ld (&BE78),a ;bios_set_message
			pop hl
	ifdef buildCPC_Tap
			ld de,&A600	;; address of 2k buffer, Casette loads first 2k to here when reading the file header
	else
			ld de,&C000	;; address of 2k buffer, this can be any value if the files have a header
	endif
			ld b,12		;12 chars

			call cas_in_open	; carry true if sucess
		pop de

		ld h,d
		ld l,e
		jr nc,DiskError

;		call null ;<-- SM ***
;DiscDestRelocateCall_Plus2
;		ld a,&C0 ;<-- SM ***
;DiskLoadBank_Plus1
;		push bc
;			call BankSwitch_C0	; switch to bank A
;		pop bc
		ifdef buildCPC_Tap
			push bc
			ld a,b
			cp &8
			jr c,TapNotTooBig
			ld bc,&0800			;Load first &800 bytes into correct location
TapNotTooBig:
			ex de,hl
			ld hl,&A600
			ldir
			ex de,hl
			pop bc
		endif

		call cas_in_direct	; carry true if sucess

		jr nc,DiskError
	pop hl
;	call BankSwitch_C0_Reset 	; Restore the previous bank

	call cas_in_close
	scf				;Set Carry flag (no error)
	ret


DiskError:
	or a	;Clear the carry flag (Error)
	pop hl
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



PrintString:
	ld a,(hl)	;Print a '255' terminated string 
	cp 255
	ret z
	inc hl
	call PrintChar
	jr PrintString


NewLine:
	ld a,13		;Carriage return
	call PrintChar
	ld a,10		;Line Feed 
	jp PrintChar






Monitor_Pause equ 1 				;*** Pause after showing debugging info
	read "\SrcALL\Multiplatform_MonitorMemdump.asm"
	read "\SrcALL\Multiplatform_Monitor.asm"
	read "\SrcALL\Multiplatform_ShowHex.asm"
	
;	read "\SrcALL\V1_BankSwapper.asm"

	NewOrg &9000
Akuyou_MusicPos:

	NewOrg &9500
Akuyou_SfxPos:
	incbin "\resALL\SFXTest9500.bin"
	
;This is my modded arkostracker.
	read "\SrcALL\Multiplatform_ArkosTrackerLite.asm"
Bankswapper_Copy:
null:

	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			End of Your program
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	read "..\SrcALL\V1_Footer.asm"