PrintChar2 equ &00A2

	;Unrem this if building with vasm
	include "\SrcALL\VasmBuildCompat.asm"

;For Cartridge	
	org &4000				;Base Cart Address
	db "AB"					;Fixed Header
	dw ProgramStart 		;Pointer to start of program
	db 00,00,00,00,00,00	;Unused

	
	;;;Effectively Code starts at address &400A

;For Disk
	;org &810A
	
	

;Common	
ProgramStart:			;Program Code Starts Here

	call &006F			;INIT32 - initialises the screen to GRAPHIC1 mode (32x24)
	
	ld a,32				;Set Screen width to 32 chars
	ld (&F3B0),a		;LINLEN 
	
Again:	
	push bc
		ld hl,&0101
		call &00C6		;Firmware Call for Locate
						;on MSX top corner is 1,1
						
		call GetMouse	;Read axis changes into HL
	pop bc
	push af				;Mouse Buttons
		ld a,b
		add h			;Add to last X
		ld b,a
		
		ld a,c
		add l			;Add to last Y
		ld c,a
	pop af
	
	call Monitor		;Show Register contents
	
	jp Again
	
Printchar:
	push hl
		call PrintChar2
	pop hl
	ret	
	
NewLine:
	push af
		ld a,13			;Carriage return
		call PrintChar
		ld a,10			;Line Feed
		call PrintChar
	pop af
	ret
	
PrintString:
	ld a,(hl)			;Print a '255' terminated string 
	cp 255
	ret z
	inc hl
	call PrintChar
	jr PrintString

Message: db 'Hello World 323!',255

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
		
GetMouse: 
		call GetMouseAxis
        ld h,a			;Save both nibbles of X
		
        call GetMouseAxis
        ld l,a			;Save both nibbles of Y
				
		ld a,b
		or %11001111	;Set Unused bits of buttons
        ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
		
GetMouseAxis:

		 ;KJ212211			;K=Kana light J= Port 21
	ld d,%11101100			;For Mouse 2
	;ld d,%10010011 		;For Mouse 1 
	
	CALL  GetMouseNibble   	;Get Top Nibble (Bit 8 ON)
	and %00001111
    rlca
    rlca
    rlca
    rlca
    ld c,a					;Save top nibble
	
	ld d,%11001100			;For Mouse 2
	;ld d,%10000011 		;For Mouse 1
    CALL  GetMouseNibble   	;Get Bottom Nibble (Bit 8 OFF)
	ld b,a 					;Bits 56 are Buttons
    and %00001111
    or c					;Or in top nibble
	ret
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GetMouseNibble: 	
		ld a,15			;Write Reg 15 (Port B)
		out (&a0),a
		ld a,d
		out (&a1),a
		
		ld b,10			;Wait for Mouse
MouseWait:  
		djnz MouseWait

        ld a,14			;Read Reg 14 (Port A)
        out (&a0),a	
        in a,(&a2)		;Read from hardware
        ret

		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Bonus! Monitor/Memdump

;These are needed only for the Monitor/Memdump

;BuildMSX equ 1			;Enable this if you don't use my scripts
SimpleBuild equ 1
	read "\SrcALL\Multiplatform_Monitor_RomVer.asm"
	read "\SrcALL\Multiplatform_ShowHex.asm"
	read "\SrcALL\Multiplatform_MonitorMemdump.asm"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	org &C000		;Pad to 32k