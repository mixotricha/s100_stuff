;This example shows a sprite 3 times with three diferent transparencies...

;The first is a basic 'PUT' - this will erase the background

;The second is a 'Byte zero' transparency - as Mode 0 has 2 colors per byte, if BOTH are zero the pixels will be skipped
;this results in a chunky border around the sprite

;The third uses a Lookup Table to calculate pixels for the sprite mask... this allows 'Color Zero' transparency... 
;where if one pixel is Zero but another is not, then only the colord pixel will be drawn, removing the chunky border
;but using 256 bytes for the LookUp Table

	nolist
	org &1200		;Start of our program

	xor a
	call &BC0E		;Set Screen 0

;Build a lookup table for masking sprite bytes
	ld hl,TranspLUT
	ld (hl),%11111111	;Both pixels kept
BuildLutAgain:
	inc l
	jr z,LUTdone		;Done all 256
	ld a,l
	and %01010101		;Right Pixel Mask
	jr nz,BuildLut2
	ld (hl),%01010101	;Mask to keep back Right pixel
	jr BuildLutAgain
BuildLut2
	ld a,l
	and %10101010		;Left Pixel Mask
	jr nz,BuildLutAgain
	ld (hl),%10101010	;Mask to keep back Left pixel
	jr BuildLutAgain
LUTdone:

;Set up  for 'Yarita' sprite

	ld hl,palette
	ld a,1
PaletteAgain;
	ld b,(hl)
	ld c,(hl)
	inc hl
	push af
	push hl
		call &BC32	;A=pen BC=Color
	pop hl
	pop af
	inc a
	cp 10
	jr nz,PaletteAgain

;Basic Example (PSET / No Transparency)

	ld de,&0010		;Xpos (in pixels)
	ld hl,&00A0		;Ypos (in pixels)
	call &BC1D		;Scr Dot Position - Returns address in HL
	call DrawSprite

	ld de,&0014		;Xpos (in pixels)
	ld hl,&00A0		;Ypos (in pixels)
	call &BC1D		;Scr Dot Position - Returns address in HL
	call DrawSprite

	ld de,&0016		;Xpos (in pixels)
	ld hl,&00A0		;Ypos (in pixels)
	call &BC1D		;Scr Dot Position - Returns address in HL
	call DrawSprite

;Byte Zero Transparency Example

	ld de,&0050		;Xpos (in pixels)
	ld hl,&00A0		;Ypos (in pixels)
	call &BC1D		;Scr Dot Position - Returns address in HL
	call DrawSpriteZ

	ld de,&0054		;Xpos (in pixels)
	ld hl,&00A0		;Ypos (in pixels)
	call &BC1D		;Scr Dot Position - Returns address in HL
	call DrawSpriteZ

	ld de,&0056		;Xpos (in pixels)
	ld hl,&00A0		;Ypos (in pixels)
	call &BC1D		;Scr Dot Position - Returns address in HL
	call DrawSpriteZ

;Color Zero Transparency Example (LUT)

	ld de,&0010		;Xpos (in pixels)
	ld hl,&0050		;Ypos (in pixels)
	call &BC1D		;Scr Dot Position - Returns address in HL
	call DrawSpriteT

	ld de,&0014		;Xpos (in pixels)
	ld hl,&0050		;Ypos (in pixels)
	call &BC1D		;Scr Dot Position - Returns address in HL
	call DrawSpriteT

	ld de,&0016		;Xpos (in pixels)
	ld hl,&0050		;Ypos (in pixels)
	call &BC1D		;Scr Dot Position - Returns address in HL
	call DrawSpriteT


;Finished
	ret			;Finished

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Basic Example (No Transparency)

DrawSprite:
	ld de,TestSprite	;Sprite Source
	ld b,64			;Lines (Height)
SpriteNextLine:
	push hl
		ld c,8		;Bytes per line (Width)
SpriteNextByte:
		ld a,(de)	;Source Byte
		ld (hl),a	;Screen Destination
		inc de		;INC Source (Sprite) Address
		inc hl		;INC Dest (Screen) Address

		dec c 		;Repeat for next byte
		jr nz,SpriteNextByte
	pop hl
	call &BC26		;Scr Next Line (Alter HL to move down a line)
	djnz SpriteNextLine	;Repeat for next line
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Byte Zero Transparency Example

DrawSpriteZ:
	ld de,TestSprite	;Sprite Source
	ld b,64			;Lines (Height)
SpriteNextLineZ:
	push hl
		ld c,8		;Bytes per line (Width)
SpriteNextByteZ:
		ld a,(de)	;Source Byte
		or a
		jr z,ZeroByte
		ld (hl),a	;Screen Destination
ZeroByte:
		inc de		;INC Source (Sprite) Address
		inc hl		;INC Dest (Screen) Address

		dec c 		;Repeat for next byte
		jr nz,SpriteNextByteZ
	pop hl
	call &BC26		;Scr Next Line (Alter HL to move down a line)
	djnz SpriteNextLineZ	;Repeat for next line
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Color Zero Transparency Example (LUT)

DrawSpriteT:
	ld de,TestSprite	;Sprite Source
	ld ixh,64			;Lines (Height)
	ld bc,TranspLUT
SpriteNextLineT:
	push hl
		ld ixl,8	;Bytes per line (Width)

SpriteNextByteT:
		ld a,(de)	;Get Sprite Byte
		ld c,a		;Change Low Byte of LUT
		ld a,(bc)	;Get Mask for Sprite Byte
		and (hl)	;Mask current pixel out of back

		or c		;Or in new pixel
		ld (hl),a	;Save to screen
	
		inc de		;INC Source (Sprite) Address
		inc hl		;INC Dest (Screen) Address

		dec ixl 	;Repeat for next byte
		jr nz,SpriteNextByteT
	pop hl
	call &BC26		;Scr Next Line (Alter HL to move down a line)
	dec ixh
	jr nz,SpriteNextLineT	;Repeat for next line
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

TestSprite:
	incbin "\ResALL\Sprites\BitmapTransparentTestCPC.raw"

Palette:
	db 0,8,5,26,13,15,24,16,23,10


	align 256	;force Low byte to zero &xx00 (Eg &1700 or &1800)
TranspLUT: ds 256	;Lookup table for Color transparency (256 bytes)