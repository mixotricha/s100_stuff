
;Uncomment one of the lines below to select your compilation target

BuildCPC equ 1	; Build for Amstrad CPC
;BuildMSX equ 1 ; Build for MSX
;BuildTI8 equ 1 ; Build for TI-83+
;BuildZXS equ 1 ; Build for ZX Spectrum
;BuildENT equ 1 ; Build for Enterprise
;BuildSAM equ 1 ; Build for SamCoupe
;BuildSMS equ 1 ; Build for Sega Mastersystem
;BuildSGG equ 1 ; Build for Sega GameGear
;BuildGMB equ 1 ; Build for GameBoy Regular
;BuildGBC equ 1 ; Build for GameBoyColor 
;BuildMSX_MSX1VDP equ 1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef vasm
		include "..\SrcALL\VasmBuildCompat.asm"
	else
		read "..\SrcALL\WinApeBuildCompat.asm"
	endif
	read "..\SrcALL\CPU_Compatability.asm"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;read "..\SrcALL\V1_Header.asm"
	read "..\SrcALL\V1_Header.asm"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			Start Your Program Here
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	Call DOINIT	; Get ready
		
;	jp Test1632
		
	call CLS
	
	ld hl,&102
	ld a,&10
	call monitor
	
	Call Mul8		;HL = HL * A
	
	call monitor
		
	call newline
	
	ld hl,&1023
	ld a,&81
	
	call monitor		;(HL=HL/A A=Remainder)
	
	Call Div8		;Only Works for A<=128
	
	call monitor
	
	call NewLine
	call NewLine


	ld hl,&1020
	ld a,255
	call monitor
	
	Call Mul32_8		;DE.HL = HL * A

	call monitor

	call NewLine
	call NewLine

	ld a,&10	
	ld de,&1011
	ld hl,&2022
	call monitor
	call Div32_8
	call monitor


	CALL SHUTDOWN ; return to basic or whatever
	ret

Test1632:
	


	ld hl,&1020
	ld bc,&8000
	call monitor
	
	Call Mul32_16		;DE.HL = HL * BC

	call monitor

	call NewLine
	call NewLine

	ld bc,&A000
	ld de,&1011
	ld hl,&2022
	call monitor		;DE.HL=DE.HL / BC (Remainder in BC)
	call Div32_16
	call monitor


	CALL SHUTDOWN ; return to basic or whatever
	ret

		
Message: db 'Hello World 323!',255
Message2: db 'Press A Key:',255
Message3: db 'You Pressed:',255


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			End of Your program
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Monitor_NextMonitorPos_2Byte equ &CFF0

	
	read "..\SrcALL\Multiplatform_Monitor_RomVer.asm"
	read "..\SrcALL\Multiplatform_ShowHex.asm"
	read "..\SrcALL\V1_Functions.asm"
	ifdef BuildSxx
		read "..\SrcALL\V1_VdpMemory.asm"
	endif
	
	read "\srcall\MultiPlatform_MultDiv.asm"	;16 bit Mult/Div
	read "\srcall\MultiPlatform_MultDiv32.asm"	;24/32 bit Mult/Div

	ifndef BitmapFont
BitmapFont:
	ifdef BMP_UppercaseOnlyFont
		incbin "\ResALL\Font64.FNT"			;Font bitmap, this is common to all systems
	else
		incbin "\ResALL\Font96.FNT"			;Font bitmap, this is common to all systems
	endif
BitmapFontEnd:
	endif
	
	read "..\SrcALL\V1_Footer.asm"
