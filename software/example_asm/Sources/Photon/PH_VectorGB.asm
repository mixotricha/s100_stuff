
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Draw Line
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	ifndef NoDrawLine

DrawLine:	;Start=IX,IY... Dest Hl,DE=Yoffset 
	z_ld_b_ixh ;ld b,ixh
	z_ld_c_ixl ;ld c,ixl
	z_ld_bc_to Xpos24+1	;Source X
	or a
	z_sbc_hl_bc ;bc hl,bc			;Calculate Difference X
	;push hl
		z_ex_dehl ;ex de,hl
		z_ld_b_iyh ;ld b,iyh
		z_ld_c_iyl ;ld c,iyl
		z_ld_bc_to Ypos24+1	;Dest Y
		or a 
		z_sbc_hl_bc ;sbc hl,bc			;Calculate Difference Y
		z_ex_dehl ;ex de,hl
	;pop hl
	
DrawLineRelative:	;HL=Xoffset DE=Yoffset 
	ld a,h
	or l
	or d
 	or e
	jr nz,DrawLineRelativeOK
	inc e			;Draw at least 1 dot
	inc l 
DrawLineRelativeOK:

	z_ld_ix XposDir	;XY Flip markers
	z_ld_ix_plusn_n 0,0 ;ld (IX),0
	bit 7,h
	call nz,FlipHL	;Xpos is negative

	z_ld_ix yposDir
	z_ld_ix_plusn_n 0,0 ;ld (IX),0

	bit 7,d
	jr z,NoFlipDE
	z_ex_dehl ;ex de,hl
	call FlipHL		;Ypos is negative
	z_ex_dehl ;ex de,hl
	
NoFlipDE:
	or a
	z_sbc_hl_de ;sbc hl,de
	jr nc,HLBigger	;X length > Ylength
	add hl,de
	push de
		ld a,e
		ld h,l			;HL=L/E
		call Div8ZeroL	;HL=Source	A=Divider (HL=HL/A A=Rem)
	pop bc
	ld de,&0100		;1 Vpixel per draw
	jr DEBigger
	
HLBigger:
	add hl,de		;Fix HL
	push hl
		ld a,l
		ld h,e			;HL=E/L
		call Div8ZeroL	;HL=Source	A=Divider (HL=HL/A A=Rem)
		z_ex_dehl ;ex de,hl
	pop bc
	ld hl,&0100		;1 Hpixel per draw
	
DEBigger:
	ld a,b			;Swap BC - we use DJNZ for lower unit
	ld b,c			;Pixels of movement for larger direction
	ld c,a
	inc c			;Make sure C loop works!
	
	z_ld_iyh_d ;ld iyh,d		;Ymove
	z_ld_iyl_e ;ld iyl,e

	z_ex_dehl ;ex de,hl
	z_ld_ixh_d ;ld ixh,d		;Xmove
	z_ld_ixl_e ; ld ixl,e

LineAgain:
	push bc
		z_ld_hl_from Xpos24+1
		ld a,(Ypos24+1)
		ld c,a		;C=Ypos

		ld a,(LineColor)
		ld d,a

		ld a,h		;AB=Xpos
		ld b,l

		call Pset	;X=AB   Y=C   color=D

		z_ld_d_ixh ;ld d,ixh	
		z_ld_e_ixl ;ld e,ixl
		ld hl,XposDir
		call Add24

		z_ld_d_iyh ;ld d,iyh	
		z_ld_e_iyl ;ld e,iyl
		inc hl		;HL= YposDir
		call Add24
	pop bc
	z_djnz LineAgain
	dec c
	jr nz,LineAgain
	ret
	
	endif
	
DrawLineSafe: ;Start=IX,IY... Dest hl,DE=Yoffset 
	z_push_ix ;push ix
	z_push_iy ;push iy
	push hl
	push de
	push af
		call DrawLine
	pop af
	pop de
	pop hl
	z_push_iy ;pop iy 
	z_push_ix ;pop ix
	ret
	
	
	
FlipHL:
	z_inc_ix_plusn 0 ;inc (ix)		;Update Flip Marker
	ld a,h
	cpl				;$0001 -> $FFFF
	ld h,a
	ld a,l
	cpl
	ld l,a
	inc hl
	ret
	
;;;;;;;;;;;;;;;;;;

Add24:	;ADD DE to (HL) 24 bit UHL Little Endian (DLHU)
	ld a,(hl)
	inc hl			;First byte is direction marker (D)
	or a
	jr nz,Sub24
	ld a,(hl)		;(HL)=(HL)+$00 $DD $EE
	add e
	ld (hl),a		;L
	inc hl
	ld a,(hl)
	adc d
	ld (hl),a		;H
	inc hl
	ret nc
	inc (hl)		;U
	ret
Sub24:				;(HL)=(HL)-$00 $DD $EE
	ld a,(hl)
	sub e
	ld (hl),a		;L
	inc hl
	ld a,(hl)		;H
	sbc d
	ld (hl),a
	inc hl			;U
	ret nc
	dec (hl)
	ret
	
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Font Driver
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PrintString:
	ld a,(hl)		;Print a '255' terminated string 
	cp 255
	ret z
	inc hl
	call PrintChar
	jr PrintString	

	
	
PrintCharNum:
		sub '!'	
			ld hl,VNumSet
			jr PrintCharactersB
PrintSpace:
	ld a,' '
PrintChar:			;Print Char A
	push af
	push bc
	push de
	push hl	
		cp ' '
		jr z,PrintCharSpace		;Space
		cp ':'+1
		jr c,PrintCharNum		;Numbers and Symbols

		and %11011111			;Convert Upper-> Lower
		sub 'A'
		ld hl,VCharSet
PrintCharactersB:
		sla a				;2 Bytes per address
		ld c,a
		ld b,0

		add hl,bc			;Get Address of Cpacket for char
		ld a,(hl)
		z_ld_ixl_a ;ld ixl,a
		inc hl
		ld a,(hl)
		z_ld_ixh_a ;ld ixh,a

		Call DrawCpacket	;Draw it
PrintCharSpaceB:
		ld bc,12
		call DoScale
		add hl,bc
		z_ld_hl_to Xpos24+1	;Move to next Char
	pop hl
	pop de
	pop bc
	pop af
	ret
PrintCharSpace:				;Print a space
	z_ld_hl_from Xpos24+1
	jr PrintCharSpaceB		;Move Right 1 char

		
Locate:	;(HL,DE) = (X,Y)
	z_ld_hl_to Xpos24+1		;Postition drawing cursor
	z_ld_de_to Ypos24+1
	xor a
	ld (Xpos24),a
	ld (Ypos24),a
	jp ZeroLow24Byte		;Set Low byte of XY to zero
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Draw Vectrex Packet format
;	$CC,$YY,$XX ... CC 00=Move FF=Line 01=Done
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

DrawPacket:	;Draw HL
	z_ld_de_from Xpos24+1		;Back Up Xpos
	push de
		z_ld_de_from Ypos24+1	;Back Up Ypos
		push de		
VtXAgain:
			ld a,(hl)		;Check Command
			inc hl
			or a			;$00 = Move 
			jp z,VtxMove
			inc a			;$FF = Draw Line
			jp z,VtxLine
			cp 2			;$01 = Line Done
			jp z,VtxDone	;Update Pos & Return
VtxLine:
			ld a,(hl)		;Get Ypos
			z_neg
			ld c,a
			inc hl	
			call SexBC
			ld d,b
			ld e,c
			ld c,(hl)		;Get Xpos
			inc hl	
			call SexBC
			push hl
				ld h,b
				ld l,c
				call DrawLineRelative	;HL=Xoffset DE=Yoffset 
			pop hl
			jr VtXAgain
VtxMove:
			ld a,(hl)			;Ypos
			z_neg
			ld c,a 
			inc hl	
			call SexBC
			z_ex_dehl ;ex de,hl
				z_ld_hl_from Ypos24+1 ;Move Ypos
				add hl,bc
				z_ld_hl_to Ypos24+1
			z_ex_dehl ;ex de,hl
			ld c,(hl)			;Xpos
			inc hl	
			call SexBC
			z_ex_dehl ;ex de,hl
				z_ld_hl_from Xpos24+1 ;Move Xpos
				add hl,bc
				z_ld_hl_to Xpos24+1
			z_ex_dehl ;ex de,hl
			jr VtXAgain

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Draw CPacket format - (7 bit per axis - 2 command bits)
;	%CYYYYYYY,%DXXXXXXX ... %CD %?0=Move %?1=Line %1?=End
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

DrawCpacket:		;Draw Cpacket IX
	z_ld_hl_from Xpos24+1			;Back Up Xpos
	push hl
		z_ld_hl_from Ypos24+1		;Back Up Ypos
		push hl		
			jr DrawCpacketStart
DrawCpacketAgain:
			z_inc_ix ;inc ix				;Move to next CMD
			z_inc_ix ;inc ix	
			
DrawCpacketStart:				;CMD CHECK
			z_bit7_ix_plusn 1 ;bit 7,(ix+1)		;%CD=%?0 = Move Type
			jp z,CpacketMove
CpacketLine:					;%CD=%?1 = Line Type
			z_ld_a_ix ;ld a,(ix+0)			
			z_neg
			ld c,a				;Ypos
			call SexBC7bit
			ld d,b
			ld e,c
			z_ld_c_ix_plusn 1 ;ld c,(ix+1)			;Xpos
			call SexBC7bit
			z_push_ix ;push ix
				ld h,b
				ld l,c
				call DrawLineRelative	;HL=Xoffset DE=Yoffset 
			z_pop_ix ;pop ix
			z_bit7_ix_plusn 0 ;bit 7,(ix)			;%CD=%1? = Drawing Done
			jp z,DrawCpacketAgain
VtxDone:
		pop hl
		z_ld_hl_to Ypos24+1		;Restore Ypos
	pop hl
	z_ld_hl_to Xpos24+1			;Restore Xpos
ZeroLow24Byte:
	xor a
	ld (Xpos24),a		;Low byte of 24 bit XY
	ld (Ypos24),a
	ret

CpacketMove:
	z_ld_a_ix ;ld a,(ix+0)			;Get Ypos
	z_neg
	ld c,a 
	call SexBC7bit
	z_ld_hl_from Ypos24+1
	add hl,bc			;Move Ypos
	z_ld_hl_to Ypos24+1
	
	z_ld_c_ix_plusn 1 ;ld c,(ix+1)			;Get Xpos
	call SexBC7bit
	z_ld_hl_from Xpos24+1
	add hl,bc			;Move Xpos
	z_ld_hl_to Xpos24+1
	
	jp DrawCpacketAgain	;Back to start of Cpacket processor

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
SexBC7bit:			;Sign Extend 7bit C-> BC
	res 7,c
	bit 6,c
	jr z,SexBC
	set 7,c
SexBC:				;Sign Extend 8bit C-> BC
	ld b,255
	bit 7,c
	jr nz,DoScale
	inc b			;B=0

DoScale:			;Scale BC 
	ld a,(Scale)
DoScaleAgain:
	or a
	ret z			;0= full size
	cp 128
	jr nc,ScaleNegative
ScalePositive:		;>0 x2 x4 x8
	sla c			
	rl b
	dec a
	jr DoScaleAgain
ScaleNegative:		;<0 /2 /4 /8
	sra b
	rr c
	inc a
	jr DoScaleAgain

	
	
	