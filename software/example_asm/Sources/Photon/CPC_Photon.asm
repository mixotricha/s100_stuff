;Photon... battle of the Chibi Photonic hunters!

UserRam Equ &8000		;Game Memory (<256 bytes)

ScreenWidth40 equ 1		;Screen Size Settings
ScreenWidth equ 320
ScreenHeight equ 200

Color4 equ 1			;Map of logical to sys colors
Color3 equ 3			;CPC only has 3 colors
Color2 equ 2			;Game can use up to 4
Color1 equ 1

CLS equ &BC14

	nolist
;BuildCPC equ 1	; Build for Amstrad CPC

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef vasm
		include "\SrcALL\VasmBuildCompat.asm"
	else
		read "\SrcALL\WinApeBuildCompat.asm"
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	read "\SrcALL\CPU_Compatability.asm"

	org &100		;Start of our program
	di
	ld a,1
	call &BC0E		;Scr Set Mode

	
;Set up Palette
	xor a
	ld hl,Palette
PaletteAgain:
	push hl
	push af
		ld b,(hl)	;2 Flashing colors
		ld c,(hl)
		call &bc32	;Set Color A=Pal num, BC=Colors
	pop af
	pop hl
	inc hl			;Next Palette Entry
	inc a
	cp 4			;Palette Count
	jr nz,PaletteAgain

	ld bc,&0101			;2 Flashing colors
	call &bc38			;Set Border BC
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	ld hl,UserRam			;Clear Game Ram
	ld de,UserRam+1
	ld bc,256
	ld (hl),0
	ldir
	
	call MainMenu			;Show Main Menu

	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

infloop:					;Main Loop
	ld a,(Tick)
	inc a
	and %00000001
	ld (Tick),a

	ld bc,150				;Slow Down Delay

	ld a,(boost)
	or a
	jr nz,BoostOff			;Boost - no delay 
	ld bc,1					;(compensate for font draw)
BoostOff:

	ld d,0					;Key buffer
PauseBC:
	push bc
		push de
			call &bb24 		;KM Get Joystick... Returns ---FRLDU
		pop de
		or a
		jr z,PauseNoKey
		ld d,a				;Store any pressed joystick buttons
		jr KeysDown
PauseNoKey:
	ld a,0
	ld (KeyTimeout),a		;Released - nuke key, and relese keypress
	ld d,0						
KeysDown:
	pop bc
	dec bc
	ld a,b
	or c
	jr nz,PauseBC


StartDraw:
	ld a,(KeyTimeout)		;Keytimeout 
	or a
	jr nz,JoySkip			;Yes, skip keypresses

	ld a,1
	ld (boost),a			;Boost Off

ProcessKeys:
	ld hl,PlayerDirection
	ld ix,PlayerXacc		;Point IX to player Accelerations 

	bit 2,d					;L
	jr z,JoyNotLeft	
	dec (hl)
	call SetPlayerDirection
	ld a,1
	ld (KeyTimeout),a		;Ignore keypresses
JoyNotLeft:
	bit 3,d					;R
	jr z,JoyNotRight	
	inc (hl)
	call SetPlayerDirection
	ld a,1
	ld (KeyTimeout),a		;Ignore keypresses
JoyNotRight: 
	bit 4,d					;Fire
	jr z,JoyNotFire
	ld a,(BoostPower)		;Check if boost power remains
	or a
	jr z,JoyNotFire
	ld a,0
	ld (boost),a			;Turn on Boost
JoyNotFire:
JoySkip:
	call HandlePlayer		;Draw and update player
	
	call HandleCPU			;Draw and update CPU
	
	jp InfLoop

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


Pset:		;X=AB Y=C color=D
	push de
		call GetPixelMask 	;Get HL=Screen RAM Address 
							;D=Background Mask E=Pixel mask
	pop af
	
	call GetColorMaskNum	;Get Masks for Pixel Color
	and e					;Mask pixel color
	ld e,a
	
	ld a,(hl)				;Get Screen Byte
	and d					;Keep background pixels
	or e					;Or in New Pixel
	ld (hl),a				;Save Screen Byte
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GetPixelMask:	;(AB,C) = (X,Y)
;Returns HL=Mempos... 
	;D=Mask to keep background - E= pixel to select
	
	push bc	
		rra		
		rr b
		rra	;4 pixels per byte in mode 1
		rr b	
		call GetScreenPos ;Get Byte Position
	pop af
	ifndef useMode0
		and %00000011 ;4 pixels per byte in Mode 1
	else 
		and %00000001 ;2 pixels per byte in Mode 0
	endif
	ld bc,PixelBitLookup
	add c
	ld c,a 
	ld a,(bc)	;Get Pixel Mask for X position
	ld e,a
	cpl
	ld d,a	
	ret 

	Align 4
PixelBitLookup:	;Pixel positions
	db %10001000,%01000100,%00100010,%00010001
	
ColorLookup:	;Colors (all pixels one color)
	db %00000000,%11110000,%00001111,%11111111

	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GetColorMaskNum:
	and %00000011	;4 pixels per byte in Mode 1
	ld bc,ColorLookup ;Get Color from LUT
	add c
	ld c,a 
	ld a,(bc)
	ret
	


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
PsetHLDE:	;DE=Xpos HL=Ypos A=Color
	push hl
	push bc	
	push de
		push af
			ld a,d	;XH
			ld b,e	;XL
			ld c,l	;YL
		pop de
		call Pset
	pop de
	pop bc
	pop hl
	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Point:	;(AB,C) 
	push bc
		call GetPixelMask ;Get HL=Address E=Background Mask 
		ld a,(hl)				;D=Pixel mask
		and e			  ;Mask pixel color
	pop bc
	call ByteToColorMask  ;Fill all pixels with same color

	ld hl,ColorLookup	  ;Look up color 
	ld b,0
PointAgain:
	cp (hl)				 ;Find the color in the Lookup
	jr z,PointDone
	inc hl
	inc b
	jr PointAgain
PointDone:
	ld a,b	
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Fill whole byte with color - eg %00000001 to %00001111

ByteToColorMask:
		 ;A=Color pixel... B=pixel pos (Xpos)
	ld c,a
	ld a,b
	and %00000011	;4 pixels per byte in Mode 1
	jr z,ByteToColorMaskNoShift ;Zero pos=no shift
	ld b,a
ByteToColorMaskLeftShift:
	sla c			;Shift C B times (color to far right
	djnz ByteToColorMaskLeftShift
ByteToColorMaskNoShift:
	ld a,c
	ifndef useMode0
		rrca		;Fill all the pixels with the same color
		or c
		rrca
		or c
	endif
	rrca
	or c
	ret			;All pixels in A now the same color

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;	; Input  BC= XY (x=bytes - so 80 across)
;	; output HL= screen mem pos
;	; de is wiped
;

;Looking at Y line (in C) - we need to take each set of bits, and work with them separately:
;YYYYYYYY	Y line number (0-200)
;-----YYY	(0-7)  - this part needs to be multiplied by &0800 and added to the total
;YYYYY---	(0-31) - This part needs to be multiplied by 80 (&50)
		;screen is 80 bytes wide = -------- %01010000
GetScreenPos:	;return memory pos in HL 
					;of screen co-ord B,C (X,Y)
	ld a,C		; 00000000 01010000
	and %11111000;-------- -YYYY---

	ld l,a
	xor a		; 00000000 01010000
	sla l		; -------- YYYY----
	rla
	ld h,a
	ld e,l		;value is in first bit position -add it
				; 00000000 01010000
	sla e		; -------Y YYY-----
	rla			;	    
	sla e		; ------YY YY------
	rla
	ld d,a

	add hl,de	;value is in 2nd bit position - add it
	;We've now effectively multiplied by 80 (&50)

				;-----YYY	(0-7) 
	ld a,C		;this part needs to be multiplied by 
	and %00000111	;&0800 and added to the total
	
	rlca		;X8
	rlca
	rlca
	or &C0		;Add the screen Base &C000
	ld d,a		;Load into top byte, and add as 16 bit

	ld e,b		;Add the X pos 
	add hl,de
	ret 		;return memory location in hl
	


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


WaitForFire:
	call DoRandom		;Reseed Random Numbers
	call &bb24 			; KM Get Joystick... Returns ---FRLDU
	and %00010000
	jr nz,WaitForFire

WaitForFireB:
	call DoRandom		;Reseed Random Numbers
	call &bb24 			; KM Get Joystick... Returns ---FRLDU
	and %00010000
	jr z,WaitForFireB
	ret
		

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Palette:
	db 0,23,8,18			;Palette in native format

	read "\SrcAll\MultiPlatform_MultDiv.asm"
	read "\ResALL\Vector\VectorFont.asm"
	read "\Sources\Photon\PH_Title.asm"

	read "\SrcALL\MultiPlatform_ShowDecimal.asm"
	read "PH_Vector.asm"
	read "PH_Multiplatform.asm"
	read "PH_DataDefs.asm"
	read "PH_RamDefs.asm"
