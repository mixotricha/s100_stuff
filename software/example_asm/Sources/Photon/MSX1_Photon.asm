;Photon... battle of the Chibi Photonic hunters!

;ReducedFont equ 1
UserRam Equ &C000			;System Memory


Color1 equ &70				;Color palette
Color2 equ &90
Color3 equ &30
Color4 equ &A0

ScreenWidth32 equ 1			;Screen Size
ScreenWidth equ 256
ScreenHeight equ 192

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;For Cartridge	
	org &4000				;Base Cart Address
	db "AB"					;Fixed Header
	dw ProgramStart 		;Pointer to start of program
	db 00,00,00,00,00,00	;Unused
	
	;Effectively Code starts at address &400A
	
VdpOut_Data equ &98			;For Data writes
VdpOut_Control equ &99		;For Reg settings /Selecting Dest addr in VRAM

ProgramStart:			;Program Code Starts Here

;Set up our screen
	ld c,VdpOut_Control
	ld b,VDPScreenInitData_End-VDPScreenInitData
	ld hl,VDPScreenInitData
	otir	

;Fill screen with all 768 tiles
	ld hl,&1800				;Address of Tilemap
	call VDP_SetWriteAddress
	
	ld bc,768				;Number of tiles
	ld d,0					;Tile Number
FillTilesAgain:	
	ld a,d
	out (VdpOut_Data),a
	inc d					;Next Tile
	dec bc
	ld a,b
	or c		
	jr nz,FillTilesAgain	;Repeat until done
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	call MainMenu

infloop:					;Main Loop
	ld a,(Tick)
	inc a
	and %00000001
	ld (Tick),a

	ld bc,150				;Slow Down Delay

	ld a,(boost)			;Check if boost is on
	or a
	jr nz,BoostOff
	ld bc,1		;Boost - no delay (compensate for font draw)
BoostOff:
	ld d,255				;Key buffer
PauseBC:
	push bc
		push de
			call ReadJoystick
		pop de
		cp 0
		jr z,PauseNoKey
		ld d,a				;Store any pressed joystick buttons
		jr KeysDown
PauseNoKey:
	ld a,0
	ld (KeyTimeout),a		;Released - nuke key, and relese keypress
	ld d,255
KeysDown:
	pop bc
	dec bc
	ld a,b
	or c
	jr nz,PauseBC

StartDraw:
	ld a,(KeyTimeout)		;Keytimeout 
	or a
	jr nz,JoySkip			;Yes, skip keypresses

	ld a,1
	ld (boost),a			;Boost Off
ProcessKeys:
	ld hl,PlayerDirection
	ld ix,PlayerXacc		;Point IX to player Accelerations 

	ld a,d		
	cp 7					;L
	jr nz,JoyNotLeft	
	dec (hl)
	call SetPlayerDirection
	ld a,1
	ld (KeyTimeout),a		;Ignore keypresses
JoyNotLeft:
	ld a,d
	cp 3					;R
	jr nz,JoyNotRight	
	inc (hl)
	call SetPlayerDirection
	ld a,1
	ld (KeyTimeout),a		;Ignore keypresses
JoyNotRight: 
	ld a,1					;Select Fire 1
	call &00D8				;Joystick Fire button firmware call
	cp 255
	jr nz,JoyNotFire
	ld a,(BoostPower)		;Check if boost power remains
	or a
	jr z,JoyNotFire
	ld a,0
	ld (boost),a			;Turn on Boost
JoyNotFire:
JoySkip:
	call HandlePlayer		;Draw and update player
	
	call HandleCPU			;Draw and update CPU
	
	jp InfLoop


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ReadJoystick:
	ei
		ld a,1				;Joystick 1 (0=keyboard 1=joy1 2=joy2)
		call &00D5			;GTSTCK - Get Keypress
	ret
	
WaitForFire:
	call DoRandom
	ld a,1					;Select Fire 1
	call &00D8				;Read Trigger
	cp 255
	jr z,WaitForFire

WaitForFireB:
	call DoRandom
	ld a,1					;Select Fire 1
	call &00D8				;Read Trigger
	cp 255
	jr nz,WaitForFireB
	ret
	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Point:			;Test XY pos BC - returns pixel A
	di
	push hl
	push bc
		push de
			ld hl,PixelMask
			ld a,b
			and %00000111	;Get pixel mask for X pixel in byte
			add l
			ld l,a
			ld d,(hl)		;Get Pixel
			push de
				ld h,c		;Ypos Block
				srl h
				srl h
				srl h
				ld a,b		;Xpos block *8
				and %11111000
				ld b,a
				ld a,c		;Ypos Line of block
				and %00000111
				or b
				ld l,a
				call VDP_SetReadAddress
			pop bc
		pop de
		in a,(VdpOut_Data)
		and b				;Mask pixel
		jr z,Point0
		ld a,1				;Color 1
Point0:
	pop bc		
	pop hl
	ei
	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Pset:
	di
	push hl
	push bc
		push de
			ld hl,PixelMask
			ld a,b
			and %00000111	;Get pixel mask for X pixel in byte
			add l
			ld l,a
			ld a,(hl)		;Get Pixel
			ld d,a
			cpl 
			ld e,a			;Get Mask
			push de
				ld h,c		;Ypos Block
				srl h
				srl h
				srl h
				ld a,b		;Xpos block *8
				and %11111000
				ld b,a
				ld a,c		;Ypos Line of block
				and %00000111
				or b
				ld l,a
				call VDP_SetReadAddress
			pop bc
			in a,(VdpOut_Data)
			and c			;Mask background pixels
			ld c,a
			call VDP_SetWriteAddress
		pop de
		ld a,d				;D=color
		or a
		jr z,Pset0
		
		ld a,c
		or b
		out (VdpOut_Data),a	;Write 1 color
		jr PsetDone
Pset0:		
		ld a,c
		out (VdpOut_Data),a	;Write 0 color
PsetDone:		
		ld a,h
		or &20				;2000+ Color info
		ld h,a
		call VDP_SetWriteAddress
		ld a,d
		out (VdpOut_Data),a	;Write color
	pop bc
	pop hl
	ei
	ret
	
	align 4
PixelMask:
	db %10000000,%01000000,%00100000,%00010000,%00001000,%00000100,%00000010,%00000001
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

VDPScreenInitData:
	db %00000010,128+0	;mode register #0		(MODE 2)
	db %01100000,128+1	;mode register #1
	;   CMMMMMMM
	db %11111111,128+3	;colour table - C=Color Address (&2000) M=Mask (New Color per Yline)
	;		 PMM
	db %00000011,128+4	;pattern generator table  P=Pattern Address (&0000) M=Mask (11=3 tables)
	
	db %00110110,128+5	;sprite attribute table (LOW)
	db %00000111,128+6	;sprite pattern generator table
	db %11110000,128+7	;border colour/character colour at text mode
	
VDPScreenInitData_End:	;Patterns at &0000-17FF    Colors at &2000-37FF    Tilemap at &1800-1AFF
						
						
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

OutiDE:
	ld c,VdpOut_Data			;Set C to data Write Addr
	
	outi		;Send a byte from HL to OUT (C)
	dec de
	ld a,d		;Repeat until DE=0
	or e
	jr nz,OutiDE
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
GetVDPScreenPos:	;Move the VDP write pointer to a memory location by XY location
		ld h,c		;B=Xpos (0-31), C=Ypos (0-23)
		xor a
		
		srl h		;32 bytes per line, so shift L left 5 times, and push any overflow into H
		rr a
		srl h
		rr a
		srl h
		rr a
		
		or b		;Or in the X co-ordinate
		ld l,a
		
		ld a,h
		or &18		;Tilemap starts at &1800
		ld h,a
		call VDP_SetWriteAddress
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
VDP_SetReadAddress:					;Set VRAM address next write will occur
	ld a, l
    out (VdpOut_Control), a		;Send L byte
    ld a, h
    out (VdpOut_Control), a		;Send H  byte	
	ret     
	
VDP_SetWriteAddress:					;Set VRAM address next write will occur
	ld a, l
    out (VdpOut_Control), a		;Send L byte
    ld a, h
    or %01000000				;Set WRITE (0=read)
    out (VdpOut_Control), a		;Send H  byte
	
	ret            
	
;Fill an area with consecutively numbered tiles, so we can simulate a bitmap area
;BC = X,Y	HL = W,H	E = Start Tile	
FillAreaWithTiles:	
FillAreaWithTiles_Yagain:
	push bc
		push hl
			call GetVDPScreenPos	;Set Dest Ram pos
		pop hl
		push hl
FillAreaWithTiles_Xagain:
			out (c),e		;Write Tile num
			inc e			;Inc tile
			dec h			;Are we at end of line?
			jr nz,FillAreaWithTiles_Xagain
		pop hl
	pop bc
	inc c					;Move down a line
	dec l					;Are we done?
	jr nz,FillAreaWithTiles_Yagain
	ret
	
cls:
	ld hl,&2000
	call VDP_SetWriteAddress
	ld de,&1800
	ld c,VdpOut_Data
clsA:	
	ld a,&B0
	out (c),a
	dec de
	ld a,d		;Repeat until DE=0
	or e
	jr nz,clsA
	
	ld hl,&0000
	call VDP_SetWriteAddress
	ld de,&1800
	ld c,VdpOut_Data
clsB:	
	ld a,&0
	out (c),a
	dec de
	ld a,d		;Repeat until DE=0
	or e
	jr nz,clsB
	
	ret	

PsetHLDE:	;DE=Xpos HL=Ypos A=Color
	push hl
	push bc	
	push de
		push af
			ld a,d	;XH
			ld b,e	;XL
			ld c,l	;YL
		pop de
		call Pset
	pop de
	pop bc
	pop hl
	ret
	

	include "\SrcAll\MultiPlatform_MultDiv.asm"
	include "\ResALL\Vector\VectorFont.asm"
	include "\Sources\Photon\PH_Title.asm"
	
	include "\SrcALL\MultiPlatform_ShowDecimal.asm"
	include "PH_Vector.asm"
	include "PH_Multiplatform.asm"
	include "PH_DataDefs.asm"
	include "PH_RamDefs.asm"
	
	org &C000			;Make cartridge 32k
 