;Photon... battle of the Chibi Photonic hunters!

UserRam Equ &C000				;System Memory

Color1 equ &70					;Color palette
Color2 equ &90
Color3 equ &30
Color4 equ &A0

ScreenWidth32 equ 1				;Screen Size
ScreenWidth equ 256
ScreenHeight equ 192

vdpOUT_Control equ &BF
vdpOUT_Data    equ &BE

	;Unrem this if building with vasm
	include "\SrcALL\VasmBuildCompat.asm"

	org &0000
	jr ProgramStart		;&0000 - RST 0
	ds 6,&C9			;&0002 - RST 0
	ds 8,&C9			;&0008 - RST 1
	ds 8,&C9			;&0010 - RST 2
	ds 8,&C9			;&0018 - RST 3
	ds 8,&C9			;&0020 - RST 4
	ds 8,&C9			;&0028 - RST 5
	ds 8,&C9			;&0030 - RST 6
	ds 8,&C9			;&0038 - RST 7
	ds 38,&C9			;&0066 - NMI
	ds 26,&C9			;&0080
						
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

	; effective Start address &0080
	
ProgramStart:	
    im 1    			;Interrupt mode 1
    ld sp, &dff0		;Default stack pointer
	
;Set up our screen (3x 256 tiles)
	ld c,VdpOut_Control
	ld b,VDPScreenInitData_End-VDPScreenInitData
	ld hl,VDPScreenInitData
	otir	
	
;Fill screen with all 768 tiles
	ld hl,&1800				;Address of Tilemap
	call VDP_SetWriteAddress
	ld bc,768				;Number of tiles
	ld d,0					;Tile Number
FillTilesAgain:	
	ld a,d
	out (VdpOut_Data),a
	inc d					;Next Tile
	dec bc
	ld a,b
	or c		
	jr nz,FillTilesAgain	;Repeat until done
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ld hl,UserRam			;Clear Game Ram
	ld de,UserRam+1
	ld bc,256
	ld (hl),0
	ldir

	call MainMenu			;Show Main Menu

infloop:					;Main Loop
	ld a,(Tick)
	inc a
	and %00000001
	ld (Tick),a

	ld bc,350				;Slow Down Delay

	ld a,(boost)			;Check if boost is on
	or a
	jr nz,BoostOff
	ld bc,1		;Boost - no delay (compensate for font draw)
BoostOff:

	ld d,%00111111			;Key buffer
PauseBC:
	push bc
		push de
			call ReadJoystick
		pop de
		and %00111111
		cp %00111111		;Keypressed?
		jr z,PauseNoKey
		ld d,a				;Store any pressed joystick buttons
		jr KeysDown
PauseNoKey:
	ld a,0
	ld (KeyTimeout),a		;Released - nuke key, and relese keypress
	ld d,%00111111
KeysDown:
	pop bc
	dec bc
	ld a,b
	or c
	jr nz,PauseBC

StartDraw:
	ld a,(KeyTimeout)		;Keytimeout 
	or a
	jr nz,JoySkip			;Yes, skip keypresses

	ld a,1
	ld (boost),a			;Boost Off

ProcessKeys:
	ld hl,PlayerDirection
	ld ix,PlayerXacc		;Point IX to player Accelerations 
	bit 2,d					;L
	jr nz,JoyNotLeft	
	dec (hl)
	call SetPlayerDirection
	ld a,1
	ld (KeyTimeout),a		;Ignore keypresses
JoyNotLeft:
	bit 3,d					;R
	jr nz,JoyNotRight	
	inc (hl)
	call SetPlayerDirection
	ld a,1
	ld (KeyTimeout),a		;Ignore keypresses
JoyNotRight: 
	bit 4,d					;Fire
	jr nz,JoyNotFire
	ld a,(BoostPower)		;Check if boost power remains
	or a
	jr z,JoyNotFire
	ld a,0
	ld (boost),a			;Turn on Boost
JoyNotFire:
JoySkip:
	call HandlePlayer		;Draw and update player
	
	call HandleCPU			;Draw and update CPU
	
	jp InfLoop
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ReadJoystick:
		in a,(&DC)	;UD are in Player1's port	---FRLDU 0=Down 1=up
	ret
	
WaitForFire:
	call DoRandom
	call ReadJoystick 			; KM Get Joystick... Returns ---FRLDU
	and %00010000
	jr nz,WaitForFire

WaitForFireB:
	call DoRandom
	call ReadJoystick 			; KM Get Joystick... Returns ---FRLDU
	and %00010000
	jr z,WaitForFireB
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	
Pset:
	di
	push hl
	push bc
		push de
			ld hl,PixelMask
			ld a,b
			and %00000111	;Get pixel mask for X pixel in byte
			add l
			ld l,a
			ld a,(hl)		;Get Pixel
			ld d,a
			cpl 
			ld e,a			;Get Mask
			push de
				ld h,c		;Ypos Block
				srl h
				srl h
				srl h	
				
				ld a,b		;Xpos block *8
				and %11111000
				ld b,a
				ld a,c		;Ypos Line of block
				and %00000111
				or b
				ld l,a
				call VDP_SetReadAddress
			pop bc
			in a,(VdpOut_Data)
			and c			;Mask background pixels
			ld c,a
			call VDP_SetWriteAddress
		pop de
		ld a,d				;D=color
		or a
		jr z,Pset0
		
		ld a,c
		or b
		out (VdpOut_Data),a	;Write 1 color
		jr PsetDone
Pset0:		
		ld a,c
		out (VdpOut_Data),a	;Write 0 color
PsetDone:		
		ld a,h
		or &20				;2000+ Color info
		ld h,a
		call VDP_SetWriteAddress
		ld a,d
		out (VdpOut_Data),a	;Write color
	pop bc
	pop hl
	ei
	ret
	

Point:			;Test XY pos BC - returns pixel A
	di
	push hl
	push bc
		push de
			ld hl,PixelMask
			ld a,b
			and %00000111	;Get pixel mask for X pixel in byte
			add l
			ld l,a
			ld d,(hl)		;Get Pixel
			push de
				ld h,c		;Ypos Block
				srl h
				srl h
				srl h
				ld a,b		;Xpos block *8
				and %11111000
				ld b,a
				ld a,c		;Ypos Line of block
				and %00000111
				or b
				ld l,a
				call VDP_SetReadAddress
			pop bc
		pop de
		in a,(VdpOut_Data)
		and b				;Mask pixel
		jr z,Point0
		ld a,1				;Color 1
Point0:
	pop bc		
	pop hl
	ei
	ret	
	
	align 4
PixelMask:
	db %10000000,%01000000,%00100000,%00010000,%00001000,%00000100,%00000010,%00000001
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

VDPScreenInitData:
	db %00000010,128+0	;mode register #0		(MODE 2)
	db %01100000,128+1	;mode register #1
	db %00000110,128+2 ; reg. 2, name table address.
	;   CMMMMMMM
	db %11111111,128+3	;colour table - C=Color Address (&2000) M=Mask (New Color per Yline)
	;		 PMM
	db %00000011,128+4	;pattern generator table  P=Pattern Address (&0000) M=Mask (11=3 tables)
	
	db %00110110,128+5	;sprite attribute table (LOW)
	db %00000111,128+6	;sprite pattern generator table
	db %11110000,128+7	;border colour/character colour at text mode
	db &00 		,128+8 ; reg. 8, horizontal scroll value = 0.
	db &00		,128+9 ; reg. 9, vertical scroll value = 0.
	db &ff 		,128+10; reg. 10, raster line interrupt. Turn off line int. requests.
VDPScreenInitData_End:	;Patterns at &0000-17FF    Colors at &2000-37FF    Tilemap at &1800-1AFF
						
						

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

VDP_SetReadAddress:				;Set VRAM address next write will occur
	ld a, l
    out (VdpOut_Control), a		;Send L byte
    ld a, h
    out (VdpOut_Control), a		;Send H  byte	
	ret     
	
VDP_SetWriteAddress:			;Set VRAM address next write will occur
	ld a, l
    out (VdpOut_Control), a		;Send L byte
    ld a, h
    or %01000000				;Set WRITE (0=read)
    out (VdpOut_Control), a		;Send H  byte
	ret            
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	
cls:
	ld hl,&2000				;Zero Color Data
	call VDP_SetWriteAddress
	ld de,&1800
	ld c,VdpOut_Data
clsA:	
	ld a,&B0
	out (c),a
	dec de
	ld a,d					;Repeat until DE=0
	or e
	jr nz,clsA
	
	ld hl,&0000				;Zero Pixel Data
	call VDP_SetWriteAddress
	ld de,&1800
	ld c,VdpOut_Data
clsB:	
	ld a,&00
	out (c),a
	dec de
	ld a,d					;Repeat until DE=0
	or e
	jr nz,clsB
	ret	

PsetHLDE:	;DE=Xpos HL=Ypos A=Color
	push hl
	push bc	
	push de
		push af
			ld a,d	;XH
			ld b,e	;XL
			ld c,l	;YL
		pop de
		call Pset
	pop de
	pop bc
	pop hl
	ret
	

	read "\SrcAll\MultiPlatform_MultDiv.asm"
	read "\ResALL\Vector\VectorFont.asm"
	read "\Sources\Photon\PH_Title.asm"
	
	
	read "\SrcALL\MultiPlatform_ShowDecimal.asm"
	read "PH_Vector.asm"
	read "PH_Multiplatform.asm"
	read "PH_DataDefs.asm"
	read "PH_RamDefs.asm"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Footer													

	
	org &7FF0
	db "TMR SEGA"	;Fixed data (needed by some SGG)
	db 0,0			;Reserved
	db &69,&69		;16 bit Checksum (sum of bytes $0000-$7FEF... Little endian)
					;Only needed for 'Export SMS', not checked by emulator without bios
	db 0,0,0 		;BCD Product Code & Version
	
	ifdef BuildSGG	;Region & Rom size (see below) - only checked by SMS export bios
		db &6C		;GG Export - 32k
	else
		db &4C		;SMS Export - 32k
	endif


 