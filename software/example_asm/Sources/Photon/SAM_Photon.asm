
userRam equ &E000		;Game Memory (<256 bytes)

ScreenWidth32 equ 1		;Screen Size Settings
ScreenWidth equ 256
ScreenHeight equ 192

Color1 equ 1			;Map of logical to sys colors
Color2 equ 2	
Color3 equ 3
Color4 equ 4

	Org &8000		;Code Origin
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	di	;The Sam Screen is 24k, and we need to move it in at &0000-&7FFF
		;we must keep interrupts disabled the whole time
	
	ld sp,&BFFF		;Define a stack pointer
	
	
		; MSSBBBBB	- M=Midi io / S=Screen mode / B=video Bank
	ld a,%01101110
	out (252),a		;VMPR - Video Memory Page Register (252 dec)
		
		; WRrBBBBB W=Write protect Bank (&0000-&3FFF) / r=ram in low area 
	ld a,%00101110  			;/ R=rom in high area / B=low ram Bank
	out (250),a		;LMPR - Low Memory Page Register (250 dec) 
	
;Define the palette
	ld hl,Palette
	ld b,0
	ld d,16
	ld c,248
NextPalette:
	ld a,(hl)
	inc hl
	out (c),a
	inc b
	dec d
	jr nz,NextPalette
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	ld hl,UserRam			;Clear Game Ram
	ld de,UserRam+1
	ld bc,256
	ld (hl),0
	ldir

	call MainMenu			;Show Main Menu
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

infloop:					;Main Loop
	ld a,(Tick)
	inc a
	and %00000001
	ld (Tick),a

	ld bc,150				;Slow Down Delay

	ld a,(boost)			;Check if boost is on
	or a
	jr nz,BoostOff
	ld bc,1					;Boost - no delay 
							;(compensate for font draw)						
BoostOff:

	ld d,%11111111			;Key buffer
PauseBC:
	push bc
		push de
			call Player_ReadControlsDual	
		pop de
		cp %11111111		;Keypressed?
		jr z,PauseNoKey
		ld d,a				;Store any pressed joystick buttons
		jr KeysDown
PauseNoKey:
	ld a,0
	ld (KeyTimeout),a		;Released - nuke key, and relese keypress
	ld d,%11111111
KeysDown:
	pop bc
	dec bc
	ld a,b
	or c
	jr nz,PauseBC			;Repeat delay loop

StartDraw:
	ld a,(KeyTimeout)		;Keytimeout 
	or a
	jr nz,JoySkip			;Yes, skip keypresses

	ld a,1
	ld (boost),a			;Boost Off

ProcessKeys:
	ld hl,PlayerDirection
	ld ix,PlayerXacc		;Point IX to player Accelerations 
	bit 2,d					;L
	jr nz,JoyNotLeft	
	dec (hl)
	call SetPlayerDirection
	ld a,1
	ld (KeyTimeout),a		;Ignore keypresses
JoyNotLeft:
	bit 3,d					;R
	jr nz,JoyNotRight	
	inc (hl)
	call SetPlayerDirection
	ld a,1
	ld (KeyTimeout),a		;Ignore keypresses
JoyNotRight: 
	bit 4,d					;Fire
	jr nz,JoyNotFire
	ld a,(BoostPower)		;Check if boost power remains
	or a
	jr z,JoyNotFire
	ld a,0
	ld (boost),a			;Turn on Boost
JoyNotFire:
JoySkip:
	call HandlePlayer		;Draw and update player
	
	call HandleCPU			;Draw and update CPU
	
	jp InfLoop

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


WaitForFire:
	call DoRandom		;Reseed Random Numbers
	call Player_ReadControlsDual 			; KM Get Joystick... Returns ---FRLDU
	and %00010000
	jr z,WaitForFire

WaitForFireB:
	call DoRandom		;Reseed Random Numbers
	call Player_ReadControlsDual 			; KM Get Joystick... Returns ---FRLDU
	and %00010000
	jr nz,WaitForFireB
	ret

	;QAOP Space Enter
Player_ReadControlsDual:
	LD  B,%01111111		;%01111111 B, N, M, Sym, Space
	;set upper address lines - note, low bit is zero
	LD  C,&FE  			;port for lines K1 - K5
	
	ld h,b	;Need l to fill top bit 
	
	ld l,3
JoyInAgain:	
	in a,(c)	
	rra		;F2 - Space, F1- Enter, Right - P
	rl h
	rrc b	;%10111111 H, J, K, L  , Enter
			;%11011111 Y, U, I, O  , P
			;%11101111 6, 7, 8, 9  , 0
	dec l
	jr nz,JoyInAgain
	
	rra			;Left - O
	rl h				
	rrc b	;%11110111 5, 4, 3, 2  , 1
	rrc b	;%11111011 T, R, E, W  , Q
	
	in a,(c)	
	rra
	rl l		;U - Q (for later)
	rrc b	;%11111101 G, F, D, S  , A
	
	in a,(c)	
	rra
	rl h		;Down - A
	
	rr l		;Up   - Q
	rl h
	
	ld l,255	;2nd Joystick
	ld a,h
	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PsetHLDE:	;DE=Xpos HL=Ypos A=Color
	push hl
	push bc	
	push de
		push af
			ld a,d	;XH
			ld b,e	;XL
			ld c,l	;YL
		pop de
		call Pset
	pop de
	pop bc
	pop hl
	ret

Pset:		;X=B Y=C color=D
	push de
		call GetPixelMask 	;Get HL=Screen RAM Address 
							;D=Background Mask E=Pixel mask
	pop af
	
	call GetColorMaskNum	;Get Masks for Pixel Color
	and e					;Mask pixel color
	ld e,a
	
	ld a,(hl)				;Get Screen Byte
	and d					;Keep background pixels
	or e					;Or in New Pixel
	ld (hl),a				;Save Screen Byte
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GetPixelMask:	;(B,C) = (X,Y)
;Returns HL=Mempos... 
	;D=Mask to keep background - E= pixel to select
	push bc	
		srl b				;2 pixels per byte
		ld h,c	
		xor a	
		rr h				;Effectively Multiply Ypos by 128
		rra 
		or b				;Add Xpos
		ld l,a				;HL=Screen pos
	pop af
	and %00000001 			;2 pixels per byte
	ld bc,PixelBitLookup
	add c
	ld c,a 
	ld a,(bc)	;Get Pixel Mask for X position
	ld e,a
	cpl						;Convert to mask
	ld d,a	
	ret 

	Align 4
PixelBitLookup:	;Pixel positions
	db %11110000,%00001111
	Align 4
	
ColorLookup:	;Colors (all pixels one color)
	db %00000000,%00010001,%00100010,%00110011
	db %01000100,%01010101,%01100110,%01110111
	db %10001000,%10011001,%10101010,%10111011
	db %11001100,%11011101,%11101110,%11111111
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GetColorMaskNum:
	and %00001111	;Color to Mask
ByteToColorMask: ;Fill whole byte with color read from screen
		 ;A=Color pixel... B=pixel pos (Xpos)
	ld c,a
	rrca
	rrca
	rrca
	rrca
	or c
	ret			;All pixels in A now the same color
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Point:	;(B,C) = (X,Y)
	push bc
		call GetPixelMask ;Get HL=Address E=Background Mask 
		ld a,(hl)				;D=Pixel mask
		and e			  ;Mask pixel color
	pop bc
	call ByteToColorMask  ;Fill all pixels with same color

	ld hl,ColorLookup	  ;Look up color 
	ld b,0
PointAgain:
	cp (hl)				 ;Find the color in the Lookup
	jr z,PointDone
	inc hl
	inc b
	jr PointAgain
PointDone:
	ld a,b	
	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Palette:
	db %00000000; ;0  --GRBLGRB
	db %01011101; ;2  --GRBLGRB
	db %00111011; ;1  --GRBLGRB
	db %01001100; ;3  --GRBLGRB
	db %01101110; ;4  --GRBLGRB
	db %01000001; ;5  --GRBLGRB
	db %01000100; ;6  --GRBLGRB
	db %00101010; ;7  --GRBLGRB
	db %00101111; ;8  --GRBLGRB
	db %01101011; ;9  --GRBLGRB
	db %01101111; ;10  --GRBLGRB
	db %00111000; ;11  --GRBLGRB
	db %00110011; ;12  --GRBLGRB
	db %00011001; ;13  --GRBLGRB
	db %00011100; ;14  --GRBLGRB
	db %01010101; ;15  --GRBLGRB

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

CLS:
	ld hl,&0000		;Zero Screen Ram &0000-&5FFF
	ld de,&0001
	ld bc,256*128
	ld (hl),0
	ldir 
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	include "\SrcAll\MultiPlatform_MultDiv.asm"
	include "\SrcALL\MultiPlatform_ShowDecimal.asm"
	
	include "\ResALL\Vector\VectorFont.asm"				
	
	include "PH_Title.asm"
	include "PH_Vector.asm"							
	include "PH_Multiplatform.asm"
	include "PH_DataDefs.asm"
	include "PH_RamDefs.asm"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
