;Photon... battle of the Chibi Photonic hunters!

Color1 equ 5+64				;Color palette
Color2 equ 3+64				;These are color attributes
Color3 equ 4+64				
Color4 equ 6+64

userRam equ &7000			;Game Memory (<256 bytes)

ScreenWidth32 equ 1			;Screen Size Settings
ScreenWidth equ 256
ScreenHeight equ 192

	Org &8000				;Code Origin
	di

	ld hl,UserRam			;Clear Game Ram
	ld de,UserRam+1
	ld bc,256
	ld (hl),0
	ldir

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	call MainMenu			;Show Main Menu

infloop:					;Main Loop
	ld a,(Tick)
	inc a
	and %00000001
	ld (Tick),a

	ld bc,150				;Slow Down Delay

	ld a,(boost)
	or a
	jr nz,BoostOff
	ld bc,1
BoostOff:

	ld d,%11111111			;Key buffer
PauseBC:
	push bc
		push de
			call Player_ReadControlsDual	
		pop de
		cp %11111111
		jr z,PauseNoKey
		ld d,a				;Store any pressed joystick buttons
		jr KeysDown
PauseNoKey:
	ld a,(KeyTimeout)
	or a
	jr z,KeysDown
	ld a,0
	ld (KeyTimeout),a		;Released - nuke key, and relese keypress
	ld d,%11111111			
KeysDown:
	pop bc
	dec bc
	ld a,b
	or c
	jr nz,PauseBC

StartDraw:
	ld a,(KeyTimeout)		;Keytimeout 
	or a
	jr nz,JoySkip			;Yes, skip keypresses

	ld a,1
	ld (boost),a			;Boost Off

ProcessKeys:
	ld hl,PlayerDirection
	ld ix,PlayerXacc		;Point IX to player Accelerations 
	bit 2,d					;L
	jr nz,JoyNotLeft	
	dec (hl)
	call SetPlayerDirection
	ld a,1
	ld (KeyTimeout),a		;Ignore keypresses
JoyNotLeft:
	bit 3,d					;R
	jr nz,JoyNotRight	
	inc (hl)
	call SetPlayerDirection
	ld a,1
	ld (KeyTimeout),a		;Ignore keypresses
JoyNotRight: 

	bit 4,d					;Fire
	jr nz,JoyNotFire
	ld a,(BoostPower)		;Check if boost power remains
	or a
	jr z,JoyNotFire
	ld a,0
	ld (boost),a			;Turn on Boost
JoyNotFire:
JoySkip:
	call HandlePlayer		;Draw and update player
	
	call HandleCPU			;Draw and update CPU
	
	jp InfLoop

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

WaitForFire:
	call DoRandom		;Reseed Random Numbers
	call Player_ReadControlsDual 			; KM Get Joystick... Returns ---FRLDU
	and %00010000
	jr z,WaitForFire

WaitForFireB:
	call DoRandom		;Reseed Random Numbers
	call Player_ReadControlsDual 			; KM Get Joystick... Returns ---FRLDU
	and %00010000
	jr nz,WaitForFireB
	ret

	;QAOP Space Enter
Player_ReadControlsDual:
	LD  B,%01111111		;%01111111 B, N, M, Sym, Space
	;set upper address lines - note, low bit is zero
	LD  C,&FE  			;port for lines K1 - K5
	
	ld h,b	;Need l to fill top bit 
	
	ld l,3
JoyInAgain:	
	in a,(c)	
	rra		;F2 - Space, F1- Enter, Right - P
	rl h
	rrc b	;%10111111 H, J, K, L  , Enter
			;%11011111 Y, U, I, O  , P
			;%11101111 6, 7, 8, 9  , 0
	dec l
	jr nz,JoyInAgain
	
	rra			;Left - O
	rl h				
	rrc b	;%11110111 5, 4, 3, 2  , 1
	rrc b	;%11111011 T, R, E, W  , Q
	
	in a,(c)	
	rra
	rl l		;U - Q (for later)
	rrc b	;%11111101 G, F, D, S  , A
	
	in a,(c)	
	rra
	rl h		;Down - A
	
	rr l		;Up   - Q
	rl h
	
	ld l,255	;2nd Joystick
	ld a,h
	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PsetHLDE:	;DE=Xpos HL=Ypos A=Color
	push hl
	push bc	
	push de
		push af
			ld a,d	;XH
			ld b,e	;XL
			ld c,l	;YL
		pop de
		call Pset
	pop de
	pop bc
	pop hl
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Pset:		;X=B Y=C color=D
	ld a,d
	or a
	jr z,PsetSkipAttrib		;Dont set attrib for Color 0
;Set Color Attribute
	push bc
		srl b
		srl b
		srl b
		call GetColMemPos	;Get Address of Color Data
		ld (hl),d			;Write color byte
	pop bc

PsetSkipAttrib:	
;Set Bitmap Data	
	push de
		call GetPixelMask ;Get HL=Address E=Background Mask 
								;D=Pixel mask
	pop af
	or a
	jr z,Pset0
	ld a,(hl)	;Colored pixel
	and d		;Keep background pixels
	or e		;Or in New Pixel
	ld (hl),a
	ret
Pset0:			;Black pixel
	ld a,(hl)
	and d		;Keep background pixels
	ld (hl),a
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Point:	;(B,C) = (X,Y)
	push bc
		call GetPixelMask ;Get HL=Address E=Background Mask
		ld a,(hl)			; D=Pixel mask
		and e		;Mask pixel color
	pop bc
	ret z			;Return 0 (Black)
	ld a,1			;Return 1 (Colored)
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GetPixelMask:	;(B,C) = (X,Y)
;Returns HL=Mempos... 
	;D=Mask to keep background - E= pixel to select
	push bc	
		srl b
		srl b	
		srl b
		call GetScreenPos ;Get Byte Position
	pop af
	and %00000111 		  ;8 pixels per byte
	
	ld bc,PixelBitLookup
	add c
	ld c,a 
	ld a,(bc)	;Get Pixel Mask for X position
	ld e,a
	cpl
	ld d,a	
	ret 

	Align 4
PixelBitLookup:	;Pixel positions
	db %10000000,%01000000,%00100000,%00010000
	db %00001000,%00000100,%00000010,%00000001 
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;0 1 0 Y7 Y6 Y2 Y1 Y0   Y5 Y4 Y3 X4 X3 X2 X1 X0

; Input  BC= XY (x=bytes - so 32 across)
; output HL= screen mem pos

GetScreenPos:	;BC=XY pos ... HL = ScreenRam
	ld a,c
	and %00111000		;*32
	rlca
	rlca
	or b
	ld l,a
	ld a,c
	and %00000111		;*256
	ld h,a
	ld a,c
	and %11000000		;*8
	rrca
	rrca
	rrca
	or h
	or  &40				;&4000 screen base
	ld h,a
	ret
	
; Input  BC= XY (x=bytes - so 32 across)
; output HL= screen mem pos
GetColMemPos:			;YYYYYyyy 	Color ram is in 8x8 tiles 
	ld a,C							;so low three Y bits are ignored
		and %11000000	;YY------
		rlca
		rlca			;------YY
		add &58 		;5800 =color ram base
		ld h,a
	ld a,C
	and %00111000		;--YYY---
	rlca
	rlca				;YYY-----
	
	add b				;Add Xpos
	ld l,a
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

CLS:
	xor a
	out (&fe),a				;Black border

	ld hl,&4000				;Zero Pixel Data
	ld de,&4001
	ld bc,&17FF
	ld (hl),0
	ldir 
	ld hl,&5800				;Zero Color Data
	ld de,&5801
	ld bc,&2FF
	ld (hl),%01000111		;Bright white		
	ldir 
	ret

	include "\SrcAll\MultiPlatform_MultDiv.asm"
	include "\SrcALL\MultiPlatform_ShowDecimal.asm"
	
	include "\ResALL\Vector\VectorFont.asm"
	include "\Sources\Photon\PH_Title.asm"
	
	include "PH_Vector.asm"
	include "PH_Multiplatform.asm"
	include "PH_DataDefs.asm"
	include "PH_RamDefs.asm"
