
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Draw Line
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ORG 0x0000

XposDir equ 2		          ;X Line Direction (1 byte)
Xpos24 equ XposDir+1 			;Xpos of current pixel (3 bytes)

YposDir equ Xpos24+3			;Y Line Direction (1 byte)
Ypos24 equ YposDir+1			;Ypos of current pixel (3 bytes)

Scale equ Ypos24+3				;Line Scale (1 byte)
LineColor equ Scale+1 		;Line Color (1 byte)

Pset:
ret

DrawLine:	;Start=IX,IY... Dest Hl,DE=Yoffset
	ld b,ixh
	ld c,ixl
	ld (Xpos24+1),bc	;Source X
	or a
	sbc hl,bc			;Calculate Difference X
	;push hl
		ex de,hl
		ld b,iyh
		ld c,iyl
		ld (Ypos24+1),bc	;Dest Y
		or a
		sbc hl,bc			;Calculate Difference Y
		ex de,hl
	;pop hl

DrawLineRelative:	;HL=Xoffset DE=Yoffset
	ld a,h
	or l
	or d
 	or e
	jr nz,DrawLineRelativeOK
	inc e			;Draw at least 1 dot
	inc l
DrawLineRelativeOK:

	ld ix,XposDir	;XY Flip markers
	ld (IX),0
	bit 7,h
	call nz,FlipHL	;Xpos is negative

	ld ix,YposDir
	ld (IX),0

	bit 7,d
	jr z,NoFlipDE
	ex de,hl
	call FlipHL		;Ypos is negative
	ex de,hl

NoFlipDE:
	or a
	sbc hl,de
	jr nc,HLBigger	;X length > Ylength
	add hl,de
	push de
		ld a,e
		ld h,l			;HL=L/E
		call Div8ZeroL	;HL=Source	A=Divider (HL=HL/A A=Rem)
	pop bc
	ld de,&0100		;1 Vpixel per draw
	jr DEBigger

HLBigger:
	add hl,de		;Fix HL
	push hl
		ld a,l
		ld h,e			;HL=E/L
		call Div8ZeroL	;HL=Source	A=Divider (HL=HL/A A=Rem)
		ex de,hl
	pop bc
	ld hl,&0100		;1 Hpixel per draw

DEBigger:
	ld a,b			;Swap BC - we use DJNZ for lower unit
	ld b,c			;Pixels of movement for larger direction
	ld c,a
	inc c			;Make sure C loop works!

	ld iyh,d		;Ymove
	ld iyl,e

	ex de,hl
	ld ixh,d		;Xmove
	ld ixl,e

LineAgain:
	push bc
		ld hl,(Xpos24+1)
		ld a,(Ypos24+1)
		ld c,a		;C=Ypos

		ld a,(LineColor)
		ld d,a

		ld a,h		;AB=Xpos
		ld b,l

		call Pset	;X=AB   Y=C   color=D

		ld d,ixh
		ld e,ixl
		ld hl,XposDir
		call Add24

		ld d,iyh
		ld e,iyl
		inc hl		;HL= YposDir
		call Add24
	pop bc
	djnz LineAgain
	dec c
	jr nz,LineAgain
	ret

DrawLineSafe: ;Start=IX,IY... Dest hl,DE=Yoffset
	push ix
	push iy
	push hl
	push de
	push af
		call DrawLine
	pop af
	pop de
	pop hl
	pop iy
	pop ix
	ret



FlipHL:
	inc (ix)		;Update Flip Marker
	ld a,h
	cpl				;$0001 -> $FFFF
	ld h,a
	ld a,l
	cpl
	ld l,a
	inc hl
	ret

;;;;;;;;;;;;;;;;;;

Add24:	;ADD DE to (HL) 24 bit UHL Little Endian (DLHU)
	ld a,(hl)
	inc hl			;First byte is direction marker (D)
	or a
	jr nz,Sub24
	ld a,(hl)		;(HL)=(HL)+$00 $DD $EE
	add e
	ld (hl),a		;L
	inc hl
	ld a,(hl)
	adc d
	ld (hl),a		;H
	inc hl
	ret nc
	inc (hl)		;U
	ret
Sub24:				;(HL)=(HL)-$00 $DD $EE
	ld a,(hl)
	sub e
	ld (hl),a		;L
	inc hl
	ld a,(hl)		;H
	sbc d
	ld (hl),a
	inc hl			;U
	ret nc
	dec (hl)
	ret

; ---------------------

mul8ZeroH_Sgn:
;	or a
;	jr z,MultResZero
	bit 7,a
	jr z,mul8ZeroH
	neg
	call mul8ZeroH
NegHL:
	ld a,h
	cpl
	ld h,a
	ld a,l
	cpl
	ld l,a
	inc hl
	ret

;MultResZero:
;	ld hl,0
;	ret
;MultHalf:
;	ld h,l
;	ld l,0
;	srl h
;	rr l
;	ret

mul8ZeroH:
;	cp 127
;	jr z,MultHalf

	ld h,0
Mul8:  	;This routine performs the operation HL=HL*A

	ex de,hl	;Paramiter into De
	ld hl,0     ;HL for result
	ld b,8  	;We're multiplying by 1 byte (8 bits)
Mul8Loop:
	rrca        ;Rotate A right - 1 bit into carry
	jr nc,Mul8Skip;If C=0 we don't need to add hl
	add hl,de

Mul8Skip:
	sla e       ;shift DE 1 bit left (doubling it)
	rl d
	djnz Mul8Loop
	ret

	;Only Works reliably for A<=128... A=C0 will fail because bits may start getting pushed out of A
	;before it is greater than C.

DivInfinite:
	ld hl,&FFFF
	ret

Div8ZeroL:
	ld l,0

Div8:	;HL=Source	A=Divider (HL=HL/A A=Remainder)
	or a
	jr z,DivInfinite ; Division by Zero
	ld c,a
	ld b,16
	xor a

Div8_Again:
	add hl,hl	;Shift Bits in HL left
				;(doubling it it and pushing one bit out)
	rla			;1 bit into A on left
	cp c		;See if A= divider
	jr c,Div8_Skip
	inc l		;If Yes, add 1 to HL
	sub c		;remove C from A

Div8_Skip:
	djnz Div8_Again
    ret

Fraction32:
	push af
		ld a,16
		call Div8
	pop af
	jr Mul8
