;Photon... battle of the Chibi Photonic hunters!



Color1 equ 1				;Color palette
Color2 equ 2				;These are color attributes
Color3 equ 3				
Color4 equ 4

userRam equ &7000			;Game Memory (<256 bytes)

ScreenWidth32 equ 1			;Screen Size Settings
ScreenWidth equ 256
ScreenHeight equ 192

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	
	;The Spectrum NEXT cpu has some extra opcodes for setting it's registers,
	;We'll define a couple of Macros to help us out.
	
	macro nextreg,reg,val
		db &ED,&91,\reg,\val;Macro for Setting A ZX Next Register
	endm
	macro nextregA,reg
		db &ED,&92,\reg		;Macro for Setting A ZX Next Register from A
	endm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
	
	Org &8000				;Code Origin
	di
	
;Palette Init
                ;IPPPSLUN
    nextreg &43,%00010000   ;Layer 2 to 1st palette
	
    nextreg &40,0           ;palette index 0

	ld b,5					;Color Count
	ld hl,MyPalette			;Palette
paletteloop:
	ld a,(hl)				;get color (RRRGGGBB)
	inc hl
    nextregA &41           	;Send the colour 
    
    djnz paletteloop      	;Repeat for next color
	
;Set transparent color to something we don't need!
				;RRRGGGBB
	nextreg &14,%11100001	;Transparency Color

;Enable Layer 2 (256 color screen) and make it writable
	
		 ;BB--P-VW		V=visible  W=Write 
	ld a,%00000011			;(Bank at &0000-&3FFF)  B=bank
	ld bc,&123B
	out (c),a		
		
	;nextreg &07,2			;CPU to 14mhz (0=3.5mjz 1=7mhz)
	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	ld hl,UserRam			;Clear Game Ram
	ld de,UserRam+1
	ld bc,256
	ld (hl),0
	ldir

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	call MainMenu			;Show Main Menu

infloop:					;Main Loop
	ld a,(Tick)
	inc a
	and %00000001
	ld (Tick),a

	ld bc,200				;Slow Down Delay

	ld a,(boost)
	or a
	jr nz,BoostOff
	ld bc,1
BoostOff:

	ld d,%11111111			;Key buffer
PauseBC:
	push bc
		push de
			call Player_ReadControlsDual	
		pop de
		cp %11111111
		jr z,PauseNoKey
		ld d,a				;Store any pressed joystick buttons
		jr KeysDown
PauseNoKey:
	ld a,(KeyTimeout)
	or a
	jr z,KeysDown
	ld a,0
	ld (KeyTimeout),a		;Released - nuke key, and relese keypress
	ld d,%11111111			
KeysDown:
	pop bc
	dec bc
	ld a,b
	or c
	jr nz,PauseBC

StartDraw:
	ld a,(KeyTimeout)		;Keytimeout 
	or a
	jr nz,JoySkip			;Yes, skip keypresses

	ld a,1
	ld (boost),a			;Boost Off

ProcessKeys:
	ld hl,PlayerDirection
	ld ix,PlayerXacc		;Point IX to player Accelerations 
	bit 1,d					;L	---FUDLR
	jr nz,JoyNotLeft	
	dec (hl)
	call SetPlayerDirection
	ld a,1
	ld (KeyTimeout),a		;Ignore keypresses
JoyNotLeft:
	bit 0,d					;R	---FUDLR
	jr nz,JoyNotRight	
	inc (hl)
	call SetPlayerDirection
	ld a,1
	ld (KeyTimeout),a		;Ignore keypresses
JoyNotRight: 

	bit 4,d					;Fire	---FUDLR
	jr nz,JoyNotFire
	ld a,(BoostPower)		;Check if boost power remains
	or a
	jr z,JoyNotFire
	ld a,0
	ld (boost),a			;Turn on Boost
JoyNotFire:
JoySkip:
	call HandlePlayer		;Draw and update player
	
	call HandleCPU			;Draw and update CPU
	
	jp InfLoop

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

WaitForFire:
	call DoRandom		;Reseed Random Numbers
	call Player_ReadControlsDual 			; KM Get Joystick... Returns ---FRLDU
	and %00010000
	jr z,WaitForFire

WaitForFireB:
	call DoRandom		;Reseed Random Numbers
	call Player_ReadControlsDual 			; KM Get Joystick... Returns ---FRLDU
	and %00010000
	jr nz,WaitForFireB
	ret

	;Kempson Joy
Player_ReadControlsDual:
	ld bc,31            ;Kempston joystick port. (---FUDLR)
    in a,(c) 			;read input.
	cpl
	or %11100000		;Fill in the unused bytes, I believe in theory it's possible to have extra fire buttons?
	ld h,a
	ret
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PsetHLDE:	;DE=Xpos HL=Ypos A=Color
	push hl
	push bc	
	push de
		push af
			ld a,d	;XH
			ld b,e	;XL
			ld c,l	;YL
		pop de
		call Pset
	pop de
	pop bc
	pop hl
	ret

Pset:		;X=B Y=C color=D
	call GetScreenPos
	ld (hl),d				;Save Screen Byte
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	
Point:	;(B,C) = (X,Y)
		call GetScreenPos
		ld a,(hl)				;Get Screen Byte
		;ld a,0
	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GetScreenPos:	;return memory pos in HL of screen co-ord B,C (X,Y)
	push af
	push bc
		ld l,b	
		ld a,c
		and %00111111	;Offset in third of bank
		ld h,a
		ld a,c
		and %11000000	;Bank in correct third for &0000-&3fff
		
			;BB--PRVW		-V= visible W=Write R=Read  B=bank
		or  %00000011
		ld bc,&123B
		out (c),a		;Page in and make visible

; *** R Bit is new as of Core 3 allowing writes to ROM area VRAM, 
; But our Emulator doesn't support it - that's what happens when you 
; keep F-ing with the spec after release!
		
		rlca			;Shift XX------
		rlca			
		rlca			;Shift -----XX-
		and %00000110
		add 16			;Bank 16 is first Layer 2 bank
		
		nextregA &50	;Page in B   to &0000-&1FFF range
		inc a			;2x 8k banks	
		nextregA &51	;Page in B+1 to &2000-&3FFF range
	pop bc
	pop af
	ret
		
	
MyPalette:			;8 bits per color
	   ;RRRGGGBB     3 red bits, 3 green bits, 2 blue bit
    db %00000000 ;0
    db %00011111 ;1
    db %11100011 ;2
    db %00011100 ;3
    db %11111100 ;4
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


Cls:	
	ld bc,0			;Bank in first 3rd of screen
	call GetScreenPos	
	call ClsPart
	ld bc,&0040		;Bank in second 3rd of screen
	call GetScreenPos	
	call ClsPart
	ld bc,&0080		;Bank in final 3rd of screen
	call GetScreenPos	
	call ClsPart
	ret
	
ClsPart:
	ld c,&40		;16k block (screen is 48k)
	ld b,0
	ld hl,&0000		;&0000-&3FFF (Rom / Vram write through)
ClsPartb:
	ld (hl),0		;Zero a byte
	inc hl
	djnz  ClsPartb
	dec c
	jr nz,ClsPartb
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


	include "\SrcAll\MultiPlatform_MultDiv.asm"
	include "\SrcALL\MultiPlatform_ShowDecimal.asm"
	
	include "\ResALL\Vector\VectorFont.asm"
	include "\Sources\Photon\PH_Title.asm"
	
	include "PH_Vector.asm"
	include "PH_Multiplatform.asm"
	include "PH_DataDefs.asm"
	include "PH_RamDefs.asm"
