	;Unrem this if building with vasm
	include "\SrcALL\VasmBuildCompat.asm"

	include "\SrcZXN\ZXN_V1_Z80_Extensions.asm"
	include "\SrcZX\ZX_V1_header.asm"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;			LO-Res Spectrum Screen


	nextreg &15,%10000000	;Enable LoRes Layer/Radasjimian Mode (128x96)
	
	ld hl,SampleScreen		;Source 
	ld de,&4000				;Destination VRAM part 1
	ld bc,&1800				;Length
	call SlowLDIR			;ldir
	
	ld de,&6000				;Destination VRAM part 2
	ld bc,&1800				;Length
	call SlowLDIR			;ldir
	
	xor a
	out (&fe),a				;border=0
	
	
	ld hl,Message			;Address of string
	Call PrintString		;Show String to screen
	;call NewLine
	
	ld hl,Message			;Address of string
	Call PrintString		;Show String to screen
	call NewLine
	
	Call Monitor				
	call NewLine
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;			ULA PLUS Recolor Test
	
	
	nextreg &14,%11101110	;Transparency Color
	
	;Load ULA 16 color Palette
    nextreg &40,0           ;palette index 0 (ULA Fore)
	ld b,16					;Entries
	ld hl,MyPalette			;Source Definition
	call DefinePalettes	
	
	call DoPause
	
	nextreg &1A,32         ;Window - X1
	nextreg &1A,128        ;Window - X2
	nextreg &1A,32         ;Window - Y1
	nextreg &1A,128        ;Window - Y2
	
	call DoPause
	
	ld a,0
	
ScrollAgain:	
	ei						;Wait a bit
	halt
	inc a					;Increases A
	
	nextregA &32			;Offset X
	nextregA &33			;Offset Y
	jp ScrollAgain
	
DoPause:
	ld b,100
PauseAgain:	
	ei
	halt
	djnz PauseAgain			;Wait a while before we enable the ULA+ Palette
	
	ret	
SlowLDIR:
	push ix
	pop ix
	push ix
	pop ix
	push ix
	pop ix
	push ix
	pop ix

	ld a,(hl)
	ld (de),a
	inc hl
	inc de
	dec bc
	ld a,b
	or c
	jr nz,SlowLDIR
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;		
	;8 bit palette definition

DefinePalettes:				
	ld a,(hl)				;get color (RRRGGGBB)
	inc hl
    nextregA &41           	;Send the colour 
    djnz DefinePalettes    	;Repeat for next color	
	ret
	

MyPalette:		
       ;RRRGGGBB
    db %11110000 ;0
    db %00000000 ;1
    db %01001001 ;2
    db %10110110 ;3
    db %11111111 ;4
    db %10000010 ;5
    db %00011111 ;6
    db %11100000 ;7
    db %00011101 ;8
    db %11101111 ;9
    db %10010011 ;10
    db %00000011 ;11
    db %00010000 ;12
    db %10111101 ;13
    db %11100010 ;14
    db %11111100 ;15

	
SampleScreen:					;Spectrum 1bpp Screen Data
	incbin "\ResALL\Sprites\NEXT_LORES.RAW"	
SampleScreen_End:	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


	
	

Message: db 'Hello World 543!',255

	
CursorX:	db 0
CursorY:	db 0

GetScreenPos:	;return memory pos in HL of screen co-ord B,C (X,Y)
	ld h,C		;Ypos
	xor a
	srl h		;Shift one bit right (effective Y * 128)
	rra
	add b		;Add Xpos
	ld l,a

	set 6,h		;ScreenBase= &4000
	ld a,c
	cp 48
	ret c
	ld a,&08	;ScreenBase= &6000
	add h
	ld h,a
	ret
	
GetNextLine:	;Move Down a Line
	ld a,l
	add 128		;Add 128 to HL
	ld l,a
	ret nc
	inc h		;Add carry if needed
	ret		

NewLine:
	push hl
		ld hl,CursorX		;Inc X
		ld (hl),0
		inc hl
		inc (hl)			;Reset Y
	pop hl
	ret
	
PrintChar:						;Print A to screen
	push af
	push bc
	push de
	push hl
	push ix
		sub 32					;First Char in our font is 32
		
		ld h,0					;8 bytes per character
		sla a
		rl h
		sla a
		rl h
		sla a
		rl h
		ld l,a

		ld de,BitmapFont		;Add Location of Bitmap font
		add hl,de
		ex de,hl
		
		ld a,(CursorX)			;8 bytes per char X
		rlca
		rlca
		rlca
		ld b,a
		ld a,(CursorY)			;8 Bytes per Char Y
		rlca
		rlca
		rlca
		ld c,a
		call GetScreenPos	;(B,C)->HL memory - also pages in ram to &0000-&3FFF
		
		ld ixl,8
PrintChar_NextLine:
		push hl
			ld b,8
			ld a,(de)			;Read in a byte from the font
			ld c,a
PrintChar_NextPixel:
			xor a
			rl c				;Pop of left most bit
			rl a
			ld (hl),a			;Write pixel to screen
			inc hl	
			djnz PrintChar_NextPixel ;Next pixel
		pop hl
		call GetNextLine
		inc de
		dec ixl
		jr nz,PrintChar_NextLine
		
		ld hl,CursorX			;Increase X
		inc (hl)
		ld a,(hl)				;At end of screen? (col 32)
		cp 16
		call z,NewLine 			;Next line of our font
	pop ix
	pop hl
	pop de
	pop bc
	pop af
	ret	

PrintString:
	ld a,(hl)		;Print a '255' terminated string 
	cp 255
	ret z
	inc hl
	call PrintChar
	jr PrintString


BitmapFont:
	ifdef BMP_UppercaseOnlyFont
		incbin "\ResALL\Font64.FNT"			;Font bitmap, this is common to all systems
	else
		incbin "\ResALL\Font96.FNT"			;Font bitmap, this is common to all systems
	endif
SimpleBuild equ 1
	read "\SrcALL\Multiplatform_Monitor_RomVer.asm"
	read "\SrcALL\Multiplatform_ShowHex.asm"
	read "\SrcALL\Multiplatform_MonitorMemdump.asm"