
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef vasm
		include "..\SrcALL\VasmBuildCompat.asm"
	else
		read "..\SrcALL\WinApeBuildCompat.asm"
	endif
	read "..\SrcALL\CPU_Compatability.asm"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


	read "..\SrcALL\V1_Header.asm"
	read "..\SrcALL\V1_BitmapMemory_Header.asm"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			Start Your Program Here
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	

	Call DOINIT		;Get ready

	call ScreenINIT		;Enable the Bitmap Screen

	
	;Fill the Tilemap
	ld hl,&9800
	ld bc,32*32
FillagainB:
	call LCDWait		;Wait for VDP Sync
	ld a,'\'-32
	ld (hl),a
	inc hl
	dec bc
	ld a,b 
	or c
	jr nz,FillagainB
	
	ld hl,Message		;Address of string
	Call PrintString	;Show String to screen
	
	;Fill the Window
	ld hl,&9C00
	ld bc,32*32
Fillagain:
	call LCDWait		;Wait for VDP Sync
	ld a,'.'-32
	ld (hl),a
	inc hl
	dec bc
	ld a,b 
	or c
	jr nz,Fillagain
	
	ld a,0
	ld (NextCharY),a
	ld (NextCharX),a
	
	ld hl,MessageW		;Address of string
	Call PrintStringW	;Show String to Window
	
	
	
	
	;O=Object sprite size 1=8x16 / D=Disable Screen / 
	;W=window at (&9C00/9800) w=Window Enable
	
	ld  hl,&FF40		;LCDC - LCD Control (R/W)	DWwBbOoC  
	set 6,(hl)			;Window at &9C00
	set 5,(hl)			;Turn on Window
	
	ld a,108
	ld (&FF4A),a		;WY - Window Y Position 0-143 (0=Top)
	ld a,7
	ld (&FF4B),a		;WX- Window X Position minus 7 (7=Left)
	

	ld h,128
	ld bc,0
ScrollTest:
	dec b
	ld a,b
	ld (&FF42),a		;SCY – Tile Scroll Y
	ld (&FF43),a		;SCX – Tile Scroll X
	
	ld de,&4000
PauseAgain:
	dec de
	ld a,d
	or e
	jr nz,PauseAgain
	
	dec h
	
	ld a,h
	ld (&FF4A),a		;Y Pos 0-143 (0=Topmost visible pixel)
	ld (&FF4B),a		;X Pos 0-166 (7=Leftmost Visible pixel)
	
	jp ScrollTest



	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PrintStringW:
	ld a,(hl)		;Print a '255' terminated string 
	cp 255
	ret z
	inc hl
	call PrintCharW
	jr PrintStringW

Message: db 'Hello World 323!',255
Messagew: db 'I am the window',255

PrintCharW:
	push hl
	push bc
		push af
			ld a,(NextCharY)
			ld b,a			;YYYYYYYY --------
			ld hl,NextCharX
			ld a,(hl)
			ld c,a			;-------- ---XXXXX
			inc (hl)
			cp 20-1
			call z,NewLine
			xor a
			rr b			;-YYYYYYY Y-------
			rra
			rr b			;--YYYYYY YY------
			rra
			rr b			;---YYYYY YYY-----
			rra
			or c			;---YYYYY YYYXXXXX
			ld c,a
			ld	hl, &9C00	;Tilemap base
			add hl,bc	
		pop af
		push af
			sub 32			;no char <32!
			call LCDWait	;Wait for VDP Sync
			ld (hl),a
			ifdef BuildGBC
				ld bc,&FF4F	;VBK - CGB Mode Only - VRAM Bank
				
				ld a,1		;Turn on GBC extras
				ld (bc),a	
				
				ld (hl),7	;Palette 7
				
				xor a		;Turn off GBC extras
				ld (bc),a			
			endif
		pop af
	pop bc
	pop hl
	ret	

	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;					Font (1bpp / Black & White)										
BitmapFont:
		;incbin "\ResALL\Font96.FNT"	;Font bitmap,
		
		DB &00,&00,&00,&00,&00,&00,&00,&00
        DB &10,&18,&18,&18,&18,&00,&18,&00
        DB &28,&6C,&28,&00,&00,&00,&00,&00
        DB &00,&28,&7C,&28,&7C,&28,&00,&00
        DB &18,&3E,&48,&3C,&12,&7C,&18,&00
        DB &02,&C4,&C8,&10,&20,&46,&86,&00
        DB &10,&28,&28,&72,&94,&8C,&72,&00
        DB &0C,&1C,&30,&00,&00,&00,&00,&00
        DB &18,&18,&30,&30,&30,&18,&18,&00
        DB &18,&18,&0C,&0C,&0C,&18,&18,&00
        DB &08,&49,&2A,&1C,&14,&22,&41,&00
        DB &00,&18,&18,&7E,&18,&18,&00,&00
        DB &00,&00,&00,&00,&00,&18,&18,&30
        DB &00,&00,&00,&7E,&7E,&00,&00,&00
        DB &00,&00,&00,&00,&00,&18,&18,&00
        DB &02,&04,&08,&10,&20,&40,&80,&00
        DB &7C,&C6,&D6,&D6,&D6,&C6,&7C,&00
        DB &10,&18,&18,&18,&18,&18,&08,&00
        DB &3C,&7E,&06,&3C,&60,&7E,&3C,&00
        DB &3C,&7E,&06,&1C,&06,&7E,&3C,&00
        DB &18,&3C,&64,&CC,&7C,&0C,&08,&00
        DB &3C,&7E,&60,&7C,&06,&7E,&3E,&00
        DB &3C,&7E,&60,&7C,&66,&66,&3C,&00
        DB &3C,&7E,&06,&0C,&18,&18,&10,&00
        DB &3C,&66,&66,&3C,&66,&66,&3C,&00
        DB &3C,&66,&66,&3E,&06,&7E,&3C,&00
        DB &00,&00,&18,&18,&00,&18,&18,&00
        DB &00,&00,&18,&18,&00,&18,&18,&30
        DB &0C,&1C,&38,&60,&38,&1C,&0C,&00
        DB &00,&00,&7E,&00,&00,&7E,&00,&00
        DB &60,&70,&38,&0C,&38,&70,&60,&00
        DB &3C,&76,&06,&1C,&00,&18,&18,&00
        DB &7C,&CE,&A6,&B6,&C6,&F0,&7C,&00
        DB &18,&3C,&66,&66,&7E,&66,&24,&00
        DB &3C,&66,&66,&7C,&66,&66,&3C,&00
        DB &38,&7C,&C0,&C0,&C0,&7C,&38,&00
        DB &3C,&64,&66,&66,&66,&64,&38,&00
        DB &3C,&7E,&60,&78,&60,&7E,&3C,&00
        DB &38,&7C,&60,&78,&60,&60,&20,&00
        DB &3C,&66,&C0,&C0,&CC,&66,&3C,&00
        DB &24,&66,&66,&7E,&66,&66,&24,&00
        DB &10,&18,&18,&18,&18,&18,&08,&00
        DB &08,&0C,&0C,&0C,&4C,&FC,&78,&00
        DB &24,&66,&6C,&78,&6C,&66,&24,&00
        DB &20,&60,&60,&60,&60,&7E,&3E,&00
        DB &44,&EE,&FE,&D6,&D6,&D6,&44,&00
        DB &44,&E6,&F6,&DE,&CE,&C6,&44,&00
        DB &38,&6C,&C6,&C6,&C6,&6C,&38,&00
        DB &38,&6C,&64,&7C,&60,&60,&20,&00
        DB &38,&6C,&C6,&C6,&CA,&74,&3A,&00
        DB &3C,&66,&66,&7C,&6C,&66,&26,&00
        DB &3C,&7E,&60,&3C,&06,&7E,&3C,&00
        DB &3C,&7E,&18,&18,&18,&18,&08,&00
        DB &24,&66,&66,&66,&66,&66,&3C,&00
        DB &24,&66,&66,&66,&66,&3C,&18,&00
        DB &44,&C6,&D6,&D6,&FE,&EE,&44,&00
        DB &C6,&6C,&38,&38,&6C,&C6,&44,&00
        DB &24,&66,&66,&3C,&18,&18,&08,&00
        DB &7C,&FC,&0C,&18,&30,&7E,&7C,&00
        DB &1C,&30,&30,&30,&30,&30,&1C,&00
        DB &80,&40,&20,&10,&08,&04,&02,&00
        DB &38,&0C,&0C,&0C,&0C,&0C,&38,&00
        DB &18,&18,&18,&18,&7E,&7E,&18,&18
        DB &18,&18,&18,&18,&3C,&3C,&18,&18
        DB &18,&18,&18,&18,&18,&18,&18,&18
        DB &00,&00,&38,&0C,&7C,&CC,&78,&00
        DB &20,&60,&7C,&66,&66,&66,&3C,&00
        DB &00,&00,&3C,&66,&60,&66,&3C,&00
        DB &08,&0C,&7C,&CC,&CC,&CC,&78,&00
        DB &00,&00,&3C,&66,&7E,&60,&3C,&00
        DB &1C,&36,&30,&38,&30,&30,&10,&00
        DB &00,&00,&3C,&66,&66,&3E,&06,&3C
        DB &20,&60,&6C,&76,&66,&66,&24,&00
        DB &18,&00,&18,&18,&18,&18,&08,&00
        DB &06,&00,&04,&06,&06,&26,&66,&3C
        DB &20,&60,&66,&6C,&78,&6C,&26,&00
        DB &10,&18,&18,&18,&18,&18,&08,&00
        DB &00,&00,&6C,&FE,&D6,&D6,&C6,&00
        DB &00,&00,&3C,&66,&66,&66,&24,&00
        DB &00,&00,&3C,&66,&66,&66,&3C,&00
        DB &00,&00,&3C,&66,&66,&7C,&60,&20
        DB &00,&00,&78,&CC,&CC,&7C,&0C,&08
        DB &00,&00,&38,&7C,&60,&60,&20,&00
        DB &00,&00,&3C,&60,&3C,&06,&7C,&00
        DB &10,&30,&3C,&30,&30,&3E,&1C,&00
        DB &00,&00,&24,&66,&66,&66,&3C,&00
        DB &00,&00,&24,&66,&66,&3C,&18,&00
        DB &00,&00,&44,&D6,&D6,&FE,&6C,&00
        DB &00,&00,&C6,&6C,&38,&6C,&C6,&00
        DB &00,&00,&24,&66,&66,&3E,&06,&7C
        DB &00,&00,&7E,&0C,&18,&30,&7E,&00
        DB &08,&08,&08,&08,&56,&55,&57,&74
        DB &18,&04,&08,&1C,&56,&55,&57,&74
        DB &00,&00,&00,&00,&7E,&7E,&FF,&FF
        DB &18,&3C,&18,&18,&18,&18,&7E,&FF
        DB &22,&77,&7F,&7F,&3E,&1C,&08,&00
		
		
BitmapFontEnd:						; this is common to all systems


	
	read "..\SrcALL\V1_VdpMemory.asm"
	read "..\SrcALL\V1_HardwareTileArray.asm"
	read "..\SrcAll\V1_Palette.asm"	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			End of Your program
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	read "..\SrcALL\V1_BitmapMemory.asm"

	read "..\SrcALL\V1_Functions.asm"
	read "..\SrcALL\V1_Footer.asm"
