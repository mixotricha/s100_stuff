	org &1200

;	call TestStackParams	;Test Parameters passed to a function

;	call TestReturnVars	;Test Parameters returned from a function

	;call AllocRam		;We can allocate some ram for temporary use on the stack

;	ld a,2	
;	call CaseTest
;	call RetCaseTest
	

;	call DoRstTest

	call StackMisuse

	

	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

TestStackParams:
;Test params in defined bytes
	call Sub1		;Read params from following bytes
	dw &1122		;Word Param for SUB1
	db &33			;Byte Param for SUB1
	
;Test Params Pushed onto stack	
	ld hl,&FFEE		;Word param for SUB2
	push hl
	ld a,&DD		;Byte param for SUB2
	push af

	xor a			;Clear Test Vals
	ld hl,0
	call Sub2		;Read params from Stack
	ret

Sub1:			;Test params in defined bytes
	ex (sp),ix
	ld l,(ix+0)	;Load Low byte of word
	ld h,(ix+1)	;Load High byte of word
	ld a,(ix+2)	;Load byte

	call Monitor
	
	ld bc,3
	add ix,bc	;Move Return address +3
	ex (sp),ix	;Update return address
	ret

Sub2:				;Test Params Pushed onto stack	
	pop iy
		ld ix,4		;Move SP to before pushed params
		add ix,sp
	
		ld l,(ix-2)	;Load Low byte of word
		ld h,(ix-1)	;Load High byte of word
		ld a,(ix-3)	;Load byte

		call Monitor
		ld sp,ix
	push iy			;Replace return address
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


TestReturnVars:
	call ReturnVars
	pop hl		;Get the 3 params passed 
	pop bc			;from the function
	pop de
	call monitor	;Show The Results
	ret	

ReturnVars:		
	pop hl			;get return addr
		ld de,&1234
		push de		;Put param on stack
		ld de,&2233
		push de		;Put param on stack
		ld de,&7788
		push de		;Put param on stack

	push hl 		;put return address
	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;We can allocate some ram for temporary use on the stack
AllocRam: 
	ld ix,-6	;Allocate 6 bytes of ram on thie stack
	add ix,sp
	ld sp,ix     	;Define new stack after allocated 6 bytes
		ld hl,&1234
		ld de,&AABB
		ld bc,&CCDD
		ld (ix+0),l
		ld (ix+1),h
		ld (ix+2),c
		ld (ix+3),b
		ld (ix+4),e
		ld (ix+5),d
	ld ix,6		;remove 6 bytes from stack
	add ix,sp
	ld sp,ix
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

DoRstTest:
	ld hl,&0030	;RST6 - Free for our use
	ld a,&C3	;JP
	ld de,RstTest
	ld (hl),a	;Define jump command on RST6 (&30)
	inc hl
	ld (hl),e
	inc hl	
	ld (hl),d

	call RstTest
	dw TxtOne	;String to show

	rst &30		;RST6
	dw TxtTwo	;String to show

	rst &30		;RST6
	dw TxtThree	;String to show

	rst &30		;RST6
	dw TxtThanks	;String to show
	ret

RstTest:	
	pop hl
		ld e,(hl)	;Get Word from after CALL to this function
		inc hl
		ld d,(hl)
		inc hl
	push hl
	ex de,hl		;Swap param into HL
	jp PrintString
	;ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PrintChar 	equ &BB5A
PrintString:
	ld a,(hl)		;Print a '255' terminated string 
	cp 255
	ret z
	inc hl
	call PrintChar
	jr PrintString


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

RetCaseTest:
	ld hl,RetCaseThanks	;put RetCaseThanks on stack
				;A RET will jump to that address
	push hl
	
		cp 1
		jr nz,RetCaseNot1
		ld hl,TxtOne
		call PrintString
	ret 			;jp RetCaseThanks
RetCaseNot1:
		cp 2
		jr nz,RetCaseNot2
		ld hl,TxtTwo
		call PrintString
	ret 			;jp RetCaseThanks	
RetCaseNot2:
		cp 3
		jr nz,RetCaseNot3
		ld hl,TxtThree
		call PrintString
	ret 			;jp RetCaseThanks	
RetCaseNot3:
		ld hl,TxtHey
		call PrintString
	pop hl			;remove RetCaseThanks from stack
	ret

RetCaseThanks:
	ld hl,txtThanks
	call PrintString
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

CaseTest:
	cp 1
	jr nz,CaseNot1
	ld hl,TxtOne		;Show ONE
	call PrintString
	jp CaseThanks	
CaseNot1:
	cp 2
	jr nz,CaseNot2
	ld hl,TxtTwo		;Show TWO
	call PrintString
	jp CaseThanks	
CaseNot2:
	cp 3
	jr nz,CaseNot3	
	ld hl,TxtThree		;Show THREE
	call PrintString
	jp CaseThanks	
CaseNot3:
	ld hl,TxtHey		;ALL OTHERS
	call PrintString
	ret

CaseThanks:
	ld hl,txtThanks		;End Routine
	call PrintString
	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


TxtOne: db "One",255
TxtTwo: db "Two",255
TxtThree: db "Three",255
TxtHey: db "Hey, What's that?",255

TxtThanks: db 13,10,"Thanks for playing!",255


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


StackMisuse:
	di
	ld ix,0
	add ix,sp		;Backup the stack for later
				;Don't use Stack/CALL from here
		ld sp,&C000+&50 ;Point stack one char down the screen
		ld hl,&C000	;Destination is top of the screen
StackMisuseAgain:
		pop de		;Read 2 bytes 
		ld (hl),e
		inc hl
		ld (hl),d
		inc hl
		ld a,h
		or a
		jr nz,StackMisuseAgain
	ld sp,ix		;Restore Stack
	ei
	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


NewLine:
	ld a,13		;Carriage return
	call PrintChar
	ld a,10		;Line Feed 
	jp PrintChar


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Bonus! Monitor/Memdump

;These are needed only for the Monitor/Memdump

BuildCPC equ 1		
SimpleBuild equ 1
	read "\SrcALL\Multiplatform_Monitor_RomVer.asm"
	read "\SrcALL\Multiplatform_ShowHex.asm"
	read "\SrcALL\Multiplatform_MonitorMemdump.asm"
	