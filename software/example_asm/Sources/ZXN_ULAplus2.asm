	;Unrem this if building with vasm
	include "\SrcALL\VasmBuildCompat.asm"
	
	macro nextreg,reg,val
		db &ED,&91,\reg,\val
	endm
	
	macro nextregA,reg
		db &ED,&92,\reg
	endm

	Org &8000				;Code Origin
	di
	
	ld hl,SampleScreen		;Source 
	ld de,&4000				;Destination VRAM
	ld bc,SampleScreen_End-SampleScreen	;Length
	ldir
	
	xor a
	out (&fe),a				;border=0
	
	nextreg $14,%11101110	;Transparency Color
	
	            ;IPPPSLUN
    nextreg &43,%00000001   ;Layer ULA palette 1
				;FBbbbfff
	nextreg &42,%00001111   ;Bits ----1111 = Foreground
							;Bits 1111---- = Background
	

    nextreg &40,0           ;palette index 0
	ld b,16					;Entries
	ld hl,MyPalette			;Source Definition
	call DefinePalettes	
	
    nextreg &40,128         ;palette index 128
	ld b,16					;Entries
	ld hl,MyPalette			;Source Definition
	call DefinePalettes	
	
	di
	halt
	
	
DefinePalettes:
	ld a,(hl)				;get color (RRRGGGBB)
	inc hl
    nextregA &41           	;Send the colour 
    djnz DefinePalettes    	;Repeat for next color	
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	

MyPalette:					;Forground
       ;RRRGGGBB    
	db %00000000 ;0
    db %00000000 ;1
    db %01001001 ;2
    db %10110110 ;3
    db %11111111 ;4
    db %10000010 ;5
    db %10011111 ;6
    db %11000000 ;7
    db %00010101 ;8
    db %11101010 ;9
    db %11001011 ;10
    db %00100010 ;11
    db %11100000 ;12
    db %10000000 ;13
    db %11111000 ;14
    db %11111110 ;15


SampleScreen:
	incbin "\ResALL\Sprites\SpecULA.SCR"
SampleScreen_End:	

BitmapFont:
	ifdef BMP_UppercaseOnlyFont
		incbin "\ResALL\Font64.FNT"			;Font bitmap, this is common to all systems
	else
		incbin "\ResALL\Font96.FNT"			;Font bitmap, this is common to all systems
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


	