	;Unrem this if building with vasm
	include "\SrcALL\VasmBuildCompat.asm"
	
	;The Spectrum NEXT cpu has some extra opcodes for setting it's registers,
	;We'll define a couple of Macros to help us out.
	
	macro nextreg,reg,val
		db &ED,&91,\reg,\val;Macro for Setting A ZX Next Register
	endm
	macro nextregA,reg
		db &ED,&92,\reg		;Macro for Setting A ZX Next Register from A
	endm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
	
	Org &8000				;Code Origin
	di
	
;Palette Init
                ;IPPPSLUN
    nextreg &43,%00010000   ;Layer 2 to 1st palette
	
    nextreg &40,0           ;palette index 0

	ld b,4					;Color Count
	ld hl,MyPalette			;Palette
paletteloop:
	ld a,(hl)				;get color (RRRGGGBB)
	inc hl
    nextregA &41           	;Send the colour 
    
    djnz paletteloop      	;Repeat for next color
	
	

;Enable Layer 2 (256 color screen) and make it writable
	
		 ;BB--P-VW		V=visible  W=Write 
	ld a,%00000011			;(Bank at &0000-&3FFF)  B=bank
	ld bc,&123B
	out (c),a		
		
	nextreg &07,2			;CPU to 14mhz (0=3.5mjz 1=7mhz)
	

	ld a,(NewPosX+1)
	ld b,a
	ld a,(NewPosY+1)
	ld c,a


	call ShowSprite	;Show the Cursor to the scree

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

ShowAgain:	
	push bc
		call ReadMouse
	pop bc

	ld a,(FireDown)
	or a
	jr z,NoFire
	
;Fire is down - show our test sprite at current location	

	ld ix,&3030
	ld de,TestSprite2
	call ShowSpriteB	;Show Chibiko

	xor a
	ld (FireDown),a		;Clear Fire
NoFire:

;Update the Cursor

	ld a,(OldPosX+1)	;Last Joystick pos
	ld b,a
	ld a,(OldPosY+1)
	ld c,a

	ld a,(NewPosX+1)	;New Joystick pos
	ld h,a
	ld a,(NewPosY+1)
	ld l,a

	or a
	sbc hl,bc			;See if Cursor pos changed
	jr z,ShowAgain
	
	ld a,(OldPosX+1)
	ld b,a
	ld a,(OldPosY+1)
	ld c,a

	call ShowSprite		;Remove old cursor

	ld hl,(NewPosX)		;Update Cursor pos
	ld de,(NewPosY)
	ld (OldPosX),hl
	ld (OldPosY),de

	ld b,h
	ld c,d
	push bc
		call ShowSprite	;Show New Cursor
	pop bc
	
	
	ld a,(FireHeld)
	or a
	jr z,MoFireHeld		;Check if fire is still down

;Fire is Held - show our test sprite at current location	
	ld ix,&0808
	ld de,DragSprite
	call ShowSpriteB	
MoFireHeld:

	jp ShowAgain
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ReadMouse:	
	ld bc,&FADF			;FADF Mouse Buttons
	in a,(c)	
	or a
	   ;%------LR
	and %00000010
	jr nz,JoyNotFire
	ld a,(FireHeld)
	or a
	jr nz,JoyNotFireB
	ld a,1
	ld (FireDown),a		;Set FireDown/Held
	ld (FireHeld),a
	jr JoyNotFireB
JoyNotFire:
	xor a		
	ld (FireHeld),a		;Release Fire Held
JoyNotFireB:
	
	ld a,(XMove)
	ld d,a
	inc b		
	in a,(c)			;FBDF Mouse Xpos
	ld (XMove),a
	sub d
	ld d,a
	
	ld a,(YMove)
	ld e,a
	ld b,&FF	
	in a,(c)			;FFDF Mouse Ypos
	ld (YMove),a
	sub e
	neg					;Flip Ypos
	ld e,a
	
	ld bc,0
	ld b,d
	ld hl,(NewPosX)
	
	ld a,d				;D=Xpos
	or a
	jp m,MouseLeft
	
;MouseRight
	add hl,bc
	jr c,MouseUPDOWN
	ld a,h
	cp 256-7			;Over right of screen?
	jr nc,MouseUPDOWN
	
	ld (NewPosX),hl
	jr MouseUPDOWN
	
MouseLeft:	
	add hl,bc
	jr nc,MouseUPDOWN	;Over left of screen?
	ld (NewPosX),hl
		
MouseUPDOWN:			
	ld b,e
	ld hl,(NewPosY)
	
	ld a,e				;E=Ypos
	or a
	jp m,MouseUp
	
;MouseDown
	add hl,bc
	ret c
	ld a,h
	cp 192-7			;At bottom of screen?
	ret nc

	ld (NewPosY),hl
	ret
	
MouseUp:	
	add hl,bc	
	ret nc				;Over top of screen?
	ld (NewPosY),hl
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

OldPosX: dw 00		;Last Mouse position
OldPosY: dw 00
NewPosX: dw 00		;Current Mouse position
NewPosY: dw 00

FireHeld: db 0		;Mouse Left is Held
FireDown: db 0		;Mouse Left was pressed (not processed yet)

XMove: db 0			;Used for detecting movement changes
YMove: db 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ShowSprite:			;Show Mouse Cursor
	ld de,Cursor ;Sprite Source
	ld ix,&0808
	
ShowSpriteB:		;Show Alt Sprite
	call GetScreenPos	;HL=ScreenAddr
NextYLine:
	push hl
		ld b,ixl			;Width
NextByte:
		ld a,(de)
		xor (hl)
		ld (hl),a
		inc hl
		inc de
		djnz NextByte
	pop hl
	call GetNextLine	;Move DE down a line
	dec ixh				;Next Line
	jr nz,NextYLine
	ret
DragSprite:
	db 0,0,0,0,0,0,0,0
	db 0,0,0,0,0,0,0,0
	db 0,0,0,1,1,0,0,0
	db 0,0,1,1,1,1,0,0
	db 0,0,1,1,1,1,0,0
	db 0,0,0,1,1,0,0,0
	db 0,0,0,0,0,0,0,0
	db 0,0,0,0,0,0,0,0
Cursor:
	db 0,0,0,3,3,0,0,0
	db 0,0,0,3,3,0,0,0
	db 0,0,0,3,3,0,0,0
	db 3,3,3,0,0,3,3,3
	db 3,3,3,0,0,3,3,3
	db 0,0,0,3,3,0,0,0
	db 0,0,0,3,3,0,0,0
	db 0,0,0,3,3,0,0,0
	
TestSprite2:	;256 color bitmap
    incbin "\resAll\Sprites\RawZXN.RAW"

GetNextLine:		;Move DE down a line
	inc h
	ld a,h
	and %00111111	;63 lines per bank
	ret nz			;Return if not changed bank
	ld h,a
	ld bc,&123B
	in a,(c)		;BB---P-VW	- Page in and make visible
	add %01000000	;nNxt Bank
	out (c),a		;Set New Bank
	
	rlca			;Shift XX------
	rlca			
	rlca			;Shift -----XX-
	and %00000110
	add 16			;Bank 16 is first Layer 2 bank
	
	nextregA &50	;Page in B   to &0000-&1FFF range
	inc a			;2x 8k banks	
	nextregA &51	;Page in B+1 to &2000-&3FFF range
	
	
	ret


GetScreenPos:	;return memory pos in HL of screen co-ord B,C (X,Y)
	push af
	push bc
		ld l,b	
		ld a,c
		and %00111111	;Offset in third of bank
		ld h,a
		ld a,c
		and %11000000	;Bank in correct third for &0000-&3fff
		
			;BB--PRVW		-V= visible W=Write R=Read  B=bank
		or  %00000011
		ld bc,&123B
		out (c),a		;Page in and make visible

; *** R Bit is new as of Core 3 allowing writes to ROM area VRAM, 
; But our Emulator doesn't support it - that's what happens when you 
; keep F-ing with the spec after release!
		
		rlca			;Shift XX------
		rlca			
		rlca			;Shift -----XX-
		and %00000110
		add 16			;Bank 16 is first Layer 2 bank
		
		nextregA &50	;Page in B   to &0000-&1FFF range
		inc a			;2x 8k banks	
		nextregA &51	;Page in B+1 to &2000-&3FFF range
	pop bc
	pop af
	ret
		
		
	
MyPalette:			;8 bits per color
	   ;RRRGGGBB     3 red bits, 3 green bits, 2 blue bit
    db %00000000 ;0
    db %10000011 ;1
    db %00011111 ;2
    db %11111111 ;3

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


	