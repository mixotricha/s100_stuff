NoStartRelocate equ 1 	;For New CPC Header

;Uncomment one of the lines below to select your compilation target

;BuildCPC equ 1	; Build for Amstrad CPC
;BuildMSX equ 1 ; Build for MSX
;BuildTI8 equ 1 ; Build for TI-83+
;BuildZXS equ 1 ; Build for ZX Spectrum
;BuildENT equ 1 ; Build for Enterprise
;BuildSAM equ 1 ; Build for SamCoupe
;BuildSxx equ 1 ; Build for Sega Mastersystem
;BuildSGG equ 1 ; Build for Sega GameGear
;BuildGMB equ 1 ; Build for GameBoy Regular
;BuildGBC equ 1 ; Build for GameBoyColor 
;BuildMSX_MSX1VDP equ 1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef vasm
		include "..\SrcALL\VasmBuildCompat.asm"
	else
		read "..\SrcALL\WinApeBuildCompat.asm"
	endif
	read "..\SrcALL\CPU_Compatability.asm"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



	read "..\SrcALL\V1_Header.asm"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;			Start Your Program Here
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Tile Settings for Hardware Tilemap Systems

	ifdef BuildSxx			;Hardware sprite system.
	
TileBitmaps equ 0			;Tile offset for bitmap Tile Data
FillData equ (FillDataT-TileData)/32			;Tile offset for 'fill data'
TileByteWidth equ 1
Tsize equ 1					;These are unused on TileSprite systems
Fsize equ 1
Fmarker equ 0
Tmarker equ 0

TranspT equ &FFFF			;Unused Tile Word

TileForeGround equ 4096	;Tile in foreground over sprites on SMS
	endif	

	
;Tile Settings for Bitmap Systems	
	
	ifdef BuildSAM
TileByteWidth equ 4		;4bpp
	endif
	ifdef BuildCPC	
TileByteWidth equ 2		;2bpp
	endif
	ifdef BuildZXS
TileByteWidth equ 1		;1bppp
	endif

	ifndef Tsize
Tsize equ 8*TileByteWidth	;Each Tile uses this many bytes
Fsize equ 4					;Each SolidFill definition uses 4 bytes

Tmarker equ 3				;Add this to a tile to make it Transparent
Fmarker equ 1				;Add this to a tile to make it solidfilled
TranspT equ 0+1				;Set a tile to this to make it totally empty (trasparent)

TileForeGround equ 0		;Unused on bitmap systems
	endif

 
;Tilemap Size settings 		World / Logical / Onscreen	

STilemapWidth equ 16		;Supertilemap size (Game world - in 4x4 blocks)
STilemapHeight equ 16

LTilemapWidth equ 40		;Logical tilemap size (in blocks) (precalculated viewable)
LTilemapHeight equ 32	

VTilemapWidth equ 32		;Visible tilemap size (in blocks) 
	ifdef BuildSxx
VTilemapHeight equ 25
	else
VTilemapHeight equ 24				  		
	endif

	
	
VTileMapXoff equ 0			;X offset in BYTES (try 8 on cpc)
HTileMapYoffH equ &0		;Screen base offset (try 01 on cpc)
HTileMapYoffL equ &0		;Screen base offset (try &40 on cpc)

;Addresses for 5 Tiles
T0 equ Tsize*0
T1 equ Tsize*1
T2 equ Tsize*2
T3 equ Tsize*3
T4 equ Tsize*4
TS equ Tsize

;Addresses for 5 Fill Tiles
F0 equ Fsize*0
F1 equ Fsize*1
F2 equ Fsize*2
F3 equ Fsize*3
F4 equ Fsize*4


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Set up Screen etc.

	ifdef BuildSxx
		ld sp, &dff0		;Default stack pointer
		di
		call ScreenInit	
	endif
	
	
	ifdef BuildSAM
		di
		ld sp,&0000
		
		ld a,%01101110	;	Bit 0 R/W  BCD 1 of video page control.
		ld bc,252 	;VMPR - Video Memory Page Register (252 dec)
		out (c),a
	
		ld a,%00101110  ;Bank for Screen
		out (250),a		;Page in &0000-7FFF
	endif
	
	
	ifdef BuildCPC
		ld sp,&8000 ;SP usually around &BF00 - this area used by second Screen buffer
		di
		call ScreenInit
	endif
	ifdef BuildZXS
		di
	endif
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Main Loop	

Again:
	ld bc,(VtileOffsetY)	;Alter YPos of tilemap
	dec bc				
	dec bc					;Scroll up
	ld (VtileOffsetY),bc

	ld bc,(VtileOffsetX)	;Alter Xpos of tilemap
	inc bc
	inc bc					;Scroll Right
	ld (VtileOffsetX),bc

	ld iy,SuperTileMapData1		;Tilemap Position settings
	ld ix,SuperTilemap			;SuperTile Source
	ld de,TileBuffer			;TileBuffer Dest
	
	call CalculateAndDrawTilemap	;Calc and draw tilemap

	
	
	call DrawChibiko	;Draw Test Sprite
	call DrawYokubiko 	;Draw Test Sprite
	
	
	
;2nd Layer!
	ld bc,(VtileOffsetY2)	;Alter Y Pos of tilemap
	dec bc
	ld (VtileOffsetY2),bc

	ld bc,(VtileOffsetX2)	;Alter Xpos of tilemap
	inc bc
	ld (VtileOffsetX2),bc
	
	ifdef XShift2
		ld hl,(XShift)	;Use shifts from first tilemap (needed for SMS)
		ld (XShift2),hl
	endif 
	
	ld iy,SuperTileMapData2
	ld ix,SuperTilemapFore
	ld de,TileBuffer2			;TileBuffer Dest
	call CalculateAndDrawTilemap
	
	
	call ClearBorder	;Draw border

	call FlipScreen		;Flip Double Buffer

	jp Again

	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;SMS Ram Defs
	
	;SuperTilemap Vars
	ifdef BuildSxx
SuperTileMapData1 equ &D400
VtileOffsetY equ &D400
VtileOffsetX equ &D402
		;LastLTilemapX	;Used to calc when to update Supertiles
		;LastLTilemapY 

SuperTileMapData2 equ &D408
VtileOffsetY2  equ &D408		;H/Vtile Pos -------- SSSSOOHQ
VtileOffsetX2  equ &D40A 		;S=Supertile Pos O=tile offset H=Half tile shift Q=Quarter shift (unused)


	;dw &6969		;Used to calc when to update Supertiles
	;dw &6969
	
sprestoreB_Plus2 equ &D442
DeRestore_plus2 equ &D444
TileYUnused_Plus2 equ &D446

ActiveBank equ &D416
VisibleBankReg equ &D417
HSpriteNum equ &D418
HSpriteLimit equ 64

ChibikoPos equ &D420
YokubikoPos equ &D424

XShift equ &D430
YShift equ &D431

XShift2 equ &D432
YShift2 equ &D433

	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;SuperTilemap Vars
	ifndef SuperTileMapData1
	
SuperTileMapData1:

;Position of the top left of the Logical tilemap

VtileOffsetY: dw 0		;H/Vtile Pos -------- SSSSOOHQ
VtileOffsetX: dw 0 		;S=Supertile Pos O=tile offset 
						;H=Half tile shift Q=Quarter shift (unused)

;These are the current position of the logical tilemap 

	dw &6969	;LastLTilemapX - Used to calc when to update Supertiles
	dw &6969	;LastLTilemapY 

SuperTileMapData2:
VtileOffsetY2: dw 0		;H/Vtile Pos -------- SSSSSOOHQ
VtileOffsetX2: dw 0 		;S=Supertile Pos O=tile offset H=Half tile shift Q=Quarter shift (unused)
	dw &6969		;Used to calc when to update Supertiles
	dw &6969
	endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Tilemap Defs

; Each byte in the SuperTilemap is a pointer to a 'Supertile definition'
; so each byte represents a 4x4 tile area


SuperTilemap: ;16x16
	db 3,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
	db 2,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2
	db 0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
	db 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
	db 0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
	db 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
	db 2,2,2,2,2,2,0,2,2,2,2,2,2,2,2,2
	db 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
	db 0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
	db 2,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2
	db 0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
	db 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
	db 0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
	db 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
	db 2,2,2,2,2,2,0,2,2,2,2,2,2,2,2,2
	db 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0

;Dummy for wrap around
	db 0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
	db 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
	db 0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1
	db 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0
	db 2,2,2,2,2,2,0,2,2,2,2,2,2,2,2,2
	db 1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0


	


SuperTilemapFore: ;16x16

	db 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3
	db 3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,3
	db 3,3,3,3,4,3,3,3,4,3,3,3,4,3,4,4
	db 4,3,3,3,4,4,3,3,4,4,3,3,4,3,4,4
	db 4,4,3,3,4,4,3,3,4,4,4,3,4,4,4,4
	db 4,4,4,3,4,4,4,3,4,4,4,4,4,4,4,4
	db 4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4
	db 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3
	db 3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,3
	db 3,3,3,3,4,3,3,3,4,3,3,3,4,3,4,4
	db 4,3,3,3,4,4,3,3,4,4,3,3,4,3,4,4
	db 4,4,3,3,4,4,3,3,4,4,4,3,4,4,4,4
	db 4,4,4,3,4,4,4,3,4,4,4,4,4,4,4,4

;Dummy for wrap around
	db 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3
	db 3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,3
	db 3,3,3,3,4,3,3,3,4,3,3,3,4,3,4,4
	db 4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4
	db 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3
	db 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3
	db 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3
	db 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3


	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;SuperTile Definitions - each word is 1 tile 
;4x4 so 16 tiles total - rotated 90 clockwise - so layout is 
	;X0Y0 X0Y1 X0Y2 X0Y3
	;X1Y0 X1Y1 X1Y2 X1Y3
	;X2Y0 X2Y1 X2Y2 X2Y3
	;X3Y0 X3Y1 X3Y2 X3Y3


	align 2		;must be aligned %-------- -------0
SuperTileDefs:	;Supertile 0
	dw FillData+Fmarker+F2,FillData+Fmarker+F2,FillData+Fmarker+F2,FillData+Fmarker+F2
	dw FillData+Fmarker+F2,FillData+Fmarker+F3,FillData+Fmarker+F1,FillData+Fmarker+F2
	dw FillData+Fmarker+F2,FillData+Fmarker+F3,FillData+Fmarker+F1,FillData+Fmarker+F2
	dw FillData+Fmarker+F2,FillData+Fmarker+F2,FillData+Fmarker+F2,FillData+Fmarker+F2
;Supertile 1
	dw TileBitmaps+T2,TileBitmaps+T0,TileBitmaps+T0,TileBitmaps+T2
	dw TileBitmaps+T0,TileBitmaps+T1,TileBitmaps+T1,TileBitmaps+T0
	dw TileBitmaps+T0,TileBitmaps+T1,TileBitmaps+T1,TileBitmaps+T0
	dw TileBitmaps+T2,TileBitmaps+T0,TileBitmaps+T0,TileBitmaps+T2
;Supertile 2
	dw TileBitmaps+T2,TileBitmaps+T3,TileBitmaps+T3,TileBitmaps+T2
	dw TileBitmaps+T3,TileBitmaps+T3,TileBitmaps+T3,TileBitmaps+T3
	dw TileBitmaps+T3,TileBitmaps+T3,TileBitmaps+T3,TileBitmaps+T3
	dw TileBitmaps+T2,TileBitmaps+T3,TileBitmaps+T3,TileBitmaps+T2
;SuperTile 3
	dw TranspT,TranspT,TranspT,TranspT	;Completely Transparent!
	dw TranspT,TranspT,TranspT,TranspT
	dw TranspT,TranspT,TranspT,TranspT
	dw TranspT,TranspT,TranspT,TranspT
;SuperTile4
	dw FillData+Fmarker+F3+TileForeGround,FillData+Fmarker+F3+TileForeGround,FillData+Fmarker+F3+TileForeGround,FillData+Fmarker+F3+TileForeGround
	dw FillData+Fmarker+F3+TileForeGround,FillData+Fmarker+F3+TileForeGround,FillData+Fmarker+F3+TileForeGround,FillData+Fmarker+F3+TileForeGround
	dw FillData+Fmarker+F3+TileForeGround,FillData+Fmarker+F3+TileForeGround,FillData+Fmarker+F3+TileForeGround,FillData+Fmarker+F3+TileForeGround
	dw FillData+Fmarker+F3+TileForeGround,FillData+Fmarker+F3+TileForeGround,FillData+Fmarker+F3+TileForeGround,FillData+Fmarker+F3+TileForeGround
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef BuildSxx
TileBuffer equ &C000	
TileBuffer2 equ &CA00
	endif

	ifndef TileBuffer
		align 2		;must be aligned %-------- -------0
TileBuffer:
		ds 32*40*2	;Buffer for logical Screen (40 wide , 32 tall)
	
		align 2		;must be aligned %-------- -------0
TileBuffer2:
		ds 32*40*2	;Buffer for logical Screen (40 wide , 32 tall)
	endif

	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Bitmap data
	
	ifdef BuildSxx

TileData:
	incbin "\ResALL\Yquest\SMS_YQuest.RAW"
FillDataT:
	db &FF,&00,&00,&00
	db &FF,&00,&00,&00
	db &FF,&00,&00,&00
	db &FF,&00,&00,&00
	db &FF,&00,&00,&00
	db &FF,&00,&00,&00
	db &FF,&00,&00,&00
	db &FF,&00,&00,&00
	
	db &00,&FF,&00,&00
	db &00,&FF,&00,&00
	db &00,&FF,&00,&00
	db &00,&FF,&00,&00
	db &00,&FF,&00,&00
	db &00,&FF,&00,&00
	db &00,&FF,&00,&00
	db &00,&FF,&00,&00
	
	db &00,&00,&FF,&00
	db &00,&00,&FF,&00
	db &00,&00,&FF,&00
	db &00,&00,&FF,&00
	db &00,&00,&FF,&00
	db &00,&00,&FF,&00
	db &00,&00,&FF,&00
	db &00,&00,&FF,&00
	
	db &00,&00,&00,&FF
	db &00,&00,&00,&FF
	db &00,&00,&00,&FF
	db &00,&00,&00,&FF
	db &00,&00,&00,&FF
	db &00,&00,&00,&FF
	db &00,&00,&00,&FF
	db &00,&00,&00,&FF
ChibikoT:
	incbin "\ResALL\Sprites\RawSMS.RAW"
YokubikoT:
	incbin "\ResALL\SpeedTiles\YokubikoTilesSMS.RAW"	
	
TileDataEnd:	

	
ChibikoBmp equ (ChibikoT-TileData)/32
YokubikoBmp equ (YokubikoT-TileData)/32
	


	endif
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	ifdef BuildZXS

	
ClearBorder:
	

	ret
FlipScreen:
		ld hl,&C000
		ld de,&4000
		ld bc,&1800
		ldir
	ret


ActiveBank:	db &C0

	align 4		;must be aligned %-------- ------00
TileBitmaps:			;Tile Data
	incbin "\ResALL\Yquest\ZXS_YQuest.RAW"

	align 4		;must be aligned %-------- ------00
FillData:			;Solid fills
	db %00000000,%00000000	;Tile 0		
	dw 0	;Spacer
	db %10101010,%01010101	;Tile 1
	dw 0	;Spacer
	db %10101010,%11111111	;Tile 2
	dw 0	;Spacer
	db %11111111,%11111111	;Tile 3
	dw 0	;Spacer
	endif


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	ifdef BuildSAM
	align 4		;must be aligned %-------- ------00
TileBitmaps:			;Tile Data
	incbin "\ResALL\Yquest\MSX2_Yquest.RAW"

	align 4		;must be aligned %-------- ------00
FillData:			;Solid fills
	db %00000000,%00000000	;Tile 0		
	dw 0	;Spacer
	db %10101010,%01010101	;Tile 1
	dw 0	;Spacer
	db %10101010,%11111111	;Tile 2
	dw 0	;Spacer
	db %11111111,%11111111	;Tile 3
	dw 0	;Spacer
ActiveBank:	db &00

ScreenBank: db %00001110
	
FlipScreen:
	
		ld a,(ScreenBank)  ;Bank for Screen
		ld h,a
		
		or %01100000	;	Bit 0 R/W  BCD 1 of video page control.
		
		out (252),a 	;VMPR - Video Memory Page Register (252 dec)
		ld a,h
		xor %00000010
		ld h,a
		
		or %00100000  ;Bank for Screen
		out (250),a		;Page in &0000-7FFF
		
		ld a,h
		ld (ScreenBank),a  ;Bank for Screen
	ret

	endif


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	
	
	ifdef BuildCPC	
	
	align 4		;must be aligned %-------- ------00
TileBitmaps:			;Tile Data
	incbin "\ResALL\SpeedTiles\TileTestCPC.RAW"

	align 4		;must be aligned %-------- ------00
FillData:			;Solid fills
	db %00000000,%00000000	;Tile 0		
	dw 0	;Spacer
	db %10100000,%01010000	;Tile 1
	dw 0	;Spacer
	db %10101111,%01011111	;Tile 2
	dw 0	;Spacer	
	db %11111010,%11110101	;Tile 3

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Page fiip &C000 <-> &8000

FlipScreen:
	ld b,&F5
	in a,(c)
	rra			;Wait for Vblank
	jr nc,FlipScreen

	ld hl,(VisibleBank)
	ld a,h			;Swap Active / Visible banks
	ld h,l
	ld l,a
	ld (VisibleBank),hl
	push hl
		rrca    
		rrca    
		and &30
		ld bc,&bc0c			
		out (c),c	;Select CRTC register 12
		inc b		;BC = bd0c
	 	out (c),a	;Set CRTC register 12 data
	pop af	
	ret	;Returns A=Active Screenpos (for Drawing)


VisibleBank 	db &C0
ActiveBank:	db &80

	endif


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;TileSprite Example


;Chibiko Sprite Tilemap
	align 2		;must be aligned %-------- -------0

ChibikoTiles:	;6x7 - Rotated 90 degrees ClockWise
	dw TS*0+ChibikoBmp+Tmarker,TS*6+ChibikoBmp+Tmarker,TS*12+ChibikoBmp+Tmarker,TS*18+ChibikoBmp+Tmarker,TS*24+ChibikoBmp+Tmarker,TS*30+ChibikoBmp+Tmarker,FillData+Fmarker+F2
	dw TS*1+ChibikoBmp+Tmarker,TS*7+ChibikoBmp+0,TS*13+ChibikoBmp+0,TS*19+ChibikoBmp+0,TS*25+ChibikoBmp+0,TS*31+ChibikoBmp+0,TranspT
	dw TS*2+ChibikoBmp+Tmarker,TS*8+ChibikoBmp+0,TS*14+ChibikoBmp+0,TS*20+ChibikoBmp+0,TS*26+ChibikoBmp+0,TS*32+ChibikoBmp+0,FillData+Fmarker+F2
	dw TS*3+ChibikoBmp+Tmarker,TS*9+ChibikoBmp+0,TS*15+ChibikoBmp+0,TS*21+ChibikoBmp+0,TS*27+ChibikoBmp+0,TS*33+ChibikoBmp+0,TranspT
	dw TS*4+ChibikoBmp+Tmarker,TS*10+ChibikoBmp+0,TS*16+ChibikoBmp+0,TS*22+ChibikoBmp+0,TS*28+ChibikoBmp+0,TS*34+ChibikoBmp+0,FillData+Fmarker+F2
	dw TS*5+ChibikoBmp+Tmarker,TS*11+ChibikoBmp+0,TS*17+ChibikoBmp+Tmarker,TS*23+ChibikoBmp+Tmarker,TS*29+ChibikoBmp+Tmarker,TS*35+ChibikoBmp+Tmarker,TranspT

YokubikoTiles:	;3x3 - Rotated 90 degrees ClockWise
	dw TS*0+YokubikoBmp+Tmarker,TS*3+YokubikoBmp+Tmarker,TS*6+YokubikoBmp+Tmarker	;First column
	dw TS*1+YokubikoBmp+0,TS*4+YokubikoBmp+0,TS*7+YokubikoBmp+0	;2nd column
	dw TS*2+YokubikoBmp+Tmarker,TS*5+YokubikoBmp+Tmarker,TS*8+YokubikoBmp+Tmarker	;3rd column

; +0 = Fast PSET tile
; +1 = Flood fill tile
; +3 = Transparent sprite
;TranspT = Unused (transp) tile

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;ChibikoBitmap Data (in 8x8 tiles)
	ifdef BuildZXS
	align 4		;must be aligned %-------- ------00
ChibikoBmp:
	incbin "\ResAll\SpeedTiles\ChibikoTilesZXS.raw"

YokubikoBmp:
	incbin "\ResALL\SpeedTiles\YokubikoTilesZXS.RAW"
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef BuildSAM
	align 4		;must be aligned %-------- ------00
ChibikoBmp:
	incbin "\ResAll\SpeedTiles\ChibikoTilesSAM.RAW"

YokubikoBmp:
	incbin "\ResALL\SpeedTiles\YokubikoTilesSAM.RAW"
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	ifdef BuildCPC
	align 4		;must be aligned %-------- ------00
ChibikoBmp:
	incbin "\ResAll\SpeedTiles\ChibikoTilesCPC.raw"

YokubikoBmp:
	incbin "\ResALL\SpeedTiles\YokubikoTilesCPC.RAW"
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


	;Virtual co-ords are in 4 pixel units
	;first visible co-ord is 64,64
	;Screen size is 64x48
	;Last visible co-ord is 96,112
	ifndef ChibikoPos
ChibikoPos:
	db &6D	;Ypos
	db &AD	;Xpos

YokubikoPos:
	db &90	;Ypos
	db &50	;Xpos	
	endif
	
	
DrawYokubiko:			;Draw a Test Chibiko Tile sprite
	ld de,YokubikoTiles	;Address of Tile Def
	ld bc,(YokubikoPos)
	inc b
	inc b
	dec c
	dec c
	ld (YokubikoPos),bc	;BC=XY Pos
	ld hl,&0303		;Size of sprite WWHH 
	jp DrawSprite
	
DrawChibiko:			;Draw a Test Chibiko Tile sprite
	
	ld de,ChibikoTiles	;Address of Tile Def
	ld bc,(ChibikoPos)
	inc b
	inc c
	ld (ChibikoPos),bc	;BC=XY Pos
	
	ld hl,&0607		;Size of sprite WWHH 
	jp DrawSprite

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Minimal version - no Transparency, and no shifts

;DisableShifts equ 1		;No Vshift module (only needed for CPC)
;DisableTranspTs equ 1		;No 0 byte transparency (less code)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;Sega Mastersystem / GameGear Modules

	ifdef BuildSxx
DisableShifts equ 1		;Minimalist Version
DisableTranspTs equ 1
		read "\SrcSMS\SMS_V1_SpeedTile_NormalTile.asm"
		read "\SrcSMS\SMS_V1_SpeedTile_SpriteTile.asm"
		read "\SrcSMS\SMS_V1_SpeedTile_ScreenDriver.asm"
	endif
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;Amstrad CPC Modules

	ifdef BuildCPC
		read "\SrcCPC\CPC_V1_SpeedTile_NormalTile.asm"
		read "\SrcCPC\CPC_V1_SpeedTile_NormalTileTransp.asm"
		
		read "\SrcCPC\CPC_V1_SpeedTile_ShiftedTile.asm"
		read "\SrcCPC\CPC_V1_SpeedTile_ScreenDriver.asm"
		
		read "\SrcCPC\CPC_V1_SpeedTile_ClearBorder.asm"
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;ZX Spectrum Modules

	ifdef BuildZXS
DisableShifts equ 1			;No Halfshift tile
DisableTranspTs equ 1	;No Transparency
	
		read "\SrcZX\ZX_V1_SpeedTile_NormalTile.asm"

	endif
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;Sam Coupe Modules
	
	ifdef BuildSAM
DisableShifts equ 1	;No Halfshift tile (SAM screen logical enough not to need it!)
		read "\SrcSAM\SAM_V1_SpeedTile_ClearBorder.asm"
		read "\SrcSAM\SAM_V1_SpeedTile_NormalTile.asm"
		read "\SrcSAM\SAM_V1_SpeedTile_NormalTileTransp.asm"
	endif
	
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Speed Tile Common Modules

	read "\SrcALL\V1_SpeedTile_Sprites.asm"
	read "\SrcALL\V1_SpeedTile_SuperTileMap.asm"
	
	
	read "..\SrcALL\V1_Footer.asm"