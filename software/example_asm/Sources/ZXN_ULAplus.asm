	;Unrem this if building with vasm
	include "\SrcALL\VasmBuildCompat.asm"

	include "\SrcZXN\ZXN_V1_Z80_Extensions.asm"
	include "\SrcZX\ZX_V1_header.asm"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;			Classic Spectrum Screen
	
	ld hl,SampleScreen		;Source 
	ld de,&4000				;Destination VRAM
	ld bc,SampleScreen_End-SampleScreen	;Length
	ldir
	
	xor a
	out (&fe),a				;border=0
		
	call DoPause
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;			ULA PLUS Recolor Test
	
	
	nextreg &14,%11101110	;Transparency Color
	
	            ;IPPPSLUN
    nextreg &43,%00000001   ;Enable ULA Next
	
				;FBbbbfff	;Color Attribute bits (&5800-&5AFF)
				
	nextreg &42,%00000111   ;Bits -----111 = Foreground
							;Bits 11111--- = Background
	
	


    nextreg &40,0           ;palette index 0
	ld b,8					;Entries
	ld hl,MyPaletteForeground  ;Source Definition
	call DefinePalettes	
	
    nextreg &40,128         ;palette index 128
	ld b,16					;Entries
	ld hl,MyPaletteBackground	;Source Definition
	call DefinePalettes	
	
	call DoPause
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;			ULA PLUS 16 Color test

	;Replace the color information - 1 nibble background - 1 nibble foreground
	;(Black and white data unchanged)

	ld hl,ULANextColorMap				;Source 
	ld de,&5800							;Destination VRAM
	ld bc,ULANextColorMap_End-ULANextColorMap ;Length
	ldir
	
	
	call DoPause
	
	
				;FBbbbfff
	nextreg &42,%00001111   ;Bits ----1111 = Foreground
							;Bits 1111---- = Background
	
	;Load ULA+ 16 color Palette
    nextreg &40,0           ;palette index 0
	ld b,32					;Entries
	ld hl,MyPalettePlus		;Source Definition
	call DefinePalettes9bit	
	
    nextreg &40,128         ;palette index 128
	ld b,32					;Entries
	ld hl,MyPalettePlus		;Source Definition
	call DefinePalettes9bit	
	
	
	di
	halt
DoPause:
	ld b,100
PauseAgain:	
	ei
	halt
	djnz PauseAgain			;Wait a while before we enable the ULA+ Palette
	
	ret	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;		
	;8 bit palette definition

DefinePalettes:				
	ld a,(hl)				;get color (RRRGGGBB)
	inc hl
    nextregA &41           	;Send the colour 
    djnz DefinePalettes    	;Repeat for next color	
	ret
	
MyPaletteForeground:		;Forground
	   ;RRRGGGBB
	db %00000000	;0
	db %01000011	;1
	db %11100100	;2
	db %11100011	;3
	db %00011100	;4
	db %00011111	;5
	db %11111100	;6
	db %11111111	;7

MyPaletteBackground:		;Background
	   ;RRRGGGBB
	db %00000000	;0-0
	db %00000011	;0-1
	db %11100000	;0-2
	db %11100011	;0-3
	db %00010110	;0-4
	db %00011111	;0-5
	db %11111100	;0-6
	db %11111111	;0-7
	
	db %00000000	;B-0
	db %00100010	;B-1
	db %11100000	;B-2
	db %11100011	;B-3
	db %00011100	;B-4
	db %00011111	;B-5
	db %11110100	;B-6
	db %11111111	;B-7
	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;9 bit palette definition
	
DefinePalettes9bit:
	ld a,(hl)				;get color (RRRGGGBBB)
	inc hl
    nextregA &44           	;Send the colour 
    djnz DefinePalettes9bit	;Repeat for next color	
	ret	
	
MyPalettePlus:			;ULA Plus 16 color palette
       ;RRRGGGBB  -------B 
    db %00000000,%00000000 ;0
    db %00000000,%00000000 ;1
    db %01001001,%00000000 ;2
    db %10110110,%00000001 ;3
    db %11111111,%00000001 ;4
    db %10000010,%00000000 ;5
    db %10011111,%00000000 ;6
    db %11000000,%00000000 ;7
    db %00010101,%00000000 ;8
    db %11101010,%00000001 ;9
    db %11001011,%00000001 ;10
    db %00100010,%00000001 ;11
    db %11100000,%00000000 ;12
    db %10000000,%00000000 ;13
    db %11111000,%00000000 ;14
    db %11111110,%00000000 ;15

	
SampleScreen:					;Spectrum 1bpp Screen Data
	incbin "\ResALL\Sprites\ZXTestScr.scr"	
SampleScreen_End:	

ULANextColorMap:					;Alternate Color Map
	incbin "\ResALL\Sprites\SpecULA.SCR"	
ULANextColorMap_End:	


BitmapFont:
	ifdef BMP_UppercaseOnlyFont
		incbin "\ResALL\Font64.FNT"			;Font bitmap, this is common to all systems
	else
		incbin "\ResALL\Font96.FNT"			;Font bitmap, this is common to all systems
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


	