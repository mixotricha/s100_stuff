	;Unrem this if building with vasm
;	include "\SrcALL\VasmBuildCompat.asm"

PrintChar 	equ &BB5A	

	org &1200
;	jp RLETest1
	jp RLETest2


RLETest2:
	ld hl,ScreenTest
	ld de,&C000
	call RLEDecompress
	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



RLETest1:
	ld hl,RleData		;Source Data
	ld de,TestArea		;Destination
	call RLEDecompress	;Decompress it!

	ld hl,TestArea		;Address to show
	ld c,32			;Bytes to show
	call Monitor_MemDumpDirect ;Show memory to screen
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


RleData:
	db 128+4 ;4x RLE Bytes
	db 	32	;Rle 32 *4

	db 4 	 ;4x Linear Bytes
	db 	1,2,3,4 

	db 128+5 ;5x RLE Bytes
	db 	&66	;Rle &66 *4

	db 5 	 ;5x Linear Bytes
	db 	1,2,3,4,5 

	db 1 	 ;1x Linear Byte
	db 	&FF	 

	db 0	 ;0=End of Stream

TestArea:
	ds 32	;Destination for decompression
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PrintString:
	ld a,(hl)	;Print a '255' terminated string 
	cp 255
	ret z
	inc hl
	call PrintChar
	jr PrintString


NewLine:
	ld a,13		;Carriage return
	call PrintChar
	ld a,10		;Line Feed 
	jp PrintChar

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

RLEDecompress:
	ld a,(hl)	;Load a header byte
	inc hl
	or a
	ret z		;Zero byte=Sequence done - return
	jp m,RLEDecompress_RLE 
			;m=Jump if sign negative (top bit 1)

RLEDecompress_LINEAR:	;Top bit = 0
	ld b,a		;byte count (1-127)
RLEDecompress_LinearAgain:
	ld a,(hl)	;Copy B bytes from source
	inc hl
	ld (de),a	;Save B bytes to Destination
	inc de
	djnz RLEDecompress_LinearAgain	;Repeat until B=0
	jr RLEDecompress ;Get Next Header byte

RLEDecompress_RLE:	;Top bit = 1
	and %01111111	
	ld b,a		;byte count (1-127)
	ld a,(hl)	;Get the byte
	inc hl
RLEDecompress_RLEAgain:
	ld (de),a	;Fill B times to Destination (RLE) 
	inc de
	djnz RLEDecompress_RLEAgain	;Repeat until B=0
	jr RLEDecompress ;Get Next Header byte

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	nolist
ScreenTest:
	incbin "\ResALL\RLE\CPC_RLE.SCR.RLE"

;	dw &6969
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Bonus! Monitor/Memdump

;These are needed only for the Monitor/Memdump

BuildCPC equ 1		
SimpleBuild equ 1
	read "\SrcALL\Multiplatform_Monitor_RomVer.asm"
	read "\SrcALL\Multiplatform_ShowHex.asm"
	read "\SrcALL\Multiplatform_MonitorMemdump.asm"
	