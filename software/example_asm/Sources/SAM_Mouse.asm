		;Unrem this if building with vasm
	include "\SrcALL\VasmBuildCompat.asm"

	Org &8000				;Code Origin

	ld hl,Message			;Address of string
	Call PrintString		;Show String to screen
	
Again:	
	di
MouseWait:	
	ld a,22	;Character for 'AT' command (Yes it's a character in the Charmap!)
		rst 16
		
		xor a
		rst 16
		rst 16
	di
	
	ld bc,&FFFF
BCPause:	
	dec bc
	ld a,b
	or c
	jr nz,BCPause
	

Again:		
	ld bc,&FFFE
	in a,(c)
	in a,(c)
	in a,(c)
	push af				;Buttons
		call DoAxis		;Yaxis (HL)
		ex de,hl
		call DoAxis		;Xaxis (DE)
	pop af
	call Monitor		;Show Register contents
	jp Again
		
DoAxis:
		in a,(c)
		and %00001111	;High Nibble
		ld d,a
		in a,(c)
		and %00001111	;Mid Nibble
		rlca
		rlca
		rlca
		rlca
		ld e,a
		in a,(c)
		and %00001111	;Low Nibble
		or e
		ld e,a
	ret
	
NewLine:
	ld a,13			;Carriage return
	jr PrintChar
	

PrintChar:
	push hl
	push bc
	push de			
	push af
		rst 16		;call &0010	- Print Accumulator character to screen
	pop af
	pop de
	pop bc			
	pop hl
	ret	

PrintString:
	ld a,(hl)		;Print a '255' terminated string 
	cp 255
	ret z
	inc hl
	call PrintChar
	jr PrintString

Message: db 'Hello World 323!',255



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Bonus! Monitor/Memdump

;These are needed only for the Monitor/Memdump

;BuildSAM equ 1		;Enable this if you don't use my scripts
SimpleBuild equ 1
	read "\SrcALL\Multiplatform_Monitor_RomVer.asm"
	read "\SrcALL\Multiplatform_ShowHex.asm"
	read "\SrcALL\Multiplatform_MonitorMemdump.asm"
	