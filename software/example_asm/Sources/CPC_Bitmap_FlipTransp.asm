nolist 


;Left  Pixel Bits 1537 
;Right Pixel Bits 0426
	macro FlipMode0	;Source Pixels AB
			ld b,a
			and %01010101	;B
			rlca
			ld c,a
			ld a,b
			and %10101010	;A
			rrca
			or c
	endm



	org &1200		;Start of our program

	ld hl,palette
	ld a,1
PaletteAgain;
	ld b,(hl)
	ld c,(hl)
	inc hl
	push af
	push hl
		call &BC32	;A=pen BC=Color
	pop hl
	pop af
	inc a
	cp 10
	jr nz,PaletteAgain


;Build a lookup table for Flipping sprite bytes
	ld hl,FlipLUT
FillLutAgain:
	ld a,l
	FlipMode0	;Mode 0 Ver Flip Macro	
	ld (hl),a
	inc l
	jr nz,FillLutAgain

;Build a lookup table for masking sprite bytes
	ld hl,TranspLUT
	ld (hl),%11111111	;Both pixels kept
BuildLutAgain:
	inc l
	jr z,LUTdone		;Done all 256
	ld a,l
	and %01010101		;Right Pixel Mask
	jr nz,BuildLut2
	ld (hl),%01010101	;Mask to keep back Right pixel
	jr BuildLutAgain
BuildLut2
	ld a,l
	and %10101010		;Left Pixel Mask
	jr nz,BuildLutAgain
	ld (hl),%10101010	;Mask to keep back Left pixel
	jr BuildLutAgain
LUTdone:


	ld a,0
	call &BC0E		;Set Screen Mode A

	ld de,&0010		;Xpos (in pixels)
	ld hl,&00A0		;Ypos (in pixels)
	call &BC1D		;Scr Dot Position - Returns address in HL
	call DrawSprite

	ld de,&0014		;Xpos (in pixels)
	ld hl,&00A0		;Ypos (in pixels)
	call &BC1D		;Scr Dot Position - Returns address in HL
	call DrawSprite

	ld de,&0016		;Xpos (in pixels)
	ld hl,&00A0		;Ypos (in pixels)
	call &BC1D		;Scr Dot Position - Returns address in HL
	call DrawSprite
	ret

DrawSprite:
	ld de,TestSprite	;Sprite Source
	ld ixh,64		;Lines (Height)
	ld bc,FlipLUT	;Lookup Table Address
	ld iy,TranspLUT
SpriteNextLine:
	push hl

	ld ixl,8	;Bytes per line (Width Mode 0/1)


SpriteNextByte:
		ld a,(de)	;Source Byte
		ld c,a		;Set Low byte of address
		ld a,(bc)	;Read Flipped byte from LUT

		ld c,a		;Backup flipped pixel

		ld iyl,a	
		ld a,(hl)	;Get Transparency mask byte From LUT
		and (iy)	;MASK background
		or c		;OR in Color

		ld (hl),a	;Screen Destination

		inc de		;INC Source (Sprite) Address
		dec hl		;INC Dest (Screen) Address

		dec ixl		;Repeat for next byte
		jr nz,SpriteNextByte
	pop hl
	call &BC26		;Scr Next Line (Alter HL to move down a line)
	dec ixh
	jr nz, SpriteNextLine	;Repeat for next line

	ret			;Finished

TestSprite:

	incbin "\ResALL\Sprites\BitmapTransparentTestCPC.raw"

Palette:
	db 0,8,5,26,13,15,24,16,23,10

	align 256	;force Low byte to zero &xx00 (Eg &1700 or &1800)	Align 256
FlipLUT:ds 256
TranspLUT: ds 256	;Lookup table for Color transparency (256 bytes)