	include "\SrcALL\VasmBuildCompat.asm"



	ORG &00F0
	
	DB 0,5			;type 5 = machine code application program
	DW FileEnd-FileStart	;16 bit lenght
	DB 0,0,0,0,0,0,0,0,0,0,0,0 ;not used bytes
;Program starts at ORG &0100

FileStart:			
	LD SP,&100			;Set up a base stack pointer
	call ScreenINIT
	
	ld hl,TestSprite
	ld de,&C000
	ld bc,TestSprite_End-TestSprite
	ldir
	
	ld bc,&FFFF

	call pause
	call pause
	call pause
	call pause
	
	;di
	;halt	
	call newline
	call newline
	call newline
			
	ld hl,Message

	call PrintString
	call newline
	call newline
	call newline
	
	call Monitor			;Show Register contents
	
	call newline
	
	ld hl,&0100			;Address to show
	ld c,32				;Bytes to show
	call Monitor_MemDumpDirect	;Show memory to screen

	
	
	
	di
	halt

	
Pause:	
	dec bc
	nop
	nop
	nop
	nop
	ld a,b
	or c
	jr nz,pause	
	ret
	
CursorX: db 0	
CursorY: db 0

PrintChar:					;Show Character A to the screen
	push hl
	push bc
	push de
	push af
		sub 32				;No characters below 32
		and %11111110		;2 characters per byte - so ignore low bit
		ld h,0				
		sla a
		rl h
		sla a
		rl h				;A*4
		ld l,a
		
		ld de,BitmapFont	;Add base address of the font
		add hl,de
		ex de,hl
		
		ld a,(CursorX)		;Xpos *4
		rlca
		rlca
		ld b,a
		ld a,(CursorY)		;Ypos *8
		rlca
		rlca
		rlca
		ld c,a
		call GetScreenPos
	
		ld ixl,8
CharNextLine:		
		ld a,(de)
		ld c,a
		inc de
	pop af
	push af	
		bit 0,a			;Depending on Bit 0 we may need to shift bits
		
		jr z,CharHighNibble		;Even Character Get the top Nibble
		sll c					
		sll c
		sll c
		sll c					;Odd Character Shift Bottom Nibble
CharHighNibble:				
		push hl
			ld b,4			;Use 4 bits of font
CharNextPixel:
			xor a
			rl c			;Shift a bit into A
			rl a
			ld (hl),a		;Send A to screen
			inc hl
			djnz CharNextPixel
		pop hl
		Call GetNextLine	;Move down a line
		dec ixl
		jr nz,CharNextLine
		
		ld hl,CursorX		;Increase Cursor Pos 
		inc (hl)
		ld a,(hl)
		cp 20
		call z,NewLine
	pop af	
	pop de
	pop bc
	pop hl
	ret
	
NewLine:
	ld hl,CursorX
	ld (hl),0
	inc hl
	inc (hl)
	ret	


PrintString:
	ld a,(hl)	;Print a '255' terminated string 
	cp 255
	ret z
	inc hl
	call PrintChar
	jr PrintString

Message: 
	db 'Hello World 323!',255



	
TestSprite:
	incbin "\ResALL\Sprites\Ent256Test.RAW"
	;incbin "\ResALL\Sprites\Ent256Test2.RAW"
TestSprite_End:	


BitmapFont:
	incbin "\ResALL\Font96LowRes.FNT"


GetScreenPos:	;return memory pos in HL of screen co-ord B,C (X,Y)
				;Input  BC= XY (x=bytes - so 80 across)
				;Output HL= screen mem pos
	push de
;screen is 80 wide = 00000000 01010000
		ld h,C		;YYYYYYYY 00000000
		xor a
		
		srl h
		rra			;0YYYYYYY Y0000000
		srl h
		rra 		;00YYYYYY YY000000
		
		ld d,h
		ld e,a		;Store first part for later
		
		srl h
		rra			;000YYYYY YYY00000
		srl h
		rra			;0000YYYY YYYY0000
		
		add B		;Add the X pos 
		add e		
		ld l,a
		
		ld a,h
		adc d
		add &C0		;Add screen Base &C000
		ld h,a	
	pop de
	ret 			;return memory location in hl


	
GetNextLine:
	push af
		ld a,l		
		add &50		;each line is 80 bytes wide
		ld l,a
		jr nc,GetNextLineDone
		inc h		;Deal with overflow if needed
GetNextLineDone:
	pop af
	ret

	
	;The code below was based on an example from the Enterprise forever forum
ScreenINIT:
	di
	LD A,12		;disable memory wait states
	OUT (191),A	

	;setting the Border color to black

	LD BC,&100+27	;B=1 write
	;C=27 number of system variable (BORDER)	
	LD D,0			;new value
	rst 6*8
	db 16			;handling EXOS variable

	CALL BankSwitch_RequestVidBank	;get a free Video segment
	Jr NZ,VideoFail	;if not available then exit
	LD A,C		
	LD (VIDS),A		;store segment number page to the page 3.
	OUT (&B3),A		

	LD DE,0			;segment number low two bits
	RRA				;will be the top two bits of the video address
	RR D			
	RRA				
	RR D			
	LD (VIDADR1),DE ;this is the start of pixel data in the video memory

	LD HL,&3F00		;after the pixel bytes starting the Line Parameter Table (LPT)
	ADD HL,DE	
	LD (LPTADR),HL	
		
	LD HL,LPT		;Line Parameter Table copy to the end of video segment
	LD DE,&FF00	
	LD BC,LPTH	
	LDIR		
VidAgain:		
	LD HL,(LPTADR)	;LPT video address
	LD B,4			;4 bit rotate
LPTA:           SRL H
	RR L
	DJNZ LPTA
	LD A,L			;low byte send to Nick
	OUT (&82),A	
	LD A,H			;high 4 bits enable Nick running
	OR &C0		
	
	;switch to the new LPT at the end of current frame send to Nick
	OUT (&83),A	
			
VideoFail:
	ret



BankSwitch_RequestVidBank:           
	LD HL,FileEnd		;buffer area
	LD (HL),0			;start of the list
GetSegment:	
	rst 6*8				;get a free segment
	db 24			
	RET NZ				;if error then return
	LD A,C				;segment number
	CP &FC				;<0FCh?, no video segment?
	JR NC,ENDGET		;exit cycle if video
	INC HL				;next buffer address
	LD (HL),C			;store segment number
	JR GetSegment		;get next segment
ENDGET: 
	PUSH BC				;store segment number
FREES:	LD C,(HL)		;deallocate onwanted

		rst 6 *8
		db 25			;free non video segments

		DEC HL			;previoud buffer address
		JR Z,FREES		;continue deallocating when call the EXOS 25 function with
		;c=0 which is stored at the start of list, then got a error, flag is NZ
		
	POP BC				;get back the video segment number
	XOR A				;Z = no error
	ret


;NICK Line Parameter Table (LPT)

LPT: DB 256-200		;Screen Size: 200 lines.... 
					;Two's complement of the number of Screen Lines 
	   ;ICCRMMML
	db %01110010   		;I   VINT=0, no IRQ
						;CC  Colour Mode=01, 256 colours mode (2/4/16/256)
						;R   VRES=1, full vertical resolution (full/half)
						;MMM Video Mode=001, pixel graphics mode
						;L   Reload=0, LPT will continue
	
	DB 11   		;left margin=11
	DB 51  			;right margin=51
VIDADR1:  DW 0    	;primary video address, address of pixel data
	DW 0  			;secondary vide0 address, not used in pixel graphics mode
ENT_PALETTE:	
	; 	GRBGRBGR  	;g0 | r0 | b0 | g1 | r1 | b1 | g2 | r2 | - x0 = lsb 
	db 0;%00000000	;Color 0: Black
	db 0;%00100100	;Color 1: Blue
	db 0;%11011011	;Color 2: Yellow
	db 0;%01001001	;Color 3: Red
	db 0,0,0,0 		;Colors 4-7 (unused)

;Line definitions			
	DB -50,&12,63,0,0,0,0,0,0,0,0,0,0,0,0,0	;50 lines of border, this is the bottom margin,
	DB -3,16,63,0,0,0,0,0,0,0,0,0,0,0,0,0	;3 black lines, syncronization off
	DB -4,16,6,63,0,0,0,0,0,0,0,0,0,0,0,0	;4 lines, syncronization on
	DB -1,&90,63,32,0,0,0,0,0,0,0,0,0,0,0,0	;1 line, syncronization will switch off at half of line
												;the NICK chip generate video IRQ at this line
	DB 252,&12,6,63,0,0,0,0,0,0,0,0,0,0,0,0 ;4 black lines
	DB -50,&13,63,0,0,0,0,0,0,0,0,0,0,0,0,0 ;50 lines of border, this is the top margin,
LPTEnd:              
LPTH equ LPTEnd-LPT ;length of LPT
	
VIDS:		DB 0	;variables for storing allocated memory
LPTADR:     DW 0		
			
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Bonus! Monitor/Memdump

;These are needed only for the Monitor/Memdump
ScreenWidth20 equ 1
;BuildENT equ 1		
	read "\SrcALL\Multiplatform_Monitor_RomVer.asm"
	read "\SrcALL\Multiplatform_ShowHex.asm"
	read "\SrcALL\Multiplatform_MonitorMemdump.asm"
	
	
FileEnd:
	