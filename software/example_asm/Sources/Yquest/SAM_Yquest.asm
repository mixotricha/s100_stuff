
ScreenWidth32 equ 1
ScreenWidth equ 32
ScreenHeight equ 24
ScreenObjWidth equ 128-4
ScreenObjHeight equ 192-8

UserRam Equ &7000			;Game Ram

	nolist

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef vasm
		include "\SrcALL\VasmBuildCompat.asm"
	else
		read "\SrcALL\WinApeBuildCompat.asm"
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	read "\SrcALL\CPU_Compatability.asm"

	Org &8000		;Code Origin

	di	;The Sam Screen is 24k, and we need to move it in at &0000-&7FFF
		;we must keep interrupts disabled the whole time
	
	ld sp,&BFFF		;Define a stack pointer
	
	
		; MSSBBBBB	- M=Midi io / S=Screen mode / B=video Bank
	ld a,%01101110
	out (252),a		;VMPR - Video Memory Page Register (252 dec)
		
	ld hl,UserRam
	ld de,UserRam+1
	ld bc,&800
	ld (hl),0
	ldir
	
	ld hl,Palette
	ld b,0
	ld d,16
	ld c,248
NextPalette:
	ld a,(hl)
	inc hl
	out (c),a
	inc b
	dec d
	jr nz,NextPalette
	

ShowTitle:
;Init Game Defaults
	ld a,3
	ld (lives),a		;Life count
	xor a
	ld (level),a		;Level number
	ld (PlayerObject),a	;Player Sprite
	call ChibiSound		;Mute sound

	call cls			;Scr Clear

;Show Title Screen	
	ld hl,TitlePic
	ld c,0
TitlePixNextY:
	ld b,0
TitlePixNextX:
	push bc		
	push hl
		ld a,(hl)		;Sprite number
		or a
		jr z,TitleNoSprite
		ld h,a
		push bc
			call GetSpriteAddr ; H=spritenum
		pop bc
		sla b				;XPos*4
		sla b
		sla c				;Ypos*8
		sla c
		sla c
		call ShowSprite 	;BC=XY HL=Sprnum
TitleNoSprite:
	pop hl
	inc hl
	pop bc
	inc b
	ld a,b
	cp ScreenWidth
	jr nz,TitlePixNextX	
	inc C
	ld a,c
	cp ScreenHeight
	jr nz,TitlePixNextY

	ld hl,&0D10
	call Locate
	ld hl,txtFire			;Show Fire
	call PrintString
	
	ld hl,&1202
	call Locate
	ld hl,txtUrl			;Show URL
	call PrintString

	ld hl,&1000
	call Locate
	ld hl,txtHiScore
	call PrintString
	ld de,HiScore	 		;Show the highscore
	ld b,4
	call BCD_Show

StartLevel:
	Call WaitForFire
	call CLS
	Call ResetPlayer
	call LevelInit
	
	ld a,%11111111			;Dummy Key value 
	JR StartDraw

infloop:
	ld bc,400				;Slow down game
	ld d,%11111111
PauseBC:
	push de
	push bc
		call ReadJoystick 	;KM Get Joystick... Returns ---FRLDU
	pop bc
	pop de
	cp %11111111
	jr z,PauseNoKey
	ld d,a
PauseNoKey:
	dec bc
	ld a,b
	or c
	jr nz,PauseBC
	ld a,d

StartDraw:
	push af
		push bc
			call DrawUi
			ld IX,PlayerObject
			call BlankSprite;Remove old player sprite
		pop bc
	pop af
	ld d,a

	ld hl,KeyTimeout
	ld a,(hl)
	or a
	jr z,ProcessKeys
	dec (hl)
	jp JoySkip			;skip player input
	
ProcessKeys:
	ld hl,PlayerAccY
	ld e,0				;Timeout
	bit 0,d
	jr nz,JoyNotUp		;Jump if UP not presesd
	ld a,(hl)
	sub 1				;Move Y Up the screen
	ld (hl),a
	ld e,2
JoyNotUp:
	bit 1,d
	jr nz,JoyNotDown	;Jump if DOWN not presesd
	ld a,(hl)
	add 1				;Move Y Down the screen
	ld (hl),a
	ld e,2
JoyNotDown:
	ld hl,PlayerAccX
	bit 2,d
	jr nz,JoyNotLeft 	;Jump if LEFT not presesd
	ld a,(hl)
	sub 1
	ld (hl),a			;Move X Left 
	ld e,2
JoyNotLeft:
	bit 3,d
	jr nz,JoyNotRight	;Jump if RIGHT not presesd
	ld a,(hl)
	add 1	
	ld (hl),a			;Move X Right
	ld e,2
JoyNotRight: 
	bit 5,d
	jr nz,JoyNotFire	;Jump if Fire not presesd
	call PlayerFirebullet
JoyNotFire: 
	bit 4,d
	jr nz,JoyNotFire2	;Jump if Fire2 not presesd
	xor a
	ld (PlayerAccX),a
	ld (PlayerAccY),a
JoyNotFire2: 
	ld a,e
	ld (KeyTimeout),a
JoySkip: 	

	call DrawAndMove
	jp infloop


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
PrintCharB:
	push bc
		sub 32
		ld h,a				;Sprite Source
		ld l,0
		srl h				;32 Bytes per char
		rr l
		srl h
		rr l
		srl h
		rr l
		ld bc,FontData		;Font Base
		add hl,bc

		ld bc,(CursorY)		;Get X and Y
		rl c				;Y*8
		rl c
		rl c
		rl b				;X*4
		rl b
	jr ShowSpriteB
	
ShowSprite:
	push bc
	jr ShowSpriteB
DoGetSpriteObj:
	push bc
		ld h,(IX+O_SprNum)		;Sprite Source
		call GetSpriteAddr
DrawBoth:
		ld b,(IX+O_Xpos)
		ld c,(IX+O_Ypos)
ShowSpriteB:
		di	;The Sam Screen is 24k, and we need to move it in at &0000-&7FFF
				;we must keep interrupts disabled the whole time
		
		call GetScreenPos		;Calculate ram position
		push ix
			ld ixl,8			;Height of bitmap
			
				; WRrBBBBB W=Write protect Bank (&0000-&3FFF) / r=ram in low area 
			ld a,%00101110  			;/ R=rom in high area / B=low ram Bank
			out (250),a			;LMPR - Low Memory Page Register (250 dec) 
	drawnextline:
			push de
				ld bc,4			;Width of bitmap in bytes
				ldir
			pop de
			call GetNextLine	;Move down a line
			
			dec ixl				;Repeat for next line
			jr nz,drawnextline
			
				; WRrBBBBB W=Write protect Bank (&0000-&3FFF) / r=ram in low area 
			ld a,%00011111  			;/ R=rom in high area / B=low ram Bank
			out (250),a			;Turn on Rom 0 again
		pop ix
	pop bc
	ret

BlankSprite:	
	ld a,(IX+O_CollProg)
	cp 250
	ret nc

	push bc
		ld hl,FontData	;Sprite Source
	jp DrawBoth

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GetSpriteAddr:				;H=Sprite Source
		ld l,0				
		srl h				;32 bytes per sprite
		rr l
		srl h
		rr l
		srl h
		rr l
		ld bc,SpriteData
		add hl,bc

		ld a,(SpriteFrame)	;Sprite frame (512 bytes per bank)
		rlca
		add h
		ld h,a	
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BitmapData:
FontData:
	incbin "\ResALL\Yquest\MSX2_Font.raw"

SpriteData:
	incbin "\ResALL\Yquest\MSX2_YQuest.raw"
	incbin "\ResALL\Yquest\MSX2_YQuest2.raw"
	incbin "\ResALL\Yquest\MSX2_YQuest3.raw"
	incbin "\ResALL\Yquest\MSX2_YQuest4.raw"
BitmapDataEnd:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GetScreenPos:	;return mempos in DE of screen co-ord B,C (X,Y)
	ld d,c	
	xor a	
	rr d		;Effectively Multiply Ypos by 128
	rra 
	or b		;Add Xpos
	ld e,a	
	ret

GetNextLine:	;Move DE down 1 line 
	ld a,&80	
	add e		;Add 128 to mem position 
	ld e,a
	ret nc
	inc d		;Add any carry
	ret
	
WaitForFire:
	call DoRandom
	call ReadJoystick 		
	and %00100000		;Fire Button
	jr z,WaitForFireB

WaitForFireB:
	call DoRandom
	call ReadJoystick 		
	and %00100000		;Fire Button
	jr nz,WaitForFireB
	ret
	
ReadJoystick:
	LD  B,%01111111		;%01111111 B, N, M, Sym, Space
	;set upper address lines - note, low bit is zero
	LD  C,&FE  			;port for lines K1 - K5
	
	ld l,3
JoyInAgain:	
	in a,(c)	
	rra		;F2 - Space, F1- Enter, Right - P
	rl h
	rrc b	;%10111111 H, J, K, L  , Enter
			;%11011111 Y, U, I, O  , P
			;%11101111 6, 7, 8, 9  , 0
	dec l
	jr nz,JoyInAgain
	
	rra			;Left - O
	rl h				
	rrc b	;%11110111 5, 4, 3, 2  , 1
	rrc b	;%11111011 T, R, E, W  , Q
	
	in a,(c)	
	rra
	rl l		;U - Q (for later)
	rrc b	;%11111101 G, F, D, S  , A
	
	in a,(c)	
	rra
	rl h		;Down - A
	
	rr l		;Up   - Q
	rl h
	ld a,h
	or %10000000 ;Set unused bit
	ret
	
	
Cls:	
	ld a,%00101110  			;/ R=rom in high area / B=low ram Bank
	out (250),a			;LMPR - Low Memory Page Register (250 dec) 
	
	ld hl,0
	ld de,1
	ld bc,(128*192)-1
	ld (hl),0
	ldir
	
	ld a,%00011111  			;/ R=rom in high area / B=low ram Bank
	out (250),a			;Turn on Rom 0 again
	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
Palette:
	db %00000000; ;0  --GRBLGRB
	db %01001100; ;1  --GRBLGRB
	db %00000111; ;2  --GRBLGRB
	db %01111000; ;3  --GRBLGRB
	db %01111111; ;4  --GRBLGRB
	db %01000001; ;5  --GRBLGRB
	db %01000100; ;6  --GRBLGRB
	db %00101010; ;7  --GRBLGRB
	db %00101111; ;8  --GRBLGRB
	db %01101011; ;9  --GRBLGRB
	db %01101111; ;10  --GRBLGRB
	db %00111000; ;11  --GRBLGRB
	db %00110011; ;12  --GRBLGRB
	db %00011001; ;13  --GRBLGRB
	db %00011100; ;14  --GRBLGRB
	db %01010101; ;15  --GRBLGRB

	read "YQ_DataDefs.asm"
	read "YQ_RamDefs.asm"
	read "YQ_Multiplatform.asm"
	
	read "\SrcALL\Multiplatform_ChibiSound.asm"			;Sound Driver
	read "\SrcALL\MultiPlatform_ShowDecimal.asm"		;Show decimal numbers
	read "\SrcALL\Multiplatform_BCD.asm"				;Show Binary Coded Decimal
	;read "\SrcALL\Multiplatform_ShowHex.asm"			;Hex Display

	