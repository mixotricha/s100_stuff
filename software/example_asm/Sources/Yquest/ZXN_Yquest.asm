ScreenWidth32 equ 1
ScreenWidth equ 32
ScreenHeight equ 24
ScreenObjWidth equ 128-4
ScreenObjHeight equ 192-8


	macro nextreg,reg,val
		db &ED,&91,\reg,\val
	endm
	
	macro nextregA,reg
		db &ED,&92,\reg
	endm

UserRam Equ &7000


	nolist

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef vasm
		include "\SrcALL\VasmBuildCompat.asm"
	else
		read "\SrcALL\WinApeBuildCompat.asm"
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	read "\SrcALL\CPU_Compatability.asm"


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
	
	Org &8000				;Code Origin
	di
	
	call Cls
                ;IPPPSLUN
    nextreg &43,%00010000   ;Layer 2 to 1st palette
	
    nextreg &40,0           ;palette index 0
	ld b,16
	ld hl,MyPalette
paletteloop:
	ld a,(hl)				;get color (RRRGGGBB)
	inc hl
    nextregA &41           	;Send the colour 
    
    djnz paletteloop      	;Repeat for next color
	
    nextreg &16,0           ; Set X scroll to 0
	nextreg &17,0           ; Set Y scroll to 0
	
	nextreg &18,0			;X1
    nextreg &18,255			;X2
    nextreg &18,0			;Y1
    nextreg &18,191         ;Y2

    ;Enable Layer 2 (256 color screen) and make it writable
		 ;BB--P-VW		-V= visible W=Write  B=bank
	ld a,%00000011
	ld bc,&123B
	out (c),a		
		
	;nextreg &07,2			;CPU to 14mhz (0=3.5mjz 1=7mhz)
	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	


ShowTitle:
;Init Game Defaults
	ld a,3
	ld (lives),a		;Life count
	xor a
	ld (level),a		;Level number
	ld (PlayerObject),a	;Player Sprite
	call ChibiSound		;Mute sound

	call cls			;Scr Clear

;Show Title Screen	
	ld hl,TitlePic
	ld c,0
TitlePixNextY:
	ld b,0
TitlePixNextX:
	push bc		
	push hl
		ld a,(hl)		;Sprite number
		or a
		jr z,TitleNoSprite
		ld h,a
		push bc
			call GetSpriteAddr ; H=spritenum
		pop bc
		sla b	;Xpos *8
		sla b
		sla b
		sla c	;Ypos *8
		sla c
		sla c
		call ShowSprite 	;BC=XY HL=Sprnum
TitleNoSprite:
	pop hl
	inc hl
	pop bc
	inc b
	ld a,b
	cp ScreenWidth
	jr nz,TitlePixNextX	
	inc C
	ld a,c
	cp ScreenHeight
	jr nz,TitlePixNextY

		
	
	ld hl,&0D10
	call Locate
	ld hl,txtFire			;Show Fire
	call PrintString
	
	ld hl,&1202
	call Locate
	ld hl,txtUrl			;Show URL
	call PrintString

	ld hl,&1000
	call Locate
	ld hl,txtHiScore
	call PrintString
	ld de,HiScore	 		;Show the highscore
	ld b,4
	call BCD_Show

	
StartLevel:
	Call WaitForFire
	call CLS
	Call ResetPlayer
	call LevelInit
	
	ld a,0					;Dummy Key value 
	JR StartDraw

infloop:
	ld bc,100
	ld d,%11111111
PauseBC:
	push de
	push bc
		call ReadJoystick 	;KM Get Joystick... Returns ---FRLDU
	pop bc
	pop de
	
	cp %11111111
	jr z,PauseNoKey
	ld d,a
PauseNoKey:
	dec bc
	ld a,b
	or c
	jr nz,PauseBC
	ld a,d

StartDraw:
	push af
		push bc
			call DrawUi
			ld IX,PlayerObject
			call BlankSprite;Remove old player sprite
		pop bc
	pop af
	ld d,a

	ld hl,KeyTimeout
	ld a,(hl)
	or a
	jr z,ProcessKeys
	dec (hl)
	jp JoySkip			;skip player input
ProcessKeys:
	ld hl,PlayerAccY
	ld e,0				;Timeout

	bit 0,d
	jr nz,JoyNotUp		;Jump if UP not presesd
	ld a,(hl)
	sub 1				;Move Y Up the screen
	ld (hl),a
	ld e,2
JoyNotUp:
	bit 1,d
	jr nz,JoyNotDown	;Jump if DOWN not presesd
	ld a,(hl)
	add 1				;Move Y Down the screen
	ld (hl),a
	ld e,2
JoyNotDown:
	ld hl,PlayerAccX
	bit 2,d
	jr nz,JoyNotLeft 	;Jump if LEFT not presesd
	ld a,(hl)
	sub 1
	ld (hl),a			;Move X Left 
	ld e,2
JoyNotLeft:
	bit 3,d
	jr nz,JoyNotRight	;Jump if RIGHT not presesd
	ld a,(hl)
	add 1	
	ld (hl),a			;Move X Right
	ld e,2
JoyNotRight: 
	bit 5,d
	jr nz,JoyNotFire		;Jump if Fire not presesd
	call PlayerFirebullet
JoyNotFire: 
	bit 4,d
	jr nz,JoyNotFire2		;Jump if Fire2 not presesd
	xor a
	ld (PlayerAccX),a
	ld (PlayerAccY),a
JoyNotFire2: 
	ld a,e
	ld (KeyTimeout),a
JoySkip: 	
	call DrawAndMove
	jp infloop

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
PrintCharB:
	push bc
		sub 32
		ld h,a				;Sprite Source
		ld l,0
		srl h				;64 Bytes per char
		rr l
		srl h
		rr l
		ld bc,FontData		;Font Base
		add hl,bc

		ld bc,(CursorY)		;Get X and Y
		rl c				;Y*8
		rl c
		rl c
		rl b				;X*8
		rl b
		rl b
	jr ShowSpriteB
	
ShowSprite:
	push bc
	jr ShowSpriteB

DoGetSpriteObj:
	push bc
		ld h,(IX+O_SprNum)		;Sprite Source
		call GetSpriteAddr
DrawBoth:
		ld b,(IX+O_Xpos)
		sla b					;Double Xpos (1 pixel per byte)
		ld c,(IX+O_Ypos)
ShowSpriteB:
	push ix
		di
		push hl
			ld ixl,8
			call GetScreenPos		;HL=ScreenAddr
		pop hl
NextYLine:
		push de
			ld bc,8			;Width 8 bytes
			ldir			;Draw Line
		pop de
		call GetNextLine	;Move DE down a line
		dec ixl				;Next Line
		jr nz,NextYLine
	pop ix
	pop bc
	ei
	ret					;Finished 

BlankSprite:	
	ld a,(IX+O_CollProg)
	cp 250
	ret nc
	push bc
		ld hl,FontData	;Sprite Source
	jp DrawBoth

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
GetSpriteAddr:				;Sprite Source
		ld l,0				
		srl h				;64 bytes per sprite
		rr l
		srl h
		rr l
		ld bc,SpriteData
		add hl,bc
		ld a,(SpriteFrame)	;Sprite frame (1024 bytes per bank)
		rlca
		rlca
		add h
		ld h,a	
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BitmapData:
FontData:
	incbin "\ResALL\Yquest\Font_NXT.raw"

SpriteData:
	incbin "\ResALL\Yquest\ZXN_YQuest.raw"
	incbin "\ResALL\Yquest\ZXN_YQuest2.raw"
	incbin "\ResALL\Yquest\ZXN_YQuest3.raw"
	incbin "\ResALL\Yquest\ZXN_YQuest4.raw"
BitmapDataEnd:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
GetNextLine:		;Move DE down a line
	inc d
	ld a,d
	and %00111111	;63 lines per bank
	ret nz			;Return if not changed bank
	ld d,a
	ld bc,&123B
	in a,(c)		;BB---P-VW	- Page in and make visible
	add %01000000	;nNxt Bank
	out (c),a		;Set New Bank
	ret

GetScreenPos:	;return memory pos in DE of screen co-ord B,C (X,Y)
	push af
	push bc
		ld e,b	
		ld a,c
		and %00111111	;Offset in third of bank
		ld d,a
		ld a,c
		and %11000000	;Bank in correct third for &0000-&3fff
		    ;BB--P-VW		-V= visible W=Write  B=bank
		or  %00000011
		ld bc,&123B
		out (c),a		;BB---P-VW	- Page in and make visible
	pop bc
	pop af
	ret
		
	
WaitForFire:
	call DoRandom
	call ReadJoystick 		
	and %00100000		;Fire Button
	jr z,WaitForFireB

WaitForFireB:
	call DoRandom
	call ReadJoystick 		
	and %00100000		;Fire Button
	jr nz,WaitForFireB
	ret
	
ReadJoystick:
	LD  B,%01111111		;%01111111 B, N, M, Sym, Space
	;set upper address lines - note, low bit is zero
	LD  C,&FE  			;port for lines K1 - K5
	
	ld l,3
JoyInAgain:	
	in a,(c)	
	rra		;F2 - Space, F1- Enter, Right - P
	rl h
	rrc b	;%10111111 H, J, K, L  , Enter
			;%11011111 Y, U, I, O  , P
			;%11101111 6, 7, 8, 9  , 0
	dec l
	jr nz,JoyInAgain
	
	rra			;Left - O
	rl h				
	rrc b	;%11110111 5, 4, 3, 2  , 1
	rrc b	;%11111011 T, R, E, W  , Q
	
	in a,(c)	
	rra
	rl l		;U - Q (for later)
	rrc b	;%11111101 G, F, D, S  , A
	
	in a,(c)	
	rra
	rl h		;Down - A
	
	rr l		;Up   - Q
	rl h
	ld a,h
	or %10000000 ;Set unused bit
	ret
	
Cls:	
	ld bc,0			;Bank in first 3rd of screen
	call GetScreenPos	
	call ClsPart
	ld bc,&0040		;Bank in second 3rd of screen
	call GetScreenPos	
	call ClsPart
	ld bc,&0080		;Bank in final 3rd of screen
	call GetScreenPos	
	call ClsPart
	ret
	
ClsPart:
	ld c,&40		;16k block (screen is 48k)
	ld b,0
	ld hl,&0000		;&0000-&3FFF (Rom / Vram write through)
ClsPartb:
	ld (hl),0		;Zero a byte
	inc hl
	djnz  ClsPartb
	dec c
	jr nz,ClsPartb
	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

	
MyPalette:			;8 bits per color
	   ;RRRGGGBB     3 red bits, 3 green bits, 2 blue bit
    db %00000000 ;0
    db %00011100 ;1
    db %01001001 ;2
    db %10110110 ;3
    db %11111111 ;4
    db %00110001 ;5
    db %00111000 ;6
    db %11100100 ;7
    db %11101101 ;8
    db %11110101 ;9
    db %11111101 ;10
    db %10100110 ;11
    db %11100011 ;12
    db %00000111 ;13
    db %00101110 ;14
    db %00011011 ;15

	
	read "YQ_DataDefs.asm"
	read "YQ_RamDefs.asm"
	read "YQ_Multiplatform.asm"
	
	read "\SrcALL\Multiplatform_ChibiSound.asm"			;Sound Driver
	read "\SrcALL\MultiPlatform_ShowDecimal.asm"		;Show decimal numbers
	read "\SrcALL\Multiplatform_BCD.asm"				;Show Binary Coded Decimal
	;read "\SrcALL\Multiplatform_ShowHex.asm"			;Hex Display

	