;useMode0 equ 1
	org &1200		;Start of our program

	ld ixl,32		;Draw 32 pixels
	ld a,0			;AB=Xpos
	ld bc,&F000		;C=Ypos
DrawLineAgain:
	push af
		call GetPixelMask 
			;Get HL=Address E=Background Mask D=Pixel mask

		ld a,3		;Color Num
		call GetColorMaskNum

		and e		;Mask pixel color
		ld e,a		;Save for Later

		ld a,(hl)	;Get Current Background
		and d		;Keep background pixels
		or e		;Or in New Pixel
		ld (hl),a	;Update Pixel
	pop af
	inc b		;Inc Xpos
	jr nz,TestXok
	inc a		;add Xpos carry to A (Top Byte of Xpos AB )
TestXok:
	inc c		;Inc Ypos
	dec ixl
	jr nz,DrawLineAgain	

	
	
	
	ld bc,&1010		;XY Pos
	call GetScreenPos
	
	ld de,TestSprite ;Sprite Source

; Lets clone an area of the screen!

	ld ixl,&20-1	;IX=Source (H=X Y=L)
	ld iyl,&40		;IY=DEST   (H=X Y=L)
CopyYAgain:
	ld ixh,&20		;Source X
	ld iyh,&40		;Dest X
CopyXAgain:
	push ix
	pop bc
	xor a			;Top byte of Xpos
	call GetPixelMask;Get Pixel
	ld a,(hl)		
	and e
	call ByteToColorMask;Convert to color mask
	push af
		push iy
		pop bc
		xor a
		call GetPixelMask ;Get Pixel
		ld a,(hl)
		and d
		ld d,a
	pop af
	and e			;Apply Color mask to new pixel
	or d			;OR in old background
	ld (hl),a

	dec ixh
	inc iyh
	ld a,iyh
	cp &60			;end Xpos
	jr nz,CopyXAgain

	dec ixl
	inc iyl
	ld a,iyl
	cp &60			;End Ypos
	jr nz,CopyYAgain

	ret				;Finished

TestSprite:
	incbin "\ResALL\Sprites\SpriteCPC.raw"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ByteToColorMask: ;Fill whole byte with color - 
;eg %00001000 to %00001111 (when B=0)
;or %00000100 to %00001111 (when B=1)
		 ;A=Color pixel... B=pixel pos (Xpos)
	ld c,a
	ld a,b
	ifndef useMode0
		and %00000011	;4 pixels per byte in Mode 1
	else
		and %00000001	;2 pixels per byte in Mode 0
	endif
	jr z,ByteToColorMaskNoShift ;Zero pos=no shift
	ld b,a
ByteToColorMaskLeftShift:
	sla c			;Shift C B times (color to far right
	djnz ByteToColorMaskLeftShift
ByteToColorMaskNoShift:
	ld a,c
	ifndef useMode0
		rrca		;Fill all the pixels with the same color
		or c
		rrca
		or c
	endif
	rrca
	or c
	ret				;All pixels in A now the same color


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GetColorMaskNum:
	ifndef useMode0
		and %00000011	;4 colors in Mode 1
	else
		and %00001111	;16 colors in Mode 0
	endif
	push bc
		ld bc,ColorLookup ;Get Color from LUT
		add c
		ld c,a 
		ld a,(bc)
	pop bc
	ret
	


GetPixelMask:	;(AB,C) = (X,Y)
;Returns HL=Mempos... D=Mask to keep background - E= pixel to select
	push af
		push bc	
			rra		;2 pixels per byte in mode 0
			rr b
			ifndef useMode0
				rra	;4 pixels per byte in mode 1
				rr b	
			endif
			call GetScreenPos ;Get Byte Position
		pop bc
		push bc
			ld a,b
			ifndef useMode0
				and %00000011 ;4 pixels per byte in Mode 1
			else 
				and %00000001 ;2 pixels per byte in Mode 0
			endif
			ld bc,PixelBitLookup	;Lookup table of single pixel bytemask
			add c
			ld c,a 
			ld a,(bc)	;Get Pixel Mask for X position
			ld e,a
			cpl
			ld d,a	
		pop bc
	pop af
	ret 



ifndef useMode0	;Mode 1

	Align 4
PixelBitLookup:	;Pixel positions
	db %10001000,%01000100,%00100010,%00010001
ColorLookup:	;Colors (all pixels one color)
	db %00000000,%11110000,%00001111,%11111111

else		;Mode 0
	Align 16
PixelBitLookup:	;Pixel positions
	db %10101010,%01010101
ColorLookup:	;Colors (all pixels one color)
	db %00000000,%00000011,%00001100,%00001111,%00110000,%00110011,%00111100,%00111111,%11000000,%11000011,%11001100,%11001111,%11110000,%11110011,%11111100,%11111111
endif

GetScreenPos:	;return memory pos in HL of screen co-ord B,C (X,Y)
;	; Input  BC= XY (x=bytes - so 80 across)
;	; output HL= screen mem pos
;	; de is wiped
;

;Looking at Y line (in C) - we need to take each set of bits, and work with them separately:
;YYYYYYYY	Y line number (0-200)
;-----YYY	(0-7)  - this part needs to be multiplied by &0800 and added to the total
;YYYYY---	(0-31) - This part needs to be multiplied by 80 (&50)
	push de
		;screen is 80 bytes wide = -------- %01010000
		ld a,C		; 00000000 01010000
		and %11111000 	; -------- -YYYY---
		ld h,0
		ld l,a
				; 00000000 01010000
		
		sla l		; -------- YYYY----
		rl h
		
		ld d,h		;value is in first bit position -add it
		ld e,l	
				; 00000000 01010000
		sla e		; -------Y YYY-----
		rl d		;	    
		sla e		; ------YY YY------
		rl d
		
		add hl,de	;value is in 2nd bit position - add it
		;We've now effectively multiplied by 80 (&50)

	;-----YYY	(0-7)  - this part needs to be multiplied by &0800 and added to the total
		ld a,C
		and %00000111	
		
		rlca		;X8
		rlca
		rlca

		ld d,a		;Load into top byte, and add as 16 bit
		ld e,0

		add hl,de
		;We've now effectively multiplied by 8

	;Screen Base
		ld a,&C0	;Add the screen Base &C000
		ld d,a

	;X position
		ld e,B		;Add the X pos 
		add hl,de
	pop de

	ret 		;return memory location in hl
	
GetNextLine:
	push af

		ld a,h		;Add &08 to H (each CPC line is &0800 bytes below the last
		add &08
		ld h,a
			;Every 8 lines we need to jump back to the top of the memory range to get the correct line
			;The code below will check if we need to do this - yes it's annoying but that's just the way the CPC screen is!
		bit 7,h		;Change this to bit 6,h if your screen is at &8000!
		jp nz,GetNextLineDone
		push bc
			ld bc,&c050	;if we got here we need to jump back to the top of the screen - the command here will do that
			add hl,bc
		pop bc
	GetNextLineDone:
	pop af
	ret
