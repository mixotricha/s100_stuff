
	org &1200		;Start of our program

	ld hl,palette
	ld a,1
PaletteAgain;
	ld b,(hl)
	ld c,(hl)
	inc hl
	push af
	push hl
		call &BC32	;A=pen BC=Color
	pop hl
	pop af
	inc a
	cp 4
	jr nz,PaletteAgain


	ld de,&0010		;Xpos (in pixels)
	ld hl,&00C0		;Ypos (in pixels)
	call &BC1D		;Scr Dot Position - Returns address in HL
	call DrawSpriteMask	;Show Mask

	ld de,&0014		;Xpos (in pixels)
	ld hl,&00C0		;Ypos (in pixels)
	call &BC1D		;Scr Dot Position - Returns address in HL
	call DrawSpriteMask	;Show Mask

	ld de,&0018		;Xpos (in pixels)
	ld hl,&00C0		;Ypos (in pixels)
	call &BC1D		;Scr Dot Position - Returns address in HL
	call DrawSpriteMask	;Show Mask

	ret



DrawSpriteMask:
	ld IY,TestSprite	;Sprite Color Source
	ld de,TestMask		;Sprite Mask Source
	ld b,64			;Lines (Height)
SpriteNextLine:
	push hl
		ld c,8		;Bytes per line (Width)
SpriteNextByte:
		ld a,(de)	;Load in byte from Mask
		and (hl)	;Keep masked Background via AND
		or (IY)		;OR in Color from Sprite
		ld (hl),a	;Save to Screen Destination

		inc IY		;INC Source Sprite Address
		inc DE		;INC Source Mask Address
		inc hl		;INC Dest Screen Address
		
		dec c 		;Repeat for next byte
		jr nz,SpriteNextByte
	pop hl
	call &BC26	;Scr Next Line (Alter HL to move down a line)
	djnz SpriteNextLine	;Repeat for next line
	ret			;Finished

TestSprite:
	incbin "\ResALL\Sprites\SpriteCPCMaskTest_Color.RAW"

TestMask:
	incbin "\ResALL\Sprites\SpriteCPCMaskTest_Mask.RAW"



Palette:
	db 8,24,20