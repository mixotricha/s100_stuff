
	org &6500	
	xor a				;By default lynx makes a noise
	out (%10000100),a	;so we mute the noise here!
	
	ld bc,&0410			;Destination Screen address
	call GetScreenPos	
	ex de,hl
	
	ld hl,testbitmap	;Source bitmap
	;ld ixh,1			;Width in blocks (8 pixel)
	ld ixl,8			;Height in pixels
	
	exx
		ld bc,&FFFF	;needed for vram write
	exx
	
	ld iyl,e			;Backup the destination 
	ld iyh,d				;(we can't use the stack here)
	
ShowBitmapAgain:
	exx					;Red/Blue Component
		ld a,03			;%00000011 - Write Bank 1+2
		out (c),a
		ld a,&28		;%00101000 - Write VRAM, Write RedBlue
		out (&80),a		
	exx

	;DON'T USE THE STACK AFTER THIS POINT!!!
	 
	ldi			;Do Blue Component (&A000-&BFFF)
	 
	dec de		;Reset the Destination pos

	set 6,d		;Move destination to &Cxxx
	res 5,d		;Note: because of the memory map, 
					;you can actually disable this line
	
	ldi			;Do Red Component (&C000-&DFFF)

	dec de 		;Reset the Destination pos
	 
	exx					;Green Component
		ld a,05			;%00000101 - Write Bank 1+3
		out (c),a
		ld a,&24		;%00100100 - Write VRAM, Write Green
		out (&80),a
	exx
	
	ldi			;Do Green Component (&C000-&DFFF)
	 
	ld bc,&0020	;Move Down a line (32 bytes per line)	
	add iy,bc
	
	ld e,iyl	;Reset the Destination pos
	ld d,iyh
	
	dec ixl
	jr nz,ShowBitmapAgain
	
	exx					;Turn off drawing
		xor a
		out (c),a
		out (&80),a
	exx
	
	; You can use the stack again
	
	halt
	

GetScreenPos:	; BC= X byte - Y line	
	xor a
	ld h,c		;Ypos * 32
	srl h
	rra
	srl h
	rra
	srl h
	rra
	add b		;Add Xpos
	ld l,a
	
	ld a,&A0	;Screen base is &A000
	add h
	ld h,a
	ret
	
	
		
		
testbitmap:
	;   BB  RR  GG
	db &3c,&00,&00	;0
	db &7e,&00,&00	;1
	db &ff,&24,&00  ;2
	db &ff,&00,&00  ;3
	db &ff,&00,&00	;4
	db &db,&24,&00  ;5
	db &66,&18,&00  ;6
	db &3c,&00,&00  ;7
	
	