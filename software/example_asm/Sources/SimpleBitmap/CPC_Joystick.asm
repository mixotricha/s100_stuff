
	org &1200			;Start of our program
	
	ld a,0				;Force Draw of character first run
	JR StartDraw
infloop:
	call &bb24 			; KM Get Joystick... Returns ---FRLDU
	or a
	jr z,infloop		;See if no keys are pressed

StartDraw:
	push af
		ld de,(PlayerX)	;Back up X
		ld (PlayerX2),de

		ld hl,(PlayerY)	;Back up Y
		ld (PlayerY2),hl

		push hl
		push de
			call BlankPlayer ;Remove old player sprite
		pop de
		pop hl
	pop af

	bit 0,A
	jr z,JoyNotUp		;Jump if UP not presesd
	inc hl				;Move Y Up the screen
JoyNotUp:
	bit 1,A
	jr z,JoyNotDown		;Jump if DOWN not presesd
	dec hl				;Move Y Down the screen
JoyNotDown:
	bit 2,A
	jr z,JoyNotLeft 	;Jump if LEFT not presesd
	dec de				;Move X Left 
JoyNotLeft:
	bit 3,A
	jr z,JoyNotRight	;Jump if RIGHT not presesd
	inc de				;Move X Right
JoyNotRight: 
	ld (PlayerX),de		;Update X
	ld (PlayerY),hl		;Update Y
	
	;X Boundary Check - if we go <0 we will end up back at &FFFF
	ld a,d	
	cp 1				;320 / 256  =1 remainder 64
	jr c,PlayerPosXOk
	ld a,e
	cp 64-4 			;320 / 256  =1 remainder 64
	jr c,PlayerPosXOk
	jr PlayerReset		;Player out of bounds - Reset!
PlayerPosXOk:

	;Y Boundary Check - only need to check 1 byte
	ld a,l
	cp 8				;Player 8 lines tall
	jr c,PlayerReset
	cp 200
	jr c,PlayerPosYOk	;Not Out of bounds
	
PlayerReset:
	ld de,(PlayerX2) 	;Reset Xpos	
	ld (PlayerX),de	

	ld hl,(PlayerY2)	;Reset Ypos
	ld (PlayerY),hl
PlayerPosYOk:


	call DrawPlayer		;Draw Player Sprite

	ld bc,500
	call PauseBC		;Wait a bit!

	jp infloop



PauseBC:
	dec bc
	ld a,b
	or c
	jr nz,PauseBC
	ret


BlankPlayer:
	ld bc,blankSprite	;Blank Sprite source
	jr DrawSprite
DrawPlayer:
	ld bc,TestSprite	;Player Sprite Source
DrawSprite:
	push bc
		call &BC1D	;Scr Dot Position - Returns address in HL
	pop de
	ld b,8				;Lines
SpriteNextLine:
	push hl
		ld a,(de)		;Source Byte
		ld (hl),a		;Screen Destination
		inc de			;INC Source (Sprite) Address
		inc hl			;INC Dest (Screen) Address
		ld a,(de)		;Source Byte
		ld (hl),a		;Screen Destination
		inc de			;INC Source (Sprite) Address
		inc hl			;INC Dest (Screen) Address
	pop hl
	call &BC26			;Scr Next Line (Alter HL to move down a line)
	djnz SpriteNextLine	;Repeat for next line
	ret					;Finished 

TestSprite:
;Bitplane   00001111  00001111
	db %00110000,%11000000	
	db %01110000,%11100000	
	db %11110010,%11110100	
	db %11110000,%11110000	
	db %11110000,%11110000	
	db %11010010,%10110100	
	db %01100001,%01101000	
	db %00110000,%11000000

blankSprite:
	db %00000000,%00000000
	db %00000000,%00000000
	db %00000000,%00000000
	db %00000000,%00000000
	db %00000000,%00000000
	db %00000000,%00000000
	db %00000000,%00000000
	db %00000000,%00000000

;Current player pos
PlayerX: dw &10
PlayerY: dw &10

;Last player pos (For clearing sprite)
PlayerX2: dw &10
PlayerY2: dw &10
