
	org &8000			;Start of our program
	
	ld hl,&4000			;Screen RAM
	ld de,&4000+1
	ld bc,&1800-1
	ld a,255
	ld (hl),a			;Clear the Screen
	ldir
	
	ld a,0
	JR StartDraw
	
infloop:
	ld a,(&5C08)		;LASTK - Firmware interrupt handler
	or a					; sets this to last keypress
	jr z,infloop		;See if no keys are pressed

StartDraw:
	push af
		ld a,(PlayerX)	;Back up X
		ld b,a
		ld (PlayerX2),a

		ld a,(PlayerY)	;Back up Y
		ld c,a
		ld (PlayerY2),a

		push bc
			call BlankPlayer ;Remove old player sprite
		pop bc
	pop af
	ld d,a
	cp 'q'
	jr nz,JoyNotUp		;Jump if UP not presesd
	ld a,c
	sub 8
	ld c,a				;Move Y Up the screen
JoyNotUp:
	ld a,d
	cp 'a'
	jr nz,JoyNotDown	;Jump if DOWN not presesd
	ld a,c
	add 8
	ld c,a				;Move Y Down the screen
JoyNotDown:
	ld a,d
	cp 'o'
	jr nz,JoyNotLeft 	;Jump if LEFT not presesd
	dec b				;Move X Left 
JoyNotLeft:
	ld a,d
	cp 'p'
	jr nz,JoyNotRight	;Jump if RIGHT not presesd
	inc b				;Move X Right
JoyNotRight: 
	ld a,b
	ld (PlayerX),a		;Update X
	ld a,c
	ld (PlayerY),a		;Update Y
	
	;X Boundary Check - if we go <0 we will end up back at &FFFF
	
	ld a,b
	cp 32 				;32 bytes wide
	jr c,PlayerPosXOk
	jr PlayerReset 		;Player out of bounds - Reset!
PlayerPosXOk

	;Y Boundary Check - only need to check 1 byte
	ld a,c
	cp 192
	jr c,PlayerPosYOk	;Not Out of bounds
	
PlayerReset:
	ld a,(PlayerX2) 	;Reset Xpos	
	ld b,a
	ld (PlayerX),a	

	ld a,(PlayerY2) 	;Reset Ypos
	ld c,a
	ld (PlayerY),a
	
PlayerPosYOk:
	call DrawPlayer		;Draw Player Sprite
	
	ld a,0
	ld (&5C08),a		;Clear keypress buffer
	
	ld bc,500
	call PauseBC		;Wait a bit!

	jp infloop

PauseBC:
	dec bc
	ld a,b
	or c
	jr nz,PauseBC
	ret

	


BlankPlayer:
	ld hl,blankSprite	;Blank Sprite source
	jr DrawSprite
DrawPlayer:
	ld hl,TestSprite	;Player Sprite Source
DrawSprite:
	push hl
		call GetColMemPos	; Do color for block
		ld a,%00000011
		ld (de),a		
	pop hl
	
	push hl
		call GetScreenPos	;Get Screen Memory pos
	pop hl
	
	ld b,8				;Lines

SpriteNextLine:
	ld a,(hl)			;Source Byte
	ld (de),a			;Screen Destination
	inc hl				;INC Source (Sprite) Address
	
	call GetNextLine	;Scr Next Line (Alter HL to move down a line)

	djnz SpriteNextLine	;Repeat for next line

	ret					;Finished 

	
;Smiley face
TestSprite:				;1 bit per pixel
	db %00111100	
	db %01111110	
	db %11011011	
	db %11111111	
	db %11111111	
	db %11011011	
	db %01100110	
	db %00111100

blankSprite:
	db %00000000
	db %00000000
	db %00000000
	db %00000000
	db %00000000
	db %00000000
	db %00000000
	db %00000000

;Current player pos
PlayerX: db &10
PlayerY: db &10

;Last player pos (For clearing sprite)
PlayerX2: db &10
PlayerY2: db &10

	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
;	_0 _1 _0 Y7 Y6 Y2 Y1 Y0   Y5 Y4 Y3 X4 X3 X2 X1 X0

;	; Input  BC= XY (x=bytes - so 32 across)
;	; output HL= screen mem pos
GetScreenPos:	;return memory pos in HL of screen co-ord B,C (X,Y)
	ld a,c
	and %00111000
	rlca
	rlca
	or b
	ld e,a
	ld a,c
	and %00000111
	ld d,a
	ld a,c
	and %11000000
	rrca
	rrca
	rrca
	or d
	or  &40				;&4000 screen base
	ld d,a
	ret

GetNextLine:			;Move HL down one line
	inc d
	ld a,d
	and   %00000111		;See if we're over the first 3rd
	ret nz
	ld a,e
	add a,%00100000
	ld e,a
	ret c				;See if we're over the 2'nd 3rd
	ld a,d
	sub   %00001000
	ld d,a
	ret

	
; Input  BC= XY (x=bytes - so 32 across)
; output HL= screen mem pos
GetColMemPos:			;YYYYYyyy 	Color ram is in 8x8 tiles 
	ld a,C							;so low three Y bits are ignored
		and %11000000	;YY------
		rlca
		rlca			;------YY
		add &58 		;5800 =color ram base
		ld d,a
	ld a,C
	and %00111000		;--YYY---
	rlca
	rlca				;YYY-----
	
	add b				;Add Xpos
	ld e,a
	ret
