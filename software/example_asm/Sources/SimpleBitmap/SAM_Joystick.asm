	Org &8000			;Code Origin
	ld sp,&BFFF			;Define a stack pointer
	
		; MSSBBBBB	- M=Midi io / S=Screen mode / B=video Bank
	ld a,%01101110
	out (252),a			;VMPR - Video Memory Page Register (252 dec)
	
	ld a,0
	JR StartDraw
	
infloop:
	call &0169			;JREADKEY - Read keyboard, flush butter 
	or a					;Key in A
	jr z,infloop		;See if no keys are pressed

StartDraw:
	push af
		ld a,(PlayerX)	;Back up X
		ld b,a
		ld (PlayerX2),a

		ld a,(PlayerY)	;Back up Y
		ld c,a
		ld (PlayerY2),a

		push bc
			call BlankPlayer ;Remove old player sprite
		pop bc
	pop af
	
	ld d,a
	cp 'q'
	jr nz,JoyNotUp		;Jump if UP not presesd
	ld a,c
	sub 2
	ld c,a				;Move Y Up the screen
JoyNotUp:
	ld a,d
	cp 'a'
	jr nz,JoyNotDown	;Jump if DOWN not presesd
	ld a,c
	add 2
	ld c,a				;Move Y Down the screen
JoyNotDown:
	ld a,d
	cp 'o'
	jr nz,JoyNotLeft 	;Jump if LEFT not presesd
	dec b				;Move X Left 
JoyNotLeft:
	ld a,d
	cp 'p'
	jr nz,JoyNotRight	;Jump if RIGHT not presesd
	inc b				;Move X Right
JoyNotRight: 

	ld a,b
	ld (PlayerX),a		;Update X
	ld a,c
	ld (PlayerY),a		;Update Y
	
;X Boundary Check - if we go <0 we will end up back at &FF
	ld a,b
	cp 128-4
	jr c,PlayerPosXOk
	jr PlayerReset 		; Player out of bounds - Reset!
PlayerPosXOk:

;Y Boundary Check - only need to check 1 byte
	ld a,c
	cp 192-8
	jr c,PlayerPosYOk	;Not Out of bounds
	
PlayerReset:
	ld a,(PlayerX2) 	;Reset Xpos	
	ld b,a
	ld (PlayerX),a	

	ld a,(PlayerY2) 	;Reset Ypos
	ld c,a
	ld (PlayerY),a
	
PlayerPosYOk:
	call DrawPlayer		;Draw Player Sprite
	
	ld bc,2000
	call PauseBC		;Wait a bit!

	jp infloop

PauseBC:
	dec bc
	ld a,b
	or c
	jr nz,PauseBC
	ret

;Current player pos
PlayerX: db &10
PlayerY: db &10

;Last player pos (For clearing sprite)
PlayerX2: db &10
PlayerY2: db &10


BlankPlayer:
	ld hl,blankSprite	;Blank Sprite source
	jr DrawSprite
DrawPlayer:
	ld hl,TestSprite	;Player Sprite Source
DrawSprite:
	di	;The Sam Screen is 24k, and we need to move it in at &0000-&7FFF
			;we must keep interrupts disabled the whole time

	call GetScreenPos	;Calculate ram position
	
	ld ixl,8			;Height of bitmap
	
	
		; WRrBBBBB W=Write protect Bank (&0000-&3FFF) / r=ram in low area 
	ld a,%00101110  			;/ R=rom in high area / B=low ram Bank
	out (250),a			;LMPR - Low Memory Page Register (250 dec) 
drawnextline:
	push de
		ld bc,4			;Width of bitmap in bytes
		ldir
	pop de
	call GetNextLine	;Move down a line
	
	dec ixl				;Repeat for next line
	jr nz,drawnextline
	
		; WRrBBBBB W=Write protect Bank (&0000-&3FFF) / r=ram in low area 
	ld a,%00011111  			;/ R=rom in high area / B=low ram Bank
	out (250),a			;Turn on Rom 0 again
	;ei	
	ret
	
GetScreenPos:			;return mempos in DE of screen co-ord B,C (X,Y)
	ld d,c	
	xor a	
	rr d				;Effectively Multiply Ypos by 128
	rra 
	or b				;Add Xpos
	ld e,a	
	ret

GetNextLine:			;Move DE down 1 line 
	ld a,&80	
	add e				;Add 128 to mem position 
	ld e,a
	ret nc
	inc d				;Add any carry
	ret
	
	
TestSprite:			;Smiley face (8x8) - 1 nibble per color
	db &00,&11,&11,&00
	db &01,&11,&11,&10
	db &11,&31,&13,&11
	db &11,&11,&11,&11
	db &11,&11,&11,&11
	db &11,&21,&12,&11
	db &01,&12,&21,&10
	db &00,&11,&11,&00
	
blankSprite:		;Empty 8x8 Sprite
	db &00,&00,&00,&00
	db &00,&00,&00,&00
	db &00,&00,&00,&00
	db &00,&00,&00,&00
	db &00,&00,&00,&00
	db &00,&00,&00,&00
	db &00,&00,&00,&00
	db &00,&00,&00,&00