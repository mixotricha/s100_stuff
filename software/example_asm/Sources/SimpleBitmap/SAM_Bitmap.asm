
	Org &8000		;Code Origin

	di	;The Sam Screen is 24k, and we need to move it in at &0000-&7FFF
		;we must keep interrupts disabled the whole time
	
	ld sp,&BFFF		;Define a stack pointer
	
	
		; MSSBBBBB	- M=Midi io / S=Screen mode / B=video Bank
	ld a,%01101110
	out (252),a		;VMPR - Video Memory Page Register (252 dec)
		
		; WRrBBBBB W=Write protect Bank (&0000-&3FFF) / r=ram in low area 
	ld a,%00101110  			;/ R=rom in high area / B=low ram Bank
	out (250),a		;LMPR - Low Memory Page Register (250 dec) 
	
	
	
	ld bc,&1010			;XY position
	call GetScreenPos	;Calculate ram position
	
	ld ixl,8			;Height of bitmap
	
	ld hl,sprite		;Source bitmap data
drawnextline:
	push de
		ld bc,4			;Width of bitmap in bytes
		ldir
	pop de
	call GetNextLine	;Move down a line
	
	dec ixl				;Repeat for next line
	jr nz,drawnextline
	
		; WRrBBBBB W=Write protect Bank (&0000-&3FFF) / r=ram in low area 
	ld a,%00011111  					;/ R=rom in high area / B=low ram Bank
	out (250),a		;Turn on Rom 0 again
		
	DI		
	halt			;Stop processor so we can see result
	
	
GetScreenPos:	;return mempos in DE of screen co-ord B,C (X,Y)
	ld d,c	
	xor a	
	rr d		;Effectively Multiply Ypos by 128
	rra 
	or b		;Add Xpos
	ld e,a	
	ret

GetNextLine:	;Move DE down 1 line 
	ld a,&80	
	add e		;Add 128 to mem position 
	ld e,a
	ret nc
	inc d		;Add any carry
	ret
	
	
Sprite:			;Smiley face (8x8) - 1 nibble per color
	db &00,&11,&11,&00
	db &01,&11,&11,&10
	db &11,&31,&13,&11
	db &11,&11,&11,&11
	db &11,&11,&11,&11
	db &11,&21,&12,&11
	db &01,&12,&21,&10
	db &00,&11,&11,&00

