
	org &6500	
	xor a				;By default lynx makes a noise
	out (%10000100),a	;so we mute the noise here!

	ld a,0					;Clear Player Pos
	JR StartDraw			;Need A=0
	
infloop:
	;in a,(%11111010)	;Read in from joystick 
							;(JoyDoesn't work on emu?)
	call ReadCursors	
	cp %11111111			
	jr z,infloop		;See if no keys are pressed
	
StartDraw:
	push af
		ld a,(PlayerX)	;Back up X
		ld b,a
		ld (PlayerX2),a

		ld a,(PlayerY)	;Back up Y
		ld c,a
		ld (PlayerY2),a
		push bc
			call BlankPlayer ;Remove old player sprite
		pop bc
	pop af
	
	ld d,a
	;   ----RLDU
	cp %11111110
	jr nz,JoyNotUp		;Jump if UP not presesd
	ld a,c				;Move Y Up the screen
	sub 8
	ld c,a
JoyNotUp:
	ld a,d
	cp %11111101
	jr nz,JoyNotDown	;Jump if DOWN not presesd
	ld a,c				;Move Y Down the screen
	add 8
	ld c,a
JoyNotDown:
	ld a,d
	cp %11111011
	jr nz,JoyNotLeft 	;Jump if LEFT not presesd
	dec b				;Move X Left 
JoyNotLeft:
	ld a,d
	cp %11110111
	jr nz,JoyNotRight	;Jump if RIGHT not presesd
	inc b				;Move X Right
JoyNotRight: 

	ld a,b
	ld (PlayerX),a		;Update X
	ld a,c
	ld (PlayerY),a		;Update Y
	
;X Boundary Check - if we go <0 we will end up back at &FF
	
	ld a,b
	cp 32 		
	jr c,PlayerPosXOk
	jr PlayerReset		;Player out of bounds - Reset!
PlayerPosXOk

	;Y Boundary Check - only need to check 1 byte
	ld a,c
	cp 256-8;192
	jr c,PlayerPosYOk	;Not Out of bounds
	
PlayerReset:
	ld a,(PlayerX2) 	;Reset Xpos	
	ld b,a
	ld (PlayerX),a	

	ld a,(PlayerY2) 	;Reset Ypos
	ld c,a
	ld (PlayerY),a
	
PlayerPosYOk:
	call DrawPlayer		;Draw Player Sprite

	ld bc,&1000
	call PauseBC		;Wait a bit!

	jp infloop

PauseBC:
	dec bc
	ld a,b
	or c
	jr nz,PauseBC
	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	

ReadCursors:		;returns %----RLDU
	ld bc,&0080
	in a,(c)	;Shift Esc Down Up ShLck - - 1
	rrca
	rrca
	rrca
	rrca	
	and %00000011	;%------DU
	ld d,a
	
	ld e,0
	ld bc,&0980
	in a,(c)	;- - right - enter left ] delete
	rla
	rla
	rla
	rl e
	rla
	rla
	rla
	rl e
	rl e
	rl e			;%----RL--
	ld a,e
	or d			
	or %11110000	;Set unused bits
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
BlankPlayer:	
	ld hl,blankbitmap	;Source bitmap	
	jr DrawBoth
DrawPlayer:	
	ld hl,testbitmap	;Source bitmap
DrawBoth:
	push hl
		call GetScreenPos	
		ex de,hl
	pop hl
	
	;ld ixh,1			;Width in blocks (8 pixel)
	ld ixl,8			;Height in pixels
	
	exx
		ld bc,&FFFF	;needed for vram write
	exx
	
	ld iyl,e			;Backup the destination 
	ld iyh,d				;(we can't use the stack here)
	
ShowBitmapAgain:
	exx					;Red/Blue Component
		ld a,03			;%00000011 - Write Bank 1+2
		out (c),a
		ld a,&28		;%00101000 - Write VRAM, Write RedBlue
		out (&80),a		
	exx

	;DON'T USE THE STACK AFTER THIS POINT!!!
	 
	ldi			;Do Blue Component (&A000-&BFFF)
	 
	dec de		;Reset the Destination pos

	set 6,d		;Move destination to &Cxxx
	res 5,d		;Note: because of the memory map, 
					;you can actually disable this line
	
	ldi			;Do Red Component (&C000-&DFFF)

	dec de 		;Reset the Destination pos
	 
	exx					;Green Component
		ld a,05			;%00000101 - Write Bank 1+3
		out (c),a
		ld a,&24		;%00100100 - Write VRAM, Write Green
		out (&80),a
	exx
	
	ldi			;Do Green Component (&C000-&DFFF)
	 
	ld bc,&0020	;Move Down a line (32 bytes per line)	
	add iy,bc
	
	ld e,iyl	;Reset the Destination pos
	ld d,iyh
	
	dec ixl
	jr nz,ShowBitmapAgain
	
	exx					;Turn off drawing
		xor a
		out (c),a
		out (&80),a
	exx
	
	; You can use the stack again
	ret
	

GetScreenPos:	; BC= X byte - Y line	
	xor a
	ld h,c		;Ypos * 32
	srl h
	rra
	srl h
	rra
	srl h
	rra
	add b		;Add Xpos
	ld l,a
	
	ld a,&A0	;Screen base is &A000
	add h
	ld h,a
	ret


PlayerX:	db 0	;Current player pos
PlayerY:	db 0

PlayerX2:	db 0	;Last player pos 
PlayerY2:	db 0		;(For clearing sprite)

	
		
blankbitmap:
	db &00,&00,&00  
	db &00,&00,&00  
	db &00,&00,&00  
	db &00,&00,&00  
	db &00,&00,&00  
	db &00,&00,&00  
	db &00,&00,&00  
	db &00,&00,&00  	
			
testbitmap:
	;   BB  RR  GG
	db &3c,&00,&00	;0
	db &7e,&00,&00	;1
	db &ff,&24,&00  ;2
	db &ff,&00,&00  ;3
	db &ff,&00,&00	;4
	db &db,&24,&00  ;5
	db &66,&18,&00  ;6
	db &3c,&00,&00  ;7
	
	