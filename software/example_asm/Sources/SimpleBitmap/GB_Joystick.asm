
;"The eight gameboy buttons/direction keys are arranged in form of a 2x4 matrix. Select either button or direction keys by writing to this register, then read-out bit 0-3.
  ;Bit 7 - Not used
  ;Bit 6 - Not used
  ;Bit 5 - P15 Select Button Keys      (0=Select)
  ;Bit 4 - P14 Select Direction Keys   (0=Select)
  ;Bit 3 - P13 Input Down  or Start    (0=Pressed) (Read Only)
  ;Bit 2 - P12 Input Up    or Select   (0=Pressed) (Read Only)
;  Bit 1 - P11 Input Left  or Button B (0=Pressed) (Read Only)
 ; Bit 0 - P10 Input Right or Button A (0=Pressed) (Read Only)




PlayerX 	equ &C010	;Current player pos
PlayerY 	equ &C011

PlayerX2 	equ &C012	;Last player pos 
PlayerY2	equ &C013	;(For clearing sprite)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Header												
	org &0000				;RST 0-7
	org &0040				;Interrupt: Vblank
		reti				
	org &0048				;Interrupt:	LCD-Stat
		reti
	org &0050				;Interrupt: Timer
		reti
	org &0058				;Interrupt: Serial
		reti
	org &0060				;Interrupt:Joypad
		reti
	org &0100
	nop			;0100	0103	Entry point (start of program)
	jp	begin
	;0104-0133	Nintendo logo (must match rom logo)	
	db &CE,&ED,&66,&66,&CC,&0D,&00,&0B,&03,&73,&00,&83,&00,&0C,&00,&0D
	db &00,&08,&11,&1F,&88,&89,&00,&0E,&DC,&CC,&6E,&E6,&DD,&DD,&D9,&99
	db &BB,&BB,&67,&63,&6E,&0E,&EC,&CC,&DD,&DC,&99,&9F,&BB,&B9,&33,&3E
	db "CHIBIAKUMAS.COM" ;0134-0142	Game Name (UCASE)
	ifdef BuildGBC 
		db &80		;0143 		Color gameboy flag (&80 = GB+CGB,&C0 = CGB only)
	else 
		db &00
	endif
	db 0,0       	;0144-0145	Game Manufacturer code
	db 0         	;0146		Super GameBoy flag (&00=normal, &03=SGB)
	db 2	  	  	;0147		Cartridge type (special upgrade hardware) 
	db 2        	;0148		Rom size (0=32k, 1=64k,2=128k etc)
	db 3         	;0149		Cart Ram size (0=none,1=2k 2=8k, 3=32k)
	db 1         	;014A		Destination Code (0=JPN 1=EU/US)
	db &33       	;014B		Old Licensee code (must be &33 for SGB)
	db 0         	;014C		Rom Version Number (usually 0)
	db 0         	;014D		Header Checksum - ‘ones complement' checksum of bytes 0134-014C… (not needed for emulators)
	dw 0         	;014E-014F	Global Checksum – 16 bit sum of all rom bytes (except 014E-014F)… unused by gameboy

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Start of code												
	 
begin:
	nop						;No-op for safety!
	di
	ld	sp, &ffff		; set the stack pointer to highest mem location + 1
	
	xor a					;We're going to reset the tilemap pos	
	ld hl,&FF42				
	ldi	(hl), a				;FF42: SCY - Tile Scroll Y
	ld	(hl), a				;FF43: SCX - Tile Scroll X
	
StopLCD_wait:				;Turn off the screen so we can define our patterns
	ld      a,(&FF44)		;Loop until we are in VBlank
	cp      145             ;Is display on scan line 145 yet?
	jr      nz,StopLCD_wait ;no? keep waiting!
	
	ld      hl,&FF40		;LCDC - LCD Control (R/W)
	res     7,(hl)      	;Turn off the screen
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Define bitmap						
	ld	de, 128*16+&8000 	;$8000 is the start of Tile Ram , 16 bytes per Tile
	ld	hl, SpriteData		;Source of Tile images
	ld	bc, SpriteDataEnd-SpriteData	;Length
	call DefineTiles

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Define palette												
		
	ifdef BuildGBC
		ld c,0*8		;palette no 0 (back)
		call SetGBCPalettes
		
	else
		ld a,%00011011		;DDCCBBAA .... A=Background 3=Black, =White
		ld hl,&FF47
		ldi (hl),a			;FF47 	BGP	BG & Window Palette Data  (R/W)	= &FC
		ldi (hl),a			;FF48  	OBP0	Object Palette 0 Data (R/W)	= &FF
		cpl					;Set sprite Palette 2 to the opposite
		ldi (hl),a			;FF49  	OBP1	Object Palette 1 Data (R/W)	= &FF
	endif
		
	ld      hl,&FF40		;LCDC - LCD Control (R/W)	EWwBbOoC 
    set     7,(hl)          ;Turn on the screen
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Our program													


	ld a,0					;Clear Player Pos
	ld (PlayerX),a
	ld (PlayerY),a
	
	JR StartDraw			;Force Draw - Need A=0 (fake input)
	
infloop:
	ld a,%11101111 	;Select UDLR
	ld (&FF00),a
	
	ld a,(&FF00)	;Read in Keys
	or %11110000
	cp 255
	jr z,infloop		;See if no keys are pressed
	
StartDraw:
	push af
		ld a,(PlayerX)	;Back up X
		ld b,a
		ld (PlayerX2),a

		ld a,(PlayerY)	;Back up Y
		ld c,a
		ld (PlayerY2),a

		push bc
			call BlankPlayer ;Remove old player sprite
		pop bc
	pop af
	
	ld d,a
	cp %11111011
	jr nz,JoyNotUp		;Jump if UP not presesd
	dec c				;Move Y Up the screen
JoyNotUp:
	ld a,d
	cp %11110111
	jr nz,JoyNotDown	;Jump if DOWN not presesd
	inc c				;Move Y Down the screen
JoyNotDown:
	ld a,d
	cp %11111101
	jr nz,JoyNotLeft 	;Jump if LEFT not presesd
	dec b				;Move X Left 
JoyNotLeft:
	ld a,d
	cp %11111110
	jr nz,JoyNotRight	;Jump if RIGHT not presesd
	inc b				;Move X Right
JoyNotRight: 
	ld a,b
	ld (PlayerX),a		;Update X
	ld a,c
	ld (PlayerY),a		;Update Y
	
;X Boundary Check - if we go <0 we will end up back at &FFFF
	ld a,b
	cp 20		
	jr c,PlayerPosXOk
	jr PlayerReset		;Player out of bounds - Reset!
PlayerPosXOk

;Y Boundary Check - only need to check 1 byte
	ld a,c
	cp 18
	jr c,PlayerPosYOk	;Not Out of bounds
	
PlayerReset:
	ld a,(PlayerX2) 	;Reset Xpos	
	ld b,a
	ld (PlayerX),a	

	ld a,(PlayerY2) 	;Reset Ypos
	ld c,a
	ld (PlayerY),a
	
PlayerPosYOk:
	call DrawPlayer		;Draw Player Sprite

	ld bc,5000
	call PauseBC		;Wait a bit!

	jp infloop

PauseBC:
	dec bc
	ld a,b
	or c
	jr nz,PauseBC
	ret
	
BlankPlayer:	
	ld a,129
	jr DrawBoth
DrawPlayer:	
	ld a,128
DrawBoth:
	push af
		call GetVDPScreenPos	;Calculate Tile VRAM Pos
		
		call LCDWait			;Wait for LCD
	pop af
	
	ld (hl),a				;Set tile to 128
	
	ifdef BuildGBC
		ld bc,&FF4F	;VBK - CGB Mode Only - VRAM Bank
		ld a,1		;Turn on GBC extras
		ld (bc),a					
		ld (hl),0	;Set Palette
		xor a		;Turn off GBC extras
		ld (bc),a			
	endif
	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
LCDWait:
	push af
        di
LCDWaitAgain
        ld a,(&FF41)  		;STAT - LCD Status (R/W)
			;-LOVHCMM
        and %00000010		;MM=video mode (0/1 =Vram available)  		
        jr nz,LCDWaitAgain 
    pop af	
	ret
 

SetGBCPalettes:
	ifdef BuildGBC
		ld hl,GBPal		
SetGBCPalettesb:
		ldi a,(hl)  	;GGGRRRRR
		ld e,a
		ldi a,(hl)  	;xBBBBBGG
		ld d,a
		inc a 			;cp 255
		ret z
		push hl
			call lcdwait ;Wait for VDP Sync
			ld hl,&ff68	
			ld (hl),c	;FF68 - BCPS/BGPI - CGB Mode Only - Background Palette Index
			inc hl			
			ld (hl),e	;FF69 - BCPD/BGPD - CGB Mode Only - Background Palette Data
			dec hl		
			inc	c		;Increase palette address
			ld (hl),c	;FF68 - BCPS/BGPI - CGB Mode Only - Background Palette Index
			inc hl		
			ld (hl),d	;FF69 - BCPD/BGPD - CGB Mode Only - Background Palette Data
			inc c		;Increase palette address
		pop hl
		jr SetGBCPalettesb
	endif

;		 	xBBBBBGGGGGRRRRR
GBPal:	dw %0111110000000000	;col 0
		dw %0111111111100000	;col 1
		dw %0000000000011111	;col 2
		dw %0000001111111111	;col 3
		dw %1111111111111111	;End of list
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
GetVDPScreenPos:;Move to a memory address with BC... B=Xpos, C=Ypos
	xor a
	ld h,c		;Ypos*32
	rr h		;Each line is 32 tiles
	rra			;and each tile is 1 byte
	rr h
	rra
	rr h
	rra
	or b		;Add XPOS
	ld l,a
	ld a,h
	add &98		;The tilemap starts at &9800
	ld h,a
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
DefineTiles:	;Write BC bytes from HL to DE for tile definition
	call LCDWait	;Wait for VDP Sync
	ldi a,(hl)		;LD a,(hl), inc HL
	ld (de),a			
	inc de
	dec bc
	ld a,b
	or c
	jr nz,DefineTiles
	ret
	
	
SpriteData:
; Bitplane  00000000  11111111
        DB %00111100,%00000000     ;  0
        DB %01111110,%00000000     ;  1
        DB %11111111,%00100100     ;  2
        DB %11111111,%00000000     ;  3
        DB %11111111,%00000000     ;  4
        DB %11011011,%00100100     ;  5
        DB %01100110,%00011000     ;  6
        DB %00111100,%00000000     ;  7
Blankata:
		DB %00000000,%00000000     ;  7
SpriteDataEnd: