
	org &1200		;Start of our program

	ld de,&0010		;Xpos (in pixels)
	ld hl,&00A0		;Ypos (in pixels)

	call &BC1D		;Scr Dot Position - Returns address in HL

	ld de,TestSprite	;Sprite Source
	ld b,8			;Lines

SpriteNextLine:
	push hl
		ld a,(de)	;Source Byte
		ld (hl),a	;Screen Destination

		inc de		;INC Source (Sprite) Address
		inc hl		;INC Dest (Screen) Address

		ld a,(de)	;Source Byte
		ld (hl),a	;Screen Destination

		inc de		;INC Source (Sprite) Address
		inc hl		;INC Dest (Screen) Address
	pop hl
	call &BC26		;Scr Next Line (Alter HL to move down a line)

	djnz SpriteNextLine	;Repeat for next line

	ret			;Finished 


TestSprite:
;Bitplane   00001111  00001111
	db %00110000,%11000000	
	db %01110000,%11100000	
	db %11110010,%11110100	
	db %11110000,%11110000	
	db %11110000,%11110000	
	db %11010010,%10110100	
	db %01100001,%01101000	
	db %00110000,%11000000

