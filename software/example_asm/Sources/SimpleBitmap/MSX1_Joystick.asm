

PlayerX 	equ &C010	;Current player pos
PlayerY 	equ &C011

PlayerX2 	equ &C012	;Last player pos 
PlayerY2	equ &C013		;(For clearing sprite)


;GTSTCK
; Address  : #00D5
; Function : Returns the joystick status
; Input    : A  - Joystick number to test (0 = cursors, 1 = port 1, 2 = port 2)
; Output   : A  - Direction
; Registers: All
;  \U/
;  L+R
;  /D\
;

; GTTRIG
; Address  : #00D8
; Function : Returns current trigger status
; Input    : A  - trigger button to test
           ; 0 = spacebar
           ; 1 = port 1, button A
           ; 2 = port 2, button A
           ; 3 = port 1, button B
           ; 4 = port 2, button B
; Output   : A  - #00 trigger button not pressed
                ; #FF trigger button pressed
; Registers: AF



;For Cartridge	
	org &4000				;Base Cart Address
	db "AB"					;Fixed Header
	dw ProgramStart 		;Pointer to start of program
	db 00,00,00,00,00,00	;Unused
	
	;Effectively Code starts at address &400A
	

VdpOut_Data equ &98			;For Data writes
VdpOut_Control equ &99		;For Reg settings /Selecting Dest addr in VRAM

ProgramStart:				;Program Code Starts Here

	;Set up our screen
	ld c,VdpOut_Control
	ld b,VDPScreenInitData_End-VDPScreenInitData
	ld hl,VDPScreenInitData
	otir	
	
	
	
	ld hl,&0000+128*8		;Define Tiles 128+ (8 Bytes per tile)
	call VDP_SetWriteAddress
	
	ld hl,TestSprite		;Copy Tile pixel Data
	ld b,TestSprite_END-TestSprite	;Bytes
	otir					;C=VdpOut_Data
	
	ld hl,&2000+128*8		;Define Tile Palette 128+ (8 Bytes per tile)
	call VDP_SetWriteAddress
	
	ld hl,TestSpritePalette ;Copy Tile Palette Data
	ld b,TestSpritePalette_END-TestSpritePalette;Bytes
	otir					;C=VdpOut_Data
	
	
	ld a,0					;Clear Player Pos
	ld (PlayerX),a
	ld (PlayerY),a
	
	JR StartDraw			;Need A=0
	
infloop:
	ld a,1				;Joystick 1 (0=keyboard 1=joy1 2=joy2)
	call &00D5			;GTSTCK - Get Keypress
	or a			
	jr z,infloop		;See if no keys are pressed
StartDraw:
	push af
		ld a,(PlayerX)	;Back up X
		ld b,a
		ld (PlayerX2),a

		ld a,(PlayerY)	;Back up Y
		ld c,a
		ld (PlayerY2),a

		push bc
			call BlankPlayer ;Remove old player sprite
		pop bc
	pop af
	
	ld d,a
	cp 1
	jr nz,JoyNotUp		;Jump if UP not presesd
	dec c				;Move Y Up the screen
JoyNotUp:
	ld a,d
	cp 5
	jr nz,JoyNotDown	;Jump if DOWN not presesd
	inc c				;Move Y Down the screen
JoyNotDown:
	ld a,d
	cp 7
	jr nz,JoyNotLeft 	;Jump if LEFT not presesd
	dec b				;Move X Left 
JoyNotLeft:
	ld a,d
	cp 3
	jr nz,JoyNotRight	;Jump if RIGHT not presesd
	inc b				;Move X Right
JoyNotRight: 

	ld a,b
	ld (PlayerX),a		;Update X
	ld a,c
	ld (PlayerY),a		;Update Y
	
;X Boundary Check - if we go <0 we will end up back at &FFFF
	
	ld a,b
	cp 32 		
	jr c,PlayerPosXOk
	jr PlayerReset		;Player out of bounds - Reset!
PlayerPosXOk

	;Y Boundary Check - only need to check 1 byte
	ld a,c
	cp 24
	jr c,PlayerPosYOk	;Not Out of bounds
	
PlayerReset:
	ld a,(PlayerX2) 	;Reset Xpos	
	ld b,a
	ld (PlayerX),a	

	ld a,(PlayerY2) 	;Reset Ypos
	ld c,a
	ld (PlayerY),a
	
PlayerPosYOk:
	call DrawPlayer		;Draw Player Sprite

	ld bc,5000
	call PauseBC		;Wait a bit!

	jp infloop

PauseBC:
	dec bc
	ld a,b
	or c
	jr nz,PauseBC
	ret

	
	
BlankPlayer:
	ld a,129		;Blank Tile
	jr DrawOther
DrawPlayer:
	ld a,128		;Smiley Tile
DrawOther:
	push af
		call GetVDPScreenPos
	Pop af
	out (c),a
	ret
	
TestSprite:			;1 bit per pixel smiley
	db %00111100	
	db %01111110	
	db %11011011	
	db %11111111	
	db %11111111	
	db %11011011	
	db %01100110	
	db %00111100
BlankSprite:		;1 bit per pixel Blank
	db %00000000
	db %00000000
	db %00000000	
	db %00000000	
	db %00000000	
	db %00000000	
	db %00000000	
	db %00000000
TestSprite_END:		
	
	
	
	
TestSpritePalette:
	;   FB 		=Fore		Back	
	db &A0		;DarkYellow	Black
	db &A0		;DarkYellow	Black
	db &BC		;Yellow		DarkGreen
	db &BC		;Yellow		DarkGreen
	db &BC		;Yellow		DarkGreen
	db &BC		;Yellow		DarkGreen
	db &AC		;DarkYellow	DarkGreen
	db &A0		;DarkYellow	Black
BlankSpritePalette	
	db &A0		;DarkYellow	Black
	db &A0		;DarkYellow	Black
	db &A0		;DarkYellow	Black
	db &A0		;DarkYellow	Black
	db &A0		;DarkYellow	Black
	db &A0		;DarkYellow	Black
	db &A0		;DarkYellow	Black
	db &A0		;DarkYellow	Black
TestSpritePalette_END:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

VDPScreenInitData:
	;	Value   ,Register
	db %00000010,128+0	;mode register #0
	db %01100000,128+1	;mode register #1
	db %10011111,128+3	;colour table (LOW)
	db %00000000,128+4	;pattern generator table
	db %00110110,128+5	;sprite attribute table (LOW)
	db %00000111,128+6	;sprite pattern generator table
	db %11110000,128+7	;border colour/character colour at text mode
VDPScreenInitData_End:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
GetVDPScreenPos:	;Move the VDP write pointer to a memory location by XY location
		ld h,c		;B=Xpos (0-31), C=Ypos (0-23)
		xor a
		
		srl h	;32 bytes per line, so shift L left 5 times, and push any overflow into H
		rr a
		srl h
		rr a
		srl h
		rr a
		
		or b	;Or in the X co-ordinate
		ld l,a
		
		ld a,h
		or &18	;Tilemap starts at &1800
		ld h,a
		call VDP_SetWriteAddress
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

VDP_SetWriteAddress:		;Set VRAM address next write will occur
	ld a, l
    out (VdpOut_Control), a	;Send L byte
    ld a, h
    or %01000000			;Set WRITE (0=read)
    out (VdpOut_Control), a	;Send H  byte
	
	ld c,VdpOut_Data		;Set C to data Write Addr
	ret            
	
	