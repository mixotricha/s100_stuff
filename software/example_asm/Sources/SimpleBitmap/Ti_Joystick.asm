
TI_LCD_COMM equ &10	;Commands to change LCD settings
TI_LCD_DATA equ &11 

	org &9D93
	db &BB,&6d			;Header
	;Program actually starts at 9D95

	di
	rst 40				;BCALL
	dw &4570			;_RunIndicOff - Hide Busy icon
	
	call &000B			;_LCD_BUSY_QUICK - Wait LCD to process
	
	ld a,&01			;8 bit per char
	call SendByte
	
	ld a,&07			;Set AutoINC 'Y'
	call SendByte	;(what the TI TFT calls Y everyone else calls X!)

	
	
	ld a,0					;Clear Player Pos
	JR StartDraw			;Need A=0
	
infloop:
	;in a,(%11111010)
	;call ReadCursors
	ld a,6
	ld c,1
	out (c),a		;Select port
	in	a,(c)		;read line
	cp %11111111			
	jr z,infloop		;See if no keys are pressed
StartDraw:
	push af
		ld a,(PlayerX)	;Back up X
		ld d,a
		ld (PlayerX2),a

		ld a,(PlayerY)	;Back up Y
		ld e,a
		ld (PlayerY2),a
		push de
			call BlankPlayer ;Remove old player sprite
		pop de
	pop af
	ld b,a
	cp %11110111
	jr nz,JoyNotUp		;Jump if UP not presesd
	ld a,e				;Move Y Up the screen
	sub 8
	ld e,a
JoyNotUp:
	ld a,b
	cp %11111110
	jr nz,JoyNotDown	;Jump if DOWN not presesd
	ld a,e				;Move Y Down the screen
	add 8
	ld e,a
JoyNotDown:
	ld a,b
	cp %11111101
	jr nz,JoyNotLeft 	;Jump if LEFT not presesd
	dec d				;Move X Left 
JoyNotLeft:
	ld a,b
	cp %11111011
	jr nz,JoyNotRight	;Jump if RIGHT not presesd
	inc d				;Move X Right
JoyNotRight: 

	ld a,d
	ld (PlayerX),a		;Update X
	ld a,e
	ld (PlayerY),a		;Update Y
	
;X Boundary Check - if we go <0 we will end up back at &FFFF
	
	ld a,d
	cp 12 		
	jr c,PlayerPosXOk
	jr PlayerReset		;Player out of bounds - Reset!
PlayerPosXOk

	;Y Boundary Check - only need to check 1 byte
	ld a,e
	cp 64
	jr c,PlayerPosYOk	;Not Out of bounds
	
PlayerReset:
	ld a,(PlayerX2) 	;Reset Xpos	
	ld d,a
	ld (PlayerX),a	

	ld a,(PlayerY2) 	;Reset Ypos
	ld e,a
	ld (PlayerY),a
	
PlayerPosYOk:
	call DrawPlayer		;Draw Player Sprite

	ld bc,&A000
	call PauseBC		;Wait a bit!

	jp infloop

PauseBC:
	dec bc
	ld a,b
	or c
	jr nz,PauseBC
	ret
	

PlayerX:	db 0	;Current player pos
PlayerY:	db 0

PlayerX2:	db 0	;Last player pos 
PlayerY2:	db 0		;(For clearing sprite)

	
	;ld de,&0505 		;VH pos (Horizontal / Vertical)
BlankPlayer:
	ld hl,BlankSprite
	jr DrawBoth
DrawPlayer:	
	ld hl,TestSprite	;Sprite source
DrawBoth:	
	ld b,8				;Sprite Lines
nextline:	
	call GetScreenPos	;Select Ram Pos

	ld a,(hl)			;Read a byte
	inc hl
	out (TI_LCD_DATA),a	;send the byte to the GPU
	call &000B 			;_LCD_BUSY_QUICK - Delay
	inc e				;update Vertical Pos
	djnz nextline
		
	ret 				;return 
	

TestSprite:			;1 bit per pixel Bitmap
	db %00111100	
	db %01111110	
	db %11011011	
	db %11111111	
	db %11111111	
	db %11011011	
	db %01100110	
	db %00111100

BlankSprite:			;1 bit per pixel Bitmap
	db %00000000
	db %00000000
	db %00000000
	db %00000000
	db %00000000
	db %00000000
	db %00000000
	db %00000000	
	
GetScreenPos:	;return memory pos in HL of screen co-ord E,D (X,Y)
	
		ld a,&80			;Set ROW
		add e
		call SendByte
		ld a,&20			;Set COL
		add d
SendByte:
		out (TI_LCD_COMM),a ;Send Data to LCD
		JP &000B			;_LCD_BUSY_QUICK - Wait LCD to process
	



