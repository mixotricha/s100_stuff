
TI_LCD_COMM equ &10	;Commands to change LCD settings
TI_LCD_DATA equ &11 

	org &9D93
	db &BB,&6d			;Header
	;Program actually starts at 9D95

	di
	rst 40				;BCALL
	dw &4570			;_RunIndicOff - Hide Busy icon
	
	call &000B			;_LCD_BUSY_QUICK - Wait LCD to process
	
	ld a,&01			;8 bits per char
	call SendByte
	
	ld a,&07			;Set AutoINC 'Y'
	call SendByte	;(what the TI TFT calls Y everyone else calls X!)

	ld de,&0505 		;VH pos (Horizontal / Vertical)
	ld hl,TestSprite	;Sprite source
	ld b,48				;Sprite Lines
nextline:	
	call GetScreenPos

	ld c,6				;Bytecount
nextByte:
	ld a,(hl)
	inc hl
	out (TI_LCD_DATA),a	;send the byte to the GPU
	call &000B 			;_LCD_BUSY_QUICK - Delay
	
	dec c
	jr nz,nextByte		;Repeat for Xbyte
		
	inc e				;update Vertical Pos
	djnz nextline		;Repeat for Yline
	
	di					;Stop CPU
	halt
	
	ret 				;return to basic
	

TestSprite:		;1 bit per pixel
		;incbin "\ResALL\Sprites\RawZX.RAW"

        DB &00,&20,&02,&00,&00,&00
        DB &00,&20,&01,&00,&00,&80
        DB &00,&20,&01,&00,&02,&80
        DB &00,&A0,&09,&00,&02,&80
        DB &00,&A0,&1F,&00,&03,&80
        DB &01,&80,&60,&E0,&07,&C0
        DB &01,&48,&48,&10,&16,&C0
        DB &01,&08,&97,&28,&1F,&40
        DB &02,&19,&00,&A6,&1F,&A0
        DB &00,&0A,&0C,&02,&1F,&80
        DB &02,&0A,&20,&01,&BF,&A0
        DB &2A,&0C,&1B,&88,&BF,&90
        DB &01,&06,&BF,&F0,&FF,&A0
        DB &00,&83,&FF,&CE,&FF,&80
        DB &00,&03,&8E,&0F,&FF,&A0
        DB &40,&83,&B6,&DF,&FF,&90
        DB &2A,&40,&F6,&FE,&BF,&00
        DB &0F,&08,&FF,&FC,&BE,&11
        DB &1E,&48,&FF,&BC,&F8,&21
        DB &1E,&20,&78,&FA,&F2,&11
        DB &1E,&FC,&7C,&FF,&C0,&23
        DB &1C,&7F,&DD,&F3,&D0,&17
        DB &10,&9F,&87,&E3,&40,&20
        DB &21,&03,&04,&41,&50,&10
        DB &00,&09,&02,&83,&00,&20
        DB &21,&00,&E9,&3F,&40,&40
        DB &02,&80,&71,&6D,&E8,&60
        DB &20,&04,&A3,&A8,&54,&C0
        DB &42,&84,&21,&22,&62,&C1
        DB &00,&05,&40,&11,&60,&81
        DB &41,&8C,&4C,&10,&21,&81
        DB &00,&A6,&43,&10,&26,&81
        DB &41,&C4,&80,&08,&20,&81
        DB &00,&44,&80,&48,&20,&01
        DB &01,&44,&98,&88,&40,&12
        DB &00,&40,&40,&10,&00,&29
        DB &01,&00,&7F,&E0,&23,&65
        DB &40,&80,&78,&E0,&23,&C3
        DB &40,&00,&30,&71,&0B,&41
        DB &00,&40,&70,&71,&9F,&03
        DB &01,&20,&78,&70,&DF,&83
        DB &44,&00,&78,&F0,&5F,&93
        DB &09,&10,&78,&FC,&1F,&20
        DB &10,&00,&F8,&FC,&3E,&20
        DB &08,&11,&F0,&7C,&40,&20
        DB &00,&09,&E0,&3C,&40,&88
        DB &64,&01,&E0,&1C,&5F,&FF
        DB &52,&04,&C0,&00,&D2,&04
	
	
	
GetScreenPos:	;return memory pos in HL of screen co-ord E,D (X,Y)
	
		ld a,&80			;Set ROW
		add e
		call SendByte
		ld a,&20			;Set COL
		add d
SendByte:
		out (TI_LCD_COMM),a ;Send Data to LCD
		JP &000B			;_LCD_BUSY_QUICK - Wait LCD to process
	



