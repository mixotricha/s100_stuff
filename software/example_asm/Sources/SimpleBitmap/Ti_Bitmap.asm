
TI_LCD_COMM equ &10	;Commands to change LCD settings
TI_LCD_DATA equ &11 

	org &9D93
	db &BB,&6d			;Header
	;Program actually starts at 9D95

	di
	rst 40				;BCALL
	dw &4570			;_RunIndicOff - Hide Busy icon
	
	call &000B			;_LCD_BUSY_QUICK - Wait LCD to process
	
	ld a,&01			;8 bit per char
	call SendByte
	
	ld a,&07			;Set AutoINC 'Y'
	call SendByte	;(what the TI TFT calls Y everyone else calls X!)

	ld de,&0505 		;VH pos (Horizontal / Vertical)
	ld hl,TestSprite	;Sprite source
	ld b,8				;Sprite Lines
nextline:	
	call GetScreenPos	;Select Ram Pos

	ld a,(hl)			;Read a byte
	inc hl
	out (TI_LCD_DATA),a	;send the byte to the GPU
	call &000B 			;_LCD_BUSY_QUICK - Delay
	inc e				;update Vertical Pos
	djnz nextline
	
	di					;Stop CPU
	halt
	
	ret 				;return to basic
	

TestSprite:			;1 bit per pixel Bitmap
	db %00111100	
	db %01111110	
	db %11011011	
	db %11111111	
	db %11111111	
	db %11011011	
	db %01100110	
	db %00111100

	
	
GetScreenPos:	;return memory pos in HL of screen co-ord E,D (X,Y)
	
		ld a,&80			;Set ROW
		add e
		call SendByte
		ld a,&20			;Set COL
		add d
SendByte:
		out (TI_LCD_COMM),a ;Send Data to LCD
		JP &000B			;_LCD_BUSY_QUICK - Wait LCD to process
	



