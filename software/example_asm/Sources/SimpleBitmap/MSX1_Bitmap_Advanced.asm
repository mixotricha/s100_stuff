
;For Cartridge	
	org &4000				;Base Cart Address
	db "AB"					;Fixed Header
	dw ProgramStart 		;Pointer to start of program
	db 00,00,00,00,00,00	;Unused

	
	;;;Effectively Code starts at address &400A
	

VdpOut_Data equ &98			;For Data writes
VdpOut_Control equ &99		;For Reg settings /Selecting Dest addr in VRAM

ProgramStart:			;Program Code Starts Here

	;Set up our screen
	ld c,VdpOut_Control
	ld b,VDPScreenInitData_End-VDPScreenInitData
	ld hl,VDPScreenInitData
	otir	
	
	
	ld hl,&0000+128*8		;Define Tiles 128+ (8 Bytes per tile)
	call VDP_SetWriteAddress

	ld hl,TestSprite		;Copy Tile pixel Data
	ld de,TestSpriteEnd-TestSprite
	call OutiDE

	
	ld hl,&2000+128*8		;Define Tile Palette 128+ (8 Bytes per tile)
	call VDP_SetWriteAddress
	
	ld hl,TestSpritePalette ;Copy Tile Palette Data
	ld de,TestSpritePalette_End-TestSpritePalette
	call OutiDE
	
	
	ld bc,&0808				;Position (X,Y)
	ld hl,&0606				;Size (W,H)
	ld de,&0000+128			;First Tile Num
	
	call FillAreaWithTiles	;Draw Tiles as a grid
		
	DI					;Can't RET on Cartridge
	Halt 	

	
	
	
TestSprite:			;1 bit per pixel smiley
	incbin "\ResALL\Sprites\RawMSX1.RAW"
TestSpriteEnd:

TestSpritePalette:
	ds 6*8,&60		;Line 1=DarkRed
	ds 6*8,&80		;Line 2=Red
	ds 6*8,&90		;Line 3=Pink
	ds 6*8,&90		;Line 4=Pink
	ds 6*8,&80		;Line 5=Red
	ds 6*8,&60		;Line 6=DarkRed
TestSpritePalette_End:	


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

VDPScreenInitData:
	db %00000010,128+0	;mode register #0
	db %01100000,128+1	;mode register #1
	db %10011111,128+3	;colour table (LOW)
	db %00000000,128+4	;pattern generator table
	db %00110110,128+5	;sprite attribute table (LOW)
	db %00000111,128+6	;sprite pattern generator table
	db %11110000,128+7	;border colour/character colour at text mode
VDPScreenInitData_End:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

OutiDE:
	outi		;Send a byte from HL to OUT (C)
	dec de
	ld a,d		;Repeat until DE=0
	or e
	jr nz,OutiDE
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
GetVDPScreenPos:	;Move the VDP write pointer to a memory location by XY location
		ld h,c		;B=Xpos (0-31), C=Ypos (0-23)
		xor a
		
		srl h	;32 bytes per line, so shift L left 5 times, and push any overflow into H
		rr a
		srl h
		rr a
		srl h
		rr a
		
		or b	;Or in the X co-ordinate
		ld l,a
		
		ld a,h
		or &18	;Tilemap starts at &1800
		ld h,a
		call VDP_SetWriteAddress
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

VDP_SetWriteAddress:					;Set VRAM address next write will occur
	ld a, l
    out (VdpOut_Control), a		;Send L byte
    ld a, h
    or %01000000				;Set WRITE (0=read)
    out (VdpOut_Control), a		;Send H  byte
	
	ld c,VdpOut_Data			;Set C to data Write Addr
	ret            
	
;Fill an area with consecutively numbered tiles, so we can simulate a bitmap area
;BC = X,Y	HL = W,H	E = Start Tile	
FillAreaWithTiles:	
FillAreaWithTiles_Yagain:
	push bc
		push hl
			call GetVDPScreenPos	;Set Dest Ram pos
		pop hl
		push hl
FillAreaWithTiles_Xagain:
			out (c),e		;Write Tile num
			inc e			;Inc tile
			dec h			;Are we at end of line?
			jr nz,FillAreaWithTiles_Xagain
		pop hl
	pop bc
	inc c					;Move down a line
	dec l					;Are we done?
	jr nz,FillAreaWithTiles_Yagain
	ret