
	org &1200		;Start of our program

	ld de,&0010		;Xpos (in pixels)
	ld hl,&00A0		;Ypos (in pixels)

	call &BC1D		;Scr Dot Position - Returns address in HL

	ld de,TestSprite	;Sprite Source

	ld b,48			;Lines (Height)
SpriteNextLine:
	push hl
		ld c,12		;Bytes per line (Width)
SpriteNextByte:
		ld a,(de)	;Source Byte
		ld (hl),a	;Screen Destination

		inc de		;INC Source (Sprite) Address
		inc hl		;INC Dest (Screen) Address

		dec c 		;Repeat for next byte
		jr nz,SpriteNextByte
	pop hl
	call &BC26		;Scr Next Line (Alter HL to move down a line)
	djnz SpriteNextLine	;Repeat for next line

	ret			;Finished

TestSprite:
	incbin "\ResALL\Sprites\SpriteCPC.raw"
