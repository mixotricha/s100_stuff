
	Org &100		;Code Origin

	
	
	
	di	;The Sam Screen is 24k, and we need to move it in at &0000-&7FFF
		;we must keep interrupts disabled the whole time
		
		; WRrBBBBB W=Write protect Bank (&0000-&3FFF) / r=ram in low area 
	ld a,%00100000 					;/ R=rom in high area / B=low ram Bank
	out (250),a		;Turn on Rom 0 again
	
	
	ld hl,&8000
	ld de,&100
	ld bc,&4000
	ldir 
	
	jp Start
Start:	
	
	ld sp,&8000		;Define a stack pointer
	
	
	
		; MSSBBBBB	- M=Midi io / S=Screen mode / B=video Bank
	ld a,%01101110
	out (252),a		;VMPR - Video Memory Page Register (252 dec)
		
		; WRrBBBBB W=Write protect Bank (&0000-&3FFF) / r=ram in low area 
	ld a,%00101110  			;/ R=rom in high area / B=low ram Bank
	out (251),a		;LMPR - Low Memory Page Register (250 dec) 
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	


	ld a,(NewPosX+1)
	ld b,a
	ld a,(NewPosY+1)
	ld c,a
	
	call ShowSprite		;Remove old cursor
ShowAgain:	
	push bc
		call ReadMouse
	pop bc

	ld a,(FireDown)
	or a
	jr z,NoFire
	
;Fire is down - show our test sprite at current location	

	ld ix,&1830
	ld de,SprChibiko
	call ShowSpriteB	;Show Chibiko

	xor a
	ld (FireDown),a		;Clear Fire
NoFire:

;Update the Cursor

	ld a,(OldPosX+1)	;Last Joystick pos
	ld b,a
	ld a,(OldPosY+1)
	ld c,a

	ld a,(NewPosX+1)	;New Joystick pos
	ld h,a
	ld a,(NewPosY+1)
	ld l,a

	or a
	sbc hl,bc			;See if Cursor pos changed
	jr z,ShowAgain
	
	ld a,(OldPosX+1)
	ld b,a
	ld a,(OldPosY+1)
	ld c,a

	call ShowSprite		;Remove old cursor

	ld hl,(NewPosX)		;Update Cursor pos
	ld de,(NewPosY)
	ld (OldPosX),hl
	ld (OldPosY),de

	ld b,h
	ld c,d
	push bc
		call ShowSprite	;Show New Cursor
	pop bc
	
	
	ld a,(FireHeld)
	or a
	jr z,MoFireHeld		;Check if fire is still down

;Fire is Held - show our test sprite at current location	
	ld ix,&0408
	ld de,SprDrag
	call ShowSpriteB	
MoFireHeld:

	jp ShowAgain
	
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ShowSprite:			;Show Mouse Cursor
	ld de,SprCursor ;Sprite Source
	ld ix,&0408
	
ShowSpriteB:		;Show Alt Sprite
	call GetScreenPos	;HL=ScreenAddr
NextYLine:
	push hl
		ld b,ixh			;Width
NextByte:
		ld a,(de)
		xor (hl)
		ld (hl),a
		inc hl
		inc de
		djnz NextByte
	pop hl
	call GetNextLine	;Move DE down a line
	dec ixl				;Next Line
	jr nz,NextYLine
	ret
	
GetScreenPos:	;return mempos in DE of screen co-ord B,C (X,Y)
	ld h,c	
	xor a	
	rr h		;Effectively Multiply Ypos by 128
	rra 
	or b		;Add Xpos
	ld l,a	
	ld a,&80
	add h
	ld h,a
	ret

GetNextLine:	;Move DE down 1 line 
	ld a,&80	
	add l		;Add 128 to mem position 
	ld l,a
	ret nc
	inc h		;Add any carry
	ret
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

DoAxis:
		in a,(c)
		and %00001111
		ld d,a
		in a,(c)
		and %00001111
		rlca
		rlca
		rlca
		rlca
		ld e,a
		in a,(c)
		and %00001111
		or e
		ld e,a
	ret
	
	
ReadMouse:	


	
	ld bc,&FFFE
	in a,(c)
	in a,(c)
	in a,(c)
	push af				;Buttons
		call DoAxis		;Xaxis
		ex de,hl
		call DoAxis		;Yaxis	
	pop af
	
	
	push af
	
		   ;%-----RML
		and %00000001
		jr nz,JoyNotFire
		ld a,(FireHeld)
		or a
		jr nz,JoyNotFireB
		ld a,1
		ld (FireDown),a		;Set FireDown/Held
		ld (FireHeld),a
		jr JoyNotFireB
JoyNotFire:
		xor a		
		ld (FireHeld),a		;Release Fire Held
JoyNotFireB:
	pop af
	
	ld d,e
	ld a,l
	neg
	ld e,a
	
	ld bc,0
	ld b,d
	ld hl,(NewPosX)
	
	ld a,d				;D=Xpos
	or a
	jp m,MouseLeft
	
;MouseRight
	add hl,bc
	jr c,MouseUPDOWN
	ld a,h
	cp 128-7			;Over right of screen?
	jr nc,MouseUPDOWN
	
	ld (NewPosX),hl
	jr MouseUPDOWN
	
MouseLeft:	
	add hl,bc
	jr nc,MouseUPDOWN	;Over left of screen?
	ld (NewPosX),hl
		
MouseUPDOWN:			
	ld b,e
	ld hl,(NewPosY)
	
	ld a,e				;E=Ypos
	or a
	jp m,MouseUp
	
;MouseDown
	add hl,bc
	ret c
	ld a,h
	cp 192-7			;At bottom of screen?
	ret nc

	ld (NewPosY),hl
	ret
	
MouseUp:	
	add hl,bc	
	ret nc				;Over top of screen?
	ld (NewPosY),hl
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

OldPosX: dw 00		;Last Mouse position
OldPosY: dw 00
NewPosX: dw 00		;Current Mouse position
NewPosY: dw 00

FireHeld: db 0		;Mouse Left is Held
FireDown: db 0		;Mouse Left was pressed (not processed yet)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SprChibiko:	
	incbin "\ResALL\Sprites\RawMSX.RAW"
SprChibiko_End:

SprCursor:
	db &00,&03,&30,&00
	db &00,&03,&30,&00
	db &00,&03,&30,&00
	db &33,&30,&03,&33
	db &33,&30,&03,&33
	db &00,&03,&30,&00
	db &00,&03,&30,&00
	db &00,&03,&30,&00
SprCursor_End:

SprDrag:
	db &00,&00,&00,&00
	db &00,&00,&00,&00
	db &00,&03,&30,&00
	db &00,&33,&33,&00
	db &00,&33,&33,&00
	db &00,&03,&30,&00
	db &00,&00,&00,&00
	db &00,&00,&00,&00
SprDrag_End: