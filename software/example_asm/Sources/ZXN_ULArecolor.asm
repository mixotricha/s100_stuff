	;Unrem this if building with vasm
	include "\SrcALL\VasmBuildCompat.asm"

	include "\SrcZXN\ZXN_V1_Z80_Extensions.asm"
	include "\SrcZX\ZX_V1_header.asm"
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;			Classic Spectrum Screen
	
	ld hl,SampleScreen		;Source 
	ld de,&4000				;Destination VRAM
	ld bc,SampleScreen_End-SampleScreen	;Length
	ldir
	
	xor a
	out (&fe),a				;border=0
	
	
	call DoPause
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;			ULA PLUS Recolor Test
	
	
	nextreg &14,%11101110	;Transparency Color
	
	;Load ULA 16 color Palette
    nextreg &40,0           ;palette index 0 (ULA Fore)
	ld b,16					;Entries
	ld hl,MyPalette			;Source Definition
	call DefinePalettes	
	
    nextreg &40,16          ;palette index 16 (ULA Back)
	ld b,16					;Entries
	ld hl,MyPalette			;Source Definition
	call DefinePalettes	
	
	call DoPause
	
	nextreg &1A,32         ;Window - X1
	nextreg &1A,128        ;Window - X2
	nextreg &1A,32         ;Window - Y1
	nextreg &1A,128        ;Window - Y2
	
	ld a,0
ScrollAgain:; According to documentation this should scroll
	ei			; the ULA, but it doesn't seem to work!
	halt
	inc a
	nextregA &32			;Offset X
	nextregA &33			;Offset Y
	jp ScrollAgain
	
DoPause:
	ld b,100
PauseAgain:	
	ei
	halt
	djnz PauseAgain			;Wait a while before we enable the ULA+ Palette
	
	ret	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;		
	;8 bit palette definition

DefinePalettes:				
	ld a,(hl)				;get color (RRRGGGBB)
	inc hl
    nextregA &41           	;Send the colour 
    djnz DefinePalettes    	;Repeat for next color	
	ret
	

MyPalette:		
	   ;RRRGGGBB
	db %00000000	;0-0  Black
	db %00000001	;0-1  Blue
	db %10000000	;0-2  Red
	db %01100001	;0-3  Magenta
	db %00001001	;0-4  Green
	db %00010011	;0-5  Cyan
	db %01110100	;0-6  Yellow
	db %01101101	;0-7  White
	
	db %00000000	;B-0  Bright Black (!)
	db %00100010	;B-1  Bright Blue
	db %11100000	;B-2  Bright Red
	db %11100011	;B-3  Bright Magenta
	db %00011100	;B-4  Bright Green
	db %00011111	;B-5  Bright Cyan
	db %11110100	;B-6  Bright Yellow
	db %11111111	;B-7  Bright White
	
SampleScreen:					;Spectrum 1bpp Screen Data
	incbin "\ResALL\Sprites\ZXTestScr.scr"	
SampleScreen_End:	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	