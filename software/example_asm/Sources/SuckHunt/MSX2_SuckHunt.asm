Enable_Speech equ 1
Enable_Intro equ 1
Use3DTilemaps equ 1		;Use 3D tilemaps for 2d game over

VramYpos_Cursors equ 768
VramYpos_Title equ 768+8
VramYpos_SuckOfDeath equ 640
VramYpos_Intro equ 900

Akuyou_MusicPos equ &F800
Akuyou_SfxPos equ &F800
ArkosRam  equ &F700

BuildMSX2 equ 1
LOG_IMP	 	  equ %00000000
LOG_XOR 	  equ %00000011

GameRam equ &E000

GunSprite equ 1

VscreenMinX equ 64		;Top left of visible screen in logical co-ordinates
VscreenMinY equ 0

VscreenWid equ 128		;Screen Size in logical units
VscreenHei equ 96
VscreenWidClip equ 0

ScreenSize256 equ 1
pixelsPerByte2 equ 1 	;Use full logical res

ScreenObjWidth equ 80-12
ScreenObjHeight equ 200-48
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef vasm
		include "\SrcALL\VasmBuildCompat.asm"
	else
		read "\SrcALL\WinApeBuildCompat.asm"
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	read "\SrcALL\CPU_Compatability.asm"

	Macro DoEI
		ei
	endm
	macro DoDI
		di
	endm
	
SlotExpansion equ &C000

FirmwareSlotRestoreA equ &C004 
FirmwareSlotRestoreB equ &C005

SlotRestoreA equ &C006
SlotRestoreB equ &C007
FullRamA equ &C008
FullRamB equ &C009
		
SXpos equ &D000
SYpos equ &D002
MaxYpos equ &D004
NextYpos equ &D006
NextXpos equ &D008
SpriteHClipPlus2 equ &D00A
SXCrop equ &D00C
SpriteDataHL equ &D00E
CurrentMode3D equ &D010
KeyMode equ &D011
KeyIgnore equ &D012
SpriteMSXData equ &D100 
FontMSXData equ &D0F0 
VdpIn_Status equ &99
VdpOut_Control equ &99
VdpOut_Indirect equ &9B
Vdp_SendByteData equ &9B	
VdpOut_Palette equ &9A

;For Cartridge	
	org &4000				;Base Cart Address
	db "AB"					;Fixed Header
	dw ProgramStart 		;Pointer to start of program
	db 00,00,00,00,00,00	;Unused

	;;;Effectively Code starts at address &400A

ProgramStart:			;Program Code Starts Here
	di
	
	call BankSwapper_INIT
	
	ld h,2
	ld l,1
	ld d,2
	call SetBank	; H=Bank  L=Slot  D=subslot .... 	

	
	
	;Initialize screen
	ld c,VdpOut_Control	
	ld b,VDPScreenInitData_End-VDPScreenInitData
	ld hl,VDPScreenInitData
	otir
	
	xor a
	ld (KeyMode),a
	call setpalette
	call cls
	
	ld a,2									;Page in Game Bank
	call BankSwapper_SetCartBank2
	
	
	ld de,GameRam
	ld bc,GameRamEnd-GameRam-1
	call Cldir								;Zero ram
	

	ld a,1
	ld (PaletteNum),a
	ld a,3
	ld (Depth3D),a
	ld hl,1
	ld (CursorMoveSpeed),hl
	
	
	ld hl,DefChar							;Char routine sprite
	ld de,SprChar
	ld bc,16
	ldir
	
	
	
	ld hl,&FD9Ah			;Firmware Interrupt handler
	ld a,&C3		;JP
	ld (hl),a
	inc hl
	ld de,InterruptHandler
	ld (hl),e
	inc hl
	ld (hl),d
	
	
	ld a,1
	ld (CurrentMode3D),a
	
	ld hl,256
	ld (SYpos),hl
	
;Send Sprite data from CPU to VRAM
	ld hl,SuckofDeath3D
	ld a,(hl)		;First byte of sprite data
	inc hl
	push hl
		ld ix,0	;Xpos
		ld iy,VramYpos_SuckOfDeath 	;Ypos
		ld hl,256	;Width
		ld de,40	;Height
		call VDP_HMMC_Generated_ViaStack	;Init sprite send
	pop hl
		
	ld bc,SuckofDeath3D_End-SuckofDeath3D-1	;Length of sprite
	call UploadSprite16Color				;Send rest of sprite
	
	
	ld a,4							;Page in Sprite Bank
	call BankSwapper_SetCartBank2

	
	
;Upload Fonts
	ld ix,0 				
	ld iy,SourceFont		
	call UploadOneSpriteAlt		;Special upload of Font
	
	ld hl,FontMSXData
	ld (SpriteDataHL),hl		;ShowSprite Source Data pos

	
;Upload Main Sprites (32 sprites)
	ld a,0
UploadNext:	
	push af
		call UploadOneSprite	;Send a game sprite
	pop af
	inc a
	cp 32
	jr nz,UploadNext

	
;Cursors (Not 3D)
;Send Sprite data from CPU to VRAM
	ld hl,SprCursorSprite-&4000
	ld a,(hl)					;First byte of sprite data
	inc hl
	push hl
		ld ix,0					;Xpos
		ld iy,VramYpos_Cursors	;Ypos
		ld hl,32				;Width
		ld de,8					;Height
		call VDP_HMMC_Generated_ViaStack
	pop hl
		
	ld bc,SprCursorSprite_End-SprCursorSprite-1	;Length of Sprite
	call UploadSprite16Color					;Send raw data

	
		
	ld a,6									;Page in Title Bank
	call BankSwapper_SetCartBank2
		
;Send Sprite data from CPU to VRAM
	ld hl,TitleTiles3D-&8000
	ld a,(hl)		;First byte of sprite data
	inc hl
	push hl
		ld ix,0	;Xpos
		ld iy,VramYpos_Title ;776	;Ypos
		ld hl,256	;Width
		ld de,104	;Height
		call VDP_HMMC_Generated_ViaStack
	pop hl
		
	ld bc,TitleTiles3D_End-TitleTiles3D-1	;Length of prite	
	call UploadSprite16Color
	;ld a,'C'
	;call PrintChar
	
	ld a,2									;Page in Game Bank
	call BankSwapper_SetCartBank2
	

	
	
	
;Send Sprite data from CPU to VRAM
	ld hl,IntroGraphics
	call DoByte1
	and %00110011
	
	push hl
		ld ix,0	;Xpos
		ld iy,VramYpos_Intro	;Ypos
		ld hl,256	;Width
		ld de,104	;Height
		call VDP_HMMC_Generated_ViaStack
	pop hl
		
	ld d,64
	ld e,104
	jr NextByte2H
NextLineHalf:
	ld d,64
NextByteH:	
	push de
		call DoByte1
	pop de
	and %00110011
	out (Vdp_SendByteData),a	;Send next byte to vdp
	
NextByte2H:
	push de
		call DoByte2
	pop de
	and %00110011
	out (Vdp_SendByteData),a	;Send next byte to vdp
	dec d
	jr nz,NextByteH

	bit 0,e
	jr nz,NextByteHEven	
	push de
		ld de,-64
		add hl,de			;Redo last line
	pop de
NextByteHEven:	
	dec e
	jr nz,NextLineHalf
	
	
ShowIntroAgain:	
	call DoPalFadeOutQ				;Fade to black

	ifdef Enable_Intro
		ld hl,SongIntro
		call Arkos_ChangeSong		;intro song
			
		ld b,4
		ld ix,IntroAnimList
DrawIntroAnim:
		push bc
			call cls
			doei

			ld a,(ix+0)		;X
			ld iyh,a
			ld a,(ix+1)		;Y
			ld e,(ix+3)
			ld d,(ix+4)
			ex de,hl
			push iy
			push ix
			push af
				;HL=Tilemap BC=Tile Data IYH=X	A=Y 	;E=Height ;D=Wid
				call	ShowTileMapAltPosQtrIntro	
				ld a,(ix+2)
				
				call DoPalFadeIn
				doei
				ld bc,90
				call PauseABC	
			pop af
			pop ix
			pop iy

			push ix
				ld e,(ix+5)
				ld d,(ix+6)
				ex de,hl
				;HL=Tilemap BC=Tile Data IYH=X	A=Y 	;E=Height ;D=Wid
				call	ShowTileMapAltPosQtrIntro	
				doei
				ld bc,200
				call PauseABC	
				ld a,(ix+2)
				call DoPalFadeOut
			pop ix
			ld bc,7
			add ix,bc

		pop bc
		djnz DrawIntroAnim
	endif

	call cls
	doei	
	
	ld a,1						;Play Speech
DrawTitleScreen:
	ld sp,&FFFF					;Reset Stack pointer

	push af
		call cls				;Clear Screen
		
		call SetPaletteBlack	;Hide while drawing

		ld a,(Depth3D)
		or a
		jr z,ShowTitle2D

		ld a,(PaletteNum)		;3D palette
		jr ShowTitle

ShowTitle2D:
	ld a,4						;2D palette (One eye only)
ShowTitle:
	push af	
			dodi
			
			ld hl,TitleTileMap3D
			ld bc,VramYpos_Title	;Ypos of Bitmap Data
		
			Call ShowTileMap		;Disable title
	
			call DoPalFadeInQ
			call DoSet3D
		pop af
		call SetPaletteB

		ld a,0
		ld hl,&1C17
		call LocateXYZ				;3D Locate (for hiscore)

		ld de,HiScore		
		ld b,4
		call BCD_Show				;Show Digits of score

		ld de,&1C16
		ld hl,txtHiScore
		call LocateandPrint			;Show Text

		call PrintKeyMode			;Show Key/Mouse state
	pop af
	
	ifdef Enable_Speech
		or a
		jr z,NoSpeech				;Only play speech after intro

		ld bc,600
		call PauseABC				;Title screen delay

		ld de,wavedataEnd-wavedata
		ld hl,wavedata
		dodi
		call ChibiWave				;Play speech "Operation Suck"

		ld bc,500
		call PauseABC				;After speech delay
	endif

NoSpeech:
	ld hl,SongTitle
	call Arkos_ChangeSong			;Title screen song
	doei
	ld bc,4000						;Delay until reshow intro
	
TitleWait:
	call ReadMouse					
	ei
	halt
	
	ifdef Enable_Intro
		dec bc
		ld a,b
		or c
		jp z,ShowIntroAgain			;Reshow intro?
	endif

	push bc
		ld h,4
		call ReadKeyRow				;Toggle Mouse/JoyKey
		bit 0,h
		jr nz,NoKeyChange
		call PrintKeyMode
		ld a,(KeyMode)
		inc a
		cp 3
		jr c,KeyModeOK				;K= Toggle
		xor a
KeyModeOK:
		ld (KeyMode),a
		call PrintKeyMode			;Show new key mode
		ei
		halt
		halt
		halt
		halt
NoKeyChange:	
	
		ld h,3
		call ReadKeyRow				;C
		bit 0,h
		call z,ChangePalette		;Toggle 3D colors 

		ld h,0
		call ReadKeyRow				;3
		bit 3,h						
		jr nz,No3DChangeTitle		;Switch 3D On/Off

		ld a,(Depth3D)
		inc a
		and %00000001
		ld (Depth3D),a
	pop bc
	xor a
	jp DrawTitleScreen				;Hard mode (level 128)

No3DChangeTitle:
		ld h,%10111111
		call ReadKeyRow				;H 
		ld a,128
		bit 4,h
		jr z,StartGameHard
		ei
	pop bc
	ld a,(FireDown)				;Fire=Start
	or a
	jr z,TitleWait				;Disable Wait

		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;StartGame
	xor a						;Start at level 0
StartGameHard:
	ld (LevelNum),a				;Start at alt level

	ld hl,SongGame
	call Arkos_ChangeSong		;Start main song
	doei

	ld hl,NewGameData
	ld de,OldPosX
	ld bc,NewGameData_End-NewGameData
	ldir						;Reset game settings

StartNewLevel:
	call InitLevel
	call RePaintScreen
	call DoPalFadeInQ
	
		

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Main Loop
ShowAgain:	
	push bc
		call ReadMouse			;Read Input
	pop bc
	
	call UpdateSoundSequence

	ld h,4
	call ReadKeyRow				;Check for P
	bit 5,h
	jr nz,NoPause
	call PLY_Stop				;Paused: Stop music
	di
	ld bc,500
	call PauseABC				;Delay
	
DoPause:
	ld h,4
	call ReadKeyRow				;Check for P
	bit 5,h
	jr nz,DoPause

	ld bc,100
	call PauseABC	
	
	
NoPause:
	ld h,8
	call ReadKeyRow				;Check for Space
	bit 0,h	
	call z,ReloadMagazine		;Reload

	
	ld h,5
	call ReadKeyRow
	bit 0,h
	jr nz,NoSpeedToggle			;Change cursor speed
	push hl
		ld hl,CursorMoveSpeedNum
		inc (hl)
		ld a,(hl)	
		and %00000011			;4 speed options
		sla a

		ld hl,CursorMoveSpeeds	;Inc and get speed
		add l
		ld l,a

		ld c,(hl)				;Get two bytes from speed
		inc hl
		ld b,(hl)
		ld (CursorMoveSpeed),bc
		call WaitForKeyRelease
	pop hl

NoSpeedToggle:
	ld h,3
	call ReadKeyRow				;Switch Palette on C
	bit 0,h
	call z,ChangePalette		

	ld h,0
	call ReadKeyRow				;Toggle 3D on 3
	bit 3,h
	jr nz,No3DChange
	push hl
		call cls
		call Change3D			;Toggle 3D
		call RePaintScreen
	pop hl
	call WaitForKeyRelease
	
No3DChange:
	ld hl,(Tick)				;Update Level tick
	inc hl
	ld (tick),hl

	ld b,SpriteDataCount 		;Sprites to update
	ld iy,SpriteArray

DrawNextSprite:
	push bc
		push iy
			call AnimateSpriteIY ;Update the sprite
		pop iy
		ld bc,SpriteDataSize
		add iy,bc

	pop bc
	ld a,b
	and %00000111
	jr nz,NoMouseUp
	push bc
		call ReadMouse			;Move player cursor
		call UpdateCursor			;(if it's moved)
	pop bc
NoMouseUp:
	djnz DrawNextSprite

notick:
	ld hl,(Tick)
	ld a,l
	and %00001111				;Minigun fire speed
	jr nz,NoFireHeld
	

	ld a,(FireHeld)
	or a
	jr z,NoFireHeld				;Is fire held

	;Fire Held (No Drag)

	call GetGun
	ld a,(IX+G_Mode)
	cp 3						;Is Chibigun?
	call z, FireBullets
	
NoFireHeld:
	ld a,(FireDown2)			;Right Mouse / Fire 2
	or a
	jr z,NoFire2
	
	call ReloadMagazine			;Reload
	xor a
	ld (FireDown2),a
	
NoFire2:	
	ld a,(FireDown)				;Left Mouse / Fire 1
	or a
	jr z,NoFire

	xor a
	ld (FireDown),a
	call FireBullet
	
NoFire:
	call UpdateCursor			;Update Cursor

	jp ShowAgain
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;Make sure this is in the first bank!
SourceFont:
	dw FontData-&4000
	db 0,24



	
	read "\SrcMSX\MSX_V2_BankSwapper.asm"
	read "MSX2_Sprites.asm"
	

	
	
InterruptHandler:
	ifdef Akuyou_MusicPos
		push af
		push bc
		push de
		push hl
		push ix
		push iy
			call PLY_Play
		pop iy
		pop ix
		pop hl
		pop de
		pop bc
		pop af
	endif
	ret		;Return to ROM interrupt handler
	
	read "\SrcALL\Multiplatform_ArkosTrackerLiteGBRom.asm"
	
		
		
		
;Exported by Akusprite Editor
SpriteInfo:
    dw &0000/PixelsPerByte + SpriteBase           ;SpriteAddr 0
     db 32,32                                     ;XY 
    dw &0400/PixelsPerByte + SpriteBase           ;SpriteAddr 1
     db 24,16                                     ;XY 
    dw &0580/PixelsPerByte + SpriteBase           ;SpriteAddr 2
     db 24,16                                     ;XY 
    dw &0700/PixelsPerByte + SpriteBase           ;SpriteAddr 3
     db 24,16                                     ;XY 
    dw &0880/PixelsPerByte + SpriteBase           ;SpriteAddr 4
     db 24,16                                     ;XY 
    dw &0A00/PixelsPerByte + SpriteBase           ;SpriteAddr 5
     db 16,16                                     ;XY 
    dw &0B00/PixelsPerByte + SpriteBase           ;SpriteAddr 6
     db 40,48                                     ;XY 
    dw &1280/PixelsPerByte + SpriteBase           ;SpriteAddr 7
     db 40,48                                     ;XY 
    dw &1A00/PixelsPerByte + SpriteBase           ;SpriteAddr 8
     db 32,32                                     ;XY 
    dw &1E00/PixelsPerByte + SpriteBase           ;SpriteAddr 9
     db 32,32                                     ;XY 
    dw &2200/PixelsPerByte + SpriteBase           ;SpriteAddr 10
     db 24,24                                     ;XY 
    dw &2440/PixelsPerByte + SpriteBase           ;SpriteAddr 11
     db 24,24                                     ;XY 
    dw &2680/PixelsPerByte + SpriteBase           ;SpriteAddr 12
     db 48,32                                     ;XY 
    dw &2C80/PixelsPerByte + SpriteBase           ;SpriteAddr 13
     db 48,32                                     ;XY 
    dw &3280/PixelsPerByte + SpriteBase           ;SpriteAddr 14
     db 32,24                                     ;XY 
    dw &3580/PixelsPerByte + SpriteBase           ;SpriteAddr 15
     db 32,16                                     ;XY 
    dw &3780/PixelsPerByte + SpriteBase           ;SpriteAddr 16
     db 24,16                                     ;XY 
    dw &3900/PixelsPerByte + SpriteBase           ;SpriteAddr 17
     db 24,16                                     ;XY 
    dw &3A80/PixelsPerByte + SpriteBase           ;SpriteAddr 18
     db 32,24                                     ;XY 
    dw &3D80/PixelsPerByte + SpriteBase           ;SpriteAddr 19
     db 32,32                                     ;XY 
    dw &4180/PixelsPerByte + SpriteBase           ;SpriteAddr 20
     db 24,16                                     ;XY 
    dw &4300/PixelsPerByte + SpriteBase           ;SpriteAddr 21
     db 24,24                                     ;XY 
    dw &4540/PixelsPerByte + SpriteBase           ;SpriteAddr 22
     db 16,16                                     ;XY 
    dw &4640/PixelsPerByte + SpriteBase           ;SpriteAddr 23
     db 16,16                                     ;XY 
    dw &4740/PixelsPerByte + SpriteBase           ;SpriteAddr 24
     db 40,61                                     ;XY 
    dw &50C8/PixelsPerByte + SpriteBase           ;SpriteAddr 25
     db 40,61                                     ;XY 
    dw &5A50/PixelsPerByte + SpriteBase           ;SpriteAddr 26
     db 40,39                                     ;XY 
    dw &6068/PixelsPerByte + SpriteBase           ;SpriteAddr 27
     db 40,39                                     ;XY 
    dw &6680/PixelsPerByte + SpriteBase           ;SpriteAddr 28
     db 24,6                                      ;XY 
    dw &6710/PixelsPerByte + SpriteBase           ;SpriteAddr 29
     db 24,16                                     ;XY 
    dw &6890/PixelsPerByte + SpriteBase           ;SpriteAddr 30
     db 48,16                                     ;XY 
    dw &6B90/PixelsPerByte + SpriteBase           ;SpriteAddr 31
     db 32,16                                     ;XY 

	
	
	
ReadJoy:
	call Player_ReadJoy
	

	ld a,h
	and %00010000
	jr nz,JoyNotFire
	ld a,(FireHeld)
	or a
	jr nz,JoyNotFireB
	ld a,1
	ld (FireDown),a		;Set FireDown/Held
	ld (FireHeld),a
	jr JoyNotFireB
JoyNotFire:
	xor a		
	ld (FireHeld),a		;Release Fire Held
JoyNotFireB:
	ld a,h
	and %00100000
	jr nz,JoyNotFire2
	ld a,1
	ld (FireDown2),a
		
JoyNotFire2:

	ei

	ld a,(KeyIgnore)
	and %00001111
	dec a
	ld (KeyIgnore),a
	ret nz				;Skip most ticks
	
	ld de,0
	
	ld a,(CursorMoveSpeed)
	bit 1,h				;---FRLDU
	jr nz,JrNotDown
	ld e,a
JrNotDown:	
	bit 0,h
	jr nz,JrNotUp
	neg
	ld e,a
JrNotUp:	
	ld a,(CursorMoveSpeed)
	bit 3,h
	jr nz,JrNotRight
	ld d,a
JrNotRight:	
	bit 2,h
	jr nz,JrNotLeft
	neg
	ld d,a
JrNotLeft:	
	
	
	jp DoFakeMouse
	
	
ReadKey:

	ld h,7
	call ReadKeyRow
	bit 7,h
	jr nz,keyNotFire
	ld a,(FireHeld)
	or a
	jr nz,keyNotFireB
	ld a,1
	ld (FireDown),a		;Set FireDown/Held
	ld (FireHeld),a
	jr keyNotFireB
keyNotFire:
	xor a		
	ld (FireHeld),a		;Release Fire Held
keyNotFireB:
	ld h,8
	call ReadKeyRow

	ld l,255
	
	bit 4,h
	jr nz,KeyNotL
	res 2,l
KeyNotL:	
	bit 7,h
	jr nz,KeyNotR
	res 3,l
KeyNotR:	

	bit 5,h
	jr nz,KeyNotU
	res 0,l
KeyNotU:
	bit 6,h
	jr nz,KeyNotD
	res 1,l
KeyNotD:
	ld h,l
	jp JoyNotFire2
	
Player_ReadJoy:	


	ld b,2			;lets read the joystick too! (2 joysticks!)
joynextjoy:
	ld	a, 15		; AY port 15  (we're getting kana LED state)
	out	(&A0), a	; &A0 = AY Register write port
	in	a, (&A2)	; &A2 = Value read port
	and	128			;kana led state
	ld c,a			;store it for later

	ld a,b			;Joystick no (2/1)
	dec a 			;Converted to 1/0
	rrca
	rrca			;Coverted to 64/0
	add 15			;Allow input from all joystick ports)
	or c

;	or	0  *64+15  ;Joyport	
	out	(&A1), a	;&A1 = Value Write ports
	ld	a, 14		;Select Reg 14 (AY Port 14)
	out	(&A0), a	;&A0 = Register write port

	in	a, (&A2) 	;&A2 = read left right up down and button 1 and 2
	or %11000000	;fill in the blanks
	ld l,h
	ld h,a
	djnz joynextjoy	;Repeat for Joy 1

BootsStrap_ConfigureControls:
	ret
	
ReadMouse:	
	dodi
	ld a,(KeyMode)
	cp 2
	jp z,ReadJoy
	cp 0
	jr z,ReadKey
	
	call GetMouse
	push af
		and %00010000			;See if fire is pressed
		jr nz,MouseNotFire
	
		ld a,(FireHeld)
		or a
		jr nz,MouseNotFireB
		ld a,1
		ld (FireDown),a			;Set FireDown/Held
		ld (FireHeld),a
		jr MouseNotFireB
MouseNotFire:
		xor a		
		ld (FireHeld),a			;Release Fire Held
	
MouseNotFireB:
	pop af
	
	and %00100000			;See if fire is pressed
	jr nz,MouseNotFire2
	ld a,1
	ld (FireDown2),a
MouseNotFire2:


	
	ex de,hl				;XY mousepos into DE
DoFakeMouse:
	doei
	ld bc,0
	ld b,d
	ld hl,(NewPosX)
	
	ld a,d					;Xmove
	or a
	jp m,MouseLeft
;MouseRight
	add hl,bc
	jr c,MouseUPDOWN
	ld a,h
	cp 128-3
	jr nc,MouseUPDOWN		;Off Right of screen
	
	ld (NewPosX),hl
	jr MouseUPDOWN
	
MouseLeft:	
	add hl,bc
	jr nc,MouseUPDOWN		;Off Left of screen
	ld (NewPosX),hl
		
MouseUPDOWN:			
	ld b,e
	ld hl,(NewPosY)
	
	ld a,e					;Ymove
	or a
	jp m,MouseUp
	
;MouseDown
	add hl,bc
	ret c
	ld a,h
	cp 192-7				;Off Bottom of screen
	ret nc
	ld (NewPosY),hl
	ret
	
MouseUp:	
	add hl,bc
	ret nc					;Off Top of screen
	ld (NewPosY),hl
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
		
GetMouse: 
		call GetMouseAxis
        ld h,a			;Save both nibbles
		
        call GetMouseAxis
        ld l,a			;Save both nibbles
				
		ld a,b
		or %11001111	;Set Unused bits of buttons
        ret
	
GetMouseAxis:

		 ;KJ212211			;K=Kana light J= Port 21
	ld d,%11101100			;For Mouse 2
	;ld d,%10010011 		;For Mouse 1 
	
	CALL  GetMouseNibble   	;Get Top Nibble (Bit 8 ON)
	and %00001111
    rlca
    rlca
    rlca
    rlca
    ld c,a					;Save top nibble
	
	ld d,%11001100			;For Mouse 2
	;ld d,%10000011 		;For Mouse 1
    CALL  GetMouseNibble   	;Get Bottom Nibble (Bit 8 OFF)
	ld b,a 					;Bits 56 are Buttons
    and %00001111
    or c					;Or in top nibble
	
	neg						;Flip the axis
	ret
	
GetMouseNibble: 	
		ld a,15			;Write Reg 15 (Port B)
		out (&a0),a
		ld a,d
		out (&a1),a
		
		ld b,10			;Wait for Mouse
MouseWait:  
		djnz MouseWait

        ld a,14			;Read Reg 14 (Port A)
        out (&a0),a	
        in a,(&a2)		;Read from hardware
        ret
	
ShowCrosshair:	
	push iy
		sla b
		ld h,0
		ld l,b
		ld d,0
		ld e,c
		exx
			ld ix,(CrosshairSprite)
			ld iy,VramYpos_Cursors			;YSource
			ld hl,8			;Width
			ld de,8			;Height
		call VDP_LMMM_ViaStack	;Fast Copy Vram->Vram
	pop iy
	ret
	
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;		
	


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
PixelsPerByte equ 4

S_AddressL equ 0
S_AddressH equ 1
S_Wid equ 2
S_Hei equ 3

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	;Settings to define our graphics screen

VDPScreenInitData:	
	db %00000110,128+0		;mode register #0
	db %01100000,128+1		;mode register #1
	db 31		,128+2		;pattern name table
	db 239		,128+5		;sprite attribute table (LOW)
	db %11110000,128+7		;border colour/character colour at text mode
	db %00001010,128+8		;mode register #2
	db %00000000,128+9	 	;mode register #3
	db 128		,128+10		;colour table (HIGH)
VDPScreenInitData_End:	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
VDP_LMMM_ViaStack:		;Fast copy from VRAM to VRAM

		ld bc,&0093		;Command Byte (don't change)
VDP_LMMM_ViaStack2:		;Fast copy from VRAM to VRAM
		push bc
		push bc			;unused
		push de			;Height
		push hl			;Width
		exx
			push de		;DestY
			push hl		;DestX
		exx
		push iy			;SourceY
		push ix			;SourceX
		
		ld hl,0
		add hl,sp		;Load Stack into HL
		
		push hl
			call VDP_FirmwareSafeWait	;Wait for VDP to be ready
			call VDP_HMMM
		pop hl
		
		ld bc,16		;Skip 8 pairs of vars pushed onto the stack
		add hl,bc
		ld sp,hl
		ret
				
VDP_HMMM:				;Fast copy from VRAM to VRAM

	ld a,32				;AutoInc From 32
	call SetIndirect
	
	defb &ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3
	defb &ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3	
	ret														  ;outi x 15
	

; MyHMMM:						;Tile Copy command Vars 	
; MyHMMM_SX:	defw &0000 		;SY 32,33	;Destination Xpos
; MyHMMM_SY:	defw &0200 		;SY 34,35	;Tiles start 512 pixels down
; MyHMMM_DX:	defw &0000 		;DX 36,37	;Destination X
; MyHMMM_DY:	defw &0000 		;DY 38,39	;Destination Y
; MyHMMM_NX:	defw &0008 		;NX 40,41 	;Width=8px
; MyHMMM_NY:	defw &0008 		;NY 42,43	;Height=8px
				; defb 0     		;Color 44 	;unused
; MyHMMM_MV:	defb 0     		;Move 45	;Unused
				; defb %11010000 	;Command 46	;HMMM command - Don't mess with this 
; T_TemplateCommands_End	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
		
VDP_HMMC_Generated_ViaStack:	;Fill ram from calculated values (first in A)

		ld bc,&00F0		;Command Byte (don't change)
		push bc
		ld c,a			;First Pixel (B unused)
		push bc
		push de			;Height
		push hl			;Width
		push iy			;Ypos
		push ix			;Xpos
		
		ld hl,0
		add hl,sp
		push hl
			call VDP_HMMC_Generated
		pop hl
		
		ld bc,12	;Skip 8 pairs of vars pushed onto the stack
		add hl,bc
		ld sp,hl
		ret
		
VDP_HMMC_Generated:		;Fill ram from calculated values (first in A)

	;Set the autoinc for more data
	ld a,36				;AutoInc From 36
	call SetIndirect
	defb &ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3
	defb &ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3			; 11 outis
	
	ld a,128+44			;128 = NO Increment ;R#44    Color byte (from CPU->VDP)
SetIndirect:
	out (VdpOut_Control),a		
	ld a,128+17			;R#17 -	Indirect Register 128=no inc  [AII] [ 0 ] [R5 ] [R4 ] [R3 ] [R2 ] [R1 ] [R0 ] 	
	out (VdpOut_Control),a
	ld c,VdpOut_Indirect	
	ret

;MyHMMC:								:HMMC Command looks like this
;MyHMMC_DX:	defw &0000 ;DX 36,37
;MyHMMC_DY:	defw &0000 ;DY 38,39
;MyHMMC_NX:	defw &0032 ;NX 40,41
;MyHMMC_NY:	defw &0032 ;NY 42,43
;MyHMMCByte:	defb 255   ;Color 44
;				defb 0     ;Move 45
;				defb %11110000 ;Command 46	


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	;Wait for VDP to be ready
VDP_FirmwareSafeWait:
	di
	call VDP_Wait						;Wait for the VDP to be available
	call VDP_GetStatusFirwareDefault	;Reset selected Status register to 0 for the firmware
	ret

VDP_Wait: 			;Get The status register - Disable interrupts, 
						;as they require status register 0 to be selected!

	call VDP_GetStatusRegister
VDP_DoWait:
	in a,(VdpIn_Status)	;Status register 2	- S#2  [TR ] [VR ] [HR ] [BD ] [ 1 ] [ 1 ] [EO ] [CE ] 
	rra
	ret nc
	jr VDP_DoWait


	


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;Set Status Reg

VDP_GetStatusFirwareDefault:
	xor a
	jr VDP_GetStatusRegisterB
VDP_GetStatusRegister:		;Get The status register - Disable interrupts!
	ld a,2					;S#2  [TR ] [VR ] [HR ] [BD ] [ 1 ] [ 1 ] [EO ] [CE ] - Status register 2
VDP_GetStatusRegisterB:
	out (VdpOut_Control),a
	ld a,128+15				;R#15  [ 0 ] [ 0 ] [ 0 ] [ 0 ] [S3 ] [S2 ] [S1 ] [S0 ] - Set Stat Reg to read
	out (VdpOut_Control),a
	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
palette:	
	;  %-----GGG-RRR-BBB			-Black and white
	dw %0000000000000000; ;0  
	dw %0000001100110011; ;0  
	dw %0000010001000100; ;0  
	dw %0000011101110111; ;0  
PaletteBlack:
	dw 0,0,0,0
	dw 0,0,0,0
	dw 0,0,0,0
	

	;  %-----GGG-RRR-BBB			-Red and Cyan
	dw %0000000000000000; ;0  
	dw %0000000000110000; ;0  
	dw %0000000001000000; ;0  
	dw %0000000001110000; ;0  
	
	dw %0000001100000011; ;0  
	dw %0000001100110011; ;0  
	dw %0000001101000011; ;0  
	dw %0000001101110011; ;0  
	
	dw %0000010000000100; ;0  
	dw %0000010000110100; ;0  
	dw %0000010001000100; ;0  
	dw %0000010001110100; ;0  
	
	dw %0000011100000111; ;0  
	dw %0000011100110111; ;0  
	dw %0000011101000111; ;0  
	dw %0000011101110111; ;0  
	
	;  %-----GGG-RRR-BBB			-Green and Magenta
	dw %0000000000000000; ;0  
	dw %0000001100000000; ;0  
	dw %0000010000000000; ;0  
	dw %0000011100000000; ;0  
	
	dw %0000000000110011; ;0  
	dw %0000001100110011; ;0  
	dw %0000010000110011; ;0  
	dw %0000011100110011; ;0  
	
	dw %0000000001000100; ;0  
	dw %0000001101000100; ;0  
	dw %0000010001000100; ;0  
	dw %0000011101000100; ;0  
	
	dw %0000000001110111; ;0  
	dw %0000001101110111; ;0  
	dw %0000010001110111; ;0  
	dw %0000011101110111; ;0  
	
		;  %-----GGG-RRR-BBB			-GM
	dw %0000000000000000; ;0  
	dw %0000001100110000; ;0  
	dw %0000010001000000; ;0  
	dw %0000011101110000; ;0  
	
	dw %0000000000000011; ;0  
	dw %0000001100110011; ;0  
	dw %0000010001000011; ;0  
	dw %0000011101110011; ;0  
	
	dw %0000000000000100; ;0  
	dw %0000001100110100; ;0  
	dw %0000010001000100; ;0  
	dw %0000011101110100; ;0  
	
	dw %0000000000000111; ;0  
	dw %0000001100110111; ;0  
	dw %0000010001000111; ;0  
	dw %0000011101110111; ;0  
	
;Mono
	dw %0000000000000000; ;0  
	dw %0000000000000000; ;0  
	dw %0000000000000000; ;0  
	dw %0000000000000000; ;0  
	
	dw %0000001100110011; ;0  
	dw %0000001100110011; ;0  
	dw %0000001100110011; ;0  
	dw %0000001100110011; ;0  	
	
	dw %0000010001000100; ;0  
	dw %0000010001000100; ;0  
	dw %0000010001000100; ;0  
	dw %0000010001000100; ;0  	
	
	dw %0000011101110111; ;0  
	dw %0000011101110111; ;0  
	dw %0000011101110111; ;0  
	dw %0000011101110111; ;0  
	
	
	
	;incbin "\ResALL\SuckHunt\CPCTitleTiles.raw"
	read "\SrcALL\Multiplatform_BCD.asm"				;Show Binary Coded Decimal
	read "\SrcALL\MultiPlatform_ShowDecimal.asm"		;Show decimal number
	
	read "SH_Multiplatform.asm"
	read "SH_DataDefs.asm"
	read "SH_RamDefs.asm"
	read "SH_LevelInit.asm"

Cls:		
	push ix
	push iy
		ld ix,0			;XDest
		ld iy,0			;YDest
		ld hl,256		;Width
		ld de,192		;Height
		ld a,&00		;Colors (&LR)
		call VDP_HMMV_ViaStack		;Fast Copy Vram->Vram
		call VDP_FirmwareSafeWait	;Wait for VDP to be ready
	pop iy
	pop ix
	ret
	
;Fill area - used by CLS
VDP_HMMV_ViaStack:
		ld bc,&00C0		;Command Byte (don't change)
		push bc
		ld c,a			;Color / Direction (UNUSED)
		push bc			
		push de			;Height
		push hl			;Width
		push IY			;DestY
		push IX			;DestX
		
		ld hl,0
		add hl,sp		;Load Stack into HL
		
		push hl
			call VDP_FirmwareSafeWait	;Wait for VDP to be ready
			call VDP_HMMV
		pop hl
		
		ld bc,12		;Skip 8 pairs of vars pushed onto the stack
		add hl,bc
		ld sp,hl
		ret
				
VDP_HMMV:				;Fast copy from VRAM to VRAM
	ld a,36				;AutoInc From 32
	call SetIndirect
	
	defb &ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3,&ED,&A3
	defb &ED,&A3,&ED,&A3,&ED,&A3						 ;outi x 11
	ret											
	

ReadKeyRow:
	;push bc
	;	ld b,h
	;	ld c,&FE
	;	in h,(c)
	;pop bc
	
	in	a,(&AA)	;Read current state
	and	&F0		;only change bits 0-3 (others are for leds and casette)
	or	h		;take row number from B
	out	(&AA),a ; Update new state
	in	a,(&A9)	;read row into A
	ld h,a
	ret

	read "\SrcALL\Multiplatform_ChibiSound.asm"			;Sound Driver	

WaitForKeyRelease:
	ld b,0
WaitForKeyRelease2:	
	ld h,b
	call ReadKeyRow
	inc h;cp %11111111
	jr nz,WaitForKeyRelease
	inc b
	ld a,b
	cp 12
	jr nz,WaitForKeyRelease2

GetNextLine:
TitleTiles:
ShowSpriteL2DMode:
	nop
	ret
NextLineCommand1_Plus2:
	
ChangePalette:
	ld a,(Depth3D)
	or a
	jr z,NoPaletteChange	;3D Off?
	
	ld a,(PaletteNum)
	cp 3
	jr c,PalOk
	xor a
PalOk
	inc a
	push hl
		call SetPalette
	pop hl
	call WaitForKeyRelease
NoPaletteChange:
	ret
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


	
PaletteOutPink:		;Fade in/out palettes
	dw &0000,&0001,&0041,&0074
	dw &0000,&0000,&0001,&0071
	dw &0000,&0000,&0000,&0001
	dw &0000,&0000,&0000,&0000
PaletteOutBlue:	
	dw &0000,&0001,&0117,&0337
	dw &0000,&0000,&0001,&0017
	dw &0000,&0000,&0000,&0001
	dw &0000,&0000,&0000,&0000
	
PaletteInBlue:	
	dw &0000,&0000,&0000,&0001
	dw &0000,&0000,&0001,&0017
	dw &0000,&0001,&0017,&0117
	dw &0000,&0014,&0117,&0337	;-GRB
PaletteInPink:
	dw &0000,&0000,&0000,&0001
	dw &0000,&0000,&0001,&0041
	dw &0000,&0001,&0041,&0074
	dw &0000,&0041,&0074,&0373	;-GRB
	
	
SetPaletteBlack:
	ld hl,PaletteBlack			;Set all colors to black
	ld b,1
	jr PalFadeB
DoPalFadeOutQ:
	xor a
	ld (FireDown),a
	inc a			;a=1
	call DoPalFadeOut			;Fade out
	
	jp cls
DoPalFadeInQ:
	xor a
	call DoPalFadeIn			;Fade in
	jp Set2D3Dmode				;Re-Enable 3d palette

DoPalFadeIn:
	ld de,PaletteInPink			;2 possible fade palettes
	ld hl,PaletteInBlue
	jr PalFade

DoPalFadeOut:
	ld de,PaletteOutPink		;2 possible fade palettes
	ld hl,PaletteOutBlue
PalFade:
	or a
	jr nz,PalFadeBluePal
	ex de,hl					;Seelect Blue/Bink palette (0=pink)
PalFadeBluePal:
	ld b,4						;4 stages of fade
PalFadeB:
	push bc
		push hl
		call SetPaletteAlt4		;Fade in 4 colors
		pop hl
		push hl
		call SetPaletteAlt4b	;Set other 12 
		pop hl
		push hl
		call SetPaletteAlt4b
		pop hl
		call SetPaletteAlt4b
		push hl
			ld bc,30
			call PauseABC	
		pop hl
	pop bc
	djnz PalFadeB
	ret
	


Set2D3Dmode:
	ld a,(Depth3D)
	or a
	jr z,Set2DMode
	jr Set3DMode
DoSet3D:
	ld a,1
	ld (CurrentMode3D),a		;Reset normal operation of sprites
	ret
	
Change3D:
	ld a,(Depth3D)
	inc a
	and %00000011
	ld (Depth3D),a				;Toggle stereo depth
	jr z,Set2DMode
Set3DMode:
	call DoSet3D				;Turn 3D on
	ld a,(PaletteNum)
	and %00000011
	jr nz,Set3DMode_PaletteOK
	inc a
Set3DMode_PaletteOK:
	jr SetPaletteB

Set2DMode:
	xor a
	ld (CurrentMode3D),a
	jr SetPaletteB				;Set 3D palette

	
SetPalette:
	ld (PaletteNum),a
SetPaletteB:
	rrca					;32 bytes per palette
	rrca
	rrca
	ld hl,Palette
	ld b,0
	ld c,a
	add hl,bc
SetPaletteAlt16:
	ld b,16*2				;16 colors - 2 bytes per color
SetPaletteAltB:
	ld c,VdpOut_Palette		;Palette port
	otir					;Send the palette to the VDP
	ret

SetPaletteAlt4:	
	ld b,0
SetPaletteAlt4b:
	ld b,4*2
	jr SetPaletteAltB
	
	
SongGame:
	incbin "\ResAll\SuckHunt\GameSongF800.bin"
SongTitle:
	incbin "\ResAll\SuckHunt\TitleSongF800.bin"
SongIntro:
	incbin "\ResAll\SuckHunt\IntroSongF800.bin"

Arkos_ChangeSong:
	ifdef Akuyou_MusicPos
		push hl
			call PLY_Stop			;Stop the music 
			di
		pop hl

		ld de,Akuyou_MusicPos		;Song Destination
		ld bc,&400					;Max song length
		ldir
		
		call PLY_Init				;Restart music
	endif
	ret
	
wavedata:
	incbin "\ResALL\SuckHunt\operationsucka1-6.raw"
wavedataEnd:
freq4 equ 1	;11025
bits1 equ 1
	
	
	
ChibiWave:			;Needed modifying to work from ROM

	ld ixh,d						;Move Length into IX
	ld ixl,e

	di
	ld a,7			;Mixer  --NNNTTT (1=off) --CBACBA
	ld c,%00111111
	call AYRegWrite

	ld c,0
	ld a,2			;TTTTTTTT Tone Lower 8 bits	A
	call AYRegWrite

	ld c,%00000000
	ld a,3			;----TTTT Tone Upper 4 bits+
	call AYRegWrite

	ld c,0
	ld a,9			;4-bit Volume / 2-bit Envelope Select for channel A ---EVVVV
	call AYRegWrite

	; Sending a value

Waveagain:
	ld d,(hl)						;Load in sample
	ld e,8							;Samples per byte
WaveNextBit:
	xor a							;Shift first bit into D
	rl d
	rla 

	call Do1BitWav					;Call correct wave function 
									;on bit depth
									
	ld b,27							;WaveDelay

Wavedelay:							;Wait for next sample
	djnz Wavedelay
	
	dec e
	jr nz,WaveNextBit				;Process any remaining bits
	inc hl

	dec ix
	ld a,ixh		
	or ixl
	jr nz,Waveagain					;Repeat until there are no more bytes
	
	ld c,0
	ld a,9			
	jp AYRegWriteQuick				;Turn off sound 
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
Do1BitWav:
	rlca							;Move bits into correct pos
Do1BitWavb:
	rlca
	rlca
Do1BitWavc
	ld c,a
	ld a,9			
	jp AYRegWriteQuick				;Send the new volume level


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef BuildCPC

AYRegWriteQuick:
		ld b,&F4		;#f4 value
		out (c),c

		ld bc,&F680		;Write VALUE
		out (c),c	

		ld bc,&f600		;inactive
		out (c),c
		ret
	endif

PrintKeyMode:
	
	ld hl,KeyText
		ld b,0
		ld a,(KeyMode)
		rlca
		rlca
		ld c,a
		add hl,bc
		
		ld de,&1417
		call LocateandPrint
	ret
	

KeyText: db "Key",255
	db "Mou",255
	db "Joy",255

	
	
TitleTileMap3D:
  db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,0,0,0,0,0,0,0,0,3,4,5,6,0,0
  db 0,0,0,0,0,0,7,8,9,10,11,12,13,0,0,14,15,16,17,18,0,0,0,0,0,0,19,20,21,22,0,0
  db 0,0,0,0,0,0,23,24,25,26,27,28,29,0,0,30,31,32,33,34,0,0,0,0,0,0,0,0,0,0,0,0
  db 8,9,10,11,12,13,0,35,36,37,38,0,0,0,0,39,40,41,42,43,0,7,8,9,10,11,12,13,0,0,0,0
  db 24,25,26,27,28,29,0,44,45,46,47,48,38,0,0,49,50,51,52,53,0,23,24,25,26,27,28,29,0,0,0,0
  db 0,0,0,54,55,56,57,58,59,46,60,61,62,63,0,0,0,0,0,0,64,0,0,0,65,66,67,68,0,0,0,0
  db 0,0,69,70,71,72,73,74,75,76,77,78,79,80,0,0,0,0,0,81,82,83,84,85,0,86,87,0,0,0,0,0
  db 0,0,88,89,90,91,92,93,94,95,96,97,98,99,100,0,0,0,0,101,102,103,104,105,0,0,0,106,107,108,0,0
  db 109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 126,127,128,129,130,131,132,133,134,135,136,137,138,46,139,46,140,141,142,143,0,0,0,0,144,145,146,0,144,145,146,0
  db 147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,38,169,170,171,172,173,170,171,172,173
  db 174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,0,0,0,0,0,0,0,0,0
  db 197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,0,220,221,222,223,224,225,0,0
  db 0,0,0,226,227,228,229,230,46,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,0
  db 0,0,0,0,253,254,255,0,255,1,46,255,2,255,3,255,4,46,255,5,255,6,255,7,255,8,255,9,255,10,255,11,255,12,255,13,255,14,255,15,255,16,255,17,255,18,255,19,255,20,255,21,255,22,0
  db 0,0,0,0,0,0,255,23,46,46,255,24,255,25,46,46,255,26,255,27,255,28,255,29,255,30,255,31,255,32,255,33,255,34,255,35,255,36,255,37,255,38,255,39,255,40,255,41,255,42,255,43,0
  db 0,0,0,0,0,255,44,230,46,46,255,5,255,45,46,231,232,255,46,255,47,255,48,255,49,255,50,255,51,255,52,255,53,0,255,54,255,55,255,56,255,57,255,58,255,59,255,60,0,0
  db 0,0,0,0,0,255,61,255,62,255,63,255,63,255,64,255,65,255,66,255,67,255,68,0,0,0,0,0,0,0,0,0,0,255,69,255,70,255,71,255,72,255,73,255,74,255,75,255,76
  db 0,0,0,0,0,0,0,0,0,0,0,255,77,255,78,255,79,255,80,0,255,81,255,82,255,83,255,84,0,0,0,255,85,255,86,255,87,255,88,255,89,255,90,255,91,255,92,255,93
  db 255,94,255,95,255,96,255,97,0,0,0,0,0,0,0,0,255,98,255,99,255,100,0,255,81,255,101,255,102,255,103,0,255,104,255,105,255,106,255,107,255,108,255,109,255,110,255,111,255,112,255,113,255,114
  db 255,115,0,255,116,255,117,255,118,0,0,255,104,255,105,255,106,0,255,119,255,120,255,121,255,122,0,255,81,255,123,255,124,255,125,0,255,126,255,127,255,128,0,0,255,129,255,130,255,130,255,131,255,104,255,105
  db 255,132,255,133,255,134,255,135,255,136,255,137,0,255,126,255,127,255,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,126,255,127
  db 255,138,255,139,255,140,255,141,255,142,255,143,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 255,144,255,145,255,146,255,147,255,148,255,149,255,150,255,151,255,152,255,153,255,154,255,155,255,156,255,157,255,158,255,159,0,0,0,255,129,255,130,255,130,255,131,0,0,0,0,0,0,0,0,0

SuckOfDeath3dTileMap:
  db 0,0,0,0,0,1,2,3,0,0,0,0,0,0,0,0
  db 0,0,0,0,0,4,5,6,7,8,0,0,0,0,0,0
  db 0,0,0,0,0,0,0,9,10,11,12,0,0,0,0,0
  db 0,0,0,0,13,14,15,16,17,18,19,20,21,22,0,0
  db 0,0,0,23,24,25,26,27,28,29,30,31,32,33,34,0
  db 0,0,35,36,37,38,39,40,41,42,43,44,45,46,0,0
  db 0,0,47,48,49,50,51,52,53,54,55,56,57,58,59,60
  db 0,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75
  db 76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,0
  db 91,92,93,0,94,95,96,97,98,99,100,101,102,103,104,105
  db 106,107,108,0,109,110,111,112,113,114,115,116,117,118,119,120
  db 121,122,123,0,124,125,126,0,127,128,129,130,131,132,133,134

  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Intro1:
  db 0,0,0,0,0,1,1,2,0,0,0,0,3,4,3,5
  db 0,0,0,0,0,6,7,8,9,0,0,0,10,11,12,13
  db 14,0,0,0,0,15,16,17,9,0,0,0,18,19,20,21
  db 3,22,0,0,0,23,24,15,25,0,0,0,26,27,28,9
  db 29,30,31,6,32,33,34,35,6,0,36,37,23,38,39,9
  db 10,40,41,30,42,43,44,45,46,47,48,49,9,6,50,16
  db 51,48,52,53,54,55,56,57,58,59,60,61,9,21,62,27
  db 0,63,64,65,66,67,68,69,70,71,72,73,23,24,24,24
  db 0,0,0,74,75,76,77,7,35,78,0,0,79,80,24,38
  db 0,0,0,0,0,23,81,82,83,84,0,0,85,86,87,88
  db 0,0,0,0,6,22,89,90,39,91,0,0,92,93,94,95
  db 0,0,0,96,24,24,24,38,25,9,0,0,23,97,98,99
Intro1b:
  db 0,0,0,0,0,1,1,2,0,0,0,0,3,4,3,5
  db 0,0,0,0,0,6,7,8,9,0,0,0,10,11,12,13
  db 0,0,0,0,0,15,16,17,9,0,0,0,18,19,20,21
  db 0,0,0,0,0,23,24,15,25,0,0,0,26,27,28,9
  db 31,6,32,33,0,0,27,35,6,0,0,0,255,126,255,24,39,255,127
  db 41,48,42,43,255,128,0,75,45,46,0,0,47,48,49,77,255,129
  db 52,53,54,55,56,255,130,255,131,57,58,255,132,255,133,59,60,61,255,134,255,135
  db 64,65,66,67,68,0,255,136,69,35,0,255,137,71,72,255,138,24,24
  db 0,74,75,76,0,0,77,7,35,255,139,0,78,79,80,24,38
  db 0,0,0,0,0,23,81,82,83,84,0,0,85,86,87,88
  db 0,0,0,0,6,22,89,90,39,91,0,0,92,93,94,95
  db 0,0,0,96,24,24,24,38,25,9,0,0,23,97,98,99

Intro2:
  db 0,100,101,102,0,103,2,104,0,104,0,105,0,0,0,105
  db 0,0,0,100,106,0,0,74,0,104,0,107,0,0,0,107
  db 0,0,0,108,109,101,0,0,12,110,111,112,0,0,0,113
  db 0,0,114,115,53,48,116,0,117,118,119,120,0,0,0,107
  db 0,0,121,53,48,122,10,48,48,48,123,124,125,126,0,113
  db 0,127,128,129,130,131,132,133,134,48,135,136,137,138,139,107
  db 140,141,142,78,143,144,24,145,146,147,48,48,48,148,149,113
  db 0,140,150,0,9,0,103,102,151,152,153,154,155,156,157,158
  db 0,159,160,0,161,102,0,0,151,162,163,164,165,166,167,168
  db 0,169,0,0,0,0,103,102,170,162,171,169,172,173,169,150
  db 0,174,0,0,0,175,0,0,151,176,177,169,178,179,169,160
  db 0,0,0,0,0,180,0,0,181,182,183,169,184,139,174,185
Intro2b:
  db 101,255,86,0,102,0,103,2,104,0,104,0,105,0,0,0,105
  db 0,100,255,87,2,255,88,0,0,74,0,104,0,107,0,0,0,107
  db 0,108,109,255,89,255,0,0,0,0,12,110,111,112,0,0,0,113
  db 114,115,255,90,48,116,0,0,0,117,118,119,120,0,0,0,107
  db 121,255,91,48,122,10,48,48,48,48,48,123,124,125,126,0,113
  db 128,129,130,131,132,133,133,133,134,48,135,136,137,138,139,107
  db 142,78,255,92,144,24,145,8,46,255,93,147,48,48,48,148,149,113
  db 150,255,94,0,0,9,0,103,102,151,152,153,154,155,156,157,158
  db 160,159,0,0,161,102,0,0,151,162,163,164,165,166,167,168
  db 0,169,0,0,0,0,103,102,170,162,171,169,172,173,169,150
  db 0,174,0,0,0,175,0,0,151,176,177,169,178,179,169,160
  db 0,0,0,0,0,180,0,0,181,182,183,169,184,139,174,185

Intro3:
  db 0,0,0,186,9,0,0,187,7,188,189,48,48,190,191,192
  db 0,0,193,0,0,0,0,30,139,0,194,195,24,196,197,198
  db 0,0,199,200,0,0,186,0,201,0,202,203,204,205,0,206
  db 0,207,191,208,139,209,0,0,0,202,210,211,212,213,126,0
  db 0,214,0,215,208,139,0,0,202,216,217,117,218,219,220,126
  db 0,221,0,0,192,222,223,202,224,225,226,151,227,147,228,229
  db 230,0,0,0,0,231,232,224,0,169,233,234,235,236,139,0
  db 237,0,0,238,239,0,0,0,0,169,231,240,0,241,242,0
  db 9,0,243,244,214,245,0,0,0,169,246,247,248,0,249,0
  db 0,0,250,251,252,253,254,254,0,113,246,0,0,255,0,241,139
  db 0,0,0,255,1,255,2,255,3,255,4,255,5,0,255,6,255,7,255,8,247,0,255,9,242
  db 0,0,0,96,255,10,255,11,255,12,255,13,96,246,0,248,0,255,8,0,255,14
Intro3b:
  db 0,0,0,186,9,0,0,187,7,188,189,48,48,190,191,192
  db 0,0,193,0,0,0,0,30,139,0,194,195,24,196,197,198
  db 0,0,199,200,0,0,186,0,201,0,202,203,204,205,0,206
  db 0,207,191,208,139,209,0,0,0,202,210,211,212,213,126,0
  db 0,214,0,215,208,139,0,0,202,216,217,117,218,219,220,126
  db 0,221,0,0,192,222,223,202,224,225,226,151,227,147,228,229
  db 230,0,0,0,0,255,95,232,224,0,169,233,234,235,236,139,0
  db 237,0,0,255,96,255,97,253,254,254,0,169,231,240,0,241,242,0
  db 9,0,243,255,98,255,2,255,3,255,4,255,5,0,169,246,247,248,0,249,0
  db 0,0,250,255,99,255,100,255,11,255,12,255,13,0,113,246,0,0,255,0,241,139
  db 0,0,0,96,255,101,48,255,102,255,103,0,255,6,255,7,255,8,247,0,255,9,242
  db 0,0,0,0,255,104,255,105,48,255,106,255,107,246,0,248,0,255,8,0,255,14
Intro4:
  db 255,15,255,16,101,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 255,17,255,18,255,19,255,20,110,37,101,101,0,0,0,255,21,255,22,255,22,255,22,255,19
  db 255,23,255,24,255,25,255,26,255,27,255,28,255,19,255,29,255,30,255,31,255,32,255,33,255,34,0,0,255,35
  db 255,18,255,19,255,19,255,30,30,255,23,255,24,255,36,255,37,152,255,19,48,30,30,255,15,37
  db 255,38,255,39,255,40,255,27,255,17,255,18,255,19,255,41,255,37,255,37,255,37,255,19,255,37,255,37,147,48
  db 134,255,42,255,43,255,44,255,45,255,46,255,46,255,47,255,37,255,48,255,39,255,3,255,37,255,49,255,50,255,51
  db 0,255,52,255,53,255,54,255,55,255,42,255,56,255,57,255,37,255,48,255,39,255,58,255,59,255,60,101,0
  db 0,0,0,0,0,255,61,255,62,255,37,255,48,255,38,255,63,255,64,255,65,255,66,255,23,255,67
  db 0,0,0,0,0,0,0,255,68,134,255,42,255,43,255,69,255,23,255,70,255,71,48
  db 0,0,0,0,0,0,96,24,255,24,24,255,68,133,255,72,255,73,48,255,74
  db 0,0,0,0,0,0,0,255,75,255,76,255,77,255,78,24,255,79,48,48,255,80
  db 0,0,0,0,0,0,0,0,0,255,81,255,82,255,83,255,84,255,23,255,85,133
Intro4b:
  db 255,15,255,16,101,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 255,17,255,18,255,19,255,20,110,37,101,101,0,0,0,255,21,255,22,255,22,255,22,255,19
  db 255,23,255,24,255,25,255,26,255,27,255,28,255,19,255,29,255,30,255,31,255,32,255,33,255,34,0,0,255,35
  db 255,18,255,19,255,19,255,30,30,255,23,255,24,255,36,255,37,152,255,19,48,30,30,255,15,37
  db 255,38,255,39,255,40,255,27,255,17,255,18,255,19,255,41,255,37,255,37,255,37,255,19,255,37,255,37,147,48
  db 134,255,42,255,43,255,44,255,45,255,46,255,46,255,47,255,37,255,48,255,39,255,3,255,37,255,108,255,109,255,74
  db 0,255,52,255,53,255,54,255,55,255,42,255,56,255,57,255,37,255,48,255,39,255,58,255,59,255,110,255,111,255,112
  db 0,0,0,0,0,255,61,255,62,255,37,255,48,255,38,255,63,255,64,255,113,255,114,255,73,48
  db 0,0,0,0,0,0,0,255,68,134,255,42,255,43,255,37,255,115,255,116,48,48
  db 0,0,0,0,0,0,96,24,255,24,24,255,68,133,255,117,255,118,255,119,255,120
  db 0,0,0,0,0,0,0,255,75,255,76,255,77,255,78,24,24,255,121,255,122,255,123
  db 0,0,0,0,0,0,0,0,0,255,81,255,82,255,83,75,255,124,255,68,255,125

IntroAnimList:
	db 4+0,12,1			;X,Y,Colors
	dw Intro1				;Frame 1
	dw Intro1b				;Frame 2
	db 4+32-16,0,0		;X,Y,Colors
	dw Intro3				;Frame 1
	dw Intro3b				;Frame 2
	db 4+32-16,12,0		;X,Y,Colors
	dw Intro2				;Frame 1
	dw Intro2b				;Frame 2
	db 4+0,0,1			;X,Y,Colors
	dw Intro4				;Frame 1
	dw Intro4b				;Frame 2
	
IntroGraphics:	
	incbin "\ResALL\SuckHunt\SuckHuntIntroMSX2.RAW"


SuckofDeath3D:
	incbin "\ResALL\SuckHunt\SuckofDeathMSX23d.RAW"
SuckofDeath3D_End:
	
;TxtPleaseWait:
;	db "Please Wait...",255
;	db "To Suck this hard takes ages!",255
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


		

	align 14;Pad to 32k
	;db "HELLO!"			;Test for bank switch
	
	
SprCursorSprite:
	incbin "\ResALL\SuckHunt\SuckHuntMSX2_Cursors.raw"
SprCursorSprite_End:

SpriteBase equ SpriteData-&4000
SpriteData:
	incbin "\ResALL\SuckHunt\SuckHuntZXN.RAW"	
FontData:				;Bitmap Font (4 color)
	;ds 16	;Space char
	incbin "\ResALL\SuckHunt\FontMSX2.RAW"
	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


	align 14;Pad to 32k
TitleTiles3D:
	incbin "\ResALL\SuckHunt\MSX2TitleTiles3D.RAW"	
TitleTiles3D_End:	


  db 0
  align 14;Pad to 32k
