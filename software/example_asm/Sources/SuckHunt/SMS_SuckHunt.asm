
vdpControl equ &BF
vdpData    equ &BE

SMSVars equ &CC00

SXpos equ SMSVars+&000
SYpos equ SMSVars+&002
MaxYpos equ SMSVars+&004
NextYpos equ SMSVars+&006
NextXpos equ SMSVars+&008
SpriteHClipPlus2 equ SMSVars+&00A
SXCrop equ SMSVars+&00C
SpriteDataHL equ SMSVars+&00E
SpriteMSXData equ SMSVars+&100 
FontMSXData equ SMSVars+&0F0 

Akuyou_MusicPos equ &F800
AYCache equ Akuyou_MusicPos+&400
Akuyou_SfxPos equ &F800
ArkosRam  equ AYCache+&100

Enable_Speech equ 1
Enable_Intro equ 1

	;We have two 'EyeBuffers' in VRAM
	;C000-C5FF: Left Eye Tilemap
	;C600-C6FF: Left Eye Sprites
	;C700-CCFF: Right Eye Tilemap
	;CD00-CDFF: Right Eye Sprites
CachTilesR equ &C0
CachTilesL equ &C6
	

LightGunX 		equ SMSVars+&010	;Last X-pos of Fire
LightGunY 		equ SMSVars+&011	;Last Y-pos of Fire
;LightGunFire 	equ &D012	;Fire pressed 
LightGunTimeout 	equ SMSVars+&013 ; Skip Time


;NextCharX equ SMSVars+&014
;NextCharY equ SMSVars+&015

CharTileMap equ SMSVars+&016	;2 Bytes
SMSDoRemoveSprite Equ SMSVars+&018
SMSDoForceRedraw Equ SMSVars+&019

PaletteDataR_Var Equ SMSVars+&01A		;2 byte palette 
PaletteDataL_Var Equ SMSVars+&01C		;2 byte palette 
RightCache Equ SMSVars+&01E		;2 byte RightCache
LeftCache Equ SMSVars+&020		;2 byte RightCache
PlayMusicOff equ SMSVars+&022
PauseButton equ SMSVars+&023
KeyIgnore equ SMSVars+&024
GunDisable equ SMSVars+&025
GameRam equ &D000

GunSprite equ 1

VscreenMinX equ 64		;Top left of visible screen in logical co-ordinates
VscreenMinY equ 0

VscreenWid equ 128		;Screen Size in logical units
VscreenHei equ 96
VscreenWidClip equ -2

ScreenSize256 equ 1
pixelsPerByte2 equ 1 	;Use full logical res

ScreenObjWidth equ 80-12
ScreenObjHeight equ 200-48
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef vasm
		include "\SrcALL\VasmBuildCompat.asm"
	else
		read "\SrcALL\WinApeBuildCompat.asm"
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	read "\SrcALL\CPU_Compatability.asm"

	Macro DoEI
		ei
	endm
	macro DoDI
		di
	endm
	
	;Unrem this if building with vasm
	include "\SrcALL\VasmBuildCompat.asm"

	org &0000
	jp ProgramStart		;&0000 - RST 0
	ds 5,&C9			;&0002 - RST 0
	ds 8,&C9			;&0008 - RST 1
	ds 8,&C9			;&0010 - RST 2
	ds 8,&C9			;&0018 - RST 3
	ds 8,&C9			;&0020 - RST 4
	ds 8,&C9			;&0028 - RST 5
	ds 8,&C9			;&0030 - RST 6
	jp InterruptHandler	;&0038 - RST 7 
	ds 5,&C9			
	ds 38,&C9	
	;&0066 - NMI
NMI:						;Pause Button
	ex af,af'
	ld a,1
	ld (PauseButton),a
	ex af,af'
	
	reti
	
	
	
NoSpriteCache equ 1	

InterruptHandler:
	exx
	ex af,af'				;';Swap in Shadow registers
	
		
		ld a,(LightGunTimeout)
		dec a
		jr nz,InterruptHandler_Skip
		
		
	
		in a,(&DC)			;FIRE - Port 1 (bit 2 of &DD for port 2)
		and %00010000
		;in a,(&DD)			;FIRE - Port 2
		;and %00000100
		jr nz,GunNoFire
		
		xor a
		out (vdpControl),a
		ld a,&C0
		out (vdpControl),a	
		
	    ld hl,flashData		;Set all colors to WHITE
		ld b,4				;(Flash screen for light detect)
		ld c,vdpData		
		otir				
		
		ld c,$7E			;Scanline counter (7E: Y-Pos)
GunScanYAgain:	
		in a,(c)			;Scanline counter
		cp 192				;Have we scanned full Screen area?
		jr z,GunMiss		;Yes? Then player missed!

		in a,(&DD)
		and %01000000		;Light IN - Port 1 (bit 7 for port 2)
		;in a,(&DD)
		;and %10000000		;Light IN - Port 1 (bit 7 for port 2)
		jr nz,GunScanYAgain
		
		in a,(c)			;Scanline counter (7E: Y-Pos)
		inc c
		in h,(c)			;HCounter (7F: X-pos)		
		jr GunFireDone
GunMiss:					;Player Missed... Zero XY pos
		ld a,1
		ld (FireDown2),a
		xor a
		ld h,a
GunFireDone:				;Save Fire Pos
		ld (LightGunY),a
		add 6
		ld (NewPosY+1),a
		
		ld a,h
		ld (LightGunX),a
		sub 20
		;rlca
		ld (NewPosX+1),a

		ld a,1
		ld (FireDown),a	;Set Fired Flag
GunMiss2:
		ld a,10
		ld (LightGunTimeout),a
		
		
		jp InterruptHandler_Done
		
InterruptHandler_Skip:		
	ld (LightGunTimeout),a	
GunNoFire:
		ld a,($fff8)
		xor %00000001
		ld ($fff8),a		;0=Right Eye Image 1=Left Eye Image
		jr z,RightEye
		jp LeftEye
InterruptHandler_Done:		
		ld a,(PlayMusicOff)
		or a
		jr nz,NoMusicInt

		exx
		ex af,af'
		push af
		push bc
		push de
		push hl
		push ix
		push iy
;		exx
			call PLY_Play
			call AYEmulation_Play
			in a,(vdpControl)	;CLEAR SMS interrupt	
;		exx
		pop iy
		pop ix
		pop hl
		pop de
		pop bc
		pop af
	ei
	ret
	
NoMusicInt:			
		in a,(vdpControl)	;CLEAR SMS interrupt	
	exx
	ex af,af'
	ei 
	ret
	
	
	
RightEye:
	ld de,(RightCache)
	ld hl,(PaletteDataR_Var)	;Reset normal palette

	jr BothEye
	

LeftEye:
	ld de,(LeftCache)
	ld hl,(PaletteDataL_Var)	;Reset normal palette
BothEye:	
	;Palette Select
	xor a
    out (vdpControl),a
    ld a,&C0
    out (vdpControl),a	
		
	ld c,vdpData			;Def palette
	outi
	outi
	outi
	outi
		
	xor a
    out (vdpControl),a
    ld a,&78
    out (vdpControl),a	
	
	ex de,hl
	
	ld b,l					;Copy Tiles
	otir
	otir
	otir
	otir
	otir
	otir
	jp InterruptHandler_Done
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
ShowCrosshair:

	ld a,(GunDisable)		;Gun mode?
	or a
	jr nz,ShowCrosshairNoGun
	
	ld a,(LightGunX)
	or a
	jr z,HideCursor
ShowCrosshairNoGun:
	;ld a,(NewPosX)	;Get Fire XY Pos
	;or a
	
	ld a,b
	rlca
	add 3
	ld b,a
	ld a,c
	;rlca
	add 1
	ld c,a
HideCursor2:	
	ld a,(GunNum)
	add 77			;Offset to Cursor in Bank 2
	ld e,a	
	ld a,1
	ld d,8
	
	call SetHardwareSprite	;A=Hardware Sprite No. B,C = X,Y , D,E = Source Data, H=Palette etc
	
	ret
HideCursor:
		ld bc,&FFFF
		jr HideCursor2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;								Basic palette in native format									

flashData:
		;   --BBGGRR
		db %00111111	;0
		db %00111111	;1
		db %00111111	;2
		db %00111111	;3

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

ProgramStart:	
	di
	
    im 1    			;Interrupt mode 1
    ld sp, &FF00		;Default stack pointer

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;									Init the screen 												

	ld hl,VdpInitData	;Source of data
    ld b,VdpInitDataEnd-VdpInitData		;Byte count
    ld c,vdpControl		;Destination port
    otir				;Out (c),(hl).. inc HL... dec B, djnz 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;									Define Palette 												
	
    ;ld hl, &c000	    ; set VRAM write address to CRAM (palette) address 0
		; note &C0-- is a set palette command... it's not a literal memory address 
    ;call prepareVram

    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;					Start of the Test Code														

	;Define our character tiles
;	ld de, 128*8*4				;8 lines of 4 bytes per tile
;	ld iy, BitmapData			;Source of bitmap data
;	ld bc, BitmapDataEnd-BitmapData/2;Length of bitmap data
;	ld hl,&0404
	
	;call DefineSprite_S

	
; ;Upload Main Sprites
	; ld a,0
	; ld de,&0620
; UploadNext:	
	; push af
		; call UploadOneSprite
	; pop af
	; inc a
	; cp 32
	; ;jr nz,UploadNext
	;ld hl,CachTilesR*256
	;ld (RightCache),hl
	;ld hl,CachTilesL*256
	;ld (LeftCache),hl
		
	ld de,GameRam
	ld bc,GameRamEnd-GameRam-1
	call Cldir
	
;Sprite Palette
	ld hl,PaletteDataG
	ld a,&10
	out (vdpControl),a
	ld a,&C0
	out (vdpControl),a	
	
	ld c,vdpData		
	outi
	outi
	outi
	outi
	
	xor a
	ld (palette),a
	call ChangePalette

	
	ld a,1
	ld (PlayMusicOff),a
	ld (Depth3D),a
	
	ld hl,2
	ld (CursorMoveSpeed),hl
	
	ld hl,DefChar
	ld de,SprChar
	ld bc,16
	ldir	
	
	
ShowIntroAgain:	
	

	call DoPalFadeOutQ

	ifdef Enable_Intro
	ld a,1
	ld (PlayMusicOff),a

						
	ld IY,IntroData
	ld de,&0
	ld H,13
	ld l,32
	call DefineSpriteDoubleHeight	;DE=VDP address, IY=Source,H=Width,L=Height
	;doei
	
	ld hl,SongIntro
	call Arkos_ChangeSong
	
	ld b,4
	ld ix,IntroAnimList
DrawIntroAnim:
	push bc
		call cls
				
		doei

		ld a,(ix+0)		;X
		ld iyh,a
		ld a,(ix+1)		;Y
		ld e,(ix+3)
		ld d,(ix+4)
		ex de,hl
		push iy
		push ix
		push af
		
			call	ShowTileMapAltPosQtrIntro	;HL=Tilemap BC=Tile Data IYH=X	A=Y 	;E=Height ;D=Wid
			ld a,(ix+2)
			
			call DoPalFadeIn
			doei
			ld bc,80
			call PauseABC	
		pop af
		pop ix
		pop iy

		push ix
			ld e,(ix+5)
			ld d,(ix+6)
			ex de,hl
			call	ShowTileMapAltPosQtrIntro	;HL=Tilemap BC=Tile Data IYH=X	A=Y 	;E=Height ;D=Wid
			doei
			ld bc,120
			call PauseABC	
			ld a,(ix+2)
			call DoPalFadeOut
		pop ix
		ld bc,7
		add ix,bc

	pop bc
	djnz DrawIntroAnim
	endif

	call cls
	
	doei	
	
	
	
	
	ld bc,400
	call PauseABC	
	
	
	
	
	ld a,1
DrawTitleScreen:
	ld sp,&FFF0		;Define a stack pointer

	push af


		call cls
		call SetPaletteBlack

		
		
		ld a,(Depth3D)
		or a
		jr z,ShowTitle2D

		ld a,(PaletteNum)
		jr ShowTitle

ShowTitle2D:
		ld a,4
ShowTitle:
		push af	




	;DE=Tilemap ;BC=XYpos ; iyh=XEnd IYL=Yend ;iXH=Cache
	
	ld de,TitleR
	ld bc,0
	ld iy,&2018
	ld ixh,CachTilesL
	call FillAreaWithTilesCustom
	ld de,TitleL
	ld bc,0
	ld iy,&2018
	ld ixh,CachTilesR
	call FillAreaWithTilesCustom
	
	
			
	ld IY,TitleData
	ld de,&0
	ld H,32
	ld l,15
	call DefineSpriteA	;DE=VDP address, IY=Source,H=Width,L=Height
	


		
		
		
		
		
		
		
	
		
		
		
		
	
			call DoPalFadeInQ
			call DoSet3D
		pop af
		call SetPaletteB
	pop af
	ifdef Enable_Speech
		or a
		jr z,NoSpeech

		ld bc,300
		call PauseABC	
		
		dodi
		ld hl,PaletteDataG
		xor a
		out (vdpControl),a
		ld a,&C0
		out (vdpControl),a	
		
		ld c,vdpData		
		outi
		outi
		outi
		outi
		
		
		ld de,wavedataEnd-wavedata
		ld hl,wavedata
		xor a	;Bits
		ld b,30	;Speed
		
		
		call ChibiWave

		ld bc,300
		call PauseABC	
	endif
	xor a
	ld (FireDown),a
NoSpeech:
	ld hl,SongTitle
	call Arkos_ChangeSong
	doei
	ld bc,4000
	xor a
	ld (GunDisable),a		;Turn Gun on

TitleWait:


	call ReadMouse
	halt
	
	ifdef Enable_Intro
		dec bc
		ld a,b
		or c
		jp z,ShowIntroAgain
	endif

	push bc

	
		ld a,(PauseButton),
		or a
		jr z,NoPalChangeTitle
		xor a
		ld (PauseButton),a
	
		call ChangePalette
		ld a,(PaletteNum)
		or a
		jr nz,NoPalChangeTitle
	
			ld de,CachTilesR*256		;Disable 3D
			ld (LeftCache),de
			ld (RightCache),de
NoPalChangeTitle:
		
		in a,(&DC)
		ld h,a
		ld a,128
		bit 7,h
		jr z,StartGameHard


		ei
	pop bc
	ld a,(FireDown)
	or a
	jr z,TitleWait				;Disable Wait
	
	
	ld a,(LightGunY)
	ld b,a
	ld a,(LightGunX)
	or b 
	jr nz,StartGame			;Lightgun mode
	
	;Pad Mode
	inc a
	ld (GunDisable),a		;Gun off (Nonzero)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	


StartGame
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	xor a
StartGameHard:
	ld (LevelNum),a
	
	

	ld hl,SongGame
	call Arkos_ChangeSong
	doei



	ld hl,NewGameData
	ld de,OldPosX
	ld bc,NewGameData_End-NewGameData
	ldir

StartNewLevel:
	
	
	call InitLevel
	
	
	call RePaintScreen
	ld a,1
	ld (SMSDoForceRedraw),a
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Main Loop
	call DoPalFadeInQ
	
		


ShowAgain:	
	
	ld a,(SMSDoForceRedraw)
	or a
	jr z,NoForceRedraw
	
	call RepaintSprites
	
	xor a
	ld (SMSDoForceRedraw),a
NoForceRedraw:
	push bc
		call ReadMouse
	pop bc
	
	call UpdateSoundSequence



	ld a,(PauseButton)
	or a
	jr z,NoPause
	ld (PlayMusicOff),a
	
	call ply_stop
	call ChibiSoundSilent
	
	ld bc,100
	call PauseABC	
	xor a
	ld (PauseButton),a
	
DoPause:
	in a,(&DC)
	ld h,a
	bit 4,h
	jr nz,NoSpeedToggle
	
		ld hl,CursorMoveSpeedNum
		inc (hl)
		ld a,(hl)	
		and %00000011
		sla a

		ld hl,CursorMoveSpeeds
		add l
		ld l,a

		ld c,(hl)
		inc hl
		ld b,(hl)
		ld (CursorMoveSpeed),bc
		call WaitForKeyRelease
		jp PauseOff
NoSpeedToggle:

	
	bit 5,h
	jr nz,No3DChange
	call cls
	call Change3D
	call RePaintScreen
	
	call WaitForKeyRelease
No3DChange:


	ld a,(PauseButton)
	or a
	jr z,DoPause
	
PauseOff:
		
	xor a
	ld (PauseButton),a
	
	ld (PlayMusicOff),a
	ld bc,100
	call PauseABC	
NoPause:
	
	

	ld hl,(Tick)
	inc hl
	ld (tick),hl

	ld b,SpriteDataCount 
	ld iy,SpriteArrayEnd-16

DrawNextSprite:
	push bc
		push iy
			call AnimateSpriteIY
		pop iy
		ld bc,-SpriteDataSize
		add iy,bc

	pop bc
	ld a,b
	and %00000111
	jr nz,NoMouseUp
	push bc
		call ReadMouse
		call UpdateCursor
	pop bc
NoMouseUp:
	djnz DrawNextSprite
	


notick:
	ld hl,(Tick)
	ld a,l
	and %00001111
	jr nz,MoFireHeld
	

	ld a,(FireHeld)
	or a
	jr z,MoFireHeld

	;Fire Held (No Drag)

	call GetGun
	ld a,(IX+G_Mode)
	cp 3
	call z, FireBullets ;Minigun
MoFireHeld:



	ld a,(FireDown2)
	or a
	jr z,MoFire2	
	call ReloadMagazine			;Reload

	xor a
	ld (FireDown2),a
MoFire2:
	ld a,(FireDown)
	or a
	jr z,MoFire

	xor a
	ld (FireDown),a

	
	call FireBullet
MoFire:
	call UpdateCursor

	ld a,(FireHeld)
	or a
	jr z,MoFireHeldDrag


;Fire Button Held/Drag (not 1st time)


MoFireHeldDrag:

	jp ShowAgain
	
SetHardwareSprite:	;A=Hardware Sprite No. B,C = X,Y , D,E = Source Data, H=Palette etc
	push hl
	push de
	push af	
			push af
				ld h,&3F			;first byte is at &3F00
				ld l,a				;1 byte per tile - so load L with sprite number
				call prepareVram	;Get the memory address so we can write to it
				ld a,c				;Y pos
				out (vdpData),a
			pop af
			
			rlca					;2 bytes per tile - 1'st is X, 2nd is tile number
			ld h,&3F
			add &80					;next data Starts from &3F80
			ld l,a
			call prepareVram		;Get the memory address so we can write to it
			
			ld a,b					;X Pos
			out (vdpData),a
			ld a,e					;tile number
			out (vdpData),a
	pop af
	pop de
	pop hl
WaitForScreenRefresh:
	ret
	

GetScreenPos:	;return memory pos in HL of screen co-ord B,C (X,Y)
	ld a,b
	srl a
	srl a
	ld (SXpos),a
	
	ld a,e
	inc a
	srl a
	;srl a
	ld (SXCrop),a
	
	;ld h,0
	ld a,c
	srl a
	srl a
	srl a
	ld (SYpos),a
	
	ret	
		

PrintCharB:					;Print Char A with Sprite Routine
		add 77+4+1
		ld (CharTileMap+1),a
		ld a,255
		ld (CharTileMap),a
		
		
		ld bc,(CursorY)		;Get XY into BC
		ld iy,SprChar

		rl c
		rl c
		rl b				
		rl b


		ld a,b
		add 48+2
	
		ld  (IY+O_Xpos),a

		ld a,c
		add 2
		ld  (IY+O_Ypos),a
		
		;ld  (IY+O_SprL),e
		;ld  (IY+O_SprH),d
		
		ld a,(CursorZ)
		ld  (IY+O_Zpos),a
	
		
		ld hl,CharInfo
		ld (SpriteDataHL),hl
		ld a,0
		ld (SprChar+O_SprNum),a
		
		jp DrawSpriteFromIY
		
SetRemoveSpriteForceRedraw:	
	ld a,1
	ld (SMSDoForceRedraw),a
SetRemoveSprite:
	ld a,1
	jr SetDrawSpriteB
SetDrawSprite:
	xor a
SetDrawSpriteB:	
	ld (SMSDoRemoveSprite),a
	ret
	

ShowSpriteR:
	ld ixh,CachTilesR

	jp ShowSpriteR2	

ShowSpriteL:
	ld ixh,CachTilesL
ShowSpriteR2:	
	ld c,ixl	;Bytes per line (Width)
	
	ld a,(IY+O_SprNum)			;Sprite Num
	add (IY+O_SprH)
	and %00111111
	push iy
		push ix
			ld ix,(SpriteDataHL)
			ld d,0
			rlca
			rlca
			ld e,a
			add ix,de

			ld e,(ix+0)			;Xpos
			ld d,(ix+1)			;Ypos
			
			ld h,(ix+2)			;Wid
			ld l,(ix+3)			;Hei
			srl l
			srl l
			srl l
			
			srl h
			srl h
			srl h
		pop ix
		
		;inc c
		srl c
		;ld c,4
		;srl c
		ld ixl,c
		
		ld a,ixl
		or a
		jr z,SprFinish		;Width=0
		
		ld a,(SXpos)
		ld b,a
		
		ld a,(SYpos)
		ld c,a
		
		ld a,(SXCrop)
		
		push hl
			ld h,0
			ld l,a
			add hl,de
			ex de,hl
		pop hl
		
		ld a,ixl
		add b
		ld iyh,a
				
		ld a,l				;Calculate End Ypos
		add c
		ld iyl,a
		
		;Zero Tilemap Here
		ld a,(SMSDoRemoveSprite)
		or a
		jr z,FillAreaWithTiles2_Yagain
		ld de,ZeroTileMap
FillAreaWithTiles2_Yagain:					;DE=Tilemap ;BC=XYpos ; iyh=XEnd IYL=Yend ;iXH=Cache
		push bc
			
			call GetCacheScreenPos	;Move to the correcr VDP location
			push ix
FillAreaWithTiles2_Xagain:;Tilemap takes two bytes, ---pcvhn nnnnnnnn
				
				ld a,(de)
				inc de
				ld ixh,0
				ld ixl,a
				
				cp 255 
				jr nz,TileLowBank
				ld a,(de)
				inc de
				push bc
					ld b,0
					ld c,a
					add ix,bc
				pop bc
TileLowBank:
			
			
			ld a,ixl			;nnnnnnnn - Tile number
			ld (hl),a	
			inc hl
			ld a,ixh			;---pcvhn - p=Priority (1=Sprites behind) C=color palette 
			ld (hl),a	;(0=back 1=sprite), V=Vert Flip, H=Horiz Flip, N=Tilenum (0-511)
			inc hl
			inc b			;Increase Xpos
			ld a,b
			cp iyh			;Are we at the end of the X-line?
			jr nz,FillAreaWithTiles2_Xagain
			pop ix
		pop bc
		
		push bc
		
			ld a,(SpriteHClipPlus2-2)
			inc a
			srl a
			jr z,LineCropDone
			ld b,a
LineCropAgain:		
			ld a,(de)
			inc de
			cp 255
			jr z,LineCropAgain
			djnz LineCropAgain
LineCropDone:		
		pop bc
		inc c				;Increase Ypos
		ld a,c
		cp iyl				;Are we at the end of the height Y-line?
		jr nz,FillAreaWithTiles2_Yagain
		
		xor a
		ld (SpriteHClipPlus2-2),a
SprFinish:		
	pop iy
	doei	
	ret
	
ShowTileMapAltPosQtrIntro:	;HL=Tilemap BC=Tile Data IYH=X	A=Y 	;E=Height ;D=Wid	
	push ix
		ex de,hl

		ld c,a
		ld a,iyh
		ld b,a
		push bc
		push de
			ld ixh,CachTilesR
			call FillAreaWithTilesCustomQ
		pop de
		pop bc
		ld ixh,CachTilesL
		call FillAreaWithTilesCustomQ
	pop ix
	ret
FillAreaWithTilesCustomQ:
	ld iy,&100C
	add iy,bc
	
FillAreaWithTilesCustom:					;DE=Tilemap ;BC=XYpos ; iyh=XEnd IYL=Yend ;iXH=Cache
	push iy
	jp FillAreaWithTiles2_Yagain
	
;This will load into an XY pos into a 'Cached' tilemap 
	;at memory address &XX00 where X=IXH 
GetCacheScreenPos:	;Move to a memory address in VDP by BC cursor pos
	push bc				;B=Xpos, C=Ypos
		ld h,c
		xor a			
		rr h			;Multiply Y*64
		rra
		rr h
		rra
		rlc b			;Multiply X*2 (Two byte per tile)
		or b
		ld l,a
		ld a,h
		add ixh			;Address of TileMap &3800 
		ld h,a			;(32x28 - 2 bytes per cell = &700 bytes)
	pop bc
	ret
	
UploadOneSprite:
	;call GetSpriteXYPos
	ld iy,SpriteInfo
	ld ix,SpriteMSXData
	push de
		ld d,0
		rlca
		rlca
		ld e,a
		add iy,de
		add ix,de
		sla e
		rl d
		add ix,de
	pop de
UploadOneSpriteAlt:	
	push de
		srl d		;16 bytes per tile
		rr e
		srl d
		rr e
		srl d
		rr e
		srl d
		rr e
		srl d
		rr e
		ld (ix+0),e
		ld (ix+1),d
	pop de
		
	;jp UploadSprite
	

	
UploadSprite:
	push ix
	;Left Eye
		
		ld c,(iy+2)
		ld b,(iy+3)
		
DoubleBytes:
		srl b
		srl b
		srl b
		srl c
		srl c
		srl c
		ld (ix+2),c		;Xpos
		ld (ix+3),b		;Ypos
		
		push bc
		pop hl
		push bc
			;ld bc,(32*16)/2 ;Length of sprite	
			ld a,(iy+0)
			ld c,a
			ld a,(iy+1)
			ld b,a
			push bc
			pop iy
			call DefineSpriteA
		pop bc
		
		ld a,b
		ld b,0
		sla c			;16 bytes per tile
		rl b
		sla c
		rl b
		sla c
		rl b
		sla c
		rl b
		sla c
		rl b
		ex de,hl
CalcNextTile:
		add hl,bc
		dec a
		jr nz,CalcNextTile
		ex de,hl
	pop ix
	ret
	
DefineSpriteA:	;DE=VDP address, IY=Source,H=Width,L=Height
	di
	ex de,hl
	call prepareVram	;Set VRAM address we want to write to
	ex de,hl
SpriteNextL:
	ld c,h
SpriteNextH:	
	ld b,8
DefineLine:
	ld a,(iy)
	out (vdpData),a		;Send Byte to VRAM
	inc iy
	ld a,(iy)
	out (vdpData),a		;Send Byte to VRAM
	inc iy
	xor a
	out (vdpData),a		;Send Byte to VRAM
	nop
	out (vdpData),a		;Send Byte to VRAM
	djnz DefineLine
	dec c
	jr nz,SpriteNextH
	dec l
	jr nz,SpriteNextL
	ret
	
DefineSpriteDoubleHeight:	;DE=VDP address, IY=Source,H=Width,L=Height
	di
	ex de,hl
	call prepareVram	;Set VRAM address we want to write to
	ex de,hl
SpriteNextLb:
	ld c,h
SpriteNextHb:	
	ld b,4
DefineLineb:
	ld a,(iy)
	out (vdpData),a		;Send Byte to VRAM
	inc iy
	ld a,(iy)
	out (vdpData),a		;Send Byte to VRAM
	dec iy
	xor a
	out (vdpData),a		;Send Byte to VRAM
	nop
	out (vdpData),a		;Send Byte to VRAM
	
	ld a,(iy)
	out (vdpData),a		;Send Byte to VRAM
	inc iy
	ld a,(iy)
	out (vdpData),a		;Send Byte to VRAM
	inc iy
	xor a
	out (vdpData),a		;Send Byte to VRAM
	nop
	out (vdpData),a		;Send Byte to VRAM
	
	djnz DefineLineb
	dec c
	jr nz,SpriteNextHb
	dec l
	jr nz,SpriteNextLb
	ret
		
	
	
prepareVram:				;Set vdpData to write to memory address HL in vram
	    ld a,l
	    out (vdpControl),a
	    ld a,h
	    or &40				;we set bit 6 to define that we want to Write data...
	    out (vdpControl),a	;As the VDP ram only goes from &0000-&3FFF 
    ret							;this does not cause a problem
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;					VDP Register settings (needed to turn on screen)							

VdpInitData:
	db %00000110,128+0 ; reg. 0, display and interrupt mode.
	db %11100000,128+1 ; reg. 1, display and interrupt mode.
	db &ff		,128+2 ; reg. 2, name table address. &ff = name table at &3800
	db &ff		,128+3 ; reg. 3, Name Table Base Address  (no function) &0000
	db &ff 		,128+4 ; reg. 4, Color Table Base Address (no function) &0000
	db &ff		,128+5 ; reg. 5, sprite attribute table. -DCBA98- = bits of address $3f00
	db &04		,128+6 ; reg. 6, sprite tile address. -----D-- = bit 13 of address $2000
	db &00		,128+7 ; reg. 7, border color. 			----CCCC = Color
	db &00 		,128+8 ; reg. 8, horizontal scroll value = 0.
	db &00		,128+9 ; reg. 9, vertical scroll value = 0.
	db &ff 		,128+10; reg. 10, raster line interrupt. Turn off line int. requests.
VdpInitDataEnd:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


SpriteBase equ BitmapData
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
PixelsPerByte equ 4

S_AddressL equ 0
S_AddressH equ 1
S_Wid equ 2
S_Hei equ 3

CharInfo:
     dw CharTileMap                                  ;SpriteAddr 1
     db 8,8		                                    ;XY 
ZeroTileMap:
	ds 32
SpriteInfo:
     dw Sprite_1                                   ;SpriteAddr 1
     db 32,32                                     ;XY 
    dw Sprite_2                                   ;SpriteAddr 2
     db 24,8                                      ;XY 
    dw Sprite_3                                   ;SpriteAddr 3
     db 24,16                                     ;XY 
    dw Sprite_4                                   ;SpriteAddr 4
     db 24,16                                     ;XY 
    dw Sprite_5                                   ;SpriteAddr 5
     db 24,16                                     ;XY 
    dw Sprite_6                                   ;SpriteAddr 6
     db 16,16                                     ;XY 
    dw Sprite_7                                   ;SpriteAddr 7
     db 40,48                                     ;XY 
    dw Sprite_8                                   ;SpriteAddr 8
     db 40,48                                     ;XY 
    dw Sprite_9                                   ;SpriteAddr 9
     db 32,32                                     ;XY 
    dw Sprite_10                                  ;SpriteAddr 10
     db 32,32                                     ;XY 
    dw Sprite_11                                  ;SpriteAddr 11
     db 24,24                                     ;XY 
    dw Sprite_12                                  ;SpriteAddr 12
     db 24,24                                     ;XY 
    dw Sprite_13                                  ;SpriteAddr 13
     db 48,32                                     ;XY 
    dw Sprite_14                                  ;SpriteAddr 14
     db 48,24                                     ;XY 
    dw Sprite_15                                  ;SpriteAddr 15
     db 32,24                                     ;XY 
    dw Sprite_16                                  ;SpriteAddr 16
     db 32,16                                     ;XY 
    dw Sprite_17                                  ;SpriteAddr 17
     db 24,16                                     ;XY 
    dw Sprite_18                                  ;SpriteAddr 18
     db 24,16                                     ;XY 
    dw Sprite_19                                  ;SpriteAddr 19
     db 32,16                                     ;XY 
    dw Sprite_20                                  ;SpriteAddr 20
     db 32,32                                     ;XY 
    dw Sprite_21                                  ;SpriteAddr 21
     db 24,16                                     ;XY 
    dw Sprite_22                                  ;SpriteAddr 22
     db 24,24                                     ;XY 
    dw Sprite_23                                  ;SpriteAddr 23
     db 16,8                                      ;XY 
    dw Sprite_24                                  ;SpriteAddr 24
     db 16,16                                     ;XY 
    dw Sprite_25                                  ;SpriteAddr 25
     db 40,64                                     ;XY 
    dw Sprite_26                                  ;SpriteAddr 26
     db 40,64                                     ;XY 
    dw Sprite_27                                  ;SpriteAddr 27
     db 40,40                                     ;XY 
    dw Sprite_28                                  ;SpriteAddr 28
     db 40,40                                     ;XY 
    dw Sprite_29                                  ;SpriteAddr 29
     db 24,8                                      ;XY 
    dw Sprite_30                                  ;SpriteAddr 30
     db 24,16                                     ;XY 
    dw Sprite_31                                  ;SpriteAddr 31
     db 48,16                                     ;XY 
    dw Sprite_32                                  ;SpriteAddr 32
     db 32,16                                     ;XY 

Sprite_1:
  db 1,2,3,4
  db 5,6,6,7
  db 8,9,9,10
  db 11,12,13,14

Sprite_2:
  db 15,16,17

Sprite_3:
  db 18,19,20
  db 21,22,23

Sprite_4:
  db 0,24,25
  db 26,27,28

Sprite_5:
  db 29,30,31
  db 32,33,34

Sprite_6:
  db 35,36
  db 37,38

Sprite_7:
  db 39,40,41,0,42
  db 43,44,45,46,47
  db 48,49,50,51,52
  db 53,54,55,56,57
  db 58,59,60,61,0
  db 0,62,63,64,0

Sprite_8:
  db 65,66,67,68,69
  db 70,71,72,73,74
  db 75,76,77,51,78
  db 79,80,55,81,82
  db 0,59,60,61,0
  db 0,83,63,84,0

Sprite_9:
  db 85,86,87,88
  db 89,90,91,92
  db 93,94,95,96
  db 0,97,98,0

Sprite_10:
  db 99,100,101,102
  db 103,104,105,106
  db 107,108,109,110
  db 0,111,112,0

Sprite_11:
  db 113,114,115
  db 116,117,118
  db 119,120,0

Sprite_12:
  db 121,122,123
  db 124,125,126
  db 119,127,0

Sprite_13:
  db 128,129,130,131,132,133
  db 134,135,136,137,138,139
  db 140,141,142,143,141,144
  db 145,0,0,0,0,146

Sprite_14:
  db 147,148,149,150,151,152
  db 153,154,155,156,157,158
  db 159,160,161,162,163,164

Sprite_15:
  db 165,166,167,168
  db 169,170,171,172
  db 173,0,0,174

Sprite_16:
  db 175,176,177,178
  db 179,180,181,182

Sprite_17:
  db 183,184,185
  db 186,187,188

Sprite_18:
  db 189,190,191
  db 192,193,194

Sprite_19:
  db 195,196,197,198
  db 199,200,201,202

Sprite_20:
  db 203,204,205,206
  db 207,208,209,210
  db 211,212,213,214
  db 215,216,217,218

Sprite_21:
  db 219,220,221
  db 0,222,0

Sprite_22:
  db 223,224,225
  db 226,227,228
  db 11,229,230

Sprite_23:
  db 231,232

Sprite_24:
  db 233,234
  db 235,236

Sprite_25:
  db 0,237,238,239,240
  db 241,242,243,244,245
  db 246,247,248,249,250
  db 0,251,252,253,254
  db 0,255,0,255,1,255,2,255,3
  db 0,255,4,255,5,255,6,255,7
  db 0,255,8,255,9,255,10,0
  db 255,11,255,12,0,255,13,255,14

Sprite_26:
  db 0,255,15,255,16,255,17,240
  db 255,18,255,19,255,20,255,21,255,22
  db 255,23,247,255,24,255,25,255,26
  db 0,251,252,253,255,27
  db 0,255,0,255,1,255,2,255,3
  db 0,255,4,255,5,255,6,255,7
  db 0,255,8,255,9,255,10,0
  db 255,11,255,12,0,255,13,255,14

Sprite_27:
  db 255,28,255,29,255,30,255,31,0
  db 255,32,255,33,255,34,255,35,0
  db 255,36,255,37,255,38,255,39,255,40
  db 255,41,255,42,255,43,255,44,255,45
  db 255,46,255,47,255,48,255,49,0

Sprite_28:
  db 255,28,255,29,255,30,255,31,0
  db 255,50,255,33,255,34,255,35,0
  db 255,51,255,52,255,53,255,39,255,54
  db 255,41,255,55,255,56,255,57,0
  db 255,46,255,58,255,59,255,60,0

Sprite_29:
  db 255,61,255,61,255,61

Sprite_30:
  db 0,255,62,0
  db 255,63,255,64,255,65

Sprite_31:
  db 255,66,255,67,255,66,255,67,255,66,255,68
  db 255,69,255,66,255,69,255,66,255,69,255,70

Sprite_32:
  db 255,71,0,0,255,72
  db 255,73,255,74,255,75,255,76



	
DefMoonTest:
	dw &4430	;XY-Pos
	dw &1420	;Size
	dw 0		;Sprite Addr
	db 80		;Zpos (Negative = Sticking out
	db 255		;life
	db %000000001	;Tick
	db 4,0,0	;XYZ move
	db prgMoon	;Program
	db 24		;Sprite
	db 0,0		;Frame Speed / Tick
	
DefMoonTest3:
	dw &4850	;XY-Pos
	dw &1010	;Size
	dw 0		;Sprite Addr
	db 80		;Zpos (Negative = Sticking out
	db 255		;life
	db %000000001	;Tick
	db 4,0,0	;XYZ move
	db prgMoon	;Program
	db 0		;Sprite
	db 0,0		;Frame Speed / Tick

	
DefMoonTest4:
	dw &C020	;XY-Pos
	dw &1010	;Size
	dw 0		;Sprite Addr
	db 80		;Zpos (Negative = Sticking out
	db 255		;life
	db %000000001	;Tick
	db 4,0,0	;XYZ move
	db prgMoon	;Program
	db 0		;Sprite
	db 0,0		;Frame Speed / Tick

DefMoonTest2:
	dw &4010	;XY-Pos
	dw &1010	;Size
	dw 0		;Sprite Addr
	db 80		;Zpos (Negative = Sticking out
	db 255		;life
	db %000000001	;Tick
	db 4,0,0	;XYZ move
	db prgMoon	;Program
	db 	0	;Sprite
	db 0,0		;Frame Speed / Tick

	read "\SrcALL\Multiplatform_BCD.asm"				;Show Binary Coded Decimal
	read "\SrcALL\MultiPlatform_ShowDecimal.asm"		;Show decimal number
	
	read "SH_Multiplatform.asm"
	read "SH_DataDefs.asm"
	read "SH_RamDefs.asm"
	read "SH_LevelInit.asm"
	



PrintSpace:
	ld a,' '
Printchar:
	push af
	push ix
	push bc
	push hl
	push de
		sub 32				;No Char below 32
		ld bc,FontData
PrintTileb:
		call PrintCharB
PrintTilec:
		ld hl,CursorX
		inc (hl)
		ld hl,SpriteInfo
		ld (SpriteDataHL),hl
	pop de
	pop hl
	pop bc
	pop ix
	pop af
	ret
	

GetNextLineWithClip:
		db 0,0
NextLineCommand2_Plus2:
NextLineCommand1_Plus2:

	
Cls:
	xor a
ClsSpriteAgain:
	push af
		ld c,200;&FFFF
		call SetHardwareSprite	;A=Hardware Sprite No. B,C = X,Y , D,E = Source Data, H=Palette etc
	pop af
	inc a
	cp 64
	jr nz,ClsSpriteAgain


	ld de,&C000
	ld bc,&c00-1
	call cldir
	call SetDrawSprite		;Sprite Routines to draw
	
	ret	


ReadMouse:

	ld a,(GunDisable)		;Gun mode?
	or a
	ret z
	
	in a,(&DC)
	ld (LightGunTimeout),a			;Disable LightGun
	ld h,a
	bit 4,h
	jr nz,keyNotFire
	ld a,(FireHeld)
	or a
	jr nz,keyNotFireB
	ld a,1
	ld (FireDown),a		;Set FireDown/Held
	ld (FireHeld),a
	jr keyNotFireB
keyNotFire:
	xor a		
	ld (FireHeld),a		;Release Fire Held

keyNotFireB:

	bit 5,h
	jr nz,keyNotFire2

	ld a,1
	ld (FireDown2),a
keyNotFire2:	

	ld a,(KeyIgnore)
	and %00001111
	dec a
	ld (KeyIgnore),a
	ret nz				;Skip most ticks
	
	
	ld de,0
	
	ld a,(CursorMoveSpeed)
	bit 1,h				;---FRLDU
	jr nz,JrNotDown
	ld e,a
JrNotDown:	
	bit 0,h
	jr nz,JrNotUp
	neg
	ld e,a
JrNotUp:	
	ld a,(CursorMoveSpeed)
	bit 3,h
	jr nz,JrNotRight
	ld d,a
JrNotRight:	
	bit 2,h
	jr nz,JrNotLeft
	neg
	ld d,a
JrNotLeft:	
	
	
DoFakeMouse:
	doei
	ld bc,0
	ld b,d
	ld hl,(NewPosX)
	
	ld a,d					;Xmove
	or a
	jp m,MouseLeft
;MouseRight
	add hl,bc
	jr c,MouseUPDOWN
	ld a,h
	cp 128-3
	jr nc,MouseUPDOWN		;Off Right of screen
	
	ld (NewPosX),hl
	jr MouseUPDOWN
	
MouseLeft:	
	add hl,bc
	jr nc,MouseUPDOWN		;Off Left of screen
	ld (NewPosX),hl
		
MouseUPDOWN:			
	ld b,e
	ld hl,(NewPosY)
	
	ld a,e					;Ymove
	or a
	jp m,MouseUp
	
;MouseDown
	add hl,bc
	ret c
	ld a,h
	cp 192-7				;Off Bottom of screen
	ret nc
	ld (NewPosY),hl
	ret
	
MouseUp:	
	add hl,bc
	ret nc					;Off Top of screen
	ld (NewPosY),hl
	ret



GetNextLine:
TitleTiles3D:
TitleTiles:
SprCursorSprite:
	ret

		
Set2D3Dmode:
	ld a,(Depth3D)
	or a
	jr z,Set2DMode
	jr Set3DMode
DoSet3D:
	ret
	
Change3D:
	ld a,(Depth3D)
	inc a
	and %00000011
	ld (Depth3D),a
	ret z
	inc a				;Don't allow Zero
	ld (Depth3D),a
	ret

	
WaitForKeyRelease:
	in a,(&dc)
	cp 255
	jr nz,WaitForKeyRelease
	in a,(&dd)
	or %11100000
	cp 255
	jr nz,WaitForKeyRelease
	
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
ChangePalette:
	ld de,CachTilesL*256		;Disable 3D
	ld (LeftCache),de

			
	ld de,CachTilesR*256
	
	
	ld a,(PaletteNum)
	inc a
	cp 5
	jr c,PalOk
	ld de,CachTilesL*256		;Disable 3D
	xor a
PalOk
	ld (RightCache),de
	push hl
		call SetPalette
	pop hl
	call WaitForKeyRelease
NoPaletteChange:
	ret
	
Set3DMode:
Set2DMode:
	ld a,(PaletteNum)
SetPalette:
	ld (PaletteNum),a
;Set up Palette
SetPaletteB:
	rlca
	rlca
	ld hl,Palette

	ld b,0
	ld c,a
	add hl,bc
SetPaletteAlt:
	ld e,(hl)				
	inc hl
    ld d,(hl)
	inc hl
	ld (PaletteDataL_Var),de	
	
	ld e,(hl)				
	inc hl
    ld d,(hl)
	ld (PaletteDataR_Var),de	
	ret
	
SetPaletteBlack:
	ld hl,PaletteBlack
SetPaletteAlt1:
	ld e,(hl)				
	inc hl
    ld d,(hl)
	inc hl
	ld (PaletteDataL_Var),de	
	ld (PaletteDataR_Var),de	
	
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
DoPalFadeOutQ:
	xor a
	ld (FireDown),a
	inc a			;a=1
	call DoPalFadeOut
	
	jp cls
DoPalFadeInQ:
	xor a
	call DoPalFadeIn
	jp Set2D3Dmode

DoPalFadeIn:
	ld de,PaletteInPink
	ld hl,PaletteInBlue
	jr PalFade

DoPalFadeOut:
	ld de,PaletteOutPink
	ld hl,PaletteOutBlue
PalFade:
	or a
	jr nz,PalFadeBluePal
	ex de,hl
PalFadeBluePal:
	ld b,4
PalFadeB:
		call SetPaletteAlt1
		ei
		halt
		halt
		halt
		halt
		halt
		halt
		halt
	djnz PalFadeB
	ret

PaletteBlack:	
	dw PaletteDataBlack,PaletteDataBlack
	

PaletteInPink:
	dw PaletteDataBlack
	dw PaletteDataPink1
	dw PaletteDataPink2
	dw PaletteDataPink3
	
PaletteInBlue:
	dw PaletteDataBlack
	dw PaletteDataBlue1
	dw PaletteDataBlue2
	dw PaletteDataBlue3
	
	
PaletteOutPink:
	dw PaletteDataPink3
	dw PaletteDataPink2
	dw PaletteDataPink1
	dw PaletteDataBlack
	
PaletteOutBlue:
	dw PaletteDataBlue3
	dw PaletteDataBlue2
	dw PaletteDataBlue1
	dw PaletteDataBlack
	

PaletteDataBlack:
		;   --BBGGRR
		db %00000000	;0
		db %00000000	;0
		db %00000000	;0
		db %00000000	;0

PaletteDataPink1:		
		;   --BBGGRR
		db %00000000	;0
		db %00000000	;0
		db %00000000	;0
		db %00010001	;0	

PaletteDataPink2:		
		;   --BBGGRR
		db %00000000	;0
		db %00000000	;0
		db %00010001	;0
		db %00100010	;0	

PaletteDataPink3:		
		;   --BBGGRR
		db %00000000	;0
		db %00010001	;0
		db %00100110	;0	
		db %00110111	;0
		
		
		
PaletteDataBlue1:
		;   --BBGGRR
		db %00000000	;0
		db %00000000	;0
		db %00000000	;0
		db %00010000	;0	
		
PaletteDataBlue2:
		;   --BBGGRR
		db %00000000	;0
		db %00000000	;0
		db %00010000	;0
		db %00100000	;0
		
PaletteDataBlue3:
		;   --BBGGRR
		db %00000000	;0
		db %00100000	;0
		db %00110000	;0
		db %00110101	;0

PaletteDataL:
		;   --BBGGRR
		db %00000000	;0
		db %00000001	;1
		db %00000010	;2
		db %00000011	;3
		;   --BBGGRR
PaletteDataR:
		db %00000000	;0
		db %00010100	;1
		db %00111000	;2
		db %00111100	;3
		
PaletteDataG:
		db %00000000	;0
		db %00010101	;1
		db %00101010	;2
		db %00111111	;3
		
PaletteDataLC:
		;   --BBGGRR
		db %00000000	;0
		db %00000101	;1
		db %00001010	;2
		db %00001111	;3
		;   --BBGGRR
PaletteDataRC:
		db %00000000	;0
		db %00010000	;1
		db %00110000	;2
		db %00110000	;3
		
PaletteDataLM:
		;   --BBGGRR
		db %00000000	;0
		db %00000100	;1
		db %00001000	;2
		db %00001100	;3
		;   --BBGGRR
PaletteDataRM:
		db %00000000	;0
		db %00010001	;1
		db %00110010	;2
		db %00110011	;3
		
Palette:
	dw PaletteDataG,PaletteDataG
	dw PaletteDataL,PaletteDataR
	dw PaletteDataLC,PaletteDataRC
	dw PaletteDataLM,PaletteDataRM
	dw PaletteDataG,PaletteDataG
	dw PaletteDataG,PaletteDataG
	
SongGame:
	incbin "\ResAll\SuckHunt\GameSongF800.bin"
SongTitle:
	incbin "\ResAll\SuckHunt\TitleSongF800.bin"
SongIntro:
	incbin "\ResAll\SuckHunt\IntroSongF800.bin"

	read "\SrcALL\Multiplatform_ArkosTrackerLiteGBRom.asm"	
	read "\SrcALL\Multiplatform_ChibiSound.asm"			;Sound Driver	
	read "\SrcSMS\SMS_V1_AYemulator.asm"
Arkos_ChangeSong:
	ifdef Akuyou_MusicPos
		push hl
			call PLY_Stop
			di
		pop hl

		ld de,Akuyou_MusicPos
		ld bc,&400
		ldir
		
		call PLY_Init
		xor a
		ld (PlayMusicOff),a
	endif
	
	ret

	
ReadKeyRow:
	ld h,255
	ret

	read "\SrcALL\Multiplatform_ShowHex.asm"


FillAreaWithTiles:	;BC = X,Y	HL = W,H 	DE = Start Tile
	ld a,h				;Calculate End Xpos
	add b
	ld h,a
	ld a,l				;Calculate End Ypos
	add c
	ld l,a
FillAreaWithTiles_Yagain:
	push bc
		push hl
			call GetVDPScreenPos	;Move to the correcr VDP location
		pop hl	
FillAreaWithTiles_Xagain:;Tilemap takes two bytes, ---pcvhn nnnnnnnn
		ld a,e			;nnnnnnnn - Tile number
		out (vdpData),a	
		ld a,d			;---pcvhn - p=Priority (1=Sprites behind) C=color palette 
		out (vdpData),a	;(0=back 1=sprite), V=Vert Flip, H=Horiz Flip, N=Tilenum (0-511)
		inc de
		inc b			;Increase Xpos
		ld a,b
		cp h			;Are we at the end of the X-line?
		jr nz,FillAreaWithTiles_Xagain
	pop bc
	inc c				;Increase Ypos
	ld a,c
	cp l				;Are we at the end of the height Y-line?
	jr nz,FillAreaWithTiles_Yagain
	ret
	GetVDPScreenPos:	;Move to a memory address in VDP by BC cursor pos
	push bc				;B=Xpos, C=Ypos
		ifdef BuildSGG
			ld a,c
			add 3		;Need add 3 on Ypos for GG to reposition screen
			ld h,a
		else 
			ld h,c
		endif
		xor a			
		rr h			;Multiply Y*64
		rra
		rr h
		rra
		rlc b			;Multiply X*2 (Two byte per tile)
		or b
		ifdef BuildSGG
			add 6*2		;Need add 6 on Xpos for GG to reposition screen
		endif
		ld l,a
		ld a,h
		add &38			;Address of TileMap &3800 
		ld h,a				;(32x28 - 2 bytes per cell = &700 bytes)
		call prepareVram
	pop bc
	ret	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;										Footer													

BitmapData:	
	incbin "\ResALL\SuckHunt\SuckHuntSMS.RAW"
BitmapDataEnd:
FontData:
	incBin "\ResALL\SuckHunt\SuckHuntSMS_Cursors.RAW"
	ds 16
	incBin "\ResALL\SuckHunt\FontSMS.RAW"

DefineGameTiles:	
	
	ld IY,BitmapData
	ld de,&0
	ld H,166
	ld l,2
	call DefineSpriteA	;DE=VDP address, IY=Source,H=Width,L=Height
	
	ld IY,FontData
	ld de,333*32
	ld H,4
	ld l,26
	call DefineSpriteA	;DE=VDP address, IY=Source,H=Width,L=Height

	;ld bc,&0000		;Start Position in BC
	;ld hl,&2018		;Width/Height of the area to fill with tiles in HL
						;We need to load DE with the first tile number we want 
						;to fill the area with.
;	ld de,0		;SMS has 512 tiles, so start at 256
	
;	call FillAreaWithTiles		;Fill a grid area with consecutive tiles 

	ret
	
TitleData:		
	incbin "\ResALL\SuckHunt\SuckHuntSMS_Title.RAW"
TitleDataEnd:			

GameOverData:		
	incbin "\ResALL\SuckHunt\SuckHuntSMS_GameOver.RAW"


	

TitleL:
  db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,0,0
  db 0,0,0,0,0,0,0,0,5,6,5,6,7,0,0,0,8,9,10,11,0,0,0,0,0,0,12,13,14,15,0,0
  db 0,0,0,0,0,0,0,16,6,5,6,5,17,0,0,0,18,19,20,21,0,0,0,0,0,0,0,0,0,0,0,0
  db 0,5,6,5,6,7,0,22,23,24,0,0,0,0,0,0,25,26,27,28,0,0,0,5,6,5,6,7,0,0,0,0
  db 16,6,5,6,5,17,0,29,30,30,31,24,0,0,0,0,32,33,34,35,0,0,16,6,5,6,5,17,0,0,0,0
  db 0,0,0,36,37,38,39,40,30,30,41,42,43,0,0,0,0,0,0,0,0,0,0,0,0,44,45,46,0,0,0,0
  db 0,0,47,48,49,50,51,0,52,30,53,54,55,56,0,0,0,0,0,0,57,58,59,60,0,0,61,0,0,0,0,0
  db 0,0,62,63,64,65,66,0,67,54,55,68,69,70,0,0,0,0,0,0,71,72,73,74,0,0,0,0,75,76,0,0
  db 65,77,78,79,80,81,82,39,83,68,69,41,30,43,84,31,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 85,86,87,88,89,90,91,92,93,41,30,94,30,30,95,30,0,96,97,0,0,0,0,0,0,98,99,0,0,98,99,0
  db 100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,30,115,30,116,117,43,118,0,0,119,120,121,122,119,120,121,122
  db 123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,41,138,139,140,141,142,143,144,0,0,0,0,0,0,0,0,0
  db 145,146,147,148,149,150,0,151,152,153,154,155,156,157,158,159,160,161,162,41,163,164,165,0,166,167,168,169,170,171,0,0
  db 0,0,0,172,173,174,175,30,30,176,0,30,30,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,0
  db 0,0,0,0,195,0,196,30,30,197,198,30,30,199,200,201,202,151,203,204,205,206,207,208,209,210,211,212,213,214,215,0
  db 0,0,0,0,0,0,200,30,30,216,217,30,30,0,30,218,219,176,219,220,218,221,222,223,224,225,226,227,228,229,230,0
  db 0,0,0,0,0,175,30,30,30,199,200,30,176,0,231,232,233,234,233,235,236,237,0,238,239,240,241,242,243,244,0,0
  db 0,0,0,0,0,245,246,246,246,0,247,247,248,0,0,0,0,0,0,0,0,0,0,0,249,250,251,252,253,254,255,0,255,1
  db 0,0,0,0,0,0,0,0,0,0,0,0,255,2,255,3,68,0,30,255,4,255,5,0,0,0,0,255,6,255,7,255,8,255,9,255,10,255,11,255,12,255,13,255,14
  db 255,15,255,16,255,17,255,18,0,0,0,0,0,0,0,0,0,255,19,255,20,0,30,255,21,30,0,0,255,22,255,23,0,255,24,255,25,255,26,255,27,255,28,255,29,255,30,255,31
  db 255,32,0,255,33,255,34,255,35,0,0,255,22,255,23,0,0,0,255,36,255,37,255,38,0,30,255,39,113,0,0,255,40,255,41,255,42,0,0,255,43,255,43,255,43,0,255,22,255,23
  db 255,44,255,45,255,46,255,47,255,48,255,49,0,255,40,255,41,255,42,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,40,255,41
  db 255,50,255,51,255,52,255,53,255,54,255,55,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,22,255,23,0,0,0,0
  db 255,56,255,57,255,58,255,59,255,60,255,61,255,62,255,63,255,64,255,65,255,66,255,67,255,68,255,69,255,70,255,71,0,0,0,255,43,255,43,255,43,0,0,0,0,255,40,255,41,255,42,0,0,0


TitleR:
  db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,0,0,0
  db 0,0,0,0,0,0,0,0,5,6,5,6,7,0,0,8,9,10,11,0,0,0,0,0,0,12,13,14,15,0,0,0
  db 0,0,0,0,0,0,0,16,6,5,6,5,17,0,0,18,19,20,21,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 0,5,6,5,6,7,0,0,23,24,0,0,0,0,0,25,26,27,28,0,0,0,0,5,6,5,6,7,0,0,0,0
  db 16,6,5,6,5,17,0,0,255,72,30,68,255,73,0,0,0,32,33,34,35,0,0,0,16,6,5,6,5,17,0,0,0,0
  db 0,0,0,0,36,37,38,39,255,74,30,255,75,55,53,24,0,0,0,0,0,0,0,0,0,0,255,76,255,77,255,78,255,79,0,0,0,0
  db 0,0,0,47,48,49,50,51,93,231,68,69,41,42,0,0,0,0,0,255,80,255,81,255,82,255,83,0,0,255,84,255,85,0,0,0,0,0
  db 0,0,0,62,63,64,65,66,202,255,86,52,30,43,93,0,0,0,0,0,255,87,255,88,255,89,255,90,0,0,0,0,255,91,255,92,255,93,0,0
  db 255,94,65,77,78,79,80,81,82,40,30,255,95,255,96,55,94,24,151,255,97,0,0,0,255,98,255,99,0,0,0,0,0,0,0,0,0,0
  db 255,100,85,86,87,255,101,255,102,90,91,92,255,103,255,104,30,255,105,30,255,106,30,255,107,175,68,0,0,0,0,0,98,99,0,0,98,99,0,0
  db 255,108,100,101,102,255,109,255,110,105,106,255,111,255,112,109,255,113,255,114,255,115,255,116,30,255,117,96,255,118,255,119,31,255,120,0,119,120,121,122,119,120,121,122,119
  db 255,121,123,124,125,126,127,128,129,130,131,132,255,122,255,123,255,124,255,125,52,138,30,255,126,30,255,127,255,128,255,129,0,0,0,0,0,0,0,0,0
  db 0,145,146,147,148,149,150,255,130,151,255,131,255,132,255,133,255,134,255,135,255,136,255,137,255,138,136,137,41,255,139,255,140,255,141,0,166,167,168,169,170,171,0,0
  db 0,0,0,0,255,142,255,143,174,219,30,30,255,144,95,30,255,145,255,146,255,147,255,148,255,149,255,150,255,151,255,152,255,153,255,154,187,188,189,190,191,192,193,194,0
  db 0,0,0,0,255,155,255,156,0,30,30,30,255,157,255,158,30,255,159,255,160,30,255,161,151,255,162,255,163,255,164,255,165,255,166,208,209,210,211,212,213,214,215,0
  db 0,0,0,0,0,0,175,30,30,30,255,167,30,30,255,168,143,30,235,30,255,169,255,170,255,171,255,172,255,173,223,224,225,226,227,228,229,230,0
  db 0,0,0,0,0,0,96,30,30,176,175,30,30,255,144,255,174,138,255,175,139,96,255,176,218,255,177,0,238,239,240,241,242,243,244,0,0
  db 0,0,0,0,0,0,246,246,246,255,178,93,247,247,0,0,0,0,0,0,0,0,0,0,0,249,250,251,252,253,254,255,0,255,1
  db 0,0,0,0,0,0,0,0,0,0,0,0,255,179,255,3,68,0,30,255,180,68,0,0,0,0,255,6,255,7,255,8,255,9,255,10,255,11,255,12,255,13,255,14
  db 255,15,255,16,255,17,255,18,0,0,0,0,0,0,0,0,255,181,255,19,255,20,0,30,199,30,255,182,0,0,255,183,0,255,24,255,25,255,26,255,27,255,28,255,29,255,30,255,31
  db 255,32,0,255,33,255,34,255,35,0,0,0,255,183,0,0,0,255,184,255,37,255,38,0,30,255,185,255,38,0,0,255,186,255,187,255,188,0,0,255,189,255,190,255,190,255,191,0,255,183
  db 255,44,255,45,255,46,255,47,255,48,255,49,0,255,186,255,187,255,188,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,186,255,187
  db 255,50,255,51,255,52,255,53,255,54,255,55,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,183,0,0,0,0
  db 255,56,255,57,255,58,255,59,255,60,255,61,255,62,255,63,255,64,255,65,255,66,255,67,255,68,255,69,255,70,255,71,0,0,0,255,189,255,190,255,190,255,191,0,0,0,255,186,255,187,255,188,0,0,0

GameOverL:
  db 0,0,0,0,0,1,2,3,0,0,0,0,0,0,0,0
  db 0,0,0,0,0,4,5,6,7,8,0,0,0,0,0,0
  db 0,0,0,0,0,0,0,9,10,11,12,0,0,0,0,0
  db 0,0,0,0,13,14,15,16,17,18,19,20,21,22,0,0
  db 0,0,0,23,24,25,26,27,28,29,30,31,32,33,34,0
  db 0,0,35,36,37,38,39,40,41,42,43,44,45,46,0,0
  db 0,0,47,48,49,50,51,52,53,54,55,56,57,58,59,60
  db 0,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75
  db 76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,0
  db 91,92,93,0,94,95,96,97,98,99,100,101,102,103,104,105
  db 106,107,108,0,109,110,111,112,113,114,115,116,117,118,119,120
  db 121,122,123,0,124,125,126,0,127,128,129,130,131,132,133,134

GameOverR:
  db 0,0,0,0,0,1,2,3,0,0,0,0,0,0,0,0
  db 0,0,0,0,0,4,5,135,136,137,0,0,0,0,0,0
  db 0,0,0,0,0,0,0,138,139,140,141,0,0,0,0,0
  db 0,0,0,0,13,142,143,144,145,146,147,148,149,150,0,0
  db 0,0,0,23,24,151,152,153,154,155,156,157,158,159,0,0
  db 0,0,35,36,37,160,161,162,163,164,165,166,167,168,0,0
  db 0,0,47,48,169,50,170,171,172,173,174,175,176,177,178,179
  db 0,61,62,63,180,181,182,183,184,185,186,187,188,189,190,191
  db 192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,0
  db 207,208,209,0,210,211,212,213,214,215,216,217,218,219,220,221
  db 222,223,224,0,225,226,227,228,229,230,231,232,233,234,235,236
  db 237,238,239,0,240,241,242,0,243,244,245,246,247,248,249,250


	org &7FF0
	db "TMR SEGA"	;Fixed data (needed by some SGG)
	db 0,0			;Reserved
	db &69,&69		;16 bit Checksum (sum of bytes $0000-$7FEF... Little endian)
					;Only needed for 'Export SMS', not checked by emulator without bios
	db 0,0,0 		;BCD Product Code & Version
	
	ifdef BuildSGG	;Region & Rom size (see below) - only checked by SMS export bios
		db &6C		;GG Export - 32k
	else
		db &4C		;SMS Export - 32k
	endif

;&3- SMS Japan 
;&4- SMS Export 
;&5- GG 	Japan 
;&6- GG 	Export 
;&7- GG 	International 
;&-C 32KB   
;&-F 128KB   
;&-0 256KB   
;&-1 512KB


wavedata:
	incbin "\ResALL\SuckHunt\operationsucka1-6.raw"
wavedataEnd:


	read "\SrcALL\Multiplatform_ChibiWave.asm"

	
Intro1:
  db 0,0,0,0,0,1,1,2,0,0,0,0,3,4,3,5
  db 0,0,0,0,0,6,7,8,9,0,0,0,10,11,12,13
  db 14,0,0,0,0,15,16,17,9,0,0,0,18,19,20,21
  db 3,22,0,0,0,23,24,15,25,0,0,0,26,27,28,9
  db 29,30,31,6,32,33,34,35,6,0,36,37,23,38,39,9
  db 10,40,41,30,42,43,44,45,46,47,48,49,9,6,50,16
  db 51,48,52,53,54,55,56,57,58,59,60,61,9,21,62,27
  db 0,63,64,65,66,67,68,69,70,71,72,73,23,24,24,24
  db 0,0,0,74,75,76,77,7,35,78,0,0,79,80,24,38
  db 0,0,0,0,0,23,81,82,83,84,0,0,85,86,87,88
  db 0,0,0,0,6,22,89,90,39,91,0,0,92,93,94,95
  db 0,0,0,96,24,24,24,38,25,9,0,0,23,97,98,99
Intro1b:
  db 0,0,0,0,0,1,1,2,0,0,0,0,3,4,3,5
  db 0,0,0,0,0,6,7,8,9,0,0,0,10,11,12,13
  db 0,0,0,0,0,15,16,17,9,0,0,0,18,19,20,21
  db 0,0,0,0,0,23,24,15,25,0,0,0,26,27,28,9
  db 31,6,32,33,0,0,27,35,6,0,0,0,255,86,255,24,39,255,87
  db 41,48,42,43,255,88,0,75,45,46,0,0,47,48,49,77,255,89
  db 52,53,54,55,56,255,90,255,91,57,58,255,92,255,93,59,60,61,255,94,255,95
  db 64,65,66,67,68,0,255,96,69,35,0,255,97,71,72,255,98,24,24
  db 0,74,75,76,0,0,77,7,35,255,99,0,78,79,80,24,38
  db 0,0,0,0,0,23,81,82,83,84,0,0,85,86,87,88
  db 0,0,0,0,6,22,89,90,39,91,0,0,92,93,94,95
  db 0,0,0,96,24,24,24,38,25,9,0,0,23,97,98,99


  
Intro2:
  db 0,100,101,102,0,103,2,104,0,104,0,105,0,0,0,105
  db 0,0,0,100,106,0,0,74,0,104,0,107,0,0,0,107
  db 0,0,0,108,109,101,0,0,12,110,111,112,0,0,0,113
  db 0,0,114,115,53,48,116,0,117,118,119,120,0,0,0,107
  db 0,0,121,53,48,122,10,48,48,48,123,124,125,126,0,113
  db 0,127,128,129,130,131,132,133,134,48,135,136,137,138,139,107
  db 140,141,142,78,143,144,24,145,146,147,48,48,48,148,149,113
  db 0,140,150,0,9,0,103,102,151,152,153,154,155,156,157,158
  db 0,159,160,0,161,102,0,0,151,162,163,164,165,166,167,168
  db 0,169,0,0,0,0,103,102,170,162,171,169,172,173,169,150
  db 0,174,0,0,0,175,0,0,151,176,177,169,178,179,169,160
  db 0,0,0,0,0,180,0,0,181,182,183,169,184,139,174,185
Intro2b:
  db 101,255,100,0,102,0,103,2,104,0,104,0,105,0,0,0,105
  db 0,100,255,101,2,255,102,0,0,74,0,104,0,107,0,0,0,107
  db 0,108,109,255,103,255,0,0,0,0,12,110,111,112,0,0,0,113
  db 114,115,255,104,48,116,0,0,0,117,118,119,120,0,0,0,107
  db 121,255,105,48,122,10,48,48,48,48,48,123,124,125,126,0,113
  db 128,129,130,131,132,133,133,133,134,48,135,136,137,138,139,107
  db 142,78,255,106,144,24,145,8,46,255,107,147,48,48,48,148,149,113
  db 150,255,108,0,0,9,0,103,102,151,152,153,154,155,156,157,158
  db 160,159,0,0,161,102,0,0,151,162,163,164,165,166,167,168
  db 0,169,0,0,0,0,103,102,170,162,171,169,172,173,169,150
  db 0,174,0,0,0,175,0,0,151,176,177,169,178,179,169,160
  db 0,0,0,0,0,180,0,0,181,182,183,169,184,139,174,185


Intro3:
  db 0,0,0,186,9,0,0,187,7,188,189,48,48,190,191,192
  db 0,0,193,0,0,0,0,30,139,0,194,195,24,196,197,198
  db 0,0,199,200,0,0,186,0,201,0,202,203,204,205,0,206
  db 0,207,191,208,139,209,0,0,0,202,210,211,212,213,126,0
  db 0,214,0,215,208,139,0,0,202,216,217,117,218,219,220,126
  db 0,221,0,0,192,222,223,202,224,225,226,151,227,147,228,229
  db 230,0,0,0,0,231,232,224,0,169,233,234,235,236,139,0
  db 237,0,0,238,239,0,0,0,0,169,231,240,0,241,242,0
  db 9,0,243,244,214,245,0,0,0,169,246,247,248,0,249,0
  db 0,0,250,251,252,253,254,254,0,113,246,0,0,255,0,241,139
  db 0,0,0,255,1,255,2,255,3,255,4,255,5,0,255,6,255,7,255,8,247,0,255,9,242
  db 0,0,0,96,255,10,255,11,255,12,255,13,96,246,0,248,0,255,8,0,255,14
Intro3b:
  db 0,0,0,186,9,0,0,187,7,188,189,48,48,190,191,192
  db 0,0,193,0,0,0,0,30,139,0,194,195,24,196,197,198
  db 0,0,199,200,0,0,186,0,201,0,202,203,204,205,0,206
  db 0,207,191,208,139,209,0,0,0,202,210,211,212,213,126,0
  db 0,214,0,215,208,139,0,0,202,216,217,117,218,219,220,126
  db 0,221,0,0,192,222,223,202,224,225,226,151,227,147,228,229
  db 230,0,0,0,0,255,109,232,224,0,169,233,234,235,236,139,0
  db 237,0,0,255,110,255,111,253,254,254,0,169,231,240,0,241,242,0
  db 9,0,243,255,112,255,2,255,3,255,4,255,5,0,169,246,247,248,0,249,0
  db 0,0,250,255,113,255,114,255,11,255,12,255,13,0,113,246,0,0,255,0,241,139
  db 0,0,0,96,255,115,48,255,116,255,117,0,255,6,255,7,255,8,247,0,255,9,242
  db 0,0,0,0,255,118,255,119,48,255,120,255,121,246,0,248,0,255,8,0,255,14

  
Intro4:
  db 255,15,255,16,101,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 255,17,255,18,255,19,255,20,110,37,101,101,0,0,0,255,21,255,22,255,22,255,22,255,19
  db 255,23,255,24,255,25,255,26,255,27,255,28,255,19,255,29,255,30,255,31,255,32,255,33,255,34,0,0,255,35
  db 255,18,255,19,255,19,255,30,30,255,23,255,24,255,36,255,37,152,255,19,48,30,30,255,15,37
  db 255,38,255,39,255,40,255,27,255,17,255,18,255,19,255,41,255,37,255,37,255,37,255,19,255,37,255,37,147,48
  db 134,255,42,255,43,255,44,255,45,255,46,255,46,255,47,255,37,255,48,255,39,255,3,255,37,255,49,255,50,255,51
  db 0,255,52,255,53,255,54,255,55,255,42,255,56,255,57,255,37,255,48,255,39,255,58,255,59,255,60,101,0
  db 0,0,0,0,0,255,61,255,62,255,37,255,48,255,38,255,63,255,64,255,65,255,66,255,23,255,67
  db 0,0,0,0,0,0,0,255,68,134,255,42,255,43,255,69,255,23,255,70,255,71,48
  db 0,0,0,0,0,0,96,24,255,24,24,255,68,133,255,72,255,73,48,255,74
  db 0,0,0,0,0,0,0,255,75,255,76,255,77,255,78,24,255,79,48,48,255,80
  db 0,0,0,0,0,0,0,0,0,255,81,255,82,255,83,255,84,255,23,255,85,133
Intro4b:
  db 255,15,255,16,101,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 255,17,255,18,255,19,255,20,110,37,101,101,0,0,0,255,21,255,22,255,22,255,22,255,19
  db 255,23,255,24,255,25,255,26,255,27,255,28,255,19,255,29,255,30,255,31,255,32,255,33,255,34,0,0,255,35
  db 255,18,255,19,255,19,255,30,30,255,23,255,24,255,36,255,37,152,255,19,48,30,30,255,15,37
  db 255,38,255,39,255,40,255,27,255,17,255,18,255,19,255,41,255,37,255,37,255,37,255,19,255,37,255,37,147,48
  db 134,255,42,255,43,255,44,255,45,255,46,255,46,255,47,255,37,255,48,255,39,255,3,255,37,255,122,255,123,255,74
  db 0,255,52,255,53,255,54,255,55,255,42,255,56,255,57,255,37,255,48,255,39,255,58,255,59,255,124,255,125,255,126
  db 0,0,0,0,0,255,61,255,62,255,37,255,48,255,38,255,63,255,64,255,127,255,128,255,73,48
  db 0,0,0,0,0,0,0,255,68,134,255,42,255,43,255,37,255,129,255,130,48,48
  db 0,0,0,0,0,0,96,24,255,24,24,255,68,133,255,131,255,132,255,133,255,134
  db 0,0,0,0,0,0,0,255,75,255,76,255,77,255,78,24,24,255,135,255,136,255,137
  db 0,0,0,0,0,0,0,0,0,255,81,255,82,255,83,75,255,138,255,68,255,139


IntroAnimList:
	db 0,12,1
	dw Intro1
	dw Intro1b
	db 32-16,0,0
	dw Intro3
	dw Intro3b
	db 32-16,12,0
	dw Intro2
	dw Intro2b
	db 0,0,1
	dw Intro4
	dw Intro4b
	
	
IntroData:
	incbin "\ResALL\SuckHunt\SuckHuntSMS_Intro.RAW"
	
