
PrintTile:	;TitleTiles
	push af
	push ix
	push bc
	push hl
	push de	
	jp PrintTileb
TileEngine_Plus2:

PrintSpace:
	ld a,' '
Printchar:
	push af
	push ix
	push bc
	push hl
	push de
		sub 32				;No Char below 32
		ld bc,FontData
PrintTileb:
		call PrintCharB
PrintTilec:
		ld hl,CursorX
		inc (hl)
	pop de
	pop hl
	pop bc
	pop ix
	pop af
	ret

PrintDoubleHeight:		;Used for Intro graphics (8x4 tiles)
	ld l,a
	xor a
	sla l				;Tile * 8
	rla
	sla l				;Tile * 8
	rla
	sla l				;Tiles * 8
	rla
	ld h,a
	add hl,bc
	ex de,hl
	ld bc,(CursorY)		;Get XY into BC
	rl c
	rl c
	rl c
	rl b	
	call GetScreenPos
	ld b,4			;Lines (Height)
DSpriteNextLine:
	call DDoTileLine
	dec hl		;INC Dest (Screen) Address
	dec de		;INC Source (Sprite) Address
	call DDoTileLine
	dec hl		;INC Dest (Screen) Address
	inc de		;INC Source (Sprite) Address

	djnz DSpriteNextLine	;Repeat for next line
	jr PrintTilec

DDoTileLine:
	ld a,(de)
	ld (hl),a	;Screen Destination
	inc de		;INC Source (Sprite) Address
	inc hl		;INC Dest (Screen) Address
	ld a,(de)
	ld (hl),a	;Screen Destination
	jp GetNextLine	;Scr Next Line (Alter HL to move down a line)
	

PrintCharB:		;Print Char A with Sprite Routine
	ld h,a
	xor a
	srl h				;Tile * 16
	rra
	srl h
	rra
	srl h
	rra
	srl h
	rra
	ld l,a
	add hl,bc
	ex de,hl
	
	ld bc,(CursorY)		;Get XY into BC
	ld iy,SprChar

	rl c				;Ypos *4
	rl c
	rl b				;Xpos *4
	rl b
	ld a,b
	add 48+2
	ld  (IY+O_Xpos),a	;Cursor X to pixel pos

	ld a,c
	add 2
	ld  (IY+O_Ypos),a	;Cursor Y to pixel pos
	
	ld  (IY+O_SprL),e	;Sprite Data
	ld  (IY+O_SprH),d
	
	ld a,(CursorZ)
	ld  (IY+O_Zpos),a
	jp DrawSpriteFromIY		;Use the sprite 

ShowCrosshair:
	ld de,(CrosshairSprite)	;Sprite Source
	ld ix,&0802
	call GetScreenPos
	push de
	push hl
		call ShowSpriteL
	pop hl
	pop de
	jr ShowSpriteR

ShowSpriteL:
	ld b,ixh			;Lines (Height)
SpriteNextLine:
	push hl
		ld c,ixl		;Bytes per line (Width)
SpriteNextByte:
		ld a,(de)
ShowSpriteL2DMode:
		and %00001111	;3D MODE (Self mod)
		rlca			;3D Mode
		rlca
		rlca
		rlca
ShowSpriteL2DModeSkip:
		xor (hl)
		ld (hl),a	;Screen Destination

		inc de		;INC Source (Sprite) Address
		inc hl		;INC Dest (Screen) Address

		dec c 		;Repeat for next byte
		jr nz,SpriteNextByte
	pop hl
	call GetNextLine	;Scr Next Line (Alter HL to move down a line)
NextLineCommand1_Plus2:

	djnz SpriteNextLine	;Repeat for next line
	ret			;Finished


ShowSpriteR:
;	RET ;2D Mode
	ld b,ixh			;Lines (Height)

SpriteNextLineR:
	push hl
		ld c,ixl	;Bytes per line (Width)
SpriteNextByteR:
		ld a,(de)
		and %00001111	;3D MODE

		xor (hl)
		ld (hl),a	;Screen Destination

		inc de		;INC Source (Sprite) Address
		inc hl		;INC Dest (Screen) Address

		dec c 		;Repeat for next byte
		jr nz,SpriteNextByteR
	pop hl

	call GetNextLine	;Scr Next Line (Alter HL to move down a line)
NextLineCommand2_Plus2:
	djnz SpriteNextLineR	;Repeat for next line

	ret			;Finished

GetNextLineWithClip:
	push hl
		ld hl,1
SpriteHClipPlus2:
		add hl,de
		ex de,hl
	pop hl 
	jp GetNextLine

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GetScreenPos:	;return memory pos in HL of screen co-ord B,C (X,Y)
;	; Input  BC= XY (x=bytes - so 80 across)
;	; output HL= screen mem pos

;Looking at Y line (in C) - we need to take each set of bits, and work with them separately:
;YYYYYYYY	Y line number (0-200)
;-----YYY	(0-7)  - this part needs to be multiplied by &0800 and added to the total
;YYYYY---	(0-31) - This part needs to be multiplied by 80 (&50)

	push de
		;screen is 80 bytes wide = -------- %01010000
		ld a,C		; 00000000 01010000
		and %11111000 	; -------- -YYYY---
		ld h,0
		ld l,a
				; 00000000 01010000
		
		sla l		; -------- YYYY----
		rl h
		
		ld d,h		;value is in first bit position -add it
		ld e,l	
				; 00000000 01010000
		sla e		; -------Y YYY-----
		rl d		;	    
		sla e		; ------YY YY------
		rl d
		
		add hl,de	;value is in 2nd bit position - add it
		;We've now effectively multiplied by 80 (&50)

	;-----YYY	(0-7)  - this part needs to be multiplied by &0800 and added to the total
		ld a,C
		and %00000111	
		
		rlca		;X8
		rlca
		rlca

		ld d,a		;Load into top byte, and add as 16 bit
		ld e,0

		add hl,de
		;We've now effectively multiplied by 8

	;Screen Base
		ld a,&C0	;Add the screen Base &C000
		ld d,a

	;X position
		ld e,B		;Add the X pos 
		add hl,de
	pop de

	ret 		;return memory location in hl
	
GetNextLine:
	push af
		ld a,h		;Add &08 to H (each CPC line is &0800 bytes below the last
		add &08
		ld h,a
			;Every 8 lines we need to jump back to the top of the memory range to get the correct line
			;The code below will check if we need to do this - yes it's annoying but that's just the way the CPC screen is!
		bit 7,h		;Change this to bit 6,h if your screen is at &8000!
		jp nz,GetNextLineDone
		push bc
			ld bc,&c050	;if we got here we need to jump back to the top of the screen - the command here will do that
			add hl,bc
		pop bc
	GetNextLineDone:
	pop af
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef Enable_Intro

ShowTileMapAltPosQtrIntro:
		ld de,PrintDoubleHeight
		ld (TileEngine_Plus2-2),de
		ld de,256*8-8
		ld (TileBankSize_Plus2-2),de

		ld bc,IntroGraphics
	endif

ShowTileMapAltPosQtr:
	ld d,16		;W
	ld e,12		;H
	jr ShowTileMapAltPos
	
ShowTileMap:		;HL=Tilemap BC=Tile Data
	ld de,PrintTileb
	ld (TileEngine_Plus2-2),de
	ld de,256*16-16
	ld (TileBankSize_Plus2-2),de

	ld iyh,4	;X
	ld a,0		;Y
	ld e,24
	ld d,32
ShowTileMapAltPos:			;HL=Tilemap BC=Tile Data IYH=X	
	ld (CursorY),a			;A=Y	;E=Height ;D=Wid
	xor a
	ld (CursorZ),a
TilemapYagain:
	ld a,iyh
	ld (CursorX),a	
	push iy
	push de
TilemapXagain:
	ld a,(hl)
	inc hl
	cp 255 
	jr c,FirstTileBank		;Value <255

	ld a,(hl)
	inc hl
	push bc
	push hl
		ld hl,256*16-16		;Calculate offset to second 255 tiles
TileBankSize_Plus2:
		add hl,bc
		ld b,h
		ld c,l
		call PrintTile		;Show 255+ Tile
	pop hl
	pop bc
	jr TileDone

FirstTileBank:
	call PrintTile			;Show 0-254 tile
TileDone:
	dec d
	jr nz,TilemapXagain
	push hl
		call NewLine
	pop hl
	pop de
	pop iy
	dec e
	jr nz,TilemapYagain
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; 		Sprites

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

FontData:				;Bitmap Font (4 color)
	ds 16	;Space char
	incbin "\ResALL\SuckHunt\FontCPC.raw"

SprCursorSprite:
	incbin "\ResALL\SuckHunt\SuckHuntCPC_Cursors.RAW"

S_AddressL equ 0
S_AddressH equ 1
S_Wid equ 2
S_Hei equ 3

SpriteBase equ SpriteData
PixelsPerByte equ 4

SpriteData:
	incbin "\ResALL\SuckHunt\SuckHuntCPC.RAW"

;Exported by Akusprite Editor
SpriteInfo:
    dw &0000/PixelsPerByte + SpriteBase           ;SpriteAddr 0
     db 32,32                                     ;XY 
    dw &0400/PixelsPerByte + SpriteBase           ;SpriteAddr 1
     db 24,16                                     ;XY 
    dw &0580/PixelsPerByte + SpriteBase           ;SpriteAddr 2
     db 24,16                                     ;XY 
    dw &0700/PixelsPerByte + SpriteBase           ;SpriteAddr 3
     db 24,16                                     ;XY 
    dw &0880/PixelsPerByte + SpriteBase           ;SpriteAddr 4
     db 24,16                                     ;XY 
    dw &0A00/PixelsPerByte + SpriteBase           ;SpriteAddr 5
     db 16,16                                     ;XY 
    dw &0B00/PixelsPerByte + SpriteBase           ;SpriteAddr 6
     db 40,48                                     ;XY 
    dw &1280/PixelsPerByte + SpriteBase           ;SpriteAddr 7
     db 40,48                                     ;XY 
    dw &1A00/PixelsPerByte + SpriteBase           ;SpriteAddr 8
     db 32,32                                     ;XY 
    dw &1E00/PixelsPerByte + SpriteBase           ;SpriteAddr 9
     db 32,32                                     ;XY 
    dw &2200/PixelsPerByte + SpriteBase           ;SpriteAddr 10
     db 24,24                                     ;XY 
    dw &2440/PixelsPerByte + SpriteBase           ;SpriteAddr 11
     db 24,24                                     ;XY 
    dw &2680/PixelsPerByte + SpriteBase           ;SpriteAddr 12
     db 48,32                                     ;XY 
    dw &2C80/PixelsPerByte + SpriteBase           ;SpriteAddr 13
     db 48,32                                     ;XY 
    dw &3280/PixelsPerByte + SpriteBase           ;SpriteAddr 14
     db 32,24                                     ;XY 
    dw &3580/PixelsPerByte + SpriteBase           ;SpriteAddr 15
     db 32,16                                     ;XY 
    dw &3780/PixelsPerByte + SpriteBase           ;SpriteAddr 16
     db 24,16                                     ;XY 
    dw &3900/PixelsPerByte + SpriteBase           ;SpriteAddr 17
     db 24,16                                     ;XY 
    dw &3A80/PixelsPerByte + SpriteBase           ;SpriteAddr 18
     db 32,24                                     ;XY 
    dw &3D80/PixelsPerByte + SpriteBase           ;SpriteAddr 19
     db 32,32                                     ;XY 
    dw &4180/PixelsPerByte + SpriteBase           ;SpriteAddr 20
     db 24,16                                     ;XY 
    dw &4300/PixelsPerByte + SpriteBase           ;SpriteAddr 21
     db 24,24                                     ;XY 
    dw &4540/PixelsPerByte + SpriteBase           ;SpriteAddr 22
     db 16,16                                     ;XY 
    dw &4640/PixelsPerByte + SpriteBase           ;SpriteAddr 23
     db 16,16                                     ;XY 
    dw &4740/PixelsPerByte + SpriteBase           ;SpriteAddr 24
     db 40,61                                     ;XY 
    dw &50C8/PixelsPerByte + SpriteBase           ;SpriteAddr 25
     db 40,61                                     ;XY 
    dw &5A50/PixelsPerByte + SpriteBase           ;SpriteAddr 26
     db 40,39                                     ;XY 
    dw &6068/PixelsPerByte + SpriteBase           ;SpriteAddr 27
     db 40,39                                     ;XY 
    dw &6680/PixelsPerByte + SpriteBase           ;SpriteAddr 28
     db 24,6                                      ;XY 
    dw &6710/PixelsPerByte + SpriteBase           ;SpriteAddr 29
     db 24,16                                     ;XY 
    dw &6890/PixelsPerByte + SpriteBase           ;SpriteAddr 30
     db 48,16                                     ;XY 
    dw &6B90/PixelsPerByte + SpriteBase           ;SpriteAddr 31
     db 32,16                                     ;XY 
