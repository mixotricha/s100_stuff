TitleTiles3D equ &C100

Akuyou_MusicPos equ &4000

Enable_Intro equ 1
Enable_Speech equ 1

;Enable this for WinAPE build

GunSprite equ 1

VscreenMinX equ 64		;Top left of visible screen in logical co-ordinates
VscreenMinY equ 0

VscreenWid equ 128		;Screen Size in logical units
VscreenHei equ 96
VscreenWidClip equ 0

ScreenSize256 equ 1
pixelsPerByte2 equ 1 	;Use full logical res

ScreenObjWidth equ 80-12
ScreenObjHeight equ 200-48

Use3DTilemaps equ 1		;Gameover in 3d?

GameRam equ &5000

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef vasm
		include "\SrcALL\VasmBuildCompat.asm"
	else
		read "\SrcALL\WinApeBuildCompat.asm"
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	read "\SrcALL\CPU_Compatability.asm"

	
	;The Spectrum NEXT cpu has some extra opcodes for setting it's registers,
	;We'll define a couple of Macros to help us out.
	
	macro nextreg,reg,val
		db &ED,&91,\reg,\val;Macro for Setting A ZX Next Register
	endm
	macro nextregA,reg
		db &ED,&92,\reg		;Macro for Setting A ZX Next Register from A
	endm
	macro GetNextReg,reg
        push bc
            ld bc,&243B
                ld a,\reg
                out (c),a
                inc b
             in a,(c)
        pop bc
    endm
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
		
	Macro DoEI
		;Call EnableInterrupts
		ei
		;Test bit 7 of $22 
	endm
	macro DoDI
		;di
	endm
		
		
	Org &6000				;Code Origin
	di
	ld sp,&4F00
;Palette Init
                ;IPPPSLUN
    nextreg &43,%00010000   ;Layer 2 to 1st palette
	
	
	
;Set transparent color to something we don't need!
				;RRRGGGBB
	nextreg &14,%00000100	;Transparency Color
	
	xor a
	out (&fe),a				;Black border


	nextreg &50,16+8		;Page Extra ram at &0000-&2000
							;&2000-&4000 is VRAM window
		
	di
	ld hl,&0038				;IM1 Interrupt address
	ld a,&C3				;JP
	ld (hl),a
	inc hl
	ld de,InterruptHandler
	ld (hl),e
	inc hl
	ld (hl),d
	
	

;Enable Layer 2 (256 color screen) and make it writable
	
		 ;BB--P-VW		V=visible  W=Write 
	ld a,%00000010			;(Bank at &0000-&3FFF)  B=bank
	ld bc,&123B
	out (c),a		
		
	;nextreg &07,1			;CPU to 14mhz (0=3.5mjz 1=7mhz)
	
			nextreg &56,0		;Page in bank
			nextreg &57,1		;Page in bank
		

	ld de,GameRam
	ld bc,GameRamEnd-GameRam-1
	call Cldir

	ld hl,DefChar
	ld de,SprChar
	ld bc,16
	ldir
	

	ld a,1
	ld (PaletteNum),a
	ld a,3
	ld (Depth3D),a
	ld hl,1
	ld (CursorMoveSpeed),hl

	ld hl,SndSilence
	ld (SoundSequence),hl


	ld a,(ShowSpriteR)
	ld (Bak3d),a


	;jp GameOver				;Jumps for testing these!
	;jp DrawTitleScreen
	
ShowIntroAgain:	
	call DoPalFadeOutQ

	ifdef Enable_Intro

	ld hl,SongIntro
	call Arkos_ChangeSong		;Intro Music
		
	ld b,4
	ld ix,IntroAnimList
DrawIntroAnim:
	push bc
		call cls
		doei

		ld a,(ix+0)		;X
		ld iyh,a
		ld a,(ix+1)		;Y
		
		ld e,(ix+3)
		ld d,(ix+4)		;Tilemap Frame 1 ->HL
		ex de,hl
		push iy
		push ix
		push af
			call	ShowTileMapAltPosQtrIntro	
;HL=Tilemap BC=Tile Data IYH=X	A=Y 	;E=Height ;D=Wid
			ld a,(ix+2)
			
			call DoPalFadeIn	;Fade in frame 1
			doei
			ld bc,50
			call PauseABC	
		pop af
		pop ix
		pop iy
		push ix
			ld e,(ix+5)	;Tilemap Frame 2 ->HL
			ld d,(ix+6)
			ex de,hl
			call	ShowTileMapAltPosQtrIntro	
;HL=Tilemap BC=Tile Data IYH=X	A=Y 	;E=Height ;D=Wid
			doei
			ld bc,100
			call PauseABC	
			ld a,(ix+2)
			call DoPalFadeOut	;Fade out frame 2
		pop ix
		ld bc,7
		add ix,bc

	pop bc
	djnz DrawIntroAnim
	endif

	call cls
	
	doei	
	ld a,1
DrawTitleScreen:
	ld sp,&6000			;Reset Stack pointer

	push af
		call cls
		call SetPaletteBlack

		ld a,(Depth3D)
		or a
		jr z,ShowTitle2D

		ld a,(PaletteNum)			;3D Palette
		jr ShowTitle

ShowTitle2D:
		ld a,4						;One Eye Palette
ShowTitle:
		push af	
			dodi
			nextreg &56,16+8		;Page in bank for Title tiles
			nextreg &57,16+9		;Page in bank for Title tiles
			
			ld hl,TitleTileMap3D
			ld bc,TitleTiles3D		;paged in at &C100
		
			Call ShowTileMap			;Disable title

			nextreg &56,0			;Page in normal bank
			nextreg &57,1			;Page in normal bank

			call DoPalFadeInQ
			call DoSet3D
		pop af
		call SetPaletteB

		ld a,0
		ld hl,&1C17
		call LocateXYZ
		ld de,HiScore		
		ld b,4
		call BCD_Show

		ld de,&1C16
		ld hl,txtHiScore
		call LocateandPrint

		call PrintKeyMode
	pop af
	
	ifdef Enable_Speech
		or a
		jr z,NoSpeech

		ld bc,200
		call PauseABC	

		ld de,wavedataEnd-wavedata
		ld hl,wavedata
		xor a	;Bits
		ld b,25	;Speed
		dodi	
		call ChibiWave			;Play speech sample

		ld bc,500
		call PauseABC	
	endif

NoSpeech:
	ld hl,SongTitle
	call Arkos_ChangeSong
	doei
	ld bc,4000
	
TitleWait:
	call ReadMouse
	halt
	
	ifdef Enable_Intro
		dec bc
		ld a,b
		or c
		jp z,ShowIntroAgain		;Rimeout until we show title screen
	endif

	push bc
		ld h,%10111111			;K= Toggle Key/Joy/Mouse
		call ReadKeyRow
		bit 2,h
		jr nz,NoKeyChange
		call PrintKeyMode
		ld a,(KeyMode)
		inc a
		cp 3
		jr c,KeyModeOK			
		xor a
KeyModeOK:
		ld (KeyMode),a
		call PrintKeyMode
		halt
NoKeyChange:	
	
		ld h,%11111110
		call ReadKeyRow
		bit 3,h
		call z,ChangePalette	;Swap Palette (3d Glasses Type)

		ld h,%11110111
		call ReadKeyRow
		bit 2,h
		jr nz,No3DChangeTitle	;3 = Toggle 3D On/Off

		ld a,(Depth3D)
		inc a
		and %00000001
		ld (Depth3D),a
	pop bc
	xor a
	jp DrawTitleScreen			

No3DChangeTitle:
		ld h,%10111111
		call ReadKeyRow
		ld a,128				;Hard mode starts at Level 128
		bit 4,h
		jr z,StartGameHard		;H=Hard mode

		ei
	pop bc
	ld a,(FireDown)
	or a
	jr z,TitleWait				;Disable Wait

		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

StartGame
	xor a						;Start at level 0 (normal)
StartGameHard:
	ld (LevelNum),a				;Start at an alt level

	ld hl,SongGame
	call Arkos_ChangeSong		;Start main theme
	doei

	ld hl,NewGameData
	ld de,OldPosX
	ld bc,NewGameData_End-NewGameData
	ldir						;Reset game settings

StartNewLevel:
	call InitLevel				;Set up level contents
	call RePaintScreen			;Draw the screen
	call DoPalFadeInQ

	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Main Loop

ShowAgain:	
	push bc
		call ReadMouse			;Process Mouse Input
	pop bc
	
	call UpdateSoundSequence	;Update SFX

	ld h,%11011111
	call ReadKeyRow
	bit 0,h
	jr nz,NoPause				;Pausing game?
	call PLY_Stop
	di
	ld bc,500					;Pause
	call PauseABC	
	
DoPause:
	ld h,%11011111
	call ReadKeyRow
	bit 0,h
	jr nz,DoPause

	ld bc,100					;Unpause
	call PauseABC	
	
NoPause:
	ld h,%01111111
	call ReadKeyRow
	bit 0,h
	call z,ReloadMagazine		;Reload

	
	ld h,%11111101
	call ReadKeyRow
	bit 1,h
	jr nz,NoSpeedToggle			;Change cursor speed
	push hl
		ld hl,CursorMoveSpeedNum
		inc (hl)
		ld a,(hl)	
		and %00000011			;4 speed options
		sla a

		ld hl,CursorMoveSpeeds
		add l
		ld l,a

		ld c,(hl)				;Save new speed
		inc hl
		ld b,(hl)
		ld (CursorMoveSpeed),bc
		call WaitForKeyRelease
	pop hl

NoSpeedToggle:
	ld h,%11111110
	call ReadKeyRow
	bit 3,h
	call z,ChangePalette		;Palette change (3D glasses type)

	ld h,%11110111
	call ReadKeyRow
	bit 2,h
	jr nz,No3DChange			;3D depth select
	push hl
		call cls
		call Change3D
		call RePaintScreen
	pop hl
	call WaitForKeyRelease
	
No3DChange:
	ld hl,(Tick)				;Update level tick
	inc hl
	ld (tick),hl

	ld b,SpriteDataCount 		;Sprites to update
	ld iy,SpriteArray

DrawNextSprite:
	push bc
		push iy
			call AnimateSpriteIY ;Update the sprite
			
		pop iy
		ld bc,SpriteDataSize
		add iy,bc

	pop bc
	ld a,b
	and %00000111
	jr nz,NoMouseUp
	push bc
		call ReadMouse			;Move player cursor 
		call UpdateCursor			;(if it's moved)
	pop bc	
NoMouseUp:
	djnz DrawNextSprite
	
	ld hl,(Tick)
	ld a,l
	and %00001111				;Minigun Fire Speed
	jr nz,MoFireHeld
	
	ld a,(FireHeld)
	or a
	jr z,MoFireHeld				;Is fire held

	call GetGun
	ld a,(IX+G_Mode)
	cp 3						;Is Chibigun?
	call z, FireBullets
	
MoFireHeld:
	ld a,(FireDown2)			;Right mouse/Fire 2
	or a
	jr z,MoFire2
	call ReloadMagazine			;Reload
	xor a
	ld (FireDown2),a			;Clear fire2
	
MoFire2:
	ld a,(FireDown)				;Left Mouse/Fire 1
	or a
	jr z,MoFire
	xor a
	ld (FireDown),a				;Clear Fire 1
	call FireBullet
	
MoFire:
	call UpdateCursor			;Update Cursor

	jp ShowAgain
	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


ReadJoystick:		;Cursor Joystick

	LD  B,%01111111		;%01111111 B, N, M, Sym, Space
	;set upper address lines - note, low bit is zero
	LD  C,&FE  			;port for lines K1 - K5
	
	ld l,3
JoyInAgain:	
	in a,(c)	
	rra		;F2 - Space, F1- Enter, Right - P
	rl h
	rrc b	;%10111111 H, J, K, L  , Enter
			;%11011111 Y, U, I, O  , P
			;%11101111 6, 7, 8, 9  , 0
	dec l
	jr nz,JoyInAgain
	
	rra			;Left - O
	rl h				
	rrc b	;%11110111 5, 4, 3, 2  , 1
	rrc b	;%11111011 T, R, E, W  , Q
	
	in a,(c)	
	rra
	rl l		;U - Q (for later)
	rrc b	;%11111101 G, F, D, S  , A
	
	in a,(c)	
	rra
	rl h		;Down - A
	
	rr l		;Up   - Q
	rl h
	ld a,h
	or %10000000 ;Set unused bit
	ret
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
ReadJoy:		;Kempson Joystick

	ld bc,31            ; Kempston joystick port. (---FUDLR)
    in a,(c)        ; read input.
	cpl
	or %11100000		;Fill in the unused bytes, I believe in theory it's possible to have extra fire buttons?
	ld l,a
	
	
	;call ReadJoystick

	ld a,h
	and %00010000
	jr nz,JoyNotFire
	ld a,(FireHeld)
	or a
	jr nz,JoyNotFireB
	ld a,1
	ld (FireDown),a		;Set FireDown/Held
	ld (FireHeld),a
	jr JoyNotFireB
JoyNotFire:
	xor a		
	ld (FireHeld),a		;Release Fire Held
JoyNotFireB:

JoyNotFire2:
	ld de,0
	
	ld a,(CursorMoveSpeed)
	
	bit 2,h				;---FUDLR
	jr nz,JrNotDown
	ld e,a
JrNotDown:	
	bit 3,h
	jr nz,JrNotUp
	neg
	ld e,a
JrNotUp:	

	ld a,(CursorMoveSpeed)
	bit 0,h
	jr nz,JrNotRight
	ld d,a
JrNotRight:	
	bit 1,h
	jr nz,JrNotLeft
	neg
	ld d,a
JrNotLeft:	
	
	jp DoFakeMouse
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
ReadKey:				;Keyboard processor
	ld h,%11101111
	call ReadKeyRow
	bit 0,h
	jr nz,keyNotFire
	ld a,(FireHeld)
	or a
	jr nz,keyNotFireB
	ld a,1
	ld (FireDown),a		;Set FireDown/Held
	ld (FireHeld),a
	jr keyNotFireB
keyNotFire:
	xor a		
	ld (FireHeld),a		;Release Fire Held
keyNotFireB:
	ld l,255
	
	bit 3,h
	jr nz,KeyNotL
	res 0,l
KeyNotL:	
	bit 4,h
	jr nz,KeyNotR
	res 1,l
KeyNotR:	

	bit 1,h
	jr nz,KeyNotU
	res 3,l
KeyNotU:
	bit 2,h
	jr nz,KeyNotD
	res 2,l
KeyNotD:
	ld h,l
	jp JoyNotFire2
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ReadMouse:				;Kempson Mouse
	ld a,(KeyMode)
	cp 2
	jp z,ReadJoy
	cp 0
	jr z,ReadKey
	
	ld bc,&FADF			;FADF Mouse Buttons
	in a,(c)	
	   ;%------LR
	and %00000010
	jr nz,MouseNotFire
	ld a,(FireHeld)
	or a
	jr nz,MouseNotFireB
	ld a,1
	ld (FireDown),a		;Set FireDown/Held
	ld (FireHeld),a
	jr MouseNotFireB
MouseNotFire:
	xor a		
	ld (FireHeld),a		;Release Fire Held
MouseNotFireB:
	
	in a,(c)	
	   ;%------LR
	and %00000001
	jr nz,MouseNotFire2
	ld a,1
	ld (FireDown2),a
MouseNotFire2:
	
	ld a,(XMove)
	ld d,a
	inc b		
	in a,(c)			;FBDF Mouse Xpos
	ld (XMove),a
	sub d
	ld d,a
	
	ld a,(YMove)
	ld e,a
	ld b,&FF	
	in a,(c)			;FFDF Mouse Ypos
	ld (YMove),a
	sub e
	neg					;Flip Ypos
	ld e,a
DoFakeMouse:	
	ld bc,0
	ld b,d
	ld hl,(NewPosX)
	
	ld a,d				;D=Xpos
	or a
	jp m,MouseLeft
	
;MouseRight
	add hl,bc
	jr c,MouseUPDOWN
	ld a,h
	cp 256-7			;Over right of screen?
	jr nc,MouseUPDOWN
	
	ld (NewPosX),hl
	jr MouseUPDOWN
	
MouseLeft:	
	add hl,bc
	jr nc,MouseUPDOWN	;Over left of screen?
	ld (NewPosX),hl
		
MouseUPDOWN:			
	ld b,e
	ld hl,(NewPosY)
	
	ld a,e				;E=Ypos
	or a
	jp m,MouseUp
	
;MouseDown
	add hl,bc
	ret c
	ld a,h
	cp 192-7			;At bottom of screen?
	ret nc

	ld (NewPosY),hl
	ret
	
MouseUp:	
	add hl,bc	
	ret nc				;Over top of screen?
	ld (NewPosY),hl
	ret

	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Ymove: db 0
Xmove: db 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Fade in/out palettes - 4 per stage 
PaletteOutPink:
	db %00000000,%00100000,%01100101,%11001010
	db %00000000,%00000000,%00100000,%01100101
	db %00000000,%00000000,%00000000,%00100000
	db %00000000,%00000000,%00000000,%00000000
PaletteOutBlue:	
    db %00000000,%00000001,%00000010,%00001011
	db %00000000,%00000000,%00000001,%00000010
	db %00000000,%00000000,%00000000,%00000001
	db %00000000,%00000000,%00000000,%00000000
PaletteInBlue:	
	db %00000000,%00000000,%00000000,%00000001
    db %00000000,%00000000,%00000001,%00000010
	db %00000000,%00000001,%00000010,%00001011
	db %00000000,%00000010,%00001011,%00010011
PaletteInPink:
    db %00000000,%00000000,%00000000,%00100000
	db %00000000,%00000000,%00100000,%01100101
	db %00000000,%00100000,%01100101,%11001010
	db %00000000,%01100101,%11001010,%11110011

SetPaletteBlack:
	ld hl,PaletteBlack
	ld b,1
	jr PalFadeB
	
DoPalFadeOutQ:
	xor a
	ld (FireDown),a			;Clear fire
	inc a					;a=1
	call DoPalFadeOut
	jp cls
	
DoPalFadeInQ:
	xor a
	call DoPalFadeIn
	jp Set2D3Dmode			;Restore 3D mode

DoPalFadeIn:
	ld de,PaletteInPink
	ld hl,PaletteInBlue
	jr PalFade

DoPalFadeOut:
	ld de,PaletteOutPink
	ld hl,PaletteOutBlue
	
PalFade:					;Show a sequence of 4 palettes
	or a						;0=Pink
	jr nz,PalFadeBluePal		;1=Blue
	ex de,hl
PalFadeBluePal:
	ld b,4
PalFadeB:
	push bc
		push hl
		call SetPaletteAlt4		;Set first 4 colors
		pop hl
		push hl
		call SetPaletteAlt4b	;Set others to same 4 colors
		pop hl
		push hl
		call SetPaletteAlt4b	;Set others to same 4 colors
		pop hl
		call SetPaletteAlt4b	;Set others to same 4 colors
		push hl
			ld bc,50
			call PauseABC	
		pop hl
	pop bc
	djnz PalFadeB				;Repeat for next brightness
	ret



ChangePalette:
	ld a,(Depth3D)
	or a
	jr z,NoPaletteChange	;3D Off?
	
	ld a,(PaletteNum)
	cp 3
	jr c,PalOk
	xor a
PalOk
	inc a
	push hl
		call SetPalette
	pop hl
	call WaitForKeyRelease
NoPaletteChange:
	ret

	
TestSprite2:	;256 color bitmap
    incbin "\resAll\Sprites\RawZXN.RAW"


	
	
Palette:			;8 bits per color
	   ;RRRGGGBB     3 red bits, 3 green bits, 2 blue bit
    db %00000000 ;0
    db %01101101 ;1
    db %10110110 ;2		;db %10010010 ;2
    db %11111111 ;3
PaletteBlack:		
	db 0,0,0,0
	db 0,0,0,0
	db 0,0,0,0
	
MyPaletteRC:			;8 bits per color
	   ;RRRGGGBB     3 red bits, 3 green bits, 2 blue bit
    db %00000000 ;0
    db %01100000 ;1
    db %10000000 ;2
    db %11100000 ;3
	
	db %00001101 ;0
    db %01101101 ;1
    db %10001101 ;2
    db %11101101 ;3
	
	db %00010010 ;0
    db %01110010 ;1
	db %10110110 ;2		;db %10010010 ;2
    db %11110010 ;3

	db %00011111 ;0
    db %01111111 ;1
    db %10011111 ;2
    db %11111111 ;3

MyPaletteBY:			;8 bits per color
	   ;RRRGGGBB     3 red bits, 3 green bits, 2 blue bit
    db %00000000 ;0
	db %01101100 ;0
	db %10010000 ;0
	db %11111100 ;0
	
    db %00000001 ;1
	db %01101101 ;1
	db %10010001 ;1
	db %11111101 ;1
	
    db %00000010 ;2
    db %01101110 ;2
	db %10110110 ;2	
	db %11111110 ;2
	
	db %00000011 ;3
	db %01101111 ;3
	db %10010011 ;3
	db %11111111 ;3

MyPaletteGM:			;8 bits per color
	   ;RRRGGGBB     3 red bits, 3 green bits, 2 blue bit
    db %00000000 ;0
    db %00001100 ;1
    db %00010000 ;2
    db %00011100 ;3
	
    db %01100001 ;0
    db %01101101 ;1
    db %01110001 ;2
    db %01111101 ;3
	
	db %10000010 ;0
    db %10001110 ;1
    db %10110110 ;2		;db %10010010 ;2
    db %10011110 ;3

	db %11100011 ;0
    db %11101111 ;1
    db %11110011 ;2
    db %11111111 ;3

Palette2Dtile:			
	   ;RRRGGGBB     	3 red bits, 3 green bits, 2 blue bit
    db 0,0,0,0			;All 4 colors are the same disabling one eye
	db %01101101,%01101101,%01101101,%01101101
	db %10110110,%10110110,%10110110,%10110110
	db %11111111,%11111111,%11111111,%11111111
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


Set2D3Dmode:
	ld a,(Depth3D)		;Check mode
	or a
	jr z,Set2DMode		;0=2D mode 1-3=Stereoscopic depths 
	jr Set3DMode			;(Separation)
DoSet3D:
	ld a,(Bak3d)
	ld (ShowSpriteR),a	;Re enable Right eye render

	ret
Change3D:
	ld a,(Depth3D)
	inc a
	and %00000011				;0=2D 1-3=3D Depth
	ld (Depth3D),a
	jr z,Set2DMode
Set3DMode:
	call DoSet3D
	ld a,(PaletteNum)
	and %00000011
	jr nz,Set3DMode_PaletteOK
	inc a						;0=not valid
Set3DMode_PaletteOK:	
	jr SetPaletteB				;Set Color Palette

Set2DMode:	
	ld a,&C9					;ret (Disable Right Eye)
	ld (ShowSpriteR),a

	xor a
	jr SetPaletteB

SetPalette:
	ld (PaletteNum),a

SetPaletteB:		;Set up Palette
	rlca					;16 bytes per palette
	rlca
	rlca
	rlca
	ld hl,Palette

	ld b,0
	ld c,a
	add hl,bc				;Get Chosen palette
SetPaletteAlt16:
	ld b,16					;16 colors
SetPaletteAltB:
	nextreg &40,0           ;palette index 0
paletteloop:
	ld a,(hl)				;get color (RRRGGGBB)
	inc hl
    nextregA &41           	;Send the colour 
    djnz paletteloop      	;Repeat for next color
	ret
SetPaletteAlt4b:
	ld b,4
	jr paletteloop
	
SetPaletteAlt4:
	ld b,4
	jr SetPaletteAltB
	
	
	
	
	read "ZXN_Sprites.asm"


	
Arkos_ChangeSong:
	push hl
	call PLY_Stop				;Stop the music
	di
	pop hl

	ld de,Akuyou_MusicPos		;Song Destionation
	ld bc,&400					;Max song length=&400
	ldir
	call PLY_Init				;Restart music
	ret


Cls:	
	ld c,0			;Bank in first 3rd of screen
ClsB:	
	push bc
		call ClsPart
	pop bc
	ld a,c
	add 32
	ld c,a
	cp 192
	jr c,ClsB
	ret
ClsPart:
	ld b,0
	call GetScreenPos	

	ld c,&20
	ld b,0
	
ClsPartb:
	ld (hl),0		;Zero a byte
	inc hl
	djnz  ClsPartb
	dec c
	jr nz,ClsPartb
	
	ret	
	
ReadKeyRow:
	push bc
		ld b,h
		ld c,&FE
		in h,(c)
	pop bc
	ret
	
WaitForKeyRelease:
	ld b,%11111110
WaitForKeyReleaseB:
	ld c,&FE
	in a,(c)
	or %11100000
	inc a
	jr nz,WaitForKeyRelease
	sll b
	jr c,WaitForKeyReleaseB
	ret	
	
EnableInterrupts:
	nextreg &50,16+8
	GetNextReg &22
	bit 7,a
	call nz,InterruptHandler
	nextreg &22,0
	ei
	ret	
	
InterruptHandler:
	ifdef Akuyou_MusicPos
		push af
		push bc
		push de
		push hl
		push ix
		push iy
			call PLY_Play
		pop iy
		pop ix
		pop hl
		pop de
		pop bc
		pop af
	endif
	ei
	ret

	
	
TitleTileMap3D:
  db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,0,0,0,0,0,0,0,0,3,4,5,6,0,0
  db 0,0,0,0,0,0,7,8,9,10,11,12,13,0,0,14,15,16,17,18,0,0,0,0,0,0,19,20,21,22,0,0
  db 0,0,0,0,0,0,23,24,25,26,27,28,29,0,0,30,31,32,33,34,0,0,0,0,0,0,0,0,0,0,0,0
  db 8,9,10,11,12,13,0,35,36,37,38,0,0,0,0,39,40,41,42,43,0,7,8,9,10,11,12,13,0,0,0,0
  db 24,25,26,27,28,29,0,44,45,46,47,48,38,0,0,49,50,51,52,53,0,23,24,25,26,27,28,29,0,0,0,0
  db 0,0,0,54,55,56,57,58,59,46,60,61,62,63,0,0,0,0,0,0,64,0,0,0,65,66,67,68,0,0,0,0
  db 0,0,69,70,71,72,73,74,75,76,77,78,79,80,0,0,0,0,0,81,82,83,84,85,0,86,87,0,0,0,0,0
  db 0,0,88,89,90,91,92,93,94,95,96,97,98,99,100,0,0,0,0,101,102,103,104,105,0,0,0,106,107,108,0,0
  db 109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 126,127,128,129,130,131,132,133,134,135,136,137,138,46,139,46,140,141,142,143,0,0,0,0,144,145,146,0,144,145,146,0
  db 147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,38,169,170,171,172,173,170,171,172,173
  db 174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,0,0,0,0,0,0,0,0,0
  db 197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,0,220,221,222,223,224,225,0,0
  db 0,0,0,226,227,228,229,230,46,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,0
  db 0,0,0,0,253,254,255,0,255,1,46,255,2,255,3,255,4,46,255,5,255,6,255,7,255,8,255,9,255,10,255,11,255,12,255,13,255,14,255,15,255,16,255,17,255,18,255,19,255,20,255,21,255,22,0
  db 0,0,0,0,0,0,255,23,46,46,255,24,255,25,46,46,255,26,255,27,255,28,255,29,255,30,255,31,255,32,255,33,255,34,255,35,255,36,255,37,255,38,255,39,255,40,255,41,255,42,255,43,0
  db 0,0,0,0,0,255,44,230,46,46,255,5,255,45,46,231,232,255,46,255,47,255,48,255,49,255,50,255,51,255,52,255,53,0,255,54,255,55,255,56,255,57,255,58,255,59,255,60,0,0
  db 0,0,0,0,0,255,61,255,62,255,63,255,63,255,64,255,65,255,66,255,67,255,68,0,0,0,0,0,0,0,0,0,0,255,69,255,70,255,71,255,72,255,73,255,74,255,75,255,76
  db 0,0,0,0,0,0,0,0,0,0,0,255,77,255,78,255,79,255,80,0,255,81,255,82,255,83,255,84,0,0,0,255,85,255,86,255,87,255,88,255,89,255,90,255,91,255,92,255,93
  db 255,94,255,95,255,96,255,97,0,0,0,0,0,0,0,0,255,98,255,99,255,100,0,255,81,255,101,255,102,255,103,0,255,104,255,105,255,106,255,107,255,108,255,109,255,110,255,111,255,112,255,113,255,114
  db 255,115,0,255,116,255,117,255,118,0,0,255,104,255,105,255,106,0,255,119,255,120,255,121,255,122,0,255,81,255,123,255,124,255,125,0,255,126,255,127,255,128,0,0,255,129,255,130,255,130,255,131,255,104,255,105
  db 255,132,255,133,255,134,255,135,255,136,255,137,0,255,126,255,127,255,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,126,255,127
  db 255,138,255,139,255,140,255,141,255,142,255,143,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 255,144,255,145,255,146,255,147,255,148,255,149,255,150,255,151,255,152,255,153,255,154,255,155,255,156,255,157,255,158,255,159,0,0,0,255,129,255,130,255,130,255,131,0,0,0,0,0,0,0,0,0

	
	
	
SongIntro:
	incbin "\ResALL\SuckHunt\IntroSong4000.bin"
SongTitle:
	incbin "\ResALL\SuckHunt\TitleSong4000.bin"
SongGame:
	incbin "\ResALL\SuckHunt\GameSong4000.bin"

	read "\SrcALL\Multiplatform_ChibiSound.asm"			;Sound Driver
	
	read "\SrcALL\Multiplatform_ArkosTrackerLite.asm"


	;incbin "\ResALL\SuckHunt\CPCTitleTiles.raw"
	read "\SrcALL\Multiplatform_BCD.asm"				;Show Binary Coded Decimal
	read "\SrcALL\MultiPlatform_ShowDecimal.asm"		;Show decimal number

	read "SH_Multiplatform.asm"
	read "SH_DataDefs.asm"
	read "SH_RamDefs.asm"
	read "SH_LevelInit.asm"

SuckOfDeath3dTileMap:
  db 0,0,0,0,0,1,2,3,0,0,0,0,0,0,0,0
  db 0,0,0,0,0,4,5,6,7,8,0,0,0,0,0,0
  db 0,0,0,0,0,0,0,9,10,11,12,0,0,0,0,0
  db 0,0,0,0,13,14,15,16,17,18,19,20,21,22,0,0
  db 0,0,0,23,24,25,26,27,28,29,30,31,32,33,34,0
  db 0,0,35,36,37,38,39,40,41,42,43,44,45,46,0,0
  db 0,0,47,48,49,50,51,52,53,54,55,56,57,58,59,60
  db 0,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75
  db 76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,0
  db 91,92,93,0,94,95,96,97,98,99,100,101,102,103,104,105
  db 106,107,108,0,109,110,111,112,113,114,115,116,117,118,119,120
  db 121,122,123,0,124,125,126,0,127,128,129,130,131,132,133,134

  
	
  
SuckOfDeath3dTiles:
	incbin "\ResALL\SuckHunt\SuckofDeathZXN3d.RAW"
	

Intro1:
  db 0,0,0,0,0,1,1,2,0,0,0,0,3,4,3,5
  db 0,0,0,0,0,6,7,8,9,0,0,0,10,11,12,13
  db 14,0,0,0,0,15,16,17,9,0,0,0,18,19,20,21
  db 3,22,0,0,0,23,24,15,25,0,0,0,26,27,28,9
  db 29,30,31,6,32,33,34,35,6,0,36,37,23,38,39,9
  db 10,40,41,30,42,43,44,45,46,47,48,49,9,6,50,16
  db 51,48,52,53,54,55,56,57,58,59,60,61,9,21,62,27
  db 0,63,64,65,66,67,68,69,70,71,72,73,23,24,24,24
  db 0,0,0,74,75,76,77,7,35,78,0,0,79,80,24,38
  db 0,0,0,0,0,23,81,82,83,84,0,0,85,86,87,88
  db 0,0,0,0,6,22,89,90,39,91,0,0,92,93,94,95
  db 0,0,0,96,24,24,24,38,25,9,0,0,23,97,98,99
Intro1b:
  db 0,0,0,0,0,1,1,2,0,0,0,0,3,4,3,5
  db 0,0,0,0,0,6,7,8,9,0,0,0,10,11,12,13
  db 0,0,0,0,0,15,16,17,9,0,0,0,18,19,20,21
  db 0,0,0,0,0,23,24,15,25,0,0,0,26,27,28,9
  db 31,6,32,33,0,0,27,35,6,0,0,0,255,126,255,24,39,255,127
  db 41,48,42,43,255,128,0,75,45,46,0,0,47,48,49,77,255,129
  db 52,53,54,55,56,255,130,255,131,57,58,255,132,255,133,59,60,61,255,134,255,135
  db 64,65,66,67,68,0,255,136,69,35,0,255,137,71,72,255,138,24,24
  db 0,74,75,76,0,0,77,7,35,255,139,0,78,79,80,24,38
  db 0,0,0,0,0,23,81,82,83,84,0,0,85,86,87,88
  db 0,0,0,0,6,22,89,90,39,91,0,0,92,93,94,95
  db 0,0,0,96,24,24,24,38,25,9,0,0,23,97,98,99

Intro2:
  db 0,100,101,102,0,103,2,104,0,104,0,105,0,0,0,105
  db 0,0,0,100,106,0,0,74,0,104,0,107,0,0,0,107
  db 0,0,0,108,109,101,0,0,12,110,111,112,0,0,0,113
  db 0,0,114,115,53,48,116,0,117,118,119,120,0,0,0,107
  db 0,0,121,53,48,122,10,48,48,48,123,124,125,126,0,113
  db 0,127,128,129,130,131,132,133,134,48,135,136,137,138,139,107
  db 140,141,142,78,143,144,24,145,146,147,48,48,48,148,149,113
  db 0,140,150,0,9,0,103,102,151,152,153,154,155,156,157,158
  db 0,159,160,0,161,102,0,0,151,162,163,164,165,166,167,168
  db 0,169,0,0,0,0,103,102,170,162,171,169,172,173,169,150
  db 0,174,0,0,0,175,0,0,151,176,177,169,178,179,169,160
  db 0,0,0,0,0,180,0,0,181,182,183,169,184,139,174,185
Intro2b:
  db 101,255,86,0,102,0,103,2,104,0,104,0,105,0,0,0,105
  db 0,100,255,87,2,255,88,0,0,74,0,104,0,107,0,0,0,107
  db 0,108,109,255,89,255,0,0,0,0,12,110,111,112,0,0,0,113
  db 114,115,255,90,48,116,0,0,0,117,118,119,120,0,0,0,107
  db 121,255,91,48,122,10,48,48,48,48,48,123,124,125,126,0,113
  db 128,129,130,131,132,133,133,133,134,48,135,136,137,138,139,107
  db 142,78,255,92,144,24,145,8,46,255,93,147,48,48,48,148,149,113
  db 150,255,94,0,0,9,0,103,102,151,152,153,154,155,156,157,158
  db 160,159,0,0,161,102,0,0,151,162,163,164,165,166,167,168
  db 0,169,0,0,0,0,103,102,170,162,171,169,172,173,169,150
  db 0,174,0,0,0,175,0,0,151,176,177,169,178,179,169,160
  db 0,0,0,0,0,180,0,0,181,182,183,169,184,139,174,185

Intro3:
  db 0,0,0,186,9,0,0,187,7,188,189,48,48,190,191,192
  db 0,0,193,0,0,0,0,30,139,0,194,195,24,196,197,198
  db 0,0,199,200,0,0,186,0,201,0,202,203,204,205,0,206
  db 0,207,191,208,139,209,0,0,0,202,210,211,212,213,126,0
  db 0,214,0,215,208,139,0,0,202,216,217,117,218,219,220,126
  db 0,221,0,0,192,222,223,202,224,225,226,151,227,147,228,229
  db 230,0,0,0,0,231,232,224,0,169,233,234,235,236,139,0
  db 237,0,0,238,239,0,0,0,0,169,231,240,0,241,242,0
  db 9,0,243,244,214,245,0,0,0,169,246,247,248,0,249,0
  db 0,0,250,251,252,253,254,254,0,113,246,0,0,255,0,241,139
  db 0,0,0,255,1,255,2,255,3,255,4,255,5,0,255,6,255,7,255,8,247,0,255,9,242
  db 0,0,0,96,255,10,255,11,255,12,255,13,96,246,0,248,0,255,8,0,255,14
Intro3b:
  db 0,0,0,186,9,0,0,187,7,188,189,48,48,190,191,192
  db 0,0,193,0,0,0,0,30,139,0,194,195,24,196,197,198
  db 0,0,199,200,0,0,186,0,201,0,202,203,204,205,0,206
  db 0,207,191,208,139,209,0,0,0,202,210,211,212,213,126,0
  db 0,214,0,215,208,139,0,0,202,216,217,117,218,219,220,126
  db 0,221,0,0,192,222,223,202,224,225,226,151,227,147,228,229
  db 230,0,0,0,0,255,95,232,224,0,169,233,234,235,236,139,0
  db 237,0,0,255,96,255,97,253,254,254,0,169,231,240,0,241,242,0
  db 9,0,243,255,98,255,2,255,3,255,4,255,5,0,169,246,247,248,0,249,0
  db 0,0,250,255,99,255,100,255,11,255,12,255,13,0,113,246,0,0,255,0,241,139
  db 0,0,0,96,255,101,48,255,102,255,103,0,255,6,255,7,255,8,247,0,255,9,242
  db 0,0,0,0,255,104,255,105,48,255,106,255,107,246,0,248,0,255,8,0,255,14
Intro4:
  db 255,15,255,16,101,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 255,17,255,18,255,19,255,20,110,37,101,101,0,0,0,255,21,255,22,255,22,255,22,255,19
  db 255,23,255,24,255,25,255,26,255,27,255,28,255,19,255,29,255,30,255,31,255,32,255,33,255,34,0,0,255,35
  db 255,18,255,19,255,19,255,30,30,255,23,255,24,255,36,255,37,152,255,19,48,30,30,255,15,37
  db 255,38,255,39,255,40,255,27,255,17,255,18,255,19,255,41,255,37,255,37,255,37,255,19,255,37,255,37,147,48
  db 134,255,42,255,43,255,44,255,45,255,46,255,46,255,47,255,37,255,48,255,39,255,3,255,37,255,49,255,50,255,51
  db 0,255,52,255,53,255,54,255,55,255,42,255,56,255,57,255,37,255,48,255,39,255,58,255,59,255,60,101,0
  db 0,0,0,0,0,255,61,255,62,255,37,255,48,255,38,255,63,255,64,255,65,255,66,255,23,255,67
  db 0,0,0,0,0,0,0,255,68,134,255,42,255,43,255,69,255,23,255,70,255,71,48
  db 0,0,0,0,0,0,96,24,255,24,24,255,68,133,255,72,255,73,48,255,74
  db 0,0,0,0,0,0,0,255,75,255,76,255,77,255,78,24,255,79,48,48,255,80
  db 0,0,0,0,0,0,0,0,0,255,81,255,82,255,83,255,84,255,23,255,85,133
Intro4b:
  db 255,15,255,16,101,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 255,17,255,18,255,19,255,20,110,37,101,101,0,0,0,255,21,255,22,255,22,255,22,255,19
  db 255,23,255,24,255,25,255,26,255,27,255,28,255,19,255,29,255,30,255,31,255,32,255,33,255,34,0,0,255,35
  db 255,18,255,19,255,19,255,30,30,255,23,255,24,255,36,255,37,152,255,19,48,30,30,255,15,37
  db 255,38,255,39,255,40,255,27,255,17,255,18,255,19,255,41,255,37,255,37,255,37,255,19,255,37,255,37,147,48
  db 134,255,42,255,43,255,44,255,45,255,46,255,46,255,47,255,37,255,48,255,39,255,3,255,37,255,108,255,109,255,74
  db 0,255,52,255,53,255,54,255,55,255,42,255,56,255,57,255,37,255,48,255,39,255,58,255,59,255,110,255,111,255,112
  db 0,0,0,0,0,255,61,255,62,255,37,255,48,255,38,255,63,255,64,255,113,255,114,255,73,48
  db 0,0,0,0,0,0,0,255,68,134,255,42,255,43,255,37,255,115,255,116,48,48
  db 0,0,0,0,0,0,96,24,255,24,24,255,68,133,255,117,255,118,255,119,255,120
  db 0,0,0,0,0,0,0,255,75,255,76,255,77,255,78,24,24,255,121,255,122,255,123
  db 0,0,0,0,0,0,0,0,0,255,81,255,82,255,83,75,255,124,255,68,255,125
IntroGraphics:	
	incbin "\ResALL\SuckHunt\SuckHuntIntroZXN.RAW"

KeyMode: db 0

KeyText: db "Sin",255
	db "Mou",255
	db "Kem",255


IntroAnimList:
	db 4+0,12,1			;X,Y,Colors
		dw Intro1			;Frame 1
		dw Intro1b			;Frame 2
	db 4+32-16,0,0		;X,Y,Colors
		dw Intro3			;Frame 1
		dw Intro3b			;Frame 2
	db 4+32-16,12,0		;X,Y,Colors
		dw Intro2			;Frame 1
		dw Intro2b			;Frame 2
	db 4+0,0,1			;X,Y,Colors
		dw Intro4			;Frame 1
		dw Intro4b			;Frame 2

	ifdef Enable_Speech
	
ZXSAYWave equ 1
	
wavedata:
	incbin "\ResALL\SuckHunt\operationsucka1-6.raw"
wavedataEnd:
freq4 equ 1	;11025
bits1 equ 1
	read "\SrcALL\Multiplatform_ChibiWave.asm"
	endif


S_AddressL equ 0
S_AddressH equ 1
S_Wid equ 2
S_Hei equ 3

PixelsPerByte equ 4
SpriteBase equ SpriteData

SpriteData:
	incbin "\ResALL\SuckHunt\SuckHuntZXN.RAW"


;Exported by Akusprite Editor
SpriteInfo:
    dw &0000/PixelsPerByte + SpriteBase           ;SpriteAddr 0
     db 32,32                                     ;XY 
    dw &0400/PixelsPerByte + SpriteBase           ;SpriteAddr 1
     db 24,16                                     ;XY 
    dw &0580/PixelsPerByte + SpriteBase           ;SpriteAddr 2
     db 24,16                                     ;XY 
    dw &0700/PixelsPerByte + SpriteBase           ;SpriteAddr 3
     db 24,16                                     ;XY 
    dw &0880/PixelsPerByte + SpriteBase           ;SpriteAddr 4
     db 24,16                                     ;XY 
    dw &0A00/PixelsPerByte + SpriteBase           ;SpriteAddr 5
     db 16,16                                     ;XY 
    dw &0B00/PixelsPerByte + SpriteBase           ;SpriteAddr 6
     db 40,48                                     ;XY 
    dw &1280/PixelsPerByte + SpriteBase           ;SpriteAddr 7
     db 40,48                                     ;XY 
    dw &1A00/PixelsPerByte + SpriteBase           ;SpriteAddr 8
     db 32,32                                     ;XY 
    dw &1E00/PixelsPerByte + SpriteBase           ;SpriteAddr 9
     db 32,32                                     ;XY 
    dw &2200/PixelsPerByte + SpriteBase           ;SpriteAddr 10
     db 24,24                                     ;XY 
    dw &2440/PixelsPerByte + SpriteBase           ;SpriteAddr 11
     db 24,24                                     ;XY 
    dw &2680/PixelsPerByte + SpriteBase           ;SpriteAddr 12
     db 48,32                                     ;XY 
    dw &2C80/PixelsPerByte + SpriteBase           ;SpriteAddr 13
     db 48,32                                     ;XY 
    dw &3280/PixelsPerByte + SpriteBase           ;SpriteAddr 14
     db 32,24                                     ;XY 
    dw &3580/PixelsPerByte + SpriteBase           ;SpriteAddr 15
     db 32,16                                     ;XY 
    dw &3780/PixelsPerByte + SpriteBase           ;SpriteAddr 16
     db 24,16                                     ;XY 
    dw &3900/PixelsPerByte + SpriteBase           ;SpriteAddr 17
     db 24,16                                     ;XY 
    dw &3A80/PixelsPerByte + SpriteBase           ;SpriteAddr 18
     db 32,24                                     ;XY 
    dw &3D80/PixelsPerByte + SpriteBase           ;SpriteAddr 19
     db 32,32                                     ;XY 
    dw &4180/PixelsPerByte + SpriteBase           ;SpriteAddr 20
     db 24,16                                     ;XY 
    dw &4300/PixelsPerByte + SpriteBase           ;SpriteAddr 21
     db 24,24                                     ;XY 
    dw &4540/PixelsPerByte + SpriteBase           ;SpriteAddr 22
     db 16,16                                     ;XY 
    dw &4640/PixelsPerByte + SpriteBase           ;SpriteAddr 23
     db 16,16                                     ;XY 
    dw &4740/PixelsPerByte + SpriteBase           ;SpriteAddr 24
     db 40,61                                     ;XY 
    dw &50C8/PixelsPerByte + SpriteBase           ;SpriteAddr 25
     db 40,61                                     ;XY 
    dw &5A50/PixelsPerByte + SpriteBase           ;SpriteAddr 26
     db 40,39                                     ;XY 
    dw &6068/PixelsPerByte + SpriteBase           ;SpriteAddr 27
     db 40,39                                     ;XY 
    dw &6680/PixelsPerByte + SpriteBase           ;SpriteAddr 28
     db 24,6                                      ;XY 
    dw &6710/PixelsPerByte + SpriteBase           ;SpriteAddr 29
     db 24,16                                     ;XY 
    dw &6890/PixelsPerByte + SpriteBase           ;SpriteAddr 30
     db 48,16                                     ;XY 
    dw &6B90/PixelsPerByte + SpriteBase           ;SpriteAddr 31
     db 32,16                                     ;XY 

FontData:				;Bitmap Font (4 color)
	ds 16	;Space char
	incbin "\ResALL\SuckHunt\FontZXN.RAW"


SprCursorSprite:
	incbin "\ResALL\SuckHunt\SuckHuntZXN_Cursors.RAW"
	
PrintKeyMode:
	
	ld hl,KeyText
		ld b,0
		ld a,(KeyMode)
		rlca
		rlca
		ld c,a
		add hl,bc
		
		ld de,&1417
		call LocateandPrint
	ret