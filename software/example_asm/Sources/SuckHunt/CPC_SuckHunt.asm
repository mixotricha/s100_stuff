Akuyou_MusicPos equ &B800

Enable_Intro equ 1
Enable_Speech equ 1

;Enable this for WinAPE build
;BuildCPC equ 1

GunSprite equ 1

VscreenMinX equ 48		;Top left of visible screen in logical co-ordinates
VscreenMinY equ 0

VscreenWid equ 160		;Screen Size in logical units
VscreenHei equ 96
VscreenWidClip equ 0


ScreenObjWidth equ 80-12
ScreenObjHeight equ 200-48

GameRam equ &B000


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef vasm
		include "\SrcALL\VasmBuildCompat.asm"
	else
		read "\SrcALL\WinApeBuildCompat.asm"
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	read "\SrcALL\CPU_Compatability.asm"

	macro dodi
		di
	endm
	macro doei
		ei
	endm

	nolist
	;Unrem this if building with vasm
;	include "\SrcALL\VasmBuildCompat.asm"


	org &100
	xor a
	ld b,a
	ld c,a
	push bc
		call &BC32		;Set Ink A
	pop bc
	call &BC38		;Set Border
	ld a,1
	call &BC0E		;Screen Mode 1

	di
	ld hl,&0038		;IM1 interrupt address (RST7)
	ld a,&C3		;JP
	ld (hl),a
	inc hl
	ld de,InterruptHanlder
	ld (hl),e
	inc hl
	ld (hl),d

	ld de,GameRam
	ld bc,GameRamEnd-GameRam-1
	call Cldir

	ld a,3
	ld (Depth3D),a
	ld hl,64*2
	ld (CursorMoveSpeed),hl

	ld hl,SndSilence
	ld (SoundSequence),hl

	ld a,(ShowSpriteR)
	ld (Bak3d),a

	ld hl,(ShowSpriteL2DMode)
	ld (Bak3d+1),hl

	ld hl,DefChar
	ld de,SprChar
	ld bc,16
	ldir

ShowIntroAgain:	
	call DoPalFadeOutQ
	
	ifdef Enable_Intro

	ld hl,SongIntro
	call Arkos_ChangeSong	;Intro music
	ei

	ld b,4
	ld ix,IntroAnimList		;Sequence of the intro
DrawIntroAnim:
	push bc
		call cls

		ld a,(ix+0)		;X
		ld iyh,a
		ld a,(ix+1)		;Y
		
		ld e,(ix+3)		;Tilemap Frame 1 -> HL
		ld d,(ix+4)
		ex de,hl
		push iy
		push ix
		push af
			call	ShowTileMapAltPosQtrIntro	
			;HL=Tilemap BC=Tile Data IYH=X	A=Y 	;E=Height ;D=Wid
			
			ld a,(ix+2)
			call DoPalFadeIn	;Fade in this Frame 1
			ld bc,100
			call PauseABC	
		pop af
		pop ix
		pop iy

		push ix		
			ld e,(ix+5)			;Tilemap Frame 2 -> HL
			ld d,(ix+6)
			ex de,hl
			call	ShowTileMapAltPosQtrIntro	
			;HL=Tilemap BC=Tile Data IYH=X	A=Y 	;E=Height ;D=Wid

			ld bc,220
			call PauseABC	
			ld a,(ix+2)
			call DoPalFadeOut	;Fade out frame 2
		pop ix
		ld bc,7
		add ix,bc
	pop bc
	djnz DrawIntroAnim
	endif

	call cls
		
	ld bc,500
	call PauseABC	
	ld a,1				;Play the speech!
DrawTitleScreen:

	ld sp,&C000			;Reset Stack pointer

	push af
		ld a,32
		ld (PlayerLife),a

		call cls
		call Set2DMode			;2D rendering engine
		call SetPaletteBlack	;Blackout screen

		ld a,(Depth3D)
		or a
		jr z,ShowTitle2D

		ld a,1					;Palette 1
		ld hl,TitleTileMap3D	;3D graphic
		ld bc,TitleTiles3D
		jr ShowTitle			

ShowTitle2D:
		xor a					;Palette 0
		ld hl,TitleTileMap2D	;2D Graphic
		ld bc,TitleTiles	

ShowTitle:
		push af	
			ei
			Call ShowTileMap		;Display title
			call DoPalFadeInQ		;Fade in
		pop af
		call SetPalette

		ld a,(Depth3D)
		or a
		call nz,DoSet3D

		ld a,-32
		ld hl,&1C17
		call LocateXYZ
		ld de,HiScore			;Show Highscore
		ld b,4
		call BCD_Show
		ld de,&1C16
		ld hl,txtHiScore
		call LocateandPrint

	pop af
	ifdef Enable_Speech
		or a
		jr z,NoSpeech			;A=0 means no speech 
							;We only play speech after intro
		ld bc,800
		call PauseABC	

		ld de,wavedataEnd-wavedata
		ld hl,wavedata
		xor a	;Bits
		ld b,25	;Speed
		di
		call ChibiWave		;Play Speech sample

		ld bc,500
		call PauseABC	
	endif

NoSpeech:
	ld hl,SongTitle
	call Arkos_ChangeSong
	ei
	ld bc,12000
TitleWait:
	halt
	halt

	ifdef Enable_Intro
		dec bc
		ld a,b
		or c
		jp z,ShowIntroAgain		;Rimeout until we show title screen
	endif

	push bc
		ld h,&47
		call ReadKeyRow	

		bit 6,h
		call z,ChangePalette	;Swap Palette (3d Glasses Type)

		bit 1,h
		jr nz,No3DChangeTitle	;Toggle 3D On/Off

		ld a,(Depth3D)
		inc a
		and %00000001			;Toggle 3D on/Off
		ld (Depth3D),a

	pop bc
	xor a						;No Speech
	jp DrawTitleScreen			;Redraw Title in new 3d mode

No3DChangeTitle:
		ld h,&45
		call ReadKeyRow	
		ld a,128				;Hard mode starts at Level 128
		bit 4,h
		jr z,StartGameHard		;H=Hard mode

		ei
	pop bc
	ld a,(FireDown)
	or a
	jr z,TitleWait				;Disable Wait

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;StartGame
	xor a					;Start at level 0 (normal)
StartGameHard:
	ld (LevelNum),a			;Start at an alt level
	
	ld hl,SongGame
	call Arkos_ChangeSong	;Start main theme
	ei
	
	ld hl,NewGameData
	ld de,OldPosX
	ld bc,NewGameData_End-NewGameData
	ldir					;Reset game settings

StartNewLevel:
	call InitLevel			;Set up level contents
	call RePaintScreen		;Draw the screen
	call DoPalFadeInQ

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Main Loop
ShowAgain:	
	call UpdateSoundSequence	;Update SFX

	ld h,&43
	call ReadKeyRow
	bit 3,h
	jr nz,NoPause				;Pausing game?
	call PLY_Stop
	di
	ld bc,500					;Pause
	call PauseABC	

DoPause:
	ld h,&43
	call ReadKeyRow
	bit 3,h
	jr nz,DoPause				
	ld bc,100					;Unpause
	call PauseABC	
	
NoPause:
	ld h,&45
	call ReadKeyRow
	bit 7,h
	call z,ReloadMagazine		;Reload

	ld h,&47
	call ReadKeyRow	
	ei

	bit 4,h
	jr nz,NoSpeedToggle			;Change cursor speed
	push hl
		ld hl,CursorMoveSpeedNum
		inc (hl)
		ld a,(hl)	
		and %00000011			;4 speed options
		sla a

		ld hl,CursorMoveSpeeds
		add l
		ld l,a

		ld c,(hl)				;Save new speed
		inc hl
		ld b,(hl)
		ld (CursorMoveSpeed),bc
		call WaitForKeyRelease
	pop hl

NoSpeedToggle:
	bit 6,h
	call z,ChangePalette	;Palette change (3D glasses type)

	bit 1,h
	jr nz,No3DChange		;3D depth select
	push hl
		call cls
		call Change3D
		call RePaintScreen
	pop hl
	call WaitForKeyRelease
	
No3DChange:
	ld hl,(Tick)			;Update level tick
	inc hl
	ld (tick),hl

	ld b,SpriteDataCount 	;Sprites to update
	ld iy,SpriteArray

DrawNextSprite:
	push bc
		push iy
			call AnimateSpriteIY	;Update the sprite

			call UpdateCursor		;Move player cursor 
										;(if it's moved)
		pop iy
		ld bc,SpriteDataSize
		add iy,bc
	pop bc
	djnz DrawNextSprite

	ld hl,(Tick)
	ld a,l
	and %00001111				;Minigun Fire Speed
	jr nz,NoFireHeld	
	
	ld a,(FireHeld)
	or a
	jr z,NoFireHeld				;Is fire held

	call GetGun
	ld a,(IX+G_Mode)
	cp 3						;Is Chibigun?
	call z, FireBullets
	
NoFireHeld:
	ld a,(FireDown2)			;Right mouse/Fire 2
	or a
	jr z,NoFire2
	call ReloadMagazine			;Reload
	xor a
	ld (FireDown2),a			;Clear fire2
	
NoFire2:
	ld a,(FireDown)				;Left Mouse/Fire 1
	or a
	jr z,NoFire				
	xor a
	ld (FireDown),a				;Clear Fire 1
	call FireBullet				;Shoot bullet
	
NoFire:
	call UpdateCursor			;Update Cursor

	jp ShowAgain
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


InterruptHanlder:
	ex af,af'
	exx
		call ReadJoystick			;Get Keypresses
		ld d,h

		ld hl,(NewPosY)
		ld bc,(CursorMoveSpeed)		;Get Move speed

		bit 0,d
		jr nz,JoyNotUp
		ld a,h
		or a
		jr z,JoyNotUp
		or a
		sbc hl,bc					;Move up
		
JoyNotUp:
		bit 1,d
		jr nz,JoyNotDown
		ld a,h
		cp 200-8
		jr z,JoyNotDown
		add hl,bc					;Move down
		
JoyNotDown:
		ld (NewPosY),hl
	
		ld hl,(NewPosX)
		srl b		;Shift move speed for X axis
		rr c
		srl b
		rr c

		bit 2,d
		jr nz,JoyNotLeft
		ld a,h
		or a
		jr z,JoyNotLeft
		or a
		sbc hl,bc					;Move Left

JoyNotLeft:
		bit 3,d
		jr nz,JoyNotRight
		ld a,h
		cp 80-2
		jr z,JoyNotRight
		add hl,bc					;Move Right

JoyNotRight:
		ld (NewPosX),hl
		bit 4,d
		jr nz,JoyNotFire
		ld a,(FireHeld)
		or a
		jr nz,JoyNotFireB

		ld a,1
		ld (FireDown),a				;Fire Pressed
		ld (FireHeld),a
		jr JoyNotFireB
JoyNotFire:
		xor a		
		ld (FireHeld),a
JoyNotFireB:

		bit 5,d
		jr nz,JoyNotFire2
		ld a,(FireHeld2)
		or a
		jr nz,JoyNotFire2B

		ld a,1
		ld (FireDown2),a
		ld (FireHeld2),a
		jr JoyNotFire2B
JoyNotFire2:
		xor a		
		ld (FireHeld2),a
JoyNotFire2B:

	ifdef Akuyou_MusicPos

		ld b,&f5			;See if we're in Vblank
		in a,(c)
		rra     			;Bit0=1 means yes!
		jp nc,Interrupts_FastTick

		exx
		ex af,af'
		push af
		push bc
		push de
		push hl
		push ix
		push iy
			call PLY_Play	;User Arkostracker
		pop iy
		pop ix
		pop hl
		pop de
		pop bc
		pop af
		ei
		ret
Interrupts_FastTick:	;5/6's of interrupts are not vblank
	endif
	exx
	ex af,af'
	ei
	ret


	;Key input on the CPC goes through the 8255 PPI an AY soundchip, we need to use the PPI to talk to the AY, 
	;and tell it to read input through reg 14

	;Port F6C0 = Select Regsiter
	;Port F600 = Inactive
	;Port F6xx = Recieve Data
	;Port F4xx = Send Data to selected Register
	
ReadJoystick:	
	ld h,&49        ;Line 49
ReadKeyRow:	
	di
	ld bc,&f782     ;Select PPI port direction... A out /C out 
	xor a
	out (c),c       
	ld bc,&f40e     ; Select Ay reg 14 on ppi port A 
	ld e,b          
	out (c),c       
	ld bc,&f6c0     ;This value is an AY index (R14) 
	ld d,b          
	out (c),c        
	out (c),a   	; F600 - set to inactive (needed)
	ld bc,&f792     ; Set PPI port direction A in/C out 
	out (c),c       

	ld c,&4a        
	ld b,d    	;d=#f6     
	out (c),h       ;select line &F640-F64A
	ld b,e    	;e=#f4   
	in h,(c)
	ld bc,&f782     
	out (c),c       ; reet PPI port direction - PPI port A out / C out 
        
	ret		;Result in H

WaitForKeyRelease:
	ei
	halt
	ld h,&47
	call ReadKeyRow	
	inc h	;=255?
	jr nz,WaitForKeyRelease

	ld h,&45
	call ReadKeyRow	
	inc h	;=255?
	jr nz,WaitForKeyRelease

	xor a
	ld (FireDown),a
	ld h,255
	ret
	

Set2D3Dmode:
	ld a,(Depth3D)				;Check mode
	or a
	jr z,Set2DMode				;0=2D mode 1-3=Stereoscopic depths 
	jr Set3DMode					;(Separation)
DoSet3D:
	ld a,(Bak3d)		
	ld (ShowSpriteR),a			;Re enable Right eye render
	ld hl,(Bak3d+1)
	ld (ShowSpriteL2DMode),hl	;Re enable Left eye.
	ret
	
Change3D:
	ld a,(Depth3D)
	inc a
	and %00000011				;0=2D 1-3=3D Depth
	ld (Depth3D),a
	jr z,Set2DMode
Set3DMode:
	call DoSet3D
	ld a,(PaletteNum)
	and %00000011
	jr nz,Set3DMode_PaletteOK
	inc a						;0=not valid
Set3DMode_PaletteOK:
	jr SetPaletteB				;Set Color Palette

Set2DMode:
	ld a,&C9					;ret (Disable Right Eye)
	ld (ShowSpriteR),a

	ld hl,&0418					;JR 04 (Skip 3D Channel mask)
	ld (ShowSpriteL2DMode),hl
	xor a
	jr SetPaletteB

	
SetPaletteBlack:
	ld hl,PaletteOutBlue+12		;Black out colors
	jr SetPaletteAlt4

SetPalette:
	ld (PaletteNum),a			;Set palette number

SetPaletteB:					;Set up Palette
	sla a
	sla a
	ld hl,Palette

	ld b,0
	ld c,a
	add hl,bc
SetPaletteAlt4:
	ld d,4						;Palette count
;SetPaletteAlt
	xor a
PaletteAgain:
	ld c,a
	ld b,&7f
	push af
		ld a,(hl)				;Send colors to hardware
		out (c),c               
		out (c),a
	pop af
	inc hl						;Next Palette Entry
	inc a
	cp d						;Palette Count
	jr nz,PaletteAgain
	ret

cls:
	ld de,&C000
	ld bc,&3FFE
	jp cldir

ChangePalette:				;Toggle the 3 3D Palettes
	ld a,(Depth3D)
	or a
	jr z,NoPaletteChange	;3D Off?
	
	ld a,(PaletteNum)
	cp 3
	jr c,PalOk
	xor a					;Over limit
PalOk
	inc a
	push hl
		call SetPalette		;Set Palette
	pop hl
	call WaitForKeyRelease
NoPaletteChange:
	ret

;Disable3DB
;	ret

;PaletteA
;	db &54,&40,&4B,&4B	;GreyB
;	db &54,&40,&40,&4B	;GreyA

Palette:
	db &54,&58,&40,&4B	;(2D mode)
	db &54,&4C,&53,&4B	;Red/Cyan
	db &54,&52,&4D,&4B	;Green/Magenta
	db &54,&4A,&55,&4B	;Yellow/Blue

PaletteInBlue:
	db &54,&54,&54,&44
	db &54,&54,&44,&55
	db &54,&44,&55,&57
	db &54,&55,&57,&5B
PaletteOutBlue:
	db &54,&44,&55,&57
	db &54,&54,&44,&55
	db &54,&54,&54,&44
	db &54,&54,&54,&54	;Blacks

PaletteInPink:
	db &54,&54,&54,&44
	db &54,&54,&44,&58
	db &54,&44,&58,&45
	db &54,&58,&45,&4F
PaletteOutPink:
	db &54,&44,&58,&45
	db &54,&54,&44,&58
	db &54,&54,&54,&44
	db &54,&54,&54,&54
	
DoPalFadeOutQ:
	xor a
	ld (FireDown),a
	inc a			;a=1
	call DoPalFadeOut
	jp cls
DoPalFadeInQ:
	xor a
	call DoPalFadeIn
	jp Set2D3Dmode

DoPalFadeIn:
	ld de,PaletteInPink
	ld hl,PaletteInBlue
	jr PalFade
DoPalFadeOut:
	ld de,PaletteOutPink
	ld hl,PaletteOutBlue
	
PalFade:					;Show a sequence of 4 palettes		
	or a						;0=Pink
	jr nz,PalFadeBluePal		;1=Blue
	ex de,hl
PalFadeBluePal:
	ld b,4
PalFadeB:
	push bc
	call SetPaletteAlt4			;Show palette HL
		ld bc,50
		push hl
			call PauseABC	
		pop hl
	pop bc
	djnz PalFadeB
	ret

	read "CPC_Sprites.asm"

TitleTiles3D:
	incbin "\ResALL\SuckHunt\CPCTitleTiles3D.RAW"
TitleTiles:
	incbin "\ResALL\SuckHunt\CPCTitleTiles.raw"

TitleTileMap2D:
  db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,0,0
  db 0,0,0,0,0,0,0,5,6,7,6,7,8,0,0,0,9,10,11,12,0,0,0,0,0,0,13,14,15,16,0,0
  db 0,0,0,0,0,0,0,17,7,6,7,6,18,0,0,0,19,20,21,22,0,0,0,0,0,0,0,0,0,0,0,0
  db 5,6,7,6,7,8,0,23,24,25,0,0,0,0,0,0,26,27,28,29,0,0,5,6,7,6,7,6,0,0,0,0
  db 17,7,6,7,6,18,0,30,31,31,32,25,0,0,0,0,33,34,35,36,0,0,17,7,6,7,6,18,0,0,0,0
  db 0,0,0,37,38,39,40,41,42,31,43,44,45,0,0,0,0,0,0,0,0,0,0,0,0,46,47,48,0,0,0,0
  db 0,0,49,50,51,52,53,0,54,55,56,57,58,59,0,0,0,0,0,0,60,61,62,63,0,0,64,0,0,0,0,0
  db 0,0,65,66,67,68,69,0,70,57,71,72,73,74,0,0,0,0,0,0,75,76,77,78,0,0,0,0,79,80,0,0
  db 68,81,82,83,84,85,86,40,87,88,73,43,89,45,90,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 91,92,93,94,95,96,97,98,99,100,101,102,31,31,103,31,0,104,105,0,0,0,0,0,0,106,107,0,0,106,107,0
  db 108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,31,123,31,124,125,126,127,0,0,128,129,130,131,128,129,130,131
  db 132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,0,0,0,0,0,0,0,0,0
  db 155,156,157,158,159,160,0,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,0,177,178,179,180,181,182,0,0
  db 0,0,0,183,184,185,186,31,31,187,0,31,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,0
  db 0,0,0,0,207,0,208,31,31,209,210,31,31,211,212,213,214,161,215,216,217,218,219,220,221,222,223,224,225,226,227,0
  db 0,0,0,0,0,0,212,31,31,228,229,31,31,0,31,230,231,187,231,232,233,234,235,236,237,238,239,240,241,242,243,0
  db 0,0,0,0,0,186,31,31,31,211,212,31,187,0,244,245,246,247,248,249,250,251,0,252,253,254,255,0,255,1,255,2,255,3,0,0
  db 0,0,0,0,0,255,4,255,5,255,5,255,5,0,255,6,255,6,255,7,0,0,0,0,0,0,0,0,0,0,0,255,8,255,9,255,10,255,11,255,12,255,13,255,14,255,15
  db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,16,255,17,255,18,255,19,255,20,255,21,255,22,255,23,255,24
  db 255,25,255,26,255,27,255,28,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,29,255,30,255,31,255,32,255,33,255,34,255,35,255,36
  db 255,37,0,255,38,255,39,255,40,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,41,255,42,255,43,0,0,255,44,255,44,255,44,0,0,0
  db 255,45,255,46,255,47,255,48,255,49,255,50,0,255,41,255,42,255,43,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,41,255,42
  db 255,51,255,52,255,53,255,54,255,55,255,56,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 255,57,255,58,255,59,255,60,255,61,255,62,255,63,255,64,255,65,255,66,255,67,255,68,255,69,255,70,255,71,255,72,0,0,0,255,44,255,44,255,44,0,0,0,0,0,0,0,0,0,0

TitleTileMap3D:
  db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,0,0
  db 0,0,0,0,0,0,5,6,7,8,9,10,11,0,0,12,13,14,15,16,0,0,0,0,0,0,17,18,19,20,0,0
  db 0,0,0,0,0,0,21,22,23,24,25,26,27,0,0,28,29,30,31,32,0,0,0,0,0,0,0,0,0,0,0,0
  db 6,7,8,9,10,11,0,33,34,35,36,0,0,0,0,37,38,39,40,41,0,5,6,7,8,9,10,11,0,0,0,0
  db 22,23,24,25,26,27,0,42,43,44,45,46,36,0,0,47,48,49,50,51,0,21,22,23,24,25,26,27,0,0,0,0
  db 0,0,0,52,53,54,55,56,57,44,58,59,60,61,0,0,0,0,0,0,0,0,0,0,62,63,64,65,0,0,0,0
  db 0,0,66,67,68,69,70,71,72,73,74,75,76,77,0,0,0,0,0,0,78,79,80,81,0,82,83,0,0,0,0,0
  db 0,0,84,85,86,87,88,89,90,91,92,93,94,95,96,0,0,0,0,97,98,99,100,101,0,0,0,102,103,104,0,0
  db 105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 122,123,124,125,126,127,128,129,130,131,132,133,134,44,135,44,136,137,138,139,0,0,0,0,140,141,142,0,140,141,142,0
  db 143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,36,165,166,167,168,169,166,167,168,169
  db 170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,0,0,0,0,0,0,0,0,0
  db 193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,0,216,217,218,219,220,221,0,0
  db 0,0,0,222,223,224,225,226,44,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,0
  db 0,0,0,0,249,250,251,252,44,253,254,255,0,44,255,1,255,2,255,3,255,4,255,5,255,6,255,7,255,8,255,9,255,10,255,11,255,12,255,13,255,14,255,15,255,16,255,17,255,18,0
  db 0,0,0,0,0,0,255,19,44,44,255,20,255,21,44,44,255,22,255,23,255,24,255,25,255,26,255,27,255,28,255,29,255,30,255,31,255,32,255,33,255,34,255,35,255,36,255,37,255,38,255,39,0
  db 0,0,0,0,0,255,40,226,44,44,255,1,255,41,44,227,228,255,42,255,43,255,44,255,45,255,46,255,47,255,48,255,49,0,255,50,255,51,255,52,255,53,255,54,255,55,255,56,0,0
  db 0,0,0,0,0,255,57,255,58,255,59,255,59,255,60,255,61,255,62,255,63,255,64,0,0,0,0,0,0,0,0,0,0,255,65,255,66,255,67,255,68,255,69,255,70,255,71,255,72
  db 0,0,0,0,0,0,0,0,0,0,0,255,73,255,74,255,75,255,76,0,44,255,77,255,78,255,79,0,0,0,255,80,255,81,255,82,255,83,255,84,255,85,255,86,255,87,255,88
  db 255,89,255,90,255,91,255,92,0,0,0,0,0,0,0,0,255,93,255,94,255,95,0,44,255,96,255,97,255,98,0,255,99,255,100,36,255,101,255,102,255,103,255,104,255,105,255,106,255,107,255,108
  db 255,109,0,255,110,255,111,255,112,0,0,255,99,255,100,36,0,255,113,255,114,255,115,255,116,0,44,255,117,255,118,255,119,0,255,120,255,121,255,122,0,0,255,123,255,124,255,124,255,125,255,99,255,100
  db 255,126,255,127,255,128,255,129,255,130,255,131,0,255,120,255,121,255,122,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,120,255,121
  db 255,132,255,133,255,134,255,135,255,136,255,137,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 255,138,255,139,255,140,255,141,255,142,255,143,255,144,255,145,255,146,255,147,255,148,255,149,255,150,255,151,255,152,255,153,0,0,0,255,123,255,124,255,124,255,125,0,0,0,0,0,0,0,0,0

SuckOfDeath3dTileMap:
  db 0,0,0,0,0,1,2,3,0,0,0,0,0,0,0,0
  db 0,0,0,0,0,4,5,6,7,8,0,0,0,0,0,0
  db 0,0,0,0,0,0,0,9,10,11,12,0,0,0,0,0
  db 0,0,0,0,13,14,15,16,17,18,19,20,21,22,23,0
  db 0,0,0,24,25,26,27,28,29,30,31,32,33,34,35,0
  db 0,0,0,36,37,38,39,40,41,42,43,44,45,46,47,0
  db 0,0,48,49,50,51,52,53,54,55,56,57,58,59,60,61
  db 0,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76
  db 77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,0
  db 92,93,94,0,95,96,97,98,99,100,101,102,103,104,105,106
  db 107,108,109,0,110,111,112,113,114,115,116,117,118,119,120,121
  db 122,123,124,0,125,126,127,128,129,130,131,132,133,134,135,136

SuckOfDeath2dTileMap:
  db 0,0,0,0,0,137,138,139,0,0,0,0,0,0,0,0
  db 0,0,0,0,0,140,141,142,143,144,0,0,0,0,0,0
  db 0,0,0,0,0,0,0,145,146,147,148,0,0,0,0,0
  db 0,0,0,0,149,150,151,152,153,154,155,156,157,158,0,0
  db 0,0,0,159,160,161,162,163,164,165,166,167,168,169,0,0
  db 0,0,170,171,172,173,174,175,176,177,178,179,180,181,0,0
  db 0,0,182,183,184,185,186,187,188,189,190,191,192,193,194,195
  db 0,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210
  db 211,212,213,214,215,216,217,0,218,219,220,221,222,223,224,0
  db 225,226,227,0,228,229,230,231,232,233,234,235,236,237,238,239
  db 240,241,242,0,243,244,245,246,247,248,249,250,251,252,253,254
  db 255,0,255,1,255,2,0,255,3,255,4,255,5,0,255,6,255,7,255,8,255,9,255,10,255,11,255,12,255,13

SuckOfDeath3dTiles:
	incbin "\ResALL\SuckHunt\SuckofDeathCPC3d.RAW"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;	Intro Graphics

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	list
	ifdef Enable_Intro

Intro1:
  db 0,0,0,0,0,1,1,2,0,0,0,0,3,4,3,5
  db 0,0,0,0,0,6,7,8,9,0,0,0,10,11,12,13
  db 14,0,0,0,0,15,16,17,9,0,0,0,18,19,20,21
  db 3,22,0,0,0,23,24,15,25,0,0,0,26,27,28,9
  db 29,30,31,6,32,33,34,35,6,0,36,37,23,38,39,9
  db 10,40,41,30,42,43,44,45,46,47,48,49,9,6,50,16
  db 51,48,52,53,54,55,56,57,58,59,60,61,9,21,62,27
  db 0,63,64,65,66,67,68,69,70,71,72,73,23,24,24,24
  db 0,0,0,74,75,76,77,7,35,78,0,0,79,80,24,38
  db 0,0,0,0,0,23,81,82,83,84,0,0,85,86,87,88
  db 0,0,0,0,6,22,89,90,39,91,0,0,92,93,94,95
  db 0,0,0,96,24,24,24,38,25,9,0,0,23,97,98,99
  
Intro1b:
  db 0,0,0,0,0,1,1,2,0,0,0,0,3,4,3,5
  db 0,0,0,0,0,6,7,8,9,0,0,0,10,11,12,13
  db 0,0,0,0,0,15,16,17,9,0,0,0,18,19,20,21
  db 0,0,0,0,0,23,24,15,25,0,0,0,26,27,28,9
  db 31,6,32,33,0,0,27,35,6,0,0,0,255,126,255,24,39,255,127
  db 41,48,42,43,255,128,0,75,45,46,0,0,47,48,49,77,255,129
  db 52,53,54,55,56,255,130,255,131,57,58,255,132,255,133,59,60,61,255,134,255,135
  db 64,65,66,67,68,0,255,136,69,35,0,255,137,71,72,255,138,24,24
  db 0,74,75,76,0,0,77,7,35,255,139,0,78,79,80,24,38
  db 0,0,0,0,0,23,81,82,83,84,0,0,85,86,87,88
  db 0,0,0,0,6,22,89,90,39,91,0,0,92,93,94,95
  db 0,0,0,96,24,24,24,38,25,9,0,0,23,97,98,99

Intro2:
  db 0,100,101,102,0,103,2,104,0,104,0,105,0,0,0,105
  db 0,0,0,100,106,0,0,74,0,104,0,107,0,0,0,107
  db 0,0,0,108,109,101,0,0,12,110,111,112,0,0,0,113
  db 0,0,114,115,53,48,116,0,117,118,119,120,0,0,0,107
  db 0,0,121,53,48,122,10,48,48,48,123,124,125,126,0,113
  db 0,127,128,129,130,131,132,133,134,48,135,136,137,138,139,107
  db 140,141,142,78,143,144,24,145,146,147,48,48,48,148,149,113
  db 0,140,150,0,9,0,103,102,151,152,153,154,155,156,157,158
  db 0,159,160,0,161,102,0,0,151,162,163,164,165,166,167,168
  db 0,169,0,0,0,0,103,102,170,162,171,169,172,173,169,150
  db 0,174,0,0,0,175,0,0,151,176,177,169,178,179,169,160
  db 0,0,0,0,0,180,0,0,181,182,183,169,184,139,174,185
  
Intro2b:
  db 101,255,86,0,102,0,103,2,104,0,104,0,105,0,0,0,105
  db 0,100,255,87,2,255,88,0,0,74,0,104,0,107,0,0,0,107
  db 0,108,109,255,89,255,0,0,0,0,12,110,111,112,0,0,0,113
  db 114,115,255,90,48,116,0,0,0,117,118,119,120,0,0,0,107
  db 121,255,91,48,122,10,48,48,48,48,48,123,124,125,126,0,113
  db 128,129,130,131,132,133,133,133,134,48,135,136,137,138,139,107
  db 142,78,255,92,144,24,145,8,46,255,93,147,48,48,48,148,149,113
  db 150,255,94,0,0,9,0,103,102,151,152,153,154,155,156,157,158
  db 160,159,0,0,161,102,0,0,151,162,163,164,165,166,167,168
  db 0,169,0,0,0,0,103,102,170,162,171,169,172,173,169,150
  db 0,174,0,0,0,175,0,0,151,176,177,169,178,179,169,160
  db 0,0,0,0,0,180,0,0,181,182,183,169,184,139,174,185

Intro3:
  db 0,0,0,186,9,0,0,187,7,188,189,48,48,190,191,192
  db 0,0,193,0,0,0,0,30,139,0,194,195,24,196,197,198
  db 0,0,199,200,0,0,186,0,201,0,202,203,204,205,0,206
  db 0,207,191,208,139,209,0,0,0,202,210,211,212,213,126,0
  db 0,214,0,215,208,139,0,0,202,216,217,117,218,219,220,126
  db 0,221,0,0,192,222,223,202,224,225,226,151,227,147,228,229
  db 230,0,0,0,0,231,232,224,0,169,233,234,235,236,139,0
  db 237,0,0,238,239,0,0,0,0,169,231,240,0,241,242,0
  db 9,0,243,244,214,245,0,0,0,169,246,247,248,0,249,0
  db 0,0,250,251,252,253,254,254,0,113,246,0,0,255,0,241,139
  db 0,0,0,255,1,255,2,255,3,255,4,255,5,0,255,6,255,7,255,8,247,0,255,9,242
  db 0,0,0,96,255,10,255,11,255,12,255,13,96,246,0,248,0,255,8,0,255,14
  
Intro3b:
  db 0,0,0,186,9,0,0,187,7,188,189,48,48,190,191,192
  db 0,0,193,0,0,0,0,30,139,0,194,195,24,196,197,198
  db 0,0,199,200,0,0,186,0,201,0,202,203,204,205,0,206
  db 0,207,191,208,139,209,0,0,0,202,210,211,212,213,126,0
  db 0,214,0,215,208,139,0,0,202,216,217,117,218,219,220,126
  db 0,221,0,0,192,222,223,202,224,225,226,151,227,147,228,229
  db 230,0,0,0,0,255,95,232,224,0,169,233,234,235,236,139,0
  db 237,0,0,255,96,255,97,253,254,254,0,169,231,240,0,241,242,0
  db 9,0,243,255,98,255,2,255,3,255,4,255,5,0,169,246,247,248,0,249,0
  db 0,0,250,255,99,255,100,255,11,255,12,255,13,0,113,246,0,0,255,0,241,139
  db 0,0,0,96,255,101,48,255,102,255,103,0,255,6,255,7,255,8,247,0,255,9,242
  db 0,0,0,0,255,104,255,105,48,255,106,255,107,246,0,248,0,255,8,0,255,14
  
Intro4:
  db 255,15,255,16,101,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 255,17,255,18,255,19,255,20,110,37,101,101,0,0,0,255,21,255,22,255,22,255,22,255,19
  db 255,23,255,24,255,25,255,26,255,27,255,28,255,19,255,29,255,30,255,31,255,32,255,33,255,34,0,0,255,35
  db 255,18,255,19,255,19,255,30,30,255,23,255,24,255,36,255,37,152,255,19,48,30,30,255,15,37
  db 255,38,255,39,255,40,255,27,255,17,255,18,255,19,255,41,255,37,255,37,255,37,255,19,255,37,255,37,147,48
  db 134,255,42,255,43,255,44,255,45,255,46,255,46,255,47,255,37,255,48,255,39,255,3,255,37,255,49,255,50,255,51
  db 0,255,52,255,53,255,54,255,55,255,42,255,56,255,57,255,37,255,48,255,39,255,58,255,59,255,60,101,0
  db 0,0,0,0,0,255,61,255,62,255,37,255,48,255,38,255,63,255,64,255,65,255,66,255,23,255,67
  db 0,0,0,0,0,0,0,255,68,134,255,42,255,43,255,69,255,23,255,70,255,71,48
  db 0,0,0,0,0,0,96,24,255,24,24,255,68,133,255,72,255,73,48,255,74
  db 0,0,0,0,0,0,0,255,75,255,76,255,77,255,78,24,255,79,48,48,255,80
  db 0,0,0,0,0,0,0,0,0,255,81,255,82,255,83,255,84,255,23,255,85,133
  
Intro4b:
  db 255,15,255,16,101,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 255,17,255,18,255,19,255,20,110,37,101,101,0,0,0,255,21,255,22,255,22,255,22,255,19
  db 255,23,255,24,255,25,255,26,255,27,255,28,255,19,255,29,255,30,255,31,255,32,255,33,255,34,0,0,255,35
  db 255,18,255,19,255,19,255,30,30,255,23,255,24,255,36,255,37,152,255,19,48,30,30,255,15,37
  db 255,38,255,39,255,40,255,27,255,17,255,18,255,19,255,41,255,37,255,37,255,37,255,19,255,37,255,37,147,48
  db 134,255,42,255,43,255,44,255,45,255,46,255,46,255,47,255,37,255,48,255,39,255,3,255,37,255,108,255,109,255,74
  db 0,255,52,255,53,255,54,255,55,255,42,255,56,255,57,255,37,255,48,255,39,255,58,255,59,255,110,255,111,255,112
  db 0,0,0,0,0,255,61,255,62,255,37,255,48,255,38,255,63,255,64,255,113,255,114,255,73,48
  db 0,0,0,0,0,0,0,255,68,134,255,42,255,43,255,37,255,115,255,116,48,48
  db 0,0,0,0,0,0,96,24,255,24,24,255,68,133,255,117,255,118,255,119,255,120
  db 0,0,0,0,0,0,0,255,75,255,76,255,77,255,78,24,24,255,121,255,122,255,123
  db 0,0,0,0,0,0,0,0,0,255,81,255,82,255,83,75,255,124,255,68,255,125
  
IntroGraphics:	
	incbin "\ResALL\SuckHunt\SuckHuntIntro.RAW"

IntroAnimList:
	db 0,13,1			;X,Y,Colors
	dw Intro1				;Frame 1
	dw Intro1b				;Frame 2
	db 40-16,0,0		;X,Y,Colors
	dw Intro3				;Frame 1
	dw Intro3b				;Frame 2
	db 40-16,13,0		;X,Y,Colors
	dw Intro2				;Frame 1
	dw Intro2b				;Frame 2
	db 0,0,1			;X,Y,Colors
	dw Intro4				;Frame 1
	dw Intro4b				;Frame 2
	endif


;Akuyou_SFXPos equ 1
	ifdef Akuyou_MusicPos
	
Arkos_ChangeSong:
	push hl
	call PLY_Stop		;Stop music
	di
	pop hl

	ld de,&B800			;Song Destination
	ld bc,&400			;Max song length=&400
	ldir
	call PLY_Init		;Restart music
	ret

SongIntro:
	incbin "\ResALL\SuckHunt\IntroSong.bin"
SongTitle:
	incbin "\ResALL\SuckHunt\TitleSong.bin"
SongGame:
	incbin "\ResALL\SuckHunt\GameSong.bin"

	read "\SrcALL\Multiplatform_ArkosTrackerLite.asm"

	else	
PLY_Play:
PLY_Stop
SongIntro:
SongTitle:
SongGame:
Arkos_ChangeSong:
	ret
	endif

	ifdef Enable_Speech
	
wavedata:
	incbin "\ResALL\SuckHunt\operationsucka1-6.raw"
wavedataEnd:

freq4 equ 1	;11025
bits1 equ 1
	read "\SrcALL\Multiplatform_ChibiWave.asm"
	endif

	read "\SrcALL\Multiplatform_ChibiSound.asm"			;Sound Driver
	read "\SrcALL\Multiplatform_BCD.asm"				;Show Binary Coded Decimal
	read "\SrcALL\MultiPlatform_ShowDecimal.asm"		;Show decimal number

	read "SH_Multiplatform.asm"
	read "SH_DataDefs.asm"
	read "SH_RamDefs.asm"
	read "SH_LevelInit.asm"

