
ShowCrosshair:

	ld de,(CrosshairSprite)	;Sprite Source
	ld ix,&0802
	push bc
	call GetScreenPos
	push de
		call ShowSpriteL
	pop de
;	call ZshiftScrPos
	pop bc
	call GetScreenPos
	jr ShowSpriteR
	
ShowSpriteL:
	ld b,ixh			;Lines (Height)
SpriteNextLine:
	push hl
		ld c,ixl	;Bytes per line (Width)
SpriteNextByte:
;Byte 1
		ld a,(de)
		and %11000000	;Source X-
		rrca
		rrca
		xor (hl)
		ld (hl),a	;Screen Destination
		
		ld a,(de)
		and %00110000	;Source -X
		rrca
		rrca
		rrca
		rrca
		xor (hl)
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address
;Byte 2	
		ld a,(de)
		and %00001100	;Source X-
		rlca
		rlca
		xor (hl)
		ld (hl),a	;Screen Destination
		
		ld a,(de)
		and %00000011	;Source -X
		xor (hl)
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address
		
		inc de		;INC Source (Sprite) Address
		
		dec c 		;Repeat for next byte
		jr nz,SpriteNextByte
	pop hl
	call GetNextLine	;Scr Next Line (Alter HL to move down a line)
NextLineCommand1_Plus2:

	djnz SpriteNextLine	;Repeat for next line
	doei
	ret			;Finished


	
	
ShowSpriteR:
	ld b,ixh			;Lines (Height)
SpriteNextLineR:
	push hl
		ld c,ixl	;Bytes per line (Width)
SpriteNextByteR:			
		ld a,(de)
		and %11000000	;3D MODE
		xor (hl)
		ld (hl),a	;Screen Destination
		
		ld a,(de)
		and %00110000	;3D MODE
		rrca
		rrca
		xor (hl)
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address
		
		ld a,(de)
		and %00001100	;3D MODE
		rlca
		rlca
		rlca
		rlca
		xor (hl)
		ld (hl),a	;Screen Destination
		
		ld a,(de)
		and %00000011	;3D MODE
		rlca
		rlca
		xor (hl)
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address
		
		inc de		;INC Source (Sprite) Address
		dec c 		;Repeat for next byte
		jr nz,SpriteNextByteR
	pop hl

	call GetNextLine	;Scr Next Line (Alter HL to move down a line)
NextLineCommand2_Plus2:
	djnz SpriteNextLineR	;Repeat for next line
	doei
	ret			;Finished

	
GetNextLineWithClip:
	push hl
		ld hl,1
SpriteHClipPlus2:
		add hl,de
		ex de,hl
	pop hl 
	jp GetNextLine



PrintSpace:
	ld a,' '
Printchar:
	push af
	push ix
	push bc
	push hl
	push de
		sub 32				;No Char below 32
		ld bc,FontData
PrintTileb:
		call PrintCharB
PrintTilec:
		ld hl,CursorX
		inc (hl)
	pop de
	pop hl
	pop bc
	pop ix
	pop af
	ret

PrintCharB:					;Print Char A with Sprite Routine
		ld h,a
		xor a
		
		srl h				;Tile * 16
		rra
		srl h
		rra
		srl h
		rra
		srl h
		rra
		ld l,a
		add hl,bc
		ex de,hl
		ld bc,(CursorY)		;Get XY into BC
		ld iy,SprChar

		rl c				;Ypos *4
		rl c
		rl b				;Xpos *4
		rl b
		
		ld a,b
		add 48+2
	
		ld  (IY+O_Xpos),a

		ld a,c
		add 2
		ld  (IY+O_Ypos),a
		ld  (IY+O_SprL),e
		ld  (IY+O_SprH),d
		
		ld a,(CursorZ)
		ld  (IY+O_Zpos),a
	
		jp DrawSpriteFromIY	;Use the sprite engine
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


ShowTileMapAltPosQtrIntro:	
	ld bc,IntroGraphics
	
	ld d,16		;W
	ld e,12		;H
;HL=Tilemap BC=Tile Data IYH=X	A=Y	;E=Height ;D=Wid
	
	ld (CursorY),a
	xor a
	ld (CursorZ),a
TilemapYagainHalf:

	ld a,iyh
	sub 4
	ld (CursorX),a	
	push iy
	push de
TilemapXagainHalf:
	ld a,(hl)
	inc hl

	cp 255 
	jr c,FirstTileBankHalf


	ld a,(hl)
	inc hl
	push bc
	push hl

		ld hl,256*8-8

		add hl,bc
		ld b,h
		ld c,l
		call PrintTileHalf	

	pop hl
	pop bc
	jr TileDoneHalf


FirstTileBankHalf:
	call PrintTileHalf

TileDoneHalf:

	dec d
	jr nz,TilemapXagainHalf
	push hl
		call NewLine
	pop hl
	pop de
	pop iy
	dec e
	jr nz,TilemapYagainHalf
	ret
	
	

PrintTileHalf:	;TitleTiles
	push af
	push ix
	push bc
	push hl
	push de	

		ld l,a
		xor a
		sla l				;Font * 8
		rla
		sla l				;Font * 8
		rla
		sla l				;Font * 8
		rla
		ld h,a
		add hl,bc
		ex de,hl
		ld bc,(CursorY)		;Get XY into BC
		
		call DrawTileHalf
		ld a,(CursorX)
		inc a
		ld (CursorX),a	
	
	pop de
	pop hl
	pop bc
	pop ix
	pop af
	ret
	
DrawTileHalf:
	rl b
	rl b
	rl c
	rl c
	rl c
	call GetScreenPos
	ld b,4			;Lines (Height)

TileNextLineDouble:
	push de
	call DoTileLine
	pop de
	call GetNextLine	;Scr Next Line (Alter HL to move down a line)
	call DoTileLine
	call GetNextLine	;Scr Next Line (Alter HL to move down a line)
	djnz TileNextLineDouble	;Repeat for next line
	ret
	
DoTileLine:	;Convert 4color to 16
	push hl	
		ld c,2	;Bytes per line (Width)
TileNextByteHalf:
		ld a,(de)
		and %11000000	;3D MODE
		rrca
		rrca
		ld (hl),a	;Screen Destination
		
		
		ld a,(de)
		and %00110000	;3D MODE
		rrca
		rrca
		rrca
		rrca
		xor (hl)
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address
		
		
		ld a,(de)
		and %00001100	;3D MODE
		rlca
		rlca
		ld (hl),a	;Screen Destination
		
		ld a,(de)
		and %00000011	;3D MODE
		xor (hl)
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address
		
		inc de		;INC Source (Sprite) Address
		dec c
		jr nz,TileNextByteHalf
	pop hl
	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;		Tilemap

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


GetScreenPos:	;return mempos in DE of screen co-ord B,C (X,Y)
	ld h,c	
	xor a	
	rr h		;Effectively Multiply Ypos by 128
	rra 
	or b		;Add Xpos
	ld l,a	
	ld a,&80
	add h
	ld h,a
	ret

GetNextLine:	;Move DE down 1 line 
	ld a,&80	
	add l		;Add 128 to mem position 
	ld l,a
	ret nc
	inc h		;Add any carry
	ret
	
		

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;		Clear Screen

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	
	
Cls:	

	ld de,&8000
	ld bc,&6000-1
	call Cldir
	ret
	
	