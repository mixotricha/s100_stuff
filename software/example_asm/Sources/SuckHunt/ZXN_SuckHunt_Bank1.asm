	Org &C000				;Code Origin - This will actually run from &6000+
	macro nextreg,reg,val
		db &ED,&91,\reg,\val;Macro for Setting A ZX Next Register
	endm
	macro nextregA,reg
		db &ED,&92,\reg		;Macro for Setting A ZX Next Register from A
	endm
		
	di
	nextreg &50,16+8		;Page in bank
	nextreg &51,16+9		;Page in bank
	ld hl,&6000
	ld de,&0000
	ld bc,&4000
	ldir
	nextreg &50,255			;Restore ROM
	nextreg &51,255			;Restore ROM
	ret
	
	org &C100
		

	TitleTiles3D:
	incbin "\ResALL\SuckHunt\ZXNTitleTiles3D.RAW"	
	db 0
	
	