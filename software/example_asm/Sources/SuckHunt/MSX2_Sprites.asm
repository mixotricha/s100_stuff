NextLineCommand2_Plus2
	db 0

GetScreenPos:	;return memory pos in HL of screen co-ord B,C (X,Y)
	sla b
	ld h,0
	ld l,b
	ld (SXpos),hl
	
	ld l,e
	ld (SXCrop),hl
	
	;ld h,0
	ld l,c
	ld (SYpos),hl
	ret
		
		



ShowSpriteL:
	ld ix,(SpriteDataHL)		;Lookip table
ShowSpriteR2:	
	ld a,(IY+O_SprNum)			;Sprite Num
	add (IY+O_SprH)
	and %00111111
	push iy
		ld d,0
		rlca
		rlca
		ld e,a
		add ix,de				;12 bytes per entry (6 * 2)
		sla e
		rl d
		add ix,de
			ld e,(ix+2)		;Y Src
			ld d,(ix+3)
			push de
				ld l,(ix+0)	;X Src
				ld h,(ix+1)
				ld de,(SXCrop)
				sla e
				sla e
				add hl,de
				push hl
					ld a,(ix+4)	;W
					sub e				;Crop W
					jr c,SpriteBad
					jr z,SpriteBad
					ld l,a
				
					ld e,(ix+5)	;H
					
					ld h,0
					ld d,0
				pop ix	;XSource
			pop iy  ;YSource
			exx
				ld hl,(SXpos)	;Xdest
				ld de,(SYpos)	;YDest
			exx
			call VDP_LMMM_ViaStack		;Fast Copy Vram->Vram
	pop iy
	doei	
	ret
SpriteBad:			;Sprite is offscreen
		pop ix
		pop iy
		pop iy
	ret

ShowSpriteR:
	ld a,(CurrentMode3D)
	or a 
	ret z
	ld ix,(SpriteDataHL)
	ld de,6
	add ix,de				;Move to 2nd eye sprite
	jr ShowSpriteR2
		
	

	
	

GetNextLineWithClip:
	push hl
		ld hl,1
SpriteHClipPlus2b
		add hl,de
		ex de,hl
	pop hl 
	jp GetNextLine

	
	
	PrintSpace:
	ld a,' '
Printchar:
	push af
	push ix
	push bc
	push hl
	push de
		sub 33				;No Char below 32
		jr c,PrintTileC
		
PrintTileb:
		call PrintCharB
		ld hl,SpriteMSXData
		ld (SpriteDataHL),hl
PrintTilec:

		
		ld hl,CursorX
		inc (hl)
	pop de
	pop hl
	pop bc
	pop ix
	pop af
	ret
	
PrintCharB:		;Print Char A with Sprite Routine
	ld ix,FontMSXData
	ld (SpriteDataHL),ix
	push af
		and %00011111
		rlca
		rlca
		rlca
		
		ld (ix+0),a			;Xpos
		ld (ix+0+6),a		;Xpos
		xor a
		ld (ix+1),a
		ld (ix+1+6),a
	pop af
	and %11100000
	rrca
	rrca
	ld (ix+2),a
	add 24
	ld (ix+2+6),a
	ld a,1				;256
	ld (ix+3),a			;Ypos
	ld (ix+3+6),a		;Ypos
		
	;Fast Copy The sprite
		
	ld a,8			;Width
	ld (ix+4),a		;W - L
	ld (ix+5),a 	;H - L
	ld (ix+4+6),a	;W - R
	ld (ix+5+6),a 	;H - R
	 
	ld iy,SprChar
	ld bc,(CursorY)		;Get XY into BC
	rl c
	rl c
	rl b				;Xpos
	rl b

	ld a,b
	add 48+2
	ld  (IY+O_Xpos),a

	ld a,c
	add 2
	ld  (IY+O_Ypos),a
			
	ld a,(CursorZ)
	ld  (IY+O_Zpos),a

	jp DrawSpriteFromIY


	

UploadOneSprite:
	ld iy,SpriteInfo		;Source Data
	ld ix,SpriteMSXData		;Dest MSX Conversion table 
	ld d,0
	rlca					;A=Sprite Number * 4
	rlca
	ld e,a
	add iy,de				;Sprite * 4
	add ix,de
	sla e					;A=Sprite Number * 8
	rl d
	add ix,de				;Sprite *12
	
UploadOneSpriteAlt:	
	ld e,%00110011
	
	call UploadSprite	;Left Eye
	ld de,6
	add ix,de
	
	ld e,%11001100
	call UploadSprite	;Right Eye
	ret

	
;ROM Cart SpriteInfo (4 bytes per entry)
    ;dw SpriteAddr
     ;db Width, Height
	
;MSX 2 Generated Table (6 bytes per entry)
	;dw Xpos, Ypos
	 ;db Width,Height
	
	
UploadSprite:
	push ix
	ld hl,0
	ld b,0
	ld c,(iy+2)		;Sprite Width
	srl c			
	srl c
	ld a,(iy+3)		;Sprite Height
	
DoubleBytes:
	add hl,bc
	dec a
	jr nz,DoubleBytes
	
	ld c,l			;Height
	ld b,h
	
	ld a,(iy+0)		;Source Sprite Bitmap
	ld l,a
	ld a,(iy+1)
	ld h,a
	call DoByte1	;Convert first Byte
	and e
	
	push de
	push bc
	push hl
	push iy
		push af
RetryNextLine2:	
			ld e,(iy+3)		;Height
			ld (ix+5),e
			ld d,0
			ld hl,(SYpos)
			ld a,l
			ld (ix+2),a		;16 bit Ypos
			ld a,h
			ld (ix+3),a
			
			add hl,de
			ld (NextYpos),hl
			
			push de
				ld de,(maxYpos)
				or a
				sbc hl,de
				jr c,NotHigherY
				ld hl,(NextYpos)
				ld (maxYpos),hl
NotHigherY:		
				ld d,0
				ld a,(iy+2)		;Width
				or a
				jr nz,WidthSizeNZ
				inc d
WidthSizeNZ:
				ld e,a
				ld (ix+4),e		;Width
				
				
				ld hl,(SXpos)
				ld a,l
				ld (ix+0),a		;16 bit Xpos
				ld a,h
				ld (ix+1),a
			
				add hl,de
				ld (NextXpos),hl
				bit 0,h
				jr z,NotOverX		;is new X>256
				xor a
				cp l
				jr z,NotOverX
				
				jp RetryNextLine
NotOverX:
				ex de,hl			;HL=Width
			pop de
			
			ld ix,(SXpos)	;Xpos
			ld iy,(SYpos)	;Ypos
		pop af
		call VDP_HMMC_Generated_ViaStack
	pop iy
	pop hl
	pop bc
	pop de
	jr NextByte2
	
NextByte:	
	call DoByte1		;Convert Byte 1
	and e
	out (Vdp_SendByteData),a	;Send next byte to vdp
	
NextByte2:
	call DoByte2		;Convert Byte 2
	and e
	out (Vdp_SendByteData),a	;Send next byte to vdp
	dec bc
	ld a,b
	or c
	jr nz,NextByte
	
	ld hl,(NextXpos)
	ld a,h
	or a
	jr z,UpdateXpos
	
	ld hl,(maxYpos)
	ld (SYpos),hl
	ld hl,0
		
UpdateXpos:
	ld (SXpos),hl
	pop ix
	ret

DoByte2:
	ld a,(hl)
	and %00001100
	rlca
	rlca
	ld d,a
	ld a,(hl)
	and %00000011
	or d
	ld d,a
	rlca
	rlca
	or d
	inc hl
	ret
		
DoByte1:
	ld a,(hl)
	and %11000000
	ld d,a
	ld a,(hl)
	and %00110000
	rrca
	rrca
	or d
	ld d,a
	rrca
	rrca
	or d
	ret
		
	
	
RetryNextLine:		;This line is full... move down a strip
	pop de
	ld hl,(maxYpos)			;Position for next Yline
	ld (SYpos),hl
	
	ld hl,0
	ld (SXpos),hl
	
	jp RetryNextLine2
	
	
UploadSprite16Color:
NextByte3:
	ld a,(hl)					;Read next byte of sprite
	out (Vdp_SendByteData),a	;Send next byte to vdp
	dec bc
	inc hl
	ld a,b
	or c
	jr nz,NextByte3
	ret
	
	
	
	
	

ShowTileMapAltPosQtrIntro:	
	ld bc,VramYpos_Intro;IntroGraphics	
ShowTileMapAltPosQtr:
	ld d,16		;W
	ld e,12		;H
	jr ShowTileMapAltPos
	
ShowTileMap:		;HL=Tilemap BC=Tile Data Ypos
	ld iyh,4	;X
	ld a,0		;Y
	ld e,24		;Hei
	ld d,32		;Wid
	
;HL=Tilemap BC=Tile Data IYH=X	A=Y	;E=Height ;D=Wid
ShowTileMapAltPos:	
	ld (CursorY),a
	xor a
	ld (CursorZ),a
	
TilemapYagain:
	ld a,iyh
	sub 4
	ld (CursorX),a	
	push iy
	push de
TilemapXagain:
		ld a,(hl)
		inc hl
		cp 255 
		jr c,FirstTileBank

		ld a,(hl)
		inc hl
		dec a
		cp 255 
		jr z,FirstTileBank
		
		push bc
		push hl
			ld hl,64			;64 lines down for 2nd bank
TileBankSize_Plus2:
			add hl,bc
			ld b,h
			ld c,l
			call PrintTile	
TileEngine_Plus2:
		pop hl
		pop bc
		jr TileDone
FirstTileBank:
		call PrintTile
TileEngine_Plus2B:
TileDone:
		dec d
		jr nz,TilemapXagain
		push hl
			call NewLine
		pop hl
	pop de
	pop iy
	dec e
	jr nz,TilemapYagain
	ret
	
	
PrintTile:					;Draw Tile A
	push ix
	push bc
	push hl
	push de	
		dodi
		call DrawTile
		doei
		ld a,(CursorX)
		inc a
		ld (CursorX),a	
	pop de
	pop hl
	pop bc
	pop ix
	ret
	
DrawTile:
	push af
		ld h,0
		and %11100000		;Line number (8 pixels tall)
		rrca
		rrca
		ld l,a
		add hl,bc
		ex de,hl
		ld iyh,d			;Ypos
		ld iyl,e
	pop af
	and %00011111			;32 x 8 pixel tiles per line
	rlca
	rlca
	rlca
	ld ixl,a				;Xpos
	ld ixh,0
	
	ld a,(CursorX)
	rlca
	rlca
	rlca
	ld h,0
	ld l,a
	ld a,(CursorY)
	rlca
	rlca
	rlca
	ld d,0
	ld e,a
	exx
		ld hl,8			;Width
		ld de,8			;Height
	ld bc,&0090		;Command Byte (PSET)
	call VDP_LMMM_ViaStack2	;Fast Copy Vram->Vram		
	ret
	
	