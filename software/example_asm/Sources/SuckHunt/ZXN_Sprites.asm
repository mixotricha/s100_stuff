

ShowCrosshair:

	ld de,(CrosshairSprite)	;Sprite Source
	ld ix,&0802
	push bc
	call GetScreenPosHD
	push de
		call ShowSpriteL
	pop de
;	call ZshiftScrPos
	pop bc
	call GetScreenPosHD
	jr ShowSpriteR
	
	
ShowSpriteL:
	ld b,ixh			;Lines (Height)
SpriteNextLine:
	push hl
		ld c,ixl	;Bytes per line (Width)
SpriteNextByte:
		ld a,(de)
		and %11000000	;3D MODE
		rlca
		rlca
		xor (hl)
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address

		ld a,(de)
		and %00110000	;3D MODE
		rlca
		rlca
		rlca
		rlca
		xor (hl)
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address
		
		ld a,(de)
		and %00001100	;3D MODE
		rrca
		rrca
		xor (hl)
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address

		ld a,(de)
		and %00000011	;3D MODE
		xor (hl)
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address
		
		inc de		;INC Source (Sprite) Address
		dec c 		;Repeat for next byte
		jr nz,SpriteNextByte
	pop hl
	call GetNextLine	;Scr Next Line (Alter HL to move down a line)
NextLineCommand1_Plus2:

	djnz SpriteNextLine	;Repeat for next line
	doei
	ret			;Finished


	
	
ShowSpriteR:
;	RET ;2D Mode
	ld b,ixh			;Lines (Height)

SpriteNextLineR:
	push hl
		ld c,ixl	;Bytes per line (Width)
SpriteNextByteR:
		;ld a,(hl)	;Source Byte
		;and %11110000
		;ld (hl),a
			
		ld a,(de)
		and %11000000	;3D MODE
		rrca
		rrca
		rrca
		rrca
		xor (hl)
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address

		ld a,(de)
		and %00110000	;3D MODE
		rrca
		rrca
		xor (hl)
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address
		
		ld a,(de)
		and %00001100	;3D MODE		
		xor (hl)
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address

		ld a,(de)
		and %00000011	;3D MODE
		rlca
		rlca
		xor (hl)
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address
		
		inc de		;INC Source (Sprite) Address
		dec c 		;Repeat for next byte
		jr nz,SpriteNextByteR
	pop hl

	call GetNextLine	;Scr Next Line (Alter HL to move down a line)
NextLineCommand2_Plus2:
	djnz SpriteNextLineR	;Repeat for next line
	doei
	ret			;Finished



	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GetScreenPos:	;return memory pos in HL of screen co-ord B,C (X,Y)
	sla b
GetScreenPosHD:
	dodi
	push af
	push bc
		ld l,b	
		ld a,c
		and %00011111	;Offset in third of bank
		or  %00100000
		ld h,a
		ld a,c
		and %11100000	;Bank in correct sixth for &2000-&3fff
		
; *** R Bit is new as of Core 3 allowing writes to ROM area VRAM, 
; But our Emulator doesn't support it - that's what happens when you 
; keep F-ing with the spec after release!
		
		rlca			;Shift XXX-----
		rlca			
		rlca			;Shift -----XXX
		add 16			;Bank 16 is first Layer 2 bank
		
		nextregA &51	;Page in B to &2000-&3FFF range
	pop bc
	pop af
	ret
	
GetNextLineWithClip:
	push hl
		ld hl,1
SpriteHClipPlus2:
		add hl,de
		ex de,hl
	pop hl 
	;jp GetNextLine

GetNextLine:		;Move DE down a line
		inc h
		ld a,h
		and %00011111	;32 lines per bank
		ret nz			;Return if not changed bank
		or  %00100000
		ld h,a
		GetNextReg &51
		inc a			;8k banks	
		nextregA &51	;Page in B+1 to &2000-&3FFF range
	ret

	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	

PrintSpace:
	ld a,' '
Printchar:
	push af
	push ix
	push bc
	push hl
	push de
		sub 32				;No Char below 32
		ld bc,FontData
PrintTileb:
		call PrintCharB
PrintTilec:
		ld hl,CursorX
		inc (hl)
	pop de
	pop hl
	pop bc
	pop ix
	pop af
	ret

PrintCharB:					;Print Char A with Sprite Routine
		ld h,a
		xor a
		
		srl h				;Tile * 16
		rra
		srl h
		rra
		srl h
		rra
		srl h
		rra
		ld l,a
		add hl,bc
		ex de,hl
		ld bc,(CursorY)		;Get XY into BC
		ld iy,SprChar

		rl c					;Ypos *4
		rl c
		rl b					;Xpos *4
		rl b

		ld a,b
		add 48+2
		ld  (IY+O_Xpos),a		;Xpos

		ld a,c
		add 2
		ld  (IY+O_Ypos),a		;Ypos
		
		ld  (IY+O_SprL),e		;Sprite source data
		ld  (IY+O_SprH),d
		
		ld a,(CursorZ)
		ld  (IY+O_Zpos),a		;Zpos
	
		jp DrawSpriteFromIY		;Use the sprite engine
	
	
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



	
ShowTileMapAltPosQtrIntro:	
	ld de,256*8-8
	ld (TileBankSize_Plus2-2),de
	ld de,PrintTileHalf	
	
	ld bc,IntroGraphics
	jr ShowTileMapAltPosQtrB
	
	;HL=Tilemap BC=Tile Data IYH=X	A=Y 	;E=Height ;D=Wid
ShowTileMapAltPosQtr:
	ld de,256*32-32
	ld (TileBankSize_Plus2-2),de
	ld de,PrintTile	
	
ShowTileMapAltPosQtrB:
	ld (TileEngine_Plus2-2),de
	ld (TileEngine_Plus2B-2),de
	
	ld d,16		;W
	ld e,12		;H
	jr ShowTileMapAltPos
	
ShowTileMap:						;HL=Tilemap BC=Tile Data
	ld de,PrintTile					;Selfmod in the settings
	ld (TileEngine_Plus2-2),de
	ld (TileEngine_Plus2B-2),de
	
	ld de,256*32-32
	ld (TileBankSize_Plus2-2),de	;Offset to 2nd tile bank (255+)

	ld iyh,4	;X
	ld a,0		;Y
	ld e,24
	ld d,32
ShowTileMapAltPos:	;HL=Tilemap BC=Tile Data IYH=X	A=Y	;E=Height ;D=Wid
	
	ld (CursorY),a
	xor a
	ld (CursorZ),a
TilemapYagain:

	ld a,iyh
	sub 4
	ld (CursorX),a	
	push iy
	push de
TilemapXagain:
	ld a,(hl)					;Load Tile num
	inc hl

	cp 255 						;255+ = Second bank
	jr c,FirstTileBank

	ld a,(hl)
	inc hl
	push bc
	push hl
		ld hl,256*32-32			;Address of tiles 255+
TileBankSize_Plus2:
		add hl,bc
		ld b,h
		ld c,l
		call PrintTile			;Show 2nd bank tile
TileEngine_Plus2:
	pop hl
	pop bc
	jr TileDone

FirstTileBank:
	call PrintTile				;Show 1st bank tile
TileEngine_Plus2B:
TileDone:
	dec d
	jr nz,TilemapXagain
	push hl
		call NewLine
	pop hl
	pop de
	pop iy
	dec e
	jr nz,TilemapYagain
	ret
	
	
PrintTile:	;TitleTiles
	push af
	push ix
	push bc
	push hl
	push de	

		ld h,a
		xor a
		
		srl h				;Tile * 16
		rra
		srl h
		rra
		srl h
		rra
		;srl h
		;rra
		ld l,a
		add hl,bc
		ex de,hl
		ld bc,(CursorY)		;Get XY into BC
		
		call DrawTile
		ld a,(CursorX)
		inc a
		ld (CursorX),a	
	
	pop de
	pop hl
	pop bc
	pop ix
	pop af
	ret

	

PrintTileHalf:	;TitleTiles (8x4 pixel)
	push af
	push ix
	push bc
	push hl
	push de	
		ld l,a
		xor a
		sla l				;Font * 8
		rla
		sla l				;Font * 8
		rla
		sla l				;Font * 8
		rla
		ld h,a
		add hl,bc
		ex de,hl
		ld bc,(CursorY)		;Get XY into BC
		
		call DrawTileHalf	;Draw two lines
		ld a,(CursorX)
		inc a
		ld (CursorX),a	
	pop de
	pop hl
	pop bc
	pop ix
	pop af
	ret

DrawTileHalf:
	rl b			
	rl b
	rl c
	rl c
	rl c
	call GetScreenPos
	ld b,4			;Lines (Height)

TileNextLineDouble:		;Draw two lines
	push de
	call DoTileLine
	pop de
	call GetNextLine	;Scr Next Line (Alter HL to move down a line)
	call DoTileLine
	call GetNextLine	;Scr Next Line (Alter HL to move down a line)
	djnz TileNextLineDouble	;Repeat for next line
	ret
	
	
DrawTile:
	rl b
	rl b
	rl c
	rl c
	rl c
	call GetScreenPos
	ld b,8			;Lines (Height)

TileNextLine:
	push hl
		ld c,4	;Bytes per line (Width)
TileNextByte:
		ld a,(de)
		and %11110000
		;and %11000000
		rlca
		rlca
		rlca
		rlca
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address
		ld a,(de)
		and %00001111
		;and %00001100
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address
		
		
		inc de		;INC Source (Sprite) Address
		dec c 		;Repeat for next byte
		jr nz,TileNextByte
	pop hl

	
	call GetNextLine	;Scr Next Line (Alter HL to move down a line)
	djnz TileNextLine	;Repeat for next line
	ret
	
	
	
DoTileLine:
	push hl	
		Call DoTileByte
		Call DoTileByte
	pop hl
	ret
DoTileByte:

		ld a,(de)
		and %11000000	;3D MODE
		rlca
		rlca
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address

		
		ld a,(de)
		and %00110000	;3D MODE
		rrca
		rrca
		rrca
		rrca
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address
		ld a,(de)
		and %00001100	;3D MODE
		rrca
		rrca
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address

		ld a,(de)
		and %00000011	;3D MODE
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address
		inc de		;INC Source (Sprite) Address

	ret
