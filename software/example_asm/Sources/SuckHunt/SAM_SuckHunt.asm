;*************************************************************************************
;*************************************************************************************

;	1st Bank (&0000-7FFF)

;*************************************************************************************
;*************************************************************************************


Enable_Intro equ 1
Use3DTilemaps equ 1		;Gameover in 3d?
Enable_Speech equ 1


GunSprite equ 1
;PLY_SystemFriendly equ 1

VscreenMinX equ 64		;Top left of visible screen in logical co-ordinates
VscreenMinY equ 0

VscreenWid equ 128		;Screen Size in logical units
VscreenHei equ 96
VscreenWidClip equ 1

ScreenSize256 equ 1
pixelsPerByte2 equ 1 	;Use full logical res

ScreenObjWidth equ 80-12
ScreenObjHeight equ 200-48
DoFarCall equ &E000


	Org &0000	;Code Origin
Akuyou_MusicPos equ &F800

GameRam equ &F000
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ifdef vasm
		include "\SrcALL\VasmBuildCompat.asm"
	else
		read "\SrcALL\WinApeBuildCompat.asm"
	endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	read "\SrcALL\CPU_Compatability.asm"

	Macro DoEI
		ei
	endm
	macro DoDI
		di
	endm
	macro ScreenStopDrawing
	endm	
	
;When our program starts, this is loaded to &8000, we page it into the low area
;Then run it at that address - this is so we can use IM1 for music during 
;Screen draw	
	
	
	di	;The Sam Screen is 24k, and we need to move it in at &0000-&7FFF
		;we must keep interrupts disabled the whole time
	
		; WRrBBBBB W=Write protect Bank (&0000-&3FFF) / r=ram in low area 
	ld a,%00100001 					;/ R=rom in high area / B=low ram Bank
	out (250),a		
	ld sp,&FFF0		;Define a stack pointer
	jp Start

	org &100
Start:	
		; MSSBBBBB	- M=Midi io / S=Screen mode / B=video Bank
	ld a,%01101110
	out (252),a		;VMPR - Video Memory Page Register (252 dec)
		
	ld a,%00101110  
	out (251),a		;VRAM to area &0000-&FFFF
	
	
	ld hl,FarCall
	ld de,DoFarCall		;Copy the 'Far Call handler' to VRAM bank at &E000
	ld bc,32			;This calls an address in a different bank
	ldir 
		
	
	ld hl,&38
	ld a,&C3			;JP
	ld (hl),a
	inc hl
	ld de,InterruptHandler	;Install our IM1 interrupt handler
	ld (hl),e
	inc hl
	ld (hl),d
	
	
	ld de,GameRam
	ld bc,GameRamEnd-GameRam-1
	call Cldir				;Init the game ram

	ld hl,DefChar
	ld de,SprChar
	ld bc,16
	ldir					;Set up the 'Sprite template' for font routine
	

	ld a,1
	ld (PaletteNum),a		;Default palette
	ld a,3
	ld (Depth3D),a			;Default 3D Depth
	ld hl,1
	ld (CursorMoveSpeed),hl	;Default Cursor speed

	ld hl,SndSilence
	ld (SoundSequence),hl	;Current playing sound (Silent)


	ld a,(ShowSpriteR)		;Backup Selfmodifying code for Stereo3D engine
	ld (Bak3d),a
	;ld hl,(ShowSpriteL2DMode)
	;ld (Bak3d+1),hl
	
ShowIntroAgain:	
	call DoPalFadeOutQ		;Fade to black

	ifdef Enable_Intro
		ld hl,SongIntro
		call Arkos_ChangeSong	;Play intro song (OpWolf spoof)
			
		ld b,4
		ld ix,IntroAnimList		;4 animations
DrawIntroAnim:
		push bc
			call cls
			doei

			ld a,(ix+0)		;X
			ld iyh,a
			ld a,(ix+1)		;Y
			ld e,(ix+3)
			ld d,(ix+4)		;DE= Mini Tilemap 
			ex de,hl
			push iy
			push ix
			push af
			;HL=Tilemap BC=Tile Data IYH=X	A=Y 	;E=Height ;D=Wid
				call ShowTileMapAltPosQtrIntro	
				
				ld a,(ix+2)			;Fade color
				call DoPalFadeIn	;Fade in
				doei
				ld bc,200
				call PauseABC		;Delay
			pop af
			pop ix
			pop iy

			push ix
				ld e,(ix+5)
				ld d,(ix+6)			;2nd Frame Tilemnap
				ex de,hl
			;HL=Tilemap BC=Tile Data IYH=X	A=Y 	;E=Height ;D=Wid
				call ShowTileMapAltPosQtrIntro	
				doei
				ld bc,300
				call PauseABC		;Delay
				
				ld a,(ix+2)			;Fade color
				call DoPalFadeOut	;Fade out
			pop ix
			ld bc,7
			add ix,bc				;Move to next frame
		pop bc
		djnz DrawIntroAnim
	endif

	call cls						;Clear Screen
	doei	
	
	ld bc,500
	call PauseABC					;Delay
	
	ld a,1							;Play Speech
DrawTitleScreen:
	ld sp,&FFF0		;Define a stack pointer

	push af
		call cls					;Clear Screen
		call SetPaletteBlack		;Hide while drawing

		ld a,(Depth3D)
		or a
		jr z,ShowTitle2D

		ld a,(PaletteNum)		;3D palette
		jr ShowTitle

ShowTitle2D:
		ld a,4					;2D palette (One eye only)
ShowTitle:
		push af	
			ld d,3
			ld hl,0				;0=Title Screen
			call DoFarCall		;Call ChibiWave after bankswitching
	
			call DoPalFadeInQ	;Fade in
			call DoSet3D		
		pop af
		call SetPaletteB		;Set pallette A

		ld a,0
		ld hl,&1C17
		call LocateXYZ			;3D Locate (for hiscore)
		
		ld de,HiScore		
		ld b,4
		call BCD_Show			;Show Digits of score

		ld de,&1C16
		ld hl,txtHiScore
		call LocateandPrint		;Show Text
		
		call PrintKeyMode		;Show Key/Mouse state
	pop af
	ifdef Enable_Speech
		or a
		jr z,NoSpeech				;Only play speech after intro

		ld bc,1200
		call PauseABC				;Title screen delay

		ld d,3
		ld hl,&8006					;6=Sample
		call DoHighCall				;Play speech "Operation Suck"
			
		ld bc,500
		call PauseABC				;After speech delay
	endif
	
NoSpeech:
	ld hl,SongTitle
	call Arkos_ChangeSong			;Title screen song
	doei
	
	ld bc,4000						;Delay until reshow intro
	
TitleWait:
	call ReadMouse
	halt
	
	ifdef Enable_Intro
		dec bc
		ld a,b
		or c
		jp z,ShowIntroAgain		;Reshow intro?
	endif

	push bc
		ld h,%10111111
		call ReadKeyRow			;Toggle Mouse/JoyKey
		bit 2,h
		jr nz,NoKeyChange
		call PrintKeyMode
		ld a,(KeyMode)
		inc a
		cp 2
		jr c,KeyModeOK			;K= Toggle
		xor a
KeyModeOK:
		ld (KeyMode),a
		call PrintKeyMode		;Show new key mode
		call WaitForKeyRelease
		halt
NoKeyChange:	
	
		ld h,%11111110
		call ReadKeyRow			;C
		bit 3,h
		call z,ChangePalette	;Toggle 3D colors 

		
		ld h,%11110111			;3
		call ReadKeyRow
		bit 2,h
		jr nz,No3DChangeTitle	;Switch 3D On/Off

		ld a,(Depth3D)
		inc a
		and %00000001
		ld (Depth3D),a
	pop bc
	xor a
	jp DrawTitleScreen			

No3DChangeTitle:
		ld h,%10111111
		call ReadKeyRow			;H 
		ld a,128
		bit 4,h
		jr z,StartGameHard		;Hard mode (level 128)

		ei
	pop bc
	ld a,(FireDown)				;Fire=Start
	or a
	jr z,TitleWait	

		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

;		New Game Begins

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

;StartGame
	xor a
StartGameHard:
	ld (LevelNum),a						

	ld hl,SongGame						;Game Music
	call Arkos_ChangeSong
	doei

	ld hl,NewGameData					;Init The game settings (Cursor)
	ld de,OldPosX
	ld bc,NewGameData_End-NewGameData
	ldir

StartNewLevel:
	call InitLevel						;Set up the new level
	call RePaintScreen
	call DoPalFadeInQ
	
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

;		Game Loop

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	


ShowAgain:	
	push bc
		call ReadMouse			;Read Input
	pop bc
	
	call UpdateSoundSequence

	ld h,%11011111
	call ReadKeyRow				;Check for P
	bit 0,h
	jr nz,NoPause
	
	call PLY_Stop				;Paused: Stop music
	di
	ld bc,500
	call PauseABC				;Delay
	
DoPause:
	ld h,%11011111
	call ReadKeyRow				;Check for P
	bit 0,h
	jr nz,DoPause

	ld bc,100
	call PauseABC	
NoPause:

	ld h,%01111111					;Check for Space
	call ReadKeyRow
	bit 0,h
	call z,ReloadMagazine			;Reload

	ld h,%11111101
	call ReadKeyRow					;Check for S
	bit 1,h
	jr nz,NoSpeedToggle
	push hl
		ld hl,CursorMoveSpeedNum
		inc (hl)
		ld a,(hl)	
		and %00000011				;Inc and get speed
		sla a

		ld hl,CursorMoveSpeeds
		add l
		ld l,a

		ld c,(hl)					;Get two bytes from speed
		inc hl
		ld b,(hl)
		ld (CursorMoveSpeed),bc
		call WaitForKeyRelease
	pop hl

NoSpeedToggle:
	ld h,%11111110
	call ReadKeyRow
	bit 3,h
	call z,ChangePalette			;Switch Palette on C

	ld h,%11110111
	call ReadKeyRow
	bit 2,h
	jr nz,No3DChange
	push hl
		call cls
		call Change3D				;Toggle 3D on 3
		call RePaintScreen
	pop hl
	call WaitForKeyRelease
No3DChange:
	ld hl,(Tick)
	inc hl							;Update 16 bit tick
	ld (tick),hl

	ld b,SpriteDataCount 
	ld iy,SpriteArray
DrawNextSprite:
	push bc
		push iy
			call AnimateSpriteIY	;Update Sprites
		pop iy
		ld bc,SpriteDataSize
		add iy,bc					;Move to next sprite

	pop bc
	ld a,b
	and %00000111
	jr nz,NoMouseUp					;Don't update mouse too often
	push bc
		call ReadMouse				;Read mouse
		call UpdateCursor			;Move cursor
	pop bc
NoMouseUp:
	djnz DrawNextSprite
	
notick:
	ld hl,(Tick)
	ld a,l
	and %00001111
	jr nz,MoFireHeld			;FireHeld=Minigun function
	
	ld a,(FireHeld)
	or a
	jr z,MoFireHeld				;Fire Held (No Drag)

	call GetGun
	ld a,(IX+G_Mode)
	cp 3
	call z, FireBullets 		;3=Minigun
MoFireHeld:

	ld a,(FireDown2)
	or a
	jr z,MoFire2				;Fire 2 pressed?
	
	call ReloadMagazine			;Reload

	xor a
	ld (FireDown2),a			;Clear Fire2
MoFire2:
	ld a,(FireDown)
	or a
	jr z,MoFire

	xor a
	ld (FireDown),a				;Clear Fire 1

	call FireBullet				;Fire a bullet
MoFire:
	call UpdateCursor			;Move the cursor if needed

	jp ShowAgain
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

;		Intro graphics (OpWolf Spoof)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

Intro1:
  db 0,0,0,0,0,1,1,2,0,0,0,0,3,4,3,5
  db 0,0,0,0,0,6,7,8,9,0,0,0,10,11,12,13
  db 14,0,0,0,0,15,16,17,9,0,0,0,18,19,20,21
  db 3,22,0,0,0,23,24,15,25,0,0,0,26,27,28,9
  db 29,30,31,6,32,33,34,35,6,0,36,37,23,38,39,9
  db 10,40,41,30,42,43,44,45,46,47,48,49,9,6,50,16
  db 51,48,52,53,54,55,56,57,58,59,60,61,9,21,62,27
  db 0,63,64,65,66,67,68,69,70,71,72,73,23,24,24,24
  db 0,0,0,74,75,76,77,7,35,78,0,0,79,80,24,38
  db 0,0,0,0,0,23,81,82,83,84,0,0,85,86,87,88
  db 0,0,0,0,6,22,89,90,39,91,0,0,92,93,94,95
  db 0,0,0,96,24,24,24,38,25,9,0,0,23,97,98,99
Intro1b:
  db 0,0,0,0,0,1,1,2,0,0,0,0,3,4,3,5
  db 0,0,0,0,0,6,7,8,9,0,0,0,10,11,12,13
  db 0,0,0,0,0,15,16,17,9,0,0,0,18,19,20,21
  db 0,0,0,0,0,23,24,15,25,0,0,0,26,27,28,9
  db 31,6,32,33,0,0,27,35,6,0,0,0,255,126,255,24,39,255,127
  db 41,48,42,43,255,128,0,75,45,46,0,0,47,48,49,77,255,129
  db 52,53,54,55,56,255,130,255,131,57,58,255,132,255,133,59,60,61,255,134,255,135
  db 64,65,66,67,68,0,255,136,69,35,0,255,137,71,72,255,138,24,24
  db 0,74,75,76,0,0,77,7,35,255,139,0,78,79,80,24,38
  db 0,0,0,0,0,23,81,82,83,84,0,0,85,86,87,88
  db 0,0,0,0,6,22,89,90,39,91,0,0,92,93,94,95
  db 0,0,0,96,24,24,24,38,25,9,0,0,23,97,98,99

Intro2:
  db 0,100,101,102,0,103,2,104,0,104,0,105,0,0,0,105
  db 0,0,0,100,106,0,0,74,0,104,0,107,0,0,0,107
  db 0,0,0,108,109,101,0,0,12,110,111,112,0,0,0,113
  db 0,0,114,115,53,48,116,0,117,118,119,120,0,0,0,107
  db 0,0,121,53,48,122,10,48,48,48,123,124,125,126,0,113
  db 0,127,128,129,130,131,132,133,134,48,135,136,137,138,139,107
  db 140,141,142,78,143,144,24,145,146,147,48,48,48,148,149,113
  db 0,140,150,0,9,0,103,102,151,152,153,154,155,156,157,158
  db 0,159,160,0,161,102,0,0,151,162,163,164,165,166,167,168
  db 0,169,0,0,0,0,103,102,170,162,171,169,172,173,169,150
  db 0,174,0,0,0,175,0,0,151,176,177,169,178,179,169,160
  db 0,0,0,0,0,180,0,0,181,182,183,169,184,139,174,185
Intro2b:
  db 101,255,86,0,102,0,103,2,104,0,104,0,105,0,0,0,105
  db 0,100,255,87,2,255,88,0,0,74,0,104,0,107,0,0,0,107
  db 0,108,109,255,89,255,0,0,0,0,12,110,111,112,0,0,0,113
  db 114,115,255,90,48,116,0,0,0,117,118,119,120,0,0,0,107
  db 121,255,91,48,122,10,48,48,48,48,48,123,124,125,126,0,113
  db 128,129,130,131,132,133,133,133,134,48,135,136,137,138,139,107
  db 142,78,255,92,144,24,145,8,46,255,93,147,48,48,48,148,149,113
  db 150,255,94,0,0,9,0,103,102,151,152,153,154,155,156,157,158
  db 160,159,0,0,161,102,0,0,151,162,163,164,165,166,167,168
  db 0,169,0,0,0,0,103,102,170,162,171,169,172,173,169,150
  db 0,174,0,0,0,175,0,0,151,176,177,169,178,179,169,160
  db 0,0,0,0,0,180,0,0,181,182,183,169,184,139,174,185

Intro3:
  db 0,0,0,186,9,0,0,187,7,188,189,48,48,190,191,192
  db 0,0,193,0,0,0,0,30,139,0,194,195,24,196,197,198
  db 0,0,199,200,0,0,186,0,201,0,202,203,204,205,0,206
  db 0,207,191,208,139,209,0,0,0,202,210,211,212,213,126,0
  db 0,214,0,215,208,139,0,0,202,216,217,117,218,219,220,126
  db 0,221,0,0,192,222,223,202,224,225,226,151,227,147,228,229
  db 230,0,0,0,0,231,232,224,0,169,233,234,235,236,139,0
  db 237,0,0,238,239,0,0,0,0,169,231,240,0,241,242,0
  db 9,0,243,244,214,245,0,0,0,169,246,247,248,0,249,0
  db 0,0,250,251,252,253,254,254,0,113,246,0,0,255,0,241,139
  db 0,0,0,255,1,255,2,255,3,255,4,255,5,0,255,6,255,7,255,8,247,0,255,9,242
  db 0,0,0,96,255,10,255,11,255,12,255,13,96,246,0,248,0,255,8,0,255,14
Intro3b:
  db 0,0,0,186,9,0,0,187,7,188,189,48,48,190,191,192
  db 0,0,193,0,0,0,0,30,139,0,194,195,24,196,197,198
  db 0,0,199,200,0,0,186,0,201,0,202,203,204,205,0,206
  db 0,207,191,208,139,209,0,0,0,202,210,211,212,213,126,0
  db 0,214,0,215,208,139,0,0,202,216,217,117,218,219,220,126
  db 0,221,0,0,192,222,223,202,224,225,226,151,227,147,228,229
  db 230,0,0,0,0,255,95,232,224,0,169,233,234,235,236,139,0
  db 237,0,0,255,96,255,97,253,254,254,0,169,231,240,0,241,242,0
  db 9,0,243,255,98,255,2,255,3,255,4,255,5,0,169,246,247,248,0,249,0
  db 0,0,250,255,99,255,100,255,11,255,12,255,13,0,113,246,0,0,255,0,241,139
  db 0,0,0,96,255,101,48,255,102,255,103,0,255,6,255,7,255,8,247,0,255,9,242
  db 0,0,0,0,255,104,255,105,48,255,106,255,107,246,0,248,0,255,8,0,255,14
Intro4:
  db 255,15,255,16,101,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 255,17,255,18,255,19,255,20,110,37,101,101,0,0,0,255,21,255,22,255,22,255,22,255,19
  db 255,23,255,24,255,25,255,26,255,27,255,28,255,19,255,29,255,30,255,31,255,32,255,33,255,34,0,0,255,35
  db 255,18,255,19,255,19,255,30,30,255,23,255,24,255,36,255,37,152,255,19,48,30,30,255,15,37
  db 255,38,255,39,255,40,255,27,255,17,255,18,255,19,255,41,255,37,255,37,255,37,255,19,255,37,255,37,147,48
  db 134,255,42,255,43,255,44,255,45,255,46,255,46,255,47,255,37,255,48,255,39,255,3,255,37,255,49,255,50,255,51
  db 0,255,52,255,53,255,54,255,55,255,42,255,56,255,57,255,37,255,48,255,39,255,58,255,59,255,60,101,0
  db 0,0,0,0,0,255,61,255,62,255,37,255,48,255,38,255,63,255,64,255,65,255,66,255,23,255,67
  db 0,0,0,0,0,0,0,255,68,134,255,42,255,43,255,69,255,23,255,70,255,71,48
  db 0,0,0,0,0,0,96,24,255,24,24,255,68,133,255,72,255,73,48,255,74
  db 0,0,0,0,0,0,0,255,75,255,76,255,77,255,78,24,255,79,48,48,255,80
  db 0,0,0,0,0,0,0,0,0,255,81,255,82,255,83,255,84,255,23,255,85,133
Intro4b:
  db 255,15,255,16,101,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 255,17,255,18,255,19,255,20,110,37,101,101,0,0,0,255,21,255,22,255,22,255,22,255,19
  db 255,23,255,24,255,25,255,26,255,27,255,28,255,19,255,29,255,30,255,31,255,32,255,33,255,34,0,0,255,35
  db 255,18,255,19,255,19,255,30,30,255,23,255,24,255,36,255,37,152,255,19,48,30,30,255,15,37
  db 255,38,255,39,255,40,255,27,255,17,255,18,255,19,255,41,255,37,255,37,255,37,255,19,255,37,255,37,147,48
  db 134,255,42,255,43,255,44,255,45,255,46,255,46,255,47,255,37,255,48,255,39,255,3,255,37,255,108,255,109,255,74
  db 0,255,52,255,53,255,54,255,55,255,42,255,56,255,57,255,37,255,48,255,39,255,58,255,59,255,110,255,111,255,112
  db 0,0,0,0,0,255,61,255,62,255,37,255,48,255,38,255,63,255,64,255,113,255,114,255,73,48
  db 0,0,0,0,0,0,0,255,68,134,255,42,255,43,255,37,255,115,255,116,48,48
  db 0,0,0,0,0,0,96,24,255,24,24,255,68,133,255,117,255,118,255,119,255,120
  db 0,0,0,0,0,0,0,255,75,255,76,255,77,255,78,24,24,255,121,255,122,255,123
  db 0,0,0,0,0,0,0,0,0,255,81,255,82,255,83,75,255,124,255,68,255,125

 IntroGraphics:	
	incbin "\ResALL\SuckHunt\SuckHuntIntroZXN.RAW"

	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;		Intro Sequence

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


IntroAnimList:	;Xpos,Ypos,Palette,Frame1,Frame2
	db 4+0,12,1
		dw Intro1
		dw Intro1b
	db 4+32-16,0,0
		dw Intro3
		dw Intro3b
	db 4+32-16,12,0
		dw Intro2
		dw Intro2b
	db 4+0,0,1
		dw Intro4
		dw Intro4b

	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;		Key handler (Simulates mouse)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	
ReadKeyRow:
	push bc
		ld b,h			;Read row h from keyboard
		ld c,&FE
		in h,(c)		;Return keys in H
	pop bc
	ret
	
ReadKey:

	
	ld h,%11101111
	call ReadKeyRow		;Read numbers (Joystick numbers)
	bit 0,h
	jr nz,keyNotFire
	ld a,(FireHeld)
	or a
	jr nz,keyNotFireB
	ld a,1
	ld (FireDown),a		;Set FireDown/Held
	ld (FireHeld),a
	jr keyNotFireB
keyNotFire:
	xor a		
	ld (FireHeld),a		;Release Fire Held
keyNotFireB:
	ld l,255
	doei
		
	ld a,(SkipReadTick)	;Correction to make Joy cursor 
	inc a					;match mouse speed
	and %00000111
	ld (SkipReadTick),a
	ret nz	
		
	bit 3,h
	jr nz,KeyNotL		;Get directions
	res 0,l
KeyNotL:	
	bit 4,h
	jr nz,KeyNotR
	res 1,l
KeyNotR:	
	bit 1,h
	jr nz,KeyNotU
	res 3,l
KeyNotU:
	bit 2,h
	jr nz,KeyNotD
	res 2,l
KeyNotD:
	ld h,l
	ld de,0
	
	ld a,(CursorMoveSpeed)	;Convert to a mouse type movement
	bit 2,h				;---FUDLR
	jr nz,JrNotDown
	ld e,a
JrNotDown:	
	bit 3,h
	jr nz,JrNotUp
	neg					;Flip direction
	ld e,a
JrNotUp:	
	ld a,(CursorMoveSpeed)
	bit 0,h
	jr nz,JrNotRight
	ld d,a
JrNotRight:	
	bit 1,h
	jr nz,JrNotLeft
	neg					;Flip Directions
	ld d,a
JrNotLeft:	
	
	jp DoFakeMouse
	
SkipReadTick: db 0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;	Mouse Handler 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ReadMouse:	
	dodi
	
	ld a,(KeyMode)
	cp 0
	jr z,ReadKey
	
	ld bc,&FFFE
	in a,(c)
	in a,(c)
	in a,(c)
	push af				;Buttons
		call DoAxis		;Xaxis
		ex de,hl
		call DoAxis		;Yaxis	
	pop af
	doei
	push af
	
		   ;%-----RML
		and %00000001
		jr nz,JoyNotFire
		ld a,(FireHeld)		;Left Mouse Pressed
		or a
		jr nz,JoyNotFireB
		ld a,1
		ld (FireDown),a		;Set FireDown/Held
		ld (FireHeld),a
		jr JoyNotFireB
JoyNotFire:
		xor a		
		ld (FireHeld),a		;Release Fire Held
JoyNotFireB:
	pop af
	
	   ;%-----RML
	and %00000100
	jr nz,JoyNotFire2
	ld a,1
	ld (FireDown2),a		;Right mouse pressed
JoyNotFire2:

	ld d,e
	ld a,l
	neg						;Flip Y Axis
	ld e,a
DoFakeMouse:		
	ld bc,0
	ld b,d
	ld hl,(NewPosX)
	
	ld a,d				;D=Xpos
	or a
	jp m,MouseLeft
	
;MouseRight
	add hl,bc
	jr c,MouseUPDOWN
	ld a,h
	cp 128-3			;Over right of screen?
	jr nc,MouseUPDOWN
	
	ld (NewPosX),hl
	jr MouseUPDOWN
	
MouseLeft:	
	add hl,bc
	jr nc,MouseUPDOWN	;Over left of screen?
	ld (NewPosX),hl
		
MouseUPDOWN:			
	ld b,e
	ld hl,(NewPosY)
	
	ld a,e				;E=Ypos
	or a
	jp m,MouseUp
	
;MouseDown
	add hl,bc
	ret c
	ld a,h
	cp 192-7			;At bottom of screen?
	ret nc

	ld (NewPosY),hl
	ret
	
MouseUp:	
	add hl,bc	
	ret nc				;Over top of screen?
	ld (NewPosY),hl
	ret

DoAxis:
		in a,(c)
		and %00001111	;Top 8-12 bits
		ld d,a
		in a,(c)
		and %00001111	;Middle 4-7 bits
		rlca
		rlca
		rlca
		rlca
		ld e,a
		in a,(c)
		and %00001111	;Bottom 0-3 bits
		or e
		ld e,a
	ret
	
	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Graphics Routines

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

S_AddressL equ 0
S_AddressH equ 1
S_Wid equ 2
S_Hei equ 3

PixelsPerByte equ 4


	read "SAM_Sprites.asm"	;Sam Sprite Code
	
;TitleTiles
;ShowSpriteL2DMode			;Dummy for selfmodifying code in the common section
;	nop
;	ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;		Keytoggle

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	

KeyText: db "Joy",255		;Text for keytoggle on title
	db "Mou",255	
	
KeyMode:
	db 0
PrintKeyMode:
	ld hl,KeyText
	ld b,0
	ld a,(KeyMode)		;Get keymode
	rlca
	rlca
	ld c,a
	add hl,bc			;String address
	
	ld de,&1417
	call LocateandPrint	;Show onscreen
	ret
	
	
WaitForKeyRelease:
	ld b,%11111110
WaitForKeyReleaseB:
	ld c,&FE
	in a,(c)
	or %11100000
	inc a
	jr nz,WaitForKeyRelease
	sll b
	jr c,WaitForKeyReleaseB
	ret	
	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;		Music

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	
SongGame:
	incbin "\ResAll\SuckHunt\GameSongF800.bin"
SongTitle:
	incbin "\ResAll\SuckHunt\TitleSongF800.bin"
SongIntro:
	incbin "\ResAll\SuckHunt\IntroSongF800.bin"

Arkos_ChangeSong:
	ifdef Akuyou_MusicPos
		push hl
			call PLY_Stop		;Stop old music
			di
		pop hl

		ld de,Akuyou_MusicPos	;We always play the song from a single address
		ld bc,&400
		ldir
		
		call PLY_Init			;Start the music!
	endif
	
	ret


	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

	
ChangePalette:
	ld a,(Depth3D)
	or a
	jr z,NoPaletteChange	;3D Off?
	
	ld a,(PaletteNum)
	cp 3
	jr c,PalOk
	xor a
PalOk
	inc a					;Next palette
	push hl
		call SetPalette		;Set it
	pop hl
	call WaitForKeyRelease
NoPaletteChange:
	ret

	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
;		3D toggles	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
Set2D3Dmode:
	ld a,(Depth3D)
	or a
	jr z,Set2DMode
	jr Set3DMode
DoSet3D:
	ld a,(Bak3d)
	ld (ShowSpriteR),a				;Reset normal operation of sprites
	ret
	
Change3D:
	ld a,(Depth3D)
	inc a
	and %00000011
	ld (Depth3D),a					;Toggle stereo depth
	jr z,Set2DMode
	
Set3DMode:							;Turn 3D on
	call DoSet3D
	ld a,(PaletteNum)
	and %00000011
	jr nz,Set3DMode_PaletteOK
	inc a
Set3DMode_PaletteOK:
	jr SetPaletteB					;Set the 3D palette

Set2DMode:
	ld a,&C9		;ret			;Selfmod sprites to 2D mode
	ld (ShowSpriteR),a
	xor a
	jr SetPaletteB					;Set palette

	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
;		Palette functions
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
	
SetPalette:
	ld (PaletteNum),a

SetPaletteB:
	rlca				;16 bytes per palette
	rlca
	rlca
	rlca
	ld hl,Palette

	ld b,0				;BC=palette address
	ld c,a
	add hl,bc
SetPaletteAlt16:
	ld d,16
	ld b,0				;Count
SetPaletteAltB:
	ld c,248
paletteloop:
	ld a,(hl)			;get color (RRRGGGBB)
	inc hl
    out (c),a			;Send color
	inc b
	dec d
    jr nz,paletteloop   ;Repeat for next color
	ret
	
SetPaletteAlt4:			;4 color palette
	ld b,0
	ld c,248
SetPaletteAlt4b:
	ld d,4
	jr paletteloop		

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PaletteOutPink:		;Fade in / out palettes
	db %00000000,%00000001,%00001011,%00111000
	db %00000000,%00000000,%00000001,%00001011
	db %00000000,%00000000,%00000000,%00000001
	db %00000000,%00000000,%00000000,%00000000
PaletteOutBlue:	
	db %00000000,%00000001,%00001001,%00011001
	db %00000000,%00000000,%00000001,%00001001
	db %00000000,%00000000,%00000000,%00000001
	db %00000000,%00000000,%00000000,%00000000
	
PaletteInBlue:	
	db %00000000,%00000000,%00000000,%00000001
    db %00000000,%00000000,%00000001,%00001001
	db %00000000,%00000001,%00001001,%00011001
	db %00000000,%00001001,%00011001,%00011111
PaletteInPink:
    db %00000000,%00000000,%00000000,%00000001
	db %00000000,%00000000,%00000001,%00001011
	db %00000000,%00000001,%00001011,%00111000
	db %00000000,%00001011,%00111000,%00111111
	
SetPaletteBlack:
	ld hl,PaletteBlack		;Set all colors black
	ld b,1
	jr PalFadeB
	
DoPalFadeOutQ:
	xor a			;a=0
	ld (FireDown),a			;Clear fire
	inc a			;a=1
	call DoPalFadeOut
	jp cls					;Clear screen
		
DoPalFadeInQ:
	xor a
	call DoPalFadeIn		;Fade in
	jp Set2D3Dmode			;Re-enable 3d palette

DoPalFadeIn:
	ld de,PaletteInPink		;2 possible fade palettes
	ld hl,PaletteInBlue
	jr PalFade

DoPalFadeOut:
	ld de,PaletteOutPink	;2 possible fade palettes
	ld hl,PaletteOutBlue
PalFade:
	or a
	jr nz,PalFadeBluePal
	ex de,hl				;Select Blue/Pink palette (0=Pink)
PalFadeBluePal:
	ld b,4						;4 stages of fade
PalFadeB:
	push bc
		push hl
		call SetPaletteAlt4		;Fade in 4 colors
		pop hl
		push hl
		call SetPaletteAlt4b	;Set other 12 to black
		pop hl
		push hl
		call SetPaletteAlt4b
		pop hl
		call SetPaletteAlt4b
		push hl
			ld bc,50
			call PauseABC	
		pop hl
	pop bc
	djnz PalFadeB
	ret

	


	
Palette:							;Grey 4color
	   ;-GRBLGRB
    db %00000000 ;0
    db %00000111 ;1
    db %01110000 ;2	
    db %01111111 ;3

PaletteBlack:						;Blackout palette
	db 0,0,0,0,0,0,0,0,0,0,0,0
	
PaletteRC:							;Red Cyan 3D Glasses
	db %00000000; ;0  --GRBLGRB
	db %00000010; ;1  --GRBLGRB
	db %00100000; ;2  --GRBLGRB
	db %00100010; ;3  --GRBLGRB
	
	db %00000101; ;0  --GRBLGRB
	db %00000111; ;1  --GRBLGRB
	db %00100101; ;2  --GRBLGRB
	db %00100111; ;3  --GRBLGRB
	
	db %01010000; ;0  --GRBLGRB
	db %01010010; ;1  --GRBLGRB
	db %01110000; ;2  --GRBLGRB
	db %01110010; ;3  --GRBLGRB
	
	db %01011101; ;0  --GRBLGRB
	db %01011111; ;1  --GRBLGRB
	db %01111101; ;2  --GRBLGRB
	db %01111111; ;3  --GRBLGRB
	
PaletteMG:							;Magenta Green 3D Glasses
	db %00000000; ;0  --GRBLGRB
	db %00000100; ;1  --GRBLGRB
	db %01000000; ;2  --GRBLGRB
	db %01000100; ;3  --GRBLGRB
	
	db %00000011; ;0  --GRBLGRB
	db %00000111; ;1  --GRBLGRB
	db %01000011; ;2  --GRBLGRB
	db %01000111; ;3  --GRBLGRB
	
	db %00110000; ;0  --GRBLGRB
	db %00110100; ;1  --GRBLGRB
	db %01110000; ;2  --GRBLGRB
	db %01110100; ;3  --GRBLGRB
	
	db %00111011; ;0  --GRBLGRB
	db %00111111; ;1  --GRBLGRB
	db %01111011; ;2  --GRBLGRB
	db %01111111; ;3  --GRBLGRB
	
PaletteBY:							;Blue Yellow 3D Glasses
	db %00000000; ;0  --GRBLGRB
	db %00000110; ;1  --GRBLGRB
	db %01100000; ;2  --GRBLGRB
	db %01100110; ;3  --GRBLGRB
	
	db %00000001; ;0  --GRBLGRB
	db %00000111; ;1  --GRBLGRB
	db %01100001; ;2  --GRBLGRB
	db %01100111; ;3  --GRBLGRB
	
	db %00010000; ;0  --GRBLGRB
	db %00010110; ;1  --GRBLGRB
	db %01110000; ;2  --GRBLGRB
	db %01110110; ;3  --GRBLGRB
	
	db %00011001; ;0  --GRBLGRB
	db %00011111; ;1  --GRBLGRB
	db %01111001; ;2  --GRBLGRB
	db %01111111; ;3  --GRBLGRB
	
	
Palette2D:							;Grey - Single eye view
	db %00000000; ;0  --GRBLGRB
	db %00000000; ;0  --GRBLGRB
	db %00000000; ;0  --GRBLGRB
	db %00000000; ;0  --GRBLGRB
	
    db %00000111 ;1
    db %00000111 ;1
	db %00000111 ;1
	db %00000111 ;1
	
    db %01110000 ;2		;db %10010010 ;2
	db %01110000 ;2		;db %10010010 ;2
	db %01110000 ;2		;db %10010010 ;2
	db %01110000 ;2		;db %10010010 ;2
	
    db %01111111 ;3
	db %01111111 ;3
	db %01111111 ;3
	db %01111111 ;3
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
;		Interrupt Handler
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
		
InterruptHandler:

	ifdef Akuyou_MusicPos
;		exx
		;ex af,af'
		push af
		push bc
		push de
		push hl
		push ix
		push iy
;		exx
			call PLY_Play			;Play music
;		exx
		pop iy
		pop ix
		pop hl
		pop de
		pop bc
		pop af
		;ex af,af'
;		exx
		
	endif
	ei
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	
	
;Exported by Akusprite Editor
SpriteInfo:
    dw &0000/PixelsPerByte + SpriteBase           ;SpriteAddr 0
     db 32,32                                     ;XY 
    dw &0400/PixelsPerByte + SpriteBase           ;SpriteAddr 1
     db 24,16                                     ;XY 
    dw &0580/PixelsPerByte + SpriteBase           ;SpriteAddr 2
     db 24,16                                     ;XY 
    dw &0700/PixelsPerByte + SpriteBase           ;SpriteAddr 3
     db 24,16                                     ;XY 
    dw &0880/PixelsPerByte + SpriteBase           ;SpriteAddr 4
     db 24,16                                     ;XY 
    dw &0A00/PixelsPerByte + SpriteBase           ;SpriteAddr 5
     db 16,16                                     ;XY 
    dw &0B00/PixelsPerByte + SpriteBase           ;SpriteAddr 6
     db 40,48                                     ;XY 
    dw &1280/PixelsPerByte + SpriteBase           ;SpriteAddr 7
     db 40,48                                     ;XY 
    dw &1A00/PixelsPerByte + SpriteBase           ;SpriteAddr 8
     db 32,32                                     ;XY 
    dw &1E00/PixelsPerByte + SpriteBase           ;SpriteAddr 9
     db 32,32                                     ;XY 
    dw &2200/PixelsPerByte + SpriteBase           ;SpriteAddr 10
     db 24,24                                     ;XY 
    dw &2440/PixelsPerByte + SpriteBase           ;SpriteAddr 11
     db 24,24                                     ;XY 
    dw &2680/PixelsPerByte + SpriteBase           ;SpriteAddr 12
     db 48,32                                     ;XY 
    dw &2C80/PixelsPerByte + SpriteBase           ;SpriteAddr 13
     db 48,32                                     ;XY 
    dw &3280/PixelsPerByte + SpriteBase           ;SpriteAddr 14
     db 32,24                                     ;XY 
    dw &3580/PixelsPerByte + SpriteBase           ;SpriteAddr 15
     db 32,16                                     ;XY 
    dw &3780/PixelsPerByte + SpriteBase           ;SpriteAddr 16
     db 24,16                                     ;XY 
    dw &3900/PixelsPerByte + SpriteBase           ;SpriteAddr 17
     db 24,16                                     ;XY 
    dw &3A80/PixelsPerByte + SpriteBase           ;SpriteAddr 18
     db 32,24                                     ;XY 
    dw &3D80/PixelsPerByte + SpriteBase           ;SpriteAddr 19
     db 32,32                                     ;XY 
    dw &4180/PixelsPerByte + SpriteBase           ;SpriteAddr 20
     db 24,16                                     ;XY 
    dw &4300/PixelsPerByte + SpriteBase           ;SpriteAddr 21
     db 24,24                                     ;XY 
    dw &4540/PixelsPerByte + SpriteBase           ;SpriteAddr 22
     db 16,16                                     ;XY 
    dw &4640/PixelsPerByte + SpriteBase           ;SpriteAddr 23
     db 16,16                                     ;XY 
    dw &4740/PixelsPerByte + SpriteBase           ;SpriteAddr 24
     db 40,61                                     ;XY 
    dw &50C8/PixelsPerByte + SpriteBase           ;SpriteAddr 25
     db 40,61                                     ;XY 
    dw &5A50/PixelsPerByte + SpriteBase           ;SpriteAddr 26
     db 40,39                                     ;XY 
    dw &6068/PixelsPerByte + SpriteBase           ;SpriteAddr 27
     db 40,39                                     ;XY 
    dw &6680/PixelsPerByte + SpriteBase           ;SpriteAddr 28
     db 24,6                                      ;XY 
    dw &6710/PixelsPerByte + SpriteBase           ;SpriteAddr 29
     db 24,16                                     ;XY 
    dw &6890/PixelsPerByte + SpriteBase           ;SpriteAddr 30
     db 48,16                                     ;XY 
    dw &6B90/PixelsPerByte + SpriteBase           ;SpriteAddr 31
     db 32,16                                     ;XY 

SpriteBase equ SpriteData

SpriteData:
	incbin "\ResALL\SuckHunt\SuckHuntZXN.RAW"		;4 color sprites

FontData:				;Bitmap Font (4 color)
	ds 16	;Space char
	incbin "\ResALL\SuckHunt\FontZXN.RAW"			;4 color font


SprCursorSprite:
	incbin "\ResALL\SuckHunt\SuckHuntZXN_Cursors.RAW"	;4 color Cursors
	
	;incbin "\ResALL\SuckHunt\CPCTitleTiles.raw"
	read "\SrcALL\Multiplatform_BCD.asm"				;Show Binary Coded Decimal
	read "\SrcALL\MultiPlatform_ShowDecimal.asm"		;Show decimal number

	read "SH_Multiplatform.asm"
	read "SH_DataDefs.asm"
	read "SH_RamDefs.asm"
	read "SH_LevelInit.asm"
	
	read "\SrcALL\Multiplatform_ChibiSound.asm"			;Sound Driver


	ifdef Akuyou_MusicPos
		read "\SrcALL\Multiplatform_ArkosTrackerLite.asm"
	else
	
Ply_Stop:
	ret
	endif	
	
	
;*************************************************************************************

;	Interbank calls

;*************************************************************************************

FarCall:
	in a,(250)
	push af
		ld a,d			;Page in bank D to low bank
		or %00100001 
		out (250),a
		call JumpHL+DoFarCall-FarCall	;Call HL
	pop af
	out (250),a			;Restore bank
	ret
	
JumpHL:					;Jump to address HL (used like CALL HL)
	jp (hl)
	
DoHighCall:				;Page in second bank at &8000 (over VRAM)
	di
	in a,(251)
	ld e,a
		ld a,d			;Page in bank D to high bank
		or %00100000 
		out (251),a
	push de
		call JumpHL		;Call HL
	pop de
	ld a,e
	out (251),a			;Restore High bank
	ret

	
;*************************************************************************************
;*************************************************************************************

;	End of 1st Bank (&0000-7FFF)

;*************************************************************************************
;*************************************************************************************
	
	include "SAM_SuckHunt_Bank2.asm"
	
	