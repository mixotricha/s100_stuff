
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; 		Data Defs

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	ifdef vasm
		align 4
	else
		align 16
	endif
;Random number Lookup tables
Randoms1:
	db &0A,&9F,&F0,&1B,&69,&3D,&E8,&52,&C6,&41,&B7,&74,&23,&AC,&8E,&D5
Randoms2:
	db &9C,&EE,&B5,&CA,&AF,&F0,&DB,&69,&3D,&58,&22,&06,&41,&17,&74,&83



CursorMoveSpeeds: 				;These are used as 'toggles' for the mouse 
	ifdef BuildCPC				;Emulation move speed (4 total)
		dw 128*2
		dw 106*2
		dw 80*2
		dw 64*2
	endif
	ifndef BuildCPC
		dw 4
		dw 3
		dw 2
		dw 1
	endif
	
	
	
RandPerspective:
	db 80,%00000111,4,0		;These are used for random Vertical positions
	db 70,%00000011,8,0		;Z depth,MoveSpeed,Ypos,Unused
	db 60,%00000001,12,0
	db 50,%00000000,16,0
	db 00,%00000111,&3C,0
	db -10,%00000011,&42,0
	db -20,%00000001,&48,0
	db -30,%00000000,&50,0
;	db -40,%00000000,&58,0



;Sound sequences Delay,ChibiSound Byte

SndEnergy:
	db 8,%01000111
	db 8,%01000100
	db 8,%01000010
	db 0,0

SndSuckedFraction:
	db 8,%01000000
	db 0,0
SndSucked:
	db 8,%01000000
	db 8,%01000001
	db 0,0

SndFire:
	db 32,%11011111
	db 0,0
SndFireHit:
	db 32,%01011111
	db 0,0

SndFireHitDead:
	db 8,%00111111
	db 8,%01111111
	db 8,%11111111
	db 0,0

SndNewLevel:
	db 32,%00000111
	db 32,%00001111
	db 32,%00011111
	db 32,%01111111
	db 32,%00111111
	db 32,%00011111
	db 32,%00001111
	db 32,%00000111
	db 0,0

SndGameover:
	db 32,%00000001
	db 32,%00000011
	db 32,%00000111
	db 32,%00001111
	db 32,%00011111
	db 32,%00111111
	db 32,%01111111
	db 32,%00111111
	db 0,0

SndGunSwap:
	db 32,%11011000
	db 32,%11001111
	db 32,%11000111
	db 0,0

SndReload:
	db 32,%11000111
	db 32,0
	db 32,%11000111
SndSilence:
	db 0,0

SndSpawn:
	db 4,%01000111
	db 4,%01000100
	db 4,%01000010
	db 4,%01000001
	db 0,0


	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; 		Vars

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

A_Bullets equ 0			;Player Ammo count
A_Magazine equ 1

G_NameL equ 0		;Byte positions for Gun Settings
G_NameH equ 1
G_Magazine equ 2
G_Mode equ 3
G_Power equ 4
G_Reload equ 5

GunConfig:			;Weapon settings
	dw txtWeaponName0		;666 shooter
		db 6			;Magazine
		db 0			;Mode
		db 1			;Power
		db 0			;Ammo
		db 0,0
	dw txtWeaponName1		;Pump Action Stakegun
		db 4			;Magazine
		db 1			;Mode
		db 2			;Power
		db 20			;Ammo
		db 0,0
	dw txtWeaponName2		;UV Laser
		db 1			;Magazine
		db 2			;Mode
		db 6			;Power
		db 3			;Ammo
		db 0,0
	dw txtWeaponName3		;Chibigun
		db 100			;Magazine
		db 3			;Mode
		db 1			;Power
		db 100			;Ammo
		db 0,0

;Weapon Names
txtWeaponName0: db "666",255,"Shooter",255
txtWeaponName1: db "Pump-Action",255,"StakeGun",255
txtWeaponName2: db "UV Laser",255,"Pulse Rifle",255
txtWeaponName3: db "N-134",255,"Chibigun",255
	
;General text strings

TxtBullets: db "Bullets:",255			;Bullets left
TxtNight: db "Night ",255				;Night (level)
txtScore: db "Score:",255				;Score
TxtHiScore:   db "HiScore:",255			;Highscore for title

;Gameover Text Strings

txtGameover1: db "You Have Sustained",255
txtGameover2: db "A Lethal Sucking.",255
txtGameover3: db "Sorry, But You Are",255
txtGameover4: db "Finished Here!",255

txtNewHiscore: db "New High Score!",255




;Binary Coded Decimal Score Additions

BCD1: db &01,&00,&00,&00	;1 		Score 'adders' for BCD... 
BCD5: db &05,&00,&00,&00	;5
BCDX: db &30,&00,&00,&00	;3		my BCD requires value to add to match in length destination score



;Object definition bytes
O_Ypos equ 0
O_Xpos equ 1
O_Hei equ 2 
O_Wid equ 3
O_SprL equ 4 
O_SprH equ 5
O_Zpos equ 6
O_Life equ 7		;0-31 (32-255 for later levels
O_AnimTick equ 8
O_MoveX equ 9
O_MoveY equ 10
O_MoveZ equ 11
O_Program equ 12
O_SprNum equ 13
O_FrameSpeed equ 14
O_FrameTick equ 15

Spr3D equ &80	;3DAnimated - 3 sizes / 2 frames

PrgDead equ &80			;Bit 7 of Program
PrgShootable equ &40	;Bit 6 of Program

prgPowerup equ 1		;Animation programs
prgBat1 equ 2
prgBackground equ 3
prgNothing equ 4
prgCorona equ 5
prgChibiko equ 6
prgMoon equ 7
prgBat2 equ 8

CloudCount equ 8
GrassCount equ 6
RockCount equ 6
EnemyCount equ 18

SpriteDataSize equ 16	;16 byttes per object

SpriteDataCount equ 4+CloudCount+GrassCount+RockCount+EnemyCount		;Total number of objects (42 objects)



;Character Sprite def (used for 3D PrintChar)

DefChar:
	dw &4040	;XY-Pos
	dw &0404	;Size
	dw 0		;Sprite Addr
	db -40		;Zpos (Negative = Sticking out
	db 0		;life
	db 0		;Tick
	db 0,0,0	;XYZ move
	db 0,0,0,0

DefCloud:					
	dw &4212	;XY-Pos
	dw &1818	;Size
	dw 0		;Sprite Addr
	db 70		;Zpos (Negative = Sticking out
	db 1		;life
	db %00000011 ;Tick
	db 2,0,0	;XYZ move
	db prgBackground;Program
	db 30		;Sprite
	db 0,0		;Frame Speed / Tick

DefMountain:
	dw &2020	;XY-Pos
	dw &1010	;Size
	dw 0		;Sprite Addr
	db 90		;Zpos (Negative = Sticking out
	db 255		;life
	db 0		;Tick
	db 0,0,0	;XYZ move
	db 0 		;program
	db 31		;Sprite
	db 0,0		;Frame Speed / Tick

DefPowerup: ;Powerups we can collect (Inc guns)
	dw &0030	;XY-Pos
	dw &1818	;Size
	dw 0		;Sprite Addr
	db -40		;Zpos (Negative = Sticking out
	db 1		;life
	db %00000000 ;Tick
	db 4,0,0	;XYZ move
	db PrgDead+prgPowerup+PrgShootable;Program
	db 5		;Sprite
	db 0,0		;Frame Speed / Tick

DefGun:		;Gun Sprite on the HUD
	ifdef ScreenSize256
		dw &785C ;XY-Pos
	else
		dw &7060 ;XY-Pos
	endif
	dw &1010	;Size
	dw 0		;Sprite Addr
	db -32		;Zpos (Negative = Sticking out
	db 255		;life
	db 0		;Tick
	db 0,0,0	;XYZ move
	db prgPowerup+PrgShootable
	db 1		;Sprite
	db 0,0		;Frame Speed / Tick

DefGrass:
	dw &4212	;XY-Pos
	dw &1818	;Size
	dw 0		;Sprite Addr
	db 0		;Zpos (Negative = Sticking out
	db 1		;life
	db %00000011 ;Tick
	db 2,0,0	;XYZ move
	db prgBackground;Program
	db 29		;Sprite
	db 0,0		;Frame Speed / Tick

DefRock:
	dw &4212	;XY-Pos
	dw &1818	;Size
	dw 0		;Sprite Addr
	db 0		;Zpos (Negative = Sticking out
	db 1		;life
	db %00000000 ;Tick
	db 2,0,0	;XYZ move
	db prgBackground;Program
	db 28		;Sprite
	db 0,0		;Frame Speed / Tick


DefBat1:	;Basic Bat
	dw &4030	;XY-Pos
	dw &1818	;Size
	dw 0		;Sprite Addr
	db 127		;Zpos (Negative = Sticking out
	db 1		;life
	db %00000000 ;Tick
	db 0,0,-8	;XYZ move
	db PrgDead+prgBat1+PrgShootable;Program
	db 12+Spr3D	;Sprite
	db 128,0	;Frame Speed / Tick
	
DefBat2:	;Gobbey bat
	dw &4030	;XY-Pos
	dw &1818	;Size
	dw 0		;Sprite Addr
	db 127		;Zpos (Negative = Sticking out
	db 1		;life
	db %00000000	;Tick
	db 0,0,-8	;XYZ move
	db PrgDead+prgBat2+PrgShootable;Program
	db 18+Spr3D		;Sprite
	db 128,0	;Frame Speed / Tick



DefCorona:	;Chibi Corona
	dw &4030	;XY-Pos
	dw &1818	;Size
	dw 0		;Sprite Addr
	db -60		;Zpos (Negative = Sticking out
	db 1		;life
	db %00000000	;Tick
	db 0,0,-0	;XYZ move
	db PrgDead+prgCorona+PrgShootable;Program
	db 24+Spr3D;-4		;Sprite
	db 128,0	;Frame Speed / Tick
	


DefChibiko:	;Here's chibi!
	dw &4030	;XY-Pos
	dw &1818	;Size
	dw 0		;Sprite Addr
	db 60		;Zpos (Negative = Sticking out
	db 1		;life
	db %00000000	;Tick
	db 2,4,-12	;XYZ move
	db PrgDead+prgChibiko+PrgShootable;Program
	db 6+Spr3D;-4		;Sprite
	db 128,0	;Frame Speed / Tick

DefMoon:	;Moon (background - also acts as level imer)
	ifdef ScreenSize256
		dw &4008	;XY-Pos
	else
		dw &3008	;XY-Pos
	endif
	dw &1010	;Size
	dw 0		;Sprite Addr
	db 80		;Zpos (Negative = Sticking out
	db 255		;life
	db %000000001	;Tick
	db 4,0,0	;XYZ move
	db prgMoon	;Program
	db 0		;Sprite
	db 0,0		;Frame Speed / Tick

	
	
	
	;Player Defs for the start of a new game
NewGameData:
	dw 40*256	;OldPosX	- Cursor pos
	dw 100*256	;OldPosY
	dw 40*256	;NewPosX
	dw 100*256	;NewPosY

	db 0		;GunNum 

	db 0		;PlayerLifeFraction
	db 32		;PlayerLife

	db 6,255	;GunAmmo
	db 0,0
	db 0,0
	db 0,0

	db 0,0,0,0	;score
	ifdef BuildMSX2
		dw 0	;Cursor sprite
	else
		dw SprCursorSprite
	endif
NewGameData_End:



LevelList:
	dw Level01	;Level pointers (repeat >16)
	dw Level02
	dw Level03
	dw Level04
	dw Level05
	dw Level06
	dw Level07
	dw Level08
	dw Level09
	dw Level10
	dw Level11
	dw Level12
	dw Level13
	dw Level14
	dw Level15
	dw Level16

LevelSequence:	;Sequence of objects in Def
	dw DefBat1	
	dw DefBat2
	dw DefChibiko
	dw DefCorona

Level01: 
	db 3	;Bat 1
	db 0	;Bat 2
	db 0	;Chibiko
	db 0	;Chibi Corona!
	db %00000000	;Moon Speed
	db %11111111	;Game Speed

Level02: 
	db 3	;Bat 1
	db 1	;Bat 2
	db 0	;Chibiko
	db 0	;Chibi Corona!
	db %00000000	;Moon Speed
	db %11111111	;Game Speed
Level03: 
	db 4	;Bat 1
	db 1	;Bat 2
	db 0	;Chibiko
	db 0	;Chibi Corona!
	db %00000001	;Moon Speed
	db %11111111	;Game Speed
Level04: 
	db 3	;Bat 1
	db 2	;Bat 2
	db 0	;Chibiko
	db 0	;Chibi Corona!
	db %00000001	;Moon Speed
	db %11111111	;Game Speed
Level05: 
	db 3	;Bat 1
	db 2	;Bat 2
	db 0	;Chibiko
	db 1	;Chibi Corona!
	db %00000011	;Moon Speed
	db %11111111	;Game Speed
Level06: 
	db 3	;Bat 1
	db 2	;Bat 2
	db 0	;Chibiko
	db 2	;Chibi Corona!
	db %00000011	;Moon Speed
	db %11111111	;Game Speed
Level07: 
	db 4	;Bat 1
	db 2	;Bat 2
	db 0	;Chibiko
	db 2	;Chibi Corona!
	db %00000011	;Moon Speed
	db %11111111	;Game Speed
Level08: 
	db 1	;Bat 1
	db 0	;Bat 2
	db 1	;Chibiko
	db 2	;Chibi Corona!
	db %00000011	;Moon Speed
	db %11111111	;Game Speed
Level09: 
	db 4	;Bat 1
	db 2	;Bat 2
	db 0	;Chibiko
	db 2	;Chibi Corona!
	db %00000011	;Moon Speed
	db %11111111	;Game Speed
Level10: 
	db 4	;Bat 1
	db 2	;Bat 2
	db 0	;Chibiko
	db 3	;Chibi Corona!
	db %00000011	;Moon Speed
	db %11111111	;Game Speed
Level11: 
	db 3	;Bat 1
	db 3	;Bat 2
	db 0	;Chibiko
	db 3	;Chibi Corona!
	db %00000111	;Moon Speed
	db %01111111	;Game Speed
Level12: 
	db 3	;Bat 1
	db 3	;Bat 2
	db 0	;Chibiko
	db 4	;Chibi Corona!
	db %00000111	;Moon Speed
	db %01111111	;Game Speed
Level13: 
	db 3	;Bat 1
	db 4	;Bat 2
	db 0	;Chibiko
	db 4	;Chibi Corona!
	db %00000111	;Moon Speed
	db %01111111	;Game Speed
Level14: 
	db 3	;Bat 1
	db 4	;Bat 2
	db 0	;Chibiko
	db 5	;Chibi Corona!
	db %00000111	;Moon Speed
	db %01111111	;Game Speed
Level15: 
	db 2	;Bat 1
	db 4	;Bat 2
	db 0	;Chibiko
	db 6	;Chibi Corona!
	db %00000111	;Moon Speed
	db %01111111	;Game Speed
Level16: 
	db 3	;Bat 1
	db 2	;Bat 2
	db 1	;Chibiko
	db 6	;Chibi Corona!
	db %00000111	;Moon Speed
	db %01111111	;Game Speed