


;Sprites
SpriteArray equ GameRam

SprPowerup equ SpriteArray
SprMoon equ SprPowerup+16
SprMountain equ SprMoon+16

SprClouds equ SprMountain+16			;8
SprGrass equ 16*CloudCount+SprClouds	;6
SprRock equ 16*GrassCount+SprGrass		;6

SprGun equ 16*RockCount+SprRock

SprEnemies equ 16+SprGun				;18

SpriteArrayEnd equ 16*EnemyCount+SprEnemies

SprChar equ SpriteArrayEnd		;For Print




Bak3d equ SprChar+16			;Bytes to reverse Selfmodifying 
										;changes to 3d code
HiScore equ Bak3d+4				;Game Highscore BCD (L)

CursorY equ HiScore+4			;3D Cursor Position
CursorX equ CursorY+1	
CursorZ equ CursorX+1	

RandomSeed equ CursorZ+1 		;Random number gen (W)

FireHeld equ RandomSeed+2 		;Fire/Mouse buttons
FireDown equ FireHeld+1 
FireHeld2 equ FireDown+1 		;Reload 
FireDown2 equ FireHeld2+1

GameSpeed equ FireDown2+1		;Game Time

Tick equ GameSpeed+1			;Game Tick (W)

PaletteNum equ Tick+2			;3D Colors
Depth3D equ PaletteNum+1		;3D Depth

LevelNum equ Depth3D+1  		;Level/Night

ScreenEdgeSpawnPos equ LevelNum+1  ;Spawn Position

CursorMoveSpeed equ ScreenEdgeSpawnPos+1 	;Cursor move speed (W)
CursorMoveSpeedNum equ CursorMoveSpeed+2

SoundSequence equ CursorMoveSpeedNum+1 		;Sound Sequence pointer (W)
SoundTimeout equ SoundSequence+2 			;Delay
SoundPriority equ SoundTimeout+1 			;Play priority

OldPosX equ SoundPriority+1			;Old Cursorpos (W,W)
OldPosY equ OldPosX+2

NewPosX equ OldPosY+2				;New Cursorpos (W,W)
NewPosY equ NewPosX+2

GunNum  equ NewPosY+2				;Selected Gun Number

PlayerLifeFraction equ GunNum+1		;Player life (parts)
PlayerLife equ PlayerLifeFraction+1	;Visible player life bar

GunAmmo equ PlayerLife+1			;Loaded Ammo/Magazine for 
										;all 4 guns (8 byte)

Score equ GunAmmo+8 				;BCD Score (L)

CrosshairSprite equ Score+4 		;Pointer to crosshair sprite (W)

GameRamEnd equ CrosshairSprite+2

