


;*************************************************************************************
;*************************************************************************************

;	2nd bank	(&8000-FFFF) - Actually maps to (&0000-7FFF in ram)

;*************************************************************************************
;*************************************************************************************

B2p equ &8000	;Fix for address offsets

;2nd bank Jump block	
	org &8000					;Bank 3 Start (will be mapped in at &0000)
	jp ShowTitleImg-B2p			;&0000 Title Screen
	jp ShowGameOver-B2p			;&0003
	jp PlaySample				;&8006 We don't need to draw to screen, 
									;so actually map to &8000 for this
	
ShowGameOver:					;Game over bitmap
	ld hl,SuckOfDeath3dTileMap-B2p
	ld bc,SuckOfDeath3dTiles-B2p
	ld iyh,12	;X
	ld a,0		;Y
	jp	ShowTileMapAltPosQtr-B2p
		
ShowTitleImg:					;Title screen
	ld hl,TitleTileMap3D-B2p
	ld bc,TitleTiles3D-B2p
	jp ShowTileMap-B2p			;Show Tilemap
	
	org &8038					;Interrupt handler to keep music playing
	push af
	push de
	push hl
	
		ld d,1
		ld hl,InterruptHandler		;Bounce IM1 to Bank1 interrupt handler
		call DoFarCall
	pop hl
	pop de
	pop af
	ret
	
	
	
ShowTileMapAltPosQtr:
	ld d,16		;W
	ld e,12		;H
	jr ShowTileMapAltPos

ShowTileMap:		;HL=Tilemap BC=Tile Data
	ld iyh,4	;X
	ld a,0		;Y
	ld e,24
	ld d,32
	
;HL=Tilemap BC=Tile Data IYH=X	A=Y	;E=Height ;D=Wid	
ShowTileMapAltPos:	
	ld (CursorY),a
	xor a
	ld (CursorZ),a
	
TilemapYagain:
	ld a,iyh
	sub 4
	ld (CursorX),a	;Set Start pos of line
	push iy
	push de
TilemapXagain:
	ld a,(hl)			;First byte>255 = 2nd bank (255-511)
	inc hl

	cp 255
	jr c,FirstTileBank

	ld a,(hl)			;Load next byte as well
	inc hl
	push bc
	push hl
		ld hl,256*32-32		;Offset to 2nd bank (255+)
TileBankSize_Plus2:
		add hl,bc			;Add to tile data source
		ld b,h
		ld c,l
		call PrintTile-B2p	;Show the tile
TileEngine_Plus2:
	pop hl
	pop bc
	jr TileDone
FirstTileBank:
	call PrintTile-B2p		;Show the tile
TileEngine_Plus2B:
TileDone:
	dec d
	jr nz,TilemapXagain
	push hl
		xor a
		ld (CursorX),a		;Reset Xpos
		ld a,(CursorY)
		inc a				;Move down a line
		ld (CursorY),a
	pop hl
	pop de
	pop iy
	dec e
	jr nz,TilemapYagain
	ret
	
	
PrintTile:	;TitleTiles
	push af
	push bc
	push hl
	push de	
		ld h,a
		xor a
		srl h				;Tile * 32
		rra
		srl h
		rra
		srl h
		rra
		ld l,a
		add hl,bc
		ex de,hl
		ld bc,(CursorY)		;Get XY into BC
		
		call DrawTile-B2p	;Show the tile
		ld a,(CursorX)
		inc a
		ld (CursorX),a	
	pop de
	pop hl
	pop bc
	pop af
	ret

;Draw a single tile (16 color)
DrawTile:
	rl b
	rl b
	rl c
	rl c
	rl c
	call GetScreenPos2-B2p
	ld b,8			;Lines (Height)
TileNextLine:
	push hl
		ld c,4	;Bytes per line (Width)
TileNextByte:
		ld a,(de)
		ld (hl),a	;Screen Destination
		inc hl		;INC Dest (Screen) Address
		inc de		;INC Source (Sprite) Address
		dec c 		;Repeat for next byte
		jr nz,TileNextByte
	pop hl	
	call GetNextLine2-B2p	;Scr Next Line (Alter HL to move down a line)
	djnz TileNextLine	;Repeat for next line
	ret
	
GetScreenPos2:	;return mempos in DE of screen co-ord B,C (X,Y)
	ld h,c	
	xor a	
	rr h		;Effectively Multiply Ypos by 128
	rra 
	or b		;Add Xpos
	ld l,a	
	ld a,&80
	add h
	ld h,a
	ret

GetNextLine2:	;Move DE down 1 line 
	ld a,&80	
	add l		;Add 128 to mem position 
	ld l,a
	ret nc
	inc h		;Add any carry
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

;		Title Screen

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

	
	
TitleTiles3D:
	incbin "\ResALL\SuckHunt\ZXNTitleTiles3D.RAW"	
	
TitleTileMap3D:
  db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,0,0,0,0,0,0,0,0,3,4,5,6,0,0
  db 0,0,0,0,0,0,7,8,9,10,11,12,13,0,0,14,15,16,17,18,0,0,0,0,0,0,19,20,21,22,0,0
  db 0,0,0,0,0,0,23,24,25,26,27,28,29,0,0,30,31,32,33,34,0,0,0,0,0,0,0,0,0,0,0,0
  db 8,9,10,11,12,13,0,35,36,37,38,0,0,0,0,39,40,41,42,43,0,7,8,9,10,11,12,13,0,0,0,0
  db 24,25,26,27,28,29,0,44,45,46,47,48,38,0,0,49,50,51,52,53,0,23,24,25,26,27,28,29,0,0,0,0
  db 0,0,0,54,55,56,57,58,59,46,60,61,62,63,0,0,0,0,0,0,64,0,0,0,65,66,67,68,0,0,0,0
  db 0,0,69,70,71,72,73,74,75,76,77,78,79,80,0,0,0,0,0,81,82,83,84,85,0,86,87,0,0,0,0,0
  db 0,0,88,89,90,91,92,93,94,95,96,97,98,99,100,0,0,0,0,101,102,103,104,105,0,0,0,106,107,108,0,0
  db 109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 126,127,128,129,130,131,132,133,134,135,136,137,138,46,139,46,140,141,142,143,0,0,0,0,144,145,146,0,144,145,146,0
  db 147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,38,169,170,171,172,173,170,171,172,173
  db 174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,0,0,0,0,0,0,0,0,0
  db 197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,0,220,221,222,223,224,225,0,0
  db 0,0,0,226,227,228,229,230,46,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,0
  db 0,0,0,0,253,254,255,0,255,1,46,255,2,255,3,255,4,46,255,5,255,6,255,7,255,8,255,9,255,10,255,11,255,12,255,13,255,14,255,15,255,16,255,17,255,18,255,19,255,20,255,21,255,22,0
  db 0,0,0,0,0,0,255,23,46,46,255,24,255,25,46,46,255,26,255,27,255,28,255,29,255,30,255,31,255,32,255,33,255,34,255,35,255,36,255,37,255,38,255,39,255,40,255,41,255,42,255,43,0
  db 0,0,0,0,0,255,44,230,46,46,255,5,255,45,46,231,232,255,46,255,47,255,48,255,49,255,50,255,51,255,52,255,53,0,255,54,255,55,255,56,255,57,255,58,255,59,255,60,0,0
  db 0,0,0,0,0,255,61,255,62,255,63,255,63,255,64,255,65,255,66,255,67,255,68,0,0,0,0,0,0,0,0,0,0,255,69,255,70,255,71,255,72,255,73,255,74,255,75,255,76
  db 0,0,0,0,0,0,0,0,0,0,0,255,77,255,78,255,79,255,80,0,255,81,255,82,255,83,255,84,0,0,0,255,85,255,86,255,87,255,88,255,89,255,90,255,91,255,92,255,93
  db 255,94,255,95,255,96,255,97,0,0,0,0,0,0,0,0,255,98,255,99,255,100,0,255,81,255,101,255,102,255,103,0,255,104,255,105,255,106,255,107,255,108,255,109,255,110,255,111,255,112,255,113,255,114
  db 255,115,0,255,116,255,117,255,118,0,0,255,104,255,105,255,106,0,255,119,255,120,255,121,255,122,0,255,81,255,123,255,124,255,125,0,255,126,255,127,255,128,0,0,255,129,255,130,255,130,255,131,255,104,255,105
  db 255,132,255,133,255,134,255,135,255,136,255,137,0,255,126,255,127,255,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,126,255,127
  db 255,138,255,139,255,140,255,141,255,142,255,143,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  db 255,144,255,145,255,146,255,147,255,148,255,149,255,150,255,151,255,152,255,153,255,154,255,155,255,156,255,157,255,158,255,159,0,0,0,255,129,255,130,255,130,255,131,0,0,0,0,0,0,0,0,0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
 
;		Game Over screen 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

  
SuckOfDeath3dTileMap:
  db 0,0,0,0,0,1,2,3,0,0,0,0,0,0,0,0
  db 0,0,0,0,0,4,5,6,7,8,0,0,0,0,0,0
  db 0,0,0,0,0,0,0,9,10,11,12,0,0,0,0,0
  db 0,0,0,0,13,14,15,16,17,18,19,20,21,22,0,0
  db 0,0,0,23,24,25,26,27,28,29,30,31,32,33,34,0
  db 0,0,35,36,37,38,39,40,41,42,43,44,45,46,0,0
  db 0,0,47,48,49,50,51,52,53,54,55,56,57,58,59,60  
  db 0,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75
  db 76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,0
  db 91,92,93,0,94,95,96,97,98,99,100,101,102,103,104,105
  db 106,107,108,0,109,110,111,112,113,114,115,116,117,118,119,120
  db 121,122,123,0,124,125,126,0,127,128,129,130,131,132,133,134

  
SuckOfDeath3dTiles:
	incbin "\ResALL\SuckHunt\SuckofDeathZXN3d.RAW"
	
  
PlaySample:
  		ld de,wavedataEnd-wavedata
		ld hl,wavedata

		xor a	;Bits
		ld b,40	;Speed
		di
		jp ChibiWave


freq4 equ 1	;11025
bits1 equ 1
	read "\SrcALL\Multiplatform_ChibiWave.asm"

	
wavedata:
	incbin "\ResALL\SuckHunt\operationsucka1-6.raw"
wavedataEnd:
	

;*************************************************************************************
;*************************************************************************************

;		End of	2nd bank	(&8000-FFFF)

;*************************************************************************************
;*************************************************************************************
