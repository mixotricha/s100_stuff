nolist 

;UnREM only one of these
;TestMode0 equ 1
;TestMode1 equ 1
TestMode2 equ 1

;Enable this to use 256 byte LookUp Table for speed
;UseLut equ 1

	macro FlipMode1	;Source Pixels ABCD
		ld b,a
		and %10001000	;A
		rrca
		rrca
		rrca
		ld c,a
		ld a,b
		and %01000100	;B
		rrca
		or c
		ld c,a
		ld a,b
		and %00100010	;C
		rlca
		or c
		ld c,a
		ld a,b
		and %00010001	;D
		rlca
		rlca
		rlca
		or c
	endm

;Left  Pixel Bits 1537 
;Right Pixel Bits 0426
	macro FlipMode0	;Source Pixels AB
			ld b,a
			and %01010101	;B
			rlca
			ld c,a
			ld a,b
			and %10101010	;A
			rrca
			or c
	endm

	macro FlipMode2 ;Source Pixels ABCDEFGH
		ld b,a	
		and %10000000	;A
		rlca
		ld c,a
		ld a,b
		and %01000000	;B
		rlca
		rlca
		rlca
		or c
		ld c,a
		ld a,b
		and %00100000	;C
		rrca
		rrca
		rrca
		or c
		ld c,a
		ld a,b	
		and %00010000 	;D
		rrca
		or c
		ld c,a
		ld a,b	
		and %00001000 	;E
		rlca
		or c
		ld c,a
		ld a,b	
		and %00000100	;F
		rlca
		rlca
		rlca
		or c
		ld c,a
		ld a,b	
		and %00000010	;G
		rrca
		rrca
		rrca
		or c
		ld c,a
		ld a,b	
		and %00000001	;H
		rrca
		or c
	endm


	org &1200		;Start of our program


	ifdef UseLut
		ld hl,FlipLUT
FillLutAgain:
		ld a,l
		ifdef TestMode0
			FlipMode0	;Mode 0 Ver Flip Macro	
		endif
		ifdef TestMode1
			FlipMode1	;Mode 1 Ver Flip Macro	
		endif
		ifdef TestMode2
			FlipMode2	;Mode 2 Ver Flip Macro	
		endif
		ld (hl),a

		inc l
		jr nz,FillLutAgain
	endif


	ifdef TestMode0
		ld a,0
	endif
	ifdef TestMode1
		ld a,1
	endif
	ifdef TestMode2
		ld a,2
	endif
	call &BC0E		;Set Screen Mode A

	ld de,&0040		;Xpos (in pixels)
	ld hl,&00A0		;Ypos (in pixels)

	call &BC1D		;Scr Dot Position - Returns address in HL

	ld de,TestSprite	;Sprite Source

	ld iyl,48		;Lines (Height)

	ifdef UseLut
		ld bc,FlipLUT	;Lookup Table Address
	endif 

SpriteNextLine:
	push hl
		ifdef TestMode2
			ld ixl,6	;Bytes per line (Width Mode 2)
		else
			ld ixl,12	;Bytes per line (Width Mode 0/1)
		endif

SpriteNextByte:
		ld a,(de)	;Source Byte
	ifdef UseLut
		ld c,a		;Set Low byte of address
		ld a,(bc)	;Read Flipped byte from LUT
	else
		ifdef TestMode0
			FlipMode0	;Mode 0 Ver Flip Macro	
		endif
		ifdef TestMode1
			FlipMode1	;Mode 1 Ver Flip Macro	
		endif
		ifdef TestMode2
			FlipMode2	;Mode 2 Ver Flip Macro	
		endif
	endif

		ld (hl),a	;Screen Destination

		inc de		;INC Source (Sprite) Address
		dec hl		;INC Dest (Screen) Address

		dec ixl		;Repeat for next byte
		jr nz,SpriteNextByte
	pop hl
	call &BC26		;Scr Next Line (Alter HL to move down a line)
	dec iyl
	jr nz, SpriteNextLine	;Repeat for next line

	ret			;Finished

TestSprite:
	ifdef TestMode0
		incbin "\ResALL\Sprites\RawCPC16.raw"	;Mode 0 Sprite
	endif
	ifdef TestMode1
		incbin "\ResALL\Sprites\SpriteCPC.raw"	;Mode 1 Sprite
	endif
	ifdef TestMode2
		incbin "\ResALL\Sprites\RawZX.RAW"	;Mode 2 Sprite
	endif

	Align 256
FlipLUT:
	ds 256