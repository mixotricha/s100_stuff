



ChibiOctive:
	;   E     F     G      A     B     C     D   
	dw &3000,&4000,&5000,&6000,&7000,&8000,&9000 ;0
	dw &A1A6,&A6CB,&B096,&B8F4,&C097,&C446,&CAD4 ;1
	dw &D09A,&D32A ,&D816,&DC76,&E058,&E218,&E55B;2
	dw &E843,&E9A7,&EC11,&EE31,&F029,&F10D,&F2AA ;3
	dw &F414,&F4CC,&F60C,&F71C,&F819,&F889,&F95A ;4
	dw &FA04,&FA6C,&FAF3,&FB8E,&FC0C,&FC3E,&FCAC;5
	dw &FCF8,&FD2E,&FD79,&FDCA,&FDFC,&FE1A,&FE4D ;6
	dw &FE40,&FEC0,&FF00,&FF40,&FF80,&FFC0,&FFFF
	   



ChibiTone:	;E=Note (Bit 1-6=Note  Bit0=Flat)
	;E-F-G-A-B-C-D-... Octive 0-6
	push hl
			ld hl,ChibiToneGotNote
			push de
				res 0,e
				add hl,de
			pop de
			ld a,e
			ld e,(hl)
			inc hl
			ld d,(hl)
			bit 0,a
			jp z,ChibiToneGotNote
			inc hl
			srl d
			rr e

			ld a,(hl)		;Half note
			inc hl
			ld h,(hl)
			ld l,a
			srl h
			rr l
			add hl,de		;add two halves
			ex de,hl 
ChibiToneGotNote:
	pop hl

;H=Volume (0-255) 
;L=Channel Num (0-127 unused channels will wrap around) / Top Bit=Noise
;DE=Pitch (0-65535)

ChibiSoundPro:
	ld a,h
	or a
	jp z,Silent
	
	ld a,ixl
	bit 7,a
	jr z,ChibiSoundPro_NoEnv
	
	push de
		ld l,4
		ld a,ixh
		ld d,a
		ld e,0
		call SetOctive
	pop de
	ld l,5					;Channel 5
	ld a,ixl
	and %00001111
	or  %10000000
	ld b,a
	ld a,&19
	call SetSoundRegister	;Set Envelope
	
	ld b,0
	ld a,&C
	call SetSoundRegister	;Freq of envelope (Chn4)
	
	
	
ChibiSoundPro_NoEnv:
	bit 7,l
	jr z,ChibiSoundPro_DontSelectNoise
	ld l,%10000011			;Play all noise on channel 3
	
	jr ChibiSoundPro_ChannelOK
ChibiSoundPro_DontSelectNoise:
	ld a,l
	and %00000111
	cp 6
	jr c,ChibiSoundPro_ChannelOK
	dec a
	dec a
	ld l,a
ChibiSoundPro_ChannelOK:

	ld a,h
	and %11110000
	ld h,a
	rrca
	rrca
	rrca
	rrca
	or h
	ld b,a
	ld a,l
	and %00000111
	;add &0
	call SetSoundRegister	;Volume 0

	ld a,&1C
	ld b,%00000001
	call SetSoundRegister	;Enable Sound

	
	
	call SetOctive

	call GetChannelMask
	ld a,(de)
	or b
	ld (de),a
	ld b,a
	ld a,&14
	call SetSoundRegister	;Tone Enable 

	call GetChannelMask
	inc de
	ld a,(de)
	and c
	ld (de),a
	ld b,a
	ld a,&15
	call SetSoundRegister	;noise disable

	
	
	ld a,l
	and %10000000
	ret z
	;Here comes the noise!

	
	call GetChannelMask
	ld a,(de)
	and c
	ld (de),a
	ld b,a
	ld a,&14
	call SetSoundRegister	;Tone disable 

	call GetChannelMask
	inc de
	ld a,(de)
	or b
	ld (de),a
	ld b,a
	ld a,&15
	call SetSoundRegister	;Noise Enable

	ld a,&16
	ld b,%00110011
	call SetSoundRegister	

	ret
SetOctive:		;Set octive/Freq L=DE

	srl d
	rr e
	srl d
	rr e
	srl d
	rr e
	srl d
	rr e
	srl d
	rr e

	ld b,e
	ld a,l
	and %00000111
	add &8
;	ld d,%11111111
	call SetSoundRegister	;Tone 0
	
	push de
		ld e,%11111000
		bit 0,l
		jr z,NoOctiveShift
		sla d
		rlc e
		sla d
		rlc e
		sla d
		rlc e
		sla d
		rlc e
NoOctiveShift:
		push hl
			ld hl,ChannelCache2
			ld b,0
			ld c,l
			srl c
			add hl,bc
			push hl
			pop bc
		pop hl
		ld a,(bc)
		and e
		or d
		ld (bc),a
		
	pop de
	ld b,a
	ld a,l
	srl a
	add &10
	call SetSoundRegister	;Octive 0
	ret
Silent:
	call GetChannelMask
	ld a,(de)
	and c
	ld (de),a
	ld b,a
	ld a,&14
	call SetSoundRegister	;Tone Enable 

	call GetChannelMask
	inc de
	ld a,(de)
	and c
	ld (de),a
	ld b,a
	ld a,&15
	call SetSoundRegister	;noise disable

	ret
	
SetSoundRegister
	push bc
		ld bc,511
		out (c),a

		ld bc,255
		
	pop af
	out (c),a
	ret
GetChannelMask:
	push hl
		ld a,l
		and %00000111
		ld c,a
		ld b,0
		ld hl,ChannelMasks
		add hl,bc
		ld a,(hl)
		ld hl,ChannelCache
		add hl,bc
		add hl,bc
		ex de,hl
	pop hl
	ld b,a
	cpl
	ld c,a
	ret
	
ChannelMasks:
	db %00000001,%00000010,%00000100,%00001000,%00010000,%00100000
ChannelCache:
;Previous values (Freq,Noise)
	db 0,0,0,0,0,0,0,0,0,0,0,0
ChannelCache2:
;Previous values (OCT)
	db 0,0,0