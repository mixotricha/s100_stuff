Player_ReadControlsDual:
	LD  B,%11111111		;Key line

	LD  C,&F9  		;port for Lines K6 - K8 - This is extra for the SAM
	IN  A,(C)
	AND  %11100000		;Keep only the top three bits
	ld l,a
	LD  C, &FE  		;port for Lines K1 - K5 this is the same as the speccy
	IN  A, (C)
	AND  %00011111     	;Keep only the valid bits
	OR l 			;OR in the value for the top 3 lines
	LD  l,A 		 	;Save the result
	
	rr l
	rl h
	
	rl l
	rl l
	rl l
	rl l
	
	rl l
	rl h
	rl l
	rl h
	rl l
	rl h
	rl l
	rl h
	ret