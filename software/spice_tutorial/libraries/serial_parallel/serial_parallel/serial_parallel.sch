EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74LS273 U?
U 1 1 5EAB6A44
P 3300 4650
F 0 "U?" H 3300 5628 50  0000 C CNN
F 1 "74LS273" H 3300 5537 50  0000 C CNN
F 2 "" H 3300 4650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS273" H 3300 4650 50  0001 C CNN
F 4 "X" H 3300 4650 50  0001 C CNN "Spice_Primitive"
F 5 "74LS273" H 3300 4650 50  0001 C CNN "Spice_Model"
F 6 "Y" H 3300 4650 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "/home/mixotricha/workspace/spice_tutorial/libraries/74LS.LIB" H 3300 4650 50  0001 C CNN "Spice_Lib_File"
	1    3300 4650
	1    0    0    -1  
$EndComp
Text HLabel 2500 5150 0    50   Input ~ 0
RST
Wire Wire Line
	2500 5150 2800 5150
Text HLabel 2500 5050 0    50   Input ~ 0
CLK
Wire Wire Line
	2500 5050 2800 5050
Text HLabel 2550 4150 0    50   Input ~ 0
DATA
Wire Wire Line
	2550 4150 2800 4150
Wire Wire Line
	3800 4150 3800 3500
Wire Wire Line
	3800 3500 2100 3500
Wire Wire Line
	2100 3500 2100 4250
Wire Wire Line
	2100 4250 2800 4250
Wire Wire Line
	2800 4350 1950 4350
Wire Wire Line
	1950 4350 1950 3400
Wire Wire Line
	1950 3400 3900 3400
Wire Wire Line
	3900 3400 3900 4250
Wire Wire Line
	3900 4250 3800 4250
Wire Wire Line
	2800 4450 1850 4450
Wire Wire Line
	1850 4450 1850 3300
Wire Wire Line
	1850 3300 4000 3300
Wire Wire Line
	4000 3300 4000 4350
Wire Wire Line
	4000 4350 3800 4350
Wire Wire Line
	2800 4550 1750 4550
Wire Wire Line
	1750 4550 1750 3200
Wire Wire Line
	1750 3200 4100 3200
Wire Wire Line
	4100 4450 3800 4450
Wire Wire Line
	2800 4650 1650 4650
Wire Wire Line
	1650 4650 1650 3100
Wire Wire Line
	1650 3100 4200 3100
Wire Wire Line
	4200 4550 3800 4550
Wire Wire Line
	2800 4750 1550 4750
Wire Wire Line
	1550 4750 1550 3000
Wire Wire Line
	1550 3000 4300 3000
Wire Wire Line
	4300 3000 4300 4650
Wire Wire Line
	4300 4650 3800 4650
Wire Wire Line
	2800 4850 1450 4850
Wire Wire Line
	1450 4850 1450 2900
Wire Wire Line
	1450 2900 4400 2900
Wire Wire Line
	4400 2900 4400 4750
Wire Wire Line
	4400 4750 3800 4750
Text HLabel 4850 4850 2    50   Output ~ 0
Q7
Wire Wire Line
	4200 3100 4200 4550
Wire Wire Line
	4100 3200 4100 4450
Wire Wire Line
	3800 4850 4850 4850
Text HLabel 4850 4750 2    50   Output ~ 0
Q6
Wire Wire Line
	4400 4750 4850 4750
Connection ~ 4400 4750
Text HLabel 4850 4650 2    50   Output ~ 0
Q5
Wire Wire Line
	4850 4650 4300 4650
Connection ~ 4300 4650
Text HLabel 4850 4550 2    50   Output ~ 0
Q4
Wire Wire Line
	4850 4550 4200 4550
Connection ~ 4200 4550
Text HLabel 4850 4450 2    50   Output ~ 0
Q3
Wire Wire Line
	4850 4450 4100 4450
Connection ~ 4100 4450
Text HLabel 4850 4350 2    50   Output ~ 0
Q2
Wire Wire Line
	4850 4350 4000 4350
Connection ~ 4000 4350
Text HLabel 4850 4250 2    50   Output ~ 0
Q1
Wire Wire Line
	3900 4250 4850 4250
Connection ~ 3900 4250
Text HLabel 4850 4150 2    50   Output ~ 0
Q0
Wire Wire Line
	4850 4150 3800 4150
Connection ~ 3800 4150
Text HLabel 3300 2750 1    50   Output ~ 0
VCC
Wire Wire Line
	3300 3850 3300 2750
$Comp
L power:GND #PWR?
U 1 1 5EAFBC8E
P 3300 5800
F 0 "#PWR?" H 3300 5550 50  0001 C CNN
F 1 "GND" H 3305 5627 50  0000 C CNN
F 2 "" H 3300 5800 50  0001 C CNN
F 3 "" H 3300 5800 50  0001 C CNN
	1    3300 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 5800 3300 5450
$EndSCHEMATC
