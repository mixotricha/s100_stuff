EESchema Schematic File Version 4
LIBS:gir-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GTL2003PW:GTL2003PW IC1
U 1 1 5E9A909C
P 4800 2500
F 0 "IC1" H 5300 2767 50  0000 C CNN
F 1 "GTL2003PW" H 5300 2676 50  0000 C CNN
F 2 "GTL2003PW:SOP65P640X110-20N" H 5300 1350 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/GTL2003.pdf" H 5300 1250 50  0001 C CNN
F 4 "NXP - GTL2003PW - IC, GTL LO-VOLT TRANSLATOR, 20TSSOP" H 5300 1150 50  0001 C CNN "Description"
F 5 "RS" H 5300 1050 50  0001 C CNN "Supplier_Name"
F 6 "510960" H 5300 950 50  0001 C CNN "RS Part Number"
F 7 "Nexperia" H 5300 850 50  0001 C CNN "Manufacturer_Name"
F 8 "GTL2003PW" H 5300 750 50  0001 C CNN "Manufacturer_Part_Number"
F 9 "1.1" H 5650 450 50  0001 C CNN "Height"
	1    4800 2500
	1    0    0    -1  
$EndComp
$Comp
L gir-rescue:R_Small-Device R9
U 1 1 5E9D548A
P 4300 2600
F 0 "R9" H 4359 2646 50  0000 L CNN
F 1 "10K" H 4359 2555 50  0000 L CNN
F 2 "library:R_0805_2012Metric" H 4300 2600 50  0001 C CNN
F 3 "~" H 4300 2600 50  0001 C CNN
	1    4300 2600
	0    1    1    0   
$EndComp
$Comp
L gir-rescue:R_Small-Device R10
U 1 1 5E9D5D2B
P 5900 2150
F 0 "R10" H 5959 2196 50  0000 L CNN
F 1 "200K" H 5959 2105 50  0000 L CNN
F 2 "library:R_0805_2012Metric" H 5900 2150 50  0001 C CNN
F 3 "~" H 5900 2150 50  0001 C CNN
	1    5900 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 2600 4800 2600
Wire Wire Line
	5900 2050 5900 1900
$Comp
L gir-rescue:C_Small-Device C1
U 1 1 5EA36D80
P 6250 1900
F 0 "C1" H 6342 1946 50  0000 L CNN
F 1 "0.1u" H 6342 1855 50  0000 L CNN
F 2 "library:C_0805_2012Metric" H 6250 1900 50  0001 C CNN
F 3 "~" H 6250 1900 50  0001 C CNN
	1    6250 1900
	-1   0    0    1   
$EndComp
$Comp
L gir-rescue:Conn_2Rows-21Pins-Connector_Generic NC1
U 1 1 5E9AD053
P 2950 3450
F 0 "NC1" H 3000 4167 50  0000 C CNN
F 1 "Conn_2Rows-21Pins" H 3000 4076 50  0000 C CNN
F 2 "library:PinHeader_2x11_P2.54mm_Horizontal" H 2950 3450 50  0001 C CNN
F 3 "~" H 2950 3450 50  0001 C CNN
	1    2950 3450
	1    0    0    -1  
$EndComp
$Comp
L gir-rescue:R_Small-Device R2
U 1 1 5EA7857A
P 2300 3250
F 0 "R2" V 2300 2950 50  0000 C CNN
F 1 "10K" V 2300 3250 50  0000 C CNN
F 2 "library:R_0805_2012Metric" H 2300 3250 50  0001 C CNN
F 3 "~" H 2300 3250 50  0001 C CNN
	1    2300 3250
	0    -1   -1   0   
$EndComp
$Comp
L gir-rescue:R_Small-Device R3
U 1 1 5EA7A707
P 2300 3350
F 0 "R3" V 2300 3050 50  0000 C CNN
F 1 "10K" V 2300 3350 50  0000 C CNN
F 2 "library:R_0805_2012Metric" H 2300 3350 50  0001 C CNN
F 3 "~" H 2300 3350 50  0001 C CNN
	1    2300 3350
	0    -1   -1   0   
$EndComp
$Comp
L gir-rescue:R_Small-Device R4
U 1 1 5EA7C9C4
P 2300 3450
F 0 "R4" V 2300 3150 50  0000 C CNN
F 1 "10K" V 2300 3450 50  0000 C CNN
F 2 "library:R_0805_2012Metric" H 2300 3450 50  0001 C CNN
F 3 "~" H 2300 3450 50  0001 C CNN
	1    2300 3450
	0    -1   -1   0   
$EndComp
$Comp
L gir-rescue:R_Small-Device R5
U 1 1 5EA7EC49
P 2300 3550
F 0 "R5" V 2300 3250 50  0000 C CNN
F 1 "10K" V 2300 3550 50  0000 C CNN
F 2 "library:R_0805_2012Metric" H 2300 3550 50  0001 C CNN
F 3 "~" H 2300 3550 50  0001 C CNN
	1    2300 3550
	0    -1   -1   0   
$EndComp
$Comp
L gir-rescue:R_Small-Device R6
U 1 1 5EA80EDB
P 2300 3650
F 0 "R6" V 2300 3350 50  0000 C CNN
F 1 "10K" V 2300 3650 50  0000 C CNN
F 2 "library:R_0805_2012Metric" H 2300 3650 50  0001 C CNN
F 3 "~" H 2300 3650 50  0001 C CNN
	1    2300 3650
	0    -1   -1   0   
$EndComp
$Comp
L gir-rescue:R_Small-Device R7
U 1 1 5EA831CB
P 2300 3750
F 0 "R7" V 2300 3450 50  0000 C CNN
F 1 "10K" V 2300 3750 50  0000 C CNN
F 2 "library:R_0805_2012Metric" H 2300 3750 50  0001 C CNN
F 3 "~" H 2300 3750 50  0001 C CNN
	1    2300 3750
	0    -1   -1   0   
$EndComp
$Comp
L gir-rescue:R_Small-Device R8
U 1 1 5EA87CD7
P 2300 3850
F 0 "R8" V 2300 3550 50  0000 C CNN
F 1 "10K" V 2300 3850 50  0000 C CNN
F 2 "library:R_0805_2012Metric" H 2300 3850 50  0001 C CNN
F 3 "~" H 2300 3850 50  0001 C CNN
	1    2300 3850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 1750 6250 1800
Wire Wire Line
	5800 2500 5900 2500
$Comp
L gir-rescue:R_Small-Device R1
U 1 1 5EBCF132
P 2300 3150
F 0 "R1" V 2300 2850 50  0000 C CNN
F 1 "10K" V 2300 3150 50  0000 C CNN
F 2 "library:R_0805_2012Metric" H 2300 3150 50  0001 C CNN
F 3 "~" H 2300 3150 50  0001 C CNN
	1    2300 3150
	0    -1   -1   0   
$EndComp
Connection ~ 5900 2500
Wire Wire Line
	5900 2500 5900 2250
$Comp
L power:GNDD #PWR0101
U 1 1 5EC6D073
P 4750 2500
F 0 "#PWR0101" H 4750 2250 50  0001 C CNN
F 1 "GNDD" H 4754 2345 50  0000 C CNN
F 2 "" H 4750 2500 50  0001 C CNN
F 3 "" H 4750 2500 50  0001 C CNN
	1    4750 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 2500 4800 2500
Wire Wire Line
	6250 2500 6000 2500
$Comp
L power:GNDD #PWR0102
U 1 1 5ECAAD13
P 6250 1750
F 0 "#PWR0102" H 6250 1500 50  0001 C CNN
F 1 "GNDD" H 6254 1595 50  0000 C CNN
F 2 "" H 6250 1750 50  0001 C CNN
F 3 "" H 6250 1750 50  0001 C CNN
	1    6250 1750
	-1   0    0    1   
$EndComp
Wire Wire Line
	4200 2600 4100 2600
Wire Wire Line
	4150 1900 5900 1900
$Comp
L power:+3.3V #PWR0105
U 1 1 5ECCE15F
P 4100 2600
F 0 "#PWR0105" H 4100 2450 50  0001 C CNN
F 1 "+3.3V" H 4115 2773 50  0000 C CNN
F 2 "" H 4100 2600 50  0001 C CNN
F 3 "" H 4100 2600 50  0001 C CNN
	1    4100 2600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3250 2950 3250 2650
Wire Wire Line
	3250 2650 3200 2650
Wire Wire Line
	4150 1600 4150 1900
Wire Wire Line
	3200 2200 3200 2250
Wire Wire Line
	5800 2600 6000 2600
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5EF65596
P 1900 1650
F 0 "#FLG0101" H 1900 1725 50  0001 C CNN
F 1 "PWR_FLAG" H 1900 1823 50  0000 C CNN
F 2 "" H 1900 1650 50  0001 C CNN
F 3 "~" H 1900 1650 50  0001 C CNN
	1    1900 1650
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR0104
U 1 1 5EF65FA5
P 1900 1650
F 0 "#PWR0104" H 1900 1400 50  0001 C CNN
F 1 "GNDD" H 1904 1495 50  0000 C CNN
F 2 "" H 1900 1650 50  0001 C CNN
F 3 "" H 1900 1650 50  0001 C CNN
	1    1900 1650
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5EF6694C
P 2450 1650
F 0 "#FLG0102" H 2450 1725 50  0001 C CNN
F 1 "PWR_FLAG" H 2450 1823 50  0000 C CNN
F 2 "" H 2450 1650 50  0001 C CNN
F 3 "~" H 2450 1650 50  0001 C CNN
	1    2450 1650
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0107
U 1 1 5EF685A5
P 2450 1650
F 0 "#PWR0107" H 2450 1500 50  0001 C CNN
F 1 "+3.3V" H 2465 1823 50  0000 C CNN
F 2 "" H 2450 1650 50  0001 C CNN
F 3 "" H 2450 1650 50  0001 C CNN
	1    2450 1650
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5EF6F75A
P 3000 1650
F 0 "#FLG0103" H 3000 1725 50  0001 C CNN
F 1 "PWR_FLAG" H 3000 1823 50  0000 C CNN
F 2 "" H 3000 1650 50  0001 C CNN
F 3 "~" H 3000 1650 50  0001 C CNN
	1    3000 1650
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 5EF70157
P 3000 1650
F 0 "#PWR0106" H 3000 1500 50  0001 C CNN
F 1 "+5V" H 3015 1823 50  0000 C CNN
F 2 "" H 3000 1650 50  0001 C CNN
F 3 "" H 3000 1650 50  0001 C CNN
	1    3000 1650
	1    0    0    1   
$EndComp
$Comp
L power:+5V #PWR0108
U 1 1 5EF70C69
P 4150 1600
F 0 "#PWR0108" H 4150 1450 50  0001 C CNN
F 1 "+5V" H 4165 1773 50  0000 C CNN
F 2 "" H 4150 1600 50  0001 C CNN
F 3 "" H 4150 1600 50  0001 C CNN
	1    4150 1600
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0103
U 1 1 5EFA0902
P 3200 2200
F 0 "#PWR0103" H 3200 2050 50  0001 C CNN
F 1 "+5V" H 3215 2373 50  0000 C CNN
F 2 "" H 3200 2200 50  0001 C CNN
F 3 "" H 3200 2200 50  0001 C CNN
	1    3200 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 2950 2550 2200
Wire Wire Line
	2550 2950 2750 2950
$Comp
L power:+3.3V #PWR0109
U 1 1 5EFBD48D
P 2550 2200
F 0 "#PWR0109" H 2550 2050 50  0001 C CNN
F 1 "+3.3V" H 2565 2373 50  0000 C CNN
F 2 "" H 2550 2200 50  0001 C CNN
F 3 "" H 2550 2200 50  0001 C CNN
	1    2550 2200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2450 3050 2450 2400
Wire Wire Line
	2450 3050 2750 3050
$Comp
L power:GNDD #PWR0110
U 1 1 5EFC4288
P 2450 2400
F 0 "#PWR0110" H 2450 2150 50  0001 C CNN
F 1 "GNDD" H 2454 2245 50  0000 C CNN
F 2 "" H 2450 2400 50  0001 C CNN
F 3 "" H 2450 2400 50  0001 C CNN
	1    2450 2400
	1    0    0    1   
$EndComp
Wire Wire Line
	6000 2600 6000 2500
Connection ~ 6000 2500
Wire Wire Line
	6000 2500 5900 2500
Wire Wire Line
	6250 2000 6250 2500
Wire Wire Line
	4450 3050 4450 2700
Wire Wire Line
	4450 2700 4800 2700
Wire Wire Line
	4800 2800 4500 2800
Wire Wire Line
	4500 2800 4500 3150
Wire Wire Line
	4800 2900 4550 2900
Wire Wire Line
	4550 2900 4550 3250
Wire Wire Line
	4600 3000 4600 3350
Wire Wire Line
	4600 3000 4800 3000
Wire Wire Line
	4800 3100 4650 3100
Wire Wire Line
	4650 3100 4650 3450
Wire Wire Line
	4800 3200 4700 3200
Wire Wire Line
	4700 3200 4700 3550
Wire Wire Line
	4800 3300 4750 3300
Wire Wire Line
	4750 3300 4750 3650
Wire Wire Line
	2150 4200 2150 3850
Wire Wire Line
	2150 3850 2200 3850
Wire Wire Line
	2100 4250 2100 3750
Wire Wire Line
	2100 3750 2200 3750
Wire Wire Line
	2050 4300 2050 3650
Wire Wire Line
	2050 3650 2200 3650
Wire Wire Line
	2000 4350 2000 3550
Wire Wire Line
	2000 3550 2200 3550
Wire Wire Line
	1950 4400 1950 3450
Wire Wire Line
	1950 3450 2200 3450
Wire Wire Line
	1900 4450 1900 3350
Wire Wire Line
	1900 3350 2200 3350
Wire Wire Line
	1850 4500 1850 3250
Wire Wire Line
	1850 3250 2200 3250
Wire Wire Line
	1800 4550 1800 3150
Wire Wire Line
	1800 3150 2200 3150
Wire Wire Line
	2400 3150 2750 3150
Wire Wire Line
	2400 3250 2750 3250
Wire Wire Line
	2400 3350 2750 3350
Wire Wire Line
	2400 3450 2750 3450
Wire Wire Line
	2400 3550 2750 3550
Wire Wire Line
	2400 3650 2750 3650
Wire Wire Line
	2400 3750 2750 3750
Wire Wire Line
	2400 3850 2750 3850
NoConn ~ 2650 3950
NoConn ~ 3350 3850
Wire Wire Line
	2650 3950 2750 3950
Wire Wire Line
	3350 3850 3250 3850
Wire Wire Line
	5900 4200 5900 2700
Wire Wire Line
	5900 2700 5800 2700
Wire Wire Line
	2150 4200 5900 4200
Wire Wire Line
	5950 4250 5950 2800
Wire Wire Line
	5950 2800 5800 2800
Wire Wire Line
	2100 4250 5950 4250
Wire Wire Line
	6000 4300 6000 2900
Wire Wire Line
	6000 2900 5800 2900
Wire Wire Line
	2050 4300 6000 4300
Wire Wire Line
	6050 4350 6050 3000
Wire Wire Line
	6050 3000 5800 3000
Wire Wire Line
	2000 4350 6050 4350
Wire Wire Line
	6100 4400 6100 3100
Wire Wire Line
	6100 3100 5800 3100
Wire Wire Line
	1950 4400 6100 4400
Wire Wire Line
	6150 4450 6150 3200
Wire Wire Line
	6150 3200 5800 3200
Wire Wire Line
	1900 4450 6150 4450
Wire Wire Line
	6200 4500 6200 3300
Wire Wire Line
	6200 3300 5800 3300
Wire Wire Line
	1850 4500 6200 4500
Wire Wire Line
	6250 4550 6250 3400
Wire Wire Line
	6250 3400 5800 3400
Wire Wire Line
	1800 4550 6250 4550
Wire Wire Line
	3250 3450 4650 3450
Wire Wire Line
	3250 3650 4750 3650
Wire Wire Line
	3250 3550 4700 3550
Wire Wire Line
	3250 3150 4500 3150
Wire Wire Line
	3250 3250 4550 3250
Wire Wire Line
	3250 3050 4450 3050
Wire Wire Line
	3250 3350 4600 3350
Wire Wire Line
	3250 3750 4800 3750
Wire Wire Line
	4800 3750 4800 3400
$Comp
L gir-rescue:LED_Small-Device D1
U 1 1 5E9ED0E1
P 3650 2250
F 0 "D1" H 3650 2045 50  0000 C CNN
F 1 "LED_Small" H 3650 2136 50  0000 C CNN
F 2 "library:LED_0805_2012Metric" V 3650 2250 50  0001 C CNN
F 3 "~" V 3650 2250 50  0001 C CNN
	1    3650 2250
	-1   0    0    1   
$EndComp
$Comp
L gir-rescue:R_Small-Device R14
U 1 1 5E9F5DD7
P 3450 2250
F 0 "R14" H 3509 2296 50  0000 L CNN
F 1 "330" H 3509 2205 50  0000 L CNN
F 2 "library:R_0805_2012Metric" H 3450 2250 50  0001 C CNN
F 3 "~" H 3450 2250 50  0001 C CNN
	1    3450 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	3350 2250 3200 2250
Connection ~ 3200 2250
Wire Wire Line
	3200 2250 3200 2650
$Comp
L power:GNDD #PWR0111
U 1 1 5EA00EF2
P 3750 2250
F 0 "#PWR0111" H 3750 2000 50  0001 C CNN
F 1 "GNDD" H 3754 2095 50  0000 C CNN
F 2 "" H 3750 2250 50  0001 C CNN
F 3 "" H 3750 2250 50  0001 C CNN
	1    3750 2250
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
