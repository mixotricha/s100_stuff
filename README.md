s100
General archive of S-100 stuff. Would like to attribute original author of OpenSCAD
for S-100 backplane if possible. Added archive for an S-100 prototype board based on
one of John Monahan's S100 Computer boards as an exercise in bringing over a V4 board
to V5. One thing I could not figure out is when you do a pcb update for some reason it
ignores the PWR_FLAGS on the supply. I overcame this by setting the appropriate pins
manually in the PCB to GND and VCC respectively. I've also not printed the board yet
so it needs a print run to be tested.