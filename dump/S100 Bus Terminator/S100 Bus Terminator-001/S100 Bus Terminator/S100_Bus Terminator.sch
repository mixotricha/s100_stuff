EESchema Schematic File Version 2  date 3/9/2013 6:50:47 PM
LIBS:power
LIBS:device
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:contrib
LIBS:valves
LIBS:N8VEM-KiCAD
LIBS:o_analog
LIBS:S100_Bus Terminator-cache
EELAYER 25  0
EELAYER END
$Descr C 22000 17000
encoding utf-8
Sheet 1 1
Title "noname.sch"
Date "10 feb 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 9150 5550 0    60   ~ 0
Card Ejector Mounting Holes
$Comp
L CONN_1 P34
U 1 1 4A0CC099
P 9900 6000
F 0 "P34" H 9980 6000 40  0000 C CNN
F 1 "CONN_1" H 9850 6040 30  0001 C CNN
F 2 "1pin" H 9850 6140 30  0001 C CNN
	1    9900 6000
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P35
U 1 1 4A0CC090
P 9900 5750
F 0 "P35" H 9980 5750 40  0000 C CNN
F 1 "CONN_1" H 9850 5790 30  0001 C CNN
F 2 "1pin" H 9850 5890 30  0001 C CNN
	1    9900 5750
	1    0    0    -1  
$EndComp
$Comp
L S100_MALE P1
U 1 1 4A06E881
P 14400 5850
F 0 "P1" H 14400 10900 60  0000 C CNN
F 1 "S100_MALE" V 14800 5850 60  0000 C CNN
F 2 "S100_MALE" V 14900 5850 60  0001 C CNN
	1    14400 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 7200 4700 7200
Wire Wire Line
	4700 7200 4700 6700
Wire Wire Line
	2700 2100 3450 2100
Wire Wire Line
	7200 2000 7950 2000
Wire Wire Line
	10200 6850 10200 7450
Wire Wire Line
	7900 6350 7600 6350
Connection ~ 3300 7850
Connection ~ 3900 6700
Connection ~ 4700 6700
Connection ~ 4300 7000
Connection ~ 9300 7850
Connection ~ 3800 7850
Connection ~ 6600 5500
Connection ~ 10200 7850
Connection ~ 10650 7850
Connection ~ 10650 6850
Connection ~ 10200 6850
Connection ~ 9750 6850
Connection ~ 9300 6850
Connection ~ 9750 7850
Connection ~ 8850 7850
Connection ~ 8500 7850
Connection ~ 6600 7850
Connection ~ 6150 7850
Connection ~ 5500 7850
Connection ~ 5200 7850
Wire Wire Line
	10650 6850 10650 7450
Wire Wire Line
	9750 6850 9750 7450
Wire Wire Line
	9300 6850 9300 7450
Connection ~ 8850 6850
Wire Wire Line
	8850 6850 8850 7450
Wire Wire Line
	8200 5000 8200 6150
Wire Wire Line
	7300 6000 6600 6000
Connection ~ 7600 7850
Wire Wire Line
	7600 7850 7600 7750
Wire Wire Line
	6150 7350 6150 7250
Connection ~ 5500 6950
Wire Wire Line
	6200 6750 5500 6750
Wire Wire Line
	5500 6750 5500 7450
Connection ~ 6600 5000
Wire Wire Line
	6600 5000 6600 5500
Connection ~ 7400 6850
Wire Wire Line
	7400 6850 7400 6150
Wire Wire Line
	7400 6150 6450 6150
Wire Wire Line
	6200 6950 5950 6950
Wire Wire Line
	5200 7350 5200 7200
Connection ~ 3800 5000
Wire Wire Line
	3800 5000 3800 7450
Wire Wire Line
	2550 7850 10650 7850
Wire Wire Line
	2550 5000 8200 5000
Wire Wire Line
	3300 5000 3300 7450
Connection ~ 4300 7850
Wire Wire Line
	3900 6700 3300 6700
Connection ~ 3300 6700
Wire Wire Line
	5950 6950 5950 6150
Wire Wire Line
	6600 6000 6600 6350
Wire Wire Line
	5350 6950 5500 6950
Wire Wire Line
	7200 6750 7300 6750
Wire Wire Line
	7300 6750 7300 7250
Wire Wire Line
	7300 7250 6150 7250
Wire Wire Line
	6600 7350 6950 7350
Wire Wire Line
	6950 7350 6950 7550
Wire Wire Line
	6950 7550 7300 7550
Wire Wire Line
	7600 6350 7600 6200
Wire Wire Line
	7600 5000 7600 5800
Connection ~ 7600 5000
Wire Wire Line
	8200 6550 8200 7150
Connection ~ 8200 6850
Wire Wire Line
	8200 7850 8200 7550
Connection ~ 8200 7850
Wire Wire Line
	8500 6850 8500 7350
Connection ~ 8500 6850
Connection ~ 3050 5000
Wire Wire Line
	4300 7850 4300 7000
Wire Wire Line
	7900 7350 7600 7350
Wire Wire Line
	5700 2800 6450 2800
Wire Wire Line
	5700 2700 6450 2700
Wire Wire Line
	5700 2600 6450 2600
Wire Wire Line
	5700 2500 6450 2500
Wire Wire Line
	5700 2400 6450 2400
Wire Wire Line
	5700 2300 6450 2300
Wire Wire Line
	5700 2200 6450 2200
Wire Wire Line
	5700 2100 6450 2100
Wire Wire Line
	4200 2900 4950 2900
Wire Wire Line
	4200 2800 4950 2800
Wire Wire Line
	4200 2700 4950 2700
Wire Wire Line
	4200 2600 4950 2600
Wire Wire Line
	4200 2500 4950 2500
Wire Wire Line
	4200 2400 4950 2400
Wire Wire Line
	4200 2300 4950 2300
Wire Wire Line
	4200 2200 4950 2200
Wire Wire Line
	4200 2100 4950 2100
Wire Wire Line
	2700 2900 3450 2900
Wire Wire Line
	2700 2800 3450 2800
Wire Wire Line
	2700 2700 3450 2700
Wire Wire Line
	2700 2600 3450 2600
Wire Wire Line
	2700 2500 3450 2500
Wire Wire Line
	2700 2400 3450 2400
Wire Wire Line
	2700 2300 3450 2300
Wire Wire Line
	2700 2200 3450 2200
Wire Wire Line
	10300 3850 10750 3850
Wire Wire Line
	10300 3750 10750 3750
Wire Wire Line
	8750 2900 9500 2900
Wire Wire Line
	8750 2800 9500 2800
Wire Wire Line
	8750 2700 9500 2700
Wire Wire Line
	8750 2600 9500 2600
Wire Wire Line
	8750 2500 9500 2500
Wire Wire Line
	8750 2400 9500 2400
Wire Wire Line
	8750 2300 9500 2300
Wire Wire Line
	8750 2200 9500 2200
Wire Wire Line
	8750 2100 9500 2100
Wire Wire Line
	7200 2900 7950 2900
Wire Wire Line
	7200 2800 7950 2800
Wire Wire Line
	7200 2700 7950 2700
Wire Wire Line
	7200 2600 7950 2600
Wire Wire Line
	7200 2500 7950 2500
Wire Wire Line
	7200 2400 7950 2400
Wire Wire Line
	7200 2300 7950 2300
Wire Wire Line
	7200 2200 7950 2200
Wire Wire Line
	7200 2100 7950 2100
Wire Wire Line
	5700 2900 6450 2900
Wire Wire Line
	10300 4050 10750 4050
Wire Wire Line
	10300 3950 10750 3950
Wire Wire Line
	8750 4150 9500 4150
Wire Wire Line
	8750 4050 9500 4050
Wire Wire Line
	8750 3950 9500 3950
Wire Wire Line
	8750 3850 9500 3850
Wire Wire Line
	8750 3750 9500 3750
Wire Wire Line
	8750 3650 9500 3650
Wire Wire Line
	8750 3550 9500 3550
Wire Wire Line
	8750 3450 9500 3450
Wire Wire Line
	8750 3350 9500 3350
Wire Wire Line
	7250 4150 8000 4150
Wire Wire Line
	7250 4050 8000 4050
Wire Wire Line
	7250 3950 8000 3950
Wire Wire Line
	7250 3850 8000 3850
Wire Wire Line
	7250 3750 8000 3750
Wire Wire Line
	7250 3650 8000 3650
Wire Wire Line
	7250 3550 8000 3550
Wire Wire Line
	7250 3450 8000 3450
Wire Wire Line
	7250 3350 8000 3350
Wire Wire Line
	4200 4150 4950 4150
Wire Wire Line
	4200 4050 4950 4050
Wire Wire Line
	4200 3950 4950 3950
Wire Wire Line
	4200 3850 4950 3850
Wire Wire Line
	4200 3750 4950 3750
Wire Wire Line
	4200 3650 4950 3650
Wire Wire Line
	4200 3550 4950 3550
Wire Wire Line
	4200 3450 4950 3450
Wire Wire Line
	4200 3350 4950 3350
Wire Wire Line
	2700 4150 3450 4150
Wire Wire Line
	2700 4050 3450 4050
Wire Wire Line
	2700 3950 3450 3950
Wire Wire Line
	2700 3850 3450 3850
Wire Wire Line
	2700 3750 3450 3750
Wire Wire Line
	2700 3650 3450 3650
Wire Wire Line
	2700 3550 3450 3550
Wire Wire Line
	2700 3450 3450 3450
Wire Wire Line
	2700 3350 3450 3350
Wire Wire Line
	5700 4150 6450 4150
Wire Wire Line
	5700 4050 6450 4050
Wire Wire Line
	5700 3950 6450 3950
Wire Wire Line
	5700 3850 6450 3850
Wire Wire Line
	5700 3750 6450 3750
Wire Wire Line
	5700 3650 6450 3650
Wire Wire Line
	5700 3550 6450 3550
Wire Wire Line
	5700 3450 6450 3450
Wire Wire Line
	5700 3350 6450 3350
Wire Wire Line
	2700 2000 3450 2000
Wire Wire Line
	4200 2000 4950 2000
Wire Wire Line
	5700 2000 6450 2000
Wire Wire Line
	8750 2000 9500 2000
Wire Wire Line
	2700 3250 3450 3250
Wire Wire Line
	4200 3250 4950 3250
Wire Wire Line
	5700 3250 6450 3250
Wire Wire Line
	7250 3250 8000 3250
Wire Wire Line
	8750 3250 9500 3250
Wire Wire Line
	7200 6850 10650 6850
Connection ~ 7600 6850
Connection ~ 7600 7350
Connection ~ 6600 7350
Connection ~ 6600 6000
Connection ~ 7600 6350
Text Label 4750 7200 0    60   ~ 0
VCC
$Comp
L PWR_FLAG #FLG01
U 1 1 511702B7
P 4700 6700
F 0 "#FLG01" H 4700 6970 30  0001 C CNN
F 1 "PWR_FLAG" H 4700 6930 30  0000 C CNN
	1    4700 6700
	1    0    0    -1  
$EndComp
Text Label 2750 2100 0    60   ~ 0
XRDY
Text Label 7250 2000 0    60   ~ 0
Vacc
Text Label 8800 3250 0    60   ~ 0
Vacc
Text Label 7300 3250 0    60   ~ 0
Vacc
Text Label 5750 3250 0    60   ~ 0
Vacc
Text Label 4250 3250 0    60   ~ 0
Vacc
Text Label 2750 3250 0    60   ~ 0
Vacc
Text Label 8800 2000 0    60   ~ 0
Vacc
Text Label 5750 2000 0    60   ~ 0
Vacc
Text Label 4250 2000 0    60   ~ 0
Vacc
Text Label 2750 2000 0    60   ~ 0
Vacc
Text Label 8300 6850 0    60   ~ 0
Vacc
$Comp
L RR9 RR10
U 1 1 51170421
P 9850 3750
F 0 "RR10" H 9900 4350 70  0000 C CNN
F 1 "270" V 9880 3750 70  0000 C CNN
F 2 "r_pack9" V 9980 3750 70  0001 C CNN
	1    9850 3750
	1    0    0    -1  
$EndComp
Text Label 5750 4150 0    60   ~ 0
A0
Text Label 5750 4050 0    60   ~ 0
PDBIN
Text Label 5750 3950 0    60   ~ 0
/PWR
Text Label 5750 3850 0    60   ~ 0
PSYNC
Text Label 5750 3750 0    60   ~ 0
/PRESET
Text Label 5750 3650 0    60   ~ 0
/PHOLD
Text Label 5750 3550 0    60   ~ 0
/PINT
Text Label 5750 3450 0    60   ~ 0
PRDY
Text Label 5750 3350 0    60   ~ 0
RUN
$Comp
L RR9 RR8
U 1 1 51170430
P 8350 3750
F 0 "RR8" H 8400 4350 70  0000 C CNN
F 1 "270" V 8380 3750 70  0000 C CNN
F 2 "r_pack9" V 8480 3750 70  0001 C CNN
	1    8350 3750
	1    0    0    -1  
$EndComp
$Comp
L RR9 RR6
U 1 1 51170436
P 6800 3750
F 0 "RR6" H 6850 4350 70  0000 C CNN
F 1 "270" V 6830 3750 70  0000 C CNN
F 2 "r_pack9" V 6930 3750 70  0001 C CNN
	1    6800 3750
	1    0    0    -1  
$EndComp
$Comp
L RR9 RR4
U 1 1 5117043C
P 5300 3750
F 0 "RR4" H 5350 4350 70  0000 C CNN
F 1 "270" V 5330 3750 70  0000 C CNN
F 2 "r_pack9" V 5430 3750 70  0001 C CNN
	1    5300 3750
	1    0    0    -1  
$EndComp
Text Label 10350 4050 0    60   ~ 0
/POC
Text Label 10350 3950 0    60   ~ 0
SSTACK
Text Label 8800 4150 0    60   ~ 0
/SWO
Text Label 8800 4050 0    60   ~ 0
SINTA
Text Label 8800 3950 0    60   ~ 0
DI0
Text Label 8800 3850 0    60   ~ 0
DI1
Text Label 8800 3750 0    60   ~ 0
DI6
Text Label 8800 3650 0    60   ~ 0
DI5
Text Label 8800 3550 0    60   ~ 0
DI4
Text Label 8800 3450 0    60   ~ 0
DO7
Text Label 8800 3350 0    60   ~ 0
DO3
Text Label 7300 4150 0    60   ~ 0
DO2
Text Label 7300 4050 0    60   ~ 0
A11
Text Label 7300 3950 0    60   ~ 0
A14
Text Label 7300 3850 0    60   ~ 0
A13
Text Label 7300 3750 0    60   ~ 0
A8
Text Label 7300 3650 0    60   ~ 0
A7
Text Label 7300 3550 0    60   ~ 0
A6
Text Label 7300 3450 0    60   ~ 0
A2
Text Label 7300 3350 0    60   ~ 0
A1
Text Label 4250 4150 0    60   ~ 0
PROT(GND)
Text Label 4250 4050 0    60   ~ 0
/PS(---)
Text Label 4250 3950 0    60   ~ 0
MWRT
Text Label 4250 3850 0    60   ~ 0
---(/PHANT)
Text Label 4250 3750 0    60   ~ 0
pin_66
Text Label 4250 3650 0    60   ~ 0
pin_65
Text Label 4250 3550 0    60   ~ 0
pin_64
Text Label 4250 3450 0    60   ~ 0
pin_63
Text Label 4250 3350 0    60   ~ 0
pin_62
Text Label 2750 4150 0    60   ~ 0
pin_61
Text Label 2750 4050 0    60   ~ 0
pin_60
Text Label 2750 3950 0    60   ~ 0
pin_59
Text Label 2750 3850 0    60   ~ 0
FRDY(---)
Text Label 2750 3750 0    60   ~ 0
DIG1(---)
Text Label 2750 3650 0    60   ~ 0
/STSTB(---)
Text Label 2750 3550 0    60   ~ 0
RTC(---)
Text Label 2750 3450 0    60   ~ 0
/EXTCLR
Text Label 2750 3350 0    60   ~ 0
/SSW_DSBL
$Comp
L RR9 RR2
U 1 1 51170468
P 3800 3750
F 0 "RR2" H 3850 4350 70  0000 C CNN
F 1 "270" V 3830 3750 70  0000 C CNN
F 2 "r_pack9" V 3930 3750 70  0001 C CNN
	1    3800 3750
	1    0    0    -1  
$EndComp
$Comp
L RR9 RR9
U 1 1 5117046E
P 9850 2500
F 0 "RR9" H 9900 3100 70  0000 C CNN
F 1 "270" V 9880 2500 70  0000 C CNN
F 2 "r_pack9" V 9980 2500 70  0001 C CNN
	1    9850 2500
	1    0    0    -1  
$EndComp
$Comp
L RR9 RR7
U 1 1 51170474
P 8300 2500
F 0 "RR7" H 8350 3100 70  0000 C CNN
F 1 "270" V 8330 2500 70  0000 C CNN
F 2 "r_pack9" V 8430 2500 70  0001 C CNN
	1    8300 2500
	1    0    0    -1  
$EndComp
Text Label 10350 3850 0    60   ~ 0
/CLOCK
Text Label 10350 3750 0    60   ~ 0
SHLTA
Text Label 8800 2900 0    60   ~ 0
SMEMR
Text Label 8800 2800 0    60   ~ 0
SINP
Text Label 8800 2700 0    60   ~ 0
SOUT
Text Label 8800 2600 0    60   ~ 0
SM1
Text Label 8800 2500 0    60   ~ 0
DI7
Text Label 8800 2400 0    60   ~ 0
DI3
Text Label 8800 2300 0    60   ~ 0
DI2
Text Label 8800 2200 0    60   ~ 0
DO6
Text Label 8800 2100 0    60   ~ 0
DO5
Text Label 7250 2900 0    60   ~ 0
DO4
Text Label 7250 2800 0    60   ~ 0
A10
Text Label 7250 2700 0    60   ~ 0
DO0
Text Label 7250 2600 0    60   ~ 0
DO1
Text Label 7250 2500 0    60   ~ 0
A9
Text Label 7250 2400 0    60   ~ 0
A12
Text Label 7250 2300 0    60   ~ 0
A15
Text Label 7250 2200 0    60   ~ 0
A3
Text Label 7250 2100 0    60   ~ 0
A4
Text Label 5750 2900 0    60   ~ 0
A5
Text Label 5750 2800 0    60   ~ 0
PINTE
Text Label 5750 2700 0    60   ~ 0
PWAIT
Text Label 5750 2600 0    60   ~ 0
PHLDA
Text Label 5750 2500 0    60   ~ 0
phi1
Text Label 5750 2400 0    60   ~ 0
phi2
Text Label 5750 2300 0    60   ~ 0
/DODSB
Text Label 5750 2200 0    60   ~ 0
/ADDDSB
Text Label 5750 2100 0    60   ~ 0
SS
Text Label 4250 2900 0    60   ~ 0
UNPROT(T5)
Text Label 4250 2800 0    60   ~ 0
/CCDSB
Text Label 4250 2700 0    60   ~ 0
/STADSB
Text Label 4250 2600 0    60   ~ 0
pin_17
Text Label 4250 2500 0    60   ~ 0
pin_16
Text Label 4250 2400 0    60   ~ 0
pin_15
Text Label 4250 2300 0    60   ~ 0
pin_14
Text Label 4250 2200 0    60   ~ 0
pin_13
Text Label 4250 2100 0    60   ~ 0
pin_12
Text Label 2750 2900 0    60   ~ 0
VI_7
Text Label 2750 2800 0    60   ~ 0
VI_6
Text Label 2750 2700 0    60   ~ 0
VI_5
Text Label 2750 2600 0    60   ~ 0
VI_4
Text Label 2750 2500 0    60   ~ 0
VI_3
Text Label 2750 2400 0    60   ~ 0
VI_2
Text Label 2750 2300 0    60   ~ 0
VI_1
Text Label 2750 2200 0    60   ~ 0
VI_0
$Comp
L RR9 RR5
U 1 1 511704A8
P 6800 2500
F 0 "RR5" H 6850 3100 70  0000 C CNN
F 1 "270" V 6830 2500 70  0000 C CNN
F 2 "r_pack9" V 6930 2500 70  0001 C CNN
	1    6800 2500
	1    0    0    -1  
$EndComp
$Comp
L RR9 RR3
U 1 1 511704AE
P 5300 2500
F 0 "RR3" H 5350 3100 70  0000 C CNN
F 1 "270" V 5330 2500 70  0000 C CNN
F 2 "r_pack9" V 5430 2500 70  0001 C CNN
	1    5300 2500
	1    0    0    -1  
$EndComp
$Comp
L RR9 RR1
U 1 1 511704B4
P 3800 2500
F 0 "RR1" H 3850 3100 70  0000 C CNN
F 1 "270" V 3830 2500 70  0000 C CNN
F 2 "r_pack9" V 3930 2500 70  0001 C CNN
	1    3800 2500
	1    0    0    -1  
$EndComp
$Comp
L TIP30 Q4
U 1 1 511704BA
P 8100 7350
F 0 "Q4" H 8250 7350 60  0000 C CNN
F 1 "TIP30" H 7900 7500 60  0000 C CNN
F 2 "TO220" H 7900 7250 60  0001 C CNN
	1    8100 7350
	1    0    0    1   
$EndComp
$Comp
L TIP29 Q3
U 1 1 511704C0
P 8100 6350
F 0 "Q3" H 8250 6350 50  0000 C CNN
F 1 "TIP29" H 7950 6500 50  0000 C CNN
F 2 "TO220" H 7900 6250 60  0001 C CNN
	1    8100 6350
	1    0    0    -1  
$EndComp
$Comp
L 7805 IC1
U 1 1 511704C6
P 4300 6750
F 0 "IC1" H 4450 6554 60  0000 C CNN
F 1 "7805" H 4300 6950 60  0000 C CNN
F 2 "TO220" H 4300 7050 60  0001 C CNN
	1    4300 6750
	1    0    0    -1  
$EndComp
Text Label 2600 7850 0    60   ~ 0
GND
$Comp
L CP C8
U 1 1 511705A7
P 10650 7650
F 0 "C8" H 10700 7750 50  0000 L CNN
F 1 "39 UF" H 10700 7550 50  0000 L CNN
F 2 "C1V7" H 10700 7650 50  0001 C CNN
	1    10650 7650
	1    0    0    -1  
$EndComp
$Comp
L CP C7
U 1 1 511705AD
P 10200 7650
F 0 "C7" H 10250 7750 50  0000 L CNN
F 1 "39 UF" H 10250 7550 50  0000 L CNN
F 2 "C1V7" H 10250 7650 50  0001 C CNN
	1    10200 7650
	1    0    0    -1  
$EndComp
$Comp
L CP C6
U 1 1 511705B3
P 9750 7650
F 0 "C6" H 9800 7750 50  0000 L CNN
F 1 "39 UF" H 9800 7550 50  0000 L CNN
F 2 "C1V7" H 9800 7650 50  0001 C CNN
	1    9750 7650
	1    0    0    -1  
$EndComp
$Comp
L CP C5
U 1 1 511705B9
P 9300 7650
F 0 "C5" H 9350 7750 50  0000 L CNN
F 1 "39 UF" H 9350 7550 50  0000 L CNN
F 2 "C1V7" H 9350 7650 50  0001 C CNN
	1    9300 7650
	1    0    0    -1  
$EndComp
$Comp
L CP C4
U 1 1 511705BF
P 8850 7650
F 0 "C4" H 8900 7750 50  0000 L CNN
F 1 "39 UF" H 8900 7550 50  0000 L CNN
F 2 "C1V7" H 8900 7650 50  0001 C CNN
	1    8850 7650
	1    0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 511705C5
P 8500 7600
F 0 "R10" V 8580 7600 50  0000 C CNN
F 1 "1000" V 8500 7600 50  0000 C CNN
F 2 "R3" V 8600 7600 50  0001 C CNN
	1    8500 7600
	1    0    0    -1  
$EndComp
$Comp
L PNP Q1
U 1 1 511705CB
P 7500 6000
F 0 "Q1" H 7650 6000 60  0000 C CNN
F 1 "2N3906" H 7250 5800 60  0000 C CNN
F 2 "TO92" H 7250 5900 60  0001 C CNN
	1    7500 6000
	1    0    0    1   
$EndComp
$Comp
L NPN Q2
U 1 1 511705D1
P 7500 7550
F 0 "Q2" H 7650 7550 50  0000 C CNN
F 1 "2N3904" H 7300 7400 50  0000 C CNN
F 2 "TO92-INVERT" H 7300 7500 50  0001 C CNN
	1    7500 7550
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 511705D7
P 7600 6600
F 0 "R8" V 7680 6600 50  0000 C CNN
F 1 "910" V 7600 6600 50  0000 C CNN
F 2 "R3" V 7700 6600 50  0001 C CNN
	1    7600 6600
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 511705DD
P 7600 7100
F 0 "R9" V 7680 7100 50  0000 C CNN
F 1 "910" V 7600 7100 50  0000 C CNN
F 2 "R3" V 7700 7100 50  0001 C CNN
	1    7600 7100
	1    0    0    -1  
$EndComp
NoConn ~ 7200 6650
NoConn ~ 7200 6950
$Comp
L R R4
U 1 1 511705E5
P 6150 7600
F 0 "R4" V 6230 7600 50  0000 C CNN
F 1 "150K" V 6150 7600 50  0000 C CNN
F 2 "R3" V 6250 7600 50  0001 C CNN
	1    6150 7600
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 511705EB
P 6600 7600
F 0 "R7" V 6680 7600 50  0000 C CNN
F 1 "560" V 6600 7600 50  0000 C CNN
F 2 "R3" V 6700 7600 50  0001 C CNN
	1    6600 7600
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 511705F1
P 6600 5750
F 0 "R6" V 6680 5750 50  0000 C CNN
F 1 "560" V 6600 5750 50  0000 C CNN
F 2 "R3" V 6700 5750 50  0001 C CNN
	1    6600 5750
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 511705F7
P 6200 6150
F 0 "R5" V 6280 6150 50  0000 C CNN
F 1 "1000" V 6200 6150 50  0000 C CNN
F 2 "R3" V 6300 6150 50  0001 C CNN
	1    6200 6150
	0    1    1    0   
$EndComp
$Comp
L LM4250 IC2
U 1 1 511705FE
P 6700 6850
F 0 "IC2" H 6820 7100 60  0000 C CNN
F 1 "LM4250" H 6820 6600 60  0000 C CNN
F 2 "8dip300" H 6820 6700 60  0001 C CNN
F 4 "386" H 6700 6850 60  0000 C CNN "Field8"
	1    6700 6850
	1    0    0    -1  
$EndComp
$Comp
L CP C3
U 1 1 51170604
P 5500 7650
F 0 "C3" H 5550 7750 50  0000 L CNN
F 1 "0.47 UF" H 5550 7550 50  0000 L CNN
F 2 "C1V7" H 5550 7650 50  0001 C CNN
	1    5500 7650
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5117060A
P 5200 7600
F 0 "R3" V 5280 7600 50  0000 C CNN
F 1 "1800" V 5200 7600 50  0000 C CNN
F 2 "R3" V 5300 7600 50  0001 C CNN
	1    5200 7600
	1    0    0    -1  
$EndComp
$Comp
L POT R2
U 1 1 51170610
P 5200 6950
F 0 "R2" H 5200 6850 50  0000 C CNN
F 1 "2000" H 5200 6950 50  0000 C CNN
F 2 "RV2" H 5200 7050 50  0001 C CNN
	1    5200 6950
	0    1    1    0   
$EndComp
$Comp
L R R1
U 1 1 51170616
P 4950 6700
F 0 "R1" V 5030 6700 50  0000 C CNN
F 1 "1800" V 4950 6700 50  0000 C CNN
F 2 "R3" V 5050 6700 50  0001 C CNN
	1    4950 6700
	0    1    1    0   
$EndComp
$Comp
L CP C2
U 1 1 5117061C
P 3800 7650
F 0 "C2" H 3850 7750 50  0000 L CNN
F 1 "39 UF" H 3850 7550 50  0000 L CNN
F 2 "C1V7" H 3850 7650 50  0001 C CNN
	1    3800 7650
	1    0    0    -1  
$EndComp
$Comp
L CP C1
U 1 1 51170622
P 3300 7650
F 0 "C1" H 3350 7750 50  0000 L CNN
F 1 "39 UF" H 3350 7550 50  0000 L CNN
F 2 "C1V7" H 3350 7650 50  0001 C CNN
	1    3300 7650
	1    0    0    -1  
$EndComp
Text Label 2600 5000 0    60   ~ 0
+8_V
$Comp
L PWR_FLAG #FLG02
U 1 1 5117087F
P 2850 7850
F 0 "#FLG02" H 2850 8120 30  0001 C CNN
F 1 "PWR_FLAG" H 2850 8080 30  0000 C CNN
	1    2850 7850
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG03
U 1 1 51170885
P 3050 5000
F 0 "#FLG03" H 3050 5270 30  0001 C CNN
F 1 "PWR_FLAG" H 3050 5230 30  0000 C CNN
	1    3050 5000
	1    0    0    -1  
$EndComp
Text Label 13050 900  0    60   ~ 0
+8_V
Text Label 13050 1000 0    60   ~ 0
+18_V
Text Label 13050 1100 0    60   ~ 0
XRDY
Text Label 13050 1200 0    60   ~ 0
VI_0
Text Label 13050 1300 0    60   ~ 0
VI_1
Text Label 13050 1400 0    60   ~ 0
VI_2
Text Label 13050 1500 0    60   ~ 0
VI_3
Text Label 13050 1600 0    60   ~ 0
VI_4
Text Label 13050 1700 0    60   ~ 0
VI_5
Text Label 13050 1800 0    60   ~ 0
VI_6
Text Label 13050 1900 0    60   ~ 0
VI_7
Text Label 13050 2000 0    60   ~ 0
pin_12
Text Label 13050 2100 0    60   ~ 0
pin_13
Text Label 13050 2200 0    60   ~ 0
pin_14
Text Label 13050 2300 0    60   ~ 0
pin_15
Text Label 13050 2400 0    60   ~ 0
pin_16
Text Label 13050 2500 0    60   ~ 0
pin_17
Text Label 13050 2600 0    60   ~ 0
/STADSB
Text Label 13050 2700 0    60   ~ 0
/CCDSB
Text Label 13050 2900 0    60   ~ 0
SS
Text Label 13050 3000 0    60   ~ 0
/ADDDSB
Text Label 13050 3100 0    60   ~ 0
/DODSB
Text Label 13050 3200 0    60   ~ 0
phi2
Text Label 13050 3300 0    60   ~ 0
phi1
Text Label 13050 3400 0    60   ~ 0
PHLDA
Text Label 13050 3500 0    60   ~ 0
PWAIT
Text Label 13050 3600 0    60   ~ 0
PINTE
Text Label 13050 3700 0    60   ~ 0
A5
Text Label 13050 3800 0    60   ~ 0
A4
Text Label 13050 3900 0    60   ~ 0
A3
Text Label 13050 4000 0    60   ~ 0
A15
Text Label 13050 4100 0    60   ~ 0
A12
Text Label 13050 4200 0    60   ~ 0
A9
Text Label 13050 4300 0    60   ~ 0
DO1
Text Label 13050 4400 0    60   ~ 0
DO0
Text Label 13050 4500 0    60   ~ 0
A10
Text Label 13050 4600 0    60   ~ 0
DO4
Text Label 13050 4700 0    60   ~ 0
DO5
Text Label 13050 4800 0    60   ~ 0
DO6
Text Label 13050 4900 0    60   ~ 0
DI2
Text Label 13050 5000 0    60   ~ 0
DI3
Text Label 13050 5100 0    60   ~ 0
DI7
Text Label 13050 5200 0    60   ~ 0
SM1
Text Label 13050 5300 0    60   ~ 0
SOUT
Text Label 13050 5400 0    60   ~ 0
SINP
Text Label 13050 5500 0    60   ~ 0
SMEMR
Text Label 13050 5600 0    60   ~ 0
SHLTA
Text Label 13050 5700 0    60   ~ 0
/CLOCK
Text Label 13050 5800 0    60   ~ 0
GND
Text Label 13050 5900 0    60   ~ 0
+8_V
Text Label 13050 6000 0    60   ~ 0
-16_V
Text Label 13050 6100 0    60   ~ 0
/SSW_DSBL
Text Label 13050 6200 0    60   ~ 0
/EXTCLR
Text Label 13050 6300 0    60   ~ 0
RTC(---)
Text Label 13050 6400 0    60   ~ 0
/STSTB(---)
Text Label 13050 6500 0    60   ~ 0
DIG1(---)
Text Label 13050 6600 0    60   ~ 0
FRDY(---)
Text Label 13050 6700 0    60   ~ 0
pin_59
Text Label 13050 6800 0    60   ~ 0
pin_60
Text Label 13050 6900 0    60   ~ 0
pin_61
Text Label 13050 7000 0    60   ~ 0
pin_62
Text Label 13050 7100 0    60   ~ 0
pin_63
Text Label 13050 7200 0    60   ~ 0
pin_64
Text Label 13050 7300 0    60   ~ 0
pin_65
Text Label 13050 7400 0    60   ~ 0
pin_66
Text Label 13050 7500 0    60   ~ 0
---(/PHANT)
Text Label 13050 7600 0    60   ~ 0
MWRT
Text Label 13050 7700 0    60   ~ 0
/PS(---)
Text Label 13050 7800 0    60   ~ 0
PROT(GND)
Text Label 13050 7900 0    60   ~ 0
RUN
Text Label 13050 8000 0    60   ~ 0
PRDY
Text Label 13050 8100 0    60   ~ 0
/PINT
Text Label 13050 8200 0    60   ~ 0
/PHOLD
Text Label 13050 8300 0    60   ~ 0
/PRESET
Text Label 13050 8400 0    60   ~ 0
PSYNC
Text Label 13050 8500 0    60   ~ 0
/PWR
Text Label 13050 8600 0    60   ~ 0
PDBIN
Text Label 13050 8700 0    60   ~ 0
A0
Text Label 13050 8800 0    60   ~ 0
A1
Text Label 13050 8900 0    60   ~ 0
A2
Text Label 13050 9000 0    60   ~ 0
A6
Text Label 13050 9100 0    60   ~ 0
A7
Text Label 13050 9200 0    60   ~ 0
A8
Text Label 13050 9300 0    60   ~ 0
A13
Text Label 13050 9400 0    60   ~ 0
A14
Text Label 13050 9500 0    60   ~ 0
A11
Text Label 13050 9600 0    60   ~ 0
DO2
Text Label 13050 9700 0    60   ~ 0
DO3
Text Label 13050 9800 0    60   ~ 0
DO7
Text Label 13050 9900 0    60   ~ 0
DI4
Text Label 13050 10000 0    60   ~ 0
DI5
Text Label 13050 10100 0    60   ~ 0
DI6
Text Label 13050 10200 0    60   ~ 0
DI1
Text Label 13050 10300 0    60   ~ 0
DI0
Text Label 13050 10400 0    60   ~ 0
SINTA
Text Label 13050 10500 0    60   ~ 0
/SWO
Text Label 13050 10600 0    60   ~ 0
SSTACK
Text Label 13050 10700 0    60   ~ 0
/POC
Text Label 13050 10800 0    60   ~ 0
GND
$Comp
L JUMPER JP1
U 1 1 511702A4
P 11650 1350
F 0 "JP1" H 11650 1500 60  0000 C CNN
F 1 "JUMPER" H 11650 1270 40  0000 C CNN
F 2 "SIL-2" H 11650 1370 40  0001 C CNN
	1    11650 1350
	1    0    0    -1  
$EndComp
$Comp
L JUMPER JP2
U 1 1 5117029E
P 11650 1700
F 0 "JP2" H 11650 1850 60  0000 C CNN
F 1 "JUMPER" H 11650 1620 40  0000 C CNN
F 2 "SIL-2" H 11650 1720 40  0001 C CNN
	1    11650 1700
	1    0    0    -1  
$EndComp
$Comp
L JUMPER JP3
U 1 1 51170298
P 11650 2050
F 0 "JP3" H 11650 2200 60  0000 C CNN
F 1 "JUMPER" H 11650 1970 40  0000 C CNN
F 2 "SIL-2" H 11650 2070 40  0001 C CNN
	1    11650 2050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 51170292
P 11350 2200
F 0 "#PWR04" H 11350 2200 30  0001 C CNN
F 1 "GND" H 11350 2130 30  0001 C CNN
	1    11350 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	12600 6100 13750 6100
Wire Wire Line
	12600 1700 12600 6100
Wire Wire Line
	11950 1700 12600 1700
Connection ~ 11350 1350
Connection ~ 11350 1700
Connection ~ 11350 2050
Wire Wire Line
	11350 1350 11350 2200
Wire Wire Line
	13000 900  13750 900 
Wire Wire Line
	13000 1000 13750 1000
Wire Wire Line
	13000 1100 13750 1100
Wire Wire Line
	13000 1200 13750 1200
Wire Wire Line
	13000 1300 13750 1300
Wire Wire Line
	13000 1400 13750 1400
Wire Wire Line
	13000 1500 13750 1500
Wire Wire Line
	13000 1600 13750 1600
Wire Wire Line
	13000 1700 13750 1700
Wire Wire Line
	13000 1800 13750 1800
Wire Wire Line
	13000 1900 13750 1900
Wire Wire Line
	13000 2000 13750 2000
Wire Wire Line
	13000 2100 13750 2100
Wire Wire Line
	13000 2200 13750 2200
Wire Wire Line
	13000 2300 13750 2300
Wire Wire Line
	13000 2400 13750 2400
Wire Wire Line
	13000 2500 13750 2500
Wire Wire Line
	13000 2600 13750 2600
Wire Wire Line
	13000 2700 13750 2700
Wire Wire Line
	13000 2900 13750 2900
Wire Wire Line
	13000 3000 13750 3000
Wire Wire Line
	13000 3100 13750 3100
Wire Wire Line
	13000 3200 13750 3200
Wire Wire Line
	13000 3300 13750 3300
Wire Wire Line
	13000 3400 13750 3400
Wire Wire Line
	13000 3500 13750 3500
Wire Wire Line
	13000 3600 13750 3600
Wire Wire Line
	13000 3700 13750 3700
Wire Wire Line
	13000 3800 13750 3800
Wire Wire Line
	13000 3900 13750 3900
Wire Wire Line
	13000 4000 13750 4000
Wire Wire Line
	13000 4100 13750 4100
Wire Wire Line
	13000 4200 13750 4200
Wire Wire Line
	13000 4300 13750 4300
Wire Wire Line
	13000 4400 13750 4400
Wire Wire Line
	13000 4500 13750 4500
Wire Wire Line
	13000 4600 13750 4600
Wire Wire Line
	13000 4700 13750 4700
Wire Wire Line
	13000 4800 13750 4800
Wire Wire Line
	13000 4900 13750 4900
Wire Wire Line
	13000 5000 13750 5000
Wire Wire Line
	13000 5100 13750 5100
Wire Wire Line
	13000 5200 13750 5200
Wire Wire Line
	13000 5300 13750 5300
Wire Wire Line
	13000 5400 13750 5400
Wire Wire Line
	13000 5500 13750 5500
Wire Wire Line
	13000 5600 13750 5600
Wire Wire Line
	13000 5700 13750 5700
Wire Wire Line
	13000 5800 13750 5800
Wire Wire Line
	13000 5900 13750 5900
Wire Wire Line
	13000 6000 13750 6000
Wire Wire Line
	13000 6200 13750 6200
Wire Wire Line
	13000 6300 13750 6300
Wire Wire Line
	13000 6400 13750 6400
Wire Wire Line
	13000 6500 13750 6500
Wire Wire Line
	13000 6600 13750 6600
Wire Wire Line
	13000 6700 13750 6700
Wire Wire Line
	13000 6800 13750 6800
Wire Wire Line
	13000 6900 13750 6900
Wire Wire Line
	13000 7000 13750 7000
Wire Wire Line
	13000 7100 13750 7100
Wire Wire Line
	13000 7200 13750 7200
Wire Wire Line
	13000 7300 13750 7300
Wire Wire Line
	13000 7400 13750 7400
Wire Wire Line
	13000 7500 13750 7500
Wire Wire Line
	13000 7600 13750 7600
Wire Wire Line
	13000 7700 13750 7700
Wire Wire Line
	13000 7900 13750 7900
Wire Wire Line
	13000 8000 13750 8000
Wire Wire Line
	13000 8200 13750 8200
Wire Wire Line
	13000 8300 13750 8300
Wire Wire Line
	13000 8400 13750 8400
Wire Wire Line
	13000 8500 13750 8500
Wire Wire Line
	13000 8600 13750 8600
Wire Wire Line
	13000 8700 13750 8700
Wire Wire Line
	13000 8800 13750 8800
Wire Wire Line
	13000 8900 13750 8900
Wire Wire Line
	13000 9000 13750 9000
Wire Wire Line
	13000 9100 13750 9100
Wire Wire Line
	13000 9200 13750 9200
Wire Wire Line
	13000 9300 13750 9300
Wire Wire Line
	13000 9400 13750 9400
Wire Wire Line
	13000 9500 13750 9500
Wire Wire Line
	13000 9600 13750 9600
Wire Wire Line
	13000 9700 13750 9700
Wire Wire Line
	13000 9800 13750 9800
Wire Wire Line
	13000 9900 13750 9900
Wire Wire Line
	13000 10000 13750 10000
Wire Wire Line
	13000 10100 13750 10100
Wire Wire Line
	13000 10200 13750 10200
Wire Wire Line
	13000 10300 13750 10300
Wire Wire Line
	13000 10400 13750 10400
Wire Wire Line
	13000 10500 13750 10500
Wire Wire Line
	13000 10600 13750 10600
Wire Wire Line
	13000 10700 13750 10700
Wire Wire Line
	13000 10800 13750 10800
Wire Wire Line
	12750 2800 13750 2800
Wire Wire Line
	12750 1350 12750 2800
Wire Wire Line
	11950 1350 12750 1350
Wire Wire Line
	12450 2050 11950 2050
Wire Wire Line
	13750 8100 13000 8100
Wire Wire Line
	12450 7800 13750 7800
Wire Wire Line
	12450 7800 12450 2050
NoConn ~ 13000 6000
Connection ~ 3300 5000
Connection ~ 2850 7850
NoConn ~ 13000 1000
Wire Wire Line
	9750 5750 9450 5750
Text Label 9500 5750 0    60   ~ 0
GND
Wire Wire Line
	9750 6000 9450 6000
Text Label 9500 6000 0    60   ~ 0
GND
Text Label 12800 2800 0    60   ~ 0
0_V1
Text Label 12650 6100 0    60   ~ 0
0_V3
Text Label 12500 7800 0    60   ~ 0
0_V4
$Comp
L CONN_6 RR11
U 1 1 51179FD9
P 11100 3800
F 0 "RR11" V 11050 3800 60  0000 C CNN
F 1 "270" V 11150 3800 60  0000 C CNN
F 2 "SIL-6" H 11100 3800 60  0001 C CNN
	1    11100 3800
	1    0    0    -1  
$EndComp
NoConn ~ 10750 3650
Wire Wire Line
	10300 3550 10750 3550
Text Label 10350 3550 0    60   ~ 0
Vacc
Text Label 13050 2800 0    60   ~ 0
UNPROT(T5)
Text Notes 4000 9200 0    60   ~ 0
Prototyping Board Grid Patterns
NoConn ~ 4850 9750
NoConn ~ 4850 9550
NoConn ~ 4450 9750
NoConn ~ 4850 9450
NoConn ~ 4850 9850
NoConn ~ 5250 9350
NoConn ~ 4850 9350
NoConn ~ 4450 9650
NoConn ~ 4450 9450
NoConn ~ 4850 9650
NoConn ~ 4450 9850
NoConn ~ 4450 9550
NoConn ~ 4450 9350
$Comp
L CONN_1 P14
U 1 1 5117DE23
P 5400 9350
F 0 "P14" H 5480 9350 40  0000 C CNN
F 1 "CONN_1" H 5350 9390 30  0001 C CNN
F 2 "PGA15X15" H 5350 9490 30  0001 C CNN
	1    5400 9350
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P13
U 1 1 5117DE2F
P 5000 9850
F 0 "P13" H 5080 9850 40  0000 C CNN
F 1 "CONN_1" H 4950 9890 30  0001 C CNN
F 2 "PGA15X15" H 4950 9990 30  0001 C CNN
	1    5000 9850
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P9
U 1 1 5117DE35
P 5000 9450
F 0 "P9" H 5080 9450 40  0000 C CNN
F 1 "CONN_1" H 4950 9490 30  0001 C CNN
F 2 "PGA15X15" H 4950 9590 30  0001 C CNN
	1    5000 9450
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P6
U 1 1 5117DE3B
P 4600 9750
F 0 "P6" H 4680 9750 40  0000 C CNN
F 1 "CONN_1" H 4550 9790 30  0001 C CNN
F 2 "PGA15X15" H 4550 9890 30  0001 C CNN
	1    4600 9750
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P10
U 1 1 5117DE41
P 5000 9550
F 0 "P10" H 5080 9550 40  0000 C CNN
F 1 "CONN_1" H 4950 9590 30  0001 C CNN
F 2 "PGA15X15" H 4950 9690 30  0001 C CNN
	1    5000 9550
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P12
U 1 1 5117DE53
P 5000 9750
F 0 "P12" H 5080 9750 40  0000 C CNN
F 1 "CONN_1" H 4950 9790 30  0001 C CNN
F 2 "PGA15X15" H 4950 9890 30  0001 C CNN
	1    5000 9750
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P8
U 1 1 5117DE59
P 5000 9350
F 0 "P8" H 5080 9350 40  0000 C CNN
F 1 "CONN_1" H 4950 9390 30  0001 C CNN
F 2 "PGA15X15" H 4950 9490 30  0001 C CNN
	1    5000 9350
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P5
U 1 1 5117DE5F
P 4600 9650
F 0 "P5" H 4680 9650 40  0000 C CNN
F 1 "CONN_1" H 4550 9690 30  0001 C CNN
F 2 "PGA15X15" H 4550 9790 30  0001 C CNN
	1    4600 9650
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P3
U 1 1 5117DE65
P 4600 9450
F 0 "P3" H 4680 9450 40  0000 C CNN
F 1 "CONN_1" H 4550 9490 30  0001 C CNN
F 2 "PGA15X15" H 4550 9590 30  0001 C CNN
	1    4600 9450
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P11
U 1 1 5117DE77
P 5000 9650
F 0 "P11" H 5080 9650 40  0000 C CNN
F 1 "CONN_1" H 4950 9690 30  0001 C CNN
F 2 "PGA15X15" H 4950 9790 30  0001 C CNN
	1    5000 9650
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P7
U 1 1 5117DE7D
P 4600 9850
F 0 "P7" H 4680 9850 40  0000 C CNN
F 1 "CONN_1" H 4550 9890 30  0001 C CNN
F 2 "PGA15X15" H 4550 9990 30  0001 C CNN
	1    4600 9850
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P4
U 1 1 5117DE83
P 4600 9550
F 0 "P4" H 4680 9550 40  0000 C CNN
F 1 "CONN_1" H 4550 9590 30  0001 C CNN
F 2 "PGA15X15" H 4550 9690 30  0001 C CNN
	1    4600 9550
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P2
U 1 1 5117DE89
P 4600 9350
F 0 "P2" H 4680 9350 40  0000 C CNN
F 1 "CONN_1" H 4550 9390 30  0001 C CNN
F 2 "PGA15X15" H 4550 9490 30  0001 C CNN
	1    4600 9350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
