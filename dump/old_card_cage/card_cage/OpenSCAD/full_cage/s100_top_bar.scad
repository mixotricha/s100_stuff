// This is the top side with card guides.
// Orientation:
// x = 0 : left edge of backplane PCB
// y = 0 : top of backplane
// z = 0 : outermost surface of card guide

include <s100_defs.scad>

$fn = 40;

bar_length = mod_width + 2*(cg_slot_gap+cg_depth);
echo(bar_length);

difference(){
  // Additions (guides, frame, lip, etc)
  union(){
    translate([0, -bar_height/2, 0]) cube(size=[bar_length,bar_height,bar_width], center=false);
  }
  // Through holes
  translate([cg_depth/2, 0, -1]) cylinder(h=bar_width+2, d=screw_clearance, center=false); 
  translate([bar_length-cg_depth/2, 0, -1]) cylinder(h=bar_width+2, d=screw_clearance, center=false); 

  // Shallow counterbores
  translate([cg_depth/2, 0, bar_width-screw_head_depth]) cylinder(h=screw_head_depth+1, d=screw_head_dia, center=false); 
  translate([bar_length-cg_depth/2, 0, bar_width-screw_head_depth]) cylinder(h=screw_head_depth+1, d=screw_head_dia, center=false); 
}

