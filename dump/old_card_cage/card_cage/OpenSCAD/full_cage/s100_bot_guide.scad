// This is the top side with card guides.
// Orientation:
// x = 0 : left edge of backplane PCB
// y = 0 : top of backplane
// z = 0 : outermost surface of card guide

include <s100_defs.scad>

$fn = 40;

lip_to_mod = mod_width/2 + mod_ofs - ((bp_hole_row3 - bp_hole_row2)/2 + bp_hole_row2) - edge[3];
lip_z = lip_to_mod + cg_slot_gap + cg_depth;
screw_cb = lip_z - attach_thick;

echo(lip_to_mod);

difference(){
  // Additions (guides, frame, lip, etc)
  union(){
    base_lip(lip_z, 1);
    card_guides(1);
    add_frame(lip_z, 1);
  }
  
  // Removals (holes + slot cutouts)
  mtg_holes(1);
  card_slots(1);
  attach_holes(1); 
  attach_cutout(1);
  support_holes(screw_threads, -1);
  top_chamfer(1);
}
