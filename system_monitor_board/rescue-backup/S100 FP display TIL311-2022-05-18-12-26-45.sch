EESchema Schematic File Version 2  date 12/11/2011 2:38:26 PM
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:diy_switches
LIBS:N8VEM-KiCAD
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title ""
Date "11 dec 2011"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 6850 3450 0    60   ~ 0
LED_VCC2
Text Label 3450 3450 0    60   ~ 0
LED_VCC1
Text Label 6900 5700 0    60   ~ 0
RUN_LED
Text Label 6900 5600 0    60   ~ 0
HD_LED
Text Label 6900 5500 0    60   ~ 0
SLAVE_CLR_SW
Text Label 6900 5400 0    60   ~ 0
VCC
Text Label 6900 5300 0    60   ~ 0
BP_SW
Text Label 6900 5200 0    60   ~ 0
RESET_SW
Text Label 6900 5100 0    60   ~ 0
SS2_SW
Text Label 6900 5000 0    60   ~ 0
SS1_SW
Text Label 6900 4900 0    60   ~ 0
GND
Text Label 6900 4800 0    60   ~ 0
STOP_SW
Text Label 6900 4700 0    60   ~ 0
MW_LED
Text Label 5600 5600 0    60   ~ 0
>64K_LED
Text Label 5600 5500 0    60   ~ 0
REQ_LED
Text Label 5600 5400 0    60   ~ 0
TMA_LED
Text Label 5600 5300 0    60   ~ 0
WAIT_LED
Text Label 5600 5200 0    60   ~ 0
PHA_LED
Text Label 5600 5100 0    60   ~ 0
HLTA_LED
Text Label 5600 5000 0    60   ~ 0
INTA_LED
Text Label 5600 4900 0    60   ~ 0
OUT_LED
Text Label 5600 4800 0    60   ~ 0
IN_LED
Text Label 5600 4700 0    60   ~ 0
MR_LED
$Comp
L CONN_13X2 P7
U 1 1 4C1E30B5
P 6450 5300
AR Path="/6FF405C44C1E30B5" Ref="P7"  Part="1" 
AR Path="/4C1E30B5" Ref="P7"  Part="1" 
AR Path="/7E44048F4C1E30B5" Ref="P7"  Part="1" 
AR Path="/23D0D4C4C1E30B5" Ref="P"  Part="1" 
AR Path="/AA1C4C1E30B5" Ref="P7"  Part="1" 
AR Path="/24C1E30B5" Ref="P7"  Part="1" 
AR Path="/23BED44C1E30B5" Ref="P7"  Part="1" 
AR Path="/2730CAC4C1E30B5" Ref="P7"  Part="1" 
AR Path="/24DB2AC4C1E30B5" Ref="P7"  Part="1" 
AR Path="/23C5984C1E30B5" Ref="P7"  Part="1" 
AR Path="/14C1E30B5" Ref="P7"  Part="1" 
F 0 "P7" H 6450 6000 60  0000 C CNN
F 1 "CONN_13X2" V 6450 5300 50  0000 C CNN
	1    6450 5300
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4C1E30B4
P 4850 5400
AR Path="/23D6404C1E30B4" Ref="D?"  Part="1" 
AR Path="/4C1E30B4" Ref="D1"  Part="1" 
AR Path="/23D33B4C1E30B4" Ref="D1"  Part="1" 
AR Path="/773F65F14C1E30B4" Ref="D1"  Part="1" 
AR Path="/383843454C1E30B4" Ref="D1"  Part="1" 
AR Path="/24C1E30B4" Ref="D1"  Part="1" 
AR Path="/23BED44C1E30B4" Ref="D1"  Part="1" 
AR Path="/2730CAC4C1E30B4" Ref="D1"  Part="1" 
AR Path="/24DB2AC4C1E30B4" Ref="D1"  Part="1" 
AR Path="/23C5984C1E30B4" Ref="D1"  Part="1" 
AR Path="/14C1E30B4" Ref="D1"  Part="1" 
F 0 "D1" H 4850 5500 50  0000 C CNN
F 1 "LED" H 4850 5300 50  0000 C CNN
	1    4850 5400
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR01
U 1 1 4C1E30B3
P 5100 4200
AR Path="/4C1E30B3" Ref="#PWR01"  Part="1" 
AR Path="/70F26E4C1E30B3" Ref="#PWR01"  Part="1" 
AR Path="/773F65F14C1E30B3" Ref="#PWR01"  Part="1" 
AR Path="/383843454C1E30B3" Ref="#PWR01"  Part="1" 
AR Path="/23BED44C1E30B3" Ref="#PWR01"  Part="1" 
AR Path="/2730CAC4C1E30B3" Ref="#PWR01"  Part="1" 
AR Path="/24DB2AC4C1E30B3" Ref="#PWR01"  Part="1" 
AR Path="/23C5984C1E30B3" Ref="#PWR01"  Part="1" 
AR Path="/14C1E30B3" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 5100 4300 30  0001 C CNN
F 1 "VCC" H 5100 4300 30  0000 C CNN
	1    5100 4200
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR02
U 1 1 4C1E30B2
P 4450 4350
AR Path="/4C1E30B2" Ref="#PWR02"  Part="1" 
AR Path="/70F26E4C1E30B2" Ref="#PWR02"  Part="1" 
AR Path="/773F65F14C1E30B2" Ref="#PWR02"  Part="1" 
AR Path="/383843454C1E30B2" Ref="#PWR02"  Part="1" 
AR Path="/23BED44C1E30B2" Ref="#PWR02"  Part="1" 
AR Path="/2730CAC4C1E30B2" Ref="#PWR02"  Part="1" 
AR Path="/24DB2AC4C1E30B2" Ref="#PWR02"  Part="1" 
AR Path="/23C5984C1E30B2" Ref="#PWR02"  Part="1" 
AR Path="/14C1E30B2" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 4450 4450 30  0001 C CNN
F 1 "VCC" H 4450 4450 30  0000 C CNN
	1    4450 4350
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4C1E30B1
P 5300 4200
AR Path="/23D6484C1E30B1" Ref="D?"  Part="1" 
AR Path="/4C1E30B1" Ref="D2"  Part="1" 
AR Path="/23D33B4C1E30B1" Ref="D2"  Part="1" 
AR Path="/773F65F14C1E30B1" Ref="D2"  Part="1" 
AR Path="/383843454C1E30B1" Ref="D2"  Part="1" 
AR Path="/24C1E30B1" Ref="D2"  Part="1" 
AR Path="/23BED44C1E30B1" Ref="D2"  Part="1" 
AR Path="/2730CAC4C1E30B1" Ref="D2"  Part="1" 
AR Path="/24DB2AC4C1E30B1" Ref="D2"  Part="1" 
AR Path="/23C5984C1E30B1" Ref="D2"  Part="1" 
AR Path="/14C1E30B1" Ref="D2"  Part="1" 
F 0 "D2" H 5300 4300 50  0000 C CNN
F 1 "LED" H 5300 4100 50  0000 C CNN
	1    5300 4200
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4C1E30B0
P 4650 4700
AR Path="/23D7D44C1E30B0" Ref="D?"  Part="1" 
AR Path="/4C1E30B0" Ref="D4"  Part="1" 
AR Path="/23D33B4C1E30B0" Ref="D4"  Part="1" 
AR Path="/773F65F14C1E30B0" Ref="D4"  Part="1" 
AR Path="/383843454C1E30B0" Ref="D4"  Part="1" 
AR Path="/24C1E30B0" Ref="D4"  Part="1" 
AR Path="/23BED44C1E30B0" Ref="D4"  Part="1" 
AR Path="/2730CAC4C1E30B0" Ref="D4"  Part="1" 
AR Path="/24DB2AC4C1E30B0" Ref="D4"  Part="1" 
AR Path="/23C5984C1E30B0" Ref="D4"  Part="1" 
AR Path="/14C1E30B0" Ref="D4"  Part="1" 
F 0 "D4" H 4650 4800 50  0000 C CNN
F 1 "LED" H 4650 4600 50  0000 C CNN
	1    4650 4700
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR03
U 1 1 4C1E30AF
P 4700 5000
AR Path="/4C1E30AF" Ref="#PWR03"  Part="1" 
AR Path="/70F26E4C1E30AF" Ref="#PWR03"  Part="1" 
AR Path="/773F65F14C1E30AF" Ref="#PWR03"  Part="1" 
AR Path="/383843454C1E30AF" Ref="#PWR03"  Part="1" 
AR Path="/23BED44C1E30AF" Ref="#PWR03"  Part="1" 
AR Path="/2730CAC4C1E30AF" Ref="#PWR03"  Part="1" 
AR Path="/24DB2AC4C1E30AF" Ref="#PWR03"  Part="1" 
AR Path="/23C5984C1E30AF" Ref="#PWR03"  Part="1" 
AR Path="/14C1E30AF" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 4700 5100 30  0001 C CNN
F 1 "VCC" H 4700 5100 30  0000 C CNN
	1    4700 5000
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR04
U 1 1 4C1E30AE
P 4900 4550
AR Path="/4C1E30AE" Ref="#PWR04"  Part="1" 
AR Path="/70F26E4C1E30AE" Ref="#PWR04"  Part="1" 
AR Path="/773F65F14C1E30AE" Ref="#PWR04"  Part="1" 
AR Path="/383843454C1E30AE" Ref="#PWR04"  Part="1" 
AR Path="/23BED44C1E30AE" Ref="#PWR04"  Part="1" 
AR Path="/2730CAC4C1E30AE" Ref="#PWR04"  Part="1" 
AR Path="/24DB2AC4C1E30AE" Ref="#PWR04"  Part="1" 
AR Path="/23C5984C1E30AE" Ref="#PWR04"  Part="1" 
AR Path="/14C1E30AE" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 4900 4650 30  0001 C CNN
F 1 "VCC" H 4900 4650 30  0000 C CNN
	1    4900 4550
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4C1E30AD
P 5100 4550
AR Path="/23D6484C1E30AD" Ref="D?"  Part="1" 
AR Path="/23D0B04C1E30AD" Ref="D3"  Part="1" 
AR Path="/23D33B4C1E30AD" Ref="D3"  Part="1" 
AR Path="/773F65F14C1E30AD" Ref="D3"  Part="1" 
AR Path="/383843454C1E30AD" Ref="D3"  Part="1" 
AR Path="/23C5984C1E30AD" Ref="D3"  Part="1" 
AR Path="/AA1C4C1E30AD" Ref="D3"  Part="1" 
AR Path="/24C1E30AD" Ref="D3"  Part="1" 
AR Path="/23BED44C1E30AD" Ref="D3"  Part="1" 
AR Path="/2730CAC4C1E30AD" Ref="D3"  Part="1" 
AR Path="/24DB2AC4C1E30AD" Ref="D3"  Part="1" 
AR Path="/6FF102AC4C1E30AD" Ref="D3"  Part="1" 
AR Path="/14C1E30AD" Ref="D3"  Part="1" 
AR Path="/FFFFFFFF4C1E30AD" Ref="D3"  Part="1" 
AR Path="/4C1E30AD" Ref="D3"  Part="1" 
F 0 "D3" H 5100 4650 50  0000 C CNN
F 1 "LED" H 5100 4450 50  0000 C CNN
	1    5100 4550
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4C1E30AC
P 10500 4800
AR Path="/23D6484C1E30AC" Ref="D?"  Part="1" 
AR Path="/4C1E30AC" Ref="D7"  Part="1" 
AR Path="/23D33B4C1E30AC" Ref="D7"  Part="1" 
AR Path="/773F65F14C1E30AC" Ref="D7"  Part="1" 
AR Path="/383843454C1E30AC" Ref="D7"  Part="1" 
AR Path="/24C1E30AC" Ref="D7"  Part="1" 
AR Path="/23BED44C1E30AC" Ref="D7"  Part="1" 
AR Path="/2730CAC4C1E30AC" Ref="D7"  Part="1" 
AR Path="/24DB2AC4C1E30AC" Ref="D7"  Part="1" 
AR Path="/23C5984C1E30AC" Ref="D7"  Part="1" 
AR Path="/14C1E30AC" Ref="D7"  Part="1" 
F 0 "D7" H 10500 4900 50  0000 C CNN
F 1 "LED" H 10500 4700 50  0000 C CNN
	1    10500 4800
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR05
U 1 1 4C1E30AB
P 5100 5300
AR Path="/4C1E30AB" Ref="#PWR05"  Part="1" 
AR Path="/70F26E4C1E30AB" Ref="#PWR05"  Part="1" 
AR Path="/773F65F14C1E30AB" Ref="#PWR05"  Part="1" 
AR Path="/383843454C1E30AB" Ref="#PWR05"  Part="1" 
AR Path="/23BED44C1E30AB" Ref="#PWR05"  Part="1" 
AR Path="/2730CAC4C1E30AB" Ref="#PWR05"  Part="1" 
AR Path="/24DB2AC4C1E30AB" Ref="#PWR05"  Part="1" 
AR Path="/23C5984C1E30AB" Ref="#PWR05"  Part="1" 
AR Path="/14C1E30AB" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 5100 5400 30  0001 C CNN
F 1 "VCC" H 5100 5400 30  0000 C CNN
	1    5100 5300
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR06
U 1 1 4C1E30AA
P 4650 5400
AR Path="/4C1E30AA" Ref="#PWR06"  Part="1" 
AR Path="/70F26E4C1E30AA" Ref="#PWR06"  Part="1" 
AR Path="/773F65F14C1E30AA" Ref="#PWR06"  Part="1" 
AR Path="/383843454C1E30AA" Ref="#PWR06"  Part="1" 
AR Path="/23BED44C1E30AA" Ref="#PWR06"  Part="1" 
AR Path="/2730CAC4C1E30AA" Ref="#PWR06"  Part="1" 
AR Path="/24DB2AC4C1E30AA" Ref="#PWR06"  Part="1" 
AR Path="/23C5984C1E30AA" Ref="#PWR06"  Part="1" 
AR Path="/14C1E30AA" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 4650 5500 30  0001 C CNN
F 1 "VCC" H 4650 5500 30  0000 C CNN
	1    4650 5400
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4C1E30A9
P 4350 5500
AR Path="/6FF405304C1E30A9" Ref="D?"  Part="1" 
AR Path="/4C1E30A9" Ref="D8"  Part="1" 
AR Path="/23D33B4C1E30A9" Ref="D8"  Part="1" 
AR Path="/773F65F14C1E30A9" Ref="D8"  Part="1" 
AR Path="/383843454C1E30A9" Ref="D8"  Part="1" 
AR Path="/24C1E30A9" Ref="D8"  Part="1" 
AR Path="/23BED44C1E30A9" Ref="D8"  Part="1" 
AR Path="/2730CAC4C1E30A9" Ref="D8"  Part="1" 
AR Path="/24DB2AC4C1E30A9" Ref="D8"  Part="1" 
AR Path="/23C5984C1E30A9" Ref="D8"  Part="1" 
AR Path="/14C1E30A9" Ref="D8"  Part="1" 
F 0 "D8" H 4350 5600 50  0000 C CNN
F 1 "LED" H 4350 5400 50  0000 C CNN
	1    4350 5500
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4C1E30A8
P 4850 5800
AR Path="/23D6484C1E30A8" Ref="D?"  Part="1" 
AR Path="/4C1E30A8" Ref="D6"  Part="1" 
AR Path="/23D33B4C1E30A8" Ref="D6"  Part="1" 
AR Path="/773F65F14C1E30A8" Ref="D6"  Part="1" 
AR Path="/383843454C1E30A8" Ref="D6"  Part="1" 
AR Path="/24C1E30A8" Ref="D6"  Part="1" 
AR Path="/23BED44C1E30A8" Ref="D6"  Part="1" 
AR Path="/2730CAC4C1E30A8" Ref="D6"  Part="1" 
AR Path="/24DB2AC4C1E30A8" Ref="D6"  Part="1" 
AR Path="/23C5984C1E30A8" Ref="D6"  Part="1" 
AR Path="/14C1E30A8" Ref="D6"  Part="1" 
F 0 "D6" H 4850 5900 50  0000 C CNN
F 1 "LED" H 4850 5700 50  0000 C CNN
	1    4850 5800
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR07
U 1 1 4C1E30A7
P 4150 5200
AR Path="/4C1E30A7" Ref="#PWR07"  Part="1" 
AR Path="/70F26E4C1E30A7" Ref="#PWR07"  Part="1" 
AR Path="/773F65F14C1E30A7" Ref="#PWR07"  Part="1" 
AR Path="/383843454C1E30A7" Ref="#PWR07"  Part="1" 
AR Path="/23BED44C1E30A7" Ref="#PWR07"  Part="1" 
AR Path="/2730CAC4C1E30A7" Ref="#PWR07"  Part="1" 
AR Path="/24DB2AC4C1E30A7" Ref="#PWR07"  Part="1" 
AR Path="/23C5984C1E30A7" Ref="#PWR07"  Part="1" 
AR Path="/14C1E30A7" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 4150 5300 30  0001 C CNN
F 1 "VCC" H 4150 5300 30  0000 C CNN
	1    4150 5200
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR08
U 1 1 4C1E30A6
P 4450 4700
AR Path="/4C1E30A6" Ref="#PWR08"  Part="1" 
AR Path="/70F26E4C1E30A6" Ref="#PWR08"  Part="1" 
AR Path="/773F65F14C1E30A6" Ref="#PWR08"  Part="1" 
AR Path="/383843454C1E30A6" Ref="#PWR08"  Part="1" 
AR Path="/23BED44C1E30A6" Ref="#PWR08"  Part="1" 
AR Path="/2730CAC4C1E30A6" Ref="#PWR08"  Part="1" 
AR Path="/24DB2AC4C1E30A6" Ref="#PWR08"  Part="1" 
AR Path="/23C5984C1E30A6" Ref="#PWR08"  Part="1" 
AR Path="/14C1E30A6" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 4450 4800 30  0001 C CNN
F 1 "VCC" H 4450 4800 30  0000 C CNN
	1    4450 4700
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4C1E30A5
P 4350 5200
AR Path="/23D6484C1E30A5" Ref="D?"  Part="1" 
AR Path="/4C1E30A5" Ref="D5"  Part="1" 
AR Path="/23D33B4C1E30A5" Ref="D5"  Part="1" 
AR Path="/773F65F14C1E30A5" Ref="D5"  Part="1" 
AR Path="/383843454C1E30A5" Ref="D5"  Part="1" 
AR Path="/24C1E30A5" Ref="D5"  Part="1" 
AR Path="/23BED44C1E30A5" Ref="D5"  Part="1" 
AR Path="/2730CAC4C1E30A5" Ref="D5"  Part="1" 
AR Path="/24DB2AC4C1E30A5" Ref="D5"  Part="1" 
AR Path="/23C5984C1E30A5" Ref="D5"  Part="1" 
AR Path="/14C1E30A5" Ref="D5"  Part="1" 
F 0 "D5" H 4350 5300 50  0000 C CNN
F 1 "LED" H 4350 5100 50  0000 C CNN
	1    4350 5200
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4C1E30A4
P 4650 4350
AR Path="/23D6484C1E30A4" Ref="D?"  Part="1" 
AR Path="/4C1E30A4" Ref="D10"  Part="1" 
AR Path="/23D33B4C1E30A4" Ref="D10"  Part="1" 
AR Path="/773F65F14C1E30A4" Ref="D10"  Part="1" 
AR Path="/383843454C1E30A4" Ref="D10"  Part="1" 
AR Path="/24C1E30A4" Ref="D10"  Part="1" 
AR Path="/23BED44C1E30A4" Ref="D10"  Part="1" 
AR Path="/2730CAC4C1E30A4" Ref="D10"  Part="1" 
AR Path="/24DB2AC4C1E30A4" Ref="D10"  Part="1" 
AR Path="/23C5984C1E30A4" Ref="D10"  Part="1" 
AR Path="/14C1E30A4" Ref="D10"  Part="1" 
F 0 "D10" H 4650 4450 50  0000 C CNN
F 1 "LED" H 4650 4250 50  0000 C CNN
	1    4650 4350
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR09
U 1 1 4C1E30A3
P 4150 5500
AR Path="/4C1E30A3" Ref="#PWR09"  Part="1" 
AR Path="/70F26E4C1E30A3" Ref="#PWR09"  Part="1" 
AR Path="/773F65F14C1E30A3" Ref="#PWR09"  Part="1" 
AR Path="/383843454C1E30A3" Ref="#PWR09"  Part="1" 
AR Path="/23BED44C1E30A3" Ref="#PWR09"  Part="1" 
AR Path="/2730CAC4C1E30A3" Ref="#PWR09"  Part="1" 
AR Path="/24DB2AC4C1E30A3" Ref="#PWR09"  Part="1" 
AR Path="/23C5984C1E30A3" Ref="#PWR09"  Part="1" 
AR Path="/14C1E30A3" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 4150 5600 30  0001 C CNN
F 1 "VCC" H 4150 5600 30  0000 C CNN
	1    4150 5500
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4C1E30A2
P 7900 4700
AR Path="/23D6484C1E30A2" Ref="D?"  Part="1" 
AR Path="/6FF405304C1E30A2" Ref="D?"  Part="1" 
AR Path="/4C1E30A2" Ref="D9"  Part="1" 
AR Path="/23D33B4C1E30A2" Ref="D9"  Part="1" 
AR Path="/773F65F14C1E30A2" Ref="D9"  Part="1" 
AR Path="/383843454C1E30A2" Ref="D9"  Part="1" 
AR Path="/24C1E30A2" Ref="D9"  Part="1" 
AR Path="/23BED44C1E30A2" Ref="D9"  Part="1" 
AR Path="/2730CAC4C1E30A2" Ref="D9"  Part="1" 
AR Path="/24DB2AC4C1E30A2" Ref="D9"  Part="1" 
AR Path="/23C5984C1E30A2" Ref="D9"  Part="1" 
AR Path="/14C1E30A2" Ref="D9"  Part="1" 
F 0 "D9" H 7900 4800 50  0000 C CNN
F 1 "LED" H 7900 4600 50  0000 C CNN
	1    7900 4700
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR010
U 1 1 4C1E30A1
P 4650 5800
AR Path="/4C1E30A1" Ref="#PWR010"  Part="1" 
AR Path="/70F26E4C1E30A1" Ref="#PWR010"  Part="1" 
AR Path="/773F65F14C1E30A1" Ref="#PWR010"  Part="1" 
AR Path="/383843454C1E30A1" Ref="#PWR010"  Part="1" 
AR Path="/23BED44C1E30A1" Ref="#PWR010"  Part="1" 
AR Path="/2730CAC4C1E30A1" Ref="#PWR010"  Part="1" 
AR Path="/24DB2AC4C1E30A1" Ref="#PWR010"  Part="1" 
AR Path="/23C5984C1E30A1" Ref="#PWR010"  Part="1" 
AR Path="/14C1E30A1" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 4650 5900 30  0001 C CNN
F 1 "VCC" H 4650 5900 30  0000 C CNN
	1    4650 5800
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4C1E30A0
P 4900 5000
AR Path="/23D6484C1E30A0" Ref="D?"  Part="1" 
AR Path="/4C1E30A0" Ref="D14"  Part="1" 
AR Path="/23D33B4C1E30A0" Ref="D14"  Part="1" 
AR Path="/773F65F14C1E30A0" Ref="D14"  Part="1" 
AR Path="/383843454C1E30A0" Ref="D14"  Part="1" 
AR Path="/24C1E30A0" Ref="D14"  Part="1" 
AR Path="/23BED44C1E30A0" Ref="D14"  Part="1" 
AR Path="/2730CAC4C1E30A0" Ref="D14"  Part="1" 
AR Path="/24DB2AC4C1E30A0" Ref="D14"  Part="1" 
AR Path="/23C5984C1E30A0" Ref="D14"  Part="1" 
AR Path="/14C1E30A0" Ref="D14"  Part="1" 
F 0 "D14" H 4900 5100 50  0000 C CNN
F 1 "LED" H 4900 4900 50  0000 C CNN
	1    4900 5000
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR011
U 1 1 4C1E309F
P 8100 4700
AR Path="/4C1E309F" Ref="#PWR011"  Part="1" 
AR Path="/70F26E4C1E309F" Ref="#PWR011"  Part="1" 
AR Path="/773F65F14C1E309F" Ref="#PWR011"  Part="1" 
AR Path="/383843454C1E309F" Ref="#PWR011"  Part="1" 
AR Path="/23BED44C1E309F" Ref="#PWR011"  Part="1" 
AR Path="/2730CAC4C1E309F" Ref="#PWR011"  Part="1" 
AR Path="/24DB2AC4C1E309F" Ref="#PWR011"  Part="1" 
AR Path="/23C5984C1E309F" Ref="#PWR011"  Part="1" 
AR Path="/14C1E309F" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 8100 4800 30  0001 C CNN
F 1 "VCC" H 8100 4800 30  0000 C CNN
	1    8100 4700
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR012
U 1 1 4C1E309E
P 8100 5600
AR Path="/4C1E309E" Ref="#PWR012"  Part="1" 
AR Path="/70F26E4C1E309E" Ref="#PWR012"  Part="1" 
AR Path="/773F65F14C1E309E" Ref="#PWR012"  Part="1" 
AR Path="/383843454C1E309E" Ref="#PWR012"  Part="1" 
AR Path="/23BED44C1E309E" Ref="#PWR012"  Part="1" 
AR Path="/2730CAC4C1E309E" Ref="#PWR012"  Part="1" 
AR Path="/24DB2AC4C1E309E" Ref="#PWR012"  Part="1" 
AR Path="/23C5984C1E309E" Ref="#PWR012"  Part="1" 
AR Path="/14C1E309E" Ref="#PWR012"  Part="1" 
F 0 "#PWR012" H 8100 5700 30  0001 C CNN
F 1 "VCC" H 8100 5700 30  0000 C CNN
	1    8100 5600
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4C1E309D
P 5300 5300
AR Path="/23D6484C1E309D" Ref="D?"  Part="1" 
AR Path="/4C1E309D" Ref="D15"  Part="1" 
AR Path="/23D33B4C1E309D" Ref="D15"  Part="1" 
AR Path="/773F65F14C1E309D" Ref="D15"  Part="1" 
AR Path="/383843454C1E309D" Ref="D15"  Part="1" 
AR Path="/24C1E309D" Ref="D15"  Part="1" 
AR Path="/23BED44C1E309D" Ref="D15"  Part="1" 
AR Path="/2730CAC4C1E309D" Ref="D15"  Part="1" 
AR Path="/24DB2AC4C1E309D" Ref="D15"  Part="1" 
AR Path="/23C5984C1E309D" Ref="D15"  Part="1" 
AR Path="/14C1E309D" Ref="D15"  Part="1" 
F 0 "D15" H 5300 5400 50  0000 C CNN
F 1 "LED" H 5300 5200 50  0000 C CNN
	1    5300 5300
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4C1E309C
P 7900 5600
AR Path="/23D6484C1E309C" Ref="D?"  Part="1" 
AR Path="/4C1E309C" Ref="D16"  Part="1" 
AR Path="/23D33B4C1E309C" Ref="D16"  Part="1" 
AR Path="/773F65F14C1E309C" Ref="D16"  Part="1" 
AR Path="/383843454C1E309C" Ref="D16"  Part="1" 
AR Path="/24C1E309C" Ref="D16"  Part="1" 
AR Path="/23BED44C1E309C" Ref="D16"  Part="1" 
AR Path="/2730CAC4C1E309C" Ref="D16"  Part="1" 
AR Path="/24DB2AC4C1E309C" Ref="D16"  Part="1" 
AR Path="/23C5984C1E309C" Ref="D16"  Part="1" 
AR Path="/14C1E309C" Ref="D16"  Part="1" 
F 0 "D16" H 7900 5700 50  0000 C CNN
F 1 "LED" H 7900 5500 50  0000 C CNN
	1    7900 5600
	-1   0    0    1   
$EndComp
$Comp
L SPDT SW?
U 1 1 4C1E309B
P 9700 5000
AR Path="/23D6484C1E309B" Ref="SW?"  Part="1" 
AR Path="/23D0B04C1E309B" Ref="SW5"  Part="1" 
AR Path="/23D33B4C1E309B" Ref="SW5"  Part="1" 
AR Path="/773F65F14C1E309B" Ref="SW5"  Part="1" 
AR Path="/383843454C1E309B" Ref="SW5"  Part="1" 
AR Path="/23C5984C1E309B" Ref="SW5"  Part="1" 
AR Path="/AA1C4C1E309B" Ref="SW5"  Part="1" 
AR Path="/24C1E309B" Ref="SW5"  Part="1" 
AR Path="/23BED44C1E309B" Ref="SW5"  Part="1" 
AR Path="/2730CAC4C1E309B" Ref="SW5"  Part="1" 
AR Path="/24DB2AC4C1E309B" Ref="SW5"  Part="1" 
AR Path="/6FF102AC4C1E309B" Ref="SW5"  Part="1" 
AR Path="/14C1E309B" Ref="SW5"  Part="1" 
AR Path="/FFFFFFFF4C1E309B" Ref="SW5"  Part="1" 
AR Path="/4C1E309B" Ref="SW5"  Part="1" 
F 0 "SW5" H 9700 5100 50  0000 C CNN
F 1 "SPDT" H 9700 4850 50  0000 C CNN
	1    9700 5000
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR013
U 1 1 4C1E309A
P 9900 5050
AR Path="/4C1E309A" Ref="#PWR013"  Part="1" 
AR Path="/70F26E4C1E309A" Ref="#PWR013"  Part="1" 
AR Path="/773F65F14C1E309A" Ref="#PWR013"  Part="1" 
AR Path="/383843454C1E309A" Ref="#PWR013"  Part="1" 
AR Path="/23BED44C1E309A" Ref="#PWR013"  Part="1" 
AR Path="/2730CAC4C1E309A" Ref="#PWR013"  Part="1" 
AR Path="/24DB2AC4C1E309A" Ref="#PWR013"  Part="1" 
AR Path="/23C5984C1E309A" Ref="#PWR013"  Part="1" 
AR Path="/14C1E309A" Ref="#PWR013"  Part="1" 
F 0 "#PWR013" H 9900 5050 30  0001 C CNN
F 1 "GND" H 9900 4980 30  0001 C CNN
	1    9900 5050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 4C1E3099
P 9900 5300
AR Path="/4C1E3099" Ref="#PWR014"  Part="1" 
AR Path="/70F26E4C1E3099" Ref="#PWR014"  Part="1" 
AR Path="/773F65F14C1E3099" Ref="#PWR014"  Part="1" 
AR Path="/383843454C1E3099" Ref="#PWR014"  Part="1" 
AR Path="/23BED44C1E3099" Ref="#PWR014"  Part="1" 
AR Path="/2730CAC4C1E3099" Ref="#PWR014"  Part="1" 
AR Path="/24DB2AC4C1E3099" Ref="#PWR014"  Part="1" 
AR Path="/23C5984C1E3099" Ref="#PWR014"  Part="1" 
AR Path="/14C1E3099" Ref="#PWR014"  Part="1" 
F 0 "#PWR014" H 9900 5300 30  0001 C CNN
F 1 "GND" H 9900 5230 30  0001 C CNN
	1    9900 5300
	1    0    0    -1  
$EndComp
$Comp
L SPST SW?
U 1 1 4C1E3098
P 9400 5250
AR Path="/23D7D44C1E3098" Ref="SW?"  Part="1" 
AR Path="/23D0B04C1E3098" Ref="SW2"  Part="1" 
AR Path="/23D33B4C1E3098" Ref="SW2"  Part="1" 
AR Path="/773F65F14C1E3098" Ref="SW2"  Part="1" 
AR Path="/383843454C1E3098" Ref="SW2"  Part="1" 
AR Path="/23C5984C1E3098" Ref="SW2"  Part="1" 
AR Path="/AA1C4C1E3098" Ref="SW2"  Part="1" 
AR Path="/24C1E3098" Ref="SW2"  Part="1" 
AR Path="/23BED44C1E3098" Ref="SW2"  Part="1" 
AR Path="/2730CAC4C1E3098" Ref="SW2"  Part="1" 
AR Path="/24DB2AC4C1E3098" Ref="SW2"  Part="1" 
AR Path="/6FF102AC4C1E3098" Ref="SW2"  Part="1" 
AR Path="/14C1E3098" Ref="SW2"  Part="1" 
AR Path="/FFFFFFFF4C1E3098" Ref="SW2"  Part="1" 
AR Path="/4C1E3098" Ref="SW2"  Part="1" 
F 0 "SW2" H 9400 5350 70  0000 C CNN
F 1 "SPST" H 9400 5150 70  0000 C CNN
	1    9400 5250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 4C1E3097
P 9300 5750
AR Path="/4C1E3097" Ref="#PWR015"  Part="1" 
AR Path="/70F26E4C1E3097" Ref="#PWR015"  Part="1" 
AR Path="/773F65F14C1E3097" Ref="#PWR015"  Part="1" 
AR Path="/383843454C1E3097" Ref="#PWR015"  Part="1" 
AR Path="/23BED44C1E3097" Ref="#PWR015"  Part="1" 
AR Path="/2730CAC4C1E3097" Ref="#PWR015"  Part="1" 
AR Path="/24DB2AC4C1E3097" Ref="#PWR015"  Part="1" 
AR Path="/23C5984C1E3097" Ref="#PWR015"  Part="1" 
AR Path="/14C1E3097" Ref="#PWR015"  Part="1" 
F 0 "#PWR015" H 9300 5750 30  0001 C CNN
F 1 "GND" H 9300 5680 30  0001 C CNN
	1    9300 5750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 4C1E3096
P 9900 6100
AR Path="/4C1E3096" Ref="#PWR016"  Part="1" 
AR Path="/70F26E4C1E3096" Ref="#PWR016"  Part="1" 
AR Path="/773F65F14C1E3096" Ref="#PWR016"  Part="1" 
AR Path="/383843454C1E3096" Ref="#PWR016"  Part="1" 
AR Path="/23BED44C1E3096" Ref="#PWR016"  Part="1" 
AR Path="/2730CAC4C1E3096" Ref="#PWR016"  Part="1" 
AR Path="/24DB2AC4C1E3096" Ref="#PWR016"  Part="1" 
AR Path="/23C5984C1E3096" Ref="#PWR016"  Part="1" 
AR Path="/14C1E3096" Ref="#PWR016"  Part="1" 
F 0 "#PWR016" H 9900 6100 30  0001 C CNN
F 1 "GND" H 9900 6030 30  0001 C CNN
	1    9900 6100
	1    0    0    -1  
$EndComp
$Comp
L SPST SW?
U 1 1 4C1E3095
P 9400 6050
AR Path="/23D6484C1E3095" Ref="SW?"  Part="1" 
AR Path="/23D7D44C1E3095" Ref="SW?"  Part="1" 
AR Path="/23D0B04C1E3095" Ref="SW4"  Part="1" 
AR Path="/23D33B4C1E3095" Ref="SW4"  Part="1" 
AR Path="/773F65F14C1E3095" Ref="SW4"  Part="1" 
AR Path="/383843454C1E3095" Ref="SW4"  Part="1" 
AR Path="/23C5984C1E3095" Ref="SW4"  Part="1" 
AR Path="/AA1C4C1E3095" Ref="SW4"  Part="1" 
AR Path="/24C1E3095" Ref="SW4"  Part="1" 
AR Path="/23BED44C1E3095" Ref="SW4"  Part="1" 
AR Path="/2730CAC4C1E3095" Ref="SW4"  Part="1" 
AR Path="/24DB2AC4C1E3095" Ref="SW4"  Part="1" 
AR Path="/6FF102AC4C1E3095" Ref="SW4"  Part="1" 
AR Path="/14C1E3095" Ref="SW4"  Part="1" 
AR Path="/FFFFFFFF4C1E3095" Ref="SW4"  Part="1" 
AR Path="/4C1E3095" Ref="SW4"  Part="1" 
F 0 "SW4" H 9400 6150 70  0000 C CNN
F 1 "SPST" H 9400 5950 70  0000 C CNN
	1    9400 6050
	1    0    0    -1  
$EndComp
$Comp
L SPDT SW?
U 1 1 4C1E3094
P 9100 5650
AR Path="/23D7D44C1E3094" Ref="SW?"  Part="1" 
AR Path="/23D0B04C1E3094" Ref="SW3"  Part="1" 
AR Path="/23D33B4C1E3094" Ref="SW3"  Part="1" 
AR Path="/6FF405C44C1E3094" Ref="SW?"  Part="1" 
AR Path="/773F65F14C1E3094" Ref="SW3"  Part="1" 
AR Path="/383843454C1E3094" Ref="SW3"  Part="1" 
AR Path="/23C5984C1E3094" Ref="SW3"  Part="1" 
AR Path="/AA1C4C1E3094" Ref="SW3"  Part="1" 
AR Path="/24C1E3094" Ref="SW3"  Part="1" 
AR Path="/23BED44C1E3094" Ref="SW3"  Part="1" 
AR Path="/2730CAC4C1E3094" Ref="SW3"  Part="1" 
AR Path="/24DB2AC4C1E3094" Ref="SW3"  Part="1" 
AR Path="/6FF102AC4C1E3094" Ref="SW3"  Part="1" 
AR Path="/14C1E3094" Ref="SW3"  Part="1" 
AR Path="/FFFFFFFF4C1E3094" Ref="SW3"  Part="1" 
AR Path="/4C1E3094" Ref="SW3"  Part="1" 
F 0 "SW3" H 9100 5750 50  0000 C CNN
F 1 "SPDT" H 9100 5500 50  0000 C CNN
	1    9100 5650
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR017
U 1 1 4C1E3093
P 9300 5600
AR Path="/4C1E3093" Ref="#PWR017"  Part="1" 
AR Path="/70F26E4C1E3093" Ref="#PWR017"  Part="1" 
AR Path="/773F65F14C1E3093" Ref="#PWR017"  Part="1" 
AR Path="/383843454C1E3093" Ref="#PWR017"  Part="1" 
AR Path="/23BED44C1E3093" Ref="#PWR017"  Part="1" 
AR Path="/2730CAC4C1E3093" Ref="#PWR017"  Part="1" 
AR Path="/24DB2AC4C1E3093" Ref="#PWR017"  Part="1" 
AR Path="/23C5984C1E3093" Ref="#PWR017"  Part="1" 
AR Path="/14C1E3093" Ref="#PWR017"  Part="1" 
F 0 "#PWR017" H 9300 5700 30  0001 C CNN
F 1 "VCC" H 9300 5700 30  0000 C CNN
	1    9300 5600
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR018
U 1 1 4C1E3092
P 9300 4500
AR Path="/4C1E3092" Ref="#PWR018"  Part="1" 
AR Path="/70F26E4C1E3092" Ref="#PWR018"  Part="1" 
AR Path="/773F65F14C1E3092" Ref="#PWR018"  Part="1" 
AR Path="/383843454C1E3092" Ref="#PWR018"  Part="1" 
AR Path="/23BED44C1E3092" Ref="#PWR018"  Part="1" 
AR Path="/2730CAC4C1E3092" Ref="#PWR018"  Part="1" 
AR Path="/24DB2AC4C1E3092" Ref="#PWR018"  Part="1" 
AR Path="/23C5984C1E3092" Ref="#PWR018"  Part="1" 
AR Path="/14C1E3092" Ref="#PWR018"  Part="1" 
F 0 "#PWR018" H 9300 4600 30  0001 C CNN
F 1 "VCC" H 9300 4600 30  0000 C CNN
	1    9300 4500
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR019
U 1 1 4C1E3091
P 10700 4800
AR Path="/4C1E3091" Ref="#PWR019"  Part="1" 
AR Path="/70F26E4C1E3091" Ref="#PWR019"  Part="1" 
AR Path="/773F65F14C1E3091" Ref="#PWR019"  Part="1" 
AR Path="/383843454C1E3091" Ref="#PWR019"  Part="1" 
AR Path="/23BED44C1E3091" Ref="#PWR019"  Part="1" 
AR Path="/2730CAC4C1E3091" Ref="#PWR019"  Part="1" 
AR Path="/24DB2AC4C1E3091" Ref="#PWR019"  Part="1" 
AR Path="/23C5984C1E3091" Ref="#PWR019"  Part="1" 
AR Path="/14C1E3091" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 10700 4900 30  0001 C CNN
F 1 "VCC" H 10700 4900 30  0000 C CNN
	1    10700 4800
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR020
U 1 1 4C1E3090
P 9300 4100
AR Path="/4C1E3090" Ref="#PWR020"  Part="1" 
AR Path="/70F26E4C1E3090" Ref="#PWR020"  Part="1" 
AR Path="/773F65F14C1E3090" Ref="#PWR020"  Part="1" 
AR Path="/383843454C1E3090" Ref="#PWR020"  Part="1" 
AR Path="/23BED44C1E3090" Ref="#PWR020"  Part="1" 
AR Path="/2730CAC4C1E3090" Ref="#PWR020"  Part="1" 
AR Path="/24DB2AC4C1E3090" Ref="#PWR020"  Part="1" 
AR Path="/23C5984C1E3090" Ref="#PWR020"  Part="1" 
AR Path="/14C1E3090" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 9300 4200 30  0001 C CNN
F 1 "VCC" H 9300 4200 30  0000 C CNN
	1    9300 4100
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 4C1E308F
P 8650 4150
AR Path="/6FF405304C1E308F" Ref="R?"  Part="1" 
AR Path="/4C1E308F" Ref="R1"  Part="1" 
AR Path="/23D33B4C1E308F" Ref="R1"  Part="1" 
AR Path="/773F65F14C1E308F" Ref="R1"  Part="1" 
AR Path="/383843454C1E308F" Ref="R1"  Part="1" 
AR Path="/24C1E308F" Ref="R1"  Part="1" 
AR Path="/23BED44C1E308F" Ref="R1"  Part="1" 
AR Path="/2730CAC4C1E308F" Ref="R1"  Part="1" 
AR Path="/24DB2AC4C1E308F" Ref="R1"  Part="1" 
AR Path="/23C5984C1E308F" Ref="R1"  Part="1" 
AR Path="/14C1E308F" Ref="R1"  Part="1" 
F 0 "R1" V 8730 4150 50  0000 C CNN
F 1 "470" V 8650 4150 50  0000 C CNN
	1    8650 4150
	0    1    1    0   
$EndComp
$Comp
L LED D?
U 1 1 4C1E308E
P 8200 4150
AR Path="/23D6484C1E308E" Ref="D?"  Part="1" 
AR Path="/4C1E308E" Ref="D17"  Part="1" 
AR Path="/23D33B4C1E308E" Ref="D17"  Part="1" 
AR Path="/773F65F14C1E308E" Ref="D17"  Part="1" 
AR Path="/383843454C1E308E" Ref="D22"  Part="1" 
AR Path="/24C1E308E" Ref="D22"  Part="1" 
AR Path="/23BED44C1E308E" Ref="D22"  Part="1" 
AR Path="/2730CAC4C1E308E" Ref="D22"  Part="1" 
AR Path="/24DB2AC4C1E308E" Ref="D22"  Part="1" 
AR Path="/23D0B04C1E308E" Ref="D17"  Part="1" 
AR Path="/23C5984C1E308E" Ref="D22"  Part="1" 
AR Path="/14C1E308E" Ref="D17"  Part="1" 
AR Path="/7E44048F4C1E308E" Ref="D17"  Part="1" 
AR Path="/16B0DBA4C1E308E" Ref="D"  Part="1" 
F 0 "D17" H 8200 4250 50  0000 C CNN
F 1 "LED" H 8200 4050 50  0000 C CNN
	1    8200 4150
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR021
U 1 1 4C1E308D
P 8000 4250
AR Path="/4C1E308D" Ref="#PWR021"  Part="1" 
AR Path="/70F26E4C1E308D" Ref="#PWR021"  Part="1" 
AR Path="/773F65F14C1E308D" Ref="#PWR021"  Part="1" 
AR Path="/383843454C1E308D" Ref="#PWR021"  Part="1" 
AR Path="/23BED44C1E308D" Ref="#PWR021"  Part="1" 
AR Path="/24DB2AC4C1E308D" Ref="#PWR021"  Part="1" 
AR Path="/23C5984C1E308D" Ref="#PWR021"  Part="1" 
AR Path="/14C1E308D" Ref="#PWR021"  Part="1" 
F 0 "#PWR021" H 8000 4250 30  0001 C CNN
F 1 "GND" H 8000 4180 30  0001 C CNN
	1    8000 4250
	1    0    0    -1  
$EndComp
$Comp
L DPDT SW6
U 1 1 4C1E308C
P 9100 4150
AR Path="/4C1E308C" Ref="SW6"  Part="1" 
AR Path="/23D33B4C1E308C" Ref="SW6"  Part="1" 
AR Path="/773F65F14C1E308C" Ref="SW6"  Part="1" 
AR Path="/383843454C1E308C" Ref="SW6"  Part="1" 
AR Path="/24C1E308C" Ref="SW6"  Part="1" 
AR Path="/23BED44C1E308C" Ref="SW6"  Part="1" 
AR Path="/24DB2AC4C1E308C" Ref="SW6"  Part="1" 
AR Path="/23C5984C1E308C" Ref="SW6"  Part="1" 
AR Path="/14C1E308C" Ref="SW6"  Part="1" 
F 0 "SW6" H 9100 4250 50  0000 C CNN
F 1 "DPDT" H 9100 4000 50  0000 C CNN
	1    9100 4150
	1    0    0    -1  
$EndComp
$Comp
L DPDT SW?
U 2 1 4C1E308B
P 9100 4550
AR Path="/23D6484C1E308B" Ref="SW?"  Part="1" 
AR Path="/4C1E308B" Ref="SW6"  Part="2" 
AR Path="/23D33B4C1E308B" Ref="SW6"  Part="1" 
AR Path="/453143344C1E308B" Ref="SW"  Part="2" 
AR Path="/773F65F14C1E308B" Ref="SW6"  Part="2" 
AR Path="/383843454C1E308B" Ref="SW6"  Part="2" 
AR Path="/23BED44C1E308B" Ref="SW6"  Part="2" 
AR Path="/24DB2AC4C1E308B" Ref="SW6"  Part="2" 
AR Path="/23C5984C1E308B" Ref="SW6"  Part="2" 
AR Path="/14C1E308B" Ref="SW6"  Part="2" 
F 0 "SW6" H 9100 4650 50  0000 C CNN
F 1 "DPDT" H 9100 4400 50  0000 C CNN
	2    9100 4550
	1    0    0    -1  
$EndComp
Text Label 5550 5700 0    60   ~ 0
VCC
Text Label 5550 5800 0    60   ~ 0
VCC
Text Label 5550 5900 0    60   ~ 0
VCC
Text Label 6900 5800 0    60   ~ 0
GND
Text Label 6900 5900 0    60   ~ 0
GND
$Comp
L R R?
U 1 1 4C1E308A
P 10050 4800
AR Path="/23D6484C1E308A" Ref="R?"  Part="1" 
AR Path="/4C1E308A" Ref="R2"  Part="1" 
AR Path="/23D33B4C1E308A" Ref="R2"  Part="1" 
AR Path="/773F65F14C1E308A" Ref="R2"  Part="1" 
AR Path="/383843454C1E308A" Ref="R2"  Part="1" 
AR Path="/24C1E308A" Ref="R2"  Part="1" 
AR Path="/23BED44C1E308A" Ref="R2"  Part="1" 
AR Path="/24DB2AC4C1E308A" Ref="R2"  Part="1" 
AR Path="/23C5984C1E308A" Ref="R2"  Part="1" 
AR Path="/14C1E308A" Ref="R2"  Part="1" 
F 0 "R2" V 10130 4800 50  0000 C CNN
F 1 "470" V 10050 4800 50  0000 C CNN
	1    10050 4800
	0    1    1    0   
$EndComp
NoConn ~ 7700 5700
Text Label 9350 4200 0    60   ~ 0
GND
Text Label 9350 4600 0    60   ~ 0
GND
Wire Wire Line
	4550 5500 6050 5500
Wire Wire Line
	6050 5100 5100 5100
Wire Wire Line
	5100 5100 5100 5000
Wire Wire Line
	6050 4900 5300 4900
Wire Wire Line
	5300 4900 5300 4550
Wire Wire Line
	5500 4200 5500 4700
Wire Wire Line
	5050 5400 6050 5400
Wire Wire Line
	6050 5000 5200 5000
Wire Wire Line
	8900 6050 8450 6050
Wire Wire Line
	8450 6050 8450 5400
Wire Wire Line
	8450 5400 7800 5400
Wire Wire Line
	7800 5400 7800 5500
Wire Wire Line
	7800 5500 6850 5500
Wire Wire Line
	9300 5700 9300 5750
Wire Wire Line
	8900 5250 8700 5250
Wire Wire Line
	8700 5250 8700 5200
Wire Wire Line
	8700 5200 6850 5200
Wire Wire Line
	6850 5000 8550 5000
Wire Wire Line
	8550 5000 8550 4950
Wire Wire Line
	8550 4950 9500 4950
Wire Wire Line
	8200 4550 8900 4550
Wire Wire Line
	8200 4550 8200 4800
Wire Wire Line
	8200 4800 6850 4800
Wire Wire Line
	5500 4700 6050 4700
Wire Wire Line
	6050 5300 5500 5300
Wire Wire Line
	6050 5700 5500 5700
Wire Wire Line
	6050 5900 5500 5900
Wire Wire Line
	6050 5800 5500 5800
Wire Wire Line
	7700 5800 6850 5800
Wire Wire Line
	7700 5900 6850 5900
Wire Wire Line
	7700 5700 6850 5700
Wire Wire Line
	7700 5600 6850 5600
Wire Wire Line
	7700 5400 6850 5400
Wire Wire Line
	7700 4900 6850 4900
Wire Wire Line
	7700 4700 6850 4700
Wire Wire Line
	9900 5000 9900 5050
Wire Wire Line
	6850 5100 8700 5100
Wire Wire Line
	8700 5100 8700 5050
Wire Wire Line
	8700 5050 9500 5050
Wire Wire Line
	9900 5250 9900 5300
Wire Wire Line
	6850 5300 8550 5300
Wire Wire Line
	8550 5300 8550 5650
Wire Wire Line
	8550 5650 8900 5650
Wire Wire Line
	9900 6050 9900 6100
Wire Wire Line
	6050 4800 5400 4800
Wire Wire Line
	6050 5600 5050 5600
Wire Wire Line
	8000 4150 8000 4250
Wire Wire Line
	9800 4800 8900 4800
Wire Wire Line
	8900 4800 8900 4550
Connection ~ 8900 4550
Wire Wire Line
	9300 4200 9600 4200
Wire Wire Line
	9300 4600 9600 4600
Wire Wire Line
	4850 4350 5400 4350
Wire Wire Line
	5400 4350 5400 4800
Wire Wire Line
	4850 4700 5200 4700
Wire Wire Line
	5200 4700 5200 5000
Wire Wire Line
	5050 5600 5050 5800
Wire Wire Line
	4550 5200 6050 5200
NoConn ~ 2700 2650
Wire Wire Line
	2700 2650 1900 2650
Wire Wire Line
	7450 1800 7450 3800
Wire Wire Line
	7450 3800 6800 3800
Connection ~ 8850 3450
Wire Wire Line
	8850 3450 8850 1800
Connection ~ 7450 3450
Wire Wire Line
	10400 3450 10800 3450
Connection ~ 5350 3450
Wire Wire Line
	5350 1800 5350 3450
Connection ~ 3950 3450
Wire Wire Line
	3950 3450 3950 1800
Wire Wire Line
	6050 3450 3250 3450
Wire Wire Line
	2700 2950 1900 2950
Connection ~ 1500 5100
Connection ~ 1500 4750
Wire Wire Line
	1600 5100 800  5100
Wire Wire Line
	1600 4750 800  4750
Wire Wire Line
	2250 2550 1900 2550
Wire Wire Line
	2700 2750 1900 2750
Wire Wire Line
	2700 2850 1900 2850
Wire Wire Line
	2250 2450 1900 2450
Wire Wire Line
	1900 2250 2250 2250
Wire Wire Line
	1900 3050 2250 3050
Wire Wire Line
	750  3050 1100 3050
Wire Wire Line
	750  1050 1100 1050
Wire Wire Line
	750  1150 1100 1150
Wire Wire Line
	750  1350 1100 1350
Wire Wire Line
	750  1250 1100 1250
Wire Wire Line
	750  850  1100 850 
Wire Wire Line
	750  950  1100 950 
Wire Wire Line
	750  750  1100 750 
Wire Wire Line
	750  650  1100 650 
Wire Wire Line
	750  1450 1100 1450
Wire Wire Line
	750  1550 1100 1550
Wire Wire Line
	750  1750 1100 1750
Wire Wire Line
	750  1650 1100 1650
Wire Wire Line
	750  2050 1100 2050
Wire Wire Line
	750  2150 1100 2150
Wire Wire Line
	750  1950 1100 1950
Wire Wire Line
	750  1850 1100 1850
Wire Wire Line
	750  2250 1100 2250
Wire Wire Line
	750  2350 1100 2350
Wire Wire Line
	750  2550 1100 2550
Wire Wire Line
	750  2450 1100 2450
Wire Wire Line
	750  2850 1100 2850
Wire Wire Line
	750  2950 1100 2950
Wire Wire Line
	750  2750 1100 2750
Wire Wire Line
	750  2650 1100 2650
Wire Wire Line
	1900 1050 2250 1050
Wire Wire Line
	1900 1150 2250 1150
Wire Wire Line
	1900 1350 2250 1350
Wire Wire Line
	1900 1250 2250 1250
Wire Wire Line
	1900 850  2250 850 
Wire Wire Line
	1900 950  2250 950 
Wire Wire Line
	1900 750  2250 750 
Wire Wire Line
	1900 650  2250 650 
Wire Wire Line
	1900 1450 2250 1450
Wire Wire Line
	1900 1550 2250 1550
Wire Wire Line
	1900 1750 2250 1750
Wire Wire Line
	1900 1650 2250 1650
Wire Wire Line
	1900 2050 2250 2050
Wire Wire Line
	1900 2150 2250 2150
Wire Wire Line
	1900 1950 2250 1950
Wire Wire Line
	1900 1850 2250 1850
Wire Wire Line
	750  3150 1100 3150
Wire Wire Line
	1900 3150 2250 3150
Wire Wire Line
	2250 2350 1900 2350
Wire Wire Line
	1100 3800 1100 4150
Connection ~ 1100 3900
Connection ~ 1100 3800
Connection ~ 1100 4000
Connection ~ 1100 4100
Wire Wire Line
	3500 2450 3500 3200
Wire Wire Line
	3600 2450 3600 3200
Wire Wire Line
	3700 2450 3700 3200
Wire Wire Line
	3400 2450 3400 3200
Wire Wire Line
	3400 850  3400 1250
Wire Wire Line
	3700 850  3700 1250
Wire Wire Line
	3600 850  3600 1250
Wire Wire Line
	3500 850  3500 1250
Wire Wire Line
	4200 850  4200 1250
Wire Wire Line
	4300 850  4300 1250
Wire Wire Line
	4400 850  4400 1250
Wire Wire Line
	4100 850  4100 1250
Wire Wire Line
	4800 850  4800 1250
Wire Wire Line
	5100 850  5100 1250
Wire Wire Line
	5000 850  5000 1250
Wire Wire Line
	4900 850  4900 1250
Wire Wire Line
	5600 850  5600 1250
Wire Wire Line
	5700 850  5700 1250
Wire Wire Line
	5800 850  5800 1250
Wire Wire Line
	5500 850  5500 1250
Wire Wire Line
	9700 850  9700 1250
Wire Wire Line
	10000 850  10000 1250
Wire Wire Line
	9900 850  9900 1250
Wire Wire Line
	9800 850  9800 1250
Wire Wire Line
	9100 850  9100 1250
Wire Wire Line
	9200 850  9200 1250
Wire Wire Line
	9300 850  9300 1250
Wire Wire Line
	9000 850  9000 1250
Wire Wire Line
	6900 850  6900 1250
Wire Wire Line
	7200 850  7200 1250
Wire Wire Line
	7100 850  7100 1250
Wire Wire Line
	7000 850  7000 1250
Wire Wire Line
	6300 850  6300 1250
Wire Wire Line
	6400 850  6400 1250
Wire Wire Line
	6500 850  6500 1250
Wire Wire Line
	6200 850  6200 1250
Wire Wire Line
	7600 850  7600 1250
Wire Wire Line
	7900 850  7900 1250
Wire Wire Line
	7800 850  7800 1250
Wire Wire Line
	7700 850  7700 1250
Wire Wire Line
	8400 850  8400 1250
Wire Wire Line
	8500 850  8500 1250
Wire Wire Line
	8600 850  8600 1250
Wire Wire Line
	8300 850  8300 1250
Wire Wire Line
	4100 2450 4100 3200
Wire Wire Line
	4400 2450 4400 3200
Wire Wire Line
	4300 2450 4300 3200
Wire Wire Line
	4200 2450 4200 3200
Wire Wire Line
	5600 2450 5600 3200
Wire Wire Line
	5700 2450 5700 3200
Wire Wire Line
	5800 2450 5800 3200
Wire Wire Line
	5500 2450 5500 3200
Wire Wire Line
	4800 2450 4800 3200
Wire Wire Line
	5100 2450 5100 3200
Wire Wire Line
	5000 2450 5000 3200
Wire Wire Line
	4900 2450 4900 3200
Wire Wire Line
	7700 2450 7700 3200
Wire Wire Line
	7800 2450 7800 3200
Wire Wire Line
	7900 2450 7900 3200
Wire Wire Line
	7600 2450 7600 3200
Wire Wire Line
	8300 2450 8300 3200
Wire Wire Line
	8600 2450 8600 3200
Wire Wire Line
	8500 2450 8500 3200
Wire Wire Line
	8400 2450 8400 3200
Wire Wire Line
	7000 2450 7000 3200
Wire Wire Line
	7100 2450 7100 3200
Wire Wire Line
	7200 2450 7200 3200
Wire Wire Line
	6900 2450 6900 3200
Wire Wire Line
	6200 2450 6200 3200
Wire Wire Line
	6500 2450 6500 3200
Wire Wire Line
	6400 2450 6400 3200
Wire Wire Line
	6300 2450 6300 3200
Wire Wire Line
	9800 2450 9800 3200
Wire Wire Line
	9900 2450 9900 3200
Wire Wire Line
	10000 2450 10000 3200
Wire Wire Line
	9700 2450 9700 3200
Wire Wire Line
	9000 2450 9000 3200
Wire Wire Line
	9300 2450 9300 3200
Wire Wire Line
	9200 2450 9200 3200
Wire Wire Line
	9100 2450 9100 3200
Wire Wire Line
	2450 3450 2850 3450
Wire Wire Line
	3250 3450 3250 1800
Wire Wire Line
	4650 3450 4650 1800
Connection ~ 4650 3450
Wire Wire Line
	10000 3450 6750 3450
Wire Wire Line
	6750 3450 6750 1800
Wire Wire Line
	8150 3450 8150 1800
Connection ~ 8150 3450
Wire Wire Line
	9550 3450 9550 1800
Connection ~ 9550 3450
Connection ~ 3250 3450
Wire Wire Line
	6050 1800 6050 3800
Wire Wire Line
	6050 3800 6300 3800
Connection ~ 6050 3450
$Comp
L PWR_FLAG #FLG?
U 1 1 4C1CEBD5
P 6300 3800
AR Path="/384C1CEBD5" Ref="#FLG?"  Part="1" 
AR Path="/25000314C1CEBD5" Ref="#FLG?"  Part="1" 
AR Path="/773F8EB44C1CEBD5" Ref="#FLG?"  Part="1" 
AR Path="/23CC3C4C1CEBD5" Ref="#FLG3"  Part="1" 
AR Path="/94C1CEBD5" Ref="#FLG"  Part="1" 
AR Path="/4C1CEBD5" Ref="#FLG022"  Part="1" 
AR Path="/773F65F14C1CEBD5" Ref="#FLG022"  Part="1" 
AR Path="/70F26E4C1CEBD5" Ref="#FLG022"  Part="1" 
AR Path="/383843454C1CEBD5" Ref="#FLG022"  Part="1" 
AR Path="/23BED44C1CEBD5" Ref="#FLG022"  Part="1" 
AR Path="/24DB2AC4C1CEBD5" Ref="#FLG022"  Part="1" 
AR Path="/23C5984C1CEBD5" Ref="#FLG022"  Part="1" 
AR Path="/14C1CEBD5" Ref="#FLG022"  Part="1" 
F 0 "#FLG022" H 6300 4070 30  0001 C CNN
F 1 "PWR_FLAG" H 6300 4030 30  0000 C CNN
	1    6300 3800
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG?
U 1 1 4C1CEBBA
P 6800 3800
AR Path="/7E4600384C1CEBBA" Ref="#FLG?"  Part="1" 
AR Path="/24C00314C1CEBBA" Ref="#FLG?"  Part="1" 
AR Path="/773F8EB44C1CEBBA" Ref="#FLG01"  Part="1" 
AR Path="/23CC3C4C1CEBBA" Ref="#FLG3"  Part="1" 
AR Path="/94C1CEBBA" Ref="#FLG"  Part="1" 
AR Path="/4C1CEBBA" Ref="#FLG023"  Part="1" 
AR Path="/773F65F14C1CEBBA" Ref="#FLG023"  Part="1" 
AR Path="/70F26E4C1CEBBA" Ref="#FLG023"  Part="1" 
AR Path="/383843454C1CEBBA" Ref="#FLG023"  Part="1" 
AR Path="/23BED44C1CEBBA" Ref="#FLG023"  Part="1" 
AR Path="/24DB2AC4C1CEBBA" Ref="#FLG023"  Part="1" 
AR Path="/23C5984C1CEBBA" Ref="#FLG023"  Part="1" 
AR Path="/14C1CEBBA" Ref="#FLG023"  Part="1" 
F 0 "#FLG023" H 6800 4070 30  0001 C CNN
F 1 "PWR_FLAG" H 6800 4030 30  0000 C CNN
	1    6800 3800
	1    0    0    -1  
$EndComp
$Comp
L DIODE D19
U 1 1 4C1CB870
P 10200 3450
AR Path="/4C1CB870" Ref="D19"  Part="1" 
AR Path="/FFFF4C1CB870" Ref="D?"  Part="1" 
AR Path="/773F8EB44C1CB870" Ref="D19"  Part="1" 
AR Path="/23CC3C4C1CB870" Ref="D19"  Part="1" 
AR Path="/94C1CB870" Ref="D"  Part="1" 
AR Path="/773F65F14C1CB870" Ref="D19"  Part="1" 
AR Path="/383843454C1CB870" Ref="D19"  Part="1" 
AR Path="/24C1CB870" Ref="D19"  Part="1" 
AR Path="/23BED44C1CB870" Ref="D19"  Part="1" 
AR Path="/24DB2AC4C1CB870" Ref="D19"  Part="1" 
AR Path="/23C5984C1CB870" Ref="D19"  Part="1" 
AR Path="/14C1CB870" Ref="D19"  Part="1" 
F 0 "D19" H 10200 3550 40  0000 C CNN
F 1 "DIODE" H 10200 3350 40  0000 C CNN
	1    10200 3450
	-1   0    0    1   
$EndComp
Text Label 10450 3450 0    60   ~ 0
VCC
Text Label 2500 3450 0    60   ~ 0
VCC
$Comp
L DIODE D?
U 1 1 4C1CB821
P 3050 3450
AR Path="/7E4600384C1CB821" Ref="D?"  Part="1" 
AR Path="/24B00314C1CB821" Ref="D?"  Part="1" 
AR Path="/773F8EB44C1CB821" Ref="D18"  Part="1" 
AR Path="/23CC3C4C1CB821" Ref="D18"  Part="1" 
AR Path="/94C1CB821" Ref="D"  Part="1" 
AR Path="/4C1CB821" Ref="D18"  Part="1" 
AR Path="/773F65F14C1CB821" Ref="D18"  Part="1" 
AR Path="/353942344C1CB821" Ref="D18"  Part="1" 
AR Path="/383843454C1CB821" Ref="D18"  Part="1" 
AR Path="/24C1CB821" Ref="D18"  Part="1" 
AR Path="/23BED44C1CB821" Ref="D18"  Part="1" 
AR Path="/24DB2AC4C1CB821" Ref="D18"  Part="1" 
AR Path="/23C5984C1CB821" Ref="D18"  Part="1" 
AR Path="/14C1CB821" Ref="D18"  Part="1" 
F 0 "D18" H 3050 3550 40  0000 C CNN
F 1 "DIODE" H 3050 3350 40  0000 C CNN
	1    3050 3450
	1    0    0    -1  
$EndComp
$Comp
L HTIL311A DIS?
U 1 1 4BEE9A74
P 9900 1850
AR Path="/384BEE9A74" Ref="DIS?"  Part="1" 
AR Path="/24A00314BEE9A74" Ref="DIS?"  Part="1" 
AR Path="/773F8EB44BEE9A74" Ref="DIS10"  Part="1" 
AR Path="/4BEE9A74" Ref="DIS10"  Part="1" 
AR Path="/94BEE9A74" Ref="DIS"  Part="1" 
AR Path="/773F65F14BEE9A74" Ref="DIS10"  Part="1" 
AR Path="/23C5984BEE9A74" Ref="DIS10"  Part="1" 
AR Path="/DA064BEE9A74" Ref="DIS10"  Part="1" 
AR Path="/24BEE9A74" Ref="DIS10"  Part="1" 
AR Path="/23BED44BEE9A74" Ref="DIS10"  Part="1" 
AR Path="/23CC3C4BEE9A74" Ref="DIS10"  Part="1" 
AR Path="/FFFFFFF04BEE9A74" Ref="DIS10"  Part="1" 
AR Path="/383843454BEE9A74" Ref="DIS10"  Part="1" 
AR Path="/D5AD4BEE9A74" Ref="DIS10"  Part="1" 
AR Path="/24DB2AC4BEE9A74" Ref="DIS10"  Part="1" 
AR Path="/14BEE9A74" Ref="DIS10"  Part="1" 
F 0 "DIS10" V 9590 1650 50  0000 L BNN
F 1 "HTIL311A" V 10115 1650 50  0000 L BNN
F 2 "display-hp-HTIL311A" H 9900 2000 50  0001 C CNN
	1    9900 1850
	1    0    0    -1  
$EndComp
$Comp
L HTIL311A DIS?
U 1 1 4BEE9A6B
P 9200 1850
AR Path="/384BEE9A6B" Ref="DIS?"  Part="1" 
AR Path="/24A00314BEE9A6B" Ref="DIS?"  Part="1" 
AR Path="/6FF0DD404BEE9A6B" Ref="DIS?"  Part="1" 
AR Path="/773F8EB44BEE9A6B" Ref="DIS9"  Part="1" 
AR Path="/4BEE9A6B" Ref="DIS9"  Part="1" 
AR Path="/94BEE9A6B" Ref="DIS"  Part="1" 
AR Path="/773F65F14BEE9A6B" Ref="DIS9"  Part="1" 
AR Path="/23C5984BEE9A6B" Ref="DIS9"  Part="1" 
AR Path="/DA064BEE9A6B" Ref="DIS9"  Part="1" 
AR Path="/24BEE9A6B" Ref="DIS9"  Part="1" 
AR Path="/23BED44BEE9A6B" Ref="DIS9"  Part="1" 
AR Path="/23CC3C4BEE9A6B" Ref="DIS9"  Part="1" 
AR Path="/FFFFFFF04BEE9A6B" Ref="DIS9"  Part="1" 
AR Path="/383843454BEE9A6B" Ref="DIS9"  Part="1" 
AR Path="/D5AD4BEE9A6B" Ref="DIS9"  Part="1" 
AR Path="/353942344BEE9A6B" Ref="DIS9"  Part="1" 
AR Path="/24DB2AC4BEE9A6B" Ref="DIS9"  Part="1" 
AR Path="/14BEE9A6B" Ref="DIS9"  Part="1" 
F 0 "DIS9" V 8890 1650 50  0000 L BNN
F 1 "HTIL311A" V 9415 1650 50  0000 L BNN
F 2 "display-hp-HTIL311A" H 9200 2000 50  0001 C CNN
	1    9200 1850
	1    0    0    -1  
$EndComp
$Comp
L HTIL311A DIS?
U 1 1 4BEE9A68
P 8500 1850
AR Path="/384BEE9A68" Ref="DIS?"  Part="1" 
AR Path="/24A00314BEE9A68" Ref="DIS?"  Part="1" 
AR Path="/773F8EB44BEE9A68" Ref="DIS8"  Part="1" 
AR Path="/4BEE9A68" Ref="DIS8"  Part="1" 
AR Path="/94BEE9A68" Ref="DIS"  Part="1" 
AR Path="/773F65F14BEE9A68" Ref="DIS8"  Part="1" 
AR Path="/23C5984BEE9A68" Ref="DIS8"  Part="1" 
AR Path="/DA064BEE9A68" Ref="DIS8"  Part="1" 
AR Path="/24BEE9A68" Ref="DIS8"  Part="1" 
AR Path="/23BED44BEE9A68" Ref="DIS8"  Part="1" 
AR Path="/23CC3C4BEE9A68" Ref="DIS8"  Part="1" 
AR Path="/FFFFFFF04BEE9A68" Ref="DIS8"  Part="1" 
AR Path="/383843454BEE9A68" Ref="DIS8"  Part="1" 
AR Path="/D5AD4BEE9A68" Ref="DIS8"  Part="1" 
AR Path="/24DB2AC4BEE9A68" Ref="DIS8"  Part="1" 
AR Path="/14BEE9A68" Ref="DIS8"  Part="1" 
F 0 "DIS8" V 8190 1650 50  0000 L BNN
F 1 "HTIL311A" V 8715 1650 50  0000 L BNN
F 2 "display-hp-HTIL311A" H 8500 2000 50  0001 C CNN
	1    8500 1850
	1    0    0    -1  
$EndComp
$Comp
L HTIL311A DIS?
U 1 1 4BEE9A65
P 7800 1850
AR Path="/384BEE9A65" Ref="DIS?"  Part="1" 
AR Path="/24A00314BEE9A65" Ref="DIS?"  Part="1" 
AR Path="/773F8EB44BEE9A65" Ref="DIS7"  Part="1" 
AR Path="/4BEE9A65" Ref="DIS7"  Part="1" 
AR Path="/94BEE9A65" Ref="DIS"  Part="1" 
AR Path="/773F65F14BEE9A65" Ref="DIS7"  Part="1" 
AR Path="/23C5984BEE9A65" Ref="DIS7"  Part="1" 
AR Path="/DA064BEE9A65" Ref="DIS7"  Part="1" 
AR Path="/24BEE9A65" Ref="DIS7"  Part="1" 
AR Path="/23BED44BEE9A65" Ref="DIS7"  Part="1" 
AR Path="/23CC3C4BEE9A65" Ref="DIS7"  Part="1" 
AR Path="/FFFFFFF04BEE9A65" Ref="DIS7"  Part="1" 
AR Path="/383843454BEE9A65" Ref="DIS7"  Part="1" 
AR Path="/D5AD4BEE9A65" Ref="DIS7"  Part="1" 
AR Path="/24DB2AC4BEE9A65" Ref="DIS7"  Part="1" 
AR Path="/14BEE9A65" Ref="DIS7"  Part="1" 
F 0 "DIS7" V 7490 1650 50  0000 L BNN
F 1 "HTIL311A" V 8015 1650 50  0000 L BNN
F 2 "display-hp-HTIL311A" H 7800 2000 50  0001 C CNN
	1    7800 1850
	1    0    0    -1  
$EndComp
$Comp
L HTIL311A DIS?
U 1 1 4BEE9A5E
P 7100 1850
AR Path="/384BEE9A5E" Ref="DIS?"  Part="1" 
AR Path="/24A00314BEE9A5E" Ref="DIS?"  Part="1" 
AR Path="/773F8EB44BEE9A5E" Ref="DIS6"  Part="1" 
AR Path="/4BEE9A5E" Ref="DIS6"  Part="1" 
AR Path="/94BEE9A5E" Ref="DIS"  Part="1" 
AR Path="/773F65F14BEE9A5E" Ref="DIS6"  Part="1" 
AR Path="/23C5984BEE9A5E" Ref="DIS6"  Part="1" 
AR Path="/DA064BEE9A5E" Ref="DIS6"  Part="1" 
AR Path="/24BEE9A5E" Ref="DIS6"  Part="1" 
AR Path="/23BED44BEE9A5E" Ref="DIS6"  Part="1" 
AR Path="/23CC3C4BEE9A5E" Ref="DIS6"  Part="1" 
AR Path="/FFFFFFF04BEE9A5E" Ref="DIS6"  Part="1" 
AR Path="/383843454BEE9A5E" Ref="DIS6"  Part="1" 
AR Path="/D5AD4BEE9A5E" Ref="DIS6"  Part="1" 
AR Path="/24DB2AC4BEE9A5E" Ref="DIS6"  Part="1" 
AR Path="/14BEE9A5E" Ref="DIS6"  Part="1" 
F 0 "DIS6" V 6790 1650 50  0000 L BNN
F 1 "HTIL311A" V 7315 1650 50  0000 L BNN
F 2 "display-hp-HTIL311A" H 7100 2000 50  0001 C CNN
	1    7100 1850
	1    0    0    -1  
$EndComp
$Comp
L HTIL311A DIS?
U 1 1 4BEE9A59
P 6400 1850
AR Path="/384BEE9A59" Ref="DIS?"  Part="1" 
AR Path="/24A00314BEE9A59" Ref="DIS?"  Part="1" 
AR Path="/773F8EB44BEE9A59" Ref="DIS5"  Part="1" 
AR Path="/4BEE9A59" Ref="DIS5"  Part="1" 
AR Path="/94BEE9A59" Ref="DIS"  Part="1" 
AR Path="/773F65F14BEE9A59" Ref="DIS5"  Part="1" 
AR Path="/23C5984BEE9A59" Ref="DIS5"  Part="1" 
AR Path="/DA064BEE9A59" Ref="DIS5"  Part="1" 
AR Path="/24BEE9A59" Ref="DIS5"  Part="1" 
AR Path="/23BED44BEE9A59" Ref="DIS5"  Part="1" 
AR Path="/23CC3C4BEE9A59" Ref="DIS5"  Part="1" 
AR Path="/FFFFFFF04BEE9A59" Ref="DIS5"  Part="1" 
AR Path="/383843454BEE9A59" Ref="DIS5"  Part="1" 
AR Path="/D5AD4BEE9A59" Ref="DIS5"  Part="1" 
AR Path="/24DB2AC4BEE9A59" Ref="DIS5"  Part="1" 
AR Path="/14BEE9A59" Ref="DIS5"  Part="1" 
F 0 "DIS5" V 6090 1650 50  0000 L BNN
F 1 "HTIL311A" V 6615 1650 50  0000 L BNN
F 2 "display-hp-HTIL311A" H 6400 2000 50  0001 C CNN
	1    6400 1850
	1    0    0    -1  
$EndComp
$Comp
L HTIL311A DIS?
U 1 1 4BEE9A55
P 5700 1850
AR Path="/384BEE9A55" Ref="DIS?"  Part="1" 
AR Path="/24A00314BEE9A55" Ref="DIS?"  Part="1" 
AR Path="/773F8EB44BEE9A55" Ref="DIS4"  Part="1" 
AR Path="/4BEE9A55" Ref="DIS4"  Part="1" 
AR Path="/94BEE9A55" Ref="DIS"  Part="1" 
AR Path="/773F65F14BEE9A55" Ref="DIS4"  Part="1" 
AR Path="/23C5984BEE9A55" Ref="DIS4"  Part="1" 
AR Path="/DA064BEE9A55" Ref="DIS4"  Part="1" 
AR Path="/24BEE9A55" Ref="DIS4"  Part="1" 
AR Path="/23BED44BEE9A55" Ref="DIS4"  Part="1" 
AR Path="/23CC3C4BEE9A55" Ref="DIS4"  Part="1" 
AR Path="/FFFFFFF04BEE9A55" Ref="DIS4"  Part="1" 
AR Path="/383843454BEE9A55" Ref="DIS4"  Part="1" 
AR Path="/D5AD4BEE9A55" Ref="DIS4"  Part="1" 
AR Path="/24DB2AC4BEE9A55" Ref="DIS4"  Part="1" 
AR Path="/14BEE9A55" Ref="DIS4"  Part="1" 
F 0 "DIS4" V 5390 1650 50  0000 L BNN
F 1 "HTIL311A" V 5915 1650 50  0000 L BNN
F 2 "display-hp-HTIL311A" H 5700 2000 50  0001 C CNN
	1    5700 1850
	1    0    0    -1  
$EndComp
$Comp
L HTIL311A DIS?
U 1 1 4BEE9A51
P 5000 1850
AR Path="/384BEE9A51" Ref="DIS?"  Part="1" 
AR Path="/24A00314BEE9A51" Ref="DIS?"  Part="1" 
AR Path="/773F8EB44BEE9A51" Ref="DIS3"  Part="1" 
AR Path="/4BEE9A51" Ref="DIS3"  Part="1" 
AR Path="/94BEE9A51" Ref="DIS"  Part="1" 
AR Path="/773F65F14BEE9A51" Ref="DIS3"  Part="1" 
AR Path="/23C5984BEE9A51" Ref="DIS3"  Part="1" 
AR Path="/DA064BEE9A51" Ref="DIS3"  Part="1" 
AR Path="/24BEE9A51" Ref="DIS3"  Part="1" 
AR Path="/23BED44BEE9A51" Ref="DIS3"  Part="1" 
AR Path="/23CC3C4BEE9A51" Ref="DIS3"  Part="1" 
AR Path="/FFFFFFF04BEE9A51" Ref="DIS3"  Part="1" 
AR Path="/383843454BEE9A51" Ref="DIS3"  Part="1" 
AR Path="/D5AD4BEE9A51" Ref="DIS3"  Part="1" 
AR Path="/24DB2AC4BEE9A51" Ref="DIS3"  Part="1" 
AR Path="/14BEE9A51" Ref="DIS3"  Part="1" 
F 0 "DIS3" V 4690 1650 50  0000 L BNN
F 1 "HTIL311A" V 5215 1650 50  0000 L BNN
F 2 "display-hp-HTIL311A" H 5000 2000 50  0001 C CNN
	1    5000 1850
	1    0    0    -1  
$EndComp
$Comp
L HTIL311A DIS?
U 1 1 4BEE9A4D
P 4300 1850
AR Path="/384BEE9A4D" Ref="DIS?"  Part="1" 
AR Path="/24A00314BEE9A4D" Ref="DIS?"  Part="1" 
AR Path="/773F8EB44BEE9A4D" Ref="DIS2"  Part="1" 
AR Path="/4BEE9A4D" Ref="DIS2"  Part="1" 
AR Path="/94BEE9A4D" Ref="DIS"  Part="1" 
AR Path="/773F65F14BEE9A4D" Ref="DIS2"  Part="1" 
AR Path="/23C5984BEE9A4D" Ref="DIS2"  Part="1" 
AR Path="/DA064BEE9A4D" Ref="DIS2"  Part="1" 
AR Path="/24BEE9A4D" Ref="DIS2"  Part="1" 
AR Path="/23BED44BEE9A4D" Ref="DIS2"  Part="1" 
AR Path="/23CC3C4BEE9A4D" Ref="DIS2"  Part="1" 
AR Path="/FFFFFFF04BEE9A4D" Ref="DIS2"  Part="1" 
AR Path="/383843454BEE9A4D" Ref="DIS2"  Part="1" 
AR Path="/D5AD4BEE9A4D" Ref="DIS2"  Part="1" 
AR Path="/24DB2AC4BEE9A4D" Ref="DIS2"  Part="1" 
AR Path="/14BEE9A4D" Ref="DIS2"  Part="1" 
F 0 "DIS2" V 3990 1650 50  0000 L BNN
F 1 "HTIL311A" V 4515 1650 50  0000 L BNN
F 2 "display-hp-HTIL311A" H 4300 2000 50  0001 C CNN
	1    4300 1850
	1    0    0    -1  
$EndComp
$Comp
L HTIL311A DIS?
U 1 1 4BEE98A8
P 3600 1850
AR Path="/2300384BEE98A8" Ref="DIS?"  Part="1" 
AR Path="/26000314BEE98A8" Ref="DIS?"  Part="1" 
AR Path="/4BEE98A8" Ref="DIS1"  Part="1" 
AR Path="/773F8EB44BEE98A8" Ref="DIS1"  Part="1" 
AR Path="/23CC3C4BEE98A8" Ref="DIS1"  Part="1" 
AR Path="/94BEE98A8" Ref="DIS"  Part="1" 
AR Path="/773F65F14BEE98A8" Ref="DIS1"  Part="1" 
AR Path="/23C5984BEE98A8" Ref="DIS1"  Part="1" 
AR Path="/DA064BEE98A8" Ref="DIS1"  Part="1" 
AR Path="/24BEE98A8" Ref="DIS1"  Part="1" 
AR Path="/23BED44BEE98A8" Ref="DIS1"  Part="1" 
AR Path="/FFFFFFF04BEE98A8" Ref="DIS1"  Part="1" 
AR Path="/383843454BEE98A8" Ref="DIS1"  Part="1" 
AR Path="/D5AD4BEE98A8" Ref="DIS1"  Part="1" 
AR Path="/24DB2AC4BEE98A8" Ref="DIS1"  Part="1" 
AR Path="/14BEE98A8" Ref="DIS1"  Part="1" 
F 0 "DIS1" V 3290 1650 50  0000 L BNN
F 1 "HTIL311A" V 3815 1650 50  0000 L BNN
F 2 "display-hp-HTIL311A" H 3600 2000 50  0001 C CNN
	1    3600 1850
	1    0    0    -1  
$EndComp
Text Label 850  5100 0    60   ~ 0
GND
$Comp
L PWR_FLAG #FLG023
U 1 1 4B959150
P 1500 5100
AR Path="/773F8EB44B959150" Ref="#FLG023"  Part="1" 
AR Path="/4B959150" Ref="#FLG024"  Part="1" 
AR Path="/24B959150" Ref="#FLG"  Part="1" 
AR Path="/773F65F14B959150" Ref="#FLG024"  Part="1" 
AR Path="/23C6504B959150" Ref="#FLG01"  Part="1" 
AR Path="/23C9F04B959150" Ref="#FLG2"  Part="1" 
AR Path="/94B959150" Ref="#FLG"  Part="1" 
AR Path="/23C34C4B959150" Ref="#FLG022"  Part="1" 
AR Path="/23BC884B959150" Ref="#FLG022"  Part="1" 
AR Path="/6684D64B959150" Ref="#FLG022"  Part="1" 
AR Path="/FFFFFFF04B959150" Ref="#FLG2"  Part="1" 
AR Path="/23CBC44B959150" Ref="#FLG022"  Part="1" 
AR Path="/FFFFFFFF4B959150" Ref="#FLG022"  Part="1" 
AR Path="/6FF0DD404B959150" Ref="#FLG022"  Part="1" 
AR Path="/3824B959150" Ref="#FLG022"  Part="1" 
AR Path="/CBBD4B959150" Ref="#FLG022"  Part="1" 
AR Path="/23CC3C4B959150" Ref="#FLG2"  Part="1" 
AR Path="/71128E4B959150" Ref="#FLG022"  Part="1" 
AR Path="/23C5984B959150" Ref="#FLG024"  Part="1" 
AR Path="/DA064B959150" Ref="#FLG022"  Part="1" 
AR Path="/23BED44B959150" Ref="#FLG024"  Part="1" 
AR Path="/383843454B959150" Ref="#FLG024"  Part="1" 
AR Path="/D5AD4B959150" Ref="#FLG022"  Part="1" 
AR Path="/70F26E4B959150" Ref="#FLG024"  Part="1" 
AR Path="/24DB2AC4B959150" Ref="#FLG024"  Part="1" 
AR Path="/14B959150" Ref="#FLG024"  Part="1" 
F 0 "#FLG024" H 1500 5370 30  0001 C CNN
F 1 "PWR_FLAG" H 1500 5330 30  0000 C CNN
	1    1500 5100
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG?
U 1 1 4B95914A
P 1500 4750
AR Path="/A100384B95914A" Ref="#FLG?"  Part="1" 
AR Path="/4B95914A" Ref="#FLG025"  Part="1" 
AR Path="/773F8EB44B95914A" Ref="#FLG024"  Part="1" 
AR Path="/24B95914A" Ref="#FLG"  Part="1" 
AR Path="/773F65F14B95914A" Ref="#FLG025"  Part="1" 
AR Path="/23C6504B95914A" Ref="#FLG02"  Part="1" 
AR Path="/23C34C4B95914A" Ref="#FLG023"  Part="1" 
AR Path="/23BC884B95914A" Ref="#FLG023"  Part="1" 
AR Path="/6684D64B95914A" Ref="#FLG023"  Part="1" 
AR Path="/22BC0224B95914A" Ref="#FLG1"  Part="1" 
AR Path="/FFFFFFF04B95914A" Ref="#FLG1"  Part="1" 
AR Path="/23CBC44B95914A" Ref="#FLG023"  Part="1" 
AR Path="/2497E0A4B95914A" Ref="#FLG1"  Part="1" 
AR Path="/24BE32A4B95914A" Ref="#FLG1"  Part="1" 
AR Path="/FFFFFFFF4B95914A" Ref="#FLG023"  Part="1" 
AR Path="/6FF0DD404B95914A" Ref="#FLG023"  Part="1" 
AR Path="/3824B95914A" Ref="#FLG023"  Part="1" 
AR Path="/CBBD4B95914A" Ref="#FLG023"  Part="1" 
AR Path="/71128E4B95914A" Ref="#FLG023"  Part="1" 
AR Path="/23C5984B95914A" Ref="#FLG025"  Part="1" 
AR Path="/DA064B95914A" Ref="#FLG023"  Part="1" 
AR Path="/23BED44B95914A" Ref="#FLG025"  Part="1" 
AR Path="/2663E0A4B95914A" Ref="#FLG1"  Part="1" 
AR Path="/383843454B95914A" Ref="#FLG025"  Part="1" 
AR Path="/D5AD4B95914A" Ref="#FLG023"  Part="1" 
AR Path="/70F26E4B95914A" Ref="#FLG025"  Part="1" 
AR Path="/24DB2AC4B95914A" Ref="#FLG025"  Part="1" 
AR Path="/14B95914A" Ref="#FLG025"  Part="1" 
F 0 "#FLG025" H 1500 5020 30  0001 C CNN
F 1 "PWR_FLAG" H 1500 4980 30  0000 C CNN
	1    1500 4750
	1    0    0    -1  
$EndComp
Text Label 850  4750 0    60   ~ 0
VCC
Text Label 7800 1200 1    60   ~ 0
B-DI6
Text Label 8400 3150 1    60   ~ 0
VCC
Text Label 9100 3150 1    60   ~ 0
VCC
Text Label 9300 3150 1    60   ~ 0
VCC
Text Label 9000 3150 1    60   ~ 0
LATCH_DATA_16*
Text Label 9700 3150 1    60   ~ 0
LATCH_DATA_16*
Text Label 10000 3150 1    60   ~ 0
VCC
Text Label 9800 3150 1    60   ~ 0
VCC
Text Label 9900 3150 1    60   ~ 0
B-SIXTN*
Text Label 6300 3150 1    60   ~ 0
VCC
Text Label 6400 3150 1    60   ~ 0
GND
Text Label 6500 3150 1    60   ~ 0
VCC
Text Label 6200 3150 1    60   ~ 0
LATCH_DATA_8*
Text Label 6900 3150 1    60   ~ 0
LATCH_DATA_8*
Text Label 7200 3150 1    60   ~ 0
VCC
Text Label 7100 3150 1    60   ~ 0
GND
Text Label 7000 3150 1    60   ~ 0
VCC
Text Label 9200 3150 1    60   ~ 0
B-SIXTN*
Text Label 8500 3150 1    60   ~ 0
GND
Text Label 8600 3150 1    60   ~ 0
VCC
Text Label 8300 3150 1    60   ~ 0
LATCH_DATA_8*
Text Label 7600 3150 1    60   ~ 0
LATCH_DATA_8*
Text Label 7900 3150 1    60   ~ 0
VCC
Text Label 7800 3150 1    60   ~ 0
GND
Text Label 7700 3150 1    60   ~ 0
VCC
Text Label 4900 3150 1    60   ~ 0
VCC
Text Label 5000 3150 1    60   ~ 0
GND
Text Label 5100 3150 1    60   ~ 0
VCC
Text Label 4800 3150 1    60   ~ 0
LATCH_DATA_8*
Text Label 5500 3150 1    60   ~ 0
LATCH_DATA_8*
Text Label 5800 3150 1    60   ~ 0
VCC
Text Label 5700 3150 1    60   ~ 0
GND
Text Label 5600 3150 1    60   ~ 0
VCC
Text Label 4200 3150 1    60   ~ 0
VCC
Text Label 4300 3150 1    60   ~ 0
GND
Text Label 4400 3150 1    60   ~ 0
VCC
Text Label 4100 3150 1    60   ~ 0
LATCH_DATA_8*
Text Label 8300 1200 1    60   ~ 0
B-DI0
Text Label 8600 1200 1    60   ~ 0
B-DI3
Text Label 8500 1200 1    60   ~ 0
B-DI2
Text Label 8400 1200 1    60   ~ 0
B-DI1
Text Label 7700 1200 1    60   ~ 0
B-DI5
Text Label 7900 1200 1    60   ~ 0
B-DI7
Text Label 7600 1200 1    60   ~ 0
B-DI4
Text Label 6200 1200 1    60   ~ 0
B-A4
Text Label 6500 1200 1    60   ~ 0
B-A7
Text Label 6400 1200 1    60   ~ 0
B-A6
Text Label 6300 1200 1    60   ~ 0
B-A5
Text Label 7000 1200 1    60   ~ 0
B-A1
Text Label 7100 1200 1    60   ~ 0
B-A2
Text Label 7200 1200 1    60   ~ 0
B-A3
Text Label 6900 1200 1    60   ~ 0
B-A0
Text Label 9000 1200 1    60   ~ 0
B-DO4
Text Label 9300 1200 1    60   ~ 0
B-DO7
Text Label 9200 1200 1    60   ~ 0
B-DO6
Text Label 9100 1200 1    60   ~ 0
B-DO5
Text Label 9800 1200 1    60   ~ 0
B-DO1
Text Label 9900 1200 1    60   ~ 0
B-DO2
Text Label 10000 1200 1    60   ~ 0
B-DO3
Text Label 9700 1200 1    60   ~ 0
B-DO0
Text Label 5500 1200 1    60   ~ 0
B-A8
Text Label 5800 1200 1    60   ~ 0
B-A11
Text Label 5700 1200 1    60   ~ 0
B-A10
Text Label 5600 1200 1    60   ~ 0
B-A9
Text Label 4900 1200 1    60   ~ 0
B-A13
Text Label 5000 1200 1    60   ~ 0
B-A14
Text Label 5100 1200 1    60   ~ 0
B-A15
Text Label 4800 1200 1    60   ~ 0
B-A12
Text Label 4100 1200 1    60   ~ 0
B-A16
Text Label 4400 1200 1    60   ~ 0
B-A19
Text Label 4300 1200 1    60   ~ 0
B-A18
Text Label 4200 1200 1    60   ~ 0
B-A17
Text Label 3500 1200 1    60   ~ 0
B-A21
Text Label 3600 1200 1    60   ~ 0
B-A22
Text Label 3700 1200 1    60   ~ 0
B-A23
Text Label 3400 1200 1    60   ~ 0
B-A20
Text Label 3400 3150 1    60   ~ 0
LATCH_DATA_8*
Text Label 3700 3150 1    60   ~ 0
VCC
Text Label 3600 3150 1    60   ~ 0
GND
Text Label 3500 3150 1    60   ~ 0
VCC
Text Notes 900  3600 0    60   ~ 0
DISPLAY MEZZANINE\nMOUNTING HOLES
$Comp
L GND #PWR?
U 1 1 4B958AC7
P 1100 4150
AR Path="/A100384B958AC7" Ref="#PWR?"  Part="1" 
AR Path="/4B958AC7" Ref="#PWR026"  Part="1" 
AR Path="/773F8EB44B958AC7" Ref="#PWR025"  Part="1" 
AR Path="/24B958AC7" Ref="#PWR"  Part="1" 
AR Path="/773F65F14B958AC7" Ref="#PWR026"  Part="1" 
AR Path="/23C6504B958AC7" Ref="#PWR03"  Part="1" 
AR Path="/353942344B958AC7" Ref="#PWR01"  Part="1" 
AR Path="/94B958AC7" Ref="#PWR"  Part="1" 
AR Path="/23C34C4B958AC7" Ref="#PWR024"  Part="1" 
AR Path="/23BC884B958AC7" Ref="#PWR024"  Part="1" 
AR Path="/6684D64B958AC7" Ref="#PWR024"  Part="1" 
AR Path="/23C9F04B958AC7" Ref="#PWR1"  Part="1" 
AR Path="/FFFFFFF04B958AC7" Ref="#PWR1"  Part="1" 
AR Path="/23CBC44B958AC7" Ref="#PWR024"  Part="1" 
AR Path="/FFFFFFFF4B958AC7" Ref="#PWR024"  Part="1" 
AR Path="/6FF0DD404B958AC7" Ref="#PWR024"  Part="1" 
AR Path="/3824B958AC7" Ref="#PWR024"  Part="1" 
AR Path="/CBBD4B958AC7" Ref="#PWR024"  Part="1" 
AR Path="/23CC3C4B958AC7" Ref="#PWR1"  Part="1" 
AR Path="/71128E4B958AC7" Ref="#PWR024"  Part="1" 
AR Path="/23C5984B958AC7" Ref="#PWR026"  Part="1" 
AR Path="/DA064B958AC7" Ref="#PWR024"  Part="1" 
AR Path="/23BED44B958AC7" Ref="#PWR026"  Part="1" 
AR Path="/383843454B958AC7" Ref="#PWR026"  Part="1" 
AR Path="/D5AD4B958AC7" Ref="#PWR024"  Part="1" 
AR Path="/70F26E4B958AC7" Ref="#PWR026"  Part="1" 
AR Path="/24DB2AC4B958AC7" Ref="#PWR026"  Part="1" 
AR Path="/14B958AC7" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 1100 4150 30  0001 C CNN
F 1 "GND" H 1100 4080 30  0001 C CNN
	1    1100 4150
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P5
U 1 1 4B958ABB
P 1250 4000
AR Path="/773F8EB44B958ABB" Ref="P5"  Part="1" 
AR Path="/4B958ABB" Ref="P5"  Part="1" 
AR Path="/24B958ABB" Ref="P5"  Part="1" 
AR Path="/773F65F14B958ABB" Ref="P5"  Part="1" 
AR Path="/23C6504B958ABB" Ref="P5"  Part="1" 
AR Path="/94B958ABB" Ref="P"  Part="1" 
AR Path="/23C9F04B958ABB" Ref="P5"  Part="1" 
AR Path="/23C34C4B958ABB" Ref="P5"  Part="1" 
AR Path="/23BC884B958ABB" Ref="P5"  Part="1" 
AR Path="/FFFFFFF04B958ABB" Ref="P5"  Part="1" 
AR Path="/23CBC44B958ABB" Ref="P5"  Part="1" 
AR Path="/FFFFFFFF4B958ABB" Ref="P5"  Part="1" 
AR Path="/3824B958ABB" Ref="P5"  Part="1" 
AR Path="/CBBD4B958ABB" Ref="P5"  Part="1" 
AR Path="/23CC3C4B958ABB" Ref="P5"  Part="1" 
AR Path="/23C5984B958ABB" Ref="P5"  Part="1" 
AR Path="/DA064B958ABB" Ref="P5"  Part="1" 
AR Path="/23BED44B958ABB" Ref="P5"  Part="1" 
AR Path="/383843454B958ABB" Ref="P5"  Part="1" 
AR Path="/D5AD4B958ABB" Ref="P5"  Part="1" 
AR Path="/24DB2AC4B958ABB" Ref="P5"  Part="1" 
AR Path="/14B958ABB" Ref="P5"  Part="1" 
F 0 "P5" H 1330 4000 40  0000 L CNN
F 1 "CONN_1" H 1250 4055 30  0001 C CNN
	1    1250 4000
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P6
U 1 1 4B958ABA
P 1250 4100
AR Path="/773F8EB44B958ABA" Ref="P6"  Part="1" 
AR Path="/4B958ABA" Ref="P6"  Part="1" 
AR Path="/24B958ABA" Ref="P6"  Part="1" 
AR Path="/773F65F14B958ABA" Ref="P6"  Part="1" 
AR Path="/23C6504B958ABA" Ref="P6"  Part="1" 
AR Path="/94B958ABA" Ref="P"  Part="1" 
AR Path="/23C9F04B958ABA" Ref="P6"  Part="1" 
AR Path="/23C34C4B958ABA" Ref="P6"  Part="1" 
AR Path="/23BC884B958ABA" Ref="P6"  Part="1" 
AR Path="/FFFFFFF04B958ABA" Ref="P6"  Part="1" 
AR Path="/23CBC44B958ABA" Ref="P6"  Part="1" 
AR Path="/FFFFFFFF4B958ABA" Ref="P6"  Part="1" 
AR Path="/3824B958ABA" Ref="P6"  Part="1" 
AR Path="/CBBD4B958ABA" Ref="P6"  Part="1" 
AR Path="/23CC3C4B958ABA" Ref="P6"  Part="1" 
AR Path="/23C5984B958ABA" Ref="P6"  Part="1" 
AR Path="/DA064B958ABA" Ref="P6"  Part="1" 
AR Path="/23BED44B958ABA" Ref="P6"  Part="1" 
AR Path="/383843454B958ABA" Ref="P6"  Part="1" 
AR Path="/D5AD4B958ABA" Ref="P6"  Part="1" 
AR Path="/24DB2AC4B958ABA" Ref="P6"  Part="1" 
AR Path="/14B958ABA" Ref="P6"  Part="1" 
F 0 "P6" H 1330 4100 40  0000 L CNN
F 1 "CONN_1" H 1250 4155 30  0001 C CNN
	1    1250 4100
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P4
U 1 1 4B958AB7
P 1250 3900
AR Path="/773F8EB44B958AB7" Ref="P4"  Part="1" 
AR Path="/4B958AB7" Ref="P4"  Part="1" 
AR Path="/24B958AB7" Ref="P4"  Part="1" 
AR Path="/773F65F14B958AB7" Ref="P4"  Part="1" 
AR Path="/23C6504B958AB7" Ref="P4"  Part="1" 
AR Path="/94B958AB7" Ref="P"  Part="1" 
AR Path="/23C9F04B958AB7" Ref="P4"  Part="1" 
AR Path="/23C34C4B958AB7" Ref="P4"  Part="1" 
AR Path="/23BC884B958AB7" Ref="P4"  Part="1" 
AR Path="/FFFFFFF04B958AB7" Ref="P4"  Part="1" 
AR Path="/23CBC44B958AB7" Ref="P4"  Part="1" 
AR Path="/FFFFFFFF4B958AB7" Ref="P4"  Part="1" 
AR Path="/3824B958AB7" Ref="P4"  Part="1" 
AR Path="/CBBD4B958AB7" Ref="P4"  Part="1" 
AR Path="/23CC3C4B958AB7" Ref="P4"  Part="1" 
AR Path="/23C5984B958AB7" Ref="P4"  Part="1" 
AR Path="/DA064B958AB7" Ref="P4"  Part="1" 
AR Path="/23BED44B958AB7" Ref="P4"  Part="1" 
AR Path="/383843454B958AB7" Ref="P4"  Part="1" 
AR Path="/D5AD4B958AB7" Ref="P4"  Part="1" 
AR Path="/24DB2AC4B958AB7" Ref="P4"  Part="1" 
AR Path="/14B958AB7" Ref="P4"  Part="1" 
F 0 "P4" H 1330 3900 40  0000 L CNN
F 1 "CONN_1" H 1250 3955 30  0001 C CNN
	1    1250 3900
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P3
U 1 1 4B958AAE
P 1250 3800
AR Path="/2300384B958AAE" Ref="P3"  Part="1" 
AR Path="/4B958AAE" Ref="P3"  Part="1" 
AR Path="/773F8EB44B958AAE" Ref="P3"  Part="1" 
AR Path="/24B958AAE" Ref="P3"  Part="1" 
AR Path="/773F65F14B958AAE" Ref="P3"  Part="1" 
AR Path="/23C6504B958AAE" Ref="P3"  Part="1" 
AR Path="/94B958AAE" Ref="P"  Part="1" 
AR Path="/23C9F04B958AAE" Ref="P3"  Part="1" 
AR Path="/23C34C4B958AAE" Ref="P3"  Part="1" 
AR Path="/23BC884B958AAE" Ref="P3"  Part="1" 
AR Path="/FFFFFFF04B958AAE" Ref="P3"  Part="1" 
AR Path="/23CBC44B958AAE" Ref="P3"  Part="1" 
AR Path="/FFFFFFFF4B958AAE" Ref="P3"  Part="1" 
AR Path="/3824B958AAE" Ref="P3"  Part="1" 
AR Path="/CBBD4B958AAE" Ref="P3"  Part="1" 
AR Path="/23C5984B958AAE" Ref="P3"  Part="1" 
AR Path="/DA064B958AAE" Ref="P3"  Part="1" 
AR Path="/23BED44B958AAE" Ref="P3"  Part="1" 
AR Path="/23CC3C4B958AAE" Ref="P3"  Part="1" 
AR Path="/383843454B958AAE" Ref="P3"  Part="1" 
AR Path="/D5AD4B958AAE" Ref="P3"  Part="1" 
AR Path="/24DB2AC4B958AAE" Ref="P3"  Part="1" 
AR Path="/14B958AAE" Ref="P3"  Part="1" 
F 0 "P3" H 1330 3800 40  0000 L CNN
F 1 "CONN_1" H 1250 3855 30  0001 C CNN
	1    1250 3800
	1    0    0    -1  
$EndComp
$Comp
L CONN_13X2 P1
U 1 1 4B9588CF
P 1500 1250
AR Path="/773F8EB44B9588CF" Ref="P1"  Part="1" 
AR Path="/4B9588CF" Ref="P1"  Part="1" 
AR Path="/24B9588CF" Ref="P1"  Part="1" 
AR Path="/773F65F14B9588CF" Ref="P1"  Part="1" 
AR Path="/23C6504B9588CF" Ref="P1"  Part="1" 
AR Path="/94B9588CF" Ref="P"  Part="1" 
AR Path="/23C9F04B9588CF" Ref="P1"  Part="1" 
AR Path="/23C34C4B9588CF" Ref="P1"  Part="1" 
AR Path="/23BC884B9588CF" Ref="P1"  Part="1" 
AR Path="/FFFFFFF04B9588CF" Ref="P1"  Part="1" 
AR Path="/23CBC44B9588CF" Ref="P1"  Part="1" 
AR Path="/FFFFFFFF4B9588CF" Ref="P1"  Part="1" 
AR Path="/23C5884B9588CF" Ref="P1"  Part="1" 
AR Path="/3824B9588CF" Ref="P1"  Part="1" 
AR Path="/CBBD4B9588CF" Ref="P1"  Part="1" 
AR Path="/23CC3C4B9588CF" Ref="P1"  Part="1" 
AR Path="/23C5984B9588CF" Ref="P1"  Part="1" 
AR Path="/DA064B9588CF" Ref="P1"  Part="1" 
AR Path="/23BED44B9588CF" Ref="P1"  Part="1" 
AR Path="/383843454B9588CF" Ref="P1"  Part="1" 
AR Path="/D5AD4B9588CF" Ref="P1"  Part="1" 
AR Path="/24DB2AC4B9588CF" Ref="P1"  Part="1" 
AR Path="/14B9588CF" Ref="P1"  Part="1" 
F 0 "P1" H 1500 1950 60  0000 C CNN
F 1 "CONN_13X2" V 1500 1250 50  0000 C CNN
	1    1500 1250
	1    0    0    -1  
$EndComp
$Comp
L CONN_13X2 P2
U 1 1 4B9588CE
P 1500 2550
AR Path="/773F8EB44B9588CE" Ref="P2"  Part="1" 
AR Path="/4B9588CE" Ref="P2"  Part="1" 
AR Path="/24B9588CE" Ref="P2"  Part="1" 
AR Path="/773F65F14B9588CE" Ref="P2"  Part="1" 
AR Path="/23C6504B9588CE" Ref="P2"  Part="1" 
AR Path="/94B9588CE" Ref="P"  Part="1" 
AR Path="/23C9F04B9588CE" Ref="P2"  Part="1" 
AR Path="/23C34C4B9588CE" Ref="P2"  Part="1" 
AR Path="/23BC884B9588CE" Ref="P2"  Part="1" 
AR Path="/FFFFFFF04B9588CE" Ref="P2"  Part="1" 
AR Path="/23CBC44B9588CE" Ref="P2"  Part="1" 
AR Path="/FFFFFFFF4B9588CE" Ref="P2"  Part="1" 
AR Path="/23C5884B9588CE" Ref="P2"  Part="1" 
AR Path="/3824B9588CE" Ref="P2"  Part="1" 
AR Path="/CBBD4B9588CE" Ref="P2"  Part="1" 
AR Path="/23CC3C4B9588CE" Ref="P2"  Part="1" 
AR Path="/23C5984B9588CE" Ref="P2"  Part="1" 
AR Path="/DA064B9588CE" Ref="P2"  Part="1" 
AR Path="/23BED44B9588CE" Ref="P2"  Part="1" 
AR Path="/383843454B9588CE" Ref="P2"  Part="1" 
AR Path="/D5AD4B9588CE" Ref="P2"  Part="1" 
AR Path="/24DB2AC4B9588CE" Ref="P2"  Part="1" 
AR Path="/14B9588CE" Ref="P2"  Part="1" 
F 0 "P2" H 1500 3250 60  0000 C CNN
F 1 "CONN_13X2" V 1500 2550 50  0000 C CNN
	1    1500 2550
	1    0    0    -1  
$EndComp
Text Label 800  650  0    60   ~ 0
B-A0
Text Label 800  750  0    60   ~ 0
B-A1
Text Label 800  850  0    60   ~ 0
B-A2
Text Label 800  950  0    60   ~ 0
B-A3
Text Label 800  1050 0    60   ~ 0
B-A4
Text Label 800  1150 0    60   ~ 0
B-A5
Text Label 800  1250 0    60   ~ 0
B-A6
Text Label 800  1350 0    60   ~ 0
B-A7
Text Label 800  1450 0    60   ~ 0
B-A8
Text Label 800  1550 0    60   ~ 0
B-A9
Text Label 800  1650 0    60   ~ 0
B-A10
Text Label 800  1750 0    60   ~ 0
B-A11
Text Label 800  1850 0    60   ~ 0
B-A12
Text Label 800  1950 0    60   ~ 0
B-A13
Text Label 800  2050 0    60   ~ 0
B-A14
Text Label 800  2150 0    60   ~ 0
B-A15
Text Label 800  2250 0    60   ~ 0
B-A16
Text Label 800  2350 0    60   ~ 0
B-A17
Text Label 800  2450 0    60   ~ 0
B-A18
Text Label 800  2550 0    60   ~ 0
B-A19
Text Label 800  2650 0    60   ~ 0
B-A20
Text Label 800  2750 0    60   ~ 0
B-A21
Text Label 800  2850 0    60   ~ 0
B-A22
Text Label 800  2950 0    60   ~ 0
B-A23
Text Label 1950 650  0    60   ~ 0
B-DO0
Text Label 1950 750  0    60   ~ 0
B-DO1
Text Label 1950 850  0    60   ~ 0
B-DO2
Text Label 1950 950  0    60   ~ 0
B-DO3
Text Label 1950 1050 0    60   ~ 0
B-DO4
Text Label 1950 1150 0    60   ~ 0
B-DO5
Text Label 1950 1250 0    60   ~ 0
B-DO6
Text Label 1950 1350 0    60   ~ 0
B-DO7
Text Label 1950 1450 0    60   ~ 0
B-DI0
Text Label 1950 1550 0    60   ~ 0
B-DI1
Text Label 1950 1650 0    60   ~ 0
B-DI2
Text Label 1950 1750 0    60   ~ 0
B-DI3
Text Label 1950 1850 0    60   ~ 0
B-DI4
Text Label 1950 1950 0    60   ~ 0
B-DI5
Text Label 1950 2050 0    60   ~ 0
B-DI6
Text Label 1950 2150 0    60   ~ 0
B-DI7
Text Label 800  3050 0    60   ~ 0
VCC
Text Label 800  3150 0    60   ~ 0
VCC
Text Label 1950 3050 0    60   ~ 0
VCC
Text Label 1950 3150 0    60   ~ 0
VCC
Text Label 1950 2250 0    60   ~ 0
GND
Text Label 1950 2350 0    60   ~ 0
GND
Text Label 1950 2450 0    60   ~ 0
GND
Text Label 1950 2850 0    60   ~ 0
LATCH_DATA_8*
Text Label 1950 2750 0    60   ~ 0
LATCH_DATA_16*
Text Label 1950 2650 0    60   ~ 0
LATCH_DATA_8
Text Label 1950 2550 0    60   ~ 0
GND
Text Label 1950 2950 0    60   ~ 0
B-SIXTN*
$EndSCHEMATC
