EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:contrib
LIBS:valves
LIBS:o_zilog
LIBS:N8VEM-KiCAD
LIBS:dips-s
LIBS:HD15_B
LIBS:lumiled
LIBS:o_memory
LIBS:as6c4008
LIBS:s100_SBC-cache
LIBS:mini_din_6
LIBS:s100_Z80 V2-cache
LIBS:VGA V11-cache
LIBS:o_intel
LIBS:s100_SBC-cache
EELAYER 27 0
EELAYER END
$Descr E 44000 34000
encoding utf-8
Sheet 1 1
Title "noname.sch"
Date "20 oct 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L VCC #PWR01
U 1 1 4EE510DA
P 14850 3250
F 0 "#PWR01" H 14850 3350 30  0001 C CNN
F 1 "VCC" H 14850 3350 30  0000 C CNN
F 2 "" H 14850 3250 60  0001 C CNN
F 3 "" H 14850 3250 60  0001 C CNN
	1    14850 3250
	1    0    0    -1  
$EndComp
Text Label 13000 2950 0    60   ~ 0
VCC
Text Label 13650 5000 0    60   ~ 0
GND
$Comp
L CONN_3 JP8
U 1 1 4C6C6E30
P 12600 3050
F 0 "JP8" H 12600 2850 60  0000 C CNN
F 1 "CONN_3" V 12600 3050 40  0000 C CNN
F 2 "SIL-3" V 12700 3050 40  0001 C CNN
F 3 "" H 12600 3050 60  0001 C CNN
	1    12600 3050
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR02
U 1 1 4C6C6DB2
P 15450 4600
F 0 "#PWR02" H 15450 4700 30  0001 C CNN
F 1 "VCC" H 15450 4700 30  0000 C CNN
F 2 "" H 15450 4600 60  0001 C CNN
F 3 "" H 15450 4600 60  0001 C CNN
	1    15450 4600
	1    0    0    -1  
$EndComp
$Comp
L R R40
U 1 1 4C6C6DAA
P 15450 4850
F 0 "R40" V 15530 4850 50  0000 C CNN
F 1 "1000" V 15450 4850 50  0000 C CNN
F 2 "R3" V 15550 4850 50  0001 C CNN
F 3 "" H 15450 4850 60  0001 C CNN
	1    15450 4850
	1    0    0    -1  
$EndComp
Text Label 11850 4150 0    60   ~ 0
VCC
Text Label 11850 4350 0    60   ~ 0
A12
Text Label 4700 2350 0    60   ~ 0
PU5K-I
$Comp
L JUMPER JP7
U 1 1 4C682A0C
P 2050 1250
F 0 "JP7" H 2050 1400 60  0000 C CNN
F 1 "JUMPER" H 2050 1170 40  0000 C CNN
F 2 "SIL-2" H 2050 1270 40  0001 C CNN
F 3 "" H 2050 1250 60  0001 C CNN
	1    2050 1250
	0    -1   -1   0   
$EndComp
$Comp
L JUMPER JP6
U 1 1 4C6829B4
P 1750 3500
F 0 "JP6" H 1750 3650 60  0000 C CNN
F 1 "JUMPER" H 1750 3420 40  0000 C CNN
F 2 "SIL-2" H 1750 3520 40  0001 C CNN
F 3 "" H 1750 3500 60  0001 C CNN
	1    1750 3500
	0    -1   -1   0   
$EndComp
Text Label 15900 12550 0    60   ~ 0
ROM_SELECT*
Text Label 16750 12100 0    60   ~ 0
ROM_SELECT
Text Label 7850 19050 0    60   ~ 0
pSYNC_RAW
Text Label 11500 24000 0    60   ~ 0
pSYNC_RAW
Text Label 7500 22450 0    60   ~ 0
START_SYNC
Text Label 8100 22550 0    60   ~ 0
END_SYNC
$Comp
L VCC #PWR03
U 1 1 4C6823A9
P 6850 18300
F 0 "#PWR03" H 6850 18400 30  0001 C CNN
F 1 "VCC" H 6850 18400 30  0000 C CNN
F 2 "" H 6850 18300 60  0001 C CNN
F 3 "" H 6850 18300 60  0001 C CNN
	1    6850 18300
	1    0    0    -1  
$EndComp
$Comp
L R R39
U 1 1 4C682395
P 6850 18550
F 0 "R39" V 6930 18550 50  0000 C CNN
F 1 "4700" V 6850 18550 50  0000 C CNN
F 2 "R3" V 6950 18550 50  0001 C CNN
F 3 "" H 6850 18550 60  0001 C CNN
	1    6850 18550
	1    0    0    -1  
$EndComp
$Comp
L CONN_2X2 P37
U 1 1 4C682367
P 7400 19000
F 0 "P37" H 7400 19150 50  0000 C CNN
F 1 "CONN_2X2" H 7410 18870 40  0000 C CNN
F 2 "PIN_ARRAY_2X2" H 7410 18970 40  0001 C CNN
F 3 "" H 7400 19000 60  0001 C CNN
	1    7400 19000
	1    0    0    -1  
$EndComp
Text Label 10300 21550 0    60   ~ 0
BUS_IN
Text Label 1800 21800 0    60   ~ 0
IORQ
Text Label 9600 27450 0    60   ~ 0
sINTA
$Comp
L CONN_3X2 P36
U 1 1 4C681EBF
P 10500 27600
F 0 "P36" H 10500 27850 50  0000 C CNN
F 1 "CONN_3X2" V 10500 27650 40  0000 C CNN
F 2 "pin_array_3x2" V 10600 27650 40  0001 C CNN
F 3 "" H 10500 27600 60  0001 C CNN
	1    10500 27600
	1    0    0    -1  
$EndComp
NoConn ~ 15950 29050
NoConn ~ 12350 29050
NoConn ~ 8750 29050
NoConn ~ 14300 30100
NoConn ~ 10700 30100
NoConn ~ 7100 30100
NoConn ~ 17250 22550
Text Label 18000 23900 0    60   ~ 0
READ_STROBE
Text Label 20450 13550 0    60   ~ 0
ERROR*
Text Label 20450 13450 0    60   ~ 0
PHANTOM*
Text Label 20450 13350 0    60   ~ 0
SIXTN*
Text Label 19900 13450 0    60   ~ 0
PU1K-Z
Text Label 19900 13550 0    60   ~ 0
PU1K-AA
Text Label 19900 13350 0    60   ~ 0
PU1K-JJ
Text Label 19950 14700 0    60   ~ 0
PU1K-II
Text Label 19950 14600 0    60   ~ 0
PU1K-HH
Text Label 19950 14500 0    60   ~ 0
PU1K-GG
Text Label 19950 14400 0    60   ~ 0
PU1K-FF
Text Label 19950 14300 0    60   ~ 0
PU1K-EE
Text Label 19950 14200 0    60   ~ 0
PU1K-DD
Text Label 19950 14100 0    60   ~ 0
PU1K-CC
Text Label 19950 14000 0    60   ~ 0
PU1K-BB
Text Label 20550 14000 0    60   ~ 0
VI0*
Text Label 20550 14100 0    60   ~ 0
VI1*
Text Label 20550 14200 0    60   ~ 0
VI2*
Text Label 20550 14300 0    60   ~ 0
VI3*
Text Label 20550 14400 0    60   ~ 0
VI4*
Text Label 20550 14500 0    60   ~ 0
VI5*
Text Label 20550 14600 0    60   ~ 0
VI6*
Text Label 20550 14700 0    60   ~ 0
VI7*
$Comp
L RR9 RR?
U 1 1 4BBBC67C
P 18650 13850
AR Path="/31324BBBC67C" Ref="RR?"  Part="1" 
AR Path="/2606084BBBC67C" Ref="RR9"  Part="1" 
AR Path="/433637434BBBC67C" Ref="RR9"  Part="1" 
AR Path="/4BBBC67C" Ref="RR9"  Part="1" 
AR Path="/FFFFFFFF4BBBC67C" Ref="RR9"  Part="1" 
AR Path="/773F65F14BBBC67C" Ref="RR9"  Part="1" 
AR Path="/D058E04BBBC67C" Ref="RR9"  Part="1" 
AR Path="/D1C3384BBBC67C" Ref="RR9"  Part="1" 
AR Path="/23C6504BBBC67C" Ref="RR9"  Part="1" 
AR Path="/22A24BBBC67C" Ref="RR9"  Part="1" 
AR Path="/24BBBC67C" Ref="RR9"  Part="1" 
AR Path="/23BC884BBBC67C" Ref="RR9"  Part="1" 
AR Path="/6FE934E34BBBC67C" Ref="RR9"  Part="1" 
AR Path="/D8F94BBBC67C" Ref="RR9"  Part="1" 
F 0 "RR9" H 18700 14450 70  0000 C CNN
F 1 "1000" V 18680 13850 70  0000 C CNN
F 2 "r_pack9" V 18780 13850 70  0001 C CNN
F 3 "" H 18650 13850 60  0001 C CNN
	1    18650 13850
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR04
U 1 1 4BBBC67B
P 18300 13350
AR Path="/4BBBC67B" Ref="#PWR04"  Part="1" 
AR Path="/FFFFFFFF4BBBC67B" Ref="#PWR01"  Part="1" 
AR Path="/773F65F14BBBC67B" Ref="#PWR01"  Part="1" 
AR Path="/D058E04BBBC67B" Ref="#PWR01"  Part="1" 
AR Path="/D1C3384BBBC67B" Ref="#PWR01"  Part="1" 
AR Path="/6FF0DD404BBBC67B" Ref="#PWR01"  Part="1" 
AR Path="/23BC884BBBC67B" Ref="#PWR01"  Part="1" 
F 0 "#PWR04" H 18300 13450 30  0001 C CNN
F 1 "VCC" H 18300 13450 30  0000 C CNN
F 2 "" H 18300 13350 60  0001 C CNN
F 3 "" H 18300 13350 60  0001 C CNN
	1    18300 13350
	1    0    0    -1  
$EndComp
Text Label 17750 13450 0    60   ~ 0
PU1K-BB
Text Label 17750 13550 0    60   ~ 0
PU1K-CC
Text Label 17750 13650 0    60   ~ 0
PU1K-DD
Text Label 17750 13750 0    60   ~ 0
PU1K-EE
Text Label 17750 13850 0    60   ~ 0
PU1K-FF
Text Label 17750 13950 0    60   ~ 0
PU1K-GG
Text Label 17750 14050 0    60   ~ 0
PU1K-HH
Text Label 17750 14150 0    60   ~ 0
PU1K-II
Text Label 17750 14250 0    60   ~ 0
PU1K-JJ
$Comp
L VCC #PWR?
U 1 1 4BBA7D1E
P 11400 22300
AR Path="/384BBA7D1E" Ref="#PWR?"  Part="1" 
AR Path="/4BBA7D1E" Ref="#PWR05"  Part="1" 
AR Path="/6FF0DD404BBA7D1E" Ref="#PWR02"  Part="1" 
AR Path="/773F65F14BBA7D1E" Ref="#PWR02"  Part="1" 
AR Path="/48414BBA7D1E" Ref="#PWR01"  Part="1" 
AR Path="/23BC884BBA7D1E" Ref="#PWR02"  Part="1" 
AR Path="/FFFFFFFF4BBA7D1E" Ref="#PWR02"  Part="1" 
AR Path="/23C34C4BBA7D1E" Ref="#PWR01"  Part="1" 
AR Path="/D058E04BBA7D1E" Ref="#PWR02"  Part="1" 
AR Path="/D1C3384BBA7D1E" Ref="#PWR02"  Part="1" 
F 0 "#PWR05" H 11400 22400 30  0001 C CNN
F 1 "VCC" H 11400 22400 30  0000 C CNN
F 2 "" H 11400 22300 60  0001 C CNN
F 3 "" H 11400 22300 60  0001 C CNN
	1    11400 22300
	1    0    0    -1  
$EndComp
Text Label 19950 15100 0    60   ~ 0
PU1K-U
Text Label 19950 15200 0    60   ~ 0
PU1K-V
Text Label 19950 15300 0    60   ~ 0
PU1K-W
Text Label 19950 15400 0    60   ~ 0
PU1K-X
Text Label 19950 15500 0    60   ~ 0
PU1K-Y
Text Label 20800 19450 0    60   ~ 0
PU1K-T
Text Label 20800 18000 0    60   ~ 0
PU1K-S
Text Label 16300 15400 0    60   ~ 0
PU1K-AA
Text Label 16300 15300 0    60   ~ 0
PU1K-Z
Text Label 16300 15200 0    60   ~ 0
PU1K-Y
Text Label 16300 15100 0    60   ~ 0
PU1K-X
Text Label 16300 15000 0    60   ~ 0
PU1K-W
Text Label 16300 14900 0    60   ~ 0
PU1K-V
Text Label 16300 14800 0    60   ~ 0
PU1K-U
Text Label 16300 14700 0    60   ~ 0
PU1K-T
Text Label 16300 14600 0    60   ~ 0
PU1K-S
$Comp
L VCC #PWR06
U 1 1 4BBA7AFA
P 16850 14500
AR Path="/4BBA7AFA" Ref="#PWR06"  Part="1" 
AR Path="/6FF0DD404BBA7AFA" Ref="#PWR03"  Part="1" 
AR Path="/773F65F14BBA7AFA" Ref="#PWR03"  Part="1" 
AR Path="/48414BBA7AFA" Ref="#PWR02"  Part="1" 
AR Path="/23BC884BBA7AFA" Ref="#PWR03"  Part="1" 
AR Path="/FFFFFFFF4BBA7AFA" Ref="#PWR03"  Part="1" 
AR Path="/23C34C4BBA7AFA" Ref="#PWR02"  Part="1" 
AR Path="/D058E04BBA7AFA" Ref="#PWR03"  Part="1" 
AR Path="/D1C3384BBA7AFA" Ref="#PWR03"  Part="1" 
F 0 "#PWR06" H 16850 14600 30  0001 C CNN
F 1 "VCC" H 16850 14600 30  0000 C CNN
F 2 "" H 16850 14500 60  0001 C CNN
F 3 "" H 16850 14500 60  0001 C CNN
	1    16850 14500
	1    0    0    -1  
$EndComp
$Comp
L RR9 RR?
U 1 1 4BBA7AF9
P 17200 15000
AR Path="/679E884BBA7AF9" Ref="RR?"  Part="1" 
AR Path="/7E42B4154BBA7AF9" Ref="RR8"  Part="1" 
AR Path="/374146394BBA7AF9" Ref="RR8"  Part="1" 
AR Path="/773F65F14BBA7AF9" Ref="RR8"  Part="1" 
AR Path="/23C6504BBA7AF9" Ref="RR8"  Part="1" 
AR Path="/453838314BBA7AF9" Ref="RR8"  Part="1" 
AR Path="/23C34C4BBA7AF9" Ref="RR8"  Part="1" 
AR Path="/48414BBA7AF9" Ref="RR8"  Part="1" 
AR Path="/24BBA7AF9" Ref="RR8"  Part="1" 
AR Path="/23BC884BBA7AF9" Ref="RR8"  Part="1" 
AR Path="/4BBA7AF9" Ref="RR8"  Part="1" 
AR Path="/FFFFFFFF4BBA7AF9" Ref="RR8"  Part="1" 
AR Path="/D058E04BBA7AF9" Ref="RR8"  Part="1" 
AR Path="/D1C3384BBA7AF9" Ref="RR8"  Part="1" 
F 0 "RR8" H 17250 15600 70  0000 C CNN
F 1 "1000" V 17230 15000 70  0000 C CNN
F 2 "r_pack9" V 17330 15000 70  0001 C CNN
F 3 "" H 17200 15000 60  0001 C CNN
	1    17200 15000
	1    0    0    -1  
$EndComp
Text Label 5150 24300 0    60   ~ 0
PU1K-R
$Comp
L C C?
U 1 1 4BB795E0
P 37850 23450
AR Path="/23D6944BB795E0" Ref="C?"  Part="1" 
AR Path="/7E42B4154BB795E0" Ref="C63"  Part="1" 
AR Path="/393545304BB795E0" Ref="C63"  Part="1" 
AR Path="/23C34C4BB795E0" Ref="C63"  Part="1" 
AR Path="/2F4A4BB795E0" Ref="C63"  Part="1" 
AR Path="/24BB795E0" Ref="C63"  Part="1" 
AR Path="/23BC884BB795E0" Ref="C63"  Part="1" 
AR Path="/773F8EB44BB795E0" Ref="C63"  Part="1" 
AR Path="/23C9F04BB795E0" Ref="C63"  Part="1" 
AR Path="/94BB795E0" Ref="C"  Part="1" 
AR Path="/4BB795E0" Ref="C63"  Part="1" 
AR Path="/773F65F14BB795E0" Ref="C63"  Part="1" 
AR Path="/6FE934E34BB795E0" Ref="C63"  Part="1" 
AR Path="/FFFFFFFF4BB795E0" Ref="C63"  Part="1" 
AR Path="/23D9004BB795E0" Ref="C63"  Part="1" 
AR Path="/23CBC44BB795E0" Ref="C63"  Part="1" 
AR Path="/2824BB795E0" Ref="C63"  Part="1" 
AR Path="/69549BC04BB795E0" Ref="C63"  Part="1" 
AR Path="/14BB795E0" Ref="C63"  Part="1" 
AR Path="/D058E04BB795E0" Ref="C63"  Part="1" 
AR Path="/D1C3384BB795E0" Ref="C63"  Part="1" 
F 0 "C63" H 37900 23550 50  0000 L CNN
F 1 "0.1 uF" H 37900 23350 50  0000 L CNN
F 2 "C2" H 37850 23450 60  0001 C CNN
F 3 "" H 37850 23450 60  0001 C CNN
	1    37850 23450
	1    0    0    -1  
$EndComp
Text Label 3350 2000 0    60   ~ 0
PU5K-G
Text Label 16700 21800 0    60   ~ 0
PU5K-F
Text Label 15450 2700 0    60   ~ 0
PU5K-E
Text Label 16700 18450 0    60   ~ 0
PU5K-D
Text Label 8200 18850 0    60   ~ 0
PU5K-C
Text Label 15500 16500 0    60   ~ 0
PU5K-B
Text Label 18850 18250 0    60   ~ 0
PU5K-A
$Comp
L RR9 RR?
U 1 1 4BB786CB
P 15800 15000
AR Path="/31324BB786CB" Ref="RR?"  Part="1" 
AR Path="/695513124BB786CB" Ref="RR?"  Part="1" 
AR Path="/4BB786CB" Ref="RR7"  Part="1" 
AR Path="/FFFFFFFF4BB786CB" Ref="RR7"  Part="1" 
AR Path="/773F8EB44BB786CB" Ref="RR7"  Part="1" 
AR Path="/23C9F04BB786CB" Ref="RR7"  Part="1" 
AR Path="/94BB786CB" Ref="RR"  Part="1" 
AR Path="/773F65F14BB786CB" Ref="RR7"  Part="1" 
AR Path="/23C6504BB786CB" Ref="RR7"  Part="1" 
AR Path="/23C34C4BB786CB" Ref="RR7"  Part="1" 
AR Path="/2F4A4BB786CB" Ref="RR7"  Part="1" 
AR Path="/24BB786CB" Ref="RR7"  Part="1" 
AR Path="/23BC884BB786CB" Ref="RR7"  Part="1" 
AR Path="/6FE934E34BB786CB" Ref="RR7"  Part="1" 
AR Path="/23D9004BB786CB" Ref="RR7"  Part="1" 
AR Path="/23CBC44BB786CB" Ref="RR7"  Part="1" 
AR Path="/2824BB786CB" Ref="RR7"  Part="1" 
AR Path="/69549BC04BB786CB" Ref="RR7"  Part="1" 
AR Path="/14BB786CB" Ref="RR7"  Part="1" 
AR Path="/D058E04BB786CB" Ref="RR7"  Part="1" 
AR Path="/D1C3384BB786CB" Ref="RR7"  Part="1" 
F 0 "RR7" H 15850 15600 70  0000 C CNN
F 1 "4700" V 15830 15000 70  0000 C CNN
F 2 "r_pack9" V 15930 15000 70  0001 C CNN
F 3 "" H 15800 15000 60  0001 C CNN
	1    15800 15000
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR04
U 1 1 4BB786CA
P 15450 14500
AR Path="/FFFFFFFF4BB786CA" Ref="#PWR04"  Part="1" 
AR Path="/4BB786CA" Ref="#PWR07"  Part="1" 
AR Path="/773F8EB44BB786CA" Ref="#PWR01"  Part="1" 
AR Path="/23C9F04BB786CA" Ref="#PWR39"  Part="1" 
AR Path="/94BB786CA" Ref="#PWR"  Part="1" 
AR Path="/773F65F14BB786CA" Ref="#PWR04"  Part="1" 
AR Path="/23C34C4BB786CA" Ref="#PWR03"  Part="1" 
AR Path="/23BC884BB786CA" Ref="#PWR04"  Part="1" 
AR Path="/6FE934E34BB786CA" Ref="#PWR01"  Part="1" 
AR Path="/23D9004BB786CA" Ref="#PWR01"  Part="1" 
AR Path="/23CBC44BB786CA" Ref="#PWR01"  Part="1" 
AR Path="/2824BB786CA" Ref="#PWR01"  Part="1" 
AR Path="/69549BC04BB786CA" Ref="#PWR01"  Part="1" 
AR Path="/6684D64BB786CA" Ref="#PWR01"  Part="1" 
AR Path="/14BB786CA" Ref="#PWR01"  Part="1" 
AR Path="/24BB786CA" Ref="#PWR39"  Part="1" 
AR Path="/6FF0DD404BB786CA" Ref="#PWR04"  Part="1" 
AR Path="/D058E04BB786CA" Ref="#PWR04"  Part="1" 
AR Path="/D1C3384BB786CA" Ref="#PWR04"  Part="1" 
F 0 "#PWR07" H 15450 14600 30  0001 C CNN
F 1 "VCC" H 15450 14600 30  0000 C CNN
F 2 "" H 15450 14500 60  0001 C CNN
F 3 "" H 15450 14500 60  0001 C CNN
	1    15450 14500
	1    0    0    -1  
$EndComp
Text Label 14900 14600 0    60   ~ 0
PU5K-A
Text Label 14900 14700 0    60   ~ 0
PU5K-B
Text Label 14900 14800 0    60   ~ 0
PU5K-C
Text Label 14900 14900 0    60   ~ 0
PU5K-D
Text Label 14900 15000 0    60   ~ 0
PU5K-E
Text Label 14900 15100 0    60   ~ 0
PU5K-F
Text Label 14900 15200 0    60   ~ 0
PU5K-G
Text Label 14900 15300 0    60   ~ 0
PU5K-H
Text Label 14900 15400 0    60   ~ 0
PU5K-I
Text Label 6300 2200 0    60   ~ 0
PU1K-Q
Text Label 1250 700  0    60   ~ 0
PU1K-P
Text Label 1250 2200 0    60   ~ 0
PU1K-O
Text Label 11700 24950 0    60   ~ 0
PU1K-N
Text Label 19000 30850 0    60   ~ 0
PU1K-M
Text Label 18100 12100 0    60   ~ 0
PU1K-A
Text Label 20800 19900 0    60   ~ 0
PU1K-K
Text Label 18000 18600 0    60   ~ 0
PU1K-J
$Comp
L RR9 RR6
U 1 1 4BB78526
P 17200 13900
AR Path="/773F8EB44BB78526" Ref="RR6"  Part="1" 
AR Path="/23C9F04BB78526" Ref="RR6"  Part="1" 
AR Path="/94BB78526" Ref="RR"  Part="1" 
AR Path="/4BB78526" Ref="RR6"  Part="1" 
AR Path="/64BB78526" Ref="RR6"  Part="1" 
AR Path="/6FE934344BB78526" Ref="RR6"  Part="1" 
AR Path="/FFFFFFFF4BB78526" Ref="RR6"  Part="1" 
AR Path="/24BB78526" Ref="RR6"  Part="1" 
AR Path="/773F65F14BB78526" Ref="RR6"  Part="1" 
AR Path="/23C6504BB78526" Ref="RR6"  Part="1" 
AR Path="/23C34C4BB78526" Ref="RR6"  Part="1" 
AR Path="/23BC884BB78526" Ref="RR6"  Part="1" 
AR Path="/6FE934E34BB78526" Ref="RR6"  Part="1" 
AR Path="/23D9004BB78526" Ref="RR6"  Part="1" 
AR Path="/23CBC44BB78526" Ref="RR6"  Part="1" 
AR Path="/2824BB78526" Ref="RR6"  Part="1" 
AR Path="/69549BC04BB78526" Ref="RR6"  Part="1" 
AR Path="/14BB78526" Ref="RR6"  Part="1" 
AR Path="/D1C3384BB78526" Ref="RR6"  Part="1" 
AR Path="/D058E04BB78526" Ref="RR6"  Part="1" 
F 0 "RR6" H 17250 14500 70  0000 C CNN
F 1 "1000" V 17230 13900 70  0000 C CNN
F 2 "r_pack9" V 17330 13900 70  0001 C CNN
F 3 "" H 17200 13900 60  0001 C CNN
	1    17200 13900
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR02
U 1 1 4BB78525
P 16850 13400
AR Path="/773F8EB44BB78525" Ref="#PWR02"  Part="1" 
AR Path="/23C9F04BB78525" Ref="#PWR35"  Part="1" 
AR Path="/94BB78525" Ref="#PWR"  Part="1" 
AR Path="/4BB78525" Ref="#PWR08"  Part="1" 
AR Path="/64BB78525" Ref="#PWR44"  Part="1" 
AR Path="/6FE934344BB78525" Ref="#PWR44"  Part="1" 
AR Path="/FFFFFFFF4BB78525" Ref="#PWR05"  Part="1" 
AR Path="/24BB78525" Ref="#PWR35"  Part="1" 
AR Path="/773F65F14BB78525" Ref="#PWR05"  Part="1" 
AR Path="/23C34C4BB78525" Ref="#PWR04"  Part="1" 
AR Path="/23BC884BB78525" Ref="#PWR05"  Part="1" 
AR Path="/6FE934E34BB78525" Ref="#PWR02"  Part="1" 
AR Path="/23D9004BB78525" Ref="#PWR02"  Part="1" 
AR Path="/23CBC44BB78525" Ref="#PWR02"  Part="1" 
AR Path="/2824BB78525" Ref="#PWR02"  Part="1" 
AR Path="/69549BC04BB78525" Ref="#PWR02"  Part="1" 
AR Path="/6684D64BB78525" Ref="#PWR02"  Part="1" 
AR Path="/14BB78525" Ref="#PWR02"  Part="1" 
AR Path="/6FF0DD404BB78525" Ref="#PWR05"  Part="1" 
AR Path="/D1C3384BB78525" Ref="#PWR05"  Part="1" 
AR Path="/D058E04BB78525" Ref="#PWR05"  Part="1" 
F 0 "#PWR08" H 16850 13500 30  0001 C CNN
F 1 "VCC" H 16850 13500 30  0000 C CNN
F 2 "" H 16850 13400 60  0001 C CNN
F 3 "" H 16850 13400 60  0001 C CNN
	1    16850 13400
	1    0    0    -1  
$EndComp
Text Label 16300 13500 0    60   ~ 0
PU1K-J
Text Label 16300 13600 0    60   ~ 0
PU1K-K
Text Label 16300 13700 0    60   ~ 0
PU1K-L
Text Label 16300 13800 0    60   ~ 0
PU1K-M
Text Label 16300 13900 0    60   ~ 0
PU1K-N
Text Label 16300 14000 0    60   ~ 0
PU1K-O
Text Label 16300 14100 0    60   ~ 0
PU1K-P
Text Label 16300 14200 0    60   ~ 0
PU1K-Q
Text Label 16300 14300 0    60   ~ 0
PU1K-R
Text Label 950  3550 0    60   ~ 0
PU1K-I
Text Label 20800 18500 0    60   ~ 0
PU1K-H
Text Label 20800 18950 0    60   ~ 0
PU1K-G
Text Label 19450 25300 0    60   ~ 0
PU1K-F
Text Label 18550 15750 0    60   ~ 0
PU1K-E
Text Label 18550 16250 0    60   ~ 0
PU1K-D
Text Label 19450 16350 0    60   ~ 0
PU1K-C
Text Label 19300 26150 0    60   ~ 0
PU1K-B
Text Label 7200 23350 0    60   ~ 0
PU1K-L
Text Label 14900 14300 0    60   ~ 0
PU1K-I
Text Label 14900 14200 0    60   ~ 0
PU1K-H
Text Label 14900 14100 0    60   ~ 0
PU1K-G
Text Label 14900 14000 0    60   ~ 0
PU1K-F
Text Label 14900 13900 0    60   ~ 0
PU1K-E
Text Label 14900 13800 0    60   ~ 0
PU1K-D
Text Label 14900 13700 0    60   ~ 0
PU1K-C
Text Label 14900 13600 0    60   ~ 0
PU1K-B
Text Label 14900 13500 0    60   ~ 0
PU1K-A
$Comp
L VCC #PWR?
U 1 1 4BB7831D
P 15450 13400
AR Path="/2300384BB7831D" Ref="#PWR?"  Part="1" 
AR Path="/4BB7831D" Ref="#PWR09"  Part="1" 
AR Path="/69549BC04BB7831D" Ref="#PWR03"  Part="1" 
AR Path="/FFFFFFFF4BB7831D" Ref="#PWR06"  Part="1" 
AR Path="/24BB7831D" Ref="#PWR33"  Part="1" 
AR Path="/773F8EB44BB7831D" Ref="#PWR03"  Part="1" 
AR Path="/23C9F04BB7831D" Ref="#PWR33"  Part="1" 
AR Path="/94BB7831D" Ref="#PWR"  Part="1" 
AR Path="/773F65F14BB7831D" Ref="#PWR06"  Part="1" 
AR Path="/64BB7831D" Ref="#PWR38"  Part="1" 
AR Path="/6FE934344BB7831D" Ref="#PWR38"  Part="1" 
AR Path="/23C34C4BB7831D" Ref="#PWR05"  Part="1" 
AR Path="/23BC884BB7831D" Ref="#PWR06"  Part="1" 
AR Path="/6FE934E34BB7831D" Ref="#PWR03"  Part="1" 
AR Path="/23D9004BB7831D" Ref="#PWR03"  Part="1" 
AR Path="/23CBC44BB7831D" Ref="#PWR03"  Part="1" 
AR Path="/2824BB7831D" Ref="#PWR03"  Part="1" 
AR Path="/6684D64BB7831D" Ref="#PWR03"  Part="1" 
AR Path="/14BB7831D" Ref="#PWR03"  Part="1" 
AR Path="/6FF0DD404BB7831D" Ref="#PWR06"  Part="1" 
AR Path="/D1C3384BB7831D" Ref="#PWR06"  Part="1" 
AR Path="/D058E04BB7831D" Ref="#PWR06"  Part="1" 
F 0 "#PWR09" H 15450 13500 30  0001 C CNN
F 1 "VCC" H 15450 13500 30  0000 C CNN
F 2 "" H 15450 13400 60  0001 C CNN
F 3 "" H 15450 13400 60  0001 C CNN
	1    15450 13400
	1    0    0    -1  
$EndComp
$Comp
L RR9 RR?
U 1 1 4BB7830E
P 15800 13900
AR Path="/A500384BB7830E" Ref="RR?"  Part="1" 
AR Path="/4BB7830E" Ref="RR5"  Part="1" 
AR Path="/380031324BB7830E" Ref="RR?"  Part="1" 
AR Path="/69549BC04BB7830E" Ref="RR5"  Part="1" 
AR Path="/FFFFFFFF4BB7830E" Ref="RR5"  Part="1" 
AR Path="/24BB7830E" Ref="RR5"  Part="1" 
AR Path="/773F8EB44BB7830E" Ref="RR5"  Part="1" 
AR Path="/23C9F04BB7830E" Ref="RR5"  Part="1" 
AR Path="/94BB7830E" Ref="RR"  Part="1" 
AR Path="/773F65F14BB7830E" Ref="RR5"  Part="1" 
AR Path="/23C6504BB7830E" Ref="RR5"  Part="1" 
AR Path="/64BB7830E" Ref="RR5"  Part="1" 
AR Path="/6FE934344BB7830E" Ref="RR5"  Part="1" 
AR Path="/23C34C4BB7830E" Ref="RR5"  Part="1" 
AR Path="/23BC884BB7830E" Ref="RR5"  Part="1" 
AR Path="/6FE934E34BB7830E" Ref="RR5"  Part="1" 
AR Path="/23D9004BB7830E" Ref="RR5"  Part="1" 
AR Path="/23CBC44BB7830E" Ref="RR5"  Part="1" 
AR Path="/2824BB7830E" Ref="RR5"  Part="1" 
AR Path="/14BB7830E" Ref="RR5"  Part="1" 
AR Path="/D1C3384BB7830E" Ref="RR5"  Part="1" 
AR Path="/D058E04BB7830E" Ref="RR5"  Part="1" 
F 0 "RR5" H 15850 14500 70  0000 C CNN
F 1 "1000" V 15830 13900 70  0000 C CNN
F 2 "r_pack9" V 15930 13900 70  0001 C CNN
F 3 "" H 15800 13900 60  0001 C CNN
	1    15800 13900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 4BB76F8C
P 36950 23700
AR Path="/773F8EB44BB76F8C" Ref="#PWR04"  Part="1" 
AR Path="/23C9F04BB76F8C" Ref="#PWR53"  Part="1" 
AR Path="/94BB76F8C" Ref="#PWR"  Part="1" 
AR Path="/4BB76F8C" Ref="#PWR010"  Part="1" 
AR Path="/23C34C4BB76F8C" Ref="#PWR06"  Part="1" 
AR Path="/23BC884BB76F8C" Ref="#PWR07"  Part="1" 
AR Path="/6FE934E34BB76F8C" Ref="#PWR04"  Part="1" 
AR Path="/FFFFFFFF4BB76F8C" Ref="#PWR07"  Part="1" 
AR Path="/773F65F14BB76F8C" Ref="#PWR07"  Part="1" 
AR Path="/23CE584BB76F8C" Ref="#PWR01"  Part="1" 
AR Path="/FFFFFFF04BB76F8C" Ref="#PWR71"  Part="1" 
AR Path="/24BB76F8C" Ref="#PWR53"  Part="1" 
AR Path="/64BB76F8C" Ref="#PWR64"  Part="1" 
AR Path="/6FE934344BB76F8C" Ref="#PWR64"  Part="1" 
AR Path="/23D9004BB76F8C" Ref="#PWR04"  Part="1" 
AR Path="/23CBC44BB76F8C" Ref="#PWR04"  Part="1" 
AR Path="/2824BB76F8C" Ref="#PWR04"  Part="1" 
AR Path="/69549BC04BB76F8C" Ref="#PWR04"  Part="1" 
AR Path="/6684D64BB76F8C" Ref="#PWR04"  Part="1" 
AR Path="/14BB76F8C" Ref="#PWR04"  Part="1" 
AR Path="/6FF0DD404BB76F8C" Ref="#PWR07"  Part="1" 
AR Path="/D1C3384BB76F8C" Ref="#PWR07"  Part="1" 
AR Path="/D058E04BB76F8C" Ref="#PWR07"  Part="1" 
F 0 "#PWR010" H 36950 23700 30  0001 C CNN
F 1 "GND" H 36950 23630 30  0001 C CNN
F 2 "" H 36950 23700 60  0001 C CNN
F 3 "" H 36950 23700 60  0001 C CNN
	1    36950 23700
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR05
U 1 1 4BB76F8B
P 36500 23250
AR Path="/773F8EB44BB76F8B" Ref="#PWR05"  Part="1" 
AR Path="/23C9F04BB76F8B" Ref="#PWR48"  Part="1" 
AR Path="/94BB76F8B" Ref="#PWR"  Part="1" 
AR Path="/4BB76F8B" Ref="#PWR011"  Part="1" 
AR Path="/23C34C4BB76F8B" Ref="#PWR07"  Part="1" 
AR Path="/23BC884BB76F8B" Ref="#PWR08"  Part="1" 
AR Path="/6FE934E34BB76F8B" Ref="#PWR05"  Part="1" 
AR Path="/FFFFFFFF4BB76F8B" Ref="#PWR08"  Part="1" 
AR Path="/773F65F14BB76F8B" Ref="#PWR08"  Part="1" 
AR Path="/23CE584BB76F8B" Ref="#PWR02"  Part="1" 
AR Path="/FFFFFFF04BB76F8B" Ref="#PWR66"  Part="1" 
AR Path="/24BB76F8B" Ref="#PWR48"  Part="1" 
AR Path="/64BB76F8B" Ref="#PWR59"  Part="1" 
AR Path="/6FE934344BB76F8B" Ref="#PWR59"  Part="1" 
AR Path="/23D9004BB76F8B" Ref="#PWR05"  Part="1" 
AR Path="/23CBC44BB76F8B" Ref="#PWR05"  Part="1" 
AR Path="/2824BB76F8B" Ref="#PWR05"  Part="1" 
AR Path="/69549BC04BB76F8B" Ref="#PWR05"  Part="1" 
AR Path="/6684D64BB76F8B" Ref="#PWR05"  Part="1" 
AR Path="/14BB76F8B" Ref="#PWR05"  Part="1" 
AR Path="/6FF0DD404BB76F8B" Ref="#PWR08"  Part="1" 
AR Path="/D1C3384BB76F8B" Ref="#PWR08"  Part="1" 
AR Path="/D058E04BB76F8B" Ref="#PWR08"  Part="1" 
F 0 "#PWR011" H 36500 23350 30  0001 C CNN
F 1 "VCC" H 36500 23350 30  0000 C CNN
F 2 "" H 36500 23250 60  0001 C CNN
F 3 "" H 36500 23250 60  0001 C CNN
	1    36500 23250
	1    0    0    -1  
$EndComp
$Comp
L C C60
U 1 1 4BB76F8A
P 36500 23450
AR Path="/773F8EB44BB76F8A" Ref="C60"  Part="1" 
AR Path="/23C9F04BB76F8A" Ref="C60"  Part="1" 
AR Path="/94BB76F8A" Ref="C"  Part="1" 
AR Path="/4BB76F8A" Ref="C60"  Part="1" 
AR Path="/23C34C4BB76F8A" Ref="C60"  Part="1" 
AR Path="/24BB76F8A" Ref="C60"  Part="1" 
AR Path="/23BC884BB76F8A" Ref="C60"  Part="1" 
AR Path="/6FE934E34BB76F8A" Ref="C60"  Part="1" 
AR Path="/FFFFFFFF4BB76F8A" Ref="C60"  Part="1" 
AR Path="/773F65F14BB76F8A" Ref="C60"  Part="1" 
AR Path="/23CE584BB76F8A" Ref="C60"  Part="1" 
AR Path="/FFFFFFF04BB76F8A" Ref="C60"  Part="1" 
AR Path="/64BB76F8A" Ref="C60"  Part="1" 
AR Path="/6FE934344BB76F8A" Ref="C60"  Part="1" 
AR Path="/23D9004BB76F8A" Ref="C60"  Part="1" 
AR Path="/23CBC44BB76F8A" Ref="C60"  Part="1" 
AR Path="/2824BB76F8A" Ref="C60"  Part="1" 
AR Path="/69549BC04BB76F8A" Ref="C60"  Part="1" 
AR Path="/14BB76F8A" Ref="C60"  Part="1" 
AR Path="/D1C3384BB76F8A" Ref="C60"  Part="1" 
AR Path="/D058E04BB76F8A" Ref="C60"  Part="1" 
F 0 "C60" H 36550 23550 50  0000 L CNN
F 1 "0.1 uF" H 36550 23350 50  0000 L CNN
F 2 "C2" H 36500 23450 60  0001 C CNN
F 3 "" H 36500 23450 60  0001 C CNN
	1    36500 23450
	1    0    0    -1  
$EndComp
$Comp
L C C61
U 1 1 4BB76F89
P 36950 23450
AR Path="/773F8EB44BB76F89" Ref="C61"  Part="1" 
AR Path="/23C9F04BB76F89" Ref="C61"  Part="1" 
AR Path="/94BB76F89" Ref="C"  Part="1" 
AR Path="/4BB76F89" Ref="C61"  Part="1" 
AR Path="/23C34C4BB76F89" Ref="C61"  Part="1" 
AR Path="/24BB76F89" Ref="C61"  Part="1" 
AR Path="/23BC884BB76F89" Ref="C61"  Part="1" 
AR Path="/FFFFFFFF4BB76F89" Ref="C61"  Part="1" 
AR Path="/773F65F14BB76F89" Ref="C61"  Part="1" 
AR Path="/6FE934E34BB76F89" Ref="C61"  Part="1" 
AR Path="/23CE584BB76F89" Ref="C61"  Part="1" 
AR Path="/FFFFFFF04BB76F89" Ref="C61"  Part="1" 
AR Path="/64BB76F89" Ref="C61"  Part="1" 
AR Path="/6FE934344BB76F89" Ref="C61"  Part="1" 
AR Path="/23D9004BB76F89" Ref="C61"  Part="1" 
AR Path="/23CBC44BB76F89" Ref="C61"  Part="1" 
AR Path="/2824BB76F89" Ref="C61"  Part="1" 
AR Path="/69549BC04BB76F89" Ref="C61"  Part="1" 
AR Path="/14BB76F89" Ref="C61"  Part="1" 
AR Path="/D1C3384BB76F89" Ref="C61"  Part="1" 
AR Path="/D058E04BB76F89" Ref="C61"  Part="1" 
F 0 "C61" H 37000 23550 50  0000 L CNN
F 1 "0.1 uF" H 37000 23350 50  0000 L CNN
F 2 "C2" H 36950 23450 60  0001 C CNN
F 3 "" H 36950 23450 60  0001 C CNN
	1    36950 23450
	1    0    0    -1  
$EndComp
$Comp
L C C62
U 1 1 4BB76F82
P 37400 23450
AR Path="/773F8EB44BB76F82" Ref="C62"  Part="1" 
AR Path="/23C9F04BB76F82" Ref="C62"  Part="1" 
AR Path="/94BB76F82" Ref="C"  Part="1" 
AR Path="/4BB76F82" Ref="C62"  Part="1" 
AR Path="/23C34C4BB76F82" Ref="C62"  Part="1" 
AR Path="/24BB76F82" Ref="C62"  Part="1" 
AR Path="/23BC884BB76F82" Ref="C62"  Part="1" 
AR Path="/FFFFFFFF4BB76F82" Ref="C62"  Part="1" 
AR Path="/773F65F14BB76F82" Ref="C62"  Part="1" 
AR Path="/6FE934E34BB76F82" Ref="C62"  Part="1" 
AR Path="/23CE584BB76F82" Ref="C62"  Part="1" 
AR Path="/FFFFFFF04BB76F82" Ref="C62"  Part="1" 
AR Path="/64BB76F82" Ref="C62"  Part="1" 
AR Path="/6FE934344BB76F82" Ref="C62"  Part="1" 
AR Path="/23D9004BB76F82" Ref="C62"  Part="1" 
AR Path="/23CBC44BB76F82" Ref="C62"  Part="1" 
AR Path="/2824BB76F82" Ref="C62"  Part="1" 
AR Path="/69549BC04BB76F82" Ref="C62"  Part="1" 
AR Path="/14BB76F82" Ref="C62"  Part="1" 
AR Path="/D1C3384BB76F82" Ref="C62"  Part="1" 
AR Path="/D058E04BB76F82" Ref="C62"  Part="1" 
F 0 "C62" H 37450 23550 50  0000 L CNN
F 1 "0.1 uF" H 37450 23350 50  0000 L CNN
F 2 "C2" H 37400 23450 60  0001 C CNN
F 3 "" H 37400 23450 60  0001 C CNN
	1    37400 23450
	1    0    0    -1  
$EndComp
NoConn ~ 41500 13050
NoConn ~ 41500 9250
$Comp
L GND #PWR?
U 1 1 4BB761C1
P 16000 11650
AR Path="/9F00384BB761C1" Ref="#PWR?"  Part="1" 
AR Path="/4BB761C1" Ref="#PWR012"  Part="1" 
AR Path="/FFFFFFFF4BB761C1" Ref="#PWR09"  Part="1" 
AR Path="/773F65F14BB761C1" Ref="#PWR09"  Part="1" 
AR Path="/23C6504BB761C1" Ref="#PWR01"  Part="1" 
AR Path="/14BB761C1" Ref="#PWR06"  Part="1" 
AR Path="/D1C1E84BB761C1" Ref="#PWR01"  Part="1" 
AR Path="/D058E04BB761C1" Ref="#PWR09"  Part="1" 
AR Path="/6684D64BB761C1" Ref="#PWR06"  Part="1" 
AR Path="/3C64BB761C1" Ref="#PWR01"  Part="1" 
AR Path="/23CBC44BB761C1" Ref="#PWR06"  Part="1" 
AR Path="/2F4A4BB761C1" Ref="#PWR01"  Part="1" 
AR Path="/23BC884BB761C1" Ref="#PWR09"  Part="1" 
AR Path="/23D9004BB761C1" Ref="#PWR06"  Part="1" 
AR Path="/773F8EB44BB761C1" Ref="#PWR06"  Part="1" 
AR Path="/23C9F04BB761C1" Ref="#PWR34"  Part="1" 
AR Path="/94BB761C1" Ref="#PWR"  Part="1" 
AR Path="/23C34C4BB761C1" Ref="#PWR08"  Part="1" 
AR Path="/23CE584BB761C1" Ref="#PWR03"  Part="1" 
AR Path="/FFFFFFF04BB761C1" Ref="#PWR43"  Part="1" 
AR Path="/24BB761C1" Ref="#PWR34"  Part="1" 
AR Path="/64BB761C1" Ref="#PWR41"  Part="1" 
AR Path="/6FE934344BB761C1" Ref="#PWR41"  Part="1" 
AR Path="/6FE934E34BB761C1" Ref="#PWR06"  Part="1" 
AR Path="/2824BB761C1" Ref="#PWR06"  Part="1" 
AR Path="/69549BC04BB761C1" Ref="#PWR06"  Part="1" 
AR Path="/6FF0DD404BB761C1" Ref="#PWR09"  Part="1" 
AR Path="/D1C3384BB761C1" Ref="#PWR09"  Part="1" 
F 0 "#PWR012" H 16000 11650 30  0001 C CNN
F 1 "GND" H 16000 11580 30  0001 C CNN
F 2 "" H 16000 11650 60  0001 C CNN
F 3 "" H 16000 11650 60  0001 C CNN
	1    16000 11650
	1    0    0    -1  
$EndComp
Text Label 14050 1600 0    60   ~ 0
ROM_SELECT*
$Comp
L 74LS05 U26
U 5 1 4BB757B6
P 7000 22900
AR Path="/384BB757B6" Ref="U26"  Part="1" 
AR Path="/4BB757B6" Ref="U26"  Part="5" 
AR Path="/7E428DAC4BB757B6" Ref="U?"  Part="1" 
AR Path="/2E44504BB757B6" Ref="U?"  Part="1" 
AR Path="/2EF093E4BB757B6" Ref="U"  Part="5" 
AR Path="/23D83C4BB757B6" Ref="U26"  Part="5" 
AR Path="/FFFFFFFF4BB757B6" Ref="U26"  Part="5" 
AR Path="/23CE444BB757B6" Ref="U26"  Part="5" 
AR Path="/47907FA4BB757B6" Ref="U26"  Part="5" 
AR Path="/23C34C4BB757B6" Ref="U26"  Part="5" 
AR Path="/14BB757B6" Ref="U26"  Part="5" 
AR Path="/23BC884BB757B6" Ref="U26"  Part="5" 
AR Path="/773F8EB44BB757B6" Ref="U26"  Part="5" 
AR Path="/94BB757B6" Ref="U"  Part="5" 
AR Path="/23C9F04BB757B6" Ref="U26"  Part="5" 
AR Path="/FFFFFFF04BB757B6" Ref="U26"  Part="5" 
AR Path="/2824BB757B6" Ref="U26"  Part="5" 
AR Path="/7E4188DA4BB757B6" Ref="U26"  Part="5" 
AR Path="/262F604BB757B6" Ref="U26"  Part="5" 
AR Path="/8D384BB757B6" Ref="U26"  Part="5" 
AR Path="/24BB757B6" Ref="U26"  Part="5" 
AR Path="/773F65F14BB757B6" Ref="U26"  Part="5" 
AR Path="/23C6504BB757B6" Ref="U26"  Part="5" 
AR Path="/D1C1E84BB757B6" Ref="U26"  Part="5" 
AR Path="/D058E04BB757B6" Ref="U26"  Part="5" 
AR Path="/3C64BB757B6" Ref="U26"  Part="5" 
AR Path="/23CBC44BB757B6" Ref="U26"  Part="5" 
AR Path="/2F4A4BB757B6" Ref="U26"  Part="5" 
AR Path="/23D9004BB757B6" Ref="U26"  Part="5" 
AR Path="/23CE584BB757B6" Ref="U26"  Part="5" 
AR Path="/64BB757B6" Ref="U26"  Part="5" 
AR Path="/6FE934344BB757B6" Ref="U26"  Part="5" 
AR Path="/6FE934E34BB757B6" Ref="U26"  Part="5" 
AR Path="/69549BC04BB757B6" Ref="U26"  Part="5" 
AR Path="/D1C3384BB757B6" Ref="U26"  Part="5" 
F 0 "U26" H 7195 23015 60  0000 C CNN
F 1 "74LS05" H 7190 22775 60  0000 C CNN
F 2 "14dip300" H 7190 22875 60  0001 C CNN
F 3 "" H 7000 22900 60  0001 C CNN
	5    7000 22900
	0    1    1    0   
$EndComp
$Comp
L 74LS05 U?
U 4 1 4BB757B1
P 17950 19900
AR Path="/9700384BB757B1" Ref="U?"  Part="1" 
AR Path="/4BB757B1" Ref="U26"  Part="4" 
AR Path="/353742364BB757B1" Ref="U?"  Part="1" 
AR Path="/2D0F704BB757B1" Ref="U?"  Part="1" 
AR Path="/D4C08524BB757B1" Ref="U"  Part="4" 
AR Path="/23D83C4BB757B1" Ref="U26"  Part="4" 
AR Path="/FFFFFFFF4BB757B1" Ref="U26"  Part="4" 
AR Path="/23CE444BB757B1" Ref="U26"  Part="4" 
AR Path="/47907FA4BB757B1" Ref="U26"  Part="4" 
AR Path="/23C34C4BB757B1" Ref="U26"  Part="4" 
AR Path="/14BB757B1" Ref="U26"  Part="4" 
AR Path="/23BC884BB757B1" Ref="U26"  Part="4" 
AR Path="/773F8EB44BB757B1" Ref="U26"  Part="4" 
AR Path="/94BB757B1" Ref="U"  Part="4" 
AR Path="/23C9F04BB757B1" Ref="U26"  Part="4" 
AR Path="/FFFFFFF04BB757B1" Ref="U26"  Part="4" 
AR Path="/2824BB757B1" Ref="U26"  Part="4" 
AR Path="/7E4188DA4BB757B1" Ref="U26"  Part="4" 
AR Path="/262F604BB757B1" Ref="U26"  Part="4" 
AR Path="/384BB757B1" Ref="U26"  Part="4" 
AR Path="/8D384BB757B1" Ref="U26"  Part="4" 
AR Path="/773F65F14BB757B1" Ref="U26"  Part="4" 
AR Path="/24BB757B1" Ref="U26"  Part="4" 
AR Path="/23C6504BB757B1" Ref="U26"  Part="4" 
AR Path="/D1C1E84BB757B1" Ref="U26"  Part="4" 
AR Path="/D058E04BB757B1" Ref="U26"  Part="4" 
AR Path="/3C64BB757B1" Ref="U26"  Part="4" 
AR Path="/23CBC44BB757B1" Ref="U26"  Part="4" 
AR Path="/2F4A4BB757B1" Ref="U26"  Part="4" 
AR Path="/23D9004BB757B1" Ref="U26"  Part="4" 
AR Path="/23CE584BB757B1" Ref="U26"  Part="4" 
AR Path="/64BB757B1" Ref="U26"  Part="4" 
AR Path="/6FE934344BB757B1" Ref="U26"  Part="4" 
AR Path="/6FE934E34BB757B1" Ref="U26"  Part="4" 
AR Path="/69549BC04BB757B1" Ref="U26"  Part="4" 
AR Path="/D1C3384BB757B1" Ref="U26"  Part="4" 
F 0 "U26" H 18145 20015 60  0000 C CNN
F 1 "74LS05" H 18140 19775 60  0000 C CNN
F 2 "14dip300" H 18140 19875 60  0001 C CNN
F 3 "" H 17950 19900 60  0001 C CNN
	4    17950 19900
	0    -1   -1   0   
$EndComp
$Comp
L 74LS02 U45
U 4 1 4BB75778
P 7200 20250
AR Path="/384BB75778" Ref="U45"  Part="1" 
AR Path="/4BB75778" Ref="U45"  Part="4" 
AR Path="/7E428DAC4BB75778" Ref="U45"  Part="1" 
AR Path="/2D03F04BB75778" Ref="U?"  Part="1" 
AR Path="/46708044BB75778" Ref="U"  Part="4" 
AR Path="/23D83C4BB75778" Ref="U45"  Part="4" 
AR Path="/FFFFFFFF4BB75778" Ref="U45"  Part="4" 
AR Path="/23CE444BB75778" Ref="U45"  Part="4" 
AR Path="/47907FA4BB75778" Ref="U45"  Part="4" 
AR Path="/23C34C4BB75778" Ref="U45"  Part="4" 
AR Path="/14BB75778" Ref="U45"  Part="4" 
AR Path="/23BC884BB75778" Ref="U45"  Part="4" 
AR Path="/773F8EB44BB75778" Ref="U45"  Part="4" 
AR Path="/94BB75778" Ref="U"  Part="4" 
AR Path="/23C9F04BB75778" Ref="U45"  Part="4" 
AR Path="/FFFFFFF04BB75778" Ref="U45"  Part="4" 
AR Path="/2824BB75778" Ref="U45"  Part="4" 
AR Path="/7E4188DA4BB75778" Ref="U45"  Part="4" 
AR Path="/262F604BB75778" Ref="U45"  Part="4" 
AR Path="/2FF09444BB75778" Ref="U"  Part="4" 
AR Path="/8D384BB75778" Ref="U45"  Part="4" 
AR Path="/24BB75778" Ref="U45"  Part="4" 
AR Path="/773F65F14BB75778" Ref="U45"  Part="4" 
AR Path="/23C6504BB75778" Ref="U45"  Part="4" 
AR Path="/D1C1E84BB75778" Ref="U45"  Part="4" 
AR Path="/D058E04BB75778" Ref="U45"  Part="4" 
AR Path="/3C64BB75778" Ref="U45"  Part="4" 
AR Path="/23CBC44BB75778" Ref="U45"  Part="4" 
AR Path="/2F4A4BB75778" Ref="U45"  Part="4" 
AR Path="/23D9004BB75778" Ref="U45"  Part="4" 
AR Path="/23CE584BB75778" Ref="U45"  Part="4" 
AR Path="/64BB75778" Ref="U45"  Part="4" 
AR Path="/6FE934344BB75778" Ref="U45"  Part="4" 
AR Path="/6FE934E34BB75778" Ref="U45"  Part="4" 
AR Path="/69549BC04BB75778" Ref="U45"  Part="4" 
AR Path="/D1C3384BB75778" Ref="U45"  Part="4" 
F 0 "U45" H 7200 20300 60  0000 C CNN
F 1 "74LS02" H 7200 20200 60  0000 C CNN
F 2 "14dip300" H 7200 20300 60  0001 C CNN
F 3 "" H 7200 20250 60  0001 C CNN
	4    7200 20250
	1    0    0    -1  
$EndComp
$Comp
L 74LS02 U?
U 3 1 4BB75771
P 6100 21600
AR Path="/2300384BB75771" Ref="U?"  Part="1" 
AR Path="/4BB75771" Ref="U45"  Part="3" 
AR Path="/353737384BB75771" Ref="U?"  Part="1" 
AR Path="/2D50084BB75771" Ref="U?"  Part="1" 
AR Path="/45409524BB75771" Ref="U"  Part="3" 
AR Path="/23D83C4BB75771" Ref="U45"  Part="3" 
AR Path="/FFFFFFFF4BB75771" Ref="U45"  Part="3" 
AR Path="/23CE444BB75771" Ref="U45"  Part="3" 
AR Path="/47907FA4BB75771" Ref="U45"  Part="3" 
AR Path="/23C34C4BB75771" Ref="U45"  Part="3" 
AR Path="/14BB75771" Ref="U45"  Part="3" 
AR Path="/23BC884BB75771" Ref="U45"  Part="3" 
AR Path="/773F8EB44BB75771" Ref="U45"  Part="3" 
AR Path="/94BB75771" Ref="U"  Part="3" 
AR Path="/23C9F04BB75771" Ref="U45"  Part="3" 
AR Path="/FFFFFFF04BB75771" Ref="U45"  Part="3" 
AR Path="/2824BB75771" Ref="U45"  Part="3" 
AR Path="/7E4188DA4BB75771" Ref="U45"  Part="3" 
AR Path="/86F08FC4BB75771" Ref="U"  Part="3" 
AR Path="/384BB75771" Ref="U45"  Part="3" 
AR Path="/8D384BB75771" Ref="U45"  Part="3" 
AR Path="/773F65F14BB75771" Ref="U45"  Part="3" 
AR Path="/24BB75771" Ref="U45"  Part="3" 
AR Path="/23C6504BB75771" Ref="U45"  Part="3" 
AR Path="/D1C1E84BB75771" Ref="U45"  Part="3" 
AR Path="/D058E04BB75771" Ref="U45"  Part="3" 
AR Path="/3C64BB75771" Ref="U45"  Part="3" 
AR Path="/23CBC44BB75771" Ref="U45"  Part="3" 
AR Path="/2F4A4BB75771" Ref="U45"  Part="3" 
AR Path="/23D9004BB75771" Ref="U45"  Part="3" 
AR Path="/23CE584BB75771" Ref="U45"  Part="3" 
AR Path="/64BB75771" Ref="U45"  Part="3" 
AR Path="/6FE934344BB75771" Ref="U45"  Part="3" 
AR Path="/6FE934E34BB75771" Ref="U45"  Part="3" 
AR Path="/69549BC04BB75771" Ref="U45"  Part="3" 
AR Path="/D1C3384BB75771" Ref="U45"  Part="3" 
F 0 "U45" H 6100 21650 60  0000 C CNN
F 1 "74LS02" H 6100 21550 60  0000 C CNN
F 2 "14dip300" H 6100 21650 60  0001 C CNN
F 3 "" H 6100 21600 60  0001 C CNN
	3    6100 21600
	1    0    0    -1  
$EndComp
$Comp
L 74LS11 U?
U 3 1 4BB7556F
P 12000 22300
AR Path="/7E428DAC4BB7556F" Ref="U?"  Part="1" 
AR Path="/2C2F984BB7556F" Ref="U?"  Part="1" 
AR Path="/45009524BB7556F" Ref="U"  Part="3" 
AR Path="/2824BB7556F" Ref="U43"  Part="3" 
AR Path="/4BB7556F" Ref="U43"  Part="3" 
AR Path="/23D83C4BB7556F" Ref="U43"  Part="3" 
AR Path="/FFFFFFFF4BB7556F" Ref="U43"  Part="3" 
AR Path="/23CE444BB7556F" Ref="U43"  Part="3" 
AR Path="/47907FA4BB7556F" Ref="U43"  Part="3" 
AR Path="/23C34C4BB7556F" Ref="U43"  Part="3" 
AR Path="/14BB7556F" Ref="U43"  Part="3" 
AR Path="/23BC884BB7556F" Ref="U43"  Part="3" 
AR Path="/773F8EB44BB7556F" Ref="U43"  Part="3" 
AR Path="/94BB7556F" Ref="U"  Part="3" 
AR Path="/23C9F04BB7556F" Ref="U43"  Part="3" 
AR Path="/FFFFFFF04BB7556F" Ref="U43"  Part="3" 
AR Path="/7E4188DA4BB7556F" Ref="U43"  Part="3" 
AR Path="/8D384BB7556F" Ref="U43"  Part="3" 
AR Path="/24BB7556F" Ref="U43"  Part="3" 
AR Path="/773F65F14BB7556F" Ref="U43"  Part="3" 
AR Path="/23C6504BB7556F" Ref="U43"  Part="3" 
AR Path="/D1C1E84BB7556F" Ref="U43"  Part="3" 
AR Path="/D058E04BB7556F" Ref="U43"  Part="3" 
AR Path="/3C64BB7556F" Ref="U43"  Part="3" 
AR Path="/23CBC44BB7556F" Ref="U43"  Part="3" 
AR Path="/2F4A4BB7556F" Ref="U43"  Part="3" 
AR Path="/23D9004BB7556F" Ref="U43"  Part="3" 
AR Path="/23CE584BB7556F" Ref="U43"  Part="3" 
AR Path="/64BB7556F" Ref="U43"  Part="3" 
AR Path="/6FE934344BB7556F" Ref="U43"  Part="3" 
AR Path="/6FE934E34BB7556F" Ref="U43"  Part="3" 
AR Path="/69549BC04BB7556F" Ref="U43"  Part="3" 
AR Path="/D1C3384BB7556F" Ref="U43"  Part="3" 
F 0 "U43" H 12000 22350 60  0000 C CNN
F 1 "74LS11" H 12000 22250 60  0000 C CNN
F 2 "14dip300" H 12000 22350 60  0001 C CNN
F 3 "" H 12000 22300 60  0001 C CNN
	3    12000 22300
	1    0    0    -1  
$EndComp
$Comp
L 74LS04 U?
U 6 1 4BB754AA
P 3850 24850
AR Path="/2300384BB754AA" Ref="U?"  Part="1" 
AR Path="/4BB754AA" Ref="U25"  Part="6" 
AR Path="/350031324BB754AA" Ref="U?"  Part="1" 
AR Path="/2B04D04BB754AA" Ref="U?"  Part="1" 
AR Path="/53C09384BB754AA" Ref="U"  Part="6" 
AR Path="/23D83C4BB754AA" Ref="U25"  Part="6" 
AR Path="/FFFFFFFF4BB754AA" Ref="U25"  Part="6" 
AR Path="/23CE444BB754AA" Ref="U25"  Part="6" 
AR Path="/47907FA4BB754AA" Ref="U25"  Part="6" 
AR Path="/23C34C4BB754AA" Ref="U25"  Part="6" 
AR Path="/14BB754AA" Ref="U25"  Part="6" 
AR Path="/23BC884BB754AA" Ref="U25"  Part="6" 
AR Path="/773F8EB44BB754AA" Ref="U25"  Part="6" 
AR Path="/94BB754AA" Ref="U"  Part="6" 
AR Path="/23C9F04BB754AA" Ref="U25"  Part="6" 
AR Path="/FFFFFFF04BB754AA" Ref="U25"  Part="6" 
AR Path="/2824BB754AA" Ref="U25"  Part="6" 
AR Path="/7E4188DA4BB754AA" Ref="U25"  Part="6" 
AR Path="/8D384BB754AA" Ref="U25"  Part="6" 
AR Path="/24BB754AA" Ref="U25"  Part="6" 
AR Path="/773F65F14BB754AA" Ref="U25"  Part="6" 
AR Path="/23C6504BB754AA" Ref="U25"  Part="6" 
AR Path="/D1C1E84BB754AA" Ref="U25"  Part="6" 
AR Path="/D058E04BB754AA" Ref="U25"  Part="6" 
AR Path="/3C64BB754AA" Ref="U25"  Part="6" 
AR Path="/23CBC44BB754AA" Ref="U25"  Part="6" 
AR Path="/2F4A4BB754AA" Ref="U25"  Part="6" 
AR Path="/23D9004BB754AA" Ref="U25"  Part="6" 
AR Path="/23CE584BB754AA" Ref="U25"  Part="6" 
AR Path="/64BB754AA" Ref="U25"  Part="6" 
AR Path="/6FE934344BB754AA" Ref="U25"  Part="6" 
AR Path="/6FE934E34BB754AA" Ref="U25"  Part="6" 
AR Path="/69549BC04BB754AA" Ref="U25"  Part="6" 
AR Path="/D1C3384BB754AA" Ref="U25"  Part="6" 
F 0 "U25" H 4045 24965 60  0000 C CNN
F 1 "74LS04" H 4040 24725 60  0000 C CNN
F 2 "14dip300" H 4040 24825 60  0001 C CNN
F 3 "" H 3850 24850 60  0001 C CNN
	6    3850 24850
	1    0    0    -1  
$EndComp
$Comp
L 74LS04 U?
U 6 1 4BB74C4D
P 22550 29950
AR Path="/9700384BB74C4D" Ref="U?"  Part="1" 
AR Path="/4BB74C4D" Ref="U24"  Part="6" 
AR Path="/344334444BB74C4D" Ref="U?"  Part="1" 
AR Path="/2606084BB74C4D" Ref="U24"  Part="1" 
AR Path="/23D8044BB74C4D" Ref="U24"  Part="1" 
AR Path="/363036324BB74C4D" Ref="U"  Part="6" 
AR Path="/FFFFFFFF4BB74C4D" Ref="U24"  Part="6" 
AR Path="/23D83C4BB74C4D" Ref="U24"  Part="6" 
AR Path="/23CE444BB74C4D" Ref="U24"  Part="6" 
AR Path="/47907FA4BB74C4D" Ref="U24"  Part="6" 
AR Path="/23C34C4BB74C4D" Ref="U24"  Part="6" 
AR Path="/14BB74C4D" Ref="U24"  Part="6" 
AR Path="/23BC884BB74C4D" Ref="U24"  Part="6" 
AR Path="/773F8EB44BB74C4D" Ref="U24"  Part="6" 
AR Path="/23C9F04BB74C4D" Ref="U24"  Part="6" 
AR Path="/94BB74C4D" Ref="U"  Part="6" 
AR Path="/FFFFFFF04BB74C4D" Ref="U24"  Part="6" 
AR Path="/2824BB74C4D" Ref="U24"  Part="6" 
AR Path="/7E4188DA4BB74C4D" Ref="U24"  Part="6" 
AR Path="/8D384BB74C4D" Ref="U24"  Part="6" 
AR Path="/24BB74C4D" Ref="U24"  Part="6" 
AR Path="/773F65F14BB74C4D" Ref="U24"  Part="6" 
AR Path="/23C6504BB74C4D" Ref="U24"  Part="6" 
AR Path="/D1C1E84BB74C4D" Ref="U24"  Part="6" 
AR Path="/D058E04BB74C4D" Ref="U24"  Part="6" 
AR Path="/3C64BB74C4D" Ref="U24"  Part="6" 
AR Path="/23CBC44BB74C4D" Ref="U24"  Part="6" 
AR Path="/2F4A4BB74C4D" Ref="U24"  Part="6" 
AR Path="/23D9004BB74C4D" Ref="U24"  Part="6" 
AR Path="/23CE584BB74C4D" Ref="U24"  Part="6" 
AR Path="/64BB74C4D" Ref="U24"  Part="6" 
AR Path="/6FE934344BB74C4D" Ref="U24"  Part="6" 
AR Path="/6FE934E34BB74C4D" Ref="U24"  Part="6" 
AR Path="/69549BC04BB74C4D" Ref="U24"  Part="6" 
AR Path="/D1C3384BB74C4D" Ref="U24"  Part="6" 
F 0 "U24" H 22745 30065 60  0000 C CNN
F 1 "74LS04" H 22740 29825 60  0000 C CNN
F 2 "14dip300" H 22740 29925 60  0001 C CNN
F 3 "" H 22550 29950 60  0001 C CNN
	6    22550 29950
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 4BB736F0
P 9800 30100
AR Path="/73254BB736F0" Ref="#PWR?"  Part="1" 
AR Path="/FFFFFFFF4BB736F0" Ref="#PWR010"  Part="1" 
AR Path="/4BB736F0" Ref="#PWR013"  Part="1" 
AR Path="/23D83C4BB736F0" Ref="#PWR29"  Part="1" 
AR Path="/23CE444BB736F0" Ref="#PWR?"  Part="1" 
AR Path="/47907FA4BB736F0" Ref="#PWR02"  Part="1" 
AR Path="/23C34C4BB736F0" Ref="#PWR09"  Part="1" 
AR Path="/14BB736F0" Ref="#PWR07"  Part="1" 
AR Path="/23BC884BB736F0" Ref="#PWR010"  Part="1" 
AR Path="/773F8EB44BB736F0" Ref="#PWR07"  Part="1" 
AR Path="/94BB736F0" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB736F0" Ref="#PWR23"  Part="1" 
AR Path="/FFFFFFF04BB736F0" Ref="#PWR30"  Part="1" 
AR Path="/2824BB736F0" Ref="#PWR07"  Part="1" 
AR Path="/7E4188DA4BB736F0" Ref="#PWR29"  Part="1" 
AR Path="/8D384BB736F0" Ref="#PWR02"  Part="1" 
AR Path="/773F65F14BB736F0" Ref="#PWR010"  Part="1" 
AR Path="/6FF0DD404BB736F0" Ref="#PWR010"  Part="1" 
AR Path="/24BB736F0" Ref="#PWR23"  Part="1" 
AR Path="/23C6504BB736F0" Ref="#PWR03"  Part="1" 
AR Path="/D1C1E84BB736F0" Ref="#PWR03"  Part="1" 
AR Path="/D058E04BB736F0" Ref="#PWR010"  Part="1" 
AR Path="/6684D64BB736F0" Ref="#PWR07"  Part="1" 
AR Path="/3C64BB736F0" Ref="#PWR03"  Part="1" 
AR Path="/23CBC44BB736F0" Ref="#PWR07"  Part="1" 
AR Path="/2F4A4BB736F0" Ref="#PWR03"  Part="1" 
AR Path="/23D9004BB736F0" Ref="#PWR07"  Part="1" 
AR Path="/23CE584BB736F0" Ref="#PWR05"  Part="1" 
AR Path="/64BB736F0" Ref="#PWR27"  Part="1" 
AR Path="/6FE934344BB736F0" Ref="#PWR27"  Part="1" 
AR Path="/6FE934E34BB736F0" Ref="#PWR07"  Part="1" 
AR Path="/69549BC04BB736F0" Ref="#PWR07"  Part="1" 
AR Path="/D1C3384BB736F0" Ref="#PWR010"  Part="1" 
F 0 "#PWR013" H 9800 30200 30  0001 C CNN
F 1 "VCC" H 9800 30200 30  0000 C CNN
F 2 "" H 9800 30100 60  0001 C CNN
F 3 "" H 9800 30100 60  0001 C CNN
	1    9800 30100
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 4BB736EE
P 10950 28950
AR Path="/73254BB736EE" Ref="#PWR?"  Part="1" 
AR Path="/FFFFFFFF4BB736EE" Ref="#PWR011"  Part="1" 
AR Path="/4BB736EE" Ref="#PWR014"  Part="1" 
AR Path="/23D83C4BB736EE" Ref="#PWR33"  Part="1" 
AR Path="/47907FA4BB736EE" Ref="#PWR03"  Part="1" 
AR Path="/23C34C4BB736EE" Ref="#PWR010"  Part="1" 
AR Path="/14BB736EE" Ref="#PWR08"  Part="1" 
AR Path="/23BC884BB736EE" Ref="#PWR011"  Part="1" 
AR Path="/773F8EB44BB736EE" Ref="#PWR08"  Part="1" 
AR Path="/94BB736EE" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB736EE" Ref="#PWR27"  Part="1" 
AR Path="/FFFFFFF04BB736EE" Ref="#PWR34"  Part="1" 
AR Path="/2824BB736EE" Ref="#PWR08"  Part="1" 
AR Path="/7E4188DA4BB736EE" Ref="#PWR33"  Part="1" 
AR Path="/8D384BB736EE" Ref="#PWR03"  Part="1" 
AR Path="/773F65F14BB736EE" Ref="#PWR011"  Part="1" 
AR Path="/6FF0DD404BB736EE" Ref="#PWR011"  Part="1" 
AR Path="/24BB736EE" Ref="#PWR27"  Part="1" 
AR Path="/23C6504BB736EE" Ref="#PWR04"  Part="1" 
AR Path="/D1C1E84BB736EE" Ref="#PWR04"  Part="1" 
AR Path="/D058E04BB736EE" Ref="#PWR011"  Part="1" 
AR Path="/6684D64BB736EE" Ref="#PWR08"  Part="1" 
AR Path="/3C64BB736EE" Ref="#PWR04"  Part="1" 
AR Path="/23CBC44BB736EE" Ref="#PWR08"  Part="1" 
AR Path="/2F4A4BB736EE" Ref="#PWR04"  Part="1" 
AR Path="/23D9004BB736EE" Ref="#PWR08"  Part="1" 
AR Path="/23CE584BB736EE" Ref="#PWR06"  Part="1" 
AR Path="/64BB736EE" Ref="#PWR31"  Part="1" 
AR Path="/6FE934344BB736EE" Ref="#PWR31"  Part="1" 
AR Path="/6FE934E34BB736EE" Ref="#PWR08"  Part="1" 
AR Path="/69549BC04BB736EE" Ref="#PWR08"  Part="1" 
AR Path="/D1C3384BB736EE" Ref="#PWR011"  Part="1" 
F 0 "#PWR014" H 10950 29050 30  0001 C CNN
F 1 "VCC" H 10950 29050 30  0000 C CNN
F 2 "" H 10950 28950 60  0001 C CNN
F 3 "" H 10950 28950 60  0001 C CNN
	1    10950 28950
	1    0    0    -1  
$EndComp
Text Label 11000 30400 0    60   ~ 0
GND
$Comp
L 74LS165 U?
U 1 1 4BB736EC
P 11650 29550
AR Path="/73254BB736EC" Ref="U?"  Part="1" 
AR Path="/FFFFFFFF4BB736EC" Ref="U10"  Part="1" 
AR Path="/4BB736EC" Ref="U10"  Part="1" 
AR Path="/23D8044BB736EC" Ref="U?"  Part="1" 
AR Path="/2606084BB736EC" Ref="U10"  Part="1" 
AR Path="/23D83C4BB736EC" Ref="U10"  Part="1" 
AR Path="/47907FA4BB736EC" Ref="U10"  Part="1" 
AR Path="/23C34C4BB736EC" Ref="U10"  Part="1" 
AR Path="/14BB736EC" Ref="U10"  Part="1" 
AR Path="/23BC884BB736EC" Ref="U10"  Part="1" 
AR Path="/773F8EB44BB736EC" Ref="U10"  Part="1" 
AR Path="/94BB736EC" Ref="U"  Part="1" 
AR Path="/23C9F04BB736EC" Ref="U10"  Part="1" 
AR Path="/FFFFFFF04BB736EC" Ref="U10"  Part="1" 
AR Path="/2824BB736EC" Ref="U10"  Part="1" 
AR Path="/7E4188DA4BB736EC" Ref="U10"  Part="1" 
AR Path="/8D384BB736EC" Ref="U10"  Part="1" 
AR Path="/24BB736EC" Ref="U10"  Part="1" 
AR Path="/773F65F14BB736EC" Ref="U10"  Part="1" 
AR Path="/23C6504BB736EC" Ref="U10"  Part="1" 
AR Path="/D1C1E84BB736EC" Ref="U10"  Part="1" 
AR Path="/D058E04BB736EC" Ref="U10"  Part="1" 
AR Path="/3C64BB736EC" Ref="U10"  Part="1" 
AR Path="/23CBC44BB736EC" Ref="U10"  Part="1" 
AR Path="/2F4A4BB736EC" Ref="U10"  Part="1" 
AR Path="/23D9004BB736EC" Ref="U10"  Part="1" 
AR Path="/23CE584BB736EC" Ref="U10"  Part="1" 
AR Path="/64BB736EC" Ref="U10"  Part="1" 
AR Path="/6FE934344BB736EC" Ref="U10"  Part="1" 
AR Path="/6FE934E34BB736EC" Ref="U10"  Part="1" 
AR Path="/69549BC04BB736EC" Ref="U10"  Part="1" 
AR Path="/D1C3384BB736EC" Ref="U10"  Part="1" 
F 0 "U10" H 11800 29500 60  0000 C CNN
F 1 "74LS165" H 11800 29300 60  0000 C CNN
F 2 "16dip300" H 11650 29550 60  0001 C CNN
F 3 "" H 11650 29550 60  0001 C CNN
	1    11650 29550
	1    0    0    -1  
$EndComp
$Comp
L RR9 RR?
U 1 1 4BB736EB
P 10300 30450
AR Path="/73254BB736EB" Ref="RR?"  Part="1" 
AR Path="/FFFFFFFF4BB736EB" Ref="RR3"  Part="1" 
AR Path="/4BB736EB" Ref="RR3"  Part="1" 
AR Path="/23D83C4BB736EB" Ref="RR3"  Part="1" 
AR Path="/47907FA4BB736EB" Ref="RR?"  Part="1" 
AR Path="/23C34C4BB736EB" Ref="RR3"  Part="1" 
AR Path="/14BB736EB" Ref="RR3"  Part="1" 
AR Path="/23BC884BB736EB" Ref="RR3"  Part="1" 
AR Path="/773F8EB44BB736EB" Ref="RR3"  Part="1" 
AR Path="/94BB736EB" Ref="RR"  Part="1" 
AR Path="/23C9F04BB736EB" Ref="RR3"  Part="1" 
AR Path="/FFFFFFF04BB736EB" Ref="RR3"  Part="1" 
AR Path="/2824BB736EB" Ref="RR3"  Part="1" 
AR Path="/7E4188DA4BB736EB" Ref="RR3"  Part="1" 
AR Path="/8D384BB736EB" Ref="RR3"  Part="1" 
AR Path="/24BB736EB" Ref="RR3"  Part="1" 
AR Path="/773F65F14BB736EB" Ref="RR3"  Part="1" 
AR Path="/23C6504BB736EB" Ref="RR3"  Part="1" 
AR Path="/D1C1E84BB736EB" Ref="RR3"  Part="1" 
AR Path="/D058E04BB736EB" Ref="RR3"  Part="1" 
AR Path="/3C64BB736EB" Ref="RR3"  Part="1" 
AR Path="/23CBC44BB736EB" Ref="RR3"  Part="1" 
AR Path="/2F4A4BB736EB" Ref="RR3"  Part="1" 
AR Path="/23D9004BB736EB" Ref="RR3"  Part="1" 
AR Path="/23CE584BB736EB" Ref="RR3"  Part="1" 
AR Path="/64BB736EB" Ref="RR3"  Part="1" 
AR Path="/6FE934344BB736EB" Ref="RR3"  Part="1" 
AR Path="/6FE934E34BB736EB" Ref="RR3"  Part="1" 
AR Path="/69549BC04BB736EB" Ref="RR3"  Part="1" 
AR Path="/D1C3384BB736EB" Ref="RR3"  Part="1" 
F 0 "RR3" H 10350 31050 70  0000 C CNN
F 1 "1000" V 10330 30450 70  0000 C CNN
F 2 "r_pack9" H 10300 30450 60  0001 C CNN
F 3 "" H 10300 30450 60  0001 C CNN
	1    10300 30450
	0    -1   1    0   
$EndComp
$Comp
L RR9 RR?
U 1 1 4BB736EA
P 13900 30450
AR Path="/73254BB736EA" Ref="RR?"  Part="1" 
AR Path="/FFFFFFFF4BB736EA" Ref="RR4"  Part="1" 
AR Path="/4BB736EA" Ref="RR4"  Part="1" 
AR Path="/23D83C4BB736EA" Ref="RR4"  Part="1" 
AR Path="/47907FA4BB736EA" Ref="RR?"  Part="1" 
AR Path="/23C34C4BB736EA" Ref="RR4"  Part="1" 
AR Path="/14BB736EA" Ref="RR4"  Part="1" 
AR Path="/23BC884BB736EA" Ref="RR4"  Part="1" 
AR Path="/773F8EB44BB736EA" Ref="RR4"  Part="1" 
AR Path="/94BB736EA" Ref="RR"  Part="1" 
AR Path="/23C9F04BB736EA" Ref="RR4"  Part="1" 
AR Path="/FFFFFFF04BB736EA" Ref="RR4"  Part="1" 
AR Path="/2824BB736EA" Ref="RR4"  Part="1" 
AR Path="/7E4188DA4BB736EA" Ref="RR4"  Part="1" 
AR Path="/8D384BB736EA" Ref="RR4"  Part="1" 
AR Path="/24BB736EA" Ref="RR4"  Part="1" 
AR Path="/773F65F14BB736EA" Ref="RR4"  Part="1" 
AR Path="/23C6504BB736EA" Ref="RR4"  Part="1" 
AR Path="/D1C1E84BB736EA" Ref="RR4"  Part="1" 
AR Path="/D058E04BB736EA" Ref="RR4"  Part="1" 
AR Path="/3C64BB736EA" Ref="RR4"  Part="1" 
AR Path="/23CBC44BB736EA" Ref="RR4"  Part="1" 
AR Path="/2F4A4BB736EA" Ref="RR4"  Part="1" 
AR Path="/23D9004BB736EA" Ref="RR4"  Part="1" 
AR Path="/23CE584BB736EA" Ref="RR4"  Part="1" 
AR Path="/64BB736EA" Ref="RR4"  Part="1" 
AR Path="/6FE934344BB736EA" Ref="RR4"  Part="1" 
AR Path="/6FE934E34BB736EA" Ref="RR4"  Part="1" 
AR Path="/69549BC04BB736EA" Ref="RR4"  Part="1" 
AR Path="/D1C3384BB736EA" Ref="RR4"  Part="1" 
F 0 "RR4" H 13950 31050 70  0000 C CNN
F 1 "1000" V 13930 30450 70  0000 C CNN
F 2 "r_pack9" H 13900 30450 60  0001 C CNN
F 3 "" H 13900 30450 60  0001 C CNN
	1    13900 30450
	0    -1   1    0   
$EndComp
$Comp
L 74LS165 U?
U 1 1 4BB736E9
P 15250 29550
AR Path="/73254BB736E9" Ref="U?"  Part="1" 
AR Path="/FFFFFFFF4BB736E9" Ref="U11"  Part="1" 
AR Path="/4BB736E9" Ref="U11"  Part="1" 
AR Path="/7E428DAC4BB736E9" Ref="U?"  Part="1" 
AR Path="/2606084BB736E9" Ref="U11"  Part="1" 
AR Path="/23D83C4BB736E9" Ref="U11"  Part="1" 
AR Path="/47907FA4BB736E9" Ref="U11"  Part="1" 
AR Path="/23C34C4BB736E9" Ref="U11"  Part="1" 
AR Path="/14BB736E9" Ref="U11"  Part="1" 
AR Path="/23BC884BB736E9" Ref="U11"  Part="1" 
AR Path="/773F8EB44BB736E9" Ref="U11"  Part="1" 
AR Path="/94BB736E9" Ref="U"  Part="1" 
AR Path="/23C9F04BB736E9" Ref="U11"  Part="1" 
AR Path="/FFFFFFF04BB736E9" Ref="U11"  Part="1" 
AR Path="/2824BB736E9" Ref="U11"  Part="1" 
AR Path="/7E4188DA4BB736E9" Ref="U11"  Part="1" 
AR Path="/8D384BB736E9" Ref="U11"  Part="1" 
AR Path="/24BB736E9" Ref="U11"  Part="1" 
AR Path="/773F65F14BB736E9" Ref="U11"  Part="1" 
AR Path="/23C6504BB736E9" Ref="U11"  Part="1" 
AR Path="/D1C1E84BB736E9" Ref="U11"  Part="1" 
AR Path="/D058E04BB736E9" Ref="U11"  Part="1" 
AR Path="/3C64BB736E9" Ref="U11"  Part="1" 
AR Path="/23CBC44BB736E9" Ref="U11"  Part="1" 
AR Path="/2F4A4BB736E9" Ref="U11"  Part="1" 
AR Path="/23D9004BB736E9" Ref="U11"  Part="1" 
AR Path="/23CE584BB736E9" Ref="U11"  Part="1" 
AR Path="/64BB736E9" Ref="U11"  Part="1" 
AR Path="/6FE934344BB736E9" Ref="U11"  Part="1" 
AR Path="/6FE934E34BB736E9" Ref="U11"  Part="1" 
AR Path="/69549BC04BB736E9" Ref="U11"  Part="1" 
AR Path="/D1C3384BB736E9" Ref="U11"  Part="1" 
F 0 "U11" H 15400 29500 60  0000 C CNN
F 1 "74LS165" H 15400 29300 60  0000 C CNN
F 2 "16dip300" H 15250 29550 60  0001 C CNN
F 3 "" H 15250 29550 60  0001 C CNN
	1    15250 29550
	1    0    0    -1  
$EndComp
Text Label 14600 30400 0    60   ~ 0
GND
$Comp
L VCC #PWR?
U 1 1 4BB736E7
P 14550 28950
AR Path="/73254BB736E7" Ref="#PWR?"  Part="1" 
AR Path="/FFFFFFFF4BB736E7" Ref="#PWR014"  Part="1" 
AR Path="/4BB736E7" Ref="#PWR015"  Part="1" 
AR Path="/23D83C4BB736E7" Ref="#PWR39"  Part="1" 
AR Path="/47907FA4BB736E7" Ref="#PWR06"  Part="1" 
AR Path="/23C34C4BB736E7" Ref="#PWR013"  Part="1" 
AR Path="/14BB736E7" Ref="#PWR011"  Part="1" 
AR Path="/23BC884BB736E7" Ref="#PWR014"  Part="1" 
AR Path="/773F8EB44BB736E7" Ref="#PWR011"  Part="1" 
AR Path="/94BB736E7" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB736E7" Ref="#PWR32"  Part="1" 
AR Path="/FFFFFFF04BB736E7" Ref="#PWR40"  Part="1" 
AR Path="/2824BB736E7" Ref="#PWR011"  Part="1" 
AR Path="/7E4188DA4BB736E7" Ref="#PWR39"  Part="1" 
AR Path="/8D384BB736E7" Ref="#PWR06"  Part="1" 
AR Path="/773F65F14BB736E7" Ref="#PWR014"  Part="1" 
AR Path="/6FF0DD404BB736E7" Ref="#PWR014"  Part="1" 
AR Path="/24BB736E7" Ref="#PWR32"  Part="1" 
AR Path="/23C6504BB736E7" Ref="#PWR07"  Part="1" 
AR Path="/D1C1E84BB736E7" Ref="#PWR07"  Part="1" 
AR Path="/D058E04BB736E7" Ref="#PWR014"  Part="1" 
AR Path="/6684D64BB736E7" Ref="#PWR011"  Part="1" 
AR Path="/3C64BB736E7" Ref="#PWR07"  Part="1" 
AR Path="/23CBC44BB736E7" Ref="#PWR011"  Part="1" 
AR Path="/2F4A4BB736E7" Ref="#PWR07"  Part="1" 
AR Path="/23D9004BB736E7" Ref="#PWR011"  Part="1" 
AR Path="/23CE584BB736E7" Ref="#PWR09"  Part="1" 
AR Path="/64BB736E7" Ref="#PWR37"  Part="1" 
AR Path="/6FE934344BB736E7" Ref="#PWR37"  Part="1" 
AR Path="/6FE934E34BB736E7" Ref="#PWR011"  Part="1" 
AR Path="/69549BC04BB736E7" Ref="#PWR011"  Part="1" 
AR Path="/D1C3384BB736E7" Ref="#PWR014"  Part="1" 
F 0 "#PWR015" H 14550 29050 30  0001 C CNN
F 1 "VCC" H 14550 29050 30  0000 C CNN
F 2 "" H 14550 28950 60  0001 C CNN
F 3 "" H 14550 28950 60  0001 C CNN
	1    14550 28950
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 4BB736E5
P 13400 30100
AR Path="/73254BB736E5" Ref="#PWR?"  Part="1" 
AR Path="/FFFFFFFF4BB736E5" Ref="#PWR015"  Part="1" 
AR Path="/4BB736E5" Ref="#PWR016"  Part="1" 
AR Path="/23D83C4BB736E5" Ref="#PWR37"  Part="1" 
AR Path="/47907FA4BB736E5" Ref="#PWR07"  Part="1" 
AR Path="/23C34C4BB736E5" Ref="#PWR014"  Part="1" 
AR Path="/14BB736E5" Ref="#PWR012"  Part="1" 
AR Path="/23BC884BB736E5" Ref="#PWR015"  Part="1" 
AR Path="/773F8EB44BB736E5" Ref="#PWR012"  Part="1" 
AR Path="/94BB736E5" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB736E5" Ref="#PWR30"  Part="1" 
AR Path="/FFFFFFF04BB736E5" Ref="#PWR38"  Part="1" 
AR Path="/2824BB736E5" Ref="#PWR012"  Part="1" 
AR Path="/7E4188DA4BB736E5" Ref="#PWR37"  Part="1" 
AR Path="/8D384BB736E5" Ref="#PWR07"  Part="1" 
AR Path="/773F65F14BB736E5" Ref="#PWR015"  Part="1" 
AR Path="/6FF0DD404BB736E5" Ref="#PWR015"  Part="1" 
AR Path="/24BB736E5" Ref="#PWR30"  Part="1" 
AR Path="/23C6504BB736E5" Ref="#PWR08"  Part="1" 
AR Path="/D1C1E84BB736E5" Ref="#PWR08"  Part="1" 
AR Path="/D058E04BB736E5" Ref="#PWR015"  Part="1" 
AR Path="/6684D64BB736E5" Ref="#PWR012"  Part="1" 
AR Path="/3C64BB736E5" Ref="#PWR08"  Part="1" 
AR Path="/23CBC44BB736E5" Ref="#PWR012"  Part="1" 
AR Path="/2F4A4BB736E5" Ref="#PWR08"  Part="1" 
AR Path="/23D9004BB736E5" Ref="#PWR012"  Part="1" 
AR Path="/23CE584BB736E5" Ref="#PWR010"  Part="1" 
AR Path="/64BB736E5" Ref="#PWR35"  Part="1" 
AR Path="/6FE934344BB736E5" Ref="#PWR35"  Part="1" 
AR Path="/6FE934E34BB736E5" Ref="#PWR012"  Part="1" 
AR Path="/69549BC04BB736E5" Ref="#PWR012"  Part="1" 
AR Path="/D1C3384BB736E5" Ref="#PWR015"  Part="1" 
F 0 "#PWR016" H 13400 30200 30  0001 C CNN
F 1 "VCC" H 13400 30200 30  0000 C CNN
F 2 "" H 13400 30100 60  0001 C CNN
F 3 "" H 13400 30100 60  0001 C CNN
	1    13400 30100
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 4BB736C0
P 6200 30100
AR Path="/73254BB736C0" Ref="#PWR?"  Part="1" 
AR Path="/FFFFFFFF4BB736C0" Ref="#PWR016"  Part="1" 
AR Path="/4BB736C0" Ref="#PWR017"  Part="1" 
AR Path="/23D83C4BB736C0" Ref="#PWR20"  Part="1" 
AR Path="/47907FA4BB736C0" Ref="#PWR08"  Part="1" 
AR Path="/23C34C4BB736C0" Ref="#PWR015"  Part="1" 
AR Path="/14BB736C0" Ref="#PWR013"  Part="1" 
AR Path="/23BC884BB736C0" Ref="#PWR016"  Part="1" 
AR Path="/773F8EB44BB736C0" Ref="#PWR013"  Part="1" 
AR Path="/94BB736C0" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB736C0" Ref="#PWR15"  Part="1" 
AR Path="/FFFFFFF04BB736C0" Ref="#PWR20"  Part="1" 
AR Path="/2824BB736C0" Ref="#PWR013"  Part="1" 
AR Path="/7E4188DA4BB736C0" Ref="#PWR20"  Part="1" 
AR Path="/8D384BB736C0" Ref="#PWR08"  Part="1" 
AR Path="/773F65F14BB736C0" Ref="#PWR016"  Part="1" 
AR Path="/6FF0DD404BB736C0" Ref="#PWR016"  Part="1" 
AR Path="/24BB736C0" Ref="#PWR15"  Part="1" 
AR Path="/23C6504BB736C0" Ref="#PWR09"  Part="1" 
AR Path="/D1C1E84BB736C0" Ref="#PWR09"  Part="1" 
AR Path="/D058E04BB736C0" Ref="#PWR016"  Part="1" 
AR Path="/6684D64BB736C0" Ref="#PWR013"  Part="1" 
AR Path="/3C64BB736C0" Ref="#PWR09"  Part="1" 
AR Path="/23CBC44BB736C0" Ref="#PWR013"  Part="1" 
AR Path="/2F4A4BB736C0" Ref="#PWR09"  Part="1" 
AR Path="/23D9004BB736C0" Ref="#PWR013"  Part="1" 
AR Path="/23CE584BB736C0" Ref="#PWR011"  Part="1" 
AR Path="/64BB736C0" Ref="#PWR18"  Part="1" 
AR Path="/6FE934344BB736C0" Ref="#PWR18"  Part="1" 
AR Path="/6FE934E34BB736C0" Ref="#PWR013"  Part="1" 
AR Path="/69549BC04BB736C0" Ref="#PWR013"  Part="1" 
AR Path="/D1C3384BB736C0" Ref="#PWR016"  Part="1" 
F 0 "#PWR017" H 6200 30200 30  0001 C CNN
F 1 "VCC" H 6200 30200 30  0000 C CNN
F 2 "" H 6200 30100 60  0001 C CNN
F 3 "" H 6200 30100 60  0001 C CNN
	1    6200 30100
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 4BB736BE
P 7350 28950
AR Path="/73254BB736BE" Ref="#PWR?"  Part="1" 
AR Path="/FFFFFFFF4BB736BE" Ref="#PWR017"  Part="1" 
AR Path="/4BB736BE" Ref="#PWR018"  Part="1" 
AR Path="/23D83C4BB736BE" Ref="#PWR23"  Part="1" 
AR Path="/47907FA4BB736BE" Ref="#PWR09"  Part="1" 
AR Path="/23C34C4BB736BE" Ref="#PWR016"  Part="1" 
AR Path="/14BB736BE" Ref="#PWR014"  Part="1" 
AR Path="/23BC884BB736BE" Ref="#PWR017"  Part="1" 
AR Path="/773F8EB44BB736BE" Ref="#PWR014"  Part="1" 
AR Path="/94BB736BE" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB736BE" Ref="#PWR18"  Part="1" 
AR Path="/FFFFFFF04BB736BE" Ref="#PWR23"  Part="1" 
AR Path="/2824BB736BE" Ref="#PWR014"  Part="1" 
AR Path="/7E4188DA4BB736BE" Ref="#PWR23"  Part="1" 
AR Path="/8D384BB736BE" Ref="#PWR09"  Part="1" 
AR Path="/773F65F14BB736BE" Ref="#PWR017"  Part="1" 
AR Path="/6FF0DD404BB736BE" Ref="#PWR017"  Part="1" 
AR Path="/24BB736BE" Ref="#PWR18"  Part="1" 
AR Path="/23C6504BB736BE" Ref="#PWR010"  Part="1" 
AR Path="/D1C1E84BB736BE" Ref="#PWR010"  Part="1" 
AR Path="/D058E04BB736BE" Ref="#PWR017"  Part="1" 
AR Path="/6684D64BB736BE" Ref="#PWR014"  Part="1" 
AR Path="/3C64BB736BE" Ref="#PWR010"  Part="1" 
AR Path="/23CBC44BB736BE" Ref="#PWR014"  Part="1" 
AR Path="/2F4A4BB736BE" Ref="#PWR010"  Part="1" 
AR Path="/23D9004BB736BE" Ref="#PWR014"  Part="1" 
AR Path="/23CE584BB736BE" Ref="#PWR012"  Part="1" 
AR Path="/64BB736BE" Ref="#PWR21"  Part="1" 
AR Path="/6FE934344BB736BE" Ref="#PWR21"  Part="1" 
AR Path="/6FE934E34BB736BE" Ref="#PWR014"  Part="1" 
AR Path="/69549BC04BB736BE" Ref="#PWR014"  Part="1" 
AR Path="/D1C3384BB736BE" Ref="#PWR017"  Part="1" 
F 0 "#PWR018" H 7350 29050 30  0001 C CNN
F 1 "VCC" H 7350 29050 30  0000 C CNN
F 2 "" H 7350 28950 60  0001 C CNN
F 3 "" H 7350 28950 60  0001 C CNN
	1    7350 28950
	1    0    0    -1  
$EndComp
Text Label 7400 30400 0    60   ~ 0
GND
$Comp
L 74LS165 U?
U 1 1 4BB736BC
P 8050 29550
AR Path="/73254BB736BC" Ref="U?"  Part="1" 
AR Path="/FFFFFFFF4BB736BC" Ref="U9"  Part="1" 
AR Path="/4BB736BC" Ref="U9"  Part="1" 
AR Path="/23D8044BB736BC" Ref="U?"  Part="1" 
AR Path="/2606084BB736BC" Ref="U9"  Part="1" 
AR Path="/23D83C4BB736BC" Ref="U9"  Part="1" 
AR Path="/47907FA4BB736BC" Ref="U9"  Part="1" 
AR Path="/23C34C4BB736BC" Ref="U9"  Part="1" 
AR Path="/14BB736BC" Ref="U9"  Part="1" 
AR Path="/23BC884BB736BC" Ref="U9"  Part="1" 
AR Path="/773F8EB44BB736BC" Ref="U9"  Part="1" 
AR Path="/94BB736BC" Ref="U"  Part="1" 
AR Path="/23C9F04BB736BC" Ref="U9"  Part="1" 
AR Path="/FFFFFFF04BB736BC" Ref="U9"  Part="1" 
AR Path="/2824BB736BC" Ref="U9"  Part="1" 
AR Path="/7E4188DA4BB736BC" Ref="U9"  Part="1" 
AR Path="/8D384BB736BC" Ref="U9"  Part="1" 
AR Path="/24BB736BC" Ref="U9"  Part="1" 
AR Path="/773F65F14BB736BC" Ref="U9"  Part="1" 
AR Path="/23C6504BB736BC" Ref="U9"  Part="1" 
AR Path="/D1C1E84BB736BC" Ref="U9"  Part="1" 
AR Path="/D058E04BB736BC" Ref="U9"  Part="1" 
AR Path="/3C64BB736BC" Ref="U9"  Part="1" 
AR Path="/23CBC44BB736BC" Ref="U9"  Part="1" 
AR Path="/2F4A4BB736BC" Ref="U9"  Part="1" 
AR Path="/23D9004BB736BC" Ref="U9"  Part="1" 
AR Path="/23CE584BB736BC" Ref="U9"  Part="1" 
AR Path="/64BB736BC" Ref="U9"  Part="1" 
AR Path="/6FE934344BB736BC" Ref="U9"  Part="1" 
AR Path="/6FE934E34BB736BC" Ref="U9"  Part="1" 
AR Path="/69549BC04BB736BC" Ref="U9"  Part="1" 
AR Path="/D1C3384BB736BC" Ref="U9"  Part="1" 
F 0 "U9" H 8200 29500 60  0000 C CNN
F 1 "74LS165" H 8200 29300 60  0000 C CNN
F 2 "16dip300" H 8050 29550 60  0001 C CNN
F 3 "" H 8050 29550 60  0001 C CNN
	1    8050 29550
	1    0    0    -1  
$EndComp
$Comp
L RR9 RR?
U 1 1 4BB736BB
P 6700 30450
AR Path="/73254BB736BB" Ref="RR?"  Part="1" 
AR Path="/FFFFFFFF4BB736BB" Ref="RR2"  Part="1" 
AR Path="/4BB736BB" Ref="RR2"  Part="1" 
AR Path="/23D83C4BB736BB" Ref="RR2"  Part="1" 
AR Path="/47907FA4BB736BB" Ref="RR?"  Part="1" 
AR Path="/23C34C4BB736BB" Ref="RR2"  Part="1" 
AR Path="/14BB736BB" Ref="RR2"  Part="1" 
AR Path="/23BC884BB736BB" Ref="RR2"  Part="1" 
AR Path="/773F8EB44BB736BB" Ref="RR2"  Part="1" 
AR Path="/94BB736BB" Ref="RR"  Part="1" 
AR Path="/23C9F04BB736BB" Ref="RR2"  Part="1" 
AR Path="/FFFFFFF04BB736BB" Ref="RR2"  Part="1" 
AR Path="/2824BB736BB" Ref="RR2"  Part="1" 
AR Path="/7E4188DA4BB736BB" Ref="RR2"  Part="1" 
AR Path="/8D384BB736BB" Ref="RR2"  Part="1" 
AR Path="/24BB736BB" Ref="RR2"  Part="1" 
AR Path="/773F65F14BB736BB" Ref="RR2"  Part="1" 
AR Path="/23C6504BB736BB" Ref="RR2"  Part="1" 
AR Path="/D1C1E84BB736BB" Ref="RR2"  Part="1" 
AR Path="/D058E04BB736BB" Ref="RR2"  Part="1" 
AR Path="/3C64BB736BB" Ref="RR2"  Part="1" 
AR Path="/23CBC44BB736BB" Ref="RR2"  Part="1" 
AR Path="/2F4A4BB736BB" Ref="RR2"  Part="1" 
AR Path="/23D9004BB736BB" Ref="RR2"  Part="1" 
AR Path="/23CE584BB736BB" Ref="RR2"  Part="1" 
AR Path="/64BB736BB" Ref="RR2"  Part="1" 
AR Path="/6FE934344BB736BB" Ref="RR2"  Part="1" 
AR Path="/6FE934E34BB736BB" Ref="RR2"  Part="1" 
AR Path="/69549BC04BB736BB" Ref="RR2"  Part="1" 
AR Path="/D1C3384BB736BB" Ref="RR2"  Part="1" 
F 0 "RR2" H 6750 31050 70  0000 C CNN
F 1 "1000" V 6730 30450 70  0000 C CNN
F 2 "r_pack9" H 6700 30450 60  0001 C CNN
F 3 "" H 6700 30450 60  0001 C CNN
	1    6700 30450
	0    -1   1    0   
$EndComp
Text Label 6100 16750 0    60   ~ 0
SDSB*
Text Label 5450 7200 0    60   ~ 0
ADSB
Text Label 9400 18600 0    60   ~ 0
PARTIAL_LATCH
Text Label 13350 4200 0    60   ~ 0
A9
Text Label 13350 4100 0    60   ~ 0
A8
Text Label 13350 3700 0    60   ~ 0
A4
Text Label 13350 3800 0    60   ~ 0
A5
Text Label 13350 4000 0    60   ~ 0
A7
Text Label 13350 3900 0    60   ~ 0
A6
Text Label 13350 3500 0    60   ~ 0
A2
Text Label 13350 3600 0    60   ~ 0
A3
Text Label 13350 3400 0    60   ~ 0
A1
$Comp
L C C48
U 1 1 4BB6912C
P 37400 22850
AR Path="/4BB6912C" Ref="C48"  Part="1" 
AR Path="/FFFFFFFF4BB6912C" Ref="C48"  Part="1" 
AR Path="/755D912A4BB6912C" Ref="C?"  Part="1" 
AR Path="/73254BB6912C" Ref="C?"  Part="1" 
AR Path="/23D83C4BB6912C" Ref="C48"  Part="1" 
AR Path="/47907FA4BB6912C" Ref="C?"  Part="1" 
AR Path="/23C34C4BB6912C" Ref="C48"  Part="1" 
AR Path="/14BB6912C" Ref="C48"  Part="1" 
AR Path="/23BC884BB6912C" Ref="C48"  Part="1" 
AR Path="/773F8EB44BB6912C" Ref="C48"  Part="1" 
AR Path="/23C9F04BB6912C" Ref="C48"  Part="1" 
AR Path="/94BB6912C" Ref="C"  Part="1" 
AR Path="/FFFFFFF04BB6912C" Ref="C48"  Part="1" 
AR Path="/7E4188DA4BB6912C" Ref="C48"  Part="1" 
AR Path="/8D384BB6912C" Ref="C48"  Part="1" 
AR Path="/24BB6912C" Ref="C48"  Part="1" 
AR Path="/773F65F14BB6912C" Ref="C48"  Part="1" 
AR Path="/23C6504BB6912C" Ref="C48"  Part="1" 
AR Path="/D1C1E84BB6912C" Ref="C48"  Part="1" 
AR Path="/D058E04BB6912C" Ref="C48"  Part="1" 
AR Path="/3C64BB6912C" Ref="C48"  Part="1" 
AR Path="/23CBC44BB6912C" Ref="C48"  Part="1" 
AR Path="/2F4A4BB6912C" Ref="C48"  Part="1" 
AR Path="/23D9004BB6912C" Ref="C48"  Part="1" 
AR Path="/23CE584BB6912C" Ref="C48"  Part="1" 
AR Path="/64BB6912C" Ref="C48"  Part="1" 
AR Path="/6FE934344BB6912C" Ref="C48"  Part="1" 
AR Path="/6FE934E34BB6912C" Ref="C48"  Part="1" 
AR Path="/2824BB6912C" Ref="C48"  Part="1" 
AR Path="/69549BC04BB6912C" Ref="C48"  Part="1" 
AR Path="/D1C3384BB6912C" Ref="C48"  Part="1" 
F 0 "C48" H 37450 22950 50  0000 L CNN
F 1 "0.1 uF" H 37450 22750 50  0000 L CNN
F 2 "C2" H 37400 22850 60  0001 C CNN
F 3 "" H 37400 22850 60  0001 C CNN
	1    37400 22850
	1    0    0    -1  
$EndComp
$Comp
L C C49
U 1 1 4BB6912B
P 37850 22850
AR Path="/4BB6912B" Ref="C49"  Part="1" 
AR Path="/FFFFFFFF4BB6912B" Ref="C49"  Part="1" 
AR Path="/755D912A4BB6912B" Ref="C?"  Part="1" 
AR Path="/73254BB6912B" Ref="C?"  Part="1" 
AR Path="/23D83C4BB6912B" Ref="C49"  Part="1" 
AR Path="/47907FA4BB6912B" Ref="C?"  Part="1" 
AR Path="/23C34C4BB6912B" Ref="C49"  Part="1" 
AR Path="/14BB6912B" Ref="C49"  Part="1" 
AR Path="/23BC884BB6912B" Ref="C49"  Part="1" 
AR Path="/773F8EB44BB6912B" Ref="C49"  Part="1" 
AR Path="/23C9F04BB6912B" Ref="C49"  Part="1" 
AR Path="/94BB6912B" Ref="C"  Part="1" 
AR Path="/FFFFFFF04BB6912B" Ref="C49"  Part="1" 
AR Path="/7E4188DA4BB6912B" Ref="C49"  Part="1" 
AR Path="/8D384BB6912B" Ref="C49"  Part="1" 
AR Path="/24BB6912B" Ref="C49"  Part="1" 
AR Path="/773F65F14BB6912B" Ref="C49"  Part="1" 
AR Path="/23C6504BB6912B" Ref="C49"  Part="1" 
AR Path="/D1C1E84BB6912B" Ref="C49"  Part="1" 
AR Path="/D058E04BB6912B" Ref="C49"  Part="1" 
AR Path="/3C64BB6912B" Ref="C49"  Part="1" 
AR Path="/23CBC44BB6912B" Ref="C49"  Part="1" 
AR Path="/2F4A4BB6912B" Ref="C49"  Part="1" 
AR Path="/23D9004BB6912B" Ref="C49"  Part="1" 
AR Path="/23CE584BB6912B" Ref="C49"  Part="1" 
AR Path="/64BB6912B" Ref="C49"  Part="1" 
AR Path="/6FE934344BB6912B" Ref="C49"  Part="1" 
AR Path="/6FE934E34BB6912B" Ref="C49"  Part="1" 
AR Path="/2824BB6912B" Ref="C49"  Part="1" 
AR Path="/69549BC04BB6912B" Ref="C49"  Part="1" 
AR Path="/D1C3384BB6912B" Ref="C49"  Part="1" 
F 0 "C49" H 37900 22950 50  0000 L CNN
F 1 "0.1 uF" H 37900 22750 50  0000 L CNN
F 2 "C2" H 37850 22850 60  0001 C CNN
F 3 "" H 37850 22850 60  0001 C CNN
	1    37850 22850
	1    0    0    -1  
$EndComp
$Comp
L C C51
U 1 1 4BB6912A
P 38750 22850
AR Path="/4BB6912A" Ref="C51"  Part="1" 
AR Path="/FFFFFFFF4BB6912A" Ref="C51"  Part="1" 
AR Path="/755D912A4BB6912A" Ref="C?"  Part="1" 
AR Path="/73254BB6912A" Ref="C?"  Part="1" 
AR Path="/23D83C4BB6912A" Ref="C51"  Part="1" 
AR Path="/47907FA4BB6912A" Ref="C?"  Part="1" 
AR Path="/23C34C4BB6912A" Ref="C51"  Part="1" 
AR Path="/14BB6912A" Ref="C51"  Part="1" 
AR Path="/23BC884BB6912A" Ref="C51"  Part="1" 
AR Path="/773F8EB44BB6912A" Ref="C51"  Part="1" 
AR Path="/23C9F04BB6912A" Ref="C51"  Part="1" 
AR Path="/94BB6912A" Ref="C"  Part="1" 
AR Path="/FFFFFFF04BB6912A" Ref="C51"  Part="1" 
AR Path="/7E4188DA4BB6912A" Ref="C51"  Part="1" 
AR Path="/8D384BB6912A" Ref="C51"  Part="1" 
AR Path="/24BB6912A" Ref="C51"  Part="1" 
AR Path="/773F65F14BB6912A" Ref="C51"  Part="1" 
AR Path="/23C6504BB6912A" Ref="C51"  Part="1" 
AR Path="/D1C1E84BB6912A" Ref="C51"  Part="1" 
AR Path="/D058E04BB6912A" Ref="C51"  Part="1" 
AR Path="/3C64BB6912A" Ref="C51"  Part="1" 
AR Path="/23CBC44BB6912A" Ref="C51"  Part="1" 
AR Path="/2F4A4BB6912A" Ref="C51"  Part="1" 
AR Path="/23D9004BB6912A" Ref="C51"  Part="1" 
AR Path="/23CE584BB6912A" Ref="C51"  Part="1" 
AR Path="/64BB6912A" Ref="C51"  Part="1" 
AR Path="/6FE934344BB6912A" Ref="C51"  Part="1" 
AR Path="/6FE934E34BB6912A" Ref="C51"  Part="1" 
AR Path="/2824BB6912A" Ref="C51"  Part="1" 
AR Path="/69549BC04BB6912A" Ref="C51"  Part="1" 
AR Path="/D1C3384BB6912A" Ref="C51"  Part="1" 
F 0 "C51" H 38800 22950 50  0000 L CNN
F 1 "0.1 uF" H 38800 22750 50  0000 L CNN
F 2 "C2" H 38750 22850 60  0001 C CNN
F 3 "" H 38750 22850 60  0001 C CNN
	1    38750 22850
	1    0    0    -1  
$EndComp
$Comp
L C C50
U 1 1 4BB69129
P 38300 22850
AR Path="/4BB69129" Ref="C50"  Part="1" 
AR Path="/FFFFFFFF4BB69129" Ref="C50"  Part="1" 
AR Path="/755D912A4BB69129" Ref="C?"  Part="1" 
AR Path="/73254BB69129" Ref="C?"  Part="1" 
AR Path="/23D83C4BB69129" Ref="C50"  Part="1" 
AR Path="/47907FA4BB69129" Ref="C?"  Part="1" 
AR Path="/23C34C4BB69129" Ref="C50"  Part="1" 
AR Path="/14BB69129" Ref="C50"  Part="1" 
AR Path="/23BC884BB69129" Ref="C50"  Part="1" 
AR Path="/773F8EB44BB69129" Ref="C50"  Part="1" 
AR Path="/23C9F04BB69129" Ref="C50"  Part="1" 
AR Path="/94BB69129" Ref="C"  Part="1" 
AR Path="/FFFFFFF04BB69129" Ref="C50"  Part="1" 
AR Path="/7E4188DA4BB69129" Ref="C50"  Part="1" 
AR Path="/8D384BB69129" Ref="C50"  Part="1" 
AR Path="/24BB69129" Ref="C50"  Part="1" 
AR Path="/773F65F14BB69129" Ref="C50"  Part="1" 
AR Path="/23C6504BB69129" Ref="C50"  Part="1" 
AR Path="/D1C1E84BB69129" Ref="C50"  Part="1" 
AR Path="/D058E04BB69129" Ref="C50"  Part="1" 
AR Path="/3C64BB69129" Ref="C50"  Part="1" 
AR Path="/23CBC44BB69129" Ref="C50"  Part="1" 
AR Path="/2F4A4BB69129" Ref="C50"  Part="1" 
AR Path="/23D9004BB69129" Ref="C50"  Part="1" 
AR Path="/23CE584BB69129" Ref="C50"  Part="1" 
AR Path="/64BB69129" Ref="C50"  Part="1" 
AR Path="/6FE934344BB69129" Ref="C50"  Part="1" 
AR Path="/6FE934E34BB69129" Ref="C50"  Part="1" 
AR Path="/2824BB69129" Ref="C50"  Part="1" 
AR Path="/69549BC04BB69129" Ref="C50"  Part="1" 
AR Path="/D1C3384BB69129" Ref="C50"  Part="1" 
F 0 "C50" H 38350 22950 50  0000 L CNN
F 1 "0.1 uF" H 38350 22750 50  0000 L CNN
F 2 "C2" H 38300 22850 60  0001 C CNN
F 3 "" H 38300 22850 60  0001 C CNN
	1    38300 22850
	1    0    0    -1  
$EndComp
$Comp
L C C54
U 1 1 4BB69128
P 40100 22850
AR Path="/4BB69128" Ref="C54"  Part="1" 
AR Path="/FFFFFFFF4BB69128" Ref="C54"  Part="1" 
AR Path="/755D912A4BB69128" Ref="C?"  Part="1" 
AR Path="/73254BB69128" Ref="C?"  Part="1" 
AR Path="/23D83C4BB69128" Ref="C54"  Part="1" 
AR Path="/47907FA4BB69128" Ref="C?"  Part="1" 
AR Path="/23C34C4BB69128" Ref="C54"  Part="1" 
AR Path="/14BB69128" Ref="C54"  Part="1" 
AR Path="/23BC884BB69128" Ref="C54"  Part="1" 
AR Path="/773F8EB44BB69128" Ref="C54"  Part="1" 
AR Path="/23C9F04BB69128" Ref="C54"  Part="1" 
AR Path="/94BB69128" Ref="C"  Part="1" 
AR Path="/FFFFFFF04BB69128" Ref="C54"  Part="1" 
AR Path="/7E4188DA4BB69128" Ref="C54"  Part="1" 
AR Path="/8D384BB69128" Ref="C54"  Part="1" 
AR Path="/24BB69128" Ref="C54"  Part="1" 
AR Path="/773F65F14BB69128" Ref="C54"  Part="1" 
AR Path="/23C6504BB69128" Ref="C54"  Part="1" 
AR Path="/D1C1E84BB69128" Ref="C54"  Part="1" 
AR Path="/D058E04BB69128" Ref="C54"  Part="1" 
AR Path="/3C64BB69128" Ref="C54"  Part="1" 
AR Path="/23CBC44BB69128" Ref="C54"  Part="1" 
AR Path="/2F4A4BB69128" Ref="C54"  Part="1" 
AR Path="/23D9004BB69128" Ref="C54"  Part="1" 
AR Path="/23CE584BB69128" Ref="C54"  Part="1" 
AR Path="/64BB69128" Ref="C54"  Part="1" 
AR Path="/6FE934344BB69128" Ref="C54"  Part="1" 
AR Path="/6FE934E34BB69128" Ref="C54"  Part="1" 
AR Path="/2824BB69128" Ref="C54"  Part="1" 
AR Path="/69549BC04BB69128" Ref="C54"  Part="1" 
AR Path="/D1C3384BB69128" Ref="C54"  Part="1" 
F 0 "C54" H 40150 22950 50  0000 L CNN
F 1 "0.1 uF" H 40150 22750 50  0000 L CNN
F 2 "C2" H 40100 22850 60  0001 C CNN
F 3 "" H 40100 22850 60  0001 C CNN
	1    40100 22850
	1    0    0    -1  
$EndComp
$Comp
L C C53
U 1 1 4BB69127
P 39650 22850
AR Path="/4BB69127" Ref="C53"  Part="1" 
AR Path="/FFFFFFFF4BB69127" Ref="C53"  Part="1" 
AR Path="/755D912A4BB69127" Ref="C?"  Part="1" 
AR Path="/73254BB69127" Ref="C?"  Part="1" 
AR Path="/23D83C4BB69127" Ref="C53"  Part="1" 
AR Path="/47907FA4BB69127" Ref="C?"  Part="1" 
AR Path="/23C34C4BB69127" Ref="C53"  Part="1" 
AR Path="/14BB69127" Ref="C53"  Part="1" 
AR Path="/23BC884BB69127" Ref="C53"  Part="1" 
AR Path="/773F8EB44BB69127" Ref="C53"  Part="1" 
AR Path="/23C9F04BB69127" Ref="C53"  Part="1" 
AR Path="/94BB69127" Ref="C"  Part="1" 
AR Path="/FFFFFFF04BB69127" Ref="C53"  Part="1" 
AR Path="/7E4188DA4BB69127" Ref="C53"  Part="1" 
AR Path="/8D384BB69127" Ref="C53"  Part="1" 
AR Path="/24BB69127" Ref="C53"  Part="1" 
AR Path="/773F65F14BB69127" Ref="C53"  Part="1" 
AR Path="/23C6504BB69127" Ref="C53"  Part="1" 
AR Path="/D1C1E84BB69127" Ref="C53"  Part="1" 
AR Path="/D058E04BB69127" Ref="C53"  Part="1" 
AR Path="/3C64BB69127" Ref="C53"  Part="1" 
AR Path="/23CBC44BB69127" Ref="C53"  Part="1" 
AR Path="/2F4A4BB69127" Ref="C53"  Part="1" 
AR Path="/23D9004BB69127" Ref="C53"  Part="1" 
AR Path="/23CE584BB69127" Ref="C53"  Part="1" 
AR Path="/64BB69127" Ref="C53"  Part="1" 
AR Path="/6FE934344BB69127" Ref="C53"  Part="1" 
AR Path="/6FE934E34BB69127" Ref="C53"  Part="1" 
AR Path="/2824BB69127" Ref="C53"  Part="1" 
AR Path="/69549BC04BB69127" Ref="C53"  Part="1" 
AR Path="/D1C3384BB69127" Ref="C53"  Part="1" 
F 0 "C53" H 39700 22950 50  0000 L CNN
F 1 "0.1 uF" H 39700 22750 50  0000 L CNN
F 2 "C2" H 39650 22850 60  0001 C CNN
F 3 "" H 39650 22850 60  0001 C CNN
	1    39650 22850
	1    0    0    -1  
$EndComp
$Comp
L C C52
U 1 1 4BB69126
P 39200 22850
AR Path="/4BB69126" Ref="C52"  Part="1" 
AR Path="/FFFFFFFF4BB69126" Ref="C52"  Part="1" 
AR Path="/755D912A4BB69126" Ref="C?"  Part="1" 
AR Path="/73254BB69126" Ref="C?"  Part="1" 
AR Path="/23D83C4BB69126" Ref="C52"  Part="1" 
AR Path="/47907FA4BB69126" Ref="C?"  Part="1" 
AR Path="/23C34C4BB69126" Ref="C52"  Part="1" 
AR Path="/14BB69126" Ref="C52"  Part="1" 
AR Path="/23BC884BB69126" Ref="C52"  Part="1" 
AR Path="/773F8EB44BB69126" Ref="C52"  Part="1" 
AR Path="/23C9F04BB69126" Ref="C52"  Part="1" 
AR Path="/94BB69126" Ref="C"  Part="1" 
AR Path="/FFFFFFF04BB69126" Ref="C52"  Part="1" 
AR Path="/7E4188DA4BB69126" Ref="C52"  Part="1" 
AR Path="/8D384BB69126" Ref="C52"  Part="1" 
AR Path="/24BB69126" Ref="C52"  Part="1" 
AR Path="/773F65F14BB69126" Ref="C52"  Part="1" 
AR Path="/23C6504BB69126" Ref="C52"  Part="1" 
AR Path="/D1C1E84BB69126" Ref="C52"  Part="1" 
AR Path="/D058E04BB69126" Ref="C52"  Part="1" 
AR Path="/3C64BB69126" Ref="C52"  Part="1" 
AR Path="/23CBC44BB69126" Ref="C52"  Part="1" 
AR Path="/2F4A4BB69126" Ref="C52"  Part="1" 
AR Path="/23D9004BB69126" Ref="C52"  Part="1" 
AR Path="/23CE584BB69126" Ref="C52"  Part="1" 
AR Path="/64BB69126" Ref="C52"  Part="1" 
AR Path="/6FE934344BB69126" Ref="C52"  Part="1" 
AR Path="/6FE934E34BB69126" Ref="C52"  Part="1" 
AR Path="/2824BB69126" Ref="C52"  Part="1" 
AR Path="/69549BC04BB69126" Ref="C52"  Part="1" 
AR Path="/D1C3384BB69126" Ref="C52"  Part="1" 
F 0 "C52" H 39250 22950 50  0000 L CNN
F 1 "0.1 uF" H 39250 22750 50  0000 L CNN
F 2 "C2" H 39200 22850 60  0001 C CNN
F 3 "" H 39200 22850 60  0001 C CNN
	1    39200 22850
	1    0    0    -1  
$EndComp
$Comp
L C C47
U 1 1 4BB69125
P 36950 22850
AR Path="/4BB69125" Ref="C47"  Part="1" 
AR Path="/FFFFFFFF4BB69125" Ref="C47"  Part="1" 
AR Path="/755D912A4BB69125" Ref="C?"  Part="1" 
AR Path="/73254BB69125" Ref="C?"  Part="1" 
AR Path="/23D83C4BB69125" Ref="C47"  Part="1" 
AR Path="/47907FA4BB69125" Ref="C?"  Part="1" 
AR Path="/23C34C4BB69125" Ref="C47"  Part="1" 
AR Path="/14BB69125" Ref="C47"  Part="1" 
AR Path="/23BC884BB69125" Ref="C47"  Part="1" 
AR Path="/773F8EB44BB69125" Ref="C47"  Part="1" 
AR Path="/23C9F04BB69125" Ref="C47"  Part="1" 
AR Path="/94BB69125" Ref="C"  Part="1" 
AR Path="/FFFFFFF04BB69125" Ref="C47"  Part="1" 
AR Path="/7E4188DA4BB69125" Ref="C47"  Part="1" 
AR Path="/8D384BB69125" Ref="C47"  Part="1" 
AR Path="/24BB69125" Ref="C47"  Part="1" 
AR Path="/773F65F14BB69125" Ref="C47"  Part="1" 
AR Path="/23C6504BB69125" Ref="C47"  Part="1" 
AR Path="/D1C1E84BB69125" Ref="C47"  Part="1" 
AR Path="/D058E04BB69125" Ref="C47"  Part="1" 
AR Path="/3C64BB69125" Ref="C47"  Part="1" 
AR Path="/23CBC44BB69125" Ref="C47"  Part="1" 
AR Path="/2F4A4BB69125" Ref="C47"  Part="1" 
AR Path="/23D9004BB69125" Ref="C47"  Part="1" 
AR Path="/23CE584BB69125" Ref="C47"  Part="1" 
AR Path="/64BB69125" Ref="C47"  Part="1" 
AR Path="/6FE934344BB69125" Ref="C47"  Part="1" 
AR Path="/6FE934E34BB69125" Ref="C47"  Part="1" 
AR Path="/2824BB69125" Ref="C47"  Part="1" 
AR Path="/69549BC04BB69125" Ref="C47"  Part="1" 
AR Path="/D1C3384BB69125" Ref="C47"  Part="1" 
F 0 "C47" H 37000 22950 50  0000 L CNN
F 1 "0.1 uF" H 37000 22750 50  0000 L CNN
F 2 "C2" H 36950 22850 60  0001 C CNN
F 3 "" H 36950 22850 60  0001 C CNN
	1    36950 22850
	1    0    0    -1  
$EndComp
$Comp
L C C46
U 1 1 4BB69124
P 36500 22850
AR Path="/4BB69124" Ref="C46"  Part="1" 
AR Path="/FFFFFFFF4BB69124" Ref="C46"  Part="1" 
AR Path="/755D912A4BB69124" Ref="C?"  Part="1" 
AR Path="/73254BB69124" Ref="C?"  Part="1" 
AR Path="/23D83C4BB69124" Ref="C46"  Part="1" 
AR Path="/47907FA4BB69124" Ref="C?"  Part="1" 
AR Path="/23C34C4BB69124" Ref="C46"  Part="1" 
AR Path="/14BB69124" Ref="C46"  Part="1" 
AR Path="/23BC884BB69124" Ref="C46"  Part="1" 
AR Path="/773F8EB44BB69124" Ref="C46"  Part="1" 
AR Path="/23C9F04BB69124" Ref="C46"  Part="1" 
AR Path="/94BB69124" Ref="C"  Part="1" 
AR Path="/FFFFFFF04BB69124" Ref="C46"  Part="1" 
AR Path="/7E4188DA4BB69124" Ref="C46"  Part="1" 
AR Path="/8D384BB69124" Ref="C46"  Part="1" 
AR Path="/24BB69124" Ref="C46"  Part="1" 
AR Path="/773F65F14BB69124" Ref="C46"  Part="1" 
AR Path="/23C6504BB69124" Ref="C46"  Part="1" 
AR Path="/D1C1E84BB69124" Ref="C46"  Part="1" 
AR Path="/D058E04BB69124" Ref="C46"  Part="1" 
AR Path="/3C64BB69124" Ref="C46"  Part="1" 
AR Path="/23CBC44BB69124" Ref="C46"  Part="1" 
AR Path="/2F4A4BB69124" Ref="C46"  Part="1" 
AR Path="/23D9004BB69124" Ref="C46"  Part="1" 
AR Path="/23CE584BB69124" Ref="C46"  Part="1" 
AR Path="/64BB69124" Ref="C46"  Part="1" 
AR Path="/6FE934344BB69124" Ref="C46"  Part="1" 
AR Path="/6FE934E34BB69124" Ref="C46"  Part="1" 
AR Path="/2824BB69124" Ref="C46"  Part="1" 
AR Path="/69549BC04BB69124" Ref="C46"  Part="1" 
AR Path="/D1C3384BB69124" Ref="C46"  Part="1" 
F 0 "C46" H 36550 22950 50  0000 L CNN
F 1 "0.1 uF" H 36550 22750 50  0000 L CNN
F 2 "C2" H 36500 22850 60  0001 C CNN
F 3 "" H 36500 22850 60  0001 C CNN
	1    36500 22850
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR019
U 1 1 4BB69123
P 36500 22650
AR Path="/4BB69123" Ref="#PWR019"  Part="1" 
AR Path="/FFFFFFFF4BB69123" Ref="#PWR022"  Part="1" 
AR Path="/755D912A4BB69123" Ref="#PWR?"  Part="1" 
AR Path="/73254BB69123" Ref="#PWR?"  Part="1" 
AR Path="/23D83C4BB69123" Ref="#PWR62"  Part="1" 
AR Path="/47907FA4BB69123" Ref="#PWR014"  Part="1" 
AR Path="/23C34C4BB69123" Ref="#PWR021"  Part="1" 
AR Path="/14BB69123" Ref="#PWR019"  Part="1" 
AR Path="/23BC884BB69123" Ref="#PWR022"  Part="1" 
AR Path="/773F8EB44BB69123" Ref="#PWR019"  Part="1" 
AR Path="/23C9F04BB69123" Ref="#PWR47"  Part="1" 
AR Path="/94BB69123" Ref="#PWR"  Part="1" 
AR Path="/FFFFFFF04BB69123" Ref="#PWR65"  Part="1" 
AR Path="/7E4188DA4BB69123" Ref="#PWR62"  Part="1" 
AR Path="/8D384BB69123" Ref="#PWR014"  Part="1" 
AR Path="/773F65F14BB69123" Ref="#PWR022"  Part="1" 
AR Path="/6FF0DD404BB69123" Ref="#PWR022"  Part="1" 
AR Path="/24BB69123" Ref="#PWR47"  Part="1" 
AR Path="/23C6504BB69123" Ref="#PWR015"  Part="1" 
AR Path="/D1C1E84BB69123" Ref="#PWR015"  Part="1" 
AR Path="/D058E04BB69123" Ref="#PWR022"  Part="1" 
AR Path="/6684D64BB69123" Ref="#PWR019"  Part="1" 
AR Path="/3C64BB69123" Ref="#PWR015"  Part="1" 
AR Path="/23CBC44BB69123" Ref="#PWR019"  Part="1" 
AR Path="/2F4A4BB69123" Ref="#PWR015"  Part="1" 
AR Path="/23D9004BB69123" Ref="#PWR019"  Part="1" 
AR Path="/23CE584BB69123" Ref="#PWR017"  Part="1" 
AR Path="/64BB69123" Ref="#PWR58"  Part="1" 
AR Path="/6FE934344BB69123" Ref="#PWR58"  Part="1" 
AR Path="/6FE934E34BB69123" Ref="#PWR019"  Part="1" 
AR Path="/2824BB69123" Ref="#PWR019"  Part="1" 
AR Path="/69549BC04BB69123" Ref="#PWR019"  Part="1" 
AR Path="/D1C3384BB69123" Ref="#PWR022"  Part="1" 
F 0 "#PWR019" H 36500 22750 30  0001 C CNN
F 1 "VCC" H 36500 22750 30  0000 C CNN
F 2 "" H 36500 22650 60  0001 C CNN
F 3 "" H 36500 22650 60  0001 C CNN
	1    36500 22650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR020
U 1 1 4BB69122
P 36950 23100
AR Path="/4BB69122" Ref="#PWR020"  Part="1" 
AR Path="/FFFFFFFF4BB69122" Ref="#PWR023"  Part="1" 
AR Path="/755D912A4BB69122" Ref="#PWR?"  Part="1" 
AR Path="/73254BB69122" Ref="#PWR?"  Part="1" 
AR Path="/23D83C4BB69122" Ref="#PWR66"  Part="1" 
AR Path="/47907FA4BB69122" Ref="#PWR015"  Part="1" 
AR Path="/23C34C4BB69122" Ref="#PWR022"  Part="1" 
AR Path="/14BB69122" Ref="#PWR020"  Part="1" 
AR Path="/23BC884BB69122" Ref="#PWR023"  Part="1" 
AR Path="/773F8EB44BB69122" Ref="#PWR020"  Part="1" 
AR Path="/23C9F04BB69122" Ref="#PWR52"  Part="1" 
AR Path="/94BB69122" Ref="#PWR"  Part="1" 
AR Path="/FFFFFFF04BB69122" Ref="#PWR70"  Part="1" 
AR Path="/7E4188DA4BB69122" Ref="#PWR66"  Part="1" 
AR Path="/8D384BB69122" Ref="#PWR015"  Part="1" 
AR Path="/773F65F14BB69122" Ref="#PWR023"  Part="1" 
AR Path="/6FF0DD404BB69122" Ref="#PWR023"  Part="1" 
AR Path="/24BB69122" Ref="#PWR52"  Part="1" 
AR Path="/23C6504BB69122" Ref="#PWR016"  Part="1" 
AR Path="/D1C1E84BB69122" Ref="#PWR016"  Part="1" 
AR Path="/D058E04BB69122" Ref="#PWR023"  Part="1" 
AR Path="/6684D64BB69122" Ref="#PWR020"  Part="1" 
AR Path="/3C64BB69122" Ref="#PWR016"  Part="1" 
AR Path="/23CBC44BB69122" Ref="#PWR020"  Part="1" 
AR Path="/2F4A4BB69122" Ref="#PWR016"  Part="1" 
AR Path="/23D9004BB69122" Ref="#PWR020"  Part="1" 
AR Path="/23CE584BB69122" Ref="#PWR018"  Part="1" 
AR Path="/64BB69122" Ref="#PWR63"  Part="1" 
AR Path="/6FE934344BB69122" Ref="#PWR63"  Part="1" 
AR Path="/6FE934E34BB69122" Ref="#PWR020"  Part="1" 
AR Path="/2824BB69122" Ref="#PWR020"  Part="1" 
AR Path="/69549BC04BB69122" Ref="#PWR020"  Part="1" 
AR Path="/D1C3384BB69122" Ref="#PWR023"  Part="1" 
F 0 "#PWR020" H 36950 23100 30  0001 C CNN
F 1 "GND" H 36950 23030 30  0001 C CNN
F 2 "" H 36950 23100 60  0001 C CNN
F 3 "" H 36950 23100 60  0001 C CNN
	1    36950 23100
	1    0    0    -1  
$EndComp
$Comp
L C C59
U 1 1 4BB69120
P 41000 22850
AR Path="/4BB69120" Ref="C59"  Part="1" 
AR Path="/FFFFFFFF4BB69120" Ref="C59"  Part="1" 
AR Path="/755D912A4BB69120" Ref="C?"  Part="1" 
AR Path="/73254BB69120" Ref="C?"  Part="1" 
AR Path="/23D83C4BB69120" Ref="C59"  Part="1" 
AR Path="/47907FA4BB69120" Ref="C?"  Part="1" 
AR Path="/23C34C4BB69120" Ref="C59"  Part="1" 
AR Path="/14BB69120" Ref="C59"  Part="1" 
AR Path="/23BC884BB69120" Ref="C59"  Part="1" 
AR Path="/773F8EB44BB69120" Ref="C59"  Part="1" 
AR Path="/94BB69120" Ref="C"  Part="1" 
AR Path="/23C9F04BB69120" Ref="C59"  Part="1" 
AR Path="/FFFFFFF04BB69120" Ref="C59"  Part="1" 
AR Path="/7E4188DA4BB69120" Ref="C59"  Part="1" 
AR Path="/8D384BB69120" Ref="C59"  Part="1" 
AR Path="/24BB69120" Ref="C59"  Part="1" 
AR Path="/773F65F14BB69120" Ref="C59"  Part="1" 
AR Path="/23C6504BB69120" Ref="C59"  Part="1" 
AR Path="/D1C1E84BB69120" Ref="C59"  Part="1" 
AR Path="/D058E04BB69120" Ref="C59"  Part="1" 
AR Path="/3C64BB69120" Ref="C59"  Part="1" 
AR Path="/23CBC44BB69120" Ref="C59"  Part="1" 
AR Path="/2F4A4BB69120" Ref="C59"  Part="1" 
AR Path="/23D9004BB69120" Ref="C59"  Part="1" 
AR Path="/23CE584BB69120" Ref="C59"  Part="1" 
AR Path="/64BB69120" Ref="C59"  Part="1" 
AR Path="/6FE934344BB69120" Ref="C59"  Part="1" 
AR Path="/6FE934E34BB69120" Ref="C59"  Part="1" 
AR Path="/2824BB69120" Ref="C59"  Part="1" 
AR Path="/69549BC04BB69120" Ref="C59"  Part="1" 
AR Path="/D1C3384BB69120" Ref="C59"  Part="1" 
F 0 "C59" H 41050 22950 50  0000 L CNN
F 1 "0.1 uF" H 41050 22750 50  0000 L CNN
F 2 "C2" H 41000 22850 60  0001 C CNN
F 3 "" H 41000 22850 60  0001 C CNN
	1    41000 22850
	1    0    0    -1  
$EndComp
$Comp
L C C57
U 1 1 4BB690F2
P 41000 21650
AR Path="/4BB690F2" Ref="C57"  Part="1" 
AR Path="/FFFFFFFF4BB690F2" Ref="C57"  Part="1" 
AR Path="/755D912A4BB690F2" Ref="C?"  Part="1" 
AR Path="/73254BB690F2" Ref="C?"  Part="1" 
AR Path="/23D83C4BB690F2" Ref="C57"  Part="1" 
AR Path="/47907FA4BB690F2" Ref="C?"  Part="1" 
AR Path="/23C34C4BB690F2" Ref="C57"  Part="1" 
AR Path="/14BB690F2" Ref="C57"  Part="1" 
AR Path="/23BC884BB690F2" Ref="C57"  Part="1" 
AR Path="/773F8EB44BB690F2" Ref="C57"  Part="1" 
AR Path="/23C9F04BB690F2" Ref="C57"  Part="1" 
AR Path="/94BB690F2" Ref="C"  Part="1" 
AR Path="/FFFFFFF04BB690F2" Ref="C57"  Part="1" 
AR Path="/7E4188DA4BB690F2" Ref="C57"  Part="1" 
AR Path="/8D384BB690F2" Ref="C57"  Part="1" 
AR Path="/24BB690F2" Ref="C57"  Part="1" 
AR Path="/773F65F14BB690F2" Ref="C57"  Part="1" 
AR Path="/23C6504BB690F2" Ref="C57"  Part="1" 
AR Path="/D1C1E84BB690F2" Ref="C57"  Part="1" 
AR Path="/D058E04BB690F2" Ref="C57"  Part="1" 
AR Path="/3C64BB690F2" Ref="C57"  Part="1" 
AR Path="/23CBC44BB690F2" Ref="C57"  Part="1" 
AR Path="/2F4A4BB690F2" Ref="C57"  Part="1" 
AR Path="/23D9004BB690F2" Ref="C57"  Part="1" 
AR Path="/23CE584BB690F2" Ref="C57"  Part="1" 
AR Path="/64BB690F2" Ref="C57"  Part="1" 
AR Path="/6FE934344BB690F2" Ref="C57"  Part="1" 
AR Path="/6FE934E34BB690F2" Ref="C57"  Part="1" 
AR Path="/2824BB690F2" Ref="C57"  Part="1" 
AR Path="/69549BC04BB690F2" Ref="C57"  Part="1" 
AR Path="/D1C3384BB690F2" Ref="C57"  Part="1" 
F 0 "C57" H 41050 21750 50  0000 L CNN
F 1 "0.1 uF" H 41050 21550 50  0000 L CNN
F 2 "C2" H 41000 21650 60  0001 C CNN
F 3 "" H 41000 21650 60  0001 C CNN
	1    41000 21650
	1    0    0    -1  
$EndComp
$Comp
L C C58
U 1 1 4BB690E8
P 41000 22250
AR Path="/4BB690E8" Ref="C58"  Part="1" 
AR Path="/FFFFFFFF4BB690E8" Ref="C58"  Part="1" 
AR Path="/755D912A4BB690E8" Ref="C?"  Part="1" 
AR Path="/73254BB690E8" Ref="C?"  Part="1" 
AR Path="/23D83C4BB690E8" Ref="C58"  Part="1" 
AR Path="/47907FA4BB690E8" Ref="C?"  Part="1" 
AR Path="/23C34C4BB690E8" Ref="C58"  Part="1" 
AR Path="/14BB690E8" Ref="C58"  Part="1" 
AR Path="/23BC884BB690E8" Ref="C58"  Part="1" 
AR Path="/773F8EB44BB690E8" Ref="C58"  Part="1" 
AR Path="/94BB690E8" Ref="C"  Part="1" 
AR Path="/23C9F04BB690E8" Ref="C58"  Part="1" 
AR Path="/FFFFFFF04BB690E8" Ref="C58"  Part="1" 
AR Path="/7E4188DA4BB690E8" Ref="C58"  Part="1" 
AR Path="/8D384BB690E8" Ref="C58"  Part="1" 
AR Path="/24BB690E8" Ref="C58"  Part="1" 
AR Path="/773F65F14BB690E8" Ref="C58"  Part="1" 
AR Path="/23C6504BB690E8" Ref="C58"  Part="1" 
AR Path="/D1C1E84BB690E8" Ref="C58"  Part="1" 
AR Path="/D058E04BB690E8" Ref="C58"  Part="1" 
AR Path="/3C64BB690E8" Ref="C58"  Part="1" 
AR Path="/23CBC44BB690E8" Ref="C58"  Part="1" 
AR Path="/2F4A4BB690E8" Ref="C58"  Part="1" 
AR Path="/23D9004BB690E8" Ref="C58"  Part="1" 
AR Path="/23CE584BB690E8" Ref="C58"  Part="1" 
AR Path="/64BB690E8" Ref="C58"  Part="1" 
AR Path="/6FE934344BB690E8" Ref="C58"  Part="1" 
AR Path="/6FE934E34BB690E8" Ref="C58"  Part="1" 
AR Path="/2824BB690E8" Ref="C58"  Part="1" 
AR Path="/69549BC04BB690E8" Ref="C58"  Part="1" 
AR Path="/D1C3384BB690E8" Ref="C58"  Part="1" 
F 0 "C58" H 41050 22350 50  0000 L CNN
F 1 "0.1 uF" H 41050 22150 50  0000 L CNN
F 2 "C2" H 41000 22250 60  0001 C CNN
F 3 "" H 41000 22250 60  0001 C CNN
	1    41000 22250
	1    0    0    -1  
$EndComp
$Comp
L 74LS04 U?
U 2 1 4BB69034
P 8450 26650
AR Path="/2300384BB69034" Ref="U?"  Part="1" 
AR Path="/4BB69034" Ref="U36"  Part="2" 
AR Path="/FFFFFFFF4BB69034" Ref="U36"  Part="1" 
AR Path="/755D912A4BB69034" Ref="U?"  Part="1" 
AR Path="/73254BB69034" Ref="U?"  Part="1" 
AR Path="/31324BB69034" Ref="U?"  Part="1" 
AR Path="/2BFC204BB69034" Ref="U?"  Part="1" 
AR Path="/2E0093E4BB69034" Ref="U"  Part="2" 
AR Path="/23D83C4BB69034" Ref="U36"  Part="2" 
AR Path="/47907FA4BB69034" Ref="U36"  Part="2" 
AR Path="/23C34C4BB69034" Ref="U36"  Part="2" 
AR Path="/14BB69034" Ref="U36"  Part="2" 
AR Path="/23BC884BB69034" Ref="U36"  Part="2" 
AR Path="/773F8EB44BB69034" Ref="U36"  Part="2" 
AR Path="/94BB69034" Ref="U"  Part="2" 
AR Path="/23C9F04BB69034" Ref="U36"  Part="2" 
AR Path="/FFFFFFF04BB69034" Ref="U36"  Part="2" 
AR Path="/7E4188DA4BB69034" Ref="U36"  Part="2" 
AR Path="/8D384BB69034" Ref="U36"  Part="2" 
AR Path="/24BB69034" Ref="U36"  Part="2" 
AR Path="/773F65F14BB69034" Ref="U36"  Part="2" 
AR Path="/23C6504BB69034" Ref="U36"  Part="2" 
AR Path="/D1C1E84BB69034" Ref="U36"  Part="2" 
AR Path="/D058E04BB69034" Ref="U36"  Part="2" 
AR Path="/3C64BB69034" Ref="U36"  Part="2" 
AR Path="/23CBC44BB69034" Ref="U36"  Part="2" 
AR Path="/2F4A4BB69034" Ref="U36"  Part="2" 
AR Path="/23D9004BB69034" Ref="U36"  Part="2" 
AR Path="/23CE584BB69034" Ref="U36"  Part="2" 
AR Path="/64BB69034" Ref="U36"  Part="2" 
AR Path="/6FE934344BB69034" Ref="U36"  Part="2" 
AR Path="/6FE934E34BB69034" Ref="U36"  Part="2" 
AR Path="/2824BB69034" Ref="U36"  Part="2" 
AR Path="/69549BC04BB69034" Ref="U36"  Part="2" 
AR Path="/D1C3384BB69034" Ref="U36"  Part="2" 
F 0 "U36" H 8645 26765 60  0000 C CNN
F 1 "74LS04" H 8640 26525 60  0000 C CNN
F 2 "14dip300" H 8640 26625 60  0001 C CNN
F 3 "" H 8450 26650 60  0001 C CNN
	2    8450 26650
	1    0    0    -1  
$EndComp
Text Notes 5100 25800 0    60   ~ 0
4 MHz TTL OSCILLATOR
$Comp
L DIL14 P?
U 1 1 4BB6902E
P 5450 26350
AR Path="/679E884BB6902E" Ref="P?"  Part="1" 
AR Path="/695513124BB6902E" Ref="P?"  Part="1" 
AR Path="/4BB6902E" Ref="U46"  Part="1" 
AR Path="/FFFFFFFF4BB6902E" Ref="U46"  Part="1" 
AR Path="/755D912A4BB6902E" Ref="P?"  Part="1" 
AR Path="/73254BB6902E" Ref="P?"  Part="1" 
AR Path="/23D83C4BB6902E" Ref="U46"  Part="1" 
AR Path="/47907FA4BB6902E" Ref="P?"  Part="1" 
AR Path="/23C34C4BB6902E" Ref="U46"  Part="1" 
AR Path="/14BB6902E" Ref="U46"  Part="1" 
AR Path="/23BC884BB6902E" Ref="U46"  Part="1" 
AR Path="/10031324BB6902E" Ref="P?"  Part="1" 
AR Path="/7E4188DA4BB6902E" Ref="U46"  Part="1" 
AR Path="/773F8EB44BB6902E" Ref="U46"  Part="1" 
AR Path="/94BB6902E" Ref="U"  Part="1" 
AR Path="/23C9F04BB6902E" Ref="U46"  Part="1" 
AR Path="/FFFFFFF04BB6902E" Ref="U46"  Part="1" 
AR Path="/8D384BB6902E" Ref="U46"  Part="1" 
AR Path="/24BB6902E" Ref="U46"  Part="1" 
AR Path="/773F65F14BB6902E" Ref="U46"  Part="1" 
AR Path="/23C6504BB6902E" Ref="U46"  Part="1" 
AR Path="/D1C1E84BB6902E" Ref="U46"  Part="1" 
AR Path="/D058E04BB6902E" Ref="U46"  Part="1" 
AR Path="/3C64BB6902E" Ref="U46"  Part="1" 
AR Path="/23CBC44BB6902E" Ref="U46"  Part="1" 
AR Path="/2F4A4BB6902E" Ref="U46"  Part="1" 
AR Path="/23D9004BB6902E" Ref="U46"  Part="1" 
AR Path="/23CE584BB6902E" Ref="U46"  Part="1" 
AR Path="/64BB6902E" Ref="U46"  Part="1" 
AR Path="/6FE934344BB6902E" Ref="U46"  Part="1" 
AR Path="/6FE934E34BB6902E" Ref="U46"  Part="1" 
AR Path="/2824BB6902E" Ref="U46"  Part="1" 
AR Path="/69549BC04BB6902E" Ref="U46"  Part="1" 
AR Path="/D1C3384BB6902E" Ref="U46"  Part="1" 
F 0 "U46" H 5450 26750 60  0000 C CNN
F 1 "4.0000 MHz" V 5450 26350 50  0000 C CNN
F 2 "14dip300" V 5550 26350 50  0001 C CNN
F 3 "" H 5450 26350 60  0001 C CNN
	1    5450 26350
	1    0    0    -1  
$EndComp
NoConn ~ 5100 26250
NoConn ~ 5100 26150
NoConn ~ 5100 26050
$Comp
L VCC #PWR021
U 1 1 4BB6902D
P 5800 26050
AR Path="/4BB6902D" Ref="#PWR021"  Part="1" 
AR Path="/FFFFFFFF4BB6902D" Ref="#PWR024"  Part="1" 
AR Path="/755D912A4BB6902D" Ref="#PWR?"  Part="1" 
AR Path="/73254BB6902D" Ref="#PWR?"  Part="1" 
AR Path="/23D83C4BB6902D" Ref="#PWR22"  Part="1" 
AR Path="/47907FA4BB6902D" Ref="#PWR016"  Part="1" 
AR Path="/23C34C4BB6902D" Ref="#PWR023"  Part="1" 
AR Path="/14BB6902D" Ref="#PWR021"  Part="1" 
AR Path="/23BC884BB6902D" Ref="#PWR024"  Part="1" 
AR Path="/773F8EB44BB6902D" Ref="#PWR021"  Part="1" 
AR Path="/94BB6902D" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB6902D" Ref="#PWR17"  Part="1" 
AR Path="/FFFFFFF04BB6902D" Ref="#PWR22"  Part="1" 
AR Path="/7E4188DA4BB6902D" Ref="#PWR22"  Part="1" 
AR Path="/8D384BB6902D" Ref="#PWR016"  Part="1" 
AR Path="/773F65F14BB6902D" Ref="#PWR024"  Part="1" 
AR Path="/6FF0DD404BB6902D" Ref="#PWR024"  Part="1" 
AR Path="/24BB6902D" Ref="#PWR17"  Part="1" 
AR Path="/23C6504BB6902D" Ref="#PWR017"  Part="1" 
AR Path="/D1C1E84BB6902D" Ref="#PWR017"  Part="1" 
AR Path="/D058E04BB6902D" Ref="#PWR024"  Part="1" 
AR Path="/6684D64BB6902D" Ref="#PWR021"  Part="1" 
AR Path="/3C64BB6902D" Ref="#PWR017"  Part="1" 
AR Path="/23CBC44BB6902D" Ref="#PWR021"  Part="1" 
AR Path="/2F4A4BB6902D" Ref="#PWR017"  Part="1" 
AR Path="/23D9004BB6902D" Ref="#PWR021"  Part="1" 
AR Path="/23CE584BB6902D" Ref="#PWR019"  Part="1" 
AR Path="/64BB6902D" Ref="#PWR20"  Part="1" 
AR Path="/6FE934344BB6902D" Ref="#PWR20"  Part="1" 
AR Path="/6FE934E34BB6902D" Ref="#PWR021"  Part="1" 
AR Path="/2824BB6902D" Ref="#PWR021"  Part="1" 
AR Path="/69549BC04BB6902D" Ref="#PWR021"  Part="1" 
AR Path="/D1C3384BB6902D" Ref="#PWR024"  Part="1" 
F 0 "#PWR021" H 5800 26150 30  0001 C CNN
F 1 "VCC" H 5800 26150 30  0000 C CNN
F 2 "" H 5800 26050 60  0001 C CNN
F 3 "" H 5800 26050 60  0001 C CNN
	1    5800 26050
	1    0    0    -1  
$EndComp
NoConn ~ 5800 26150
NoConn ~ 5800 26250
$Comp
L GND #PWR022
U 1 1 4BB6902C
P 5100 26800
AR Path="/4BB6902C" Ref="#PWR022"  Part="1" 
AR Path="/FFFFFFFF4BB6902C" Ref="#PWR025"  Part="1" 
AR Path="/755D912A4BB6902C" Ref="#PWR?"  Part="1" 
AR Path="/73254BB6902C" Ref="#PWR?"  Part="1" 
AR Path="/23D83C4BB6902C" Ref="#PWR21"  Part="1" 
AR Path="/47907FA4BB6902C" Ref="#PWR017"  Part="1" 
AR Path="/23C34C4BB6902C" Ref="#PWR024"  Part="1" 
AR Path="/14BB6902C" Ref="#PWR022"  Part="1" 
AR Path="/23BC884BB6902C" Ref="#PWR025"  Part="1" 
AR Path="/773F8EB44BB6902C" Ref="#PWR022"  Part="1" 
AR Path="/94BB6902C" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB6902C" Ref="#PWR16"  Part="1" 
AR Path="/FFFFFFF04BB6902C" Ref="#PWR21"  Part="1" 
AR Path="/7E4188DA4BB6902C" Ref="#PWR21"  Part="1" 
AR Path="/8D384BB6902C" Ref="#PWR017"  Part="1" 
AR Path="/773F65F14BB6902C" Ref="#PWR025"  Part="1" 
AR Path="/6FF0DD404BB6902C" Ref="#PWR025"  Part="1" 
AR Path="/24BB6902C" Ref="#PWR16"  Part="1" 
AR Path="/23C6504BB6902C" Ref="#PWR018"  Part="1" 
AR Path="/D1C1E84BB6902C" Ref="#PWR018"  Part="1" 
AR Path="/D058E04BB6902C" Ref="#PWR025"  Part="1" 
AR Path="/6684D64BB6902C" Ref="#PWR022"  Part="1" 
AR Path="/3C64BB6902C" Ref="#PWR018"  Part="1" 
AR Path="/23CBC44BB6902C" Ref="#PWR022"  Part="1" 
AR Path="/2F4A4BB6902C" Ref="#PWR018"  Part="1" 
AR Path="/23D9004BB6902C" Ref="#PWR022"  Part="1" 
AR Path="/23CE584BB6902C" Ref="#PWR020"  Part="1" 
AR Path="/64BB6902C" Ref="#PWR19"  Part="1" 
AR Path="/6FE934344BB6902C" Ref="#PWR19"  Part="1" 
AR Path="/6FE934E34BB6902C" Ref="#PWR022"  Part="1" 
AR Path="/2824BB6902C" Ref="#PWR022"  Part="1" 
AR Path="/69549BC04BB6902C" Ref="#PWR022"  Part="1" 
AR Path="/D1C3384BB6902C" Ref="#PWR025"  Part="1" 
F 0 "#PWR022" H 5100 26800 30  0001 C CNN
F 1 "GND" H 5100 26730 30  0001 C CNN
F 2 "" H 5100 26800 60  0001 C CNN
F 3 "" H 5100 26800 60  0001 C CNN
	1    5100 26800
	1    0    0    -1  
$EndComp
$Comp
L C C44
U 1 1 4BB6902B
P 6000 26250
AR Path="/4BB6902B" Ref="C44"  Part="1" 
AR Path="/FFFFFFFF4BB6902B" Ref="C44"  Part="1" 
AR Path="/755D912A4BB6902B" Ref="C?"  Part="1" 
AR Path="/73254BB6902B" Ref="C?"  Part="1" 
AR Path="/23D83C4BB6902B" Ref="C44"  Part="1" 
AR Path="/47907FA4BB6902B" Ref="C?"  Part="1" 
AR Path="/23C34C4BB6902B" Ref="C44"  Part="1" 
AR Path="/14BB6902B" Ref="C44"  Part="1" 
AR Path="/23BC884BB6902B" Ref="C44"  Part="1" 
AR Path="/773F8EB44BB6902B" Ref="C44"  Part="1" 
AR Path="/94BB6902B" Ref="C"  Part="1" 
AR Path="/23C9F04BB6902B" Ref="C44"  Part="1" 
AR Path="/FFFFFFF04BB6902B" Ref="C44"  Part="1" 
AR Path="/7E4188DA4BB6902B" Ref="C44"  Part="1" 
AR Path="/8D384BB6902B" Ref="C44"  Part="1" 
AR Path="/24BB6902B" Ref="C44"  Part="1" 
AR Path="/773F65F14BB6902B" Ref="C44"  Part="1" 
AR Path="/23C6504BB6902B" Ref="C44"  Part="1" 
AR Path="/D1C1E84BB6902B" Ref="C44"  Part="1" 
AR Path="/D058E04BB6902B" Ref="C44"  Part="1" 
AR Path="/3C64BB6902B" Ref="C44"  Part="1" 
AR Path="/23CBC44BB6902B" Ref="C44"  Part="1" 
AR Path="/2F4A4BB6902B" Ref="C44"  Part="1" 
AR Path="/23D9004BB6902B" Ref="C44"  Part="1" 
AR Path="/23CE584BB6902B" Ref="C44"  Part="1" 
AR Path="/64BB6902B" Ref="C44"  Part="1" 
AR Path="/6FE934344BB6902B" Ref="C44"  Part="1" 
AR Path="/6FE934E34BB6902B" Ref="C44"  Part="1" 
AR Path="/2824BB6902B" Ref="C44"  Part="1" 
AR Path="/69549BC04BB6902B" Ref="C44"  Part="1" 
AR Path="/D1C3384BB6902B" Ref="C44"  Part="1" 
F 0 "C44" H 6050 26350 50  0000 L CNN
F 1 "0.1 uF" H 6050 26150 50  0000 L CNN
F 2 "C2" H 6050 26250 50  0001 C CNN
F 3 "" H 6000 26250 60  0001 C CNN
	1    6000 26250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR023
U 1 1 4BB6902A
P 6000 26450
AR Path="/4BB6902A" Ref="#PWR023"  Part="1" 
AR Path="/FFFFFFFF4BB6902A" Ref="#PWR026"  Part="1" 
AR Path="/755D912A4BB6902A" Ref="#PWR?"  Part="1" 
AR Path="/73254BB6902A" Ref="#PWR?"  Part="1" 
AR Path="/23D83C4BB6902A" Ref="#PWR24"  Part="1" 
AR Path="/47907FA4BB6902A" Ref="#PWR018"  Part="1" 
AR Path="/23C34C4BB6902A" Ref="#PWR025"  Part="1" 
AR Path="/14BB6902A" Ref="#PWR023"  Part="1" 
AR Path="/23BC884BB6902A" Ref="#PWR026"  Part="1" 
AR Path="/773F8EB44BB6902A" Ref="#PWR023"  Part="1" 
AR Path="/94BB6902A" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB6902A" Ref="#PWR19"  Part="1" 
AR Path="/FFFFFFF04BB6902A" Ref="#PWR25"  Part="1" 
AR Path="/7E4188DA4BB6902A" Ref="#PWR24"  Part="1" 
AR Path="/8D384BB6902A" Ref="#PWR018"  Part="1" 
AR Path="/773F65F14BB6902A" Ref="#PWR026"  Part="1" 
AR Path="/6FF0DD404BB6902A" Ref="#PWR026"  Part="1" 
AR Path="/24BB6902A" Ref="#PWR19"  Part="1" 
AR Path="/23C6504BB6902A" Ref="#PWR019"  Part="1" 
AR Path="/D1C1E84BB6902A" Ref="#PWR019"  Part="1" 
AR Path="/D058E04BB6902A" Ref="#PWR026"  Part="1" 
AR Path="/6684D64BB6902A" Ref="#PWR023"  Part="1" 
AR Path="/3C64BB6902A" Ref="#PWR019"  Part="1" 
AR Path="/23CBC44BB6902A" Ref="#PWR023"  Part="1" 
AR Path="/2F4A4BB6902A" Ref="#PWR019"  Part="1" 
AR Path="/23D9004BB6902A" Ref="#PWR023"  Part="1" 
AR Path="/23CE584BB6902A" Ref="#PWR021"  Part="1" 
AR Path="/64BB6902A" Ref="#PWR22"  Part="1" 
AR Path="/6FE934344BB6902A" Ref="#PWR22"  Part="1" 
AR Path="/6FE934E34BB6902A" Ref="#PWR023"  Part="1" 
AR Path="/2824BB6902A" Ref="#PWR023"  Part="1" 
AR Path="/69549BC04BB6902A" Ref="#PWR023"  Part="1" 
AR Path="/D1C3384BB6902A" Ref="#PWR026"  Part="1" 
F 0 "#PWR023" H 6000 26450 30  0001 C CNN
F 1 "GND" H 6000 26380 30  0001 C CNN
F 2 "" H 6000 26450 60  0001 C CNN
F 3 "" H 6000 26450 60  0001 C CNN
	1    6000 26450
	1    0    0    -1  
$EndComp
Text Label 20550 31150 0    60   ~ 0
NMI*
Text Label 20550 30950 0    60   ~ 0
PWRFAIL*
$Comp
L CONN_3 K?
U 1 1 4BB68E17
P 21500 31050
AR Path="/2300384BB68E17" Ref="K?"  Part="1" 
AR Path="/4BB68E17" Ref="K1"  Part="1" 
AR Path="/FFFFFFFF4BB68E17" Ref="K1"  Part="1" 
AR Path="/755D912A4BB68E17" Ref="K?"  Part="1" 
AR Path="/73254BB68E17" Ref="K?"  Part="1" 
AR Path="/31324BB68E17" Ref="K?"  Part="1" 
AR Path="/2606084BB68E17" Ref="K1"  Part="1" 
AR Path="/23D83C4BB68E17" Ref="K1"  Part="1" 
AR Path="/47907FA4BB68E17" Ref="K1"  Part="1" 
AR Path="/23C34C4BB68E17" Ref="K1"  Part="1" 
AR Path="/14BB68E17" Ref="K1"  Part="1" 
AR Path="/23BC884BB68E17" Ref="K1"  Part="1" 
AR Path="/773F8EB44BB68E17" Ref="K1"  Part="1" 
AR Path="/94BB68E17" Ref="K"  Part="1" 
AR Path="/23C9F04BB68E17" Ref="K1"  Part="1" 
AR Path="/FFFFFFF04BB68E17" Ref="K1"  Part="1" 
AR Path="/7E4188DA4BB68E17" Ref="K1"  Part="1" 
AR Path="/8D384BB68E17" Ref="K1"  Part="1" 
AR Path="/24BB68E17" Ref="K1"  Part="1" 
AR Path="/773F65F14BB68E17" Ref="K1"  Part="1" 
AR Path="/23C6504BB68E17" Ref="K1"  Part="1" 
AR Path="/D1C1E84BB68E17" Ref="K1"  Part="1" 
AR Path="/D058E04BB68E17" Ref="K1"  Part="1" 
AR Path="/3C64BB68E17" Ref="K1"  Part="1" 
AR Path="/23CBC44BB68E17" Ref="K1"  Part="1" 
AR Path="/2F4A4BB68E17" Ref="K1"  Part="1" 
AR Path="/23D9004BB68E17" Ref="K1"  Part="1" 
AR Path="/23CE584BB68E17" Ref="K1"  Part="1" 
AR Path="/64BB68E17" Ref="K1"  Part="1" 
AR Path="/6FE934344BB68E17" Ref="K1"  Part="1" 
AR Path="/6FE934E34BB68E17" Ref="K1"  Part="1" 
AR Path="/2824BB68E17" Ref="K1"  Part="1" 
AR Path="/69549BC04BB68E17" Ref="K1"  Part="1" 
AR Path="/D1C3384BB68E17" Ref="K1"  Part="1" 
F 0 "K1" V 21450 31050 50  0000 C CNN
F 1 "CONN_3" V 21550 31050 40  0000 C CNN
F 2 "SIL-3" V 21650 31050 40  0001 C CNN
F 3 "" H 21500 31050 60  0001 C CNN
	1    21500 31050
	1    0    0    -1  
$EndComp
Text Label 17450 28050 0    60   ~ 0
ROM_SELECT
Text Label 9600 28050 0    60   ~ 0
IO_WAIT
Text Label 9600 27800 0    60   ~ 0
MEM_WAIT
Text Label 9600 27550 0    60   ~ 0
M1_WAIT
$Comp
L 74LS32 U?
U 3 1 4BB68A88
P 21500 29950
AR Path="/384BB68A88" Ref="U?"  Part="1" 
AR Path="/4BB68A88" Ref="U35"  Part="3" 
AR Path="/FFFFFFFF4BB68A88" Ref="U35"  Part="1" 
AR Path="/755D912A4BB68A88" Ref="U?"  Part="1" 
AR Path="/7E428DAC4BB68A88" Ref="U?"  Part="1" 
AR Path="/2BBF084BB68A88" Ref="U?"  Part="1" 
AR Path="/44408324BB68A88" Ref="U"  Part="3" 
AR Path="/23D83C4BB68A88" Ref="U35"  Part="3" 
AR Path="/47907FA4BB68A88" Ref="U35"  Part="3" 
AR Path="/23C34C4BB68A88" Ref="U35"  Part="3" 
AR Path="/14BB68A88" Ref="U35"  Part="3" 
AR Path="/23BC884BB68A88" Ref="U35"  Part="3" 
AR Path="/773F8EB44BB68A88" Ref="U35"  Part="3" 
AR Path="/94BB68A88" Ref="U"  Part="3" 
AR Path="/23C9F04BB68A88" Ref="U35"  Part="3" 
AR Path="/FFFFFFF04BB68A88" Ref="U35"  Part="3" 
AR Path="/7E4188DA4BB68A88" Ref="U35"  Part="3" 
AR Path="/8D384BB68A88" Ref="U35"  Part="3" 
AR Path="/24BB68A88" Ref="U35"  Part="3" 
AR Path="/773F65F14BB68A88" Ref="U35"  Part="3" 
AR Path="/23C6504BB68A88" Ref="U35"  Part="3" 
AR Path="/D1C1E84BB68A88" Ref="U35"  Part="3" 
AR Path="/D058E04BB68A88" Ref="U35"  Part="3" 
AR Path="/3C64BB68A88" Ref="U35"  Part="3" 
AR Path="/23CBC44BB68A88" Ref="U35"  Part="3" 
AR Path="/2F4A4BB68A88" Ref="U35"  Part="3" 
AR Path="/23D9004BB68A88" Ref="U35"  Part="3" 
AR Path="/23CE584BB68A88" Ref="U35"  Part="3" 
AR Path="/64BB68A88" Ref="U35"  Part="3" 
AR Path="/6FE934344BB68A88" Ref="U35"  Part="3" 
AR Path="/6FE934E34BB68A88" Ref="U35"  Part="3" 
AR Path="/2824BB68A88" Ref="U35"  Part="3" 
AR Path="/69549BC04BB68A88" Ref="U35"  Part="3" 
AR Path="/D1C3384BB68A88" Ref="U35"  Part="3" 
F 0 "U35" H 21500 30000 60  0000 C CNN
F 1 "74LS32" H 21500 29900 60  0000 C CNN
F 2 "14dip300" H 21500 30000 60  0001 C CNN
F 3 "" H 21500 29950 60  0001 C CNN
	3    21500 29950
	1    0    0    -1  
$EndComp
$Comp
L 74LS32 U?
U 2 1 4BB68A82
P 20300 29850
AR Path="/384BB68A82" Ref="U?"  Part="1" 
AR Path="/4BB68A82" Ref="U35"  Part="2" 
AR Path="/FFFFFFFF4BB68A82" Ref="U35"  Part="1" 
AR Path="/755D912A4BB68A82" Ref="U?"  Part="1" 
AR Path="/7E428DAC4BB68A82" Ref="U?"  Part="1" 
AR Path="/2C24A84BB68A82" Ref="U?"  Part="1" 
AR Path="/62008744BB68A82" Ref="U"  Part="2" 
AR Path="/23D83C4BB68A82" Ref="U35"  Part="2" 
AR Path="/47907FA4BB68A82" Ref="U35"  Part="2" 
AR Path="/23C34C4BB68A82" Ref="U35"  Part="2" 
AR Path="/14BB68A82" Ref="U35"  Part="2" 
AR Path="/23BC884BB68A82" Ref="U35"  Part="2" 
AR Path="/773F8EB44BB68A82" Ref="U35"  Part="2" 
AR Path="/94BB68A82" Ref="U"  Part="2" 
AR Path="/23C9F04BB68A82" Ref="U35"  Part="2" 
AR Path="/FFFFFFF04BB68A82" Ref="U35"  Part="2" 
AR Path="/7E4188DA4BB68A82" Ref="U35"  Part="2" 
AR Path="/8D384BB68A82" Ref="U35"  Part="2" 
AR Path="/773F65F14BB68A82" Ref="U35"  Part="2" 
AR Path="/24BB68A82" Ref="U35"  Part="2" 
AR Path="/23C6504BB68A82" Ref="U35"  Part="2" 
AR Path="/D1C1E84BB68A82" Ref="U35"  Part="2" 
AR Path="/D058E04BB68A82" Ref="U35"  Part="2" 
AR Path="/3C64BB68A82" Ref="U35"  Part="2" 
AR Path="/23CBC44BB68A82" Ref="U35"  Part="2" 
AR Path="/2F4A4BB68A82" Ref="U35"  Part="2" 
AR Path="/23D9004BB68A82" Ref="U35"  Part="2" 
AR Path="/23CE584BB68A82" Ref="U35"  Part="2" 
AR Path="/64BB68A82" Ref="U35"  Part="2" 
AR Path="/6FE934344BB68A82" Ref="U35"  Part="2" 
AR Path="/6FE934E34BB68A82" Ref="U35"  Part="2" 
AR Path="/2824BB68A82" Ref="U35"  Part="2" 
AR Path="/69549BC04BB68A82" Ref="U35"  Part="2" 
AR Path="/D1C3384BB68A82" Ref="U35"  Part="2" 
F 0 "U35" H 20300 29900 60  0000 C CNN
F 1 "74LS32" H 20300 29800 60  0000 C CNN
F 2 "14dip300" H 20300 29900 60  0001 C CNN
F 3 "" H 20300 29850 60  0001 C CNN
	2    20300 29850
	1    0    0    -1  
$EndComp
$Comp
L 74LS32 U?
U 1 1 4BB68A67
P 7850 1600
AR Path="/A200384BB68A67" Ref="U?"  Part="1" 
AR Path="/4BB68A67" Ref="U35"  Part="1" 
AR Path="/FFFFFFFF4BB68A67" Ref="U35"  Part="1" 
AR Path="/755D912A4BB68A67" Ref="U?"  Part="1" 
AR Path="/7E428DAC4BB68A67" Ref="U?"  Part="1" 
AR Path="/2BF5504BB68A67" Ref="U?"  Part="1" 
AR Path="/49B08104BB68A67" Ref="U"  Part="1" 
AR Path="/23D83C4BB68A67" Ref="U35"  Part="1" 
AR Path="/47907FA4BB68A67" Ref="U35"  Part="1" 
AR Path="/23C34C4BB68A67" Ref="U35"  Part="1" 
AR Path="/14BB68A67" Ref="U35"  Part="1" 
AR Path="/23BC884BB68A67" Ref="U35"  Part="1" 
AR Path="/773F8EB44BB68A67" Ref="U35"  Part="1" 
AR Path="/94BB68A67" Ref="U"  Part="1" 
AR Path="/23C9F04BB68A67" Ref="U35"  Part="1" 
AR Path="/FFFFFFF04BB68A67" Ref="U35"  Part="1" 
AR Path="/7E4188DA4BB68A67" Ref="U35"  Part="1" 
AR Path="/8D384BB68A67" Ref="U35"  Part="1" 
AR Path="/773F65F14BB68A67" Ref="U35"  Part="1" 
AR Path="/24BB68A67" Ref="U35"  Part="1" 
AR Path="/23C6504BB68A67" Ref="U35"  Part="1" 
AR Path="/D1C1E84BB68A67" Ref="U35"  Part="1" 
AR Path="/D058E04BB68A67" Ref="U35"  Part="1" 
AR Path="/3C64BB68A67" Ref="U35"  Part="1" 
AR Path="/23CBC44BB68A67" Ref="U35"  Part="1" 
AR Path="/2F4A4BB68A67" Ref="U35"  Part="1" 
AR Path="/23D9004BB68A67" Ref="U35"  Part="1" 
AR Path="/23CE584BB68A67" Ref="U35"  Part="1" 
AR Path="/64BB68A67" Ref="U35"  Part="1" 
AR Path="/6FE934344BB68A67" Ref="U35"  Part="1" 
AR Path="/6FE934E34BB68A67" Ref="U35"  Part="1" 
AR Path="/2824BB68A67" Ref="U35"  Part="1" 
AR Path="/69549BC04BB68A67" Ref="U35"  Part="1" 
AR Path="/D1C3384BB68A67" Ref="U35"  Part="1" 
F 0 "U35" H 7850 1650 60  0000 C CNN
F 1 "74LS32" H 7850 1550 60  0000 C CNN
F 2 "14dip300" H 7850 1650 60  0001 C CNN
F 3 "" H 7850 1600 60  0001 C CNN
	1    7850 1600
	1    0    0    -1  
$EndComp
$Comp
L 74LS00 U?
U 4 1 4BB68997
P 19050 27450
AR Path="/384BB68997" Ref="U?"  Part="1" 
AR Path="/4BB68997" Ref="U31"  Part="4" 
AR Path="/FFFFFFFF4BB68997" Ref="U31"  Part="1" 
AR Path="/755D912A4BB68997" Ref="U?"  Part="1" 
AR Path="/7E428DAC4BB68997" Ref="U?"  Part="1" 
AR Path="/2B95204BB68997" Ref="U?"  Part="1" 
AR Path="/49708104BB68997" Ref="U"  Part="4" 
AR Path="/23D83C4BB68997" Ref="U31"  Part="4" 
AR Path="/47907FA4BB68997" Ref="U31"  Part="4" 
AR Path="/23C34C4BB68997" Ref="U31"  Part="4" 
AR Path="/14BB68997" Ref="U31"  Part="4" 
AR Path="/23BC884BB68997" Ref="U31"  Part="4" 
AR Path="/773F8EB44BB68997" Ref="U31"  Part="4" 
AR Path="/94BB68997" Ref="U"  Part="4" 
AR Path="/23C9F04BB68997" Ref="U31"  Part="4" 
AR Path="/FFFFFFF04BB68997" Ref="U31"  Part="4" 
AR Path="/7E4188DA4BB68997" Ref="U31"  Part="4" 
AR Path="/8D384BB68997" Ref="U31"  Part="4" 
AR Path="/24BB68997" Ref="U31"  Part="4" 
AR Path="/773F65F14BB68997" Ref="U31"  Part="4" 
AR Path="/23C6504BB68997" Ref="U31"  Part="4" 
AR Path="/D1C1E84BB68997" Ref="U31"  Part="4" 
AR Path="/D058E04BB68997" Ref="U31"  Part="4" 
AR Path="/3C64BB68997" Ref="U31"  Part="4" 
AR Path="/23CBC44BB68997" Ref="U31"  Part="4" 
AR Path="/2F4A4BB68997" Ref="U31"  Part="4" 
AR Path="/23D9004BB68997" Ref="U31"  Part="4" 
AR Path="/23CE584BB68997" Ref="U31"  Part="4" 
AR Path="/64BB68997" Ref="U31"  Part="4" 
AR Path="/6FE934344BB68997" Ref="U31"  Part="4" 
AR Path="/6FE934E34BB68997" Ref="U31"  Part="4" 
AR Path="/2824BB68997" Ref="U31"  Part="4" 
AR Path="/69549BC04BB68997" Ref="U31"  Part="4" 
AR Path="/D1C3384BB68997" Ref="U31"  Part="4" 
F 0 "U31" H 19050 27500 60  0000 C CNN
F 1 "74LS00" H 19050 27400 60  0000 C CNN
F 2 "14dip300" H 19050 27500 60  0001 C CNN
F 3 "" H 19050 27450 60  0001 C CNN
	4    19050 27450
	1    0    0    -1  
$EndComp
$Comp
L 74LS00 U?
U 3 1 4BB68992
P 16500 27450
AR Path="/384BB68992" Ref="U?"  Part="1" 
AR Path="/4BB68992" Ref="U31"  Part="3" 
AR Path="/FFFFFFFF4BB68992" Ref="U31"  Part="1" 
AR Path="/755D912A4BB68992" Ref="U?"  Part="1" 
AR Path="/7E428DAC4BB68992" Ref="U?"  Part="1" 
AR Path="/2BB4384BB68992" Ref="U?"  Part="1" 
AR Path="/61C08744BB68992" Ref="U"  Part="3" 
AR Path="/23D83C4BB68992" Ref="U31"  Part="3" 
AR Path="/47907FA4BB68992" Ref="U31"  Part="3" 
AR Path="/23C34C4BB68992" Ref="U31"  Part="3" 
AR Path="/14BB68992" Ref="U31"  Part="3" 
AR Path="/23BC884BB68992" Ref="U31"  Part="3" 
AR Path="/773F8EB44BB68992" Ref="U31"  Part="3" 
AR Path="/94BB68992" Ref="U"  Part="3" 
AR Path="/23C9F04BB68992" Ref="U31"  Part="3" 
AR Path="/FFFFFFF04BB68992" Ref="U31"  Part="3" 
AR Path="/7E4188DA4BB68992" Ref="U31"  Part="3" 
AR Path="/8D384BB68992" Ref="U31"  Part="3" 
AR Path="/773F65F14BB68992" Ref="U31"  Part="3" 
AR Path="/24BB68992" Ref="U31"  Part="3" 
AR Path="/23C6504BB68992" Ref="U31"  Part="3" 
AR Path="/D1C1E84BB68992" Ref="U31"  Part="3" 
AR Path="/D058E04BB68992" Ref="U31"  Part="3" 
AR Path="/3C64BB68992" Ref="U31"  Part="3" 
AR Path="/23CBC44BB68992" Ref="U31"  Part="3" 
AR Path="/2F4A4BB68992" Ref="U31"  Part="3" 
AR Path="/23D9004BB68992" Ref="U31"  Part="3" 
AR Path="/23CE584BB68992" Ref="U31"  Part="3" 
AR Path="/64BB68992" Ref="U31"  Part="3" 
AR Path="/6FE934344BB68992" Ref="U31"  Part="3" 
AR Path="/6FE934E34BB68992" Ref="U31"  Part="3" 
AR Path="/2824BB68992" Ref="U31"  Part="3" 
AR Path="/69549BC04BB68992" Ref="U31"  Part="3" 
AR Path="/D1C3384BB68992" Ref="U31"  Part="3" 
F 0 "U31" H 16500 27500 60  0000 C CNN
F 1 "74LS00" H 16500 27400 60  0000 C CNN
F 2 "14dip300" H 16500 27500 60  0001 C CNN
F 3 "" H 16500 27450 60  0001 C CNN
	3    16500 27450
	1    0    0    -1  
$EndComp
$Comp
L 74LS00 U?
U 2 1 4BB68971
P 14300 27450
AR Path="/384BB68971" Ref="U?"  Part="1" 
AR Path="/4BB68971" Ref="U31"  Part="2" 
AR Path="/FFFFFFFF4BB68971" Ref="U31"  Part="1" 
AR Path="/755D912A4BB68971" Ref="U?"  Part="1" 
AR Path="/7E428DAC4BB68971" Ref="U?"  Part="1" 
AR Path="/2BB8A84BB68971" Ref="U?"  Part="1" 
AR Path="/44008324BB68971" Ref="U"  Part="2" 
AR Path="/23D83C4BB68971" Ref="U31"  Part="2" 
AR Path="/47907FA4BB68971" Ref="U31"  Part="2" 
AR Path="/23C34C4BB68971" Ref="U31"  Part="2" 
AR Path="/14BB68971" Ref="U31"  Part="2" 
AR Path="/23BC884BB68971" Ref="U31"  Part="2" 
AR Path="/773F8EB44BB68971" Ref="U31"  Part="2" 
AR Path="/94BB68971" Ref="U"  Part="2" 
AR Path="/23C9F04BB68971" Ref="U31"  Part="2" 
AR Path="/FFFFFFF04BB68971" Ref="U31"  Part="2" 
AR Path="/7E4188DA4BB68971" Ref="U31"  Part="2" 
AR Path="/8D384BB68971" Ref="U31"  Part="2" 
AR Path="/773F65F14BB68971" Ref="U31"  Part="2" 
AR Path="/24BB68971" Ref="U31"  Part="2" 
AR Path="/23C6504BB68971" Ref="U31"  Part="2" 
AR Path="/D1C1E84BB68971" Ref="U31"  Part="2" 
AR Path="/D058E04BB68971" Ref="U31"  Part="2" 
AR Path="/3C64BB68971" Ref="U31"  Part="2" 
AR Path="/23CBC44BB68971" Ref="U31"  Part="2" 
AR Path="/2F4A4BB68971" Ref="U31"  Part="2" 
AR Path="/23D9004BB68971" Ref="U31"  Part="2" 
AR Path="/23CE584BB68971" Ref="U31"  Part="2" 
AR Path="/64BB68971" Ref="U31"  Part="2" 
AR Path="/6FE934344BB68971" Ref="U31"  Part="2" 
AR Path="/6FE934E34BB68971" Ref="U31"  Part="2" 
AR Path="/2824BB68971" Ref="U31"  Part="2" 
AR Path="/69549BC04BB68971" Ref="U31"  Part="2" 
AR Path="/D1C3384BB68971" Ref="U31"  Part="2" 
F 0 "U31" H 14300 27500 60  0000 C CNN
F 1 "74LS00" H 14300 27400 60  0000 C CNN
F 2 "14dip300" H 14300 27500 60  0001 C CNN
F 3 "" H 14300 27450 60  0001 C CNN
	2    14300 27450
	1    0    0    -1  
$EndComp
$Comp
L 74LS00 U?
U 1 1 4BB684A5
P 9600 20850
AR Path="/2300384BB684A5" Ref="U?"  Part="1" 
AR Path="/4BB684A5" Ref="U31"  Part="1" 
AR Path="/FFFFFFFF4BB684A5" Ref="U31"  Part="1" 
AR Path="/755D912A4BB684A5" Ref="U?"  Part="1" 
AR Path="/7E428DAC4BB684A5" Ref="U?"  Part="1" 
AR Path="/2B6E684BB684A5" Ref="U?"  Part="1" 
AR Path="/45208044BB684A5" Ref="U"  Part="1" 
AR Path="/23D83C4BB684A5" Ref="U31"  Part="1" 
AR Path="/47907FA4BB684A5" Ref="U31"  Part="1" 
AR Path="/23C34C4BB684A5" Ref="U31"  Part="1" 
AR Path="/14BB684A5" Ref="U31"  Part="1" 
AR Path="/23BC884BB684A5" Ref="U31"  Part="1" 
AR Path="/773F8EB44BB684A5" Ref="U31"  Part="1" 
AR Path="/94BB684A5" Ref="U"  Part="1" 
AR Path="/23C9F04BB684A5" Ref="U31"  Part="1" 
AR Path="/FFFFFFF04BB684A5" Ref="U31"  Part="1" 
AR Path="/7E4188DA4BB684A5" Ref="U31"  Part="1" 
AR Path="/8D384BB684A5" Ref="U31"  Part="1" 
AR Path="/773F65F14BB684A5" Ref="U31"  Part="1" 
AR Path="/24BB684A5" Ref="U31"  Part="1" 
AR Path="/23C6504BB684A5" Ref="U31"  Part="1" 
AR Path="/D1C1E84BB684A5" Ref="U31"  Part="1" 
AR Path="/D058E04BB684A5" Ref="U31"  Part="1" 
AR Path="/3C64BB684A5" Ref="U31"  Part="1" 
AR Path="/23CBC44BB684A5" Ref="U31"  Part="1" 
AR Path="/2F4A4BB684A5" Ref="U31"  Part="1" 
AR Path="/23D9004BB684A5" Ref="U31"  Part="1" 
AR Path="/23CE584BB684A5" Ref="U31"  Part="1" 
AR Path="/64BB684A5" Ref="U31"  Part="1" 
AR Path="/6FE934344BB684A5" Ref="U31"  Part="1" 
AR Path="/6FE934E34BB684A5" Ref="U31"  Part="1" 
AR Path="/2824BB684A5" Ref="U31"  Part="1" 
AR Path="/69549BC04BB684A5" Ref="U31"  Part="1" 
AR Path="/D1C3384BB684A5" Ref="U31"  Part="1" 
F 0 "U31" H 9600 20900 60  0000 C CNN
F 1 "74LS00" H 9600 20800 60  0000 C CNN
F 2 "14dip300" H 9600 20900 60  0001 C CNN
F 3 "" H 9600 20850 60  0001 C CNN
	1    9600 20850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR024
U 1 1 4BB683E2
P 18100 25700
AR Path="/4BB683E2" Ref="#PWR024"  Part="1" 
AR Path="/FFFFFFFF4BB683E2" Ref="#PWR027"  Part="1" 
AR Path="/755D912A4BB683E2" Ref="#PWR?"  Part="1" 
AR Path="/23D83C4BB683E2" Ref="#PWR47"  Part="1" 
AR Path="/47907FA4BB683E2" Ref="#PWR020"  Part="1" 
AR Path="/23C34C4BB683E2" Ref="#PWR026"  Part="1" 
AR Path="/14BB683E2" Ref="#PWR024"  Part="1" 
AR Path="/23BC884BB683E2" Ref="#PWR027"  Part="1" 
AR Path="/773F8EB44BB683E2" Ref="#PWR024"  Part="1" 
AR Path="/94BB683E2" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB683E2" Ref="#PWR38"  Part="1" 
AR Path="/FFFFFFF04BB683E2" Ref="#PWR50"  Part="1" 
AR Path="/7E4188DA4BB683E2" Ref="#PWR47"  Part="1" 
AR Path="/8D384BB683E2" Ref="#PWR020"  Part="1" 
AR Path="/773F65F14BB683E2" Ref="#PWR027"  Part="1" 
AR Path="/6FF0DD404BB683E2" Ref="#PWR027"  Part="1" 
AR Path="/24BB683E2" Ref="#PWR38"  Part="1" 
AR Path="/23C6504BB683E2" Ref="#PWR021"  Part="1" 
AR Path="/D1C1E84BB683E2" Ref="#PWR021"  Part="1" 
AR Path="/D058E04BB683E2" Ref="#PWR027"  Part="1" 
AR Path="/6684D64BB683E2" Ref="#PWR024"  Part="1" 
AR Path="/3C64BB683E2" Ref="#PWR021"  Part="1" 
AR Path="/23CBC44BB683E2" Ref="#PWR024"  Part="1" 
AR Path="/2F4A4BB683E2" Ref="#PWR021"  Part="1" 
AR Path="/23D9004BB683E2" Ref="#PWR024"  Part="1" 
AR Path="/23CE584BB683E2" Ref="#PWR023"  Part="1" 
AR Path="/64BB683E2" Ref="#PWR49"  Part="1" 
AR Path="/6FE934344BB683E2" Ref="#PWR49"  Part="1" 
AR Path="/6FE934E34BB683E2" Ref="#PWR024"  Part="1" 
AR Path="/2824BB683E2" Ref="#PWR024"  Part="1" 
AR Path="/69549BC04BB683E2" Ref="#PWR024"  Part="1" 
AR Path="/D1C3384BB683E2" Ref="#PWR027"  Part="1" 
F 0 "#PWR024" H 18100 25700 30  0001 C CNN
F 1 "GND" H 18100 25630 30  0001 C CNN
F 2 "" H 18100 25700 60  0001 C CNN
F 3 "" H 18100 25700 60  0001 C CNN
	1    18100 25700
	1    0    0    -1  
$EndComp
$Comp
L C C45
U 1 1 4BB683E1
P 18100 25500
AR Path="/4BB683E1" Ref="C45"  Part="1" 
AR Path="/FFFFFFFF4BB683E1" Ref="C45"  Part="1" 
AR Path="/755D912A4BB683E1" Ref="C?"  Part="1" 
AR Path="/23D83C4BB683E1" Ref="C45"  Part="1" 
AR Path="/47907FA4BB683E1" Ref="C?"  Part="1" 
AR Path="/23C34C4BB683E1" Ref="C45"  Part="1" 
AR Path="/14BB683E1" Ref="C45"  Part="1" 
AR Path="/23BC884BB683E1" Ref="C45"  Part="1" 
AR Path="/773F8EB44BB683E1" Ref="C45"  Part="1" 
AR Path="/94BB683E1" Ref="C"  Part="1" 
AR Path="/23C9F04BB683E1" Ref="C45"  Part="1" 
AR Path="/FFFFFFF04BB683E1" Ref="C45"  Part="1" 
AR Path="/7E4188DA4BB683E1" Ref="C45"  Part="1" 
AR Path="/8D384BB683E1" Ref="C45"  Part="1" 
AR Path="/24BB683E1" Ref="C45"  Part="1" 
AR Path="/773F65F14BB683E1" Ref="C45"  Part="1" 
AR Path="/23C6504BB683E1" Ref="C45"  Part="1" 
AR Path="/D1C1E84BB683E1" Ref="C45"  Part="1" 
AR Path="/D058E04BB683E1" Ref="C45"  Part="1" 
AR Path="/3C64BB683E1" Ref="C45"  Part="1" 
AR Path="/23CBC44BB683E1" Ref="C45"  Part="1" 
AR Path="/2F4A4BB683E1" Ref="C45"  Part="1" 
AR Path="/23D9004BB683E1" Ref="C45"  Part="1" 
AR Path="/23CE584BB683E1" Ref="C45"  Part="1" 
AR Path="/64BB683E1" Ref="C45"  Part="1" 
AR Path="/6FE934344BB683E1" Ref="C45"  Part="1" 
AR Path="/6FE934E34BB683E1" Ref="C45"  Part="1" 
AR Path="/2824BB683E1" Ref="C45"  Part="1" 
AR Path="/69549BC04BB683E1" Ref="C45"  Part="1" 
AR Path="/D1C3384BB683E1" Ref="C45"  Part="1" 
F 0 "C45" H 18150 25600 50  0000 L CNN
F 1 "0.1 uF" H 18150 25400 50  0000 L CNN
F 2 "C2" H 18150 25500 50  0001 C CNN
F 3 "" H 18100 25500 60  0001 C CNN
	1    18100 25500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR025
U 1 1 4BB683E0
P 17200 26050
AR Path="/4BB683E0" Ref="#PWR025"  Part="1" 
AR Path="/FFFFFFFF4BB683E0" Ref="#PWR028"  Part="1" 
AR Path="/755D912A4BB683E0" Ref="#PWR?"  Part="1" 
AR Path="/23D83C4BB683E0" Ref="#PWR45"  Part="1" 
AR Path="/47907FA4BB683E0" Ref="#PWR021"  Part="1" 
AR Path="/23C34C4BB683E0" Ref="#PWR027"  Part="1" 
AR Path="/14BB683E0" Ref="#PWR025"  Part="1" 
AR Path="/23BC884BB683E0" Ref="#PWR028"  Part="1" 
AR Path="/773F8EB44BB683E0" Ref="#PWR025"  Part="1" 
AR Path="/94BB683E0" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB683E0" Ref="#PWR36"  Part="1" 
AR Path="/FFFFFFF04BB683E0" Ref="#PWR47"  Part="1" 
AR Path="/7E4188DA4BB683E0" Ref="#PWR45"  Part="1" 
AR Path="/8D384BB683E0" Ref="#PWR021"  Part="1" 
AR Path="/773F65F14BB683E0" Ref="#PWR028"  Part="1" 
AR Path="/6FF0DD404BB683E0" Ref="#PWR028"  Part="1" 
AR Path="/24BB683E0" Ref="#PWR36"  Part="1" 
AR Path="/23C6504BB683E0" Ref="#PWR022"  Part="1" 
AR Path="/D1C1E84BB683E0" Ref="#PWR022"  Part="1" 
AR Path="/D058E04BB683E0" Ref="#PWR028"  Part="1" 
AR Path="/6684D64BB683E0" Ref="#PWR025"  Part="1" 
AR Path="/3C64BB683E0" Ref="#PWR022"  Part="1" 
AR Path="/23CBC44BB683E0" Ref="#PWR025"  Part="1" 
AR Path="/2F4A4BB683E0" Ref="#PWR022"  Part="1" 
AR Path="/23D9004BB683E0" Ref="#PWR025"  Part="1" 
AR Path="/23CE584BB683E0" Ref="#PWR024"  Part="1" 
AR Path="/64BB683E0" Ref="#PWR46"  Part="1" 
AR Path="/6FE934344BB683E0" Ref="#PWR46"  Part="1" 
AR Path="/6FE934E34BB683E0" Ref="#PWR025"  Part="1" 
AR Path="/2824BB683E0" Ref="#PWR025"  Part="1" 
AR Path="/69549BC04BB683E0" Ref="#PWR025"  Part="1" 
AR Path="/D1C3384BB683E0" Ref="#PWR028"  Part="1" 
F 0 "#PWR025" H 17200 26050 30  0001 C CNN
F 1 "GND" H 17200 25980 30  0001 C CNN
F 2 "" H 17200 26050 60  0001 C CNN
F 3 "" H 17200 26050 60  0001 C CNN
	1    17200 26050
	1    0    0    -1  
$EndComp
NoConn ~ 17900 25500
NoConn ~ 17900 25400
$Comp
L VCC #PWR026
U 1 1 4BB683DF
P 17900 25300
AR Path="/4BB683DF" Ref="#PWR026"  Part="1" 
AR Path="/FFFFFFFF4BB683DF" Ref="#PWR029"  Part="1" 
AR Path="/755D912A4BB683DF" Ref="#PWR?"  Part="1" 
AR Path="/23D83C4BB683DF" Ref="#PWR46"  Part="1" 
AR Path="/47907FA4BB683DF" Ref="#PWR022"  Part="1" 
AR Path="/23C34C4BB683DF" Ref="#PWR028"  Part="1" 
AR Path="/14BB683DF" Ref="#PWR026"  Part="1" 
AR Path="/23BC884BB683DF" Ref="#PWR029"  Part="1" 
AR Path="/773F8EB44BB683DF" Ref="#PWR026"  Part="1" 
AR Path="/94BB683DF" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB683DF" Ref="#PWR37"  Part="1" 
AR Path="/FFFFFFF04BB683DF" Ref="#PWR48"  Part="1" 
AR Path="/7E4188DA4BB683DF" Ref="#PWR46"  Part="1" 
AR Path="/8D384BB683DF" Ref="#PWR022"  Part="1" 
AR Path="/773F65F14BB683DF" Ref="#PWR029"  Part="1" 
AR Path="/6FF0DD404BB683DF" Ref="#PWR029"  Part="1" 
AR Path="/24BB683DF" Ref="#PWR37"  Part="1" 
AR Path="/23C6504BB683DF" Ref="#PWR023"  Part="1" 
AR Path="/D1C1E84BB683DF" Ref="#PWR023"  Part="1" 
AR Path="/D058E04BB683DF" Ref="#PWR029"  Part="1" 
AR Path="/6684D64BB683DF" Ref="#PWR026"  Part="1" 
AR Path="/3C64BB683DF" Ref="#PWR023"  Part="1" 
AR Path="/23CBC44BB683DF" Ref="#PWR026"  Part="1" 
AR Path="/2F4A4BB683DF" Ref="#PWR023"  Part="1" 
AR Path="/23D9004BB683DF" Ref="#PWR026"  Part="1" 
AR Path="/23CE584BB683DF" Ref="#PWR025"  Part="1" 
AR Path="/64BB683DF" Ref="#PWR47"  Part="1" 
AR Path="/6FE934344BB683DF" Ref="#PWR47"  Part="1" 
AR Path="/6FE934E34BB683DF" Ref="#PWR026"  Part="1" 
AR Path="/2824BB683DF" Ref="#PWR026"  Part="1" 
AR Path="/69549BC04BB683DF" Ref="#PWR026"  Part="1" 
AR Path="/D1C3384BB683DF" Ref="#PWR029"  Part="1" 
F 0 "#PWR026" H 17900 25400 30  0001 C CNN
F 1 "VCC" H 17900 25400 30  0000 C CNN
F 2 "" H 17900 25300 60  0001 C CNN
F 3 "" H 17900 25300 60  0001 C CNN
	1    17900 25300
	1    0    0    -1  
$EndComp
NoConn ~ 17200 25300
NoConn ~ 17200 25400
NoConn ~ 17200 25500
$Comp
L DIL14 U42
U 1 1 4BB683DE
P 17550 25600
AR Path="/4BB683DE" Ref="U42"  Part="1" 
AR Path="/FFFFFFFF4BB683DE" Ref="U42"  Part="1" 
AR Path="/755D912A4BB683DE" Ref="P?"  Part="1" 
AR Path="/23D83C4BB683DE" Ref="U47"  Part="1" 
AR Path="/47907FA4BB683DE" Ref="P?"  Part="1" 
AR Path="/23C34C4BB683DE" Ref="U42"  Part="1" 
AR Path="/14BB683DE" Ref="U42"  Part="1" 
AR Path="/23BC884BB683DE" Ref="U42"  Part="1" 
AR Path="/31324BB683DE" Ref="P?"  Part="1" 
AR Path="/2606084BB683DE" Ref="U47"  Part="1" 
AR Path="/773F8EB44BB683DE" Ref="U42"  Part="1" 
AR Path="/94BB683DE" Ref="U"  Part="1" 
AR Path="/23C9F04BB683DE" Ref="U42"  Part="1" 
AR Path="/FFFFFFF04BB683DE" Ref="U42"  Part="1" 
AR Path="/7E4188DA4BB683DE" Ref="U47"  Part="1" 
AR Path="/8D384BB683DE" Ref="U47"  Part="1" 
AR Path="/24BB683DE" Ref="U42"  Part="1" 
AR Path="/773F65F14BB683DE" Ref="U42"  Part="1" 
AR Path="/23C6504BB683DE" Ref="U42"  Part="1" 
AR Path="/D1C1E84BB683DE" Ref="U47"  Part="1" 
AR Path="/D058E04BB683DE" Ref="U42"  Part="1" 
AR Path="/3C64BB683DE" Ref="U47"  Part="1" 
AR Path="/23CBC44BB683DE" Ref="U42"  Part="1" 
AR Path="/2F4A4BB683DE" Ref="U47"  Part="1" 
AR Path="/23D9004BB683DE" Ref="U42"  Part="1" 
AR Path="/23CE584BB683DE" Ref="U47"  Part="1" 
AR Path="/7E42B4154BB683DE" Ref="U42"  Part="1" 
AR Path="/64BB683DE" Ref="U42"  Part="1" 
AR Path="/6FE934344BB683DE" Ref="U42"  Part="1" 
AR Path="/6FE934E34BB683DE" Ref="U42"  Part="1" 
AR Path="/2824BB683DE" Ref="U42"  Part="1" 
AR Path="/69549BC04BB683DE" Ref="U42"  Part="1" 
AR Path="/D1C3384BB683DE" Ref="U42"  Part="1" 
F 0 "U42" H 17550 26000 60  0000 C CNN
F 1 "2.0000 MHz" V 17550 25600 50  0000 C CNN
F 2 "14dip300" V 17650 25600 50  0001 C CNN
F 3 "" H 17550 25600 60  0001 C CNN
	1    17550 25600
	1    0    0    -1  
$EndComp
Text Notes 17200 25050 0    60   ~ 0
2 MHz TTL OSCILLATOR
Text Label 20550 15500 0    60   ~ 0
sXTRQ*
Text Label 20550 15400 0    60   ~ 0
TMA0*
Text Label 20550 15300 0    60   ~ 0
TMA1*
Text Label 20550 15200 0    60   ~ 0
TMA2*
Text Label 20550 15100 0    60   ~ 0
TMA3*
Text Label 20850 16200 0    60   ~ 0
HOLD*
Text Label 20850 16050 0    60   ~ 0
RDY
Text Label 20850 15900 0    60   ~ 0
XRDY
$Comp
L 74LS05 U?
U 4 1 4BB68153
P 20300 17800
AR Path="/23D6704BB68153" Ref="U?"  Part="1" 
AR Path="/FFFFFFFF4BB68153" Ref="U37"  Part="4" 
AR Path="/4BB68153" Ref="U37"  Part="4" 
AR Path="/755D912A4BB68153" Ref="U?"  Part="4" 
AR Path="/7E0031324BB68153" Ref="U?"  Part="4" 
AR Path="/2606084BB68153" Ref="U37"  Part="4" 
AR Path="/23D83C4BB68153" Ref="U37"  Part="4" 
AR Path="/47907FA4BB68153" Ref="U37"  Part="4" 
AR Path="/23C34C4BB68153" Ref="U37"  Part="4" 
AR Path="/14BB68153" Ref="U37"  Part="4" 
AR Path="/23BC884BB68153" Ref="U37"  Part="4" 
AR Path="/773F8EB44BB68153" Ref="U37"  Part="4" 
AR Path="/94BB68153" Ref="U"  Part="4" 
AR Path="/23C9F04BB68153" Ref="U37"  Part="4" 
AR Path="/FFFFFFF04BB68153" Ref="U37"  Part="4" 
AR Path="/7E4188DA4BB68153" Ref="U37"  Part="4" 
AR Path="/8D384BB68153" Ref="U37"  Part="4" 
AR Path="/24BB68153" Ref="U37"  Part="4" 
AR Path="/773F65F14BB68153" Ref="U37"  Part="4" 
AR Path="/23C6504BB68153" Ref="U37"  Part="4" 
AR Path="/D1C1E84BB68153" Ref="U37"  Part="4" 
AR Path="/D058E04BB68153" Ref="U37"  Part="4" 
AR Path="/3C64BB68153" Ref="U37"  Part="4" 
AR Path="/23CBC44BB68153" Ref="U37"  Part="4" 
AR Path="/2F4A4BB68153" Ref="U37"  Part="4" 
AR Path="/23D9004BB68153" Ref="U37"  Part="4" 
AR Path="/64BB68153" Ref="U37"  Part="4" 
AR Path="/6FE934344BB68153" Ref="U37"  Part="4" 
AR Path="/6FE934E34BB68153" Ref="U37"  Part="4" 
AR Path="/2824BB68153" Ref="U37"  Part="4" 
AR Path="/69549BC04BB68153" Ref="U37"  Part="4" 
AR Path="/D1C3384BB68153" Ref="U37"  Part="4" 
F 0 "U37" H 20495 17915 60  0000 C CNN
F 1 "74LS05" H 20490 17675 60  0000 C CNN
F 2 "14dip300" H 20490 17775 60  0001 C CNN
F 3 "" H 20300 17800 60  0001 C CNN
	4    20300 17800
	1    0    0    -1  
$EndComp
Text Label 20800 17800 0    60   ~ 0
CDSB*
Text Label 20800 20150 0    60   ~ 0
pHLDA
$Comp
L 74LS05 U?
U 5 1 4BB680F3
P 20300 20150
AR Path="/384BB680F3" Ref="U?"  Part="1" 
AR Path="/4BB680F3" Ref="U37"  Part="5" 
AR Path="/FFFFFFFF4BB680F3" Ref="U37"  Part="1" 
AR Path="/755D912A4BB680F3" Ref="U?"  Part="1" 
AR Path="/23D8044BB680F3" Ref="U?"  Part="1" 
AR Path="/2606084BB680F3" Ref="U37"  Part="1" 
AR Path="/23D83C4BB680F3" Ref="U37"  Part="1" 
AR Path="/47907FA4BB680F3" Ref="U37"  Part="1" 
AR Path="/23C34C4BB680F3" Ref="U37"  Part="1" 
AR Path="/14BB680F3" Ref="U37"  Part="1" 
AR Path="/23BC884BB680F3" Ref="U37"  Part="1" 
AR Path="/773F8EB44BB680F3" Ref="U37"  Part="1" 
AR Path="/23C9F04BB680F3" Ref="U37"  Part="1" 
AR Path="/94BB680F3" Ref="U"  Part="5" 
AR Path="/FFFFFFF04BB680F3" Ref="U37"  Part="1" 
AR Path="/7E4188DA4BB680F3" Ref="U37"  Part="1" 
AR Path="/2671AC4BB680F3" Ref="U"  Part="5" 
AR Path="/8D384BB680F3" Ref="U37"  Part="5" 
AR Path="/773F65F14BB680F3" Ref="U37"  Part="5" 
AR Path="/24BB680F3" Ref="U37"  Part="5" 
AR Path="/23C6504BB680F3" Ref="U37"  Part="5" 
AR Path="/D1C1E84BB680F3" Ref="U37"  Part="5" 
AR Path="/D058E04BB680F3" Ref="U37"  Part="5" 
AR Path="/3C64BB680F3" Ref="U37"  Part="5" 
AR Path="/23CBC44BB680F3" Ref="U37"  Part="5" 
AR Path="/2F4A4BB680F3" Ref="U37"  Part="5" 
AR Path="/23D9004BB680F3" Ref="U37"  Part="5" 
AR Path="/64BB680F3" Ref="U37"  Part="5" 
AR Path="/6FE934344BB680F3" Ref="U37"  Part="5" 
AR Path="/6FE934E34BB680F3" Ref="U37"  Part="5" 
AR Path="/2824BB680F3" Ref="U37"  Part="5" 
AR Path="/69549BC04BB680F3" Ref="U37"  Part="5" 
AR Path="/D1C3384BB680F3" Ref="U37"  Part="5" 
F 0 "U37" H 20495 20265 60  0000 C CNN
F 1 "74LS05" H 20490 20025 60  0000 C CNN
F 2 "14dip300" H 20490 20125 60  0001 C CNN
F 3 "" H 20300 20150 60  0001 C CNN
	5    20300 20150
	1    0    0    -1  
$EndComp
Text Label 20800 19250 0    60   ~ 0
SDSB*
Text Label 20800 18800 0    60   ~ 0
DODSB*
Text Label 20800 18350 0    60   ~ 0
ADSB*
$Comp
L 74LS05 U?
U 3 1 4BB6805B
P 20300 19250
AR Path="/384BB6805B" Ref="U?"  Part="1" 
AR Path="/4BB6805B" Ref="U37"  Part="3" 
AR Path="/94BB6805B" Ref="U"  Part="3" 
AR Path="/FFFFFFFF4BB6805B" Ref="U37"  Part="3" 
AR Path="/755D912A4BB6805B" Ref="U?"  Part="3" 
AR Path="/23D8044BB6805B" Ref="U?"  Part="3" 
AR Path="/2606084BB6805B" Ref="U37"  Part="3" 
AR Path="/23D83C4BB6805B" Ref="U37"  Part="3" 
AR Path="/47907FA4BB6805B" Ref="U37"  Part="3" 
AR Path="/23C34C4BB6805B" Ref="U37"  Part="3" 
AR Path="/14BB6805B" Ref="U37"  Part="3" 
AR Path="/23BC884BB6805B" Ref="U37"  Part="3" 
AR Path="/773F8EB44BB6805B" Ref="U37"  Part="3" 
AR Path="/23C9F04BB6805B" Ref="U37"  Part="3" 
AR Path="/FFFFFFF04BB6805B" Ref="U37"  Part="3" 
AR Path="/7E4188DA4BB6805B" Ref="U37"  Part="3" 
AR Path="/8D384BB6805B" Ref="U37"  Part="3" 
AR Path="/773F65F14BB6805B" Ref="U37"  Part="3" 
AR Path="/24BB6805B" Ref="U37"  Part="3" 
AR Path="/23C6504BB6805B" Ref="U37"  Part="3" 
AR Path="/D1C1E84BB6805B" Ref="U37"  Part="3" 
AR Path="/D058E04BB6805B" Ref="U37"  Part="3" 
AR Path="/3C64BB6805B" Ref="U37"  Part="3" 
AR Path="/23CBC44BB6805B" Ref="U37"  Part="3" 
AR Path="/2F4A4BB6805B" Ref="U37"  Part="3" 
AR Path="/23D9004BB6805B" Ref="U37"  Part="3" 
AR Path="/64BB6805B" Ref="U37"  Part="3" 
AR Path="/6FE934344BB6805B" Ref="U37"  Part="3" 
AR Path="/6FE934E34BB6805B" Ref="U37"  Part="3" 
AR Path="/2824BB6805B" Ref="U37"  Part="3" 
AR Path="/69549BC04BB6805B" Ref="U37"  Part="3" 
AR Path="/D1C3384BB6805B" Ref="U37"  Part="3" 
F 0 "U37" H 20495 19365 60  0000 C CNN
F 1 "74LS05" H 20490 19125 60  0000 C CNN
F 2 "14dip300" H 20490 19225 60  0001 C CNN
F 3 "" H 20300 19250 60  0001 C CNN
	3    20300 19250
	1    0    0    -1  
$EndComp
$Comp
L 74LS05 U?
U 1 1 4BB68055
P 20300 18800
AR Path="/2300384BB68055" Ref="U?"  Part="1" 
AR Path="/4BB68055" Ref="U37"  Part="1" 
AR Path="/FFFFFFFF4BB68055" Ref="U37"  Part="1" 
AR Path="/755D912A4BB68055" Ref="U?"  Part="1" 
AR Path="/23D6704BB68055" Ref="U?"  Part="1" 
AR Path="/2606084BB68055" Ref="U37"  Part="1" 
AR Path="/23D83C4BB68055" Ref="U37"  Part="1" 
AR Path="/47907FA4BB68055" Ref="U37"  Part="1" 
AR Path="/23C34C4BB68055" Ref="U37"  Part="1" 
AR Path="/14BB68055" Ref="U37"  Part="1" 
AR Path="/23BC884BB68055" Ref="U37"  Part="1" 
AR Path="/773F8EB44BB68055" Ref="U37"  Part="1" 
AR Path="/94BB68055" Ref="U"  Part="1" 
AR Path="/23C9F04BB68055" Ref="U37"  Part="1" 
AR Path="/FFFFFFF04BB68055" Ref="U37"  Part="1" 
AR Path="/7E4188DA4BB68055" Ref="U37"  Part="1" 
AR Path="/8D384BB68055" Ref="U37"  Part="1" 
AR Path="/773F65F14BB68055" Ref="U37"  Part="1" 
AR Path="/24BB68055" Ref="U37"  Part="1" 
AR Path="/23C6504BB68055" Ref="U37"  Part="1" 
AR Path="/D1C1E84BB68055" Ref="U37"  Part="1" 
AR Path="/D058E04BB68055" Ref="U37"  Part="1" 
AR Path="/3C64BB68055" Ref="U37"  Part="1" 
AR Path="/23CBC44BB68055" Ref="U37"  Part="1" 
AR Path="/2F4A4BB68055" Ref="U37"  Part="1" 
AR Path="/23D9004BB68055" Ref="U37"  Part="1" 
AR Path="/64BB68055" Ref="U37"  Part="1" 
AR Path="/6FE934344BB68055" Ref="U37"  Part="1" 
AR Path="/6FE934E34BB68055" Ref="U37"  Part="1" 
AR Path="/2824BB68055" Ref="U37"  Part="1" 
AR Path="/69549BC04BB68055" Ref="U37"  Part="1" 
AR Path="/D1C3384BB68055" Ref="U37"  Part="1" 
F 0 "U37" H 20495 18915 60  0000 C CNN
F 1 "74LS05" H 20490 18675 60  0000 C CNN
F 2 "14dip300" H 20490 18775 60  0001 C CNN
F 3 "" H 20300 18800 60  0001 C CNN
	1    20300 18800
	1    0    0    -1  
$EndComp
$Comp
L 74LS05 U?
U 2 1 4BB6804E
P 20300 18350
AR Path="/A200384BB6804E" Ref="U?"  Part="1" 
AR Path="/4BB6804E" Ref="U37"  Part="2" 
AR Path="/380031324BB6804E" Ref="U?"  Part="1" 
AR Path="/FFFFFFFF4BB6804E" Ref="U37"  Part="2" 
AR Path="/755D912A4BB6804E" Ref="U?"  Part="2" 
AR Path="/23D8044BB6804E" Ref="U?"  Part="2" 
AR Path="/7E42B4154BB6804E" Ref="U37"  Part="2" 
AR Path="/383034454BB6804E" Ref="U37"  Part="2" 
AR Path="/23D83C4BB6804E" Ref="U37"  Part="2" 
AR Path="/47907FA4BB6804E" Ref="U37"  Part="2" 
AR Path="/23C34C4BB6804E" Ref="U37"  Part="2" 
AR Path="/14BB6804E" Ref="U37"  Part="2" 
AR Path="/23BC884BB6804E" Ref="U37"  Part="2" 
AR Path="/773F8EB44BB6804E" Ref="U37"  Part="2" 
AR Path="/94BB6804E" Ref="U"  Part="2" 
AR Path="/23C9F04BB6804E" Ref="U37"  Part="2" 
AR Path="/FFFFFFF04BB6804E" Ref="U37"  Part="2" 
AR Path="/7E4188DA4BB6804E" Ref="U37"  Part="2" 
AR Path="/8D384BB6804E" Ref="U37"  Part="2" 
AR Path="/773F65F14BB6804E" Ref="U37"  Part="2" 
AR Path="/24BB6804E" Ref="U37"  Part="2" 
AR Path="/23C6504BB6804E" Ref="U37"  Part="2" 
AR Path="/D1C1E84BB6804E" Ref="U37"  Part="2" 
AR Path="/D058E04BB6804E" Ref="U37"  Part="2" 
AR Path="/3C64BB6804E" Ref="U37"  Part="2" 
AR Path="/23CBC44BB6804E" Ref="U37"  Part="2" 
AR Path="/2F4A4BB6804E" Ref="U37"  Part="2" 
AR Path="/23D9004BB6804E" Ref="U37"  Part="2" 
AR Path="/64BB6804E" Ref="U37"  Part="2" 
AR Path="/6FE934344BB6804E" Ref="U37"  Part="2" 
AR Path="/6FE934E34BB6804E" Ref="U37"  Part="2" 
AR Path="/2824BB6804E" Ref="U37"  Part="2" 
AR Path="/69549BC04BB6804E" Ref="U37"  Part="2" 
AR Path="/D1C3384BB6804E" Ref="U37"  Part="2" 
F 0 "U37" H 20495 18465 60  0000 C CNN
F 1 "74LS05" H 20490 18225 60  0000 C CNN
F 2 "14dip300" H 20490 18325 60  0001 C CNN
F 3 "" H 20300 18350 60  0001 C CNN
	2    20300 18350
	1    0    0    -1  
$EndComp
Text Label 22050 24750 0    60   ~ 0
bINT*
Text Label 22250 24500 0    60   ~ 0
MWRT
Text Label 22250 24400 0    60   ~ 0
CLOCK*
$Comp
L CONN_2X2 P?
U 1 1 4BB67EC1
P 21800 24450
AR Path="/A200384BB67EC1" Ref="P?"  Part="1" 
AR Path="/4BB67EC1" Ref="P4"  Part="1" 
AR Path="/FFFFFFFF4BB67EC1" Ref="P4"  Part="1" 
AR Path="/755D912A4BB67EC1" Ref="P?"  Part="1" 
AR Path="/2606084BB67EC1" Ref="P4"  Part="1" 
AR Path="/23D83C4BB67EC1" Ref="P4"  Part="1" 
AR Path="/47907FA4BB67EC1" Ref="P1"  Part="1" 
AR Path="/23C34C4BB67EC1" Ref="P4"  Part="1" 
AR Path="/14BB67EC1" Ref="P1"  Part="1" 
AR Path="/23BC884BB67EC1" Ref="P4"  Part="1" 
AR Path="/773F8EB44BB67EC1" Ref="P4"  Part="1" 
AR Path="/94BB67EC1" Ref="P"  Part="1" 
AR Path="/23C9F04BB67EC1" Ref="P4"  Part="1" 
AR Path="/FFFFFFF04BB67EC1" Ref="P4"  Part="1" 
AR Path="/7E4188DA4BB67EC1" Ref="P4"  Part="1" 
AR Path="/8D384BB67EC1" Ref="P4"  Part="1" 
AR Path="/24BB67EC1" Ref="P4"  Part="1" 
AR Path="/773F65F14BB67EC1" Ref="P4"  Part="1" 
AR Path="/23C6504BB67EC1" Ref="P4"  Part="1" 
AR Path="/D1C1E84BB67EC1" Ref="P4"  Part="1" 
AR Path="/D058E04BB67EC1" Ref="P4"  Part="1" 
AR Path="/3C64BB67EC1" Ref="P4"  Part="1" 
AR Path="/23CBC44BB67EC1" Ref="P4"  Part="1" 
AR Path="/2F4A4BB67EC1" Ref="P4"  Part="1" 
AR Path="/23D9004BB67EC1" Ref="P4"  Part="1" 
AR Path="/64BB67EC1" Ref="P4"  Part="1" 
AR Path="/6FE934344BB67EC1" Ref="P4"  Part="1" 
AR Path="/6FE934E34BB67EC1" Ref="P4"  Part="1" 
AR Path="/2824BB67EC1" Ref="P4"  Part="1" 
AR Path="/69549BC04BB67EC1" Ref="P4"  Part="1" 
AR Path="/D1C3384BB67EC1" Ref="P4"  Part="1" 
F 0 "P4" H 21800 24250 50  0000 C CNN
F 1 "CONN_2X2" H 21810 24320 40  0000 C CNN
F 2 "PIN_ARRAY_2X2" H 21810 24420 40  0001 C CNN
F 3 "" H 21800 24450 60  0001 C CNN
	1    21800 24450
	1    0    0    -1  
$EndComp
Text Label 22000 24300 0    60   ~ 0
PHI
$Comp
L R R37
U 1 1 4BB67E6E
P 21700 23950
AR Path="/4BB67E6E" Ref="R37"  Part="1" 
AR Path="/FFFFFFFF4BB67E6E" Ref="R37"  Part="1" 
AR Path="/755D912A4BB67E6E" Ref="R?"  Part="1" 
AR Path="/23D83C4BB67E6E" Ref="R37"  Part="1" 
AR Path="/47907FA4BB67E6E" Ref="R?"  Part="1" 
AR Path="/23C34C4BB67E6E" Ref="R37"  Part="1" 
AR Path="/14BB67E6E" Ref="R?"  Part="1" 
AR Path="/23BC884BB67E6E" Ref="R37"  Part="1" 
AR Path="/773F8EB44BB67E6E" Ref="R37"  Part="1" 
AR Path="/94BB67E6E" Ref="R"  Part="1" 
AR Path="/23C9F04BB67E6E" Ref="R37"  Part="1" 
AR Path="/FFFFFFF04BB67E6E" Ref="R37"  Part="1" 
AR Path="/7E4188DA4BB67E6E" Ref="R37"  Part="1" 
AR Path="/8D384BB67E6E" Ref="R37"  Part="1" 
AR Path="/24BB67E6E" Ref="R37"  Part="1" 
AR Path="/773F65F14BB67E6E" Ref="R37"  Part="1" 
AR Path="/23C6504BB67E6E" Ref="R37"  Part="1" 
AR Path="/D1C1E84BB67E6E" Ref="R37"  Part="1" 
AR Path="/D058E04BB67E6E" Ref="R37"  Part="1" 
AR Path="/3C64BB67E6E" Ref="R37"  Part="1" 
AR Path="/23CBC44BB67E6E" Ref="R37"  Part="1" 
AR Path="/2F4A4BB67E6E" Ref="R37"  Part="1" 
AR Path="/23D9004BB67E6E" Ref="R37"  Part="1" 
AR Path="/64BB67E6E" Ref="R37"  Part="1" 
AR Path="/6FE934344BB67E6E" Ref="R37"  Part="1" 
AR Path="/6FE934E34BB67E6E" Ref="R37"  Part="1" 
AR Path="/2824BB67E6E" Ref="R37"  Part="1" 
AR Path="/69549BC04BB67E6E" Ref="R37"  Part="1" 
AR Path="/D1C3384BB67E6E" Ref="R37"  Part="1" 
F 0 "R37" V 21780 23950 50  0000 C CNN
F 1 "10" V 21700 23950 50  0000 C CNN
F 2 "R3" V 21800 23950 50  0001 C CNN
F 3 "" H 21700 23950 60  0001 C CNN
	1    21700 23950
	0    1    1    0   
$EndComp
Text Label 22000 23950 0    60   ~ 0
pSTVAL*
Text Label 22000 24150 0    60   ~ 0
pWR*
$Comp
L R R38
U 1 1 4BB67E6D
P 21700 24150
AR Path="/4BB67E6D" Ref="R38"  Part="1" 
AR Path="/FFFFFFFF4BB67E6D" Ref="R38"  Part="1" 
AR Path="/755D912A4BB67E6D" Ref="R?"  Part="1" 
AR Path="/23D83C4BB67E6D" Ref="R38"  Part="1" 
AR Path="/47907FA4BB67E6D" Ref="R?"  Part="1" 
AR Path="/23C34C4BB67E6D" Ref="R38"  Part="1" 
AR Path="/14BB67E6D" Ref="R?"  Part="1" 
AR Path="/23BC884BB67E6D" Ref="R38"  Part="1" 
AR Path="/773F8EB44BB67E6D" Ref="R38"  Part="1" 
AR Path="/94BB67E6D" Ref="R"  Part="1" 
AR Path="/23C9F04BB67E6D" Ref="R38"  Part="1" 
AR Path="/FFFFFFF04BB67E6D" Ref="R38"  Part="1" 
AR Path="/7E4188DA4BB67E6D" Ref="R38"  Part="1" 
AR Path="/8D384BB67E6D" Ref="R38"  Part="1" 
AR Path="/24BB67E6D" Ref="R38"  Part="1" 
AR Path="/773F65F14BB67E6D" Ref="R38"  Part="1" 
AR Path="/23C6504BB67E6D" Ref="R38"  Part="1" 
AR Path="/D1C1E84BB67E6D" Ref="R38"  Part="1" 
AR Path="/D058E04BB67E6D" Ref="R38"  Part="1" 
AR Path="/3C64BB67E6D" Ref="R38"  Part="1" 
AR Path="/23CBC44BB67E6D" Ref="R38"  Part="1" 
AR Path="/2F4A4BB67E6D" Ref="R38"  Part="1" 
AR Path="/23D9004BB67E6D" Ref="R38"  Part="1" 
AR Path="/64BB67E6D" Ref="R38"  Part="1" 
AR Path="/6FE934344BB67E6D" Ref="R38"  Part="1" 
AR Path="/6FE934E34BB67E6D" Ref="R38"  Part="1" 
AR Path="/2824BB67E6D" Ref="R38"  Part="1" 
AR Path="/69549BC04BB67E6D" Ref="R38"  Part="1" 
AR Path="/D1C3384BB67E6D" Ref="R38"  Part="1" 
F 0 "R38" V 21780 24150 50  0000 C CNN
F 1 "10" V 21700 24150 50  0000 C CNN
F 2 "R3" V 21800 24150 50  0001 C CNN
F 3 "" H 21700 24150 60  0001 C CNN
	1    21700 24150
	0    1    1    0   
$EndComp
$Comp
L R R36
U 1 1 4BB67E59
P 21700 23750
AR Path="/4BB67E59" Ref="R36"  Part="1" 
AR Path="/FFFFFFFF4BB67E59" Ref="R36"  Part="1" 
AR Path="/755D912A4BB67E59" Ref="R?"  Part="1" 
AR Path="/23D83C4BB67E59" Ref="R36"  Part="1" 
AR Path="/47907FA4BB67E59" Ref="R?"  Part="1" 
AR Path="/23C34C4BB67E59" Ref="R36"  Part="1" 
AR Path="/14BB67E59" Ref="R?"  Part="1" 
AR Path="/23BC884BB67E59" Ref="R36"  Part="1" 
AR Path="/773F8EB44BB67E59" Ref="R36"  Part="1" 
AR Path="/94BB67E59" Ref="R"  Part="1" 
AR Path="/23C9F04BB67E59" Ref="R36"  Part="1" 
AR Path="/FFFFFFF04BB67E59" Ref="R36"  Part="1" 
AR Path="/7E4188DA4BB67E59" Ref="R36"  Part="1" 
AR Path="/8D384BB67E59" Ref="R36"  Part="1" 
AR Path="/24BB67E59" Ref="R36"  Part="1" 
AR Path="/773F65F14BB67E59" Ref="R36"  Part="1" 
AR Path="/23C6504BB67E59" Ref="R36"  Part="1" 
AR Path="/D1C1E84BB67E59" Ref="R36"  Part="1" 
AR Path="/D058E04BB67E59" Ref="R36"  Part="1" 
AR Path="/3C64BB67E59" Ref="R36"  Part="1" 
AR Path="/23CBC44BB67E59" Ref="R36"  Part="1" 
AR Path="/2F4A4BB67E59" Ref="R36"  Part="1" 
AR Path="/23D9004BB67E59" Ref="R36"  Part="1" 
AR Path="/64BB67E59" Ref="R36"  Part="1" 
AR Path="/6FE934344BB67E59" Ref="R36"  Part="1" 
AR Path="/6FE934E34BB67E59" Ref="R36"  Part="1" 
AR Path="/2824BB67E59" Ref="R36"  Part="1" 
AR Path="/69549BC04BB67E59" Ref="R36"  Part="1" 
AR Path="/D1C3384BB67E59" Ref="R36"  Part="1" 
F 0 "R36" V 21780 23750 50  0000 C CNN
F 1 "10" V 21700 23750 50  0000 C CNN
F 2 "R3" V 21800 23750 50  0001 C CNN
F 3 "" H 21700 23750 60  0001 C CNN
	1    21700 23750
	0    1    1    0   
$EndComp
Text Label 22000 23750 0    60   ~ 0
pSYNC
Text Label 22000 23550 0    60   ~ 0
pDBIN
$Comp
L R R?
U 1 1 4BB67E2A
P 21700 23550
AR Path="/2300384BB67E2A" Ref="R?"  Part="1" 
AR Path="/4BB67E2A" Ref="R35"  Part="1" 
AR Path="/FFFFFFFF4BB67E2A" Ref="R35"  Part="1" 
AR Path="/755D912A4BB67E2A" Ref="R?"  Part="1" 
AR Path="/23D83C4BB67E2A" Ref="R35"  Part="1" 
AR Path="/47907FA4BB67E2A" Ref="R?"  Part="1" 
AR Path="/23C34C4BB67E2A" Ref="R35"  Part="1" 
AR Path="/14BB67E2A" Ref="R?"  Part="1" 
AR Path="/23BC884BB67E2A" Ref="R35"  Part="1" 
AR Path="/773F8EB44BB67E2A" Ref="R35"  Part="1" 
AR Path="/94BB67E2A" Ref="R"  Part="1" 
AR Path="/23C9F04BB67E2A" Ref="R35"  Part="1" 
AR Path="/FFFFFFF04BB67E2A" Ref="R35"  Part="1" 
AR Path="/7E4188DA4BB67E2A" Ref="R35"  Part="1" 
AR Path="/8D384BB67E2A" Ref="R35"  Part="1" 
AR Path="/24BB67E2A" Ref="R35"  Part="1" 
AR Path="/773F65F14BB67E2A" Ref="R35"  Part="1" 
AR Path="/23C6504BB67E2A" Ref="R35"  Part="1" 
AR Path="/D1C1E84BB67E2A" Ref="R35"  Part="1" 
AR Path="/D058E04BB67E2A" Ref="R35"  Part="1" 
AR Path="/3C64BB67E2A" Ref="R35"  Part="1" 
AR Path="/23CBC44BB67E2A" Ref="R35"  Part="1" 
AR Path="/2F4A4BB67E2A" Ref="R35"  Part="1" 
AR Path="/23D9004BB67E2A" Ref="R35"  Part="1" 
AR Path="/64BB67E2A" Ref="R35"  Part="1" 
AR Path="/6FE934344BB67E2A" Ref="R35"  Part="1" 
AR Path="/6FE934E34BB67E2A" Ref="R35"  Part="1" 
AR Path="/2824BB67E2A" Ref="R35"  Part="1" 
AR Path="/69549BC04BB67E2A" Ref="R35"  Part="1" 
AR Path="/D1C3384BB67E2A" Ref="R35"  Part="1" 
F 0 "R35" V 21780 23550 50  0000 C CNN
F 1 "10" V 21700 23550 50  0000 C CNN
F 2 "R3" V 21800 23550 50  0001 C CNN
F 3 "" H 21700 23550 60  0001 C CNN
	1    21700 23550
	0    1    1    0   
$EndComp
Text Label 20800 25950 0    60   ~ 0
INT*
$Comp
L 74LS04 U?
U 3 1 4BB67D2D
P 9950 26250
AR Path="/2300384BB67D2D" Ref="U?"  Part="1" 
AR Path="/4BB67D2D" Ref="U36"  Part="3" 
AR Path="/374432444BB67D2D" Ref="U?"  Part="1" 
AR Path="/2600004BB67D2D" Ref="U"  Part="2" 
AR Path="/FFFFFFFF4BB67D2D" Ref="U36"  Part="2" 
AR Path="/755D912A4BB67D2D" Ref="U?"  Part="2" 
AR Path="/7E428DAC4BB67D2D" Ref="U?"  Part="2" 
AR Path="/2AF2D04BB67D2D" Ref="U?"  Part="2" 
AR Path="/49D08104BB67D2D" Ref="U"  Part="3" 
AR Path="/23D83C4BB67D2D" Ref="U36"  Part="3" 
AR Path="/47907FA4BB67D2D" Ref="U36"  Part="3" 
AR Path="/23C34C4BB67D2D" Ref="U36"  Part="3" 
AR Path="/14BB67D2D" Ref="U36"  Part="3" 
AR Path="/23BC884BB67D2D" Ref="U36"  Part="3" 
AR Path="/773F8EB44BB67D2D" Ref="U36"  Part="3" 
AR Path="/94BB67D2D" Ref="U"  Part="3" 
AR Path="/23C9F04BB67D2D" Ref="U36"  Part="3" 
AR Path="/FFFFFFF04BB67D2D" Ref="U36"  Part="3" 
AR Path="/7E4188DA4BB67D2D" Ref="U36"  Part="3" 
AR Path="/8D384BB67D2D" Ref="U36"  Part="3" 
AR Path="/773F65F14BB67D2D" Ref="U36"  Part="3" 
AR Path="/24BB67D2D" Ref="U36"  Part="3" 
AR Path="/23C6504BB67D2D" Ref="U36"  Part="3" 
AR Path="/D058E04BB67D2D" Ref="U36"  Part="3" 
AR Path="/3C64BB67D2D" Ref="U36"  Part="3" 
AR Path="/23CBC44BB67D2D" Ref="U36"  Part="3" 
AR Path="/2F4A4BB67D2D" Ref="U36"  Part="3" 
AR Path="/23D9004BB67D2D" Ref="U36"  Part="3" 
AR Path="/64BB67D2D" Ref="U36"  Part="3" 
AR Path="/6FE934344BB67D2D" Ref="U36"  Part="3" 
AR Path="/6FE934E34BB67D2D" Ref="U36"  Part="3" 
AR Path="/2824BB67D2D" Ref="U36"  Part="3" 
AR Path="/69549BC04BB67D2D" Ref="U36"  Part="3" 
AR Path="/D1C3384BB67D2D" Ref="U36"  Part="3" 
F 0 "U36" H 10145 26365 60  0000 C CNN
F 1 "74LS04" H 10140 26125 60  0000 C CNN
F 2 "14dip300" H 10140 26225 60  0001 C CNN
F 3 "" H 9950 26250 60  0001 C CNN
	3    9950 26250
	0    1    1    0   
$EndComp
Text Label 12550 25250 0    60   ~ 0
RESET*
$Comp
L CONN_3 K?
U 1 1 4BB67C89
P 12400 24600
AR Path="/A200384BB67C89" Ref="K?"  Part="1" 
AR Path="/4BB67C89" Ref="K2"  Part="1" 
AR Path="/FFFFFFFF4BB67C89" Ref="K2"  Part="1" 
AR Path="/755D912A4BB67C89" Ref="K?"  Part="1" 
AR Path="/31324BB67C89" Ref="K?"  Part="1" 
AR Path="/2606084BB67C89" Ref="K2"  Part="1" 
AR Path="/23D83C4BB67C89" Ref="K2"  Part="1" 
AR Path="/47907FA4BB67C89" Ref="K2"  Part="1" 
AR Path="/23C34C4BB67C89" Ref="K2"  Part="1" 
AR Path="/14BB67C89" Ref="K2"  Part="1" 
AR Path="/23BC884BB67C89" Ref="K2"  Part="1" 
AR Path="/773F8EB44BB67C89" Ref="K2"  Part="1" 
AR Path="/23C9F04BB67C89" Ref="K2"  Part="1" 
AR Path="/94BB67C89" Ref="K"  Part="1" 
AR Path="/FFFFFFF04BB67C89" Ref="K2"  Part="1" 
AR Path="/7E4188DA4BB67C89" Ref="K2"  Part="1" 
AR Path="/8D384BB67C89" Ref="K2"  Part="1" 
AR Path="/24BB67C89" Ref="K2"  Part="1" 
AR Path="/773F65F14BB67C89" Ref="K2"  Part="1" 
AR Path="/23C6504BB67C89" Ref="K2"  Part="1" 
AR Path="/D058E04BB67C89" Ref="K2"  Part="1" 
AR Path="/3C64BB67C89" Ref="K2"  Part="1" 
AR Path="/23CBC44BB67C89" Ref="K2"  Part="1" 
AR Path="/2F4A4BB67C89" Ref="K2"  Part="1" 
AR Path="/23D9004BB67C89" Ref="K2"  Part="1" 
AR Path="/64BB67C89" Ref="K2"  Part="1" 
AR Path="/6FE934344BB67C89" Ref="K2"  Part="1" 
AR Path="/6FE934E34BB67C89" Ref="K2"  Part="1" 
AR Path="/2824BB67C89" Ref="K2"  Part="1" 
AR Path="/69549BC04BB67C89" Ref="K2"  Part="1" 
AR Path="/D1C3384BB67C89" Ref="K2"  Part="1" 
F 0 "K2" V 12350 24600 50  0000 C CNN
F 1 "CONN_3" V 12450 24600 40  0000 C CNN
F 2 "SIL-3" V 12550 24600 40  0001 C CNN
F 3 "" H 12400 24600 60  0001 C CNN
	1    12400 24600
	0    -1   -1   0   
$EndComp
$Comp
L 74LS74 U?
U 2 1 4BB67C55
P 12450 25800
AR Path="/2300384BB67C55" Ref="U?"  Part="1" 
AR Path="/4BB67C55" Ref="U34"  Part="2" 
AR Path="/FFFFFFFF4BB67C55" Ref="U34"  Part="1" 
AR Path="/755D912A4BB67C55" Ref="U?"  Part="1" 
AR Path="/31324BB67C55" Ref="U?"  Part="1" 
AR Path="/2B91304BB67C55" Ref="U?"  Part="1" 
AR Path="/49308104BB67C55" Ref="U"  Part="2" 
AR Path="/23D83C4BB67C55" Ref="U34"  Part="2" 
AR Path="/47907FA4BB67C55" Ref="U34"  Part="2" 
AR Path="/23C34C4BB67C55" Ref="U34"  Part="2" 
AR Path="/14BB67C55" Ref="U34"  Part="2" 
AR Path="/23BC884BB67C55" Ref="U34"  Part="2" 
AR Path="/773F8EB44BB67C55" Ref="U34"  Part="2" 
AR Path="/94BB67C55" Ref="U"  Part="2" 
AR Path="/23C9F04BB67C55" Ref="U34"  Part="2" 
AR Path="/FFFFFFF04BB67C55" Ref="U34"  Part="2" 
AR Path="/7E4188DA4BB67C55" Ref="U34"  Part="2" 
AR Path="/8D384BB67C55" Ref="U34"  Part="2" 
AR Path="/24BB67C55" Ref="U34"  Part="2" 
AR Path="/773F65F14BB67C55" Ref="U34"  Part="2" 
AR Path="/23C6504BB67C55" Ref="U34"  Part="2" 
AR Path="/D058E04BB67C55" Ref="U34"  Part="2" 
AR Path="/3C64BB67C55" Ref="U34"  Part="2" 
AR Path="/23CBC44BB67C55" Ref="U34"  Part="2" 
AR Path="/2F4A4BB67C55" Ref="U34"  Part="2" 
AR Path="/23D9004BB67C55" Ref="U34"  Part="2" 
AR Path="/64BB67C55" Ref="U34"  Part="2" 
AR Path="/6FE934344BB67C55" Ref="U34"  Part="2" 
AR Path="/6FE934E34BB67C55" Ref="U34"  Part="2" 
AR Path="/2824BB67C55" Ref="U34"  Part="2" 
AR Path="/69549BC04BB67C55" Ref="U34"  Part="2" 
AR Path="/D1C3384BB67C55" Ref="U34"  Part="2" 
F 0 "U34" H 12600 26100 60  0000 C CNN
F 1 "74LS74" H 12750 25505 60  0000 C CNN
F 2 "14dip300" H 12750 25605 60  0001 C CNN
F 3 "" H 12450 25800 60  0001 C CNN
	2    12450 25800
	1    0    0    -1  
$EndComp
Text Label 20750 25600 0    60   ~ 0
CDSB*
$Comp
L 74LS05 U?
U 6 1 4BB67BF8
P 20250 25600
AR Path="/2300384BB67BF8" Ref="U?"  Part="1" 
AR Path="/4BB67BF8" Ref="U37"  Part="6" 
AR Path="/374246384BB67BF8" Ref="U?"  Part="1" 
AR Path="/2600004BB67BF8" Ref="U"  Part="4" 
AR Path="/FFFFFFFF4BB67BF8" Ref="U37"  Part="4" 
AR Path="/755D912A4BB67BF8" Ref="U?"  Part="4" 
AR Path="/7E428DAC4BB67BF8" Ref="U37"  Part="4" 
AR Path="/2B3C304BB67BF8" Ref="U?"  Part="4" 
AR Path="/43C08324BB67BF8" Ref="U"  Part="6" 
AR Path="/2B66D84BB67BF8" Ref="U37"  Part="6" 
AR Path="/61808744BB67BF8" Ref="U"  Part="6" 
AR Path="/23D83C4BB67BF8" Ref="U37"  Part="6" 
AR Path="/47907FA4BB67BF8" Ref="U37"  Part="6" 
AR Path="/23C34C4BB67BF8" Ref="U37"  Part="6" 
AR Path="/14BB67BF8" Ref="U37"  Part="6" 
AR Path="/23BC884BB67BF8" Ref="U37"  Part="6" 
AR Path="/773F8EB44BB67BF8" Ref="U37"  Part="6" 
AR Path="/94BB67BF8" Ref="U"  Part="6" 
AR Path="/23C9F04BB67BF8" Ref="U37"  Part="6" 
AR Path="/FFFFFFF04BB67BF8" Ref="U37"  Part="6" 
AR Path="/7E4188DA4BB67BF8" Ref="U37"  Part="6" 
AR Path="/8D384BB67BF8" Ref="U37"  Part="6" 
AR Path="/773F65F14BB67BF8" Ref="U37"  Part="6" 
AR Path="/24BB67BF8" Ref="U37"  Part="6" 
AR Path="/23C6504BB67BF8" Ref="U37"  Part="6" 
AR Path="/D058E04BB67BF8" Ref="U37"  Part="6" 
AR Path="/3C64BB67BF8" Ref="U37"  Part="6" 
AR Path="/23CBC44BB67BF8" Ref="U37"  Part="6" 
AR Path="/2F4A4BB67BF8" Ref="U37"  Part="6" 
AR Path="/23D9004BB67BF8" Ref="U37"  Part="6" 
AR Path="/64BB67BF8" Ref="U37"  Part="6" 
AR Path="/6FE934344BB67BF8" Ref="U37"  Part="6" 
AR Path="/6FE934E34BB67BF8" Ref="U37"  Part="6" 
AR Path="/2824BB67BF8" Ref="U37"  Part="6" 
AR Path="/69549BC04BB67BF8" Ref="U37"  Part="6" 
AR Path="/D1C3384BB67BF8" Ref="U37"  Part="6" 
F 0 "U37" H 20445 25715 60  0000 C CNN
F 1 "74LS05" H 20440 25475 60  0000 C CNN
F 2 "14dip300" H 20440 25575 60  0001 C CNN
F 3 "" H 20250 25600 60  0001 C CNN
	6    20250 25600
	-1   0    0    1   
$EndComp
$Comp
L 74LS02 U?
U 2 1 4BB67BA4
P 20250 22800
AR Path="/2300384BB67BA4" Ref="U?"  Part="1" 
AR Path="/4BB67BA4" Ref="U45"  Part="2" 
AR Path="/374241344BB67BA4" Ref="U?"  Part="1" 
AR Path="/2D8D184BB67BA4" Ref="U?"  Part="1" 
AR Path="/FFFFFFFF4BB67BA4" Ref="U45"  Part="3" 
AR Path="/755D912A4BB67BA4" Ref="U?"  Part="3" 
AR Path="/23D8044BB67BA4" Ref="U?"  Part="3" 
AR Path="/7E4188DA4BB67BA4" Ref="U45"  Part="3" 
AR Path="/B8C4BB67BA4" Ref="U?"  Part="3" 
AR Path="/2B13084BB67BA4" Ref="U?"  Part="3" 
AR Path="/44E08044BB67BA4" Ref="U"  Part="2" 
AR Path="/23D83C4BB67BA4" Ref="U45"  Part="2" 
AR Path="/47907FA4BB67BA4" Ref="U45"  Part="2" 
AR Path="/23C34C4BB67BA4" Ref="U45"  Part="2" 
AR Path="/14BB67BA4" Ref="U45"  Part="2" 
AR Path="/23BC884BB67BA4" Ref="U45"  Part="2" 
AR Path="/773F8EB44BB67BA4" Ref="U45"  Part="2" 
AR Path="/94BB67BA4" Ref="U"  Part="2" 
AR Path="/23C9F04BB67BA4" Ref="U45"  Part="2" 
AR Path="/FFFFFFF04BB67BA4" Ref="U45"  Part="2" 
AR Path="/8D384BB67BA4" Ref="U45"  Part="2" 
AR Path="/773F65F14BB67BA4" Ref="U45"  Part="2" 
AR Path="/24BB67BA4" Ref="U45"  Part="2" 
AR Path="/23C6504BB67BA4" Ref="U45"  Part="2" 
AR Path="/D058E04BB67BA4" Ref="U45"  Part="2" 
AR Path="/3C64BB67BA4" Ref="U45"  Part="2" 
AR Path="/23CBC44BB67BA4" Ref="U45"  Part="2" 
AR Path="/2F4A4BB67BA4" Ref="U45"  Part="2" 
AR Path="/23D9004BB67BA4" Ref="U45"  Part="2" 
AR Path="/64BB67BA4" Ref="U45"  Part="2" 
AR Path="/6FE934344BB67BA4" Ref="U45"  Part="2" 
AR Path="/6FE934E34BB67BA4" Ref="U45"  Part="2" 
AR Path="/2824BB67BA4" Ref="U45"  Part="2" 
AR Path="/69549BC04BB67BA4" Ref="U45"  Part="2" 
AR Path="/D1C3384BB67BA4" Ref="U45"  Part="2" 
F 0 "U45" H 20250 22850 60  0000 C CNN
F 1 "74LS02" H 20300 22750 60  0000 C CNN
F 2 "14dip300" H 20300 22850 60  0001 C CNN
F 3 "" H 20250 22800 60  0001 C CNN
	2    20250 22800
	-1   0    0    1   
$EndComp
$Comp
L 74LS74 U?
U 2 1 4BB67B37
P 18800 19100
AR Path="/2300384BB67B37" Ref="U?"  Part="1" 
AR Path="/4BB67B37" Ref="U33"  Part="2" 
AR Path="/374233374BB67B37" Ref="U?"  Part="1" 
AR Path="/2600004BB67B37" Ref="U"  Part="2" 
AR Path="/FFFFFFFF4BB67B37" Ref="U33"  Part="2" 
AR Path="/755D912A4BB67B37" Ref="U?"  Part="2" 
AR Path="/7E428DAC4BB67B37" Ref="U?"  Part="2" 
AR Path="/29EC004BB67B37" Ref="U?"  Part="2" 
AR Path="/44A08044BB67B37" Ref="U"  Part="2" 
AR Path="/23D83C4BB67B37" Ref="U33"  Part="2" 
AR Path="/47907FA4BB67B37" Ref="U33"  Part="2" 
AR Path="/23C34C4BB67B37" Ref="U33"  Part="2" 
AR Path="/14BB67B37" Ref="U33"  Part="2" 
AR Path="/23BC884BB67B37" Ref="U33"  Part="2" 
AR Path="/773F8EB44BB67B37" Ref="U33"  Part="2" 
AR Path="/94BB67B37" Ref="U"  Part="2" 
AR Path="/23C9F04BB67B37" Ref="U33"  Part="2" 
AR Path="/FFFFFFF04BB67B37" Ref="U33"  Part="2" 
AR Path="/7E4188DA4BB67B37" Ref="U33"  Part="2" 
AR Path="/8D384BB67B37" Ref="U33"  Part="2" 
AR Path="/24BB67B37" Ref="U33"  Part="2" 
AR Path="/773F65F14BB67B37" Ref="U33"  Part="2" 
AR Path="/23C6504BB67B37" Ref="U33"  Part="2" 
AR Path="/D058E04BB67B37" Ref="U33"  Part="2" 
AR Path="/3C64BB67B37" Ref="U33"  Part="2" 
AR Path="/23CBC44BB67B37" Ref="U33"  Part="2" 
AR Path="/2F4A4BB67B37" Ref="U33"  Part="2" 
AR Path="/23D9004BB67B37" Ref="U33"  Part="2" 
AR Path="/64BB67B37" Ref="U33"  Part="2" 
AR Path="/6FE934344BB67B37" Ref="U33"  Part="2" 
AR Path="/6FE934E34BB67B37" Ref="U33"  Part="2" 
AR Path="/2824BB67B37" Ref="U33"  Part="2" 
AR Path="/69549BC04BB67B37" Ref="U33"  Part="2" 
AR Path="/D1C3384BB67B37" Ref="U33"  Part="2" 
F 0 "U33" H 18950 19400 60  0000 C CNN
F 1 "74LS74" H 19100 18805 60  0000 C CNN
F 2 "14dip300" H 19100 18905 60  0001 C CNN
F 3 "" H 18800 19100 60  0001 C CNN
	2    18800 19100
	1    0    0    -1  
$EndComp
$Comp
L 74LS241 U?
U 1 1 4BB67A1D
P 20500 24400
AR Path="/A200384BB67A1D" Ref="U?"  Part="1" 
AR Path="/4BB67A1D" Ref="U21"  Part="1" 
AR Path="/FFFFFFFF4BB67A1D" Ref="U21"  Part="1" 
AR Path="/755D912A4BB67A1D" Ref="U?"  Part="1" 
AR Path="/7E428DAC4BB67A1D" Ref="U?"  Part="1" 
AR Path="/2606084BB67A1D" Ref="U21"  Part="1" 
AR Path="/23D83C4BB67A1D" Ref="U21"  Part="1" 
AR Path="/47907FA4BB67A1D" Ref="U21"  Part="1" 
AR Path="/23C34C4BB67A1D" Ref="U21"  Part="1" 
AR Path="/14BB67A1D" Ref="U21"  Part="1" 
AR Path="/23BC884BB67A1D" Ref="U21"  Part="1" 
AR Path="/773F8EB44BB67A1D" Ref="U21"  Part="1" 
AR Path="/94BB67A1D" Ref="U"  Part="1" 
AR Path="/23C9F04BB67A1D" Ref="U21"  Part="1" 
AR Path="/FFFFFFF04BB67A1D" Ref="U21"  Part="1" 
AR Path="/7E4188DA4BB67A1D" Ref="U21"  Part="1" 
AR Path="/8D384BB67A1D" Ref="U21"  Part="1" 
AR Path="/24BB67A1D" Ref="U21"  Part="1" 
AR Path="/773F65F14BB67A1D" Ref="U21"  Part="1" 
AR Path="/23C6504BB67A1D" Ref="U21"  Part="1" 
AR Path="/D058E04BB67A1D" Ref="U21"  Part="1" 
AR Path="/3C64BB67A1D" Ref="U21"  Part="1" 
AR Path="/23CBC44BB67A1D" Ref="U21"  Part="1" 
AR Path="/2F4A4BB67A1D" Ref="U21"  Part="1" 
AR Path="/23D9004BB67A1D" Ref="U21"  Part="1" 
AR Path="/64BB67A1D" Ref="U21"  Part="1" 
AR Path="/6FE934344BB67A1D" Ref="U21"  Part="1" 
AR Path="/6FE934E34BB67A1D" Ref="U21"  Part="1" 
AR Path="/2824BB67A1D" Ref="U21"  Part="1" 
AR Path="/69549BC04BB67A1D" Ref="U21"  Part="1" 
AR Path="/D1C3384BB67A1D" Ref="U21"  Part="1" 
F 0 "U21" H 20550 24200 60  0000 C CNN
F 1 "74LS244" H 20600 24000 60  0000 C CNN
F 2 "20dip300" H 20600 24100 60  0001 C CNN
F 3 "" H 20500 24400 60  0001 C CNN
	1    20500 24400
	1    0    0    -1  
$EndComp
$Comp
L 74LS00 U?
U 1 1 4BB67916
P 9050 24200
AR Path="/2300384BB67916" Ref="U?"  Part="1" 
AR Path="/4BB67916" Ref="U40"  Part="1" 
AR Path="/FFFFFFFF4BB67916" Ref="U40"  Part="1" 
AR Path="/755D912A4BB67916" Ref="U?"  Part="1" 
AR Path="/2606084BB67916" Ref="U40"  Part="1" 
AR Path="/23D83C4BB67916" Ref="U40"  Part="1" 
AR Path="/47907FA4BB67916" Ref="U40"  Part="1" 
AR Path="/23C34C4BB67916" Ref="U40"  Part="1" 
AR Path="/14BB67916" Ref="U40"  Part="1" 
AR Path="/23BC884BB67916" Ref="U40"  Part="1" 
AR Path="/773F8EB44BB67916" Ref="U40"  Part="1" 
AR Path="/94BB67916" Ref="U"  Part="1" 
AR Path="/23C9F04BB67916" Ref="U40"  Part="1" 
AR Path="/FFFFFFF04BB67916" Ref="U40"  Part="1" 
AR Path="/7E4188DA4BB67916" Ref="U40"  Part="1" 
AR Path="/8D384BB67916" Ref="U40"  Part="1" 
AR Path="/24BB67916" Ref="U40"  Part="1" 
AR Path="/773F65F14BB67916" Ref="U40"  Part="1" 
AR Path="/23C6504BB67916" Ref="U40"  Part="1" 
AR Path="/D058E04BB67916" Ref="U40"  Part="1" 
AR Path="/3C64BB67916" Ref="U40"  Part="1" 
AR Path="/23CBC44BB67916" Ref="U40"  Part="1" 
AR Path="/2F4A4BB67916" Ref="U40"  Part="1" 
AR Path="/23D9004BB67916" Ref="U40"  Part="1" 
AR Path="/64BB67916" Ref="U40"  Part="1" 
AR Path="/6FE934344BB67916" Ref="U40"  Part="1" 
AR Path="/6FE934E34BB67916" Ref="U40"  Part="1" 
AR Path="/2824BB67916" Ref="U40"  Part="1" 
AR Path="/69549BC04BB67916" Ref="U40"  Part="1" 
AR Path="/D1C3384BB67916" Ref="U40"  Part="1" 
F 0 "U40" H 9050 24250 60  0000 C CNN
F 1 "74S00" H 9050 24150 60  0000 C CNN
F 2 "14dip300" H 9050 24250 60  0001 C CNN
F 3 "" H 9050 24200 60  0001 C CNN
	1    9050 24200
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 4BB678BD
P 1750 25100
AR Path="/2300384BB678BD" Ref="#PWR?"  Part="1" 
AR Path="/4BB678BD" Ref="#PWR027"  Part="1" 
AR Path="/FFFFFFFF4BB678BD" Ref="#PWR030"  Part="1" 
AR Path="/755D912A4BB678BD" Ref="#PWR?"  Part="1" 
AR Path="/23D83C4BB678BD" Ref="#PWR5"  Part="1" 
AR Path="/47907FA4BB678BD" Ref="#PWR031"  Part="1" 
AR Path="/23C34C4BB678BD" Ref="#PWR029"  Part="1" 
AR Path="/14BB678BD" Ref="#PWR031"  Part="1" 
AR Path="/23BC884BB678BD" Ref="#PWR030"  Part="1" 
AR Path="/773F8EB44BB678BD" Ref="#PWR028"  Part="1" 
AR Path="/94BB678BD" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB678BD" Ref="#PWR1"  Part="1" 
AR Path="/FFFFFFF04BB678BD" Ref="#PWR5"  Part="1" 
AR Path="/7E4188DA4BB678BD" Ref="#PWR5"  Part="1" 
AR Path="/8D384BB678BD" Ref="#PWR031"  Part="1" 
AR Path="/773F65F14BB678BD" Ref="#PWR030"  Part="1" 
AR Path="/6FF0DD404BB678BD" Ref="#PWR030"  Part="1" 
AR Path="/24BB678BD" Ref="#PWR1"  Part="1" 
AR Path="/23C6504BB678BD" Ref="#PWR032"  Part="1" 
AR Path="/D058E04BB678BD" Ref="#PWR030"  Part="1" 
AR Path="/6684D64BB678BD" Ref="#PWR028"  Part="1" 
AR Path="/3C64BB678BD" Ref="#PWR032"  Part="1" 
AR Path="/23CBC44BB678BD" Ref="#PWR028"  Part="1" 
AR Path="/2F4A4BB678BD" Ref="#PWR032"  Part="1" 
AR Path="/23D9004BB678BD" Ref="#PWR028"  Part="1" 
AR Path="/64BB678BD" Ref="#PWR3"  Part="1" 
AR Path="/6FE934344BB678BD" Ref="#PWR3"  Part="1" 
AR Path="/6FE934E34BB678BD" Ref="#PWR028"  Part="1" 
AR Path="/2824BB678BD" Ref="#PWR028"  Part="1" 
AR Path="/69549BC04BB678BD" Ref="#PWR028"  Part="1" 
AR Path="/D1C3384BB678BD" Ref="#PWR030"  Part="1" 
F 0 "#PWR027" H 1750 25200 30  0001 C CNN
F 1 "VCC" H 1750 25200 30  0000 C CNN
F 2 "" H 1750 25100 60  0001 C CNN
F 3 "" H 1750 25100 60  0001 C CNN
	1    1750 25100
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 4BB678B4
P 1750 25350
AR Path="/2300384BB678B4" Ref="R?"  Part="1" 
AR Path="/4BB678B4" Ref="R5"  Part="1" 
AR Path="/373842444BB678B4" Ref="R?"  Part="1" 
AR Path="/FFFFFFFF4BB678B4" Ref="R5"  Part="1" 
AR Path="/755D912A4BB678B4" Ref="R?"  Part="1" 
AR Path="/23D83C4BB678B4" Ref="R5"  Part="1" 
AR Path="/47907FA4BB678B4" Ref="R?"  Part="1" 
AR Path="/23C34C4BB678B4" Ref="R5"  Part="1" 
AR Path="/14BB678B4" Ref="R?"  Part="1" 
AR Path="/23BC884BB678B4" Ref="R5"  Part="1" 
AR Path="/773F8EB44BB678B4" Ref="R5"  Part="1" 
AR Path="/94BB678B4" Ref="R"  Part="1" 
AR Path="/23C9F04BB678B4" Ref="R5"  Part="1" 
AR Path="/FFFFFFF04BB678B4" Ref="R5"  Part="1" 
AR Path="/7E4188DA4BB678B4" Ref="R5"  Part="1" 
AR Path="/8D384BB678B4" Ref="R5"  Part="1" 
AR Path="/24BB678B4" Ref="R5"  Part="1" 
AR Path="/773F65F14BB678B4" Ref="R5"  Part="1" 
AR Path="/23C6504BB678B4" Ref="R5"  Part="1" 
AR Path="/D058E04BB678B4" Ref="R5"  Part="1" 
AR Path="/3C64BB678B4" Ref="R5"  Part="1" 
AR Path="/23CBC44BB678B4" Ref="R5"  Part="1" 
AR Path="/2F4A4BB678B4" Ref="R5"  Part="1" 
AR Path="/23D9004BB678B4" Ref="R5"  Part="1" 
AR Path="/64BB678B4" Ref="R5"  Part="1" 
AR Path="/6FE934344BB678B4" Ref="R5"  Part="1" 
AR Path="/6FE934E34BB678B4" Ref="R5"  Part="1" 
AR Path="/2824BB678B4" Ref="R5"  Part="1" 
AR Path="/69549BC04BB678B4" Ref="R5"  Part="1" 
AR Path="/D1C3384BB678B4" Ref="R5"  Part="1" 
F 0 "R5" V 1830 25350 50  0000 C CNN
F 1 "330" V 1750 25350 50  0000 C CNN
F 2 "R3" V 1850 25350 50  0001 C CNN
F 3 "" H 1750 25350 60  0001 C CNN
	1    1750 25350
	1    0    0    -1  
$EndComp
$Comp
L 74LS04 U?
U 1 1 4BB6788F
P 4050 25600
AR Path="/2300384BB6788F" Ref="U?"  Part="1" 
AR Path="/4BB6788F" Ref="U36"  Part="1" 
AR Path="/FFFFFFFF4BB6788F" Ref="U36"  Part="1" 
AR Path="/755D912A4BB6788F" Ref="U?"  Part="1" 
AR Path="/7E428DAC4BB6788F" Ref="U?"  Part="1" 
AR Path="/2AFF704BB6788F" Ref="U?"  Part="1" 
AR Path="/60E08184BB6788F" Ref="U"  Part="1" 
AR Path="/23D83C4BB6788F" Ref="U36"  Part="1" 
AR Path="/47907FA4BB6788F" Ref="U36"  Part="1" 
AR Path="/23C34C4BB6788F" Ref="U36"  Part="1" 
AR Path="/14BB6788F" Ref="U36"  Part="1" 
AR Path="/23BC884BB6788F" Ref="U36"  Part="1" 
AR Path="/773F8EB44BB6788F" Ref="U36"  Part="1" 
AR Path="/94BB6788F" Ref="U"  Part="1" 
AR Path="/23C9F04BB6788F" Ref="U36"  Part="1" 
AR Path="/FFFFFFF04BB6788F" Ref="U36"  Part="1" 
AR Path="/7E4188DA4BB6788F" Ref="U36"  Part="1" 
AR Path="/8D384BB6788F" Ref="U36"  Part="1" 
AR Path="/773F65F14BB6788F" Ref="U36"  Part="1" 
AR Path="/24BB6788F" Ref="U36"  Part="1" 
AR Path="/23C6504BB6788F" Ref="U36"  Part="1" 
AR Path="/D058E04BB6788F" Ref="U36"  Part="1" 
AR Path="/3C64BB6788F" Ref="U36"  Part="1" 
AR Path="/23CBC44BB6788F" Ref="U36"  Part="1" 
AR Path="/2F4A4BB6788F" Ref="U36"  Part="1" 
AR Path="/23D9004BB6788F" Ref="U36"  Part="1" 
AR Path="/64BB6788F" Ref="U36"  Part="1" 
AR Path="/6FE934344BB6788F" Ref="U36"  Part="1" 
AR Path="/6FE934E34BB6788F" Ref="U36"  Part="1" 
AR Path="/2824BB6788F" Ref="U36"  Part="1" 
AR Path="/69549BC04BB6788F" Ref="U36"  Part="1" 
AR Path="/D1C3384BB6788F" Ref="U36"  Part="1" 
F 0 "U36" H 4245 25715 60  0000 C CNN
F 1 "74LS04" H 4240 25475 60  0000 C CNN
F 2 "14dip300" H 4240 25575 60  0001 C CNN
F 3 "" H 4050 25600 60  0001 C CNN
	1    4050 25600
	-1   0    0    1   
$EndComp
NoConn ~ 14850 17650
$Comp
L 74LS11 U?
U 1 1 4BB67751
P 16800 16050
AR Path="/A200384BB67751" Ref="U?"  Part="1" 
AR Path="/4BB67751" Ref="U43"  Part="1" 
AR Path="/FFFFFFFF4BB67751" Ref="U43"  Part="1" 
AR Path="/755D912A4BB67751" Ref="U?"  Part="1" 
AR Path="/6FE934E34BB67751" Ref="U43"  Part="1" 
AR Path="/2606084BB67751" Ref="U43"  Part="1" 
AR Path="/23D83C4BB67751" Ref="U43"  Part="1" 
AR Path="/47907FA4BB67751" Ref="U43"  Part="1" 
AR Path="/23C34C4BB67751" Ref="U43"  Part="1" 
AR Path="/14BB67751" Ref="U43"  Part="1" 
AR Path="/23BC884BB67751" Ref="U43"  Part="1" 
AR Path="/773F8EB44BB67751" Ref="U43"  Part="1" 
AR Path="/94BB67751" Ref="U"  Part="1" 
AR Path="/23C9F04BB67751" Ref="U43"  Part="1" 
AR Path="/FFFFFFF04BB67751" Ref="U43"  Part="1" 
AR Path="/7E4188DA4BB67751" Ref="U43"  Part="1" 
AR Path="/8D384BB67751" Ref="U43"  Part="1" 
AR Path="/773F65F14BB67751" Ref="U43"  Part="1" 
AR Path="/24BB67751" Ref="U43"  Part="1" 
AR Path="/23C6504BB67751" Ref="U43"  Part="1" 
AR Path="/D058E04BB67751" Ref="U43"  Part="1" 
AR Path="/3C64BB67751" Ref="U43"  Part="1" 
AR Path="/23CBC44BB67751" Ref="U43"  Part="1" 
AR Path="/2F4A4BB67751" Ref="U43"  Part="1" 
AR Path="/23D9004BB67751" Ref="U43"  Part="1" 
AR Path="/64BB67751" Ref="U43"  Part="1" 
AR Path="/6FE934344BB67751" Ref="U43"  Part="1" 
AR Path="/2824BB67751" Ref="U43"  Part="1" 
AR Path="/69549BC04BB67751" Ref="U43"  Part="1" 
AR Path="/D1C3384BB67751" Ref="U43"  Part="1" 
F 0 "U43" H 16800 16100 60  0000 C CNN
F 1 "74LS11" H 16800 16000 60  0000 C CNN
F 2 "14dip300" H 16800 16100 60  0001 C CNN
F 3 "" H 16800 16050 60  0001 C CNN
	1    16800 16050
	-1   0    0    1   
$EndComp
$Comp
L 74LS02 U?
U 1 1 4BB67737
P 15100 18900
AR Path="/A200384BB67737" Ref="U?"  Part="1" 
AR Path="/4BB67737" Ref="U45"  Part="1" 
AR Path="/31324BB67737" Ref="U?"  Part="1" 
AR Path="/C800144BB67737" Ref="U"  Part="4" 
AR Path="/FFFFFFFF4BB67737" Ref="U45"  Part="1" 
AR Path="/755D912A4BB67737" Ref="U?"  Part="4" 
AR Path="/23D8044BB67737" Ref="U?"  Part="4" 
AR Path="/7E42B4154BB67737" Ref="U45"  Part="4" 
AR Path="/373733374BB67737" Ref="U45"  Part="4" 
AR Path="/23D6704BB67737" Ref="U45"  Part="4" 
AR Path="/23D83C4BB67737" Ref="U45"  Part="1" 
AR Path="/47907FA4BB67737" Ref="U45"  Part="1" 
AR Path="/23C34C4BB67737" Ref="U45"  Part="1" 
AR Path="/14BB67737" Ref="U45"  Part="1" 
AR Path="/23BC884BB67737" Ref="U45"  Part="1" 
AR Path="/773F8EB44BB67737" Ref="U45"  Part="1" 
AR Path="/94BB67737" Ref="U"  Part="1" 
AR Path="/23C9F04BB67737" Ref="U45"  Part="1" 
AR Path="/FFFFFFF04BB67737" Ref="U45"  Part="1" 
AR Path="/7E4188DA4BB67737" Ref="U45"  Part="1" 
AR Path="/8D384BB67737" Ref="U45"  Part="1" 
AR Path="/773F65F14BB67737" Ref="U45"  Part="1" 
AR Path="/24BB67737" Ref="U45"  Part="1" 
AR Path="/23C6504BB67737" Ref="U45"  Part="1" 
AR Path="/D058E04BB67737" Ref="U45"  Part="1" 
AR Path="/3C64BB67737" Ref="U45"  Part="1" 
AR Path="/23CBC44BB67737" Ref="U45"  Part="1" 
AR Path="/2F4A4BB67737" Ref="U45"  Part="1" 
AR Path="/23D9004BB67737" Ref="U45"  Part="1" 
AR Path="/64BB67737" Ref="U45"  Part="1" 
AR Path="/6FE934344BB67737" Ref="U45"  Part="1" 
AR Path="/6FE934E34BB67737" Ref="U45"  Part="1" 
AR Path="/2824BB67737" Ref="U45"  Part="1" 
AR Path="/69549BC04BB67737" Ref="U45"  Part="1" 
AR Path="/D1C3384BB67737" Ref="U45"  Part="1" 
F 0 "U45" H 15100 18950 60  0000 C CNN
F 1 "74LS02" H 15150 18850 60  0000 C CNN
F 2 "14dip300" H 15150 18950 60  0001 C CNN
F 3 "" H 15100 18900 60  0001 C CNN
	1    15100 18900
	1    0    0    -1  
$EndComp
$Comp
L 74LS74 U?
U 1 1 4BB67726
P 15450 17450
AR Path="/384BB67726" Ref="U?"  Part="1" 
AR Path="/4BB67726" Ref="U34"  Part="1" 
AR Path="/EC0031324BB67726" Ref="U?"  Part="1" 
AR Path="/FFFFFFFF4BB67726" Ref="U34"  Part="1" 
AR Path="/755D912A4BB67726" Ref="U?"  Part="1" 
AR Path="/23D8044BB67726" Ref="U?"  Part="1" 
AR Path="/2606084BB67726" Ref="U34"  Part="1" 
AR Path="/23D83C4BB67726" Ref="U34"  Part="1" 
AR Path="/47907FA4BB67726" Ref="U34"  Part="1" 
AR Path="/23C34C4BB67726" Ref="U34"  Part="1" 
AR Path="/14BB67726" Ref="U34"  Part="1" 
AR Path="/23BC884BB67726" Ref="U34"  Part="1" 
AR Path="/773F8EB44BB67726" Ref="U34"  Part="1" 
AR Path="/94BB67726" Ref="U"  Part="1" 
AR Path="/23C9F04BB67726" Ref="U34"  Part="1" 
AR Path="/FFFFFFF04BB67726" Ref="U34"  Part="1" 
AR Path="/7E4188DA4BB67726" Ref="U34"  Part="1" 
AR Path="/8D384BB67726" Ref="U34"  Part="1" 
AR Path="/773F65F14BB67726" Ref="U34"  Part="1" 
AR Path="/24BB67726" Ref="U34"  Part="1" 
AR Path="/23C6504BB67726" Ref="U34"  Part="1" 
AR Path="/D058E04BB67726" Ref="U34"  Part="1" 
AR Path="/3C64BB67726" Ref="U34"  Part="1" 
AR Path="/23CBC44BB67726" Ref="U34"  Part="1" 
AR Path="/2F4A4BB67726" Ref="U34"  Part="1" 
AR Path="/23D9004BB67726" Ref="U34"  Part="1" 
AR Path="/64BB67726" Ref="U34"  Part="1" 
AR Path="/6FE934344BB67726" Ref="U34"  Part="1" 
AR Path="/6FE934E34BB67726" Ref="U34"  Part="1" 
AR Path="/2824BB67726" Ref="U34"  Part="1" 
AR Path="/69549BC04BB67726" Ref="U34"  Part="1" 
AR Path="/D1C3384BB67726" Ref="U34"  Part="1" 
F 0 "U34" H 15600 17750 60  0000 C CNN
F 1 "74LS74" H 15750 17155 60  0000 C CNN
F 2 "14dip300" H 15750 17255 60  0001 C CNN
F 3 "" H 15450 17450 60  0001 C CNN
	1    15450 17450
	-1   0    0    -1  
$EndComp
$Comp
L 74LS74 U?
U 1 1 4BB676DC
P 16650 19100
AR Path="/2300384BB676DC" Ref="U?"  Part="1" 
AR Path="/4BB676DC" Ref="U33"  Part="1" 
AR Path="/FFFFFFFF4BB676DC" Ref="U33"  Part="1" 
AR Path="/755D912A4BB676DC" Ref="U?"  Part="1" 
AR Path="/23D8044BB676DC" Ref="U?"  Part="1" 
AR Path="/2606084BB676DC" Ref="U33"  Part="1" 
AR Path="/23D83C4BB676DC" Ref="U33"  Part="1" 
AR Path="/47907FA4BB676DC" Ref="U33"  Part="1" 
AR Path="/23C34C4BB676DC" Ref="U33"  Part="1" 
AR Path="/14BB676DC" Ref="U33"  Part="1" 
AR Path="/23BC884BB676DC" Ref="U33"  Part="1" 
AR Path="/773F8EB44BB676DC" Ref="U33"  Part="1" 
AR Path="/94BB676DC" Ref="U"  Part="1" 
AR Path="/23C9F04BB676DC" Ref="U33"  Part="1" 
AR Path="/FFFFFFF04BB676DC" Ref="U33"  Part="1" 
AR Path="/7E4188DA4BB676DC" Ref="U33"  Part="1" 
AR Path="/8D384BB676DC" Ref="U33"  Part="1" 
AR Path="/773F65F14BB676DC" Ref="U33"  Part="1" 
AR Path="/24BB676DC" Ref="U33"  Part="1" 
AR Path="/23C6504BB676DC" Ref="U33"  Part="1" 
AR Path="/D058E04BB676DC" Ref="U33"  Part="1" 
AR Path="/3C64BB676DC" Ref="U33"  Part="1" 
AR Path="/23CBC44BB676DC" Ref="U33"  Part="1" 
AR Path="/2F4A4BB676DC" Ref="U33"  Part="1" 
AR Path="/23D9004BB676DC" Ref="U33"  Part="1" 
AR Path="/64BB676DC" Ref="U33"  Part="1" 
AR Path="/6FE934344BB676DC" Ref="U33"  Part="1" 
AR Path="/6FE934E34BB676DC" Ref="U33"  Part="1" 
AR Path="/2824BB676DC" Ref="U33"  Part="1" 
AR Path="/69549BC04BB676DC" Ref="U33"  Part="1" 
AR Path="/D1C3384BB676DC" Ref="U33"  Part="1" 
F 0 "U33" H 16800 19400 60  0000 C CNN
F 1 "74LS74" H 16950 18805 60  0000 C CNN
F 2 "14dip300" H 16950 18905 60  0001 C CNN
F 3 "" H 16650 19100 60  0001 C CNN
	1    16650 19100
	1    0    0    -1  
$EndComp
$Comp
L 74LS74 U?
U 2 1 4BB675D7
P 16650 22350
AR Path="/2300384BB675D7" Ref="U?"  Part="1" 
AR Path="/4BB675D7" Ref="U32"  Part="2" 
AR Path="/FFFFFFFF4BB675D7" Ref="U32"  Part="1" 
AR Path="/755D912A4BB675D7" Ref="U?"  Part="1" 
AR Path="/23D8044BB675D7" Ref="U?"  Part="1" 
AR Path="/2917C04BB675D7" Ref="U?"  Part="1" 
AR Path="/61408744BB675D7" Ref="U"  Part="2" 
AR Path="/23D83C4BB675D7" Ref="U32"  Part="2" 
AR Path="/47907FA4BB675D7" Ref="U32"  Part="2" 
AR Path="/23C34C4BB675D7" Ref="U32"  Part="2" 
AR Path="/14BB675D7" Ref="U32"  Part="2" 
AR Path="/23BC884BB675D7" Ref="U32"  Part="2" 
AR Path="/773F8EB44BB675D7" Ref="U32"  Part="2" 
AR Path="/94BB675D7" Ref="U"  Part="2" 
AR Path="/23C9F04BB675D7" Ref="U32"  Part="2" 
AR Path="/FFFFFFF04BB675D7" Ref="U32"  Part="2" 
AR Path="/7E4188DA4BB675D7" Ref="U32"  Part="2" 
AR Path="/8D384BB675D7" Ref="U32"  Part="2" 
AR Path="/24BB675D7" Ref="U32"  Part="2" 
AR Path="/773F65F14BB675D7" Ref="U32"  Part="2" 
AR Path="/23C6504BB675D7" Ref="U32"  Part="2" 
AR Path="/D058E04BB675D7" Ref="U32"  Part="2" 
AR Path="/3C64BB675D7" Ref="U32"  Part="2" 
AR Path="/23CBC44BB675D7" Ref="U32"  Part="2" 
AR Path="/2F4A4BB675D7" Ref="U32"  Part="2" 
AR Path="/23D9004BB675D7" Ref="U32"  Part="2" 
AR Path="/64BB675D7" Ref="U32"  Part="2" 
AR Path="/6FE934344BB675D7" Ref="U32"  Part="2" 
AR Path="/6FE934E34BB675D7" Ref="U32"  Part="2" 
AR Path="/2824BB675D7" Ref="U32"  Part="2" 
AR Path="/69549BC04BB675D7" Ref="U32"  Part="2" 
AR Path="/D1C3384BB675D7" Ref="U32"  Part="2" 
AR Path="/1E4BB675D7" Ref="U32"  Part="2" 
AR Path="/6FF405304BB675D7" Ref="U32"  Part="2" 
F 0 "U32" H 16800 22650 60  0000 C CNN
F 1 "74LS74" H 16950 22055 60  0000 C CNN
F 2 "14dip300" H 16950 22155 60  0001 C CNN
F 3 "" H 16650 22350 60  0001 C CNN
	2    16650 22350
	1    0    0    -1  
$EndComp
$Comp
L 74LS04 U?
U 4 1 4BB674F9
P 15350 22350
AR Path="/384BB674F9" Ref="U?"  Part="1" 
AR Path="/4BB674F9" Ref="U36"  Part="4" 
AR Path="/C00001354BB674F9" Ref="U"  Part="4" 
AR Path="/FFFFFFFF4BB674F9" Ref="U36"  Part="4" 
AR Path="/755D912A4BB674F9" Ref="U?"  Part="4" 
AR Path="/23D8044BB674F9" Ref="U?"  Part="4" 
AR Path="/2606084BB674F9" Ref="U36"  Part="4" 
AR Path="/23D83C4BB674F9" Ref="U36"  Part="4" 
AR Path="/47907FA4BB674F9" Ref="U36"  Part="4" 
AR Path="/23C34C4BB674F9" Ref="U36"  Part="4" 
AR Path="/14BB674F9" Ref="U36"  Part="4" 
AR Path="/23BC884BB674F9" Ref="U36"  Part="4" 
AR Path="/773F8EB44BB674F9" Ref="U36"  Part="4" 
AR Path="/94BB674F9" Ref="U"  Part="4" 
AR Path="/23C9F04BB674F9" Ref="U36"  Part="4" 
AR Path="/FFFFFFF04BB674F9" Ref="U36"  Part="4" 
AR Path="/7E4188DA4BB674F9" Ref="U36"  Part="4" 
AR Path="/8D384BB674F9" Ref="U36"  Part="4" 
AR Path="/773F65F14BB674F9" Ref="U36"  Part="4" 
AR Path="/24BB674F9" Ref="U36"  Part="4" 
AR Path="/23C6504BB674F9" Ref="U36"  Part="4" 
AR Path="/D058E04BB674F9" Ref="U36"  Part="4" 
AR Path="/3C64BB674F9" Ref="U36"  Part="4" 
AR Path="/23CBC44BB674F9" Ref="U36"  Part="4" 
AR Path="/2F4A4BB674F9" Ref="U36"  Part="4" 
AR Path="/23D9004BB674F9" Ref="U36"  Part="4" 
AR Path="/64BB674F9" Ref="U36"  Part="4" 
AR Path="/6FE934344BB674F9" Ref="U36"  Part="4" 
AR Path="/6FE934E34BB674F9" Ref="U36"  Part="4" 
AR Path="/2824BB674F9" Ref="U36"  Part="4" 
AR Path="/69549BC04BB674F9" Ref="U36"  Part="4" 
AR Path="/D1C3384BB674F9" Ref="U36"  Part="4" 
AR Path="/6FF405304BB674F9" Ref="U36"  Part="4" 
F 0 "U36" H 15545 22465 60  0000 C CNN
F 1 "74LS04" H 15540 22225 60  0000 C CNN
F 2 "14dip300" H 15540 22325 60  0001 C CNN
F 3 "" H 15350 22350 60  0001 C CNN
	4    15350 22350
	1    0    0    -1  
$EndComp
$Comp
L 74LS04 U?
U 5 1 4BB674F5
P 14450 22350
AR Path="/384BB674F5" Ref="U?"  Part="1" 
AR Path="/4BB674F5" Ref="U36"  Part="5" 
AR Path="/C00001354BB674F5" Ref="U"  Part="5" 
AR Path="/FFFFFFFF4BB674F5" Ref="U36"  Part="5" 
AR Path="/755D912A4BB674F5" Ref="U?"  Part="5" 
AR Path="/7E428DAC4BB674F5" Ref="U?"  Part="5" 
AR Path="/2606084BB674F5" Ref="U36"  Part="5" 
AR Path="/23D8044BB674F5" Ref="U336"  Part="5" 
AR Path="/23D83C4BB674F5" Ref="U36"  Part="5" 
AR Path="/47907FA4BB674F5" Ref="U36"  Part="5" 
AR Path="/23C34C4BB674F5" Ref="U36"  Part="5" 
AR Path="/14BB674F5" Ref="U36"  Part="5" 
AR Path="/23BC884BB674F5" Ref="U36"  Part="5" 
AR Path="/773F8EB44BB674F5" Ref="U36"  Part="5" 
AR Path="/94BB674F5" Ref="U"  Part="5" 
AR Path="/23C9F04BB674F5" Ref="U36"  Part="5" 
AR Path="/FFFFFFF04BB674F5" Ref="U36"  Part="5" 
AR Path="/7E4188DA4BB674F5" Ref="U36"  Part="5" 
AR Path="/8D384BB674F5" Ref="U36"  Part="5" 
AR Path="/773F65F14BB674F5" Ref="U36"  Part="5" 
AR Path="/24BB674F5" Ref="U36"  Part="5" 
AR Path="/23C6504BB674F5" Ref="U36"  Part="5" 
AR Path="/D058E04BB674F5" Ref="U36"  Part="5" 
AR Path="/3C64BB674F5" Ref="U36"  Part="5" 
AR Path="/23CBC44BB674F5" Ref="U36"  Part="5" 
AR Path="/2F4A4BB674F5" Ref="U36"  Part="5" 
AR Path="/23D9004BB674F5" Ref="U36"  Part="5" 
AR Path="/64BB674F5" Ref="U36"  Part="5" 
AR Path="/6FE934344BB674F5" Ref="U36"  Part="5" 
AR Path="/6FE934E34BB674F5" Ref="U36"  Part="5" 
AR Path="/2824BB674F5" Ref="U36"  Part="5" 
AR Path="/69549BC04BB674F5" Ref="U36"  Part="5" 
AR Path="/D1C3384BB674F5" Ref="U36"  Part="5" 
F 0 "U36" H 14645 22465 60  0000 C CNN
F 1 "74LS04" H 14640 22225 60  0000 C CNN
F 2 "14dip300" H 14640 22325 60  0001 C CNN
F 3 "" H 14450 22350 60  0001 C CNN
	5    14450 22350
	1    0    0    -1  
$EndComp
$Comp
L 74LS04 U?
U 6 1 4BB674F1
P 13050 22300
AR Path="/2300384BB674F1" Ref="U?"  Part="1" 
AR Path="/4BB674F1" Ref="U36"  Part="6" 
AR Path="/370031324BB674F1" Ref="U?"  Part="1" 
AR Path="/C00001354BB674F1" Ref="U"  Part="6" 
AR Path="/FFFFFFFF4BB674F1" Ref="U36"  Part="6" 
AR Path="/755D912A4BB674F1" Ref="U?"  Part="6" 
AR Path="/7E428DAC4BB674F1" Ref="U?"  Part="6" 
AR Path="/2911B04BB674F1" Ref="U?"  Part="6" 
AR Path="/5FC08184BB674F1" Ref="U"  Part="6" 
AR Path="/23D83C4BB674F1" Ref="U36"  Part="6" 
AR Path="/47907FA4BB674F1" Ref="U36"  Part="6" 
AR Path="/23C34C4BB674F1" Ref="U36"  Part="6" 
AR Path="/14BB674F1" Ref="U36"  Part="6" 
AR Path="/23BC884BB674F1" Ref="U36"  Part="6" 
AR Path="/773F8EB44BB674F1" Ref="U36"  Part="6" 
AR Path="/94BB674F1" Ref="U"  Part="6" 
AR Path="/23C9F04BB674F1" Ref="U36"  Part="6" 
AR Path="/FFFFFFF04BB674F1" Ref="U36"  Part="6" 
AR Path="/7E4188DA4BB674F1" Ref="U36"  Part="6" 
AR Path="/8D384BB674F1" Ref="U36"  Part="6" 
AR Path="/773F65F14BB674F1" Ref="U36"  Part="6" 
AR Path="/24BB674F1" Ref="U36"  Part="6" 
AR Path="/23C6504BB674F1" Ref="U36"  Part="6" 
AR Path="/D058E04BB674F1" Ref="U36"  Part="6" 
AR Path="/3C64BB674F1" Ref="U36"  Part="6" 
AR Path="/23CBC44BB674F1" Ref="U36"  Part="6" 
AR Path="/2F4A4BB674F1" Ref="U36"  Part="6" 
AR Path="/23D9004BB674F1" Ref="U36"  Part="6" 
AR Path="/64BB674F1" Ref="U36"  Part="6" 
AR Path="/6FE934344BB674F1" Ref="U36"  Part="6" 
AR Path="/6FE934E34BB674F1" Ref="U36"  Part="6" 
AR Path="/2824BB674F1" Ref="U36"  Part="6" 
AR Path="/69549BC04BB674F1" Ref="U36"  Part="6" 
AR Path="/6FF405C44BB674F1" Ref="U36"  Part="6" 
AR Path="/2653D04BB674F1" Ref="U36"  Part="6" 
AR Path="/D1C3384BB674F1" Ref="U36"  Part="6" 
F 0 "U36" H 13245 22415 60  0000 C CNN
F 1 "74LS04" H 13240 22175 60  0000 C CNN
F 2 "14dip300" H 13240 22275 60  0001 C CNN
F 3 "" H 13050 22300 60  0001 C CNN
	6    13050 22300
	1    0    0    -1  
$EndComp
Text Label 9050 22100 0    60   ~ 0
JUMP_ENABLE
$Comp
L 74LS27 U?
U 2 1 4BB67362
P 10800 22450
AR Path="/2300384BB67362" Ref="U?"  Part="1" 
AR Path="/4BB67362" Ref="U41"  Part="2" 
AR Path="/31324BB67362" Ref="U?"  Part="1" 
AR Path="/77F1609B4BB67362" Ref="U"  Part="2" 
AR Path="/FFFFFFFF4BB67362" Ref="U41"  Part="2" 
AR Path="/755D912A4BB67362" Ref="U?"  Part="2" 
AR Path="/7E428DAC4BB67362" Ref="U?"  Part="2" 
AR Path="/2606084BB67362" Ref="U41"  Part="2" 
AR Path="/23D83C4BB67362" Ref="U41"  Part="2" 
AR Path="/47907FA4BB67362" Ref="U41"  Part="2" 
AR Path="/23C34C4BB67362" Ref="U41"  Part="2" 
AR Path="/14BB67362" Ref="U41"  Part="2" 
AR Path="/23BC884BB67362" Ref="U41"  Part="2" 
AR Path="/773F8EB44BB67362" Ref="U41"  Part="2" 
AR Path="/94BB67362" Ref="U"  Part="2" 
AR Path="/23C9F04BB67362" Ref="U41"  Part="2" 
AR Path="/FFFFFFF04BB67362" Ref="U41"  Part="2" 
AR Path="/7E4188DA4BB67362" Ref="U41"  Part="2" 
AR Path="/8D384BB67362" Ref="U41"  Part="2" 
AR Path="/24BB67362" Ref="U41"  Part="2" 
AR Path="/773F65F14BB67362" Ref="U41"  Part="2" 
AR Path="/23C6504BB67362" Ref="U41"  Part="2" 
AR Path="/D058E04BB67362" Ref="U41"  Part="2" 
AR Path="/3C64BB67362" Ref="U41"  Part="2" 
AR Path="/23CBC44BB67362" Ref="U41"  Part="2" 
AR Path="/2F4A4BB67362" Ref="U41"  Part="2" 
AR Path="/23D9004BB67362" Ref="U41"  Part="2" 
AR Path="/64BB67362" Ref="U41"  Part="2" 
AR Path="/6FE934344BB67362" Ref="U41"  Part="2" 
AR Path="/6FE934E34BB67362" Ref="U41"  Part="2" 
AR Path="/2824BB67362" Ref="U41"  Part="2" 
AR Path="/69549BC04BB67362" Ref="U41"  Part="2" 
AR Path="/D1C3384BB67362" Ref="U41"  Part="2" 
F 0 "U41" H 10800 22500 60  0000 C CNN
F 1 "74LS27" H 10800 22400 60  0000 C CNN
F 2 "14dip300" H 10800 22500 60  0001 C CNN
F 3 "" H 10800 22450 60  0001 C CNN
	2    10800 22450
	1    0    0    -1  
$EndComp
$Comp
L 74LS32 U?
U 4 1 4BB6735B
P 9600 22450
AR Path="/2300384BB6735B" Ref="U?"  Part="1" 
AR Path="/4BB6735B" Ref="U35"  Part="4" 
AR Path="/FFFFFFFF4BB6735B" Ref="U35"  Part="1" 
AR Path="/755D912A4BB6735B" Ref="U?"  Part="1" 
AR Path="/31324BB6735B" Ref="U?"  Part="1" 
AR Path="/29E3404BB6735B" Ref="U?"  Part="1" 
AR Path="/61008744BB6735B" Ref="U"  Part="4" 
AR Path="/23D83C4BB6735B" Ref="U35"  Part="4" 
AR Path="/47907FA4BB6735B" Ref="U35"  Part="4" 
AR Path="/23C34C4BB6735B" Ref="U35"  Part="4" 
AR Path="/14BB6735B" Ref="U35"  Part="4" 
AR Path="/23BC884BB6735B" Ref="U35"  Part="4" 
AR Path="/773F8EB44BB6735B" Ref="U35"  Part="4" 
AR Path="/94BB6735B" Ref="U"  Part="4" 
AR Path="/23C9F04BB6735B" Ref="U35"  Part="4" 
AR Path="/FFFFFFF04BB6735B" Ref="U35"  Part="4" 
AR Path="/7E4188DA4BB6735B" Ref="U35"  Part="4" 
AR Path="/8D384BB6735B" Ref="U35"  Part="4" 
AR Path="/773F65F14BB6735B" Ref="U35"  Part="4" 
AR Path="/24BB6735B" Ref="U35"  Part="4" 
AR Path="/23C6504BB6735B" Ref="U35"  Part="4" 
AR Path="/D058E04BB6735B" Ref="U35"  Part="4" 
AR Path="/3C64BB6735B" Ref="U35"  Part="4" 
AR Path="/23CBC44BB6735B" Ref="U35"  Part="4" 
AR Path="/2F4A4BB6735B" Ref="U35"  Part="4" 
AR Path="/23D9004BB6735B" Ref="U35"  Part="4" 
AR Path="/64BB6735B" Ref="U35"  Part="4" 
AR Path="/6FE934344BB6735B" Ref="U35"  Part="4" 
AR Path="/6FE934E34BB6735B" Ref="U35"  Part="4" 
AR Path="/2824BB6735B" Ref="U35"  Part="4" 
AR Path="/69549BC04BB6735B" Ref="U35"  Part="4" 
AR Path="/D1C3384BB6735B" Ref="U35"  Part="4" 
F 0 "U35" H 9600 22500 60  0000 C CNN
F 1 "74LS32" H 9600 22400 60  0000 C CNN
F 2 "14dip300" H 9600 22500 60  0001 C CNN
F 3 "" H 9600 22450 60  0001 C CNN
	4    9600 22450
	1    0    0    -1  
$EndComp
NoConn ~ 10100 19600
$Comp
L 74LS74 U?
U 1 1 4BB6719A
P 9500 19400
AR Path="/2300384BB6719A" Ref="U?"  Part="1" 
AR Path="/4BB6719A" Ref="U32"  Part="1" 
AR Path="/FFFFFFFF4BB6719A" Ref="U32"  Part="1" 
AR Path="/755D912A4BB6719A" Ref="U?"  Part="1" 
AR Path="/23D8044BB6719A" Ref="U?32"  Part="1" 
AR Path="/2606084BB6719A" Ref="U32"  Part="1" 
AR Path="/23D83C4BB6719A" Ref="U32"  Part="1" 
AR Path="/47907FA4BB6719A" Ref="U32"  Part="1" 
AR Path="/23C34C4BB6719A" Ref="U32"  Part="1" 
AR Path="/14BB6719A" Ref="U32"  Part="1" 
AR Path="/23BC884BB6719A" Ref="U32"  Part="1" 
AR Path="/773F8EB44BB6719A" Ref="U32"  Part="1" 
AR Path="/94BB6719A" Ref="U"  Part="1" 
AR Path="/23C9F04BB6719A" Ref="U32"  Part="1" 
AR Path="/FFFFFFF04BB6719A" Ref="U32"  Part="1" 
AR Path="/7E4188DA4BB6719A" Ref="U32"  Part="1" 
AR Path="/8D384BB6719A" Ref="U32"  Part="1" 
AR Path="/773F65F14BB6719A" Ref="U32"  Part="1" 
AR Path="/24BB6719A" Ref="U32"  Part="1" 
AR Path="/23C6504BB6719A" Ref="U32"  Part="1" 
AR Path="/D058E04BB6719A" Ref="U32"  Part="1" 
AR Path="/3C64BB6719A" Ref="U32"  Part="1" 
AR Path="/23CBC44BB6719A" Ref="U32"  Part="1" 
AR Path="/2F4A4BB6719A" Ref="U32"  Part="1" 
AR Path="/23D9004BB6719A" Ref="U32"  Part="1" 
AR Path="/64BB6719A" Ref="U32"  Part="1" 
AR Path="/6FE934344BB6719A" Ref="U32"  Part="1" 
AR Path="/6FE934E34BB6719A" Ref="U32"  Part="1" 
AR Path="/2824BB6719A" Ref="U32"  Part="1" 
AR Path="/69549BC04BB6719A" Ref="U32"  Part="1" 
AR Path="/D1C3384BB6719A" Ref="U32"  Part="1" 
F 0 "U32" H 9650 19700 60  0000 C CNN
F 1 "74LS74" H 9800 19105 60  0000 C CNN
F 2 "14dip300" H 9800 19205 60  0001 C CNN
F 3 "" H 9500 19400 60  0001 C CNN
	1    9500 19400
	1    0    0    -1  
$EndComp
NoConn ~ 9400 18100
Text Label 9450 18000 0    60   ~ 0
sINTA
Text Label 9450 17900 0    60   ~ 0
sM1
Text Label 9450 17800 0    60   ~ 0
sWO*
Text Label 9450 17700 0    60   ~ 0
sMEMR
Text Label 9450 17600 0    60   ~ 0
sINP
Text Label 9450 17500 0    60   ~ 0
sOUT
Text Label 9450 17400 0    60   ~ 0
sHLTA
NoConn ~ 8000 18100
NoConn ~ 7600 23700
$Comp
L 74LS74 U?
U 2 1 4BB66F8B
P 7000 23900
AR Path="/384BB66F8B" Ref="U?"  Part="1" 
AR Path="/4BB66F8B" Ref="U30"  Part="2" 
AR Path="/FFFFFFFF4BB66F8B" Ref="U30"  Part="2" 
AR Path="/755D912A4BB66F8B" Ref="U?"  Part="2" 
AR Path="/7E428DAC4BB66F8B" Ref="U?"  Part="2" 
AR Path="/2AAFF04BB66F8B" Ref="U?"  Part="2" 
AR Path="/5FE08184BB66F8B" Ref="U"  Part="2" 
AR Path="/23D83C4BB66F8B" Ref="U30"  Part="2" 
AR Path="/47907FA4BB66F8B" Ref="U30"  Part="2" 
AR Path="/23C34C4BB66F8B" Ref="U30"  Part="2" 
AR Path="/14BB66F8B" Ref="U30"  Part="2" 
AR Path="/23BC884BB66F8B" Ref="U30"  Part="2" 
AR Path="/773F8EB44BB66F8B" Ref="U30"  Part="2" 
AR Path="/94BB66F8B" Ref="U"  Part="2" 
AR Path="/23C9F04BB66F8B" Ref="U30"  Part="2" 
AR Path="/FFFFFFF04BB66F8B" Ref="U30"  Part="2" 
AR Path="/7E4188DA4BB66F8B" Ref="U30"  Part="2" 
AR Path="/8D384BB66F8B" Ref="U30"  Part="2" 
AR Path="/24BB66F8B" Ref="U30"  Part="2" 
AR Path="/773F65F14BB66F8B" Ref="U30"  Part="2" 
AR Path="/23C6504BB66F8B" Ref="U30"  Part="2" 
AR Path="/373041344BB66F8B" Ref="U30"  Part="2" 
AR Path="/D058E04BB66F8B" Ref="U30"  Part="2" 
AR Path="/3C64BB66F8B" Ref="U30"  Part="2" 
AR Path="/23CBC44BB66F8B" Ref="U30"  Part="2" 
AR Path="/2F4A4BB66F8B" Ref="U30"  Part="2" 
AR Path="/23D9004BB66F8B" Ref="U30"  Part="2" 
AR Path="/64BB66F8B" Ref="U30"  Part="2" 
AR Path="/6FE934344BB66F8B" Ref="U30"  Part="2" 
AR Path="/6FE934E34BB66F8B" Ref="U30"  Part="2" 
AR Path="/2824BB66F8B" Ref="U30"  Part="2" 
AR Path="/69549BC04BB66F8B" Ref="U30"  Part="2" 
AR Path="/D1C3384BB66F8B" Ref="U30"  Part="2" 
F 0 "U30" H 7150 24200 60  0000 C CNN
F 1 "74LS74" H 7300 23605 60  0000 C CNN
F 2 "14dip300" H 7300 23705 60  0001 C CNN
F 3 "" H 7000 23900 60  0001 C CNN
	2    7000 23900
	1    0    0    -1  
$EndComp
NoConn ~ 4900 23100
$Comp
L 74LS74 U?
U 1 1 4BB66F14
P 4300 22900
AR Path="/A200384BB66F14" Ref="U?"  Part="1" 
AR Path="/4BB66F14" Ref="U30"  Part="1" 
AR Path="/6FF405304BB66F14" Ref="U?"  Part="1" 
AR Path="/34BB66F14" Ref="U?"  Part="1" 
AR Path="/364638424BB66F14" Ref="U?"  Part="1" 
AR Path="/FFFFFFFF4BB66F14" Ref="U30"  Part="1" 
AR Path="/755D912A4BB66F14" Ref="U?"  Part="1" 
AR Path="/23D8044BB66F14" Ref="U?"  Part="1" 
AR Path="/2E26384BB66F14" Ref="U?"  Part="1" 
AR Path="/60C08744BB66F14" Ref="U"  Part="1" 
AR Path="/23D83C4BB66F14" Ref="U30"  Part="1" 
AR Path="/47907FA4BB66F14" Ref="U30"  Part="1" 
AR Path="/23C34C4BB66F14" Ref="U30"  Part="1" 
AR Path="/14BB66F14" Ref="U30"  Part="1" 
AR Path="/23BC884BB66F14" Ref="U30"  Part="1" 
AR Path="/773F8EB44BB66F14" Ref="U30"  Part="1" 
AR Path="/94BB66F14" Ref="U"  Part="1" 
AR Path="/23C9F04BB66F14" Ref="U30"  Part="1" 
AR Path="/FFFFFFF04BB66F14" Ref="U30"  Part="1" 
AR Path="/7E4188DA4BB66F14" Ref="U30"  Part="1" 
AR Path="/8D384BB66F14" Ref="U30"  Part="1" 
AR Path="/773F65F14BB66F14" Ref="U30"  Part="1" 
AR Path="/24BB66F14" Ref="U30"  Part="1" 
AR Path="/23C6504BB66F14" Ref="U30"  Part="1" 
AR Path="/373041344BB66F14" Ref="U30"  Part="1" 
AR Path="/D058E04BB66F14" Ref="U30"  Part="1" 
AR Path="/343836364BB66F14" Ref="U?"  Part="1" 
AR Path="/3C64BB66F14" Ref="U?"  Part="1" 
AR Path="/2606084BB66F14" Ref="U30"  Part="1" 
AR Path="/23CBC44BB66F14" Ref="U30"  Part="1" 
AR Path="/2F4A4BB66F14" Ref="U30"  Part="1" 
AR Path="/23D9004BB66F14" Ref="U30"  Part="1" 
AR Path="/64BB66F14" Ref="U30"  Part="1" 
AR Path="/6FE934344BB66F14" Ref="U30"  Part="1" 
AR Path="/6FE934E34BB66F14" Ref="U30"  Part="1" 
AR Path="/2824BB66F14" Ref="U30"  Part="1" 
AR Path="/69549BC04BB66F14" Ref="U30"  Part="1" 
AR Path="/D1C3384BB66F14" Ref="U30"  Part="1" 
F 0 "U30" H 4450 23200 60  0000 C CNN
F 1 "74LS74" H 4600 22605 60  0000 C CNN
F 2 "14dip300" H 4600 22705 60  0001 C CNN
F 3 "" H 4300 22900 60  0001 C CNN
	1    4300 22900
	1    0    0    -1  
$EndComp
$Comp
L 74LS27 U?
U 1 1 4BB66EEC
P 6100 22450
AR Path="/A200384BB66EEC" Ref="U?"  Part="1" 
AR Path="/4BB66EEC" Ref="U41"  Part="1" 
AR Path="/FFFFFFFF4BB66EEC" Ref="U41"  Part="1" 
AR Path="/755D912A4BB66EEC" Ref="U?"  Part="1" 
AR Path="/7E428DAC4BB66EEC" Ref="U?"  Part="1" 
AR Path="/2606084BB66EEC" Ref="U41"  Part="1" 
AR Path="/23D83C4BB66EEC" Ref="U41"  Part="1" 
AR Path="/47907FA4BB66EEC" Ref="U41"  Part="1" 
AR Path="/23C34C4BB66EEC" Ref="U41"  Part="1" 
AR Path="/14BB66EEC" Ref="U41"  Part="1" 
AR Path="/23BC884BB66EEC" Ref="U41"  Part="1" 
AR Path="/773F8EB44BB66EEC" Ref="U41"  Part="1" 
AR Path="/94BB66EEC" Ref="U"  Part="1" 
AR Path="/23C9F04BB66EEC" Ref="U41"  Part="1" 
AR Path="/FFFFFFF04BB66EEC" Ref="U41"  Part="1" 
AR Path="/7E4188DA4BB66EEC" Ref="U41"  Part="1" 
AR Path="/8D384BB66EEC" Ref="U41"  Part="1" 
AR Path="/773F65F14BB66EEC" Ref="U41"  Part="1" 
AR Path="/24BB66EEC" Ref="U41"  Part="1" 
AR Path="/23C6504BB66EEC" Ref="U41"  Part="1" 
AR Path="/D058E04BB66EEC" Ref="U41"  Part="1" 
AR Path="/3C64BB66EEC" Ref="U41"  Part="1" 
AR Path="/23CBC44BB66EEC" Ref="U41"  Part="1" 
AR Path="/2F4A4BB66EEC" Ref="U41"  Part="1" 
AR Path="/23D9004BB66EEC" Ref="U41"  Part="1" 
AR Path="/64BB66EEC" Ref="U41"  Part="1" 
AR Path="/6FE934344BB66EEC" Ref="U41"  Part="1" 
AR Path="/6FE934E34BB66EEC" Ref="U41"  Part="1" 
AR Path="/2824BB66EEC" Ref="U41"  Part="1" 
AR Path="/69549BC04BB66EEC" Ref="U41"  Part="1" 
AR Path="/D1C3384BB66EEC" Ref="U41"  Part="1" 
F 0 "U41" H 6100 22500 60  0000 C CNN
F 1 "74LS27" H 6100 22400 60  0000 C CNN
F 2 "14dip300" H 6100 22500 60  0001 C CNN
F 3 "" H 6100 22450 60  0001 C CNN
	1    6100 22450
	1    0    0    -1  
$EndComp
Text Label 6500 19900 0    60   ~ 0
MEM_RD
$Comp
L 74LS04 U?
U 6 1 4BB66931
P 1900 19850
AR Path="/2300384BB66931" Ref="U?"  Part="1" 
AR Path="/4BB66931" Ref="U38"  Part="6" 
AR Path="/FFFFFFFF4BB66931" Ref="U38"  Part="1" 
AR Path="/755D912A4BB66931" Ref="U?"  Part="1" 
AR Path="/7E428DAC4BB66931" Ref="U?"  Part="1" 
AR Path="/2983F84BB66931" Ref="U?"  Part="1" 
AR Path="/5F408184BB66931" Ref="U"  Part="6" 
AR Path="/23D83C4BB66931" Ref="U38"  Part="6" 
AR Path="/47907FA4BB66931" Ref="U38"  Part="6" 
AR Path="/23C34C4BB66931" Ref="U38"  Part="6" 
AR Path="/14BB66931" Ref="U38"  Part="6" 
AR Path="/23BC884BB66931" Ref="U38"  Part="6" 
AR Path="/773F8EB44BB66931" Ref="U38"  Part="6" 
AR Path="/94BB66931" Ref="U"  Part="6" 
AR Path="/23C9F04BB66931" Ref="U38"  Part="6" 
AR Path="/FFFFFFF04BB66931" Ref="U38"  Part="6" 
AR Path="/7E4188DA4BB66931" Ref="U38"  Part="6" 
AR Path="/8D384BB66931" Ref="U38"  Part="6" 
AR Path="/24BB66931" Ref="U38"  Part="6" 
AR Path="/773F65F14BB66931" Ref="U38"  Part="6" 
AR Path="/23C6504BB66931" Ref="U38"  Part="6" 
AR Path="/D058E04BB66931" Ref="U38"  Part="6" 
AR Path="/3C64BB66931" Ref="U38"  Part="6" 
AR Path="/23CBC44BB66931" Ref="U38"  Part="6" 
AR Path="/2F4A4BB66931" Ref="U38"  Part="6" 
AR Path="/23D9004BB66931" Ref="U38"  Part="6" 
AR Path="/64BB66931" Ref="U38"  Part="6" 
AR Path="/6FE934344BB66931" Ref="U38"  Part="6" 
AR Path="/6FE934E34BB66931" Ref="U38"  Part="6" 
AR Path="/2824BB66931" Ref="U38"  Part="6" 
AR Path="/69549BC04BB66931" Ref="U38"  Part="6" 
AR Path="/D1C3384BB66931" Ref="U38"  Part="6" 
F 0 "U38" H 2095 19965 60  0000 C CNN
F 1 "74F04" H 2090 19725 60  0000 C CNN
F 2 "14dip300" H 2090 19825 60  0001 C CNN
F 3 "" H 1900 19850 60  0001 C CNN
	6    1900 19850
	1    0    0    -1  
$EndComp
$Comp
L 74LS00 U?
U 2 1 4BB668E8
P 4550 21700
AR Path="/2300384BB668E8" Ref="U?"  Part="1" 
AR Path="/4BB668E8" Ref="U40"  Part="2" 
AR Path="/363845384BB668E8" Ref="U?"  Part="1" 
AR Path="/2600004BB668E8" Ref="U"  Part="2" 
AR Path="/FFFFFFFF4BB668E8" Ref="U40"  Part="2" 
AR Path="/755D912A4BB668E8" Ref="U?"  Part="2" 
AR Path="/23D8044BB668E8" Ref="U?"  Part="2" 
AR Path="/2606084BB668E8" Ref="U40"  Part="2" 
AR Path="/23D83C4BB668E8" Ref="U40"  Part="2" 
AR Path="/47907FA4BB668E8" Ref="U40"  Part="2" 
AR Path="/23C34C4BB668E8" Ref="U40"  Part="2" 
AR Path="/14BB668E8" Ref="U40"  Part="2" 
AR Path="/23BC884BB668E8" Ref="U40"  Part="2" 
AR Path="/773F8EB44BB668E8" Ref="U40"  Part="2" 
AR Path="/94BB668E8" Ref="U"  Part="2" 
AR Path="/23C9F04BB668E8" Ref="U40"  Part="2" 
AR Path="/FFFFFFF04BB668E8" Ref="U40"  Part="2" 
AR Path="/7E4188DA4BB668E8" Ref="U40"  Part="2" 
AR Path="/8D384BB668E8" Ref="U40"  Part="2" 
AR Path="/773F65F14BB668E8" Ref="U40"  Part="2" 
AR Path="/24BB668E8" Ref="U40"  Part="2" 
AR Path="/23C6504BB668E8" Ref="U40"  Part="2" 
AR Path="/D058E04BB668E8" Ref="U40"  Part="2" 
AR Path="/3C64BB668E8" Ref="U40"  Part="2" 
AR Path="/23CBC44BB668E8" Ref="U40"  Part="2" 
AR Path="/2F4A4BB668E8" Ref="U40"  Part="2" 
AR Path="/23D9004BB668E8" Ref="U40"  Part="2" 
AR Path="/64BB668E8" Ref="U40"  Part="2" 
AR Path="/6FE934344BB668E8" Ref="U40"  Part="2" 
AR Path="/6FE934E34BB668E8" Ref="U40"  Part="2" 
AR Path="/2824BB668E8" Ref="U40"  Part="2" 
AR Path="/69549BC04BB668E8" Ref="U40"  Part="2" 
AR Path="/D1C3384BB668E8" Ref="U40"  Part="2" 
F 0 "U40" H 4550 21750 60  0000 C CNN
F 1 "74S00" H 4550 21650 60  0000 C CNN
F 2 "14dip300" H 4550 21750 60  0001 C CNN
F 3 "" H 4550 21700 60  0001 C CNN
	2    4550 21700
	1    0    0    -1  
$EndComp
$Comp
L 74LS00 U?
U 4 1 4BB668BB
P 7200 20800
AR Path="/384BB668BB" Ref="U?"  Part="1" 
AR Path="/4BB668BB" Ref="U40"  Part="4" 
AR Path="/5ADA11784BB668BB" Ref="U?"  Part="1" 
AR Path="/94BB668BB" Ref="U"  Part="4" 
AR Path="/FFFFFFFF4BB668BB" Ref="U40"  Part="4" 
AR Path="/755D912A4BB668BB" Ref="U?"  Part="4" 
AR Path="/23D8044BB668BB" Ref="U?"  Part="4" 
AR Path="/2606084BB668BB" Ref="U40"  Part="4" 
AR Path="/23D83C4BB668BB" Ref="U40"  Part="4" 
AR Path="/47907FA4BB668BB" Ref="U40"  Part="4" 
AR Path="/23C34C4BB668BB" Ref="U40"  Part="4" 
AR Path="/14BB668BB" Ref="U40"  Part="4" 
AR Path="/23BC884BB668BB" Ref="U40"  Part="4" 
AR Path="/773F8EB44BB668BB" Ref="U40"  Part="4" 
AR Path="/23C9F04BB668BB" Ref="U40"  Part="4" 
AR Path="/FFFFFFF04BB668BB" Ref="U40"  Part="4" 
AR Path="/7E4188DA4BB668BB" Ref="U40"  Part="4" 
AR Path="/8D384BB668BB" Ref="U40"  Part="4" 
AR Path="/773F65F14BB668BB" Ref="U40"  Part="4" 
AR Path="/24BB668BB" Ref="U40"  Part="4" 
AR Path="/23C6504BB668BB" Ref="U40"  Part="4" 
AR Path="/D058E04BB668BB" Ref="U40"  Part="4" 
AR Path="/3C64BB668BB" Ref="U40"  Part="4" 
AR Path="/23CBC44BB668BB" Ref="U40"  Part="4" 
AR Path="/2F4A4BB668BB" Ref="U40"  Part="4" 
AR Path="/23D9004BB668BB" Ref="U40"  Part="4" 
AR Path="/64BB668BB" Ref="U40"  Part="4" 
AR Path="/6FE934344BB668BB" Ref="U40"  Part="4" 
AR Path="/6FE934E34BB668BB" Ref="U40"  Part="4" 
AR Path="/2824BB668BB" Ref="U40"  Part="4" 
AR Path="/69549BC04BB668BB" Ref="U40"  Part="4" 
AR Path="/D1C3384BB668BB" Ref="U40"  Part="4" 
F 0 "U40" H 7200 20850 60  0000 C CNN
F 1 "74S00" H 7200 20750 60  0000 C CNN
F 2 "14dip300" H 7200 20850 60  0001 C CNN
F 3 "" H 7200 20800 60  0001 C CNN
	4    7200 20800
	1    0    0    -1  
$EndComp
$Comp
L 74LS00 U?
U 3 1 4BB66891
P 6000 20700
AR Path="/A200384BB66891" Ref="U?"  Part="1" 
AR Path="/4BB66891" Ref="U40"  Part="3" 
AR Path="/FFFFFFFF4BB66891" Ref="U40"  Part="1" 
AR Path="/755D912A4BB66891" Ref="U?"  Part="1" 
AR Path="/23D8044BB66891" Ref="U?"  Part="1" 
AR Path="/2606084BB66891" Ref="U40"  Part="1" 
AR Path="/6FE934E34BB66891" Ref="U40"  Part="1" 
AR Path="/363036324BB66891" Ref="U"  Part="3" 
AR Path="/23D83C4BB66891" Ref="U40"  Part="3" 
AR Path="/47907FA4BB66891" Ref="U40"  Part="3" 
AR Path="/23C34C4BB66891" Ref="U40"  Part="3" 
AR Path="/14BB66891" Ref="U40"  Part="3" 
AR Path="/23BC884BB66891" Ref="U40"  Part="3" 
AR Path="/773F8EB44BB66891" Ref="U40"  Part="3" 
AR Path="/94BB66891" Ref="U"  Part="3" 
AR Path="/23C9F04BB66891" Ref="U40"  Part="3" 
AR Path="/FFFFFFF04BB66891" Ref="U40"  Part="3" 
AR Path="/7E4188DA4BB66891" Ref="U40"  Part="3" 
AR Path="/8D384BB66891" Ref="U40"  Part="3" 
AR Path="/773F65F14BB66891" Ref="U40"  Part="3" 
AR Path="/24BB66891" Ref="U40"  Part="3" 
AR Path="/23C6504BB66891" Ref="U40"  Part="3" 
AR Path="/D058E04BB66891" Ref="U40"  Part="3" 
AR Path="/3C64BB66891" Ref="U40"  Part="3" 
AR Path="/23CBC44BB66891" Ref="U40"  Part="3" 
AR Path="/2F4A4BB66891" Ref="U40"  Part="3" 
AR Path="/23D9004BB66891" Ref="U40"  Part="3" 
AR Path="/64BB66891" Ref="U40"  Part="3" 
AR Path="/6FE934344BB66891" Ref="U40"  Part="3" 
AR Path="/2824BB66891" Ref="U40"  Part="3" 
AR Path="/69549BC04BB66891" Ref="U40"  Part="3" 
AR Path="/D1C3384BB66891" Ref="U40"  Part="3" 
F 0 "U40" H 6000 20750 60  0000 C CNN
F 1 "74S00" H 6000 20650 60  0000 C CNN
F 2 "14dip300" H 6000 20750 60  0001 C CNN
F 3 "" H 6000 20700 60  0001 C CNN
	3    6000 20700
	1    0    0    -1  
$EndComp
$Comp
L 74LS08 U?
U 2 1 4BB6687D
P 4500 20600
AR Path="/2300384BB6687D" Ref="U?"  Part="1" 
AR Path="/4BB6687D" Ref="U39"  Part="2" 
AR Path="/360031324BB6687D" Ref="U?"  Part="1" 
AR Path="/FFFFFFFF4BB6687D" Ref="U39"  Part="2" 
AR Path="/755D912A4BB6687D" Ref="U?"  Part="2" 
AR Path="/2606084BB6687D" Ref="U39"  Part="2" 
AR Path="/23D83C4BB6687D" Ref="U39"  Part="2" 
AR Path="/47907FA4BB6687D" Ref="U39"  Part="2" 
AR Path="/23C34C4BB6687D" Ref="U39"  Part="2" 
AR Path="/14BB6687D" Ref="U39"  Part="2" 
AR Path="/23BC884BB6687D" Ref="U39"  Part="2" 
AR Path="/773F8EB44BB6687D" Ref="U39"  Part="2" 
AR Path="/94BB6687D" Ref="U"  Part="2" 
AR Path="/23C9F04BB6687D" Ref="U39"  Part="2" 
AR Path="/FFFFFFF04BB6687D" Ref="U39"  Part="2" 
AR Path="/7E4188DA4BB6687D" Ref="U39"  Part="2" 
AR Path="/8D384BB6687D" Ref="U39"  Part="2" 
AR Path="/24BB6687D" Ref="U39"  Part="2" 
AR Path="/773F65F14BB6687D" Ref="U39"  Part="2" 
AR Path="/23C6504BB6687D" Ref="U39"  Part="2" 
AR Path="/D058E04BB6687D" Ref="U39"  Part="2" 
AR Path="/3C64BB6687D" Ref="U39"  Part="2" 
AR Path="/23CBC44BB6687D" Ref="U39"  Part="2" 
AR Path="/2F4A4BB6687D" Ref="U39"  Part="2" 
AR Path="/23D9004BB6687D" Ref="U39"  Part="2" 
AR Path="/64BB6687D" Ref="U39"  Part="2" 
AR Path="/6FE934344BB6687D" Ref="U39"  Part="2" 
AR Path="/6FE934E34BB6687D" Ref="U39"  Part="2" 
AR Path="/2824BB6687D" Ref="U39"  Part="2" 
AR Path="/69549BC04BB6687D" Ref="U39"  Part="2" 
AR Path="/D1C3384BB6687D" Ref="U39"  Part="2" 
F 0 "U39" H 4500 20650 60  0000 C CNN
F 1 "74S08" H 4500 20550 60  0000 C CNN
F 2 "14dip300" H 4500 20650 60  0001 C CNN
F 3 "" H 4500 20600 60  0001 C CNN
	2    4500 20600
	1    0    0    -1  
$EndComp
$Comp
L 74LS04 U?
U 5 1 4BB6638A
P 1900 19450
AR Path="/2300384BB6638A" Ref="U?"  Part="1" 
AR Path="/4BB6638A" Ref="U38"  Part="5" 
AR Path="/FFFFFFFF4BB6638A" Ref="U38"  Part="1" 
AR Path="/755D912A4BB6638A" Ref="U?"  Part="1" 
AR Path="/7E428DAC4BB6638A" Ref="U?"  Part="1" 
AR Path="/29BFB04BB6638A" Ref="U?"  Part="1" 
AR Path="/42E08324BB6638A" Ref="U"  Part="5" 
AR Path="/23D83C4BB6638A" Ref="U38"  Part="5" 
AR Path="/47907FA4BB6638A" Ref="U38"  Part="5" 
AR Path="/23C34C4BB6638A" Ref="U38"  Part="5" 
AR Path="/14BB6638A" Ref="U38"  Part="5" 
AR Path="/23BC884BB6638A" Ref="U38"  Part="5" 
AR Path="/773F8EB44BB6638A" Ref="U38"  Part="5" 
AR Path="/94BB6638A" Ref="U"  Part="5" 
AR Path="/23C9F04BB6638A" Ref="U38"  Part="5" 
AR Path="/FFFFFFF04BB6638A" Ref="U38"  Part="5" 
AR Path="/7E4188DA4BB6638A" Ref="U38"  Part="5" 
AR Path="/8D384BB6638A" Ref="U38"  Part="5" 
AR Path="/773F65F14BB6638A" Ref="U38"  Part="5" 
AR Path="/24BB6638A" Ref="U38"  Part="5" 
AR Path="/23C6504BB6638A" Ref="U38"  Part="5" 
AR Path="/D058E04BB6638A" Ref="U38"  Part="5" 
AR Path="/3C64BB6638A" Ref="U38"  Part="5" 
AR Path="/23CBC44BB6638A" Ref="U38"  Part="5" 
AR Path="/2F4A4BB6638A" Ref="U38"  Part="5" 
AR Path="/23D9004BB6638A" Ref="U38"  Part="5" 
AR Path="/64BB6638A" Ref="U38"  Part="5" 
AR Path="/6FE934344BB6638A" Ref="U38"  Part="5" 
AR Path="/6FE934E34BB6638A" Ref="U38"  Part="5" 
AR Path="/2824BB6638A" Ref="U38"  Part="5" 
AR Path="/69549BC04BB6638A" Ref="U38"  Part="5" 
AR Path="/D1C3384BB6638A" Ref="U38"  Part="5" 
F 0 "U38" H 2095 19565 60  0000 C CNN
F 1 "74F04" H 2090 19325 60  0000 C CNN
F 2 "14dip300" H 2090 19425 60  0001 C CNN
F 3 "" H 1900 19450 60  0001 C CNN
	5    1900 19450
	1    0    0    -1  
$EndComp
$Comp
L 74LS08 U?
U 3 1 4BB6633F
P 5650 19900
AR Path="/2300384BB6633F" Ref="U?"  Part="1" 
AR Path="/4BB6633F" Ref="U39"  Part="3" 
AR Path="/FFFFFFFF4BB6633F" Ref="U39"  Part="3" 
AR Path="/755D912A4BB6633F" Ref="U?"  Part="1" 
AR Path="/23D8044BB6633F" Ref="U?"  Part="1" 
AR Path="/7E42B4154BB6633F" Ref="U39"  Part="1" 
AR Path="/363333464BB6633F" Ref="U39"  Part="1" 
AR Path="/6FE934E34BB6633F" Ref="U39"  Part="1" 
AR Path="/23D83C4BB6633F" Ref="U39"  Part="3" 
AR Path="/47907FA4BB6633F" Ref="U39"  Part="3" 
AR Path="/23C34C4BB6633F" Ref="U39"  Part="3" 
AR Path="/14BB6633F" Ref="U39"  Part="3" 
AR Path="/23BC884BB6633F" Ref="U39"  Part="3" 
AR Path="/773F8EB44BB6633F" Ref="U39"  Part="3" 
AR Path="/94BB6633F" Ref="U"  Part="3" 
AR Path="/23C9F04BB6633F" Ref="U39"  Part="3" 
AR Path="/FFFFFFF04BB6633F" Ref="U39"  Part="3" 
AR Path="/7E4188DA4BB6633F" Ref="U39"  Part="3" 
AR Path="/8D384BB6633F" Ref="U39"  Part="3" 
AR Path="/773F65F14BB6633F" Ref="U39"  Part="3" 
AR Path="/24BB6633F" Ref="U39"  Part="3" 
AR Path="/23C6504BB6633F" Ref="U39"  Part="3" 
AR Path="/D058E04BB6633F" Ref="U39"  Part="3" 
AR Path="/3C64BB6633F" Ref="U39"  Part="3" 
AR Path="/23CBC44BB6633F" Ref="U39"  Part="3" 
AR Path="/2F4A4BB6633F" Ref="U39"  Part="3" 
AR Path="/23D9004BB6633F" Ref="U39"  Part="3" 
AR Path="/64BB6633F" Ref="U39"  Part="3" 
AR Path="/6FE934344BB6633F" Ref="U39"  Part="3" 
AR Path="/2824BB6633F" Ref="U39"  Part="3" 
AR Path="/69549BC04BB6633F" Ref="U39"  Part="3" 
AR Path="/D1C3384BB6633F" Ref="U39"  Part="3" 
F 0 "U39" H 5650 19950 60  0000 C CNN
F 1 "74S08" H 5650 19850 60  0000 C CNN
F 2 "14dip300" H 5650 19950 60  0001 C CNN
F 3 "" H 5650 19900 60  0001 C CNN
	3    5650 19900
	1    0    0    -1  
$EndComp
$Comp
L 74LS04 U?
U 4 1 4BB66319
P 4300 19500
AR Path="/2300384BB66319" Ref="U?"  Part="1" 
AR Path="/4BB66319" Ref="U38"  Part="4" 
AR Path="/363331394BB66319" Ref="U?"  Part="1" 
AR Path="/174BB66319" Ref="U"  Part="4" 
AR Path="/FFFFFFFF4BB66319" Ref="U38"  Part="4" 
AR Path="/755D912A4BB66319" Ref="U?"  Part="4" 
AR Path="/7E428DAC4BB66319" Ref="U?"  Part="4" 
AR Path="/29BF304BB66319" Ref="U?"  Part="4" 
AR Path="/44208044BB66319" Ref="U"  Part="4" 
AR Path="/23D83C4BB66319" Ref="U38"  Part="4" 
AR Path="/47907FA4BB66319" Ref="U38"  Part="4" 
AR Path="/23C34C4BB66319" Ref="U38"  Part="4" 
AR Path="/14BB66319" Ref="U38"  Part="4" 
AR Path="/23BC884BB66319" Ref="U38"  Part="4" 
AR Path="/773F8EB44BB66319" Ref="U38"  Part="4" 
AR Path="/94BB66319" Ref="U"  Part="4" 
AR Path="/23C9F04BB66319" Ref="U38"  Part="4" 
AR Path="/FFFFFFF04BB66319" Ref="U38"  Part="4" 
AR Path="/7E4188DA4BB66319" Ref="U38"  Part="4" 
AR Path="/8D384BB66319" Ref="U38"  Part="4" 
AR Path="/773F65F14BB66319" Ref="U38"  Part="4" 
AR Path="/24BB66319" Ref="U38"  Part="4" 
AR Path="/23C6504BB66319" Ref="U38"  Part="4" 
AR Path="/D058E04BB66319" Ref="U38"  Part="4" 
AR Path="/3C64BB66319" Ref="U38"  Part="4" 
AR Path="/23CBC44BB66319" Ref="U38"  Part="4" 
AR Path="/2F4A4BB66319" Ref="U38"  Part="4" 
AR Path="/23D9004BB66319" Ref="U38"  Part="4" 
AR Path="/64BB66319" Ref="U38"  Part="4" 
AR Path="/6FE934344BB66319" Ref="U38"  Part="4" 
AR Path="/6FE934E34BB66319" Ref="U38"  Part="4" 
AR Path="/2824BB66319" Ref="U38"  Part="4" 
AR Path="/69549BC04BB66319" Ref="U38"  Part="4" 
AR Path="/D1C3384BB66319" Ref="U38"  Part="4" 
F 0 "U38" H 4495 19615 60  0000 C CNN
F 1 "74F04" H 4490 19375 60  0000 C CNN
F 2 "14dip300" H 4490 19475 60  0001 C CNN
F 3 "" H 4300 19500 60  0001 C CNN
	4    4300 19500
	1    0    0    -1  
$EndComp
$Comp
L 74LS08 U?
U 4 1 4BB662E2
P 5650 19350
AR Path="/2300384BB662E2" Ref="U?"  Part="1" 
AR Path="/4BB662E2" Ref="U39"  Part="4" 
AR Path="/363245324BB662E2" Ref="U?"  Part="1" 
AR Path="/2600004BB662E2" Ref="U"  Part="4" 
AR Path="/FFFFFFFF4BB662E2" Ref="U39"  Part="4" 
AR Path="/755D912A4BB662E2" Ref="U?"  Part="4" 
AR Path="/23D6704BB662E2" Ref="U?"  Part="4" 
AR Path="/2606084BB662E2" Ref="U39"  Part="4" 
AR Path="/23D83C4BB662E2" Ref="U39"  Part="4" 
AR Path="/47907FA4BB662E2" Ref="U39"  Part="4" 
AR Path="/23C34C4BB662E2" Ref="U39"  Part="4" 
AR Path="/14BB662E2" Ref="U39"  Part="4" 
AR Path="/23BC884BB662E2" Ref="U39"  Part="4" 
AR Path="/773F8EB44BB662E2" Ref="U39"  Part="4" 
AR Path="/94BB662E2" Ref="U"  Part="4" 
AR Path="/23C9F04BB662E2" Ref="U39"  Part="4" 
AR Path="/FFFFFFF04BB662E2" Ref="U39"  Part="4" 
AR Path="/7E4188DA4BB662E2" Ref="U39"  Part="4" 
AR Path="/8D384BB662E2" Ref="U39"  Part="4" 
AR Path="/773F65F14BB662E2" Ref="U39"  Part="4" 
AR Path="/24BB662E2" Ref="U39"  Part="4" 
AR Path="/23C6504BB662E2" Ref="U39"  Part="4" 
AR Path="/D058E04BB662E2" Ref="U39"  Part="4" 
AR Path="/3C64BB662E2" Ref="U39"  Part="4" 
AR Path="/23CBC44BB662E2" Ref="U39"  Part="4" 
AR Path="/2F4A4BB662E2" Ref="U39"  Part="4" 
AR Path="/23D9004BB662E2" Ref="U39"  Part="4" 
AR Path="/64BB662E2" Ref="U39"  Part="4" 
AR Path="/6FE934344BB662E2" Ref="U39"  Part="4" 
AR Path="/6FE934E34BB662E2" Ref="U39"  Part="4" 
AR Path="/2824BB662E2" Ref="U39"  Part="4" 
AR Path="/69549BC04BB662E2" Ref="U39"  Part="4" 
AR Path="/D1C3384BB662E2" Ref="U39"  Part="4" 
F 0 "U39" H 5650 19400 60  0000 C CNN
F 1 "74S08" H 5650 19300 60  0000 C CNN
F 2 "14dip300" H 5650 19400 60  0001 C CNN
F 3 "" H 5650 19350 60  0001 C CNN
	4    5650 19350
	1    0    0    -1  
$EndComp
$Comp
L 74LS04 U?
U 3 1 4BB66246
P 3650 19050
AR Path="/384BB66246" Ref="U?"  Part="1" 
AR Path="/4BB66246" Ref="U38"  Part="3" 
AR Path="/94BB66246" Ref="U"  Part="3" 
AR Path="/FFFFFFFF4BB66246" Ref="U38"  Part="5" 
AR Path="/755D912A4BB66246" Ref="U?"  Part="5" 
AR Path="/7E428DAC4BB66246" Ref="U?"  Part="5" 
AR Path="/2E29F04BB66246" Ref="U?"  Part="5" 
AR Path="/60808744BB66246" Ref="U"  Part="3" 
AR Path="/23D83C4BB66246" Ref="U38"  Part="3" 
AR Path="/47907FA4BB66246" Ref="U38"  Part="3" 
AR Path="/23C34C4BB66246" Ref="U38"  Part="3" 
AR Path="/14BB66246" Ref="U38"  Part="3" 
AR Path="/23BC884BB66246" Ref="U38"  Part="3" 
AR Path="/773F8EB44BB66246" Ref="U38"  Part="3" 
AR Path="/23C9F04BB66246" Ref="U38"  Part="3" 
AR Path="/FFFFFFF04BB66246" Ref="U38"  Part="3" 
AR Path="/7E4188DA4BB66246" Ref="U38"  Part="3" 
AR Path="/8D384BB66246" Ref="U38"  Part="3" 
AR Path="/773F65F14BB66246" Ref="U38"  Part="3" 
AR Path="/24BB66246" Ref="U38"  Part="3" 
AR Path="/23C6504BB66246" Ref="U38"  Part="3" 
AR Path="/D058E04BB66246" Ref="U38"  Part="3" 
AR Path="/3C64BB66246" Ref="U38"  Part="3" 
AR Path="/23CBC44BB66246" Ref="U38"  Part="3" 
AR Path="/2F4A4BB66246" Ref="U38"  Part="3" 
AR Path="/23D9004BB66246" Ref="U38"  Part="3" 
AR Path="/64BB66246" Ref="U38"  Part="3" 
AR Path="/6FE934344BB66246" Ref="U38"  Part="3" 
AR Path="/6FE934E34BB66246" Ref="U38"  Part="3" 
AR Path="/2824BB66246" Ref="U38"  Part="3" 
AR Path="/69549BC04BB66246" Ref="U38"  Part="3" 
AR Path="/D1C3384BB66246" Ref="U38"  Part="3" 
F 0 "U38" H 3845 19165 60  0000 C CNN
F 1 "74F04" H 3840 18925 60  0000 C CNN
F 2 "14dip300" H 3840 19025 60  0001 C CNN
F 3 "" H 3650 19050 60  0001 C CNN
	3    3650 19050
	1    0    0    -1  
$EndComp
$Comp
L 74LS04 U?
U 2 1 4BB661F1
P 4450 18700
AR Path="/2300384BB661F1" Ref="U?"  Part="1" 
AR Path="/4BB661F1" Ref="U38"  Part="2" 
AR Path="/94BB661F1" Ref="U"  Part="2" 
AR Path="/FFFFFFFF4BB661F1" Ref="U38"  Part="3" 
AR Path="/755D912A4BB661F1" Ref="U?"  Part="3" 
AR Path="/2996E84BB661F1" Ref="U?"  Part="3" 
AR Path="/5F008184BB661F1" Ref="U"  Part="2" 
AR Path="/23D83C4BB661F1" Ref="U38"  Part="2" 
AR Path="/47907FA4BB661F1" Ref="U38"  Part="2" 
AR Path="/23C34C4BB661F1" Ref="U38"  Part="2" 
AR Path="/14BB661F1" Ref="U38"  Part="2" 
AR Path="/23BC884BB661F1" Ref="U38"  Part="2" 
AR Path="/773F8EB44BB661F1" Ref="U38"  Part="2" 
AR Path="/23C9F04BB661F1" Ref="U38"  Part="2" 
AR Path="/FFFFFFF04BB661F1" Ref="U38"  Part="2" 
AR Path="/7E4188DA4BB661F1" Ref="U38"  Part="2" 
AR Path="/8D384BB661F1" Ref="U38"  Part="2" 
AR Path="/773F65F14BB661F1" Ref="U38"  Part="2" 
AR Path="/24BB661F1" Ref="U38"  Part="2" 
AR Path="/23C6504BB661F1" Ref="U38"  Part="2" 
AR Path="/D058E04BB661F1" Ref="U38"  Part="2" 
AR Path="/3C64BB661F1" Ref="U38"  Part="2" 
AR Path="/23CBC44BB661F1" Ref="U38"  Part="2" 
AR Path="/2F4A4BB661F1" Ref="U38"  Part="2" 
AR Path="/23D9004BB661F1" Ref="U38"  Part="2" 
AR Path="/64BB661F1" Ref="U38"  Part="2" 
AR Path="/6FE934344BB661F1" Ref="U38"  Part="2" 
AR Path="/6FE934E34BB661F1" Ref="U38"  Part="2" 
AR Path="/2824BB661F1" Ref="U38"  Part="2" 
AR Path="/69549BC04BB661F1" Ref="U38"  Part="2" 
AR Path="/D1C3384BB661F1" Ref="U38"  Part="2" 
F 0 "U38" H 4645 18815 60  0000 C CNN
F 1 "74F04" H 4640 18575 60  0000 C CNN
F 2 "14dip300" H 4640 18675 60  0001 C CNN
F 3 "" H 4450 18700 60  0001 C CNN
	2    4450 18700
	1    0    0    -1  
$EndComp
$Comp
L 74LS08 U?
U 1 1 4BB661E5
P 5650 18800
AR Path="/A200384BB661E5" Ref="U?"  Part="1" 
AR Path="/4BB661E5" Ref="U39"  Part="1" 
AR Path="/FFFFFFFF4BB661E5" Ref="U39"  Part="1" 
AR Path="/755D912A4BB661E5" Ref="U?"  Part="1" 
AR Path="/7E428DAC4BB661E5" Ref="U?"  Part="1" 
AR Path="/7E42B4154BB661E5" Ref="U39"  Part="1" 
AR Path="/363145354BB661E5" Ref="U39"  Part="1" 
AR Path="/23D83C4BB661E5" Ref="U39"  Part="1" 
AR Path="/47907FA4BB661E5" Ref="U39"  Part="1" 
AR Path="/23C34C4BB661E5" Ref="U39"  Part="1" 
AR Path="/14BB661E5" Ref="U39"  Part="1" 
AR Path="/23BC884BB661E5" Ref="U39"  Part="1" 
AR Path="/773F8EB44BB661E5" Ref="U39"  Part="1" 
AR Path="/94BB661E5" Ref="U"  Part="1" 
AR Path="/23C9F04BB661E5" Ref="U39"  Part="1" 
AR Path="/FFFFFFF04BB661E5" Ref="U39"  Part="1" 
AR Path="/7E4188DA4BB661E5" Ref="U39"  Part="1" 
AR Path="/8D384BB661E5" Ref="U39"  Part="1" 
AR Path="/773F65F14BB661E5" Ref="U39"  Part="1" 
AR Path="/24BB661E5" Ref="U39"  Part="1" 
AR Path="/23C6504BB661E5" Ref="U39"  Part="1" 
AR Path="/D058E04BB661E5" Ref="U39"  Part="1" 
AR Path="/3C64BB661E5" Ref="U39"  Part="1" 
AR Path="/23CBC44BB661E5" Ref="U39"  Part="1" 
AR Path="/2F4A4BB661E5" Ref="U39"  Part="1" 
AR Path="/23D9004BB661E5" Ref="U39"  Part="1" 
AR Path="/64BB661E5" Ref="U39"  Part="1" 
AR Path="/6FE934344BB661E5" Ref="U39"  Part="1" 
AR Path="/6FE934E34BB661E5" Ref="U39"  Part="1" 
AR Path="/2824BB661E5" Ref="U39"  Part="1" 
AR Path="/69549BC04BB661E5" Ref="U39"  Part="1" 
AR Path="/D1C3384BB661E5" Ref="U39"  Part="1" 
F 0 "U39" H 5650 18850 60  0000 C CNN
F 1 "74S08" H 5650 18750 60  0000 C CNN
F 2 "14dip300" H 5650 18850 60  0001 C CNN
F 3 "" H 5650 18800 60  0001 C CNN
	1    5650 18800
	1    0    0    -1  
$EndComp
$Comp
L 74LS04 U?
U 1 1 4BB66172
P 4450 18300
AR Path="/A200384BB66172" Ref="U?"  Part="1" 
AR Path="/4BB66172" Ref="U38"  Part="1" 
AR Path="/363234364BB66172" Ref="U?"  Part="1" 
AR Path="/FFFFFFFF4BB66172" Ref="U38"  Part="2" 
AR Path="/755D912A4BB66172" Ref="U?"  Part="2" 
AR Path="/10000004BB66172" Ref="U?"  Part="2" 
AR Path="/7E4188DA4BB66172" Ref="U38"  Part="2" 
AR Path="/B8C4BB66172" Ref="U38"  Part="2" 
AR Path="/313445374BB66172" Ref="U"  Part="1" 
AR Path="/23D83C4BB66172" Ref="U38"  Part="1" 
AR Path="/47907FA4BB66172" Ref="U38"  Part="1" 
AR Path="/23C34C4BB66172" Ref="U38"  Part="1" 
AR Path="/14BB66172" Ref="U38"  Part="1" 
AR Path="/23BC884BB66172" Ref="U38"  Part="1" 
AR Path="/773F8EB44BB66172" Ref="U38"  Part="1" 
AR Path="/94BB66172" Ref="U"  Part="1" 
AR Path="/23C9F04BB66172" Ref="U38"  Part="1" 
AR Path="/FFFFFFF04BB66172" Ref="U38"  Part="1" 
AR Path="/8D384BB66172" Ref="U38"  Part="1" 
AR Path="/773F65F14BB66172" Ref="U38"  Part="1" 
AR Path="/24BB66172" Ref="U38"  Part="1" 
AR Path="/23C6504BB66172" Ref="U38"  Part="1" 
AR Path="/D058E04BB66172" Ref="U38"  Part="1" 
AR Path="/3C64BB66172" Ref="U38"  Part="1" 
AR Path="/23CBC44BB66172" Ref="U38"  Part="1" 
AR Path="/2F4A4BB66172" Ref="U38"  Part="1" 
AR Path="/23D9004BB66172" Ref="U38"  Part="1" 
AR Path="/64BB66172" Ref="U38"  Part="1" 
AR Path="/6FE934344BB66172" Ref="U38"  Part="1" 
AR Path="/6FE934E34BB66172" Ref="U38"  Part="1" 
AR Path="/2824BB66172" Ref="U38"  Part="1" 
AR Path="/69549BC04BB66172" Ref="U38"  Part="1" 
AR Path="/D1C3384BB66172" Ref="U38"  Part="1" 
F 0 "U38" H 4645 18415 60  0000 C CNN
F 1 "74F04" H 4640 18175 60  0000 C CNN
F 2 "14dip300" H 4640 18275 60  0001 C CNN
F 3 "" H 4450 18300 60  0001 C CNN
	1    4450 18300
	1    0    0    -1  
$EndComp
$Comp
L 74LS373 U?
U 1 1 4BB6614F
P 8700 17900
AR Path="/A200384BB6614F" Ref="U?"  Part="1" 
AR Path="/4BB6614F" Ref="U22"  Part="1" 
AR Path="/FFFFFFFF4BB6614F" Ref="U22"  Part="1" 
AR Path="/755D912A4BB6614F" Ref="U?"  Part="1" 
AR Path="/7E428DAC4BB6614F" Ref="U?"  Part="1" 
AR Path="/2606084BB6614F" Ref="U22"  Part="1" 
AR Path="/23D83C4BB6614F" Ref="U22"  Part="1" 
AR Path="/47907FA4BB6614F" Ref="U22"  Part="1" 
AR Path="/23C34C4BB6614F" Ref="U22"  Part="1" 
AR Path="/14BB6614F" Ref="U22"  Part="1" 
AR Path="/23BC884BB6614F" Ref="U22"  Part="1" 
AR Path="/773F8EB44BB6614F" Ref="U22"  Part="1" 
AR Path="/94BB6614F" Ref="U"  Part="1" 
AR Path="/23C9F04BB6614F" Ref="U22"  Part="1" 
AR Path="/FFFFFFF04BB6614F" Ref="U22"  Part="1" 
AR Path="/7E4188DA4BB6614F" Ref="U22"  Part="1" 
AR Path="/8D384BB6614F" Ref="U22"  Part="1" 
AR Path="/24BB6614F" Ref="U22"  Part="1" 
AR Path="/773F65F14BB6614F" Ref="U22"  Part="1" 
AR Path="/23C6504BB6614F" Ref="U22"  Part="1" 
AR Path="/D058E04BB6614F" Ref="U22"  Part="1" 
AR Path="/3C64BB6614F" Ref="U22"  Part="1" 
AR Path="/23CBC44BB6614F" Ref="U22"  Part="1" 
AR Path="/2F4A4BB6614F" Ref="U22"  Part="1" 
AR Path="/23D9004BB6614F" Ref="U22"  Part="1" 
AR Path="/64BB6614F" Ref="U22"  Part="1" 
AR Path="/6FE934344BB6614F" Ref="U22"  Part="1" 
AR Path="/6FE934E34BB6614F" Ref="U22"  Part="1" 
AR Path="/2824BB6614F" Ref="U22"  Part="1" 
AR Path="/69549BC04BB6614F" Ref="U22"  Part="1" 
AR Path="/D1C3384BB6614F" Ref="U22"  Part="1" 
F 0 "U22" H 8700 17900 60  0000 C CNN
F 1 "74LS373" H 8750 17550 60  0000 C CNN
F 2 "20dip300" H 8750 17650 60  0001 C CNN
F 3 "" H 8700 17900 60  0001 C CNN
	1    8700 17900
	1    0    0    -1  
$EndComp
Text Label 900  19850 0    60   ~ 0
M1*
Text Label 900  19450 0    60   ~ 0
MREQ*
Text Label 900  19050 0    60   ~ 0
IORQ*
Text Label 900  18850 0    60   ~ 0
WR*
Text Label 900  19250 0    60   ~ 0
RD*
Text Label 900  19650 0    60   ~ 0
REFSH*
Text Label 900  18650 0    60   ~ 0
HALT*
Text Label 900  17850 0    60   ~ 0
WAIT*
Text Label 900  20050 0    60   ~ 0
NMI*
Text Label 900  18050 0    60   ~ 0
BUSRQ*
Text Label 900  18250 0    60   ~ 0
BUSAK*
Text Label 900  18450 0    60   ~ 0
CLK
Text Label 16350 11100 0    60   ~ 0
CLK
Text Label 16350 10900 0    60   ~ 0
BUSAK*
Text Label 16350 10800 0    60   ~ 0
BUSRQ*
Text Label 16350 10600 0    60   ~ 0
RESET*
Text Label 16350 10400 0    60   ~ 0
NMI*
Text Label 16350 10300 0    60   ~ 0
bINT*
Text Label 16350 10100 0    60   ~ 0
WAIT*
Text Label 16350 9900 0    60   ~ 0
HALT*
Text Label 16350 9700 0    60   ~ 0
REFSH*
Text Label 16350 9500 0    60   ~ 0
RD*
Text Label 16350 9400 0    60   ~ 0
WR*
Text Label 16350 9300 0    60   ~ 0
IORQ*
Text Label 16350 9200 0    60   ~ 0
MREQ*
Text Label 16350 9000 0    60   ~ 0
M1*
Text Label 18850 6750 0    60   ~ 0
DODSB*
$Comp
L 74LS00 U?
U 1 1 4BB3EB43
P 17550 6850
AR Path="/2300384BB3EB43" Ref="U?"  Part="1" 
AR Path="/4BB3EB43" Ref="U28"  Part="1" 
AR Path="/FFFFFFFF4BB3EB43" Ref="U28"  Part="1" 
AR Path="/755D912A4BB3EB43" Ref="U?"  Part="1" 
AR Path="/23D8044BB3EB43" Ref="U?"  Part="1" 
AR Path="/2606084BB3EB43" Ref="U28"  Part="1" 
AR Path="/23D83C4BB3EB43" Ref="U28"  Part="1" 
AR Path="/47907FA4BB3EB43" Ref="U28"  Part="1" 
AR Path="/23C34C4BB3EB43" Ref="U28"  Part="1" 
AR Path="/14BB3EB43" Ref="U28"  Part="1" 
AR Path="/23BC884BB3EB43" Ref="U28"  Part="1" 
AR Path="/773F8EB44BB3EB43" Ref="U28"  Part="1" 
AR Path="/94BB3EB43" Ref="U"  Part="1" 
AR Path="/23C9F04BB3EB43" Ref="U28"  Part="1" 
AR Path="/FFFFFFF04BB3EB43" Ref="U28"  Part="1" 
AR Path="/7E4188DA4BB3EB43" Ref="U28"  Part="1" 
AR Path="/8D384BB3EB43" Ref="U28"  Part="1" 
AR Path="/24BB3EB43" Ref="U28"  Part="1" 
AR Path="/773F65F14BB3EB43" Ref="U28"  Part="1" 
AR Path="/23C6504BB3EB43" Ref="U28"  Part="1" 
AR Path="/D058E04BB3EB43" Ref="U28"  Part="1" 
AR Path="/3C64BB3EB43" Ref="U28"  Part="1" 
AR Path="/23CBC44BB3EB43" Ref="U28"  Part="1" 
AR Path="/2F4A4BB3EB43" Ref="U28"  Part="1" 
AR Path="/23D9004BB3EB43" Ref="U28"  Part="1" 
AR Path="/64BB3EB43" Ref="U28"  Part="1" 
AR Path="/6FE934344BB3EB43" Ref="U28"  Part="1" 
AR Path="/6FE934E34BB3EB43" Ref="U28"  Part="1" 
AR Path="/2824BB3EB43" Ref="U28"  Part="1" 
AR Path="/D1C3384BB3EB43" Ref="U28"  Part="1" 
F 0 "U28" H 17550 6900 60  0000 C CNN
F 1 "74LS00" H 17550 6800 60  0000 C CNN
F 2 "14dip300" H 17550 6900 60  0001 C CNN
F 3 "" H 17550 6850 60  0001 C CNN
	1    17550 6850
	-1   0    0    1   
$EndComp
Text Label 13000 3150 0    60   ~ 0
A11
Text Label 13350 3300 0    60   ~ 0
A0
Text Label 15050 1950 0    60   ~ 0
GND
Text Label 16800 2350 0    60   ~ 0
JUMP_ENABLE
$Comp
L 74LS74 U?
U 1 1 4BB3E9F3
P 16050 2150
AR Path="/2300384BB3E9F3" Ref="U?"  Part="1" 
AR Path="/4BB3E9F3" Ref="U29"  Part="1" 
AR Path="/FFFFFFFF4BB3E9F3" Ref="U29"  Part="1" 
AR Path="/755D912A4BB3E9F3" Ref="U?"  Part="1" 
AR Path="/10000004BB3E9F3" Ref="U?"  Part="1" 
AR Path="/2606084BB3E9F3" Ref="U29"  Part="1" 
AR Path="/23D83C4BB3E9F3" Ref="U29"  Part="1" 
AR Path="/47907FA4BB3E9F3" Ref="U29"  Part="1" 
AR Path="/23C34C4BB3E9F3" Ref="U29"  Part="1" 
AR Path="/14BB3E9F3" Ref="U29"  Part="1" 
AR Path="/23BC884BB3E9F3" Ref="U29"  Part="1" 
AR Path="/773F8EB44BB3E9F3" Ref="U29"  Part="1" 
AR Path="/94BB3E9F3" Ref="U"  Part="1" 
AR Path="/23C9F04BB3E9F3" Ref="U29"  Part="1" 
AR Path="/FFFFFFF04BB3E9F3" Ref="U29"  Part="1" 
AR Path="/7E4188DA4BB3E9F3" Ref="U29"  Part="1" 
AR Path="/8D384BB3E9F3" Ref="U29"  Part="1" 
AR Path="/773F65F14BB3E9F3" Ref="U29"  Part="1" 
AR Path="/24BB3E9F3" Ref="U29"  Part="1" 
AR Path="/23C6504BB3E9F3" Ref="U29"  Part="1" 
AR Path="/D058E04BB3E9F3" Ref="U29"  Part="1" 
AR Path="/3C64BB3E9F3" Ref="U29"  Part="1" 
AR Path="/23CBC44BB3E9F3" Ref="U29"  Part="1" 
AR Path="/2F4A4BB3E9F3" Ref="U29"  Part="1" 
AR Path="/23D9004BB3E9F3" Ref="U29"  Part="1" 
AR Path="/64BB3E9F3" Ref="U29"  Part="1" 
AR Path="/6FE934344BB3E9F3" Ref="U29"  Part="1" 
AR Path="/6FE934E34BB3E9F3" Ref="U29"  Part="1" 
AR Path="/2824BB3E9F3" Ref="U29"  Part="1" 
AR Path="/D1C3384BB3E9F3" Ref="U29"  Part="1" 
F 0 "U29" H 16200 2450 60  0000 C CNN
F 1 "74LS74" H 16350 1855 60  0000 C CNN
F 2 "14dip300" H 16350 1955 60  0001 C CNN
F 3 "" H 16050 2150 60  0001 C CNN
	1    16050 2150
	1    0    0    -1  
$EndComp
Text Label 19950 9300 0    60   ~ 0
READ_STROBE
$Comp
L 74LS00 U?
U 3 1 4BB3E972
P 19000 9200
AR Path="/2300384BB3E972" Ref="U?"  Part="1" 
AR Path="/4BB3E972" Ref="U28"  Part="3" 
AR Path="/FFFFFFFF4BB3E972" Ref="U28"  Part="1" 
AR Path="/755D912A4BB3E972" Ref="U?"  Part="1" 
AR Path="/23D8044BB3E972" Ref="U28"  Part="1" 
AR Path="/2606084BB3E972" Ref="U28"  Part="1" 
AR Path="/363036324BB3E972" Ref="U"  Part="3" 
AR Path="/23D83C4BB3E972" Ref="U28"  Part="3" 
AR Path="/47907FA4BB3E972" Ref="U28"  Part="3" 
AR Path="/23C34C4BB3E972" Ref="U28"  Part="3" 
AR Path="/14BB3E972" Ref="U28"  Part="3" 
AR Path="/23BC884BB3E972" Ref="U28"  Part="3" 
AR Path="/773F8EB44BB3E972" Ref="U28"  Part="3" 
AR Path="/94BB3E972" Ref="U"  Part="3" 
AR Path="/23C9F04BB3E972" Ref="U28"  Part="3" 
AR Path="/FFFFFFF04BB3E972" Ref="U28"  Part="3" 
AR Path="/7E4188DA4BB3E972" Ref="U28"  Part="3" 
AR Path="/8D384BB3E972" Ref="U28"  Part="3" 
AR Path="/773F65F14BB3E972" Ref="U28"  Part="3" 
AR Path="/24BB3E972" Ref="U28"  Part="3" 
AR Path="/23C6504BB3E972" Ref="U28"  Part="3" 
AR Path="/D058E04BB3E972" Ref="U28"  Part="3" 
AR Path="/3C64BB3E972" Ref="U28"  Part="3" 
AR Path="/23CBC44BB3E972" Ref="U28"  Part="3" 
AR Path="/2F4A4BB3E972" Ref="U28"  Part="3" 
AR Path="/23D9004BB3E972" Ref="U28"  Part="3" 
AR Path="/64BB3E972" Ref="U28"  Part="3" 
AR Path="/6FE934344BB3E972" Ref="U28"  Part="3" 
AR Path="/6FE934E34BB3E972" Ref="U28"  Part="3" 
AR Path="/2824BB3E972" Ref="U28"  Part="3" 
AR Path="/D1C3384BB3E972" Ref="U28"  Part="3" 
F 0 "U28" H 19000 9250 60  0000 C CNN
F 1 "74LS00" H 19000 9150 60  0000 C CNN
F 2 "14dip300" H 19000 9250 60  0001 C CNN
F 3 "" H 19000 9200 60  0001 C CNN
	3    19000 9200
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR?
U 1 1 4BB3E8CE
P 18850 4450
AR Path="/2300384BB3E8CE" Ref="#PWR?"  Part="1" 
AR Path="/4BB3E8CE" Ref="#PWR028"  Part="1" 
AR Path="/FFFFFFFF4BB3E8CE" Ref="#PWR036"  Part="1" 
AR Path="/755D912A4BB3E8CE" Ref="#PWR?"  Part="1" 
AR Path="/23D83C4BB3E8CE" Ref="#PWR50"  Part="1" 
AR Path="/47907FA4BB3E8CE" Ref="#PWR043"  Part="1" 
AR Path="/23C34C4BB3E8CE" Ref="#PWR035"  Part="1" 
AR Path="/14BB3E8CE" Ref="#PWR043"  Part="1" 
AR Path="/23BC884BB3E8CE" Ref="#PWR036"  Part="1" 
AR Path="/773F8EB44BB3E8CE" Ref="#PWR034"  Part="1" 
AR Path="/94BB3E8CE" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB3E8CE" Ref="#PWR40"  Part="1" 
AR Path="/FFFFFFF04BB3E8CE" Ref="#PWR53"  Part="1" 
AR Path="/7E4188DA4BB3E8CE" Ref="#PWR50"  Part="1" 
AR Path="/8D384BB3E8CE" Ref="#PWR043"  Part="1" 
AR Path="/773F65F14BB3E8CE" Ref="#PWR036"  Part="1" 
AR Path="/6FF0DD404BB3E8CE" Ref="#PWR036"  Part="1" 
AR Path="/24BB3E8CE" Ref="#PWR40"  Part="1" 
AR Path="/23C6504BB3E8CE" Ref="#PWR044"  Part="1" 
AR Path="/D058E04BB3E8CE" Ref="#PWR036"  Part="1" 
AR Path="/6684D64BB3E8CE" Ref="#PWR034"  Part="1" 
AR Path="/3C64BB3E8CE" Ref="#PWR044"  Part="1" 
AR Path="/23CBC44BB3E8CE" Ref="#PWR034"  Part="1" 
AR Path="/2F4A4BB3E8CE" Ref="#PWR044"  Part="1" 
AR Path="/23D9004BB3E8CE" Ref="#PWR034"  Part="1" 
AR Path="/64BB3E8CE" Ref="#PWR51"  Part="1" 
AR Path="/6FE934344BB3E8CE" Ref="#PWR51"  Part="1" 
AR Path="/6FE934E34BB3E8CE" Ref="#PWR034"  Part="1" 
AR Path="/2824BB3E8CE" Ref="#PWR034"  Part="1" 
AR Path="/D1C3384BB3E8CE" Ref="#PWR036"  Part="1" 
F 0 "#PWR028" H 18850 4450 30  0001 C CNN
F 1 "GND" H 18850 4380 30  0001 C CNN
F 2 "" H 18850 4450 60  0001 C CNN
F 3 "" H 18850 4450 60  0001 C CNN
	1    18850 4450
	1    0    0    -1  
$EndComp
Text Label 18400 5450 0    60   ~ 0
DO7
Text Label 18400 5550 0    60   ~ 0
DO6
Text Label 18400 5650 0    60   ~ 0
DO5
Text Label 18400 5750 0    60   ~ 0
DO4
Text Label 18400 5850 0    60   ~ 0
DO3
Text Label 18400 5950 0    60   ~ 0
DO2
Text Label 18400 6050 0    60   ~ 0
DO1
Text Label 18400 6150 0    60   ~ 0
DO0
Text Label 18450 8050 0    60   ~ 0
DI0
Text Label 18450 7950 0    60   ~ 0
DI1
Text Label 18450 7850 0    60   ~ 0
DI2
Text Label 18450 7750 0    60   ~ 0
DI3
Text Label 18450 7650 0    60   ~ 0
DI4
Text Label 18450 7550 0    60   ~ 0
DI5
Text Label 18450 7450 0    60   ~ 0
DI6
Text Label 18450 7350 0    60   ~ 0
DI7
$Comp
L 74LS241 U?
U 1 1 4BB3E6B3
P 17700 7850
AR Path="/2300384BB3E6B3" Ref="U?"  Part="1" 
AR Path="/4BB3E6B3" Ref="U20"  Part="1" 
AR Path="/450036314BB3E6B3" Ref="U?"  Part="1" 
AR Path="/FFFFFFFF4BB3E6B3" Ref="U20"  Part="1" 
AR Path="/755D912A4BB3E6B3" Ref="U?"  Part="1" 
AR Path="/23D8044BB3E6B3" Ref="U?"  Part="1" 
AR Path="/299AC84BB3E6B3" Ref="U?"  Part="1" 
AR Path="/174BB3E6B3" Ref="U?"  Part="1" 
AR Path="/2606084BB3E6B3" Ref="U20"  Part="1" 
AR Path="/23D83C4BB3E6B3" Ref="U20"  Part="1" 
AR Path="/47907FA4BB3E6B3" Ref="U20"  Part="1" 
AR Path="/23C34C4BB3E6B3" Ref="U20"  Part="1" 
AR Path="/14BB3E6B3" Ref="U20"  Part="1" 
AR Path="/23BC884BB3E6B3" Ref="U20"  Part="1" 
AR Path="/773F8EB44BB3E6B3" Ref="U20"  Part="1" 
AR Path="/94BB3E6B3" Ref="U"  Part="1" 
AR Path="/23C9F04BB3E6B3" Ref="U20"  Part="1" 
AR Path="/FFFFFFF04BB3E6B3" Ref="U20"  Part="1" 
AR Path="/7E4188DA4BB3E6B3" Ref="U20"  Part="1" 
AR Path="/8D384BB3E6B3" Ref="U20"  Part="1" 
AR Path="/24BB3E6B3" Ref="U20"  Part="1" 
AR Path="/773F65F14BB3E6B3" Ref="U20"  Part="1" 
AR Path="/23C6504BB3E6B3" Ref="U20"  Part="1" 
AR Path="/D058E04BB3E6B3" Ref="U20"  Part="1" 
AR Path="/3C64BB3E6B3" Ref="U20"  Part="1" 
AR Path="/23CBC44BB3E6B3" Ref="U20"  Part="1" 
AR Path="/2F4A4BB3E6B3" Ref="U20"  Part="1" 
AR Path="/23D9004BB3E6B3" Ref="U20"  Part="1" 
AR Path="/64BB3E6B3" Ref="U20"  Part="1" 
AR Path="/6FE934344BB3E6B3" Ref="U20"  Part="1" 
AR Path="/6FE934E34BB3E6B3" Ref="U20"  Part="1" 
AR Path="/2824BB3E6B3" Ref="U20"  Part="1" 
AR Path="/D1C3384BB3E6B3" Ref="U20"  Part="1" 
F 0 "U20" H 17750 7650 60  0000 C CNN
F 1 "74LS244" H 17800 7450 60  0000 C CNN
F 2 "20dip300" H 17800 7550 60  0001 C CNN
F 3 "" H 17700 7850 60  0001 C CNN
	1    17700 7850
	-1   0    0    -1  
$EndComp
$Comp
L 74LS241 U?
U 1 1 4BB3E69A
P 17650 5950
AR Path="/384BB3E69A" Ref="U?"  Part="1" 
AR Path="/4BB3E69A" Ref="U19"  Part="1" 
AR Path="/FFFFFFFF4BB3E69A" Ref="U19"  Part="1" 
AR Path="/755D912A4BB3E69A" Ref="U?"  Part="1" 
AR Path="/23D8044BB3E69A" Ref="U?"  Part="1" 
AR Path="/2606084BB3E69A" Ref="U19"  Part="1" 
AR Path="/23D83C4BB3E69A" Ref="U19"  Part="1" 
AR Path="/47907FA4BB3E69A" Ref="U19"  Part="1" 
AR Path="/23C34C4BB3E69A" Ref="U19"  Part="1" 
AR Path="/14BB3E69A" Ref="U19"  Part="1" 
AR Path="/23BC884BB3E69A" Ref="U19"  Part="1" 
AR Path="/773F8EB44BB3E69A" Ref="U19"  Part="1" 
AR Path="/94BB3E69A" Ref="U"  Part="1" 
AR Path="/23C9F04BB3E69A" Ref="U19"  Part="1" 
AR Path="/FFFFFFF04BB3E69A" Ref="U19"  Part="1" 
AR Path="/7E4188DA4BB3E69A" Ref="U19"  Part="1" 
AR Path="/8D384BB3E69A" Ref="U19"  Part="1" 
AR Path="/24BB3E69A" Ref="U19"  Part="1" 
AR Path="/773F65F14BB3E69A" Ref="U19"  Part="1" 
AR Path="/23C6504BB3E69A" Ref="U19"  Part="1" 
AR Path="/D058E04BB3E69A" Ref="U19"  Part="1" 
AR Path="/3C64BB3E69A" Ref="U19"  Part="1" 
AR Path="/23CBC44BB3E69A" Ref="U19"  Part="1" 
AR Path="/2F4A4BB3E69A" Ref="U19"  Part="1" 
AR Path="/23D9004BB3E69A" Ref="U19"  Part="1" 
AR Path="/64BB3E69A" Ref="U19"  Part="1" 
AR Path="/6FE934344BB3E69A" Ref="U19"  Part="1" 
AR Path="/6FE934E34BB3E69A" Ref="U19"  Part="1" 
AR Path="/2824BB3E69A" Ref="U19"  Part="1" 
AR Path="/D1C3384BB3E69A" Ref="U19"  Part="1" 
F 0 "U19" H 17700 5750 60  0000 C CNN
F 1 "74LS244" H 17750 5550 60  0000 C CNN
F 2 "20dip300" H 17750 5650 60  0001 C CNN
F 3 "" H 17650 5950 60  0001 C CNN
	1    17650 5950
	1    0    0    -1  
$EndComp
$Comp
L 74LS241 U?
U 1 1 4BB3E651
P 17650 3800
AR Path="/2300384BB3E651" Ref="U?"  Part="1" 
AR Path="/4BB3E651" Ref="U18"  Part="1" 
AR Path="/453635314BB3E651" Ref="U?"  Part="1" 
AR Path="/FFFFFFFF4BB3E651" Ref="U18"  Part="1" 
AR Path="/755D912A4BB3E651" Ref="U?"  Part="1" 
AR Path="/23D8044BB3E651" Ref="U?"  Part="1" 
AR Path="/2606084BB3E651" Ref="U18"  Part="1" 
AR Path="/23D83C4BB3E651" Ref="U18"  Part="1" 
AR Path="/47907FA4BB3E651" Ref="U18"  Part="1" 
AR Path="/23C34C4BB3E651" Ref="U18"  Part="1" 
AR Path="/14BB3E651" Ref="U18"  Part="1" 
AR Path="/23BC884BB3E651" Ref="U18"  Part="1" 
AR Path="/773F8EB44BB3E651" Ref="U18"  Part="1" 
AR Path="/94BB3E651" Ref="U"  Part="1" 
AR Path="/23C9F04BB3E651" Ref="U18"  Part="1" 
AR Path="/FFFFFFF04BB3E651" Ref="U18"  Part="1" 
AR Path="/7E4188DA4BB3E651" Ref="U18"  Part="1" 
AR Path="/8D384BB3E651" Ref="U18"  Part="1" 
AR Path="/24BB3E651" Ref="U18"  Part="1" 
AR Path="/773F65F14BB3E651" Ref="U18"  Part="1" 
AR Path="/23C6504BB3E651" Ref="U18"  Part="1" 
AR Path="/D058E04BB3E651" Ref="U18"  Part="1" 
AR Path="/3C64BB3E651" Ref="U18"  Part="1" 
AR Path="/23CBC44BB3E651" Ref="U18"  Part="1" 
AR Path="/2F4A4BB3E651" Ref="U18"  Part="1" 
AR Path="/23D9004BB3E651" Ref="U18"  Part="1" 
AR Path="/64BB3E651" Ref="U18"  Part="1" 
AR Path="/6FE934344BB3E651" Ref="U18"  Part="1" 
AR Path="/6FE934E34BB3E651" Ref="U18"  Part="1" 
AR Path="/2824BB3E651" Ref="U18"  Part="1" 
AR Path="/D1C3384BB3E651" Ref="U18"  Part="1" 
F 0 "U18" H 17700 3600 60  0000 C CNN
F 1 "74LS244" H 17750 3400 60  0000 C CNN
F 2 "20dip300" H 17750 3500 60  0001 C CNN
F 3 "" H 17650 3800 60  0001 C CNN
	1    17650 3800
	-1   0    0    -1  
$EndComp
$Comp
L 27C64 U?
U 1 1 4BB3E63B
P 14850 4200
AR Path="/2300384BB3E63B" Ref="U?"  Part="1" 
AR Path="/4BB3E63B" Ref="U13"  Part="1" 
AR Path="/FFFFFFFF4BB3E63B" Ref="U13"  Part="1" 
AR Path="/755D912A4BB3E63B" Ref="U?"  Part="1" 
AR Path="/23D8044BB3E63B" Ref="U?"  Part="1" 
AR Path="/2606084BB3E63B" Ref="U13"  Part="1" 
AR Path="/23D83C4BB3E63B" Ref="U13"  Part="1" 
AR Path="/47907FA4BB3E63B" Ref="U13"  Part="1" 
AR Path="/23C34C4BB3E63B" Ref="U13"  Part="1" 
AR Path="/14BB3E63B" Ref="U13"  Part="1" 
AR Path="/23BC884BB3E63B" Ref="U13"  Part="1" 
AR Path="/773F8EB44BB3E63B" Ref="U13"  Part="1" 
AR Path="/94BB3E63B" Ref="U"  Part="1" 
AR Path="/23C9F04BB3E63B" Ref="U13"  Part="1" 
AR Path="/FFFFFFF04BB3E63B" Ref="U13"  Part="1" 
AR Path="/7E4188DA4BB3E63B" Ref="U13"  Part="1" 
AR Path="/8D384BB3E63B" Ref="U13"  Part="1" 
AR Path="/24BB3E63B" Ref="U13"  Part="1" 
AR Path="/773F65F14BB3E63B" Ref="U13"  Part="1" 
AR Path="/23C6504BB3E63B" Ref="U13"  Part="1" 
AR Path="/D058E04BB3E63B" Ref="U13"  Part="1" 
AR Path="/3C64BB3E63B" Ref="U13"  Part="1" 
AR Path="/23CBC44BB3E63B" Ref="U13"  Part="1" 
AR Path="/2F4A4BB3E63B" Ref="U13"  Part="1" 
AR Path="/23D9004BB3E63B" Ref="U13"  Part="1" 
AR Path="/64BB3E63B" Ref="U13"  Part="1" 
AR Path="/6FE934344BB3E63B" Ref="U13"  Part="1" 
AR Path="/6FE934E34BB3E63B" Ref="U13"  Part="1" 
AR Path="/2824BB3E63B" Ref="U13"  Part="1" 
AR Path="/D1C3384BB3E63B" Ref="U13"  Part="1" 
F 0 "U13" H 15000 4050 90  0000 C CNN
F 1 "27C64" H 15000 3850 90  0000 C CNN
F 2 "28dip600" H 15000 3950 90  0001 C CNN
F 3 "" H 14850 4200 60  0001 C CNN
	1    14850 4200
	1    0    0    -1  
$EndComp
$Comp
L LED D?
U 1 1 4BB3E4CC
P 17850 12100
AR Path="/A000384BB3E4CC" Ref="D?"  Part="1" 
AR Path="/4BB3E4CC" Ref="D5"  Part="1" 
AR Path="/450036314BB3E4CC" Ref="D?"  Part="1" 
AR Path="/2606084BB3E4CC" Ref="D5"  Part="1" 
AR Path="/FFFFFFFF4BB3E4CC" Ref="D5"  Part="1" 
AR Path="/755D912A4BB3E4CC" Ref="D5"  Part="1" 
AR Path="/23D83C4BB3E4CC" Ref="D5"  Part="1" 
AR Path="/47907FA4BB3E4CC" Ref="D5"  Part="1" 
AR Path="/23C34C4BB3E4CC" Ref="D5"  Part="1" 
AR Path="/14BB3E4CC" Ref="D5"  Part="1" 
AR Path="/23BC884BB3E4CC" Ref="D5"  Part="1" 
AR Path="/773F8EB44BB3E4CC" Ref="D5"  Part="1" 
AR Path="/94BB3E4CC" Ref="D"  Part="1" 
AR Path="/23C9F04BB3E4CC" Ref="D5"  Part="1" 
AR Path="/FFFFFFF04BB3E4CC" Ref="D5"  Part="1" 
AR Path="/7E4188DA4BB3E4CC" Ref="D5"  Part="1" 
AR Path="/8D384BB3E4CC" Ref="D5"  Part="1" 
AR Path="/24BB3E4CC" Ref="D5"  Part="1" 
AR Path="/773F65F14BB3E4CC" Ref="D5"  Part="1" 
AR Path="/23C6504BB3E4CC" Ref="D5"  Part="1" 
AR Path="/D058E04BB3E4CC" Ref="D5"  Part="1" 
AR Path="/3C64BB3E4CC" Ref="D5"  Part="1" 
AR Path="/23CBC44BB3E4CC" Ref="D5"  Part="1" 
AR Path="/2F4A4BB3E4CC" Ref="D5"  Part="1" 
AR Path="/23D9004BB3E4CC" Ref="D5"  Part="1" 
AR Path="/64BB3E4CC" Ref="D5"  Part="1" 
AR Path="/6FE934344BB3E4CC" Ref="D5"  Part="1" 
AR Path="/6FE934E34BB3E4CC" Ref="D5"  Part="1" 
AR Path="/2824BB3E4CC" Ref="D5"  Part="1" 
AR Path="/D1C3384BB3E4CC" Ref="D5"  Part="1" 
F 0 "D5" H 17850 12200 50  0000 C CNN
F 1 "LED" H 17850 12000 50  0000 C CNN
F 2 "LEDV" H 17850 12100 50  0001 C CNN
F 3 "" H 17850 12100 60  0001 C CNN
	1    17850 12100
	-1   0    0    1   
$EndComp
$Comp
L 74LS04 U?
U 5 1 4BB3E4BC
P 16250 12100
AR Path="/2300384BB3E4BC" Ref="U?"  Part="1" 
AR Path="/4BB3E4BC" Ref="U25"  Part="5" 
AR Path="/FFFFFFFF4BB3E4BC" Ref="U25"  Part="1" 
AR Path="/755D912A4BB3E4BC" Ref="U?"  Part="1" 
AR Path="/2606084BB3E4BC" Ref="U25"  Part="1" 
AR Path="/23D8044BB3E4BC" Ref="U25"  Part="1" 
AR Path="/363036324BB3E4BC" Ref="U"  Part="5" 
AR Path="/23D83C4BB3E4BC" Ref="U25"  Part="5" 
AR Path="/47907FA4BB3E4BC" Ref="U25"  Part="5" 
AR Path="/23C34C4BB3E4BC" Ref="U25"  Part="5" 
AR Path="/14BB3E4BC" Ref="U25"  Part="5" 
AR Path="/23BC884BB3E4BC" Ref="U25"  Part="5" 
AR Path="/773F8EB44BB3E4BC" Ref="U25"  Part="5" 
AR Path="/94BB3E4BC" Ref="U"  Part="5" 
AR Path="/23C9F04BB3E4BC" Ref="U25"  Part="5" 
AR Path="/FFFFFFF04BB3E4BC" Ref="U25"  Part="5" 
AR Path="/7E4188DA4BB3E4BC" Ref="U25"  Part="5" 
AR Path="/8D384BB3E4BC" Ref="U25"  Part="5" 
AR Path="/773F65F14BB3E4BC" Ref="U25"  Part="5" 
AR Path="/24BB3E4BC" Ref="U25"  Part="5" 
AR Path="/23C6504BB3E4BC" Ref="U25"  Part="5" 
AR Path="/D058E04BB3E4BC" Ref="U25"  Part="5" 
AR Path="/3C64BB3E4BC" Ref="U25"  Part="5" 
AR Path="/23CBC44BB3E4BC" Ref="U25"  Part="5" 
AR Path="/2F4A4BB3E4BC" Ref="U25"  Part="5" 
AR Path="/23D9004BB3E4BC" Ref="U25"  Part="5" 
AR Path="/64BB3E4BC" Ref="U25"  Part="5" 
AR Path="/6FE934344BB3E4BC" Ref="U25"  Part="5" 
AR Path="/6FE934E34BB3E4BC" Ref="U25"  Part="5" 
AR Path="/2824BB3E4BC" Ref="U25"  Part="5" 
AR Path="/D1C3384BB3E4BC" Ref="U25"  Part="5" 
F 0 "U25" H 16445 12215 60  0000 C CNN
F 1 "74LS04" H 16440 11975 60  0000 C CNN
F 2 "14dip300" H 16440 12075 60  0001 C CNN
F 3 "" H 16250 12100 60  0001 C CNN
	5    16250 12100
	1    0    0    -1  
$EndComp
NoConn ~ 10450 12100
Text Label 13400 12650 0    60   ~ 0
MEM_RD
$Comp
L 74LS00 U?
U 2 1 4BB3E46B
P 15200 12550
AR Path="/2300384BB3E46B" Ref="U?"  Part="1" 
AR Path="/4BB3E46B" Ref="U28"  Part="2" 
AR Path="/FFFFFFFF4BB3E46B" Ref="U28"  Part="1" 
AR Path="/755D912A4BB3E46B" Ref="U?"  Part="1" 
AR Path="/31324BB3E46B" Ref="U?"  Part="1" 
AR Path="/2606084BB3E46B" Ref="U28"  Part="1" 
AR Path="/23D8044BB3E46B" Ref="U28"  Part="1" 
AR Path="/363036324BB3E46B" Ref="U"  Part="2" 
AR Path="/23D83C4BB3E46B" Ref="U28"  Part="2" 
AR Path="/47907FA4BB3E46B" Ref="U28"  Part="2" 
AR Path="/23C34C4BB3E46B" Ref="U28"  Part="2" 
AR Path="/14BB3E46B" Ref="U28"  Part="2" 
AR Path="/23BC884BB3E46B" Ref="U28"  Part="2" 
AR Path="/773F8EB44BB3E46B" Ref="U28"  Part="2" 
AR Path="/94BB3E46B" Ref="U"  Part="2" 
AR Path="/23C9F04BB3E46B" Ref="U28"  Part="2" 
AR Path="/FFFFFFF04BB3E46B" Ref="U28"  Part="2" 
AR Path="/7E4188DA4BB3E46B" Ref="U28"  Part="2" 
AR Path="/8D384BB3E46B" Ref="U28"  Part="2" 
AR Path="/773F65F14BB3E46B" Ref="U28"  Part="2" 
AR Path="/24BB3E46B" Ref="U28"  Part="2" 
AR Path="/23C6504BB3E46B" Ref="U28"  Part="2" 
AR Path="/D058E04BB3E46B" Ref="U28"  Part="2" 
AR Path="/3C64BB3E46B" Ref="U28"  Part="2" 
AR Path="/23CBC44BB3E46B" Ref="U28"  Part="2" 
AR Path="/2F4A4BB3E46B" Ref="U28"  Part="2" 
AR Path="/23D9004BB3E46B" Ref="U28"  Part="2" 
AR Path="/64BB3E46B" Ref="U28"  Part="2" 
AR Path="/6FE934344BB3E46B" Ref="U28"  Part="2" 
AR Path="/6FE934E34BB3E46B" Ref="U28"  Part="2" 
AR Path="/2824BB3E46B" Ref="U28"  Part="2" 
AR Path="/D1C3384BB3E46B" Ref="U28"  Part="2" 
F 0 "U28" H 15200 12600 60  0000 C CNN
F 1 "74LS00" H 15200 12500 60  0000 C CNN
F 2 "14dip300" H 15200 12600 60  0001 C CNN
F 3 "" H 15200 12550 60  0001 C CNN
	2    15200 12550
	1    0    0    -1  
$EndComp
$Comp
L 74LS04 U?
U 4 1 4BB3E454
P 14150 12450
AR Path="/2300384BB3E454" Ref="U?"  Part="1" 
AR Path="/4BB3E454" Ref="U25"  Part="4" 
AR Path="/2600004BB3E454" Ref="U"  Part="2" 
AR Path="/FFFFFFFF4BB3E454" Ref="U25"  Part="2" 
AR Path="/755D912A4BB3E454" Ref="U?"  Part="2" 
AR Path="/2606084BB3E454" Ref="U25"  Part="2" 
AR Path="/23D8044BB3E454" Ref="U25"  Part="2" 
AR Path="/363036324BB3E454" Ref="U"  Part="4" 
AR Path="/23D83C4BB3E454" Ref="U25"  Part="4" 
AR Path="/47907FA4BB3E454" Ref="U25"  Part="4" 
AR Path="/23C34C4BB3E454" Ref="U25"  Part="4" 
AR Path="/14BB3E454" Ref="U25"  Part="4" 
AR Path="/23BC884BB3E454" Ref="U25"  Part="4" 
AR Path="/773F8EB44BB3E454" Ref="U25"  Part="4" 
AR Path="/94BB3E454" Ref="U"  Part="4" 
AR Path="/23C9F04BB3E454" Ref="U25"  Part="4" 
AR Path="/FFFFFFF04BB3E454" Ref="U25"  Part="4" 
AR Path="/7E4188DA4BB3E454" Ref="U25"  Part="4" 
AR Path="/8D384BB3E454" Ref="U25"  Part="4" 
AR Path="/773F65F14BB3E454" Ref="U25"  Part="4" 
AR Path="/24BB3E454" Ref="U25"  Part="4" 
AR Path="/23C6504BB3E454" Ref="U25"  Part="4" 
AR Path="/D058E04BB3E454" Ref="U25"  Part="4" 
AR Path="/3C64BB3E454" Ref="U25"  Part="4" 
AR Path="/23CBC44BB3E454" Ref="U25"  Part="4" 
AR Path="/2F4A4BB3E454" Ref="U25"  Part="4" 
AR Path="/23D9004BB3E454" Ref="U25"  Part="4" 
AR Path="/64BB3E454" Ref="U25"  Part="4" 
AR Path="/6FE934344BB3E454" Ref="U25"  Part="4" 
AR Path="/6FE934E34BB3E454" Ref="U25"  Part="4" 
AR Path="/2824BB3E454" Ref="U25"  Part="4" 
AR Path="/D1C3384BB3E454" Ref="U25"  Part="4" 
F 0 "U25" H 14345 12565 60  0000 C CNN
F 1 "74LS04" H 14340 12325 60  0000 C CNN
F 2 "14dip300" H 14340 12425 60  0001 C CNN
F 3 "" H 14150 12450 60  0001 C CNN
	4    14150 12450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 4BB3E0E1
P 12250 13600
AR Path="/2300384BB3E0E1" Ref="#PWR?"  Part="1" 
AR Path="/4BB3E0E1" Ref="#PWR029"  Part="1" 
AR Path="/FFFFFFFF4BB3E0E1" Ref="#PWR037"  Part="1" 
AR Path="/755D912A4BB3E0E1" Ref="#PWR?"  Part="1" 
AR Path="/23D83C4BB3E0E1" Ref="#PWR36"  Part="1" 
AR Path="/47907FA4BB3E0E1" Ref="#PWR045"  Part="1" 
AR Path="/23C34C4BB3E0E1" Ref="#PWR036"  Part="1" 
AR Path="/14BB3E0E1" Ref="#PWR045"  Part="1" 
AR Path="/23BC884BB3E0E1" Ref="#PWR037"  Part="1" 
AR Path="/773F8EB44BB3E0E1" Ref="#PWR035"  Part="1" 
AR Path="/94BB3E0E1" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB3E0E1" Ref="#PWR29"  Part="1" 
AR Path="/FFFFFFF04BB3E0E1" Ref="#PWR37"  Part="1" 
AR Path="/7E4188DA4BB3E0E1" Ref="#PWR36"  Part="1" 
AR Path="/8D384BB3E0E1" Ref="#PWR045"  Part="1" 
AR Path="/773F65F14BB3E0E1" Ref="#PWR037"  Part="1" 
AR Path="/6FF0DD404BB3E0E1" Ref="#PWR037"  Part="1" 
AR Path="/24BB3E0E1" Ref="#PWR29"  Part="1" 
AR Path="/23C6504BB3E0E1" Ref="#PWR046"  Part="1" 
AR Path="/D058E04BB3E0E1" Ref="#PWR037"  Part="1" 
AR Path="/6684D64BB3E0E1" Ref="#PWR035"  Part="1" 
AR Path="/3C64BB3E0E1" Ref="#PWR046"  Part="1" 
AR Path="/23CBC44BB3E0E1" Ref="#PWR035"  Part="1" 
AR Path="/2F4A4BB3E0E1" Ref="#PWR046"  Part="1" 
AR Path="/23D9004BB3E0E1" Ref="#PWR035"  Part="1" 
AR Path="/64BB3E0E1" Ref="#PWR34"  Part="1" 
AR Path="/6FE934344BB3E0E1" Ref="#PWR34"  Part="1" 
AR Path="/6FE934E34BB3E0E1" Ref="#PWR035"  Part="1" 
AR Path="/2824BB3E0E1" Ref="#PWR035"  Part="1" 
AR Path="/D1C3384BB3E0E1" Ref="#PWR037"  Part="1" 
F 0 "#PWR029" H 12250 13600 30  0001 C CNN
F 1 "GND" H 12250 13530 30  0001 C CNN
F 2 "" H 12250 13600 60  0001 C CNN
F 3 "" H 12250 13600 60  0001 C CNN
	1    12250 13600
	1    0    0    -1  
$EndComp
$Comp
L CONN_4X2 P?
U 1 1 4BB3E0B9
P 11850 13350
AR Path="/A000384BB3E0B9" Ref="P?"  Part="1" 
AR Path="/4BB3E0B9" Ref="P3"  Part="1" 
AR Path="/FFFFFFFF4BB3E0B9" Ref="P3"  Part="1" 
AR Path="/755D912A4BB3E0B9" Ref="P?"  Part="1" 
AR Path="/23D83C4BB3E0B9" Ref="P3"  Part="1" 
AR Path="/23D6DD4BB3E0B9" Ref="P?"  Part="1" 
AR Path="/2606084BB3E0B9" Ref="P3"  Part="1" 
AR Path="/47907FA4BB3E0B9" Ref="P3"  Part="1" 
AR Path="/23C34C4BB3E0B9" Ref="P3"  Part="1" 
AR Path="/14BB3E0B9" Ref="P3"  Part="1" 
AR Path="/23BC884BB3E0B9" Ref="P3"  Part="1" 
AR Path="/773F8EB44BB3E0B9" Ref="P3"  Part="1" 
AR Path="/94BB3E0B9" Ref="P"  Part="1" 
AR Path="/23C9F04BB3E0B9" Ref="P3"  Part="1" 
AR Path="/FFFFFFF04BB3E0B9" Ref="P3"  Part="1" 
AR Path="/7E4188DA4BB3E0B9" Ref="P3"  Part="1" 
AR Path="/8D384BB3E0B9" Ref="P3"  Part="1" 
AR Path="/24BB3E0B9" Ref="P3"  Part="1" 
AR Path="/773F65F14BB3E0B9" Ref="P3"  Part="1" 
AR Path="/23C6504BB3E0B9" Ref="P3"  Part="1" 
AR Path="/D058E04BB3E0B9" Ref="P3"  Part="1" 
AR Path="/3C64BB3E0B9" Ref="P3"  Part="1" 
AR Path="/23CBC44BB3E0B9" Ref="P3"  Part="1" 
AR Path="/2F4A4BB3E0B9" Ref="P3"  Part="1" 
AR Path="/23D9004BB3E0B9" Ref="P3"  Part="1" 
AR Path="/64BB3E0B9" Ref="P3"  Part="1" 
AR Path="/6FE934344BB3E0B9" Ref="P3"  Part="1" 
AR Path="/6FE934E34BB3E0B9" Ref="P3"  Part="1" 
AR Path="/2824BB3E0B9" Ref="P3"  Part="1" 
AR Path="/D1C3384BB3E0B9" Ref="P3"  Part="1" 
F 0 "P3" H 11850 13050 50  0000 C CNN
F 1 "CONN_4X2" V 11850 13350 40  0000 C CNN
F 2 "pin_array_4x2" V 11950 13350 40  0001 C CNN
F 3 "" H 11850 13350 60  0001 C CNN
	1    11850 13350
	1    0    0    -1  
$EndComp
$Comp
L 74LS682N IC?
U 1 1 4BB3E073
P 10950 12700
AR Path="/2300384BB3E073" Ref="IC?"  Part="1" 
AR Path="/4BB3E073" Ref="U15"  Part="1" 
AR Path="/453037334BB3E073" Ref="IC?"  Part="1" 
AR Path="/FFFFFFFF4BB3E073" Ref="U15"  Part="1" 
AR Path="/755D912A4BB3E073" Ref="IC?"  Part="1" 
AR Path="/23D8044BB3E073" Ref="IC?"  Part="1" 
AR Path="/2606084BB3E073" Ref="U15"  Part="1" 
AR Path="/23D83C4BB3E073" Ref="U15"  Part="1" 
AR Path="/47907FA4BB3E073" Ref="U15"  Part="1" 
AR Path="/23C34C4BB3E073" Ref="U15"  Part="1" 
AR Path="/14BB3E073" Ref="U15"  Part="1" 
AR Path="/23BC884BB3E073" Ref="U15"  Part="1" 
AR Path="/773F8EB44BB3E073" Ref="U15"  Part="1" 
AR Path="/94BB3E073" Ref="U"  Part="1" 
AR Path="/23C9F04BB3E073" Ref="U15"  Part="1" 
AR Path="/FFFFFFF04BB3E073" Ref="U15"  Part="1" 
AR Path="/7E4188DA4BB3E073" Ref="U15"  Part="1" 
AR Path="/8D384BB3E073" Ref="U15"  Part="1" 
AR Path="/773F65F14BB3E073" Ref="U15"  Part="1" 
AR Path="/24BB3E073" Ref="U15"  Part="1" 
AR Path="/23C6504BB3E073" Ref="U15"  Part="1" 
AR Path="/D058E04BB3E073" Ref="U15"  Part="1" 
AR Path="/3C64BB3E073" Ref="U15"  Part="1" 
AR Path="/23CBC44BB3E073" Ref="U15"  Part="1" 
AR Path="/2F4A4BB3E073" Ref="U15"  Part="1" 
AR Path="/23D9004BB3E073" Ref="U15"  Part="1" 
AR Path="/64BB3E073" Ref="U15"  Part="1" 
AR Path="/6FE934344BB3E073" Ref="U15"  Part="1" 
AR Path="/6FE934E34BB3E073" Ref="U15"  Part="1" 
AR Path="/2824BB3E073" Ref="U15"  Part="1" 
AR Path="/D1C3384BB3E073" Ref="U15"  Part="1" 
F 0 "U15" H 10650 13625 50  0000 L BNN
F 1 "74LS682N" H 10650 11700 50  0000 L BNN
F 2 "20dip300" H 10950 12850 50  0001 C CNN
F 3 "" H 10950 12700 60  0001 C CNN
	1    10950 12700
	-1   0    0    -1  
$EndComp
Text Label 14100 9700 2    60   ~ 0
bA7
Text Label 14100 9600 2    60   ~ 0
bA6
Text Label 14100 9500 2    60   ~ 0
bA5
Text Label 14100 9400 2    60   ~ 0
bA4
Text Label 14100 9300 2    60   ~ 0
bA3
Text Label 14100 9200 2    60   ~ 0
bA2
Text Label 14100 9100 2    60   ~ 0
bA1
Text Label 14100 9000 2    60   ~ 0
bA0
$Comp
L Z80 U?
U 1 1 4BB3D210
P 15350 10200
AR Path="/A000384BB3D210" Ref="U?"  Part="1" 
AR Path="/4BB3D210" Ref="U1"  Part="1" 
AR Path="/2606084BB3D210" Ref="U1"  Part="1" 
AR Path="/FFFFFFFF4BB3D210" Ref="U1"  Part="1" 
AR Path="/755D912A4BB3D210" Ref="U1"  Part="1" 
AR Path="/23D83C4BB3D210" Ref="U1"  Part="1" 
AR Path="/47907FA4BB3D210" Ref="U1"  Part="1" 
AR Path="/23C34C4BB3D210" Ref="U1"  Part="1" 
AR Path="/14BB3D210" Ref="U1"  Part="1" 
AR Path="/23BC884BB3D210" Ref="U1"  Part="1" 
AR Path="/773F8EB44BB3D210" Ref="U1"  Part="1" 
AR Path="/94BB3D210" Ref="U"  Part="1" 
AR Path="/23C9F04BB3D210" Ref="U1"  Part="1" 
AR Path="/FFFFFFF04BB3D210" Ref="U1"  Part="1" 
AR Path="/7E4188DA4BB3D210" Ref="U1"  Part="1" 
AR Path="/8D384BB3D210" Ref="U1"  Part="1" 
AR Path="/24BB3D210" Ref="U1"  Part="1" 
AR Path="/773F65F14BB3D210" Ref="U1"  Part="1" 
AR Path="/23C6504BB3D210" Ref="U1"  Part="1" 
AR Path="/D058E04BB3D210" Ref="U1"  Part="1" 
AR Path="/3C64BB3D210" Ref="U1"  Part="1" 
AR Path="/23CBC44BB3D210" Ref="U1"  Part="1" 
AR Path="/2F4A4BB3D210" Ref="U1"  Part="1" 
AR Path="/23D9004BB3D210" Ref="U1"  Part="1" 
AR Path="/64BB3D210" Ref="U1"  Part="1" 
AR Path="/6FE934344BB3D210" Ref="U1"  Part="1" 
AR Path="/6FE934E34BB3D210" Ref="U1"  Part="1" 
AR Path="/2824BB3D210" Ref="U1"  Part="1" 
AR Path="/D1C3384BB3D210" Ref="U1"  Part="1" 
F 0 "U1" H 15350 11550 60  0000 C CNN
F 1 "Z80" H 15350 8850 60  0000 C CNN
F 2 "40dip600" H 15350 8950 60  0001 C CNN
F 3 "" H 15350 10200 60  0001 C CNN
	1    15350 10200
	-1   0    0    -1  
$EndComp
$Comp
L 74LS373 U?
U 1 1 4BB29C79
P 6200 11250
AR Path="/679E884BB29C79" Ref="U?"  Part="1" 
AR Path="/2606084BB29C79" Ref="U3"  Part="1" 
AR Path="/394337394BB29C79" Ref="U3"  Part="1" 
AR Path="/4BB29C79" Ref="U3"  Part="1" 
AR Path="/FFFFFFFF4BB29C79" Ref="U3"  Part="1" 
AR Path="/23D83C4BB29C79" Ref="U3"  Part="1" 
AR Path="/47907FA4BB29C79" Ref="U3"  Part="1" 
AR Path="/23C34C4BB29C79" Ref="U3"  Part="1" 
AR Path="/14BB29C79" Ref="U3"  Part="1" 
AR Path="/23BC884BB29C79" Ref="U3"  Part="1" 
AR Path="/773F8EB44BB29C79" Ref="U3"  Part="1" 
AR Path="/94BB29C79" Ref="U"  Part="1" 
AR Path="/23C9F04BB29C79" Ref="U3"  Part="1" 
AR Path="/FFFFFFF04BB29C79" Ref="U3"  Part="1" 
AR Path="/8D384BB29C79" Ref="U3"  Part="1" 
AR Path="/24BB29C79" Ref="U3"  Part="1" 
AR Path="/773F65F14BB29C79" Ref="U3"  Part="1" 
AR Path="/23C6504BB29C79" Ref="U3"  Part="1" 
AR Path="/D058E04BB29C79" Ref="U3"  Part="1" 
AR Path="/3C64BB29C79" Ref="U3"  Part="1" 
AR Path="/23CBC44BB29C79" Ref="U3"  Part="1" 
AR Path="/2F4A4BB29C79" Ref="U3"  Part="1" 
AR Path="/23D9004BB29C79" Ref="U3"  Part="1" 
AR Path="/64BB29C79" Ref="U3"  Part="1" 
AR Path="/6FE934344BB29C79" Ref="U3"  Part="1" 
AR Path="/6FE934E34BB29C79" Ref="U3"  Part="1" 
AR Path="/2824BB29C79" Ref="U3"  Part="1" 
AR Path="/D1C3384BB29C79" Ref="U3"  Part="1" 
F 0 "U3" H 6200 11250 60  0000 C CNN
F 1 "74LS373" H 6250 10900 60  0000 C CNN
F 2 "20dip300" H 6250 11000 60  0001 C CNN
F 3 "" H 6200 11250 60  0001 C CNN
	1    6200 11250
	-1   0    0    -1  
$EndComp
Text Label 5100 11450 0    60   ~ 0
A15
Text Label 5100 11350 0    60   ~ 0
A14
Text Label 5100 11250 0    60   ~ 0
A13
Text Label 5100 11150 0    60   ~ 0
A12
Text Label 5100 11050 0    60   ~ 0
A11
Text Label 5100 10950 0    60   ~ 0
A10
Text Label 5100 10850 0    60   ~ 0
A9
Text Label 5100 10750 0    60   ~ 0
A8
Text Label 6850 7450 0    60   ~ 0
GND
Text Label 3900 8200 0    60   ~ 0
A16
Text Label 3900 8100 0    60   ~ 0
A17
Text Label 3900 8000 0    60   ~ 0
A18
Text Label 3900 7900 0    60   ~ 0
A19
Text Label 3900 7800 0    60   ~ 0
A20
Text Label 3900 7700 0    60   ~ 0
A21
Text Label 3900 7600 0    60   ~ 0
A22
Text Label 3900 7500 0    60   ~ 0
A23
$Comp
L 74LS373 U?
U 1 1 4BB29085
P 6100 8000
AR Path="/2300384BB29085" Ref="U?"  Part="1" 
AR Path="/4BB29085" Ref="U12"  Part="1" 
AR Path="/7E42B4154BB29085" Ref="U12"  Part="1" 
AR Path="/393038354BB29085" Ref="U12"  Part="1" 
AR Path="/FFFFFFFF4BB29085" Ref="U12"  Part="1" 
AR Path="/23D83C4BB29085" Ref="U12"  Part="1" 
AR Path="/47907FA4BB29085" Ref="U12"  Part="1" 
AR Path="/23C34C4BB29085" Ref="U12"  Part="1" 
AR Path="/14BB29085" Ref="U12"  Part="1" 
AR Path="/23BC884BB29085" Ref="U12"  Part="1" 
AR Path="/773F8EB44BB29085" Ref="U12"  Part="1" 
AR Path="/94BB29085" Ref="U"  Part="1" 
AR Path="/23C9F04BB29085" Ref="U12"  Part="1" 
AR Path="/FFFFFFF04BB29085" Ref="U12"  Part="1" 
AR Path="/8D384BB29085" Ref="U12"  Part="1" 
AR Path="/24BB29085" Ref="U12"  Part="1" 
AR Path="/773F65F14BB29085" Ref="U12"  Part="1" 
AR Path="/23C6504BB29085" Ref="U12"  Part="1" 
AR Path="/D058E04BB29085" Ref="U12"  Part="1" 
AR Path="/3C64BB29085" Ref="U12"  Part="1" 
AR Path="/23CBC44BB29085" Ref="U12"  Part="1" 
AR Path="/2F4A4BB29085" Ref="U12"  Part="1" 
AR Path="/23D9004BB29085" Ref="U12"  Part="1" 
AR Path="/64BB29085" Ref="U12"  Part="1" 
AR Path="/6FE934344BB29085" Ref="U12"  Part="1" 
AR Path="/6FE934E34BB29085" Ref="U12"  Part="1" 
AR Path="/D1C3384BB29085" Ref="U12"  Part="1" 
F 0 "U12" H 6100 8000 60  0000 C CNN
F 1 "74LS373" H 6150 7650 60  0000 C CNN
F 2 "20dip300" H 6150 7750 60  0001 C CNN
F 3 "" H 6100 8000 60  0001 C CNN
	1    6100 8000
	-1   0    0    -1  
$EndComp
$Comp
L 74LS04 U?
U 1 1 4BB29048
P 4550 7200
AR Path="/2300384BB29048" Ref="U?"  Part="1" 
AR Path="/4BB29048" Ref="U24"  Part="1" 
AR Path="/393034384BB29048" Ref="U?"  Part="1" 
AR Path="/297DF04BB29048" Ref="U?"  Part="1" 
AR Path="/37E09A84BB29048" Ref="U"  Part="1" 
AR Path="/FFFFFFFF4BB29048" Ref="U24"  Part="1" 
AR Path="/31324BB29048" Ref="U1"  Part="1" 
AR Path="/2606084BB29048" Ref="U24"  Part="1" 
AR Path="/23D83C4BB29048" Ref="U24"  Part="1" 
AR Path="/47907FA4BB29048" Ref="U24"  Part="1" 
AR Path="/23C34C4BB29048" Ref="U24"  Part="1" 
AR Path="/14BB29048" Ref="U24"  Part="1" 
AR Path="/23BC884BB29048" Ref="U24"  Part="1" 
AR Path="/773F8EB44BB29048" Ref="U24"  Part="1" 
AR Path="/94BB29048" Ref="U"  Part="1" 
AR Path="/23C9F04BB29048" Ref="U24"  Part="1" 
AR Path="/FFFFFFF04BB29048" Ref="U24"  Part="1" 
AR Path="/8D384BB29048" Ref="U24"  Part="1" 
AR Path="/773F65F14BB29048" Ref="U24"  Part="1" 
AR Path="/23C6504BB29048" Ref="U24"  Part="1" 
AR Path="/D058E04BB29048" Ref="U24"  Part="1" 
AR Path="/3C64BB29048" Ref="U24"  Part="1" 
AR Path="/23CBC44BB29048" Ref="U24"  Part="1" 
AR Path="/2F4A4BB29048" Ref="U24"  Part="1" 
AR Path="/23D9004BB29048" Ref="U24"  Part="1" 
AR Path="/64BB29048" Ref="U24"  Part="1" 
AR Path="/6FE934344BB29048" Ref="U24"  Part="1" 
AR Path="/24BB29048" Ref="U24"  Part="1" 
AR Path="/6FE934E34BB29048" Ref="U24"  Part="1" 
AR Path="/D1C3384BB29048" Ref="U24"  Part="1" 
F 0 "U24" H 4745 7315 60  0000 C CNN
F 1 "74LS04" H 4740 7075 60  0000 C CNN
F 2 "14dip300" H 4740 7175 60  0001 C CNN
F 3 "" H 4550 7200 60  0001 C CNN
	1    4550 7200
	1    0    0    -1  
$EndComp
Text Label 2850 7200 0    60   ~ 0
ADSB*
$Comp
L VCC #PWR?
U 1 1 4BB28FCA
P 6600 2500
AR Path="/2300384BB28FCA" Ref="#PWR?"  Part="1" 
AR Path="/4BB28FCA" Ref="#PWR030"  Part="1" 
AR Path="/FFFFFFFF4BB28FCA" Ref="#PWR043"  Part="1" 
AR Path="/23D83C4BB28FCA" Ref="#PWR18"  Part="1" 
AR Path="/47907FA4BB28FCA" Ref="#PWR052"  Part="1" 
AR Path="/23C34C4BB28FCA" Ref="#PWR042"  Part="1" 
AR Path="/14BB28FCA" Ref="#PWR052"  Part="1" 
AR Path="/23BC884BB28FCA" Ref="#PWR043"  Part="1" 
AR Path="/773F8EB44BB28FCA" Ref="#PWR041"  Part="1" 
AR Path="/94BB28FCA" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB28FCA" Ref="#PWR13"  Part="1" 
AR Path="/FFFFFFF04BB28FCA" Ref="#PWR18"  Part="1" 
AR Path="/8D384BB28FCA" Ref="#PWR052"  Part="1" 
AR Path="/773F65F14BB28FCA" Ref="#PWR043"  Part="1" 
AR Path="/6FF0DD404BB28FCA" Ref="#PWR043"  Part="1" 
AR Path="/23C6504BB28FCA" Ref="#PWR053"  Part="1" 
AR Path="/D058E04BB28FCA" Ref="#PWR043"  Part="1" 
AR Path="/6684D64BB28FCA" Ref="#PWR041"  Part="1" 
AR Path="/3C64BB28FCA" Ref="#PWR053"  Part="1" 
AR Path="/23CBC44BB28FCA" Ref="#PWR041"  Part="1" 
AR Path="/2F4A4BB28FCA" Ref="#PWR053"  Part="1" 
AR Path="/23D9004BB28FCA" Ref="#PWR041"  Part="1" 
AR Path="/64BB28FCA" Ref="#PWR16"  Part="1" 
AR Path="/6FE934344BB28FCA" Ref="#PWR16"  Part="1" 
AR Path="/24BB28FCA" Ref="#PWR13"  Part="1" 
AR Path="/6FE934E34BB28FCA" Ref="#PWR041"  Part="1" 
AR Path="/D1C3384BB28FCA" Ref="#PWR043"  Part="1" 
F 0 "#PWR030" H 6600 2600 30  0001 C CNN
F 1 "VCC" H 6600 2600 30  0000 C CNN
F 2 "" H 6600 2500 60  0001 C CNN
F 3 "" H 6600 2500 60  0001 C CNN
	1    6600 2500
	1    0    0    -1  
$EndComp
$Comp
L CP C43
U 1 1 4BB28FBA
P 6200 2700
AR Path="/4BB28FBA" Ref="C43"  Part="1" 
AR Path="/FFFFFFFF4BB28FBA" Ref="C43"  Part="1" 
AR Path="/23D83C4BB28FBA" Ref="C43"  Part="1" 
AR Path="/47907FA4BB28FBA" Ref="C?"  Part="1" 
AR Path="/23C34C4BB28FBA" Ref="C43"  Part="1" 
AR Path="/14BB28FBA" Ref="C?"  Part="1" 
AR Path="/23BC884BB28FBA" Ref="C43"  Part="1" 
AR Path="/773F8EB44BB28FBA" Ref="C43"  Part="1" 
AR Path="/94BB28FBA" Ref="C"  Part="1" 
AR Path="/23C9F04BB28FBA" Ref="C43"  Part="1" 
AR Path="/FFFFFFF04BB28FBA" Ref="C43"  Part="1" 
AR Path="/8D384BB28FBA" Ref="C43"  Part="1" 
AR Path="/24BB28FBA" Ref="C43"  Part="1" 
AR Path="/773F65F14BB28FBA" Ref="C43"  Part="1" 
AR Path="/23C6504BB28FBA" Ref="C43"  Part="1" 
AR Path="/D058E04BB28FBA" Ref="C43"  Part="1" 
AR Path="/3C64BB28FBA" Ref="C43"  Part="1" 
AR Path="/23CBC44BB28FBA" Ref="C43"  Part="1" 
AR Path="/2F4A4BB28FBA" Ref="C43"  Part="1" 
AR Path="/23D9004BB28FBA" Ref="C43"  Part="1" 
AR Path="/64BB28FBA" Ref="C43"  Part="1" 
AR Path="/6FE934344BB28FBA" Ref="C43"  Part="1" 
AR Path="/6FE934E34BB28FBA" Ref="C43"  Part="1" 
AR Path="/D1C3384BB28FBA" Ref="C43"  Part="1" 
F 0 "C43" H 6250 2800 50  0000 L CNN
F 1 "33 uF" H 6250 2600 50  0000 L CNN
F 2 "C1V7" H 6250 2700 50  0001 C CNN
F 3 "" H 6200 2700 60  0001 C CNN
	1    6200 2700
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR031
U 1 1 4BB28FB9
P 6200 2900
AR Path="/4BB28FB9" Ref="#PWR031"  Part="1" 
AR Path="/FFFFFFFF4BB28FB9" Ref="#PWR044"  Part="1" 
AR Path="/23D83C4BB28FB9" Ref="#PWR17"  Part="1" 
AR Path="/47907FA4BB28FB9" Ref="#PWR053"  Part="1" 
AR Path="/23C34C4BB28FB9" Ref="#PWR043"  Part="1" 
AR Path="/14BB28FB9" Ref="#PWR053"  Part="1" 
AR Path="/23BC884BB28FB9" Ref="#PWR044"  Part="1" 
AR Path="/773F8EB44BB28FB9" Ref="#PWR042"  Part="1" 
AR Path="/94BB28FB9" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB28FB9" Ref="#PWR12"  Part="1" 
AR Path="/FFFFFFF04BB28FB9" Ref="#PWR17"  Part="1" 
AR Path="/8D384BB28FB9" Ref="#PWR053"  Part="1" 
AR Path="/773F65F14BB28FB9" Ref="#PWR044"  Part="1" 
AR Path="/6FF0DD404BB28FB9" Ref="#PWR044"  Part="1" 
AR Path="/23C6504BB28FB9" Ref="#PWR054"  Part="1" 
AR Path="/D058E04BB28FB9" Ref="#PWR044"  Part="1" 
AR Path="/6684D64BB28FB9" Ref="#PWR042"  Part="1" 
AR Path="/3C64BB28FB9" Ref="#PWR054"  Part="1" 
AR Path="/23CBC44BB28FB9" Ref="#PWR042"  Part="1" 
AR Path="/2F4A4BB28FB9" Ref="#PWR054"  Part="1" 
AR Path="/23D9004BB28FB9" Ref="#PWR042"  Part="1" 
AR Path="/64BB28FB9" Ref="#PWR15"  Part="1" 
AR Path="/6FE934344BB28FB9" Ref="#PWR15"  Part="1" 
AR Path="/24BB28FB9" Ref="#PWR12"  Part="1" 
AR Path="/6FE934E34BB28FB9" Ref="#PWR042"  Part="1" 
AR Path="/D1C3384BB28FB9" Ref="#PWR044"  Part="1" 
F 0 "#PWR031" H 6200 2900 30  0001 C CNN
F 1 "GND" H 6200 2830 30  0001 C CNN
F 2 "" H 6200 2900 60  0001 C CNN
F 3 "" H 6200 2900 60  0001 C CNN
	1    6200 2900
	1    0    0    -1  
$EndComp
$Comp
L DIODE D?
U 1 1 4BB28F83
P 6400 2500
AR Path="/2300384BB28F83" Ref="D?"  Part="1" 
AR Path="/4BB28F83" Ref="D6"  Part="1" 
AR Path="/FFFFFFFF4BB28F83" Ref="D6"  Part="1" 
AR Path="/23D83C4BB28F83" Ref="D6"  Part="1" 
AR Path="/31324BB28F83" Ref="D?"  Part="1" 
AR Path="/7E4188DA4BB28F83" Ref="D6"  Part="1" 
AR Path="/47907FA4BB28F83" Ref="D6"  Part="1" 
AR Path="/23C34C4BB28F83" Ref="D6"  Part="1" 
AR Path="/14BB28F83" Ref="D6"  Part="1" 
AR Path="/23BC884BB28F83" Ref="D6"  Part="1" 
AR Path="/773F8EB44BB28F83" Ref="D6"  Part="1" 
AR Path="/94BB28F83" Ref="D"  Part="1" 
AR Path="/23C9F04BB28F83" Ref="D6"  Part="1" 
AR Path="/FFFFFFF04BB28F83" Ref="D6"  Part="1" 
AR Path="/8D384BB28F83" Ref="D6"  Part="1" 
AR Path="/24BB28F83" Ref="D6"  Part="1" 
AR Path="/773F65F14BB28F83" Ref="D6"  Part="1" 
AR Path="/23C6504BB28F83" Ref="D6"  Part="1" 
AR Path="/D058E04BB28F83" Ref="D6"  Part="1" 
AR Path="/3C64BB28F83" Ref="D6"  Part="1" 
AR Path="/23CBC44BB28F83" Ref="D6"  Part="1" 
AR Path="/2F4A4BB28F83" Ref="D6"  Part="1" 
AR Path="/23D9004BB28F83" Ref="D6"  Part="1" 
AR Path="/64BB28F83" Ref="D6"  Part="1" 
AR Path="/6FE934344BB28F83" Ref="D6"  Part="1" 
AR Path="/6FE934E34BB28F83" Ref="D6"  Part="1" 
AR Path="/D1C3384BB28F83" Ref="D6"  Part="1" 
F 0 "D6" H 6400 2600 40  0000 C CNN
F 1 "1N4148" H 6400 2400 40  0000 C CNN
F 2 "D3" H 6400 2500 40  0001 C CNN
F 3 "" H 6400 2500 60  0001 C CNN
	1    6400 2500
	1    0    0    -1  
$EndComp
$Comp
L JUMPER JP?
U 1 1 4BB28F56
P 6200 1900
AR Path="/2300384BB28F56" Ref="JP?"  Part="1" 
AR Path="/4BB28F56" Ref="JP5"  Part="1" 
AR Path="/FFFFFFFF4BB28F56" Ref="JP5"  Part="1" 
AR Path="/23D83C4BB28F56" Ref="JP5"  Part="1" 
AR Path="/47907FA4BB28F56" Ref="JP1"  Part="1" 
AR Path="/23C34C4BB28F56" Ref="JP5"  Part="1" 
AR Path="/14BB28F56" Ref="JP?"  Part="1" 
AR Path="/23BC884BB28F56" Ref="JP5"  Part="1" 
AR Path="/7E4188DA4BB28F56" Ref="JP1"  Part="1" 
AR Path="/23D8044BB28F56" Ref="JP1"  Part="1" 
AR Path="/2606084BB28F56" Ref="JP5"  Part="1" 
AR Path="/773F8EB44BB28F56" Ref="JP5"  Part="1" 
AR Path="/94BB28F56" Ref="JP"  Part="1" 
AR Path="/23C9F04BB28F56" Ref="JP5"  Part="1" 
AR Path="/FFFFFFF04BB28F56" Ref="JP5"  Part="1" 
AR Path="/8D384BB28F56" Ref="JP5"  Part="1" 
AR Path="/24BB28F56" Ref="JP5"  Part="1" 
AR Path="/773F65F14BB28F56" Ref="JP5"  Part="1" 
AR Path="/23C6504BB28F56" Ref="JP5"  Part="1" 
AR Path="/D058E04BB28F56" Ref="JP5"  Part="1" 
AR Path="/3C64BB28F56" Ref="JP5"  Part="1" 
AR Path="/23CBC44BB28F56" Ref="JP5"  Part="1" 
AR Path="/2F4A4BB28F56" Ref="JP5"  Part="1" 
AR Path="/23D9004BB28F56" Ref="JP5"  Part="1" 
AR Path="/64BB28F56" Ref="JP5"  Part="1" 
AR Path="/6FE934344BB28F56" Ref="JP5"  Part="1" 
AR Path="/6FE934E34BB28F56" Ref="JP5"  Part="1" 
AR Path="/D1C3384BB28F56" Ref="JP5"  Part="1" 
F 0 "JP5" H 6200 2050 60  0000 C CNN
F 1 "JUMPER" H 6200 1820 40  0000 C CNN
F 2 "SIL-2" H 6200 1920 40  0001 C CNN
F 3 "" H 6200 1900 60  0001 C CNN
	1    6200 1900
	0    1    1    0   
$EndComp
Text Label 850  3800 0    60   ~ 0
SLAVE_CLR*
$Comp
L 74LS05 U?
U 3 1 4BB28EF7
P 2550 2900
AR Path="/2300384BB28EF7" Ref="U?"  Part="1" 
AR Path="/4BB28EF7" Ref="U26"  Part="3" 
AR Path="/384546374BB28EF7" Ref="U?"  Part="1" 
AR Path="/2A2F484BB28EF7" Ref="U?"  Part="1" 
AR Path="/1F70A3E4BB28EF7" Ref="U"  Part="3" 
AR Path="/FFFFFFFF4BB28EF7" Ref="U26"  Part="3" 
AR Path="/23D8044BB28EF7" Ref="U3"  Part="3" 
AR Path="/2606084BB28EF7" Ref="U26"  Part="3" 
AR Path="/23D83C4BB28EF7" Ref="U26"  Part="3" 
AR Path="/47907FA4BB28EF7" Ref="U26"  Part="3" 
AR Path="/23C34C4BB28EF7" Ref="U26"  Part="3" 
AR Path="/14BB28EF7" Ref="U26"  Part="3" 
AR Path="/23BC884BB28EF7" Ref="U26"  Part="3" 
AR Path="/773F8EB44BB28EF7" Ref="U26"  Part="3" 
AR Path="/94BB28EF7" Ref="U"  Part="3" 
AR Path="/23C9F04BB28EF7" Ref="U26"  Part="3" 
AR Path="/FFFFFFF04BB28EF7" Ref="U26"  Part="3" 
AR Path="/8D384BB28EF7" Ref="U26"  Part="3" 
AR Path="/773F65F14BB28EF7" Ref="U26"  Part="3" 
AR Path="/23C6504BB28EF7" Ref="U26"  Part="3" 
AR Path="/D058E04BB28EF7" Ref="U26"  Part="3" 
AR Path="/3C64BB28EF7" Ref="U26"  Part="3" 
AR Path="/23CBC44BB28EF7" Ref="U26"  Part="3" 
AR Path="/2F4A4BB28EF7" Ref="U26"  Part="3" 
AR Path="/23D9004BB28EF7" Ref="U26"  Part="3" 
AR Path="/64BB28EF7" Ref="U26"  Part="3" 
AR Path="/6FE934344BB28EF7" Ref="U26"  Part="3" 
AR Path="/24BB28EF7" Ref="U26"  Part="3" 
AR Path="/6FE934E34BB28EF7" Ref="U26"  Part="3" 
AR Path="/D1C3384BB28EF7" Ref="U26"  Part="3" 
F 0 "U26" H 2745 3015 60  0000 C CNN
F 1 "74LS05" H 2740 2775 60  0000 C CNN
F 2 "14dip300" H 2740 2875 60  0001 C CNN
F 3 "" H 2550 2900 60  0001 C CNN
	3    2550 2900
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR?
U 1 1 4BB28ED2
P 5400 3100
AR Path="/2300384BB28ED2" Ref="#PWR?"  Part="1" 
AR Path="/4BB28ED2" Ref="#PWR032"  Part="1" 
AR Path="/FFFFFFFF4BB28ED2" Ref="#PWR045"  Part="1" 
AR Path="/23D83C4BB28ED2" Ref="#PWR16"  Part="1" 
AR Path="/47907FA4BB28ED2" Ref="#PWR055"  Part="1" 
AR Path="/23C34C4BB28ED2" Ref="#PWR044"  Part="1" 
AR Path="/14BB28ED2" Ref="#PWR055"  Part="1" 
AR Path="/23BC884BB28ED2" Ref="#PWR045"  Part="1" 
AR Path="/773F8EB44BB28ED2" Ref="#PWR043"  Part="1" 
AR Path="/94BB28ED2" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB28ED2" Ref="#PWR11"  Part="1" 
AR Path="/FFFFFFF04BB28ED2" Ref="#PWR16"  Part="1" 
AR Path="/8D384BB28ED2" Ref="#PWR055"  Part="1" 
AR Path="/773F65F14BB28ED2" Ref="#PWR045"  Part="1" 
AR Path="/6FF0DD404BB28ED2" Ref="#PWR045"  Part="1" 
AR Path="/23C6504BB28ED2" Ref="#PWR056"  Part="1" 
AR Path="/D058E04BB28ED2" Ref="#PWR045"  Part="1" 
AR Path="/6684D64BB28ED2" Ref="#PWR043"  Part="1" 
AR Path="/3C64BB28ED2" Ref="#PWR056"  Part="1" 
AR Path="/23CBC44BB28ED2" Ref="#PWR043"  Part="1" 
AR Path="/2F4A4BB28ED2" Ref="#PWR056"  Part="1" 
AR Path="/23D9004BB28ED2" Ref="#PWR043"  Part="1" 
AR Path="/64BB28ED2" Ref="#PWR14"  Part="1" 
AR Path="/6FE934344BB28ED2" Ref="#PWR14"  Part="1" 
AR Path="/24BB28ED2" Ref="#PWR11"  Part="1" 
AR Path="/6FE934E34BB28ED2" Ref="#PWR043"  Part="1" 
AR Path="/D1C3384BB28ED2" Ref="#PWR045"  Part="1" 
F 0 "#PWR032" H 5400 3100 30  0001 C CNN
F 1 "GND" H 5400 3030 30  0001 C CNN
F 2 "" H 5400 3100 60  0001 C CNN
F 3 "" H 5400 3100 60  0001 C CNN
	1    5400 3100
	1    0    0    -1  
$EndComp
$Comp
L CP C?
U 1 1 4BB28E9E
P 5400 2900
AR Path="/2300384BB28E9E" Ref="C?"  Part="1" 
AR Path="/4BB28E9E" Ref="C42"  Part="1" 
AR Path="/23D63C4BB28E9E" Ref="C?"  Part="1" 
AR Path="/D1E6204BB28E9E" Ref="C?"  Part="1" 
AR Path="/23D6704BB28E9E" Ref="C?"  Part="1" 
AR Path="/7E4293134BB28E9E" Ref="C?"  Part="1" 
AR Path="/FFFFFFFF4BB28E9E" Ref="C42"  Part="1" 
AR Path="/23D83C4BB28E9E" Ref="C42"  Part="1" 
AR Path="/47907FA4BB28E9E" Ref="C?"  Part="1" 
AR Path="/23C34C4BB28E9E" Ref="C42"  Part="1" 
AR Path="/14BB28E9E" Ref="C?"  Part="1" 
AR Path="/23BC884BB28E9E" Ref="C42"  Part="1" 
AR Path="/773F8EB44BB28E9E" Ref="C42"  Part="1" 
AR Path="/23C9F04BB28E9E" Ref="C42"  Part="1" 
AR Path="/94BB28E9E" Ref="C"  Part="1" 
AR Path="/FFFFFFF04BB28E9E" Ref="C42"  Part="1" 
AR Path="/8D384BB28E9E" Ref="C42"  Part="1" 
AR Path="/24BB28E9E" Ref="C42"  Part="1" 
AR Path="/773F65F14BB28E9E" Ref="C42"  Part="1" 
AR Path="/23C6504BB28E9E" Ref="C42"  Part="1" 
AR Path="/D058E04BB28E9E" Ref="C42"  Part="1" 
AR Path="/3C64BB28E9E" Ref="C42"  Part="1" 
AR Path="/23CBC44BB28E9E" Ref="C42"  Part="1" 
AR Path="/2F4A4BB28E9E" Ref="C42"  Part="1" 
AR Path="/23D9004BB28E9E" Ref="C42"  Part="1" 
AR Path="/64BB28E9E" Ref="C42"  Part="1" 
AR Path="/6FE934344BB28E9E" Ref="C42"  Part="1" 
AR Path="/6FE934E34BB28E9E" Ref="C42"  Part="1" 
AR Path="/23D8044BB28E9E" Ref="C42"  Part="1" 
AR Path="/D1C3384BB28E9E" Ref="C42"  Part="1" 
F 0 "C42" H 5450 3000 50  0000 L CNN
F 1 "22 uF" H 5450 2800 50  0000 L CNN
F 2 "C1V7" H 5450 2900 50  0001 C CNN
F 3 "" H 5400 2900 60  0001 C CNN
	1    5400 2900
	-1   0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 4BB28E93
P 5400 2200
AR Path="/2300384BB28E93" Ref="#PWR?"  Part="1" 
AR Path="/4BB28E93" Ref="#PWR033"  Part="1" 
AR Path="/FFFFFFFF4BB28E93" Ref="#PWR046"  Part="1" 
AR Path="/23D83C4BB28E93" Ref="#PWR15"  Part="1" 
AR Path="/47907FA4BB28E93" Ref="#PWR056"  Part="1" 
AR Path="/23C34C4BB28E93" Ref="#PWR045"  Part="1" 
AR Path="/14BB28E93" Ref="#PWR056"  Part="1" 
AR Path="/23BC884BB28E93" Ref="#PWR046"  Part="1" 
AR Path="/773F8EB44BB28E93" Ref="#PWR044"  Part="1" 
AR Path="/94BB28E93" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB28E93" Ref="#PWR10"  Part="1" 
AR Path="/FFFFFFF04BB28E93" Ref="#PWR15"  Part="1" 
AR Path="/8D384BB28E93" Ref="#PWR056"  Part="1" 
AR Path="/773F65F14BB28E93" Ref="#PWR046"  Part="1" 
AR Path="/6FF0DD404BB28E93" Ref="#PWR046"  Part="1" 
AR Path="/23C6504BB28E93" Ref="#PWR057"  Part="1" 
AR Path="/D058E04BB28E93" Ref="#PWR046"  Part="1" 
AR Path="/6684D64BB28E93" Ref="#PWR044"  Part="1" 
AR Path="/3C64BB28E93" Ref="#PWR057"  Part="1" 
AR Path="/23CBC44BB28E93" Ref="#PWR044"  Part="1" 
AR Path="/2F4A4BB28E93" Ref="#PWR057"  Part="1" 
AR Path="/23D9004BB28E93" Ref="#PWR044"  Part="1" 
AR Path="/64BB28E93" Ref="#PWR13"  Part="1" 
AR Path="/6FE934344BB28E93" Ref="#PWR13"  Part="1" 
AR Path="/24BB28E93" Ref="#PWR10"  Part="1" 
AR Path="/6FE934E34BB28E93" Ref="#PWR044"  Part="1" 
AR Path="/D1C3384BB28E93" Ref="#PWR046"  Part="1" 
F 0 "#PWR033" H 5400 2300 30  0001 C CNN
F 1 "VCC" H 5400 2300 30  0000 C CNN
F 2 "" H 5400 2200 60  0001 C CNN
F 3 "" H 5400 2200 60  0001 C CNN
	1    5400 2200
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 4BB28E8C
P 5400 2450
AR Path="/2300384BB28E8C" Ref="R?"  Part="1" 
AR Path="/4BB28E8C" Ref="R11"  Part="1" 
AR Path="/FFFFFFFF4BB28E8C" Ref="R11"  Part="1" 
AR Path="/23D83C4BB28E8C" Ref="R11"  Part="1" 
AR Path="/47907FA4BB28E8C" Ref="R?"  Part="1" 
AR Path="/23C34C4BB28E8C" Ref="R11"  Part="1" 
AR Path="/14BB28E8C" Ref="R?"  Part="1" 
AR Path="/23BC884BB28E8C" Ref="R11"  Part="1" 
AR Path="/773F8EB44BB28E8C" Ref="R11"  Part="1" 
AR Path="/94BB28E8C" Ref="R"  Part="1" 
AR Path="/23C9F04BB28E8C" Ref="R11"  Part="1" 
AR Path="/FFFFFFF04BB28E8C" Ref="R11"  Part="1" 
AR Path="/8D384BB28E8C" Ref="R11"  Part="1" 
AR Path="/24BB28E8C" Ref="R11"  Part="1" 
AR Path="/773F65F14BB28E8C" Ref="R11"  Part="1" 
AR Path="/23C6504BB28E8C" Ref="R11"  Part="1" 
AR Path="/D058E04BB28E8C" Ref="R11"  Part="1" 
AR Path="/3C64BB28E8C" Ref="R11"  Part="1" 
AR Path="/23CBC44BB28E8C" Ref="R11"  Part="1" 
AR Path="/2F4A4BB28E8C" Ref="R11"  Part="1" 
AR Path="/23D9004BB28E8C" Ref="R11"  Part="1" 
AR Path="/64BB28E8C" Ref="R11"  Part="1" 
AR Path="/6FE934344BB28E8C" Ref="R11"  Part="1" 
AR Path="/6FE934E34BB28E8C" Ref="R11"  Part="1" 
AR Path="/D1C3384BB28E8C" Ref="R11"  Part="1" 
F 0 "R11" V 5480 2450 50  0000 C CNN
F 1 "4700" V 5400 2450 50  0000 C CNN
F 2 "R3" V 5500 2450 50  0001 C CNN
F 3 "" H 5400 2450 60  0001 C CNN
	1    5400 2450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 4BB28E7B
P 5100 2900
AR Path="/2300384BB28E7B" Ref="#PWR?"  Part="1" 
AR Path="/4BB28E7B" Ref="#PWR034"  Part="1" 
AR Path="/FFFFFFFF4BB28E7B" Ref="#PWR047"  Part="1" 
AR Path="/23D83C4BB28E7B" Ref="#PWR14"  Part="1" 
AR Path="/47907FA4BB28E7B" Ref="#PWR057"  Part="1" 
AR Path="/23C34C4BB28E7B" Ref="#PWR046"  Part="1" 
AR Path="/14BB28E7B" Ref="#PWR057"  Part="1" 
AR Path="/23BC884BB28E7B" Ref="#PWR047"  Part="1" 
AR Path="/773F8EB44BB28E7B" Ref="#PWR045"  Part="1" 
AR Path="/94BB28E7B" Ref="#PWR"  Part="1" 
AR Path="/23C9F04BB28E7B" Ref="#PWR9"  Part="1" 
AR Path="/FFFFFFF04BB28E7B" Ref="#PWR14"  Part="1" 
AR Path="/8D384BB28E7B" Ref="#PWR057"  Part="1" 
AR Path="/773F65F14BB28E7B" Ref="#PWR047"  Part="1" 
AR Path="/6FF0DD404BB28E7B" Ref="#PWR047"  Part="1" 
AR Path="/23C6504BB28E7B" Ref="#PWR058"  Part="1" 
AR Path="/D058E04BB28E7B" Ref="#PWR047"  Part="1" 
AR Path="/6684D64BB28E7B" Ref="#PWR045"  Part="1" 
AR Path="/3C64BB28E7B" Ref="#PWR058"  Part="1" 
AR Path="/23CBC44BB28E7B" Ref="#PWR045"  Part="1" 
AR Path="/2F4A4BB28E7B" Ref="#PWR058"  Part="1" 
AR Path="/23D9004BB28E7B" Ref="#PWR045"  Part="1" 
AR Path="/64BB28E7B" Ref="#PWR12"  Part="1" 
AR Path="/6FE934344BB28E7B" Ref="#PWR12"  Part="1" 
AR Path="/24BB28E7B" Ref="#PWR9"  Part="1" 
AR Path="/6FE934E34BB28E7B" Ref="#PWR045"  Part="1" 
AR Path="/D1C3384BB28E7B" Ref="#PWR047"  Part="1" 
F 0 "#PWR034" H 5100 2900 30  0001 C CNN
F 1 "GND" H 5100 2830 30  0001 C CNN
F 2 "" H 5100 2900 60  0001 C CNN
F 3 "" H 5100 2900 60  0001 C CNN
	1    5100 2900
	1    0    0    -1  
$EndComp
$Comp
L NPN Q?
U 1 1 4BB28E5E
P 5200 2700
AR Path="/2300384BB28E5E" Ref="Q?"  Part="1" 
AR Path="/4BB28E5E" Ref="Q1"  Part="1" 
AR Path="/384535454BB28E5E" Ref="Q?"  Part="1" 
AR Path="/FFFFFFFF4BB28E5E" Ref="Q1"  Part="1" 
AR Path="/23D83C4BB28E5E" Ref="Q1"  Part="1" 
AR Path="/23D8044BB28E5E" Ref="Q?"  Part="1" 
AR Path="/B8C4BB28E5E" Ref="Q?"  Part="1" 
AR Path="/47907FA4BB28E5E" Ref="Q?"  Part="1" 
AR Path="/23C34C4BB28E5E" Ref="Q1"  Part="1" 
AR Path="/14BB28E5E" Ref="Q?"  Part="1" 
AR Path="/23BC884BB28E5E" Ref="Q1"  Part="1" 
AR Path="/773F8EB44BB28E5E" Ref="Q1"  Part="1" 
AR Path="/23C9F04BB28E5E" Ref="Q1"  Part="1" 
AR Path="/94BB28E5E" Ref="Q"  Part="1" 
AR Path="/FFFFFFF04BB28E5E" Ref="Q1"  Part="1" 
AR Path="/8D384BB28E5E" Ref="Q1"  Part="1" 
AR Path="/24BB28E5E" Ref="Q1"  Part="1" 
AR Path="/773F65F14BB28E5E" Ref="Q1"  Part="1" 
AR Path="/23C6504BB28E5E" Ref="Q1"  Part="1" 
AR Path="/D058E04BB28E5E" Ref="Q1"  Part="1" 
AR Path="/3C64BB28E5E" Ref="Q1"  Part="1" 
AR Path="/23CBC44BB28E5E" Ref="Q1"  Part="1" 
AR Path="/2F4A4BB28E5E" Ref="Q1"  Part="1" 
AR Path="/23D9004BB28E5E" Ref="Q1"  Part="1" 
AR Path="/64BB28E5E" Ref="Q1"  Part="1" 
AR Path="/6FE934344BB28E5E" Ref="Q1"  Part="1" 
AR Path="/6FE934E34BB28E5E" Ref="Q1"  Part="1" 
AR Path="/D1C3384BB28E5E" Ref="Q1"  Part="1" 
F 0 "Q1" H 5350 2700 50  0000 C CNN
F 1 "2N3904" H 5500 2600 50  0000 C CNN
F 2 "TO92-INVERT" H 5500 2700 50  0001 C CNN
F 3 "" H 5200 2700 60  0001 C CNN
	1    5200 2700
	-1   0    0    -1  
$EndComp
$Comp
L 74LS05 U?
U 2 1 4BB28E03
P 3450 2500
AR Path="/2300384BB28E03" Ref="U?"  Part="1" 
AR Path="/4BB28E03" Ref="U26"  Part="2" 
AR Path="/384530334BB28E03" Ref="U?"  Part="1" 
AR Path="/2A25E04BB28E03" Ref="U?"  Part="1" 
AR Path="/4970A504BB28E03" Ref="U"  Part="2" 
AR Path="/FFFFFFFF4BB28E03" Ref="U26"  Part="2" 
AR Path="/23D8044BB28E03" Ref="U3"  Part="2" 
AR Path="/2606084BB28E03" Ref="U26"  Part="2" 
AR Path="/23D83C4BB28E03" Ref="U26"  Part="2" 
AR Path="/47907FA4BB28E03" Ref="U26"  Part="2" 
AR Path="/23C34C4BB28E03" Ref="U26"  Part="2" 
AR Path="/14BB28E03" Ref="U26"  Part="2" 
AR Path="/23BC884BB28E03" Ref="U26"  Part="2" 
AR Path="/773F8EB44BB28E03" Ref="U26"  Part="2" 
AR Path="/94BB28E03" Ref="U"  Part="2" 
AR Path="/23C9F04BB28E03" Ref="U26"  Part="2" 
AR Path="/FFFFFFF04BB28E03" Ref="U26"  Part="2" 
AR Path="/8D384BB28E03" Ref="U26"  Part="2" 
AR Path="/773F65F14BB28E03" Ref="U26"  Part="2" 
AR Path="/23C6504BB28E03" Ref="U26"  Part="2" 
AR Path="/D058E04BB28E03" Ref="U26"  Part="2" 
AR Path="/3C64BB28E03" Ref="U26"  Part="2" 
AR Path="/23CBC44BB28E03" Ref="U26"  Part="2" 
AR Path="/2F4A4BB28E03" Ref="U26"  Part="2" 
AR Path="/23D9004BB28E03" Ref="U26"  Part="2" 
AR Path="/64BB28E03" Ref="U26"  Part="2" 
AR Path="/6FE934344BB28E03" Ref="U26"  Part="2" 
AR Path="/24BB28E03" Ref="U26"  Part="2" 
AR Path="/6FE934E34BB28E03" Ref="U26"  Part="2" 
AR Path="/D1C3384BB28E03" Ref="U26"  Part="2" 
F 0 "U26" H 3645 2615 60  0000 C CNN
F 1 "74LS05" H 3640 2375 60  0000 C CNN
F 2 "14dip300" H 3640 2475 60  0001 C CNN
F 3 "" H 3450 2500 60  0001 C CNN
	2    3450 2500
	-1   0    0    1   
$EndComp
$Comp
L JUMPER JP?
U 1 1 4BB28DF7
P 2700 2500
AR Path="/9B00384BB28DF7" Ref="JP?"  Part="1" 
AR Path="/4BB28DF7" Ref="JP4"  Part="1" 
AR Path="/FFFFFFFF4BB28DF7" Ref="JP4"  Part="1" 
AR Path="/23D83C4BB28DF7" Ref="JP4"  Part="1" 
AR Path="/47907FA4BB28DF7" Ref="JP2"  Part="1" 
AR Path="/23C34C4BB28DF7" Ref="JP4"  Part="1" 
AR Path="/14BB28DF7" Ref="JP?"  Part="1" 
AR Path="/23BC884BB28DF7" Ref="JP4"  Part="1" 
AR Path="/7E4188DA4BB28DF7" Ref="JP2"  Part="1" 
AR Path="/2606084BB28DF7" Ref="JP4"  Part="1" 
AR Path="/773F8EB44BB28DF7" Ref="JP4"  Part="1" 
AR Path="/94BB28DF7" Ref="JP"  Part="1" 
AR Path="/23C9F04BB28DF7" Ref="JP4"  Part="1" 
AR Path="/FFFFFFF04BB28DF7" Ref="JP4"  Part="1" 
AR Path="/8D384BB28DF7" Ref="JP4"  Part="1" 
AR Path="/24BB28DF7" Ref="JP4"  Part="1" 
AR Path="/773F65F14BB28DF7" Ref="JP4"  Part="1" 
AR Path="/23C6504BB28DF7" Ref="JP4"  Part="1" 
AR Path="/D058E04BB28DF7" Ref="JP4"  Part="1" 
AR Path="/3C64BB28DF7" Ref="JP4"  Part="1" 
AR Path="/23CBC44BB28DF7" Ref="JP4"  Part="1" 
AR Path="/2F4A4BB28DF7" Ref="JP4"  Part="1" 
AR Path="/23D9004BB28DF7" Ref="JP4"  Part="1" 
AR Path="/64BB28DF7" Ref="JP4"  Part="1" 
AR Path="/6FE934344BB28DF7" Ref="JP4"  Part="1" 
AR Path="/6FE934E34BB28DF7" Ref="JP4"  Part="1" 
AR Path="/D1C3384BB28DF7" Ref="JP4"  Part="1" 
F 0 "JP4" H 2700 2650 60  0000 C CNN
F 1 "JUMPER" H 2700 2420 40  0000 C CNN
F 2 "SIL-2" H 2700 2520 40  0001 C CNN
F 3 "" H 2700 2500 60  0001 C CNN
	1    2700 2500
	1    0    0    -1  
$EndComp
Text Label 1150 2500 0    60   ~ 0
POC*
$Comp
L 74LS05 U?
U 6 1 4BB28DA3
P 2600 1850
AR Path="/9B00384BB28DA3" Ref="U?"  Part="1" 
AR Path="/4BB28DA3" Ref="U26"  Part="6" 
AR Path="/384441334BB28DA3" Ref="U?"  Part="1" 
AR Path="/2848E04BB28DA3" Ref="U?"  Part="1" 
AR Path="/287096A4BB28DA3" Ref="U"  Part="6" 
AR Path="/FFFFFFFF4BB28DA3" Ref="U26"  Part="6" 
AR Path="/10031324BB28DA3" Ref="U3"  Part="6" 
AR Path="/2606084BB28DA3" Ref="U26"  Part="6" 
AR Path="/23D83C4BB28DA3" Ref="U26"  Part="6" 
AR Path="/47907FA4BB28DA3" Ref="U26"  Part="6" 
AR Path="/23C34C4BB28DA3" Ref="U26"  Part="6" 
AR Path="/14BB28DA3" Ref="U26"  Part="6" 
AR Path="/23BC884BB28DA3" Ref="U26"  Part="6" 
AR Path="/773F8EB44BB28DA3" Ref="U26"  Part="6" 
AR Path="/94BB28DA3" Ref="U"  Part="6" 
AR Path="/23C9F04BB28DA3" Ref="U26"  Part="6" 
AR Path="/FFFFFFF04BB28DA3" Ref="U26"  Part="6" 
AR Path="/8D384BB28DA3" Ref="U26"  Part="6" 
AR Path="/773F65F14BB28DA3" Ref="U26"  Part="6" 
AR Path="/23C6504BB28DA3" Ref="U26"  Part="6" 
AR Path="/D058E04BB28DA3" Ref="U26"  Part="6" 
AR Path="/3C64BB28DA3" Ref="U26"  Part="6" 
AR Path="/23CBC44BB28DA3" Ref="U26"  Part="6" 
AR Path="/2F4A4BB28DA3" Ref="U26"  Part="6" 
AR Path="/23D9004BB28DA3" Ref="U26"  Part="6" 
AR Path="/64BB28DA3" Ref="U26"  Part="6" 
AR Path="/6FE934344BB28DA3" Ref="U26"  Part="6" 
AR Path="/24BB28DA3" Ref="U26"  Part="6" 
AR Path="/6FE934E34BB28DA3" Ref="U26"  Part="6" 
AR Path="/D1C3384BB28DA3" Ref="U26"  Part="6" 
F 0 "U26" H 2795 1965 60  0000 C CNN
F 1 "74LS05" H 2790 1725 60  0000 C CNN
F 2 "14dip300" H 2790 1825 60  0001 C CNN
F 3 "" H 2600 1850 60  0001 C CNN
	6    2600 1850
	-1   0    0    1   
$EndComp
$Comp
L 74LS04 U?
U 2 1 4BB28D08
P 6900 16750
AR Path="/9B00384BB28D08" Ref="U?"  Part="1" 
AR Path="/4BB28D08" Ref="U25"  Part="2" 
AR Path="/384430384BB28D08" Ref="U?"  Part="1" 
AR Path="/284BD84BB28D08" Ref="U?"  Part="1" 
AR Path="/1F00A3E4BB28D08" Ref="U"  Part="2" 
AR Path="/FFFFFFFF4BB28D08" Ref="U25"  Part="2" 
AR Path="/23D83C4BB28D08" Ref="U25"  Part="2" 
AR Path="/47907FA4BB28D08" Ref="U2"  Part="2" 
AR Path="/23C34C4BB28D08" Ref="U25"  Part="2" 
AR Path="/14BB28D08" Ref="U2"  Part="2" 
AR Path="/23BC884BB28D08" Ref="U25"  Part="2" 
AR Path="/773F8EB44BB28D08" Ref="U25"  Part="2" 
AR Path="/94BB28D08" Ref="U"  Part="2" 
AR Path="/23C9F04BB28D08" Ref="U25"  Part="2" 
AR Path="/FFFFFFF04BB28D08" Ref="U25"  Part="2" 
AR Path="/2606084BB28D08" Ref="U25"  Part="2" 
AR Path="/8D384BB28D08" Ref="U25"  Part="2" 
AR Path="/773F65F14BB28D08" Ref="U25"  Part="2" 
AR Path="/23C6504BB28D08" Ref="U25"  Part="2" 
AR Path="/D058E04BB28D08" Ref="U25"  Part="2" 
AR Path="/3C64BB28D08" Ref="U25"  Part="2" 
AR Path="/23CBC44BB28D08" Ref="U25"  Part="2" 
AR Path="/2F4A4BB28D08" Ref="U25"  Part="2" 
AR Path="/23D9004BB28D08" Ref="U25"  Part="2" 
AR Path="/64BB28D08" Ref="U25"  Part="2" 
AR Path="/6FE934344BB28D08" Ref="U25"  Part="2" 
AR Path="/24BB28D08" Ref="U25"  Part="2" 
AR Path="/6FE934E34BB28D08" Ref="U25"  Part="2" 
AR Path="/D1C3384BB28D08" Ref="U25"  Part="2" 
F 0 "U25" H 7095 16865 60  0000 C CNN
F 1 "74LS04" H 7090 16625 60  0000 C CNN
F 2 "14dip300" H 7090 16725 60  0001 C CNN
F 3 "" H 6900 16750 60  0001 C CNN
	2    6900 16750
	1    0    0    -1  
$EndComp
Text Label 1150 950  0    60   ~ 0
RESET*
$Comp
L GND #PWR035
U 1 1 4B3D3479
P 36950 22500
AR Path="/4B3D3479" Ref="#PWR035"  Part="1" 
AR Path="/94B3D3479" Ref="#PWR30"  Part="1" 
AR Path="/25E4B3D3479" Ref="#PWR09"  Part="1" 
AR Path="/FFFFFFF04B3D3479" Ref="#PWR69"  Part="1" 
AR Path="/DCBAABCD4B3D3479" Ref="#PWR01"  Part="1" 
AR Path="/6FE901F74B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/4032778D4B3D3479" Ref="#PWR01"  Part="1" 
AR Path="/A84B3D3479" Ref="#PWR29"  Part="1" 
AR Path="/363030314B3D3479" Ref="#PWR09"  Part="1" 
AR Path="/5AD7153D4B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/A4B3D3479" Ref="#PWR043"  Part="1" 
AR Path="/3FEFFFFF4B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/403091264B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/403051264B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/4032F78D4B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/403251264B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/4032AAC04B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/4030D1264B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/4031778D4B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/3FEA24DD4B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/23D8D44B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/2600004B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/6FF0DD404B3D3479" Ref="#PWR048"  Part="1" 
AR Path="/6FE934E34B3D3479" Ref="#PWR046"  Part="1" 
AR Path="/773F65F14B3D3479" Ref="#PWR048"  Part="1" 
AR Path="/773F8EB44B3D3479" Ref="#PWR046"  Part="1" 
AR Path="/23C9F04B3D3479" Ref="#PWR51"  Part="1" 
AR Path="/23BC884B3D3479" Ref="#PWR048"  Part="1" 
AR Path="/DC0C124B3D3479" Ref="#PWR010"  Part="1" 
AR Path="/6684D64B3D3479" Ref="#PWR046"  Part="1" 
AR Path="/23C34C4B3D3479" Ref="#PWR047"  Part="1" 
AR Path="/23CBC44B3D3479" Ref="#PWR046"  Part="1" 
AR Path="/69549BC04B3D3479" Ref="#PWR011"  Part="1" 
AR Path="/23C6504B3D3479" Ref="#PWR062"  Part="1" 
AR Path="/39803EA4B3D3479" Ref="#PWR015"  Part="1" 
AR Path="/FFFFFFFF4B3D3479" Ref="#PWR048"  Part="1" 
AR Path="/6FE934344B3D3479" Ref="#PWR62"  Part="1" 
AR Path="/7E0073254B3D3479" Ref="#PWR01"  Part="1" 
AR Path="/73254B3D3479" Ref="#PWR074"  Part="1" 
AR Path="/8CEE4B3D3479" Ref="#PWR074"  Part="1" 
AR Path="/23D9004B3D3479" Ref="#PWR046"  Part="1" 
AR Path="/91964B3D3479" Ref="#PWR054"  Part="1" 
AR Path="/77F16B254B3D3479" Ref="#PWR041"  Part="1" 
AR Path="/E7D24B3D3479" Ref="#PWR01"  Part="1" 
AR Path="/10000004B3D3479" Ref="#PWR01"  Part="1" 
AR Path="/23D83C4B3D3479" Ref="#PWR65"  Part="1" 
AR Path="/47907FA4B3D3479" Ref="#PWR061"  Part="1" 
AR Path="/14B3D3479" Ref="#PWR061"  Part="1" 
AR Path="/8D384B3D3479" Ref="#PWR061"  Part="1" 
AR Path="/D058E04B3D3479" Ref="#PWR048"  Part="1" 
AR Path="/3C64B3D3479" Ref="#PWR062"  Part="1" 
AR Path="/2F4A4B3D3479" Ref="#PWR062"  Part="1" 
AR Path="/24B3D3479" Ref="#PWR51"  Part="1" 
AR Path="/D1C3384B3D3479" Ref="#PWR048"  Part="1" 
F 0 "#PWR035" H 36950 22500 30  0001 C CNN
F 1 "GND" H 36950 22430 30  0001 C CNN
F 2 "" H 36950 22500 60  0001 C CNN
F 3 "" H 36950 22500 60  0001 C CNN
	1    36950 22500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR036
U 1 1 4B3D3475
P 36950 21900
AR Path="/4B3D3475" Ref="#PWR036"  Part="1" 
AR Path="/94B3D3475" Ref="#PWR29"  Part="1" 
AR Path="/25E4B3D3475" Ref="#PWR010"  Part="1" 
AR Path="/FFFFFFF04B3D3475" Ref="#PWR68"  Part="1" 
AR Path="/DCBAABCD4B3D3475" Ref="#PWR02"  Part="1" 
AR Path="/6FE901F74B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/4032778D4B3D3475" Ref="#PWR02"  Part="1" 
AR Path="/A84B3D3475" Ref="#PWR28"  Part="1" 
AR Path="/363030314B3D3475" Ref="#PWR010"  Part="1" 
AR Path="/5AD7153D4B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/A4B3D3475" Ref="#PWR044"  Part="1" 
AR Path="/3FEFFFFF4B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/403091264B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/403051264B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/4032F78D4B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/403251264B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/4032AAC04B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/4030D1264B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/4031778D4B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/3FEA24DD4B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/23D8D44B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/2600004B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/6FF0DD404B3D3475" Ref="#PWR049"  Part="1" 
AR Path="/6FE934E34B3D3475" Ref="#PWR047"  Part="1" 
AR Path="/773F65F14B3D3475" Ref="#PWR049"  Part="1" 
AR Path="/773F8EB44B3D3475" Ref="#PWR047"  Part="1" 
AR Path="/23C9F04B3D3475" Ref="#PWR50"  Part="1" 
AR Path="/23BC884B3D3475" Ref="#PWR049"  Part="1" 
AR Path="/DC0C124B3D3475" Ref="#PWR011"  Part="1" 
AR Path="/6684D64B3D3475" Ref="#PWR047"  Part="1" 
AR Path="/23C34C4B3D3475" Ref="#PWR048"  Part="1" 
AR Path="/23CBC44B3D3475" Ref="#PWR047"  Part="1" 
AR Path="/69549BC04B3D3475" Ref="#PWR012"  Part="1" 
AR Path="/23C6504B3D3475" Ref="#PWR063"  Part="1" 
AR Path="/39803EA4B3D3475" Ref="#PWR016"  Part="1" 
AR Path="/FFFFFFFF4B3D3475" Ref="#PWR049"  Part="1" 
AR Path="/6FE934344B3D3475" Ref="#PWR61"  Part="1" 
AR Path="/7E0073254B3D3475" Ref="#PWR02"  Part="1" 
AR Path="/73254B3D3475" Ref="#PWR075"  Part="1" 
AR Path="/8CEE4B3D3475" Ref="#PWR075"  Part="1" 
AR Path="/23D9004B3D3475" Ref="#PWR047"  Part="1" 
AR Path="/91964B3D3475" Ref="#PWR055"  Part="1" 
AR Path="/77F16B254B3D3475" Ref="#PWR042"  Part="1" 
AR Path="/E7D24B3D3475" Ref="#PWR02"  Part="1" 
AR Path="/23D83C4B3D3475" Ref="#PWR64"  Part="1" 
AR Path="/47907FA4B3D3475" Ref="#PWR062"  Part="1" 
AR Path="/14B3D3475" Ref="#PWR062"  Part="1" 
AR Path="/8D384B3D3475" Ref="#PWR062"  Part="1" 
AR Path="/D058E04B3D3475" Ref="#PWR049"  Part="1" 
AR Path="/3C64B3D3475" Ref="#PWR063"  Part="1" 
AR Path="/2F4A4B3D3475" Ref="#PWR063"  Part="1" 
AR Path="/24B3D3475" Ref="#PWR50"  Part="1" 
AR Path="/D1C3384B3D3475" Ref="#PWR049"  Part="1" 
F 0 "#PWR036" H 36950 21900 30  0001 C CNN
F 1 "GND" H 36950 21830 30  0001 C CNN
F 2 "" H 36950 21900 60  0001 C CNN
F 3 "" H 36950 21900 60  0001 C CNN
	1    36950 21900
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 4B3D3469
P 36500 22050
AR Path="/23D9D84B3D3469" Ref="#PWR?"  Part="1" 
AR Path="/394433324B3D3469" Ref="#PWR?"  Part="1" 
AR Path="/4B3D3469" Ref="#PWR037"  Part="1" 
AR Path="/94B3D3469" Ref="#PWR27"  Part="1" 
AR Path="/25E4B3D3469" Ref="#PWR011"  Part="1" 
AR Path="/FFFFFFF04B3D3469" Ref="#PWR64"  Part="1" 
AR Path="/DCBAABCD4B3D3469" Ref="#PWR03"  Part="1" 
AR Path="/6FE901F74B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/4032778D4B3D3469" Ref="#PWR03"  Part="1" 
AR Path="/A84B3D3469" Ref="#PWR26"  Part="1" 
AR Path="/363030314B3D3469" Ref="#PWR011"  Part="1" 
AR Path="/5AD7153D4B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/A4B3D3469" Ref="#PWR045"  Part="1" 
AR Path="/3FEFFFFF4B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/403091264B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/403051264B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/4032F78D4B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/403251264B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/4032AAC04B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/4030D1264B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/4031778D4B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/3FEA24DD4B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/23D8D44B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/2600004B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/6FF0DD404B3D3469" Ref="#PWR050"  Part="1" 
AR Path="/6FE934E34B3D3469" Ref="#PWR048"  Part="1" 
AR Path="/773F65F14B3D3469" Ref="#PWR050"  Part="1" 
AR Path="/773F8EB44B3D3469" Ref="#PWR048"  Part="1" 
AR Path="/23C9F04B3D3469" Ref="#PWR46"  Part="1" 
AR Path="/23BC884B3D3469" Ref="#PWR050"  Part="1" 
AR Path="/DC0C124B3D3469" Ref="#PWR012"  Part="1" 
AR Path="/6684D64B3D3469" Ref="#PWR048"  Part="1" 
AR Path="/23C34C4B3D3469" Ref="#PWR049"  Part="1" 
AR Path="/23CBC44B3D3469" Ref="#PWR048"  Part="1" 
AR Path="/69549BC04B3D3469" Ref="#PWR013"  Part="1" 
AR Path="/23C6504B3D3469" Ref="#PWR064"  Part="1" 
AR Path="/39803EA4B3D3469" Ref="#PWR017"  Part="1" 
AR Path="/FFFFFFFF4B3D3469" Ref="#PWR050"  Part="1" 
AR Path="/6FE934344B3D3469" Ref="#PWR57"  Part="1" 
AR Path="/7E0073254B3D3469" Ref="#PWR03"  Part="1" 
AR Path="/73254B3D3469" Ref="#PWR076"  Part="1" 
AR Path="/8CEE4B3D3469" Ref="#PWR076"  Part="1" 
AR Path="/23D9004B3D3469" Ref="#PWR048"  Part="1" 
AR Path="/91964B3D3469" Ref="#PWR056"  Part="1" 
AR Path="/77F16B254B3D3469" Ref="#PWR043"  Part="1" 
AR Path="/E7D24B3D3469" Ref="#PWR03"  Part="1" 
AR Path="/23D83C4B3D3469" Ref="#PWR61"  Part="1" 
AR Path="/47907FA4B3D3469" Ref="#PWR063"  Part="1" 
AR Path="/14B3D3469" Ref="#PWR063"  Part="1" 
AR Path="/8D384B3D3469" Ref="#PWR063"  Part="1" 
AR Path="/D058E04B3D3469" Ref="#PWR050"  Part="1" 
AR Path="/3C64B3D3469" Ref="#PWR064"  Part="1" 
AR Path="/2F4A4B3D3469" Ref="#PWR064"  Part="1" 
AR Path="/24B3D3469" Ref="#PWR46"  Part="1" 
AR Path="/D1C3384B3D3469" Ref="#PWR050"  Part="1" 
F 0 "#PWR037" H 36500 22150 30  0001 C CNN
F 1 "VCC" H 36500 22150 30  0000 C CNN
F 2 "" H 36500 22050 60  0001 C CNN
F 3 "" H 36500 22050 60  0001 C CNN
	1    36500 22050
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 4B3D344D
P 36500 21450
AR Path="/23D9D84B3D344D" Ref="#PWR?"  Part="1" 
AR Path="/394433324B3D344D" Ref="#PWR?"  Part="1" 
AR Path="/4B3D344D" Ref="#PWR038"  Part="1" 
AR Path="/94B3D344D" Ref="#PWR26"  Part="1" 
AR Path="/25E4B3D344D" Ref="#PWR012"  Part="1" 
AR Path="/FFFFFFF04B3D344D" Ref="#PWR63"  Part="1" 
AR Path="/DCBAABCD4B3D344D" Ref="#PWR04"  Part="1" 
AR Path="/6FE901F74B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/4032778D4B3D344D" Ref="#PWR04"  Part="1" 
AR Path="/A84B3D344D" Ref="#PWR25"  Part="1" 
AR Path="/363030314B3D344D" Ref="#PWR012"  Part="1" 
AR Path="/5AD7153D4B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/A4B3D344D" Ref="#PWR046"  Part="1" 
AR Path="/3FEFFFFF4B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/403091264B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/403051264B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/4032F78D4B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/403251264B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/4032AAC04B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/4030D1264B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/4031778D4B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/3FEA24DD4B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/23D8D44B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/2600004B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/6FF0DD404B3D344D" Ref="#PWR051"  Part="1" 
AR Path="/6FE934E34B3D344D" Ref="#PWR049"  Part="1" 
AR Path="/773F65F14B3D344D" Ref="#PWR051"  Part="1" 
AR Path="/773F8EB44B3D344D" Ref="#PWR049"  Part="1" 
AR Path="/23C9F04B3D344D" Ref="#PWR45"  Part="1" 
AR Path="/23BC884B3D344D" Ref="#PWR051"  Part="1" 
AR Path="/DC0C124B3D344D" Ref="#PWR013"  Part="1" 
AR Path="/6684D64B3D344D" Ref="#PWR049"  Part="1" 
AR Path="/23C34C4B3D344D" Ref="#PWR050"  Part="1" 
AR Path="/23CBC44B3D344D" Ref="#PWR049"  Part="1" 
AR Path="/69549BC04B3D344D" Ref="#PWR014"  Part="1" 
AR Path="/23C6504B3D344D" Ref="#PWR065"  Part="1" 
AR Path="/39803EA4B3D344D" Ref="#PWR018"  Part="1" 
AR Path="/FFFFFFFF4B3D344D" Ref="#PWR051"  Part="1" 
AR Path="/6FE934344B3D344D" Ref="#PWR56"  Part="1" 
AR Path="/7E0073254B3D344D" Ref="#PWR04"  Part="1" 
AR Path="/73254B3D344D" Ref="#PWR077"  Part="1" 
AR Path="/8CEE4B3D344D" Ref="#PWR077"  Part="1" 
AR Path="/23D9004B3D344D" Ref="#PWR049"  Part="1" 
AR Path="/91964B3D344D" Ref="#PWR057"  Part="1" 
AR Path="/77F16B254B3D344D" Ref="#PWR044"  Part="1" 
AR Path="/E7D24B3D344D" Ref="#PWR04"  Part="1" 
AR Path="/23D83C4B3D344D" Ref="#PWR60"  Part="1" 
AR Path="/47907FA4B3D344D" Ref="#PWR064"  Part="1" 
AR Path="/14B3D344D" Ref="#PWR064"  Part="1" 
AR Path="/8D384B3D344D" Ref="#PWR064"  Part="1" 
AR Path="/D058E04B3D344D" Ref="#PWR051"  Part="1" 
AR Path="/3C64B3D344D" Ref="#PWR065"  Part="1" 
AR Path="/2F4A4B3D344D" Ref="#PWR065"  Part="1" 
AR Path="/24B3D344D" Ref="#PWR45"  Part="1" 
AR Path="/D1C3384B3D344D" Ref="#PWR051"  Part="1" 
F 0 "#PWR038" H 36500 21550 30  0001 C CNN
F 1 "VCC" H 36500 21550 30  0000 C CNN
F 2 "" H 36500 21450 60  0001 C CNN
F 3 "" H 36500 21450 60  0001 C CNN
	1    36500 21450
	1    0    0    -1  
$EndComp
$Comp
L C C41
U 1 1 4B3D33B5
P 40550 21650
AR Path="/4B3D33B5" Ref="C41"  Part="1" 
AR Path="/94B3D33B5" Ref="C41"  Part="1" 
AR Path="/FFFFFFF04B3D33B5" Ref="C41"  Part="1" 
AR Path="/4032778D4B3D33B5" Ref="C41"  Part="1" 
AR Path="/A84B3D33B5" Ref="C41"  Part="1" 
AR Path="/5AD7153D4B3D33B5" Ref="C41"  Part="1" 
AR Path="/A4B3D33B5" Ref="C41"  Part="1" 
AR Path="/6FE901F74B3D33B5" Ref="C41"  Part="1" 
AR Path="/3FEFFFFF4B3D33B5" Ref="C41"  Part="1" 
AR Path="/403091264B3D33B5" Ref="C41"  Part="1" 
AR Path="/403051264B3D33B5" Ref="C41"  Part="1" 
AR Path="/4032F78D4B3D33B5" Ref="C41"  Part="1" 
AR Path="/403251264B3D33B5" Ref="C41"  Part="1" 
AR Path="/4032AAC04B3D33B5" Ref="C41"  Part="1" 
AR Path="/4030D1264B3D33B5" Ref="C41"  Part="1" 
AR Path="/4031778D4B3D33B5" Ref="C41"  Part="1" 
AR Path="/3FEA24DD4B3D33B5" Ref="C41"  Part="1" 
AR Path="/23D8D44B3D33B5" Ref="C41"  Part="1" 
AR Path="/2600004B3D33B5" Ref="C41"  Part="1" 
AR Path="/14B3D33B5" Ref="C41"  Part="1" 
AR Path="/6FF0DD404B3D33B5" Ref="C41"  Part="1" 
AR Path="/6FE934E34B3D33B5" Ref="C41"  Part="1" 
AR Path="/773F65F14B3D33B5" Ref="C41"  Part="1" 
AR Path="/773F8EB44B3D33B5" Ref="C41"  Part="1" 
AR Path="/23C9F04B3D33B5" Ref="C41"  Part="1" 
AR Path="/24B3D33B5" Ref="C41"  Part="1" 
AR Path="/23BC884B3D33B5" Ref="C41"  Part="1" 
AR Path="/DC0C124B3D33B5" Ref="C41"  Part="1" 
AR Path="/23C34C4B3D33B5" Ref="C41"  Part="1" 
AR Path="/23CBC44B3D33B5" Ref="C41"  Part="1" 
AR Path="/69549BC04B3D33B5" Ref="C41"  Part="1" 
AR Path="/23C6504B3D33B5" Ref="C41"  Part="1" 
AR Path="/39803EA4B3D33B5" Ref="C41"  Part="1" 
AR Path="/FFFFFFFF4B3D33B5" Ref="C41"  Part="1" 
AR Path="/6FE934344B3D33B5" Ref="C41"  Part="1" 
AR Path="/7E0073254B3D33B5" Ref="C41"  Part="1" 
AR Path="/73254B3D33B5" Ref="C41"  Part="1" 
AR Path="/8CEE4B3D33B5" Ref="C41"  Part="1" 
AR Path="/23D9004B3D33B5" Ref="C41"  Part="1" 
AR Path="/91964B3D33B5" Ref="C41"  Part="1" 
AR Path="/77F16B254B3D33B5" Ref="C41"  Part="1" 
AR Path="/E7D24B3D33B5" Ref="C41"  Part="1" 
AR Path="/23D83C4B3D33B5" Ref="C41"  Part="1" 
AR Path="/47907FA4B3D33B5" Ref="C41"  Part="1" 
AR Path="/8D384B3D33B5" Ref="C41"  Part="1" 
AR Path="/D058E04B3D33B5" Ref="C41"  Part="1" 
AR Path="/3C64B3D33B5" Ref="C41"  Part="1" 
AR Path="/2F4A4B3D33B5" Ref="C41"  Part="1" 
AR Path="/D1C3384B3D33B5" Ref="C41"  Part="1" 
F 0 "C41" H 40600 21750 50  0000 L CNN
F 1 "0.1 uF" H 40600 21550 50  0000 L CNN
F 2 "C2" H 40550 21650 60  0001 C CNN
F 3 "" H 40550 21650 60  0001 C CNN
	1    40550 21650
	1    0    0    -1  
$EndComp
$Comp
L C C38
U 1 1 4B37DA08
P 36950 21650
AR Path="/FFFFFFFF4B37DA08" Ref="C38"  Part="1" 
AR Path="/4B37DA08" Ref="C38"  Part="1" 
AR Path="/94B37DA08" Ref="C38"  Part="1" 
AR Path="/A4B37DA08" Ref="C38"  Part="1" 
AR Path="/6FE901F74B37DA08" Ref="C38"  Part="1" 
AR Path="/402755814B37DA08" Ref="C38"  Part="1" 
AR Path="/3FEFFFFF4B37DA08" Ref="C38"  Part="1" 
AR Path="/4030AAC04B37DA08" Ref="C38"  Part="1" 
AR Path="/FFFFFFF04B37DA08" Ref="C38"  Part="1" 
AR Path="/5AD7153D4B37DA08" Ref="C38"  Part="1" 
AR Path="/A84B37DA08" Ref="C38"  Part="1" 
AR Path="/14B37DA08" Ref="C38"  Part="1" 
AR Path="/6FF0DD404B37DA08" Ref="C38"  Part="1" 
AR Path="/23D9304B37DA08" Ref="C38"  Part="1" 
AR Path="/23D8D44B37DA08" Ref="C38"  Part="1" 
AR Path="/4031DDF34B37DA08" Ref="C38"  Part="1" 
AR Path="/3FE88B434B37DA08" Ref="C38"  Part="1" 
AR Path="/4032778D4B37DA08" Ref="C38"  Part="1" 
AR Path="/403091264B37DA08" Ref="C38"  Part="1" 
AR Path="/403051264B37DA08" Ref="C38"  Part="1" 
AR Path="/4032F78D4B37DA08" Ref="C38"  Part="1" 
AR Path="/403251264B37DA08" Ref="C38"  Part="1" 
AR Path="/4032AAC04B37DA08" Ref="C38"  Part="1" 
AR Path="/4030D1264B37DA08" Ref="C38"  Part="1" 
AR Path="/4031778D4B37DA08" Ref="C38"  Part="1" 
AR Path="/3FEA24DD4B37DA08" Ref="C38"  Part="1" 
AR Path="/2600004B37DA08" Ref="C38"  Part="1" 
AR Path="/6FE934E34B37DA08" Ref="C38"  Part="1" 
AR Path="/773F65F14B37DA08" Ref="C38"  Part="1" 
AR Path="/773F8EB44B37DA08" Ref="C38"  Part="1" 
AR Path="/23C9F04B37DA08" Ref="C38"  Part="1" 
AR Path="/24B37DA08" Ref="C38"  Part="1" 
AR Path="/23BC884B37DA08" Ref="C38"  Part="1" 
AR Path="/DC0C124B37DA08" Ref="C38"  Part="1" 
AR Path="/23C34C4B37DA08" Ref="C38"  Part="1" 
AR Path="/23CBC44B37DA08" Ref="C38"  Part="1" 
AR Path="/23C6504B37DA08" Ref="C38"  Part="1" 
AR Path="/39803EA4B37DA08" Ref="C38"  Part="1" 
AR Path="/6FE934344B37DA08" Ref="C38"  Part="1" 
AR Path="/7E0073254B37DA08" Ref="C38"  Part="1" 
AR Path="/73254B37DA08" Ref="C38"  Part="1" 
AR Path="/8CEE4B37DA08" Ref="C38"  Part="1" 
AR Path="/23D9004B37DA08" Ref="C38"  Part="1" 
AR Path="/91964B37DA08" Ref="C38"  Part="1" 
AR Path="/77F16B254B37DA08" Ref="C38"  Part="1" 
AR Path="/23D83C4B37DA08" Ref="C38"  Part="1" 
AR Path="/47907FA4B37DA08" Ref="C38"  Part="1" 
AR Path="/8D384B37DA08" Ref="C38"  Part="1" 
AR Path="/D058E04B37DA08" Ref="C38"  Part="1" 
AR Path="/3C64B37DA08" Ref="C38"  Part="1" 
AR Path="/2F4A4B37DA08" Ref="C38"  Part="1" 
AR Path="/D1C3384B37DA08" Ref="C38"  Part="1" 
F 0 "C38" H 37000 21750 50  0000 L CNN
F 1 "0.1 uF" H 37000 21550 50  0000 L CNN
F 2 "C2" H 36950 21650 60  0001 C CNN
F 3 "" H 36950 21650 60  0001 C CNN
	1    36950 21650
	1    0    0    -1  
$EndComp
$Comp
L C C39
U 1 1 4B37DA03
P 37400 21650
AR Path="/FFFFFFFF4B37DA03" Ref="C39"  Part="1" 
AR Path="/4B37DA03" Ref="C39"  Part="1" 
AR Path="/94B37DA03" Ref="C39"  Part="1" 
AR Path="/A4B37DA03" Ref="C39"  Part="1" 
AR Path="/6FE901F74B37DA03" Ref="C39"  Part="1" 
AR Path="/402755814B37DA03" Ref="C39"  Part="1" 
AR Path="/3FEFFFFF4B37DA03" Ref="C39"  Part="1" 
AR Path="/4030AAC04B37DA03" Ref="C39"  Part="1" 
AR Path="/FFFFFFF04B37DA03" Ref="C39"  Part="1" 
AR Path="/5AD7153D4B37DA03" Ref="C39"  Part="1" 
AR Path="/A84B37DA03" Ref="C39"  Part="1" 
AR Path="/14B37DA03" Ref="C39"  Part="1" 
AR Path="/6FF0DD404B37DA03" Ref="C39"  Part="1" 
AR Path="/23D9304B37DA03" Ref="C39"  Part="1" 
AR Path="/23D8D44B37DA03" Ref="C39"  Part="1" 
AR Path="/4031DDF34B37DA03" Ref="C39"  Part="1" 
AR Path="/3FE88B434B37DA03" Ref="C39"  Part="1" 
AR Path="/4032778D4B37DA03" Ref="C39"  Part="1" 
AR Path="/403091264B37DA03" Ref="C39"  Part="1" 
AR Path="/403051264B37DA03" Ref="C39"  Part="1" 
AR Path="/4032F78D4B37DA03" Ref="C39"  Part="1" 
AR Path="/403251264B37DA03" Ref="C39"  Part="1" 
AR Path="/4032AAC04B37DA03" Ref="C39"  Part="1" 
AR Path="/4030D1264B37DA03" Ref="C39"  Part="1" 
AR Path="/4031778D4B37DA03" Ref="C39"  Part="1" 
AR Path="/3FEA24DD4B37DA03" Ref="C39"  Part="1" 
AR Path="/2600004B37DA03" Ref="C39"  Part="1" 
AR Path="/6FE934E34B37DA03" Ref="C39"  Part="1" 
AR Path="/773F65F14B37DA03" Ref="C39"  Part="1" 
AR Path="/773F8EB44B37DA03" Ref="C39"  Part="1" 
AR Path="/23C9F04B37DA03" Ref="C39"  Part="1" 
AR Path="/24B37DA03" Ref="C39"  Part="1" 
AR Path="/23BC884B37DA03" Ref="C39"  Part="1" 
AR Path="/DC0C124B37DA03" Ref="C39"  Part="1" 
AR Path="/23C34C4B37DA03" Ref="C39"  Part="1" 
AR Path="/23CBC44B37DA03" Ref="C39"  Part="1" 
AR Path="/23C6504B37DA03" Ref="C39"  Part="1" 
AR Path="/39803EA4B37DA03" Ref="C39"  Part="1" 
AR Path="/6FE934344B37DA03" Ref="C39"  Part="1" 
AR Path="/7E0073254B37DA03" Ref="C39"  Part="1" 
AR Path="/73254B37DA03" Ref="C39"  Part="1" 
AR Path="/8CEE4B37DA03" Ref="C39"  Part="1" 
AR Path="/23D9004B37DA03" Ref="C39"  Part="1" 
AR Path="/91964B37DA03" Ref="C39"  Part="1" 
AR Path="/77F16B254B37DA03" Ref="C39"  Part="1" 
AR Path="/23D83C4B37DA03" Ref="C39"  Part="1" 
AR Path="/47907FA4B37DA03" Ref="C39"  Part="1" 
AR Path="/8D384B37DA03" Ref="C39"  Part="1" 
AR Path="/D058E04B37DA03" Ref="C39"  Part="1" 
AR Path="/3C64B37DA03" Ref="C39"  Part="1" 
AR Path="/2F4A4B37DA03" Ref="C39"  Part="1" 
AR Path="/D1C3384B37DA03" Ref="C39"  Part="1" 
F 0 "C39" H 37450 21750 50  0000 L CNN
F 1 "0.1 uF" H 37450 21550 50  0000 L CNN
F 2 "C2" H 37400 21650 60  0001 C CNN
F 3 "" H 37400 21650 60  0001 C CNN
	1    37400 21650
	1    0    0    -1  
$EndComp
$Comp
L C C40
U 1 1 4B37DA01
P 37850 21650
AR Path="/FFFFFFFF4B37DA01" Ref="C40"  Part="1" 
AR Path="/4B37DA01" Ref="C40"  Part="1" 
AR Path="/94B37DA01" Ref="C40"  Part="1" 
AR Path="/A4B37DA01" Ref="C40"  Part="1" 
AR Path="/6FE901F74B37DA01" Ref="C40"  Part="1" 
AR Path="/402755814B37DA01" Ref="C40"  Part="1" 
AR Path="/3FEFFFFF4B37DA01" Ref="C40"  Part="1" 
AR Path="/4030AAC04B37DA01" Ref="C40"  Part="1" 
AR Path="/FFFFFFF04B37DA01" Ref="C40"  Part="1" 
AR Path="/5AD7153D4B37DA01" Ref="C40"  Part="1" 
AR Path="/A84B37DA01" Ref="C40"  Part="1" 
AR Path="/14B37DA01" Ref="C40"  Part="1" 
AR Path="/6FF0DD404B37DA01" Ref="C40"  Part="1" 
AR Path="/23D9304B37DA01" Ref="C40"  Part="1" 
AR Path="/23D8D44B37DA01" Ref="C40"  Part="1" 
AR Path="/4031DDF34B37DA01" Ref="C40"  Part="1" 
AR Path="/3FE88B434B37DA01" Ref="C40"  Part="1" 
AR Path="/4032778D4B37DA01" Ref="C40"  Part="1" 
AR Path="/403091264B37DA01" Ref="C40"  Part="1" 
AR Path="/403051264B37DA01" Ref="C40"  Part="1" 
AR Path="/4032F78D4B37DA01" Ref="C40"  Part="1" 
AR Path="/403251264B37DA01" Ref="C40"  Part="1" 
AR Path="/4032AAC04B37DA01" Ref="C40"  Part="1" 
AR Path="/4030D1264B37DA01" Ref="C40"  Part="1" 
AR Path="/4031778D4B37DA01" Ref="C40"  Part="1" 
AR Path="/3FEA24DD4B37DA01" Ref="C40"  Part="1" 
AR Path="/2600004B37DA01" Ref="C40"  Part="1" 
AR Path="/6FE934E34B37DA01" Ref="C40"  Part="1" 
AR Path="/773F65F14B37DA01" Ref="C40"  Part="1" 
AR Path="/773F8EB44B37DA01" Ref="C40"  Part="1" 
AR Path="/23C9F04B37DA01" Ref="C40"  Part="1" 
AR Path="/24B37DA01" Ref="C40"  Part="1" 
AR Path="/23BC884B37DA01" Ref="C40"  Part="1" 
AR Path="/DC0C124B37DA01" Ref="C40"  Part="1" 
AR Path="/23C34C4B37DA01" Ref="C40"  Part="1" 
AR Path="/23CBC44B37DA01" Ref="C40"  Part="1" 
AR Path="/23C6504B37DA01" Ref="C40"  Part="1" 
AR Path="/39803EA4B37DA01" Ref="C40"  Part="1" 
AR Path="/6FE934344B37DA01" Ref="C40"  Part="1" 
AR Path="/7E0073254B37DA01" Ref="C40"  Part="1" 
AR Path="/73254B37DA01" Ref="C40"  Part="1" 
AR Path="/8CEE4B37DA01" Ref="C40"  Part="1" 
AR Path="/23D9004B37DA01" Ref="C40"  Part="1" 
AR Path="/91964B37DA01" Ref="C40"  Part="1" 
AR Path="/77F16B254B37DA01" Ref="C40"  Part="1" 
AR Path="/23D83C4B37DA01" Ref="C40"  Part="1" 
AR Path="/47907FA4B37DA01" Ref="C40"  Part="1" 
AR Path="/8D384B37DA01" Ref="C40"  Part="1" 
AR Path="/D058E04B37DA01" Ref="C40"  Part="1" 
AR Path="/3C64B37DA01" Ref="C40"  Part="1" 
AR Path="/2F4A4B37DA01" Ref="C40"  Part="1" 
AR Path="/D1C3384B37DA01" Ref="C40"  Part="1" 
F 0 "C40" H 37900 21750 50  0000 L CNN
F 1 "0.1 uF" H 37900 21550 50  0000 L CNN
F 2 "C2" H 37850 21650 60  0001 C CNN
F 3 "" H 37850 21650 60  0001 C CNN
	1    37850 21650
	1    0    0    -1  
$EndComp
$Comp
L C C25
U 1 1 4B366286
P 39650 21050
AR Path="/4B366286" Ref="C25"  Part="1" 
AR Path="/94B366286" Ref="C25"  Part="1" 
AR Path="/5AD7153D4B366286" Ref="C25"  Part="1" 
AR Path="/DCBAABCD4B366286" Ref="C25"  Part="1" 
AR Path="/23D9304B366286" Ref="C25"  Part="1" 
AR Path="/6FE901F74B366286" Ref="C25"  Part="1" 
AR Path="/3FE224DD4B366286" Ref="C25"  Part="1" 
AR Path="/3FEFFFFF4B366286" Ref="C25"  Part="1" 
AR Path="/23D8D44B366286" Ref="C25"  Part="1" 
AR Path="/14B366286" Ref="C25"  Part="1" 
AR Path="/6FF0DD404B366286" Ref="C25"  Part="1" 
AR Path="/FFFFFFF04B366286" Ref="C25"  Part="1" 
AR Path="/402955814B366286" Ref="C25"  Part="1" 
AR Path="/A84B366286" Ref="C25"  Part="1" 
AR Path="/A4B366286" Ref="C25"  Part="1" 
AR Path="/402C08B44B366286" Ref="C25"  Part="1" 
AR Path="/23D2034B366286" Ref="C25"  Part="1" 
AR Path="/4E4B366286" Ref="C25"  Part="1" 
AR Path="/402BEF1A4B366286" Ref="C25"  Part="1" 
AR Path="/54B366286" Ref="C25"  Part="1" 
AR Path="/40293BE74B366286" Ref="C25"  Part="1" 
AR Path="/402C55814B366286" Ref="C25"  Part="1" 
AR Path="/40273BE74B366286" Ref="C25"  Part="1" 
AR Path="/2600004B366286" Ref="C25"  Part="1" 
AR Path="/3D8EA0004B366286" Ref="C25"  Part="1" 
AR Path="/402908B44B366286" Ref="C25"  Part="1" 
AR Path="/3D6CC0004B366286" Ref="C25"  Part="1" 
AR Path="/3D5A40004B366286" Ref="C25"  Part="1" 
AR Path="/402755814B366286" Ref="C25"  Part="1" 
AR Path="/4030AAC04B366286" Ref="C25"  Part="1" 
AR Path="/4031DDF34B366286" Ref="C25"  Part="1" 
AR Path="/3FE88B434B366286" Ref="C25"  Part="1" 
AR Path="/4032778D4B366286" Ref="C25"  Part="1" 
AR Path="/403091264B366286" Ref="C25"  Part="1" 
AR Path="/403051264B366286" Ref="C25"  Part="1" 
AR Path="/4032F78D4B366286" Ref="C25"  Part="1" 
AR Path="/403251264B366286" Ref="C25"  Part="1" 
AR Path="/4032AAC04B366286" Ref="C25"  Part="1" 
AR Path="/4030D1264B366286" Ref="C25"  Part="1" 
AR Path="/4031778D4B366286" Ref="C25"  Part="1" 
AR Path="/3FEA24DD4B366286" Ref="C25"  Part="1" 
AR Path="/6FE934E34B366286" Ref="C25"  Part="1" 
AR Path="/773F65F14B366286" Ref="C25"  Part="1" 
AR Path="/773F8EB44B366286" Ref="C25"  Part="1" 
AR Path="/23C9F04B366286" Ref="C25"  Part="1" 
AR Path="/24B366286" Ref="C25"  Part="1" 
AR Path="/23BC884B366286" Ref="C25"  Part="1" 
AR Path="/DC0C124B366286" Ref="C25"  Part="1" 
AR Path="/23C34C4B366286" Ref="C25"  Part="1" 
AR Path="/23CBC44B366286" Ref="C25"  Part="1" 
AR Path="/23C6504B366286" Ref="C25"  Part="1" 
AR Path="/39803EA4B366286" Ref="C25"  Part="1" 
AR Path="/FFFFFFFF4B366286" Ref="C25"  Part="1" 
AR Path="/6FE934344B366286" Ref="C25"  Part="1" 
AR Path="/7E0073254B366286" Ref="C25"  Part="1" 
AR Path="/73254B366286" Ref="C25"  Part="1" 
AR Path="/8CEE4B366286" Ref="C25"  Part="1" 
AR Path="/23D9004B366286" Ref="C25"  Part="1" 
AR Path="/91964B366286" Ref="C25"  Part="1" 
AR Path="/77F16B254B366286" Ref="C25"  Part="1" 
AR Path="/23D83C4B366286" Ref="C25"  Part="1" 
AR Path="/47907FA4B366286" Ref="C25"  Part="1" 
AR Path="/8D384B366286" Ref="C25"  Part="1" 
AR Path="/D058E04B366286" Ref="C25"  Part="1" 
AR Path="/3C64B366286" Ref="C25"  Part="1" 
AR Path="/2F4A4B366286" Ref="C25"  Part="1" 
AR Path="/D1C3384B366286" Ref="C25"  Part="1" 
F 0 "C25" H 39700 21150 50  0000 L CNN
F 1 "0.1 uF" H 39700 20950 50  0000 L CNN
F 2 "C2" H 39650 21050 60  0001 C CNN
F 3 "" H 39650 21050 60  0001 C CNN
	1    39650 21050
	1    0    0    -1  
$EndComp
$Comp
L C C22
U 1 1 4B366285
P 38750 21050
AR Path="/4B366285" Ref="C22"  Part="1" 
AR Path="/94B366285" Ref="C22"  Part="1" 
AR Path="/5AD7153D4B366285" Ref="C22"  Part="1" 
AR Path="/23D9304B366285" Ref="C22"  Part="1" 
AR Path="/6FE901F74B366285" Ref="C22"  Part="1" 
AR Path="/3FE224DD4B366285" Ref="C22"  Part="1" 
AR Path="/3FEFFFFF4B366285" Ref="C22"  Part="1" 
AR Path="/23D8D44B366285" Ref="C22"  Part="1" 
AR Path="/14B366285" Ref="C22"  Part="1" 
AR Path="/6FF0DD404B366285" Ref="C22"  Part="1" 
AR Path="/FFFFFFF04B366285" Ref="C22"  Part="1" 
AR Path="/402955814B366285" Ref="C22"  Part="1" 
AR Path="/DCBAABCD4B366285" Ref="C22"  Part="1" 
AR Path="/A84B366285" Ref="C22"  Part="1" 
AR Path="/A4B366285" Ref="C22"  Part="1" 
AR Path="/402C08B44B366285" Ref="C22"  Part="1" 
AR Path="/23D2034B366285" Ref="C22"  Part="1" 
AR Path="/4E4B366285" Ref="C22"  Part="1" 
AR Path="/402BEF1A4B366285" Ref="C22"  Part="1" 
AR Path="/54B366285" Ref="C22"  Part="1" 
AR Path="/40293BE74B366285" Ref="C22"  Part="1" 
AR Path="/402C55814B366285" Ref="C22"  Part="1" 
AR Path="/40273BE74B366285" Ref="C22"  Part="1" 
AR Path="/2600004B366285" Ref="C22"  Part="1" 
AR Path="/3D8EA0004B366285" Ref="C22"  Part="1" 
AR Path="/402908B44B366285" Ref="C22"  Part="1" 
AR Path="/3D6CC0004B366285" Ref="C22"  Part="1" 
AR Path="/3D5A40004B366285" Ref="C22"  Part="1" 
AR Path="/402755814B366285" Ref="C22"  Part="1" 
AR Path="/4030AAC04B366285" Ref="C22"  Part="1" 
AR Path="/4031DDF34B366285" Ref="C22"  Part="1" 
AR Path="/3FE88B434B366285" Ref="C22"  Part="1" 
AR Path="/4032778D4B366285" Ref="C22"  Part="1" 
AR Path="/403091264B366285" Ref="C22"  Part="1" 
AR Path="/403051264B366285" Ref="C22"  Part="1" 
AR Path="/4032F78D4B366285" Ref="C22"  Part="1" 
AR Path="/403251264B366285" Ref="C22"  Part="1" 
AR Path="/4032AAC04B366285" Ref="C22"  Part="1" 
AR Path="/4030D1264B366285" Ref="C22"  Part="1" 
AR Path="/4031778D4B366285" Ref="C22"  Part="1" 
AR Path="/3FEA24DD4B366285" Ref="C22"  Part="1" 
AR Path="/6FE934E34B366285" Ref="C22"  Part="1" 
AR Path="/773F65F14B366285" Ref="C22"  Part="1" 
AR Path="/773F8EB44B366285" Ref="C22"  Part="1" 
AR Path="/23C9F04B366285" Ref="C22"  Part="1" 
AR Path="/24B366285" Ref="C22"  Part="1" 
AR Path="/23BC884B366285" Ref="C22"  Part="1" 
AR Path="/DC0C124B366285" Ref="C22"  Part="1" 
AR Path="/23C34C4B366285" Ref="C22"  Part="1" 
AR Path="/23CBC44B366285" Ref="C22"  Part="1" 
AR Path="/23C6504B366285" Ref="C22"  Part="1" 
AR Path="/39803EA4B366285" Ref="C22"  Part="1" 
AR Path="/FFFFFFFF4B366285" Ref="C22"  Part="1" 
AR Path="/6FE934344B366285" Ref="C22"  Part="1" 
AR Path="/7E0073254B366285" Ref="C22"  Part="1" 
AR Path="/73254B366285" Ref="C22"  Part="1" 
AR Path="/8CEE4B366285" Ref="C22"  Part="1" 
AR Path="/23D9004B366285" Ref="C22"  Part="1" 
AR Path="/91964B366285" Ref="C22"  Part="1" 
AR Path="/77F16B254B366285" Ref="C22"  Part="1" 
AR Path="/23D83C4B366285" Ref="C22"  Part="1" 
AR Path="/47907FA4B366285" Ref="C22"  Part="1" 
AR Path="/8D384B366285" Ref="C22"  Part="1" 
AR Path="/D058E04B366285" Ref="C22"  Part="1" 
AR Path="/3C64B366285" Ref="C22"  Part="1" 
AR Path="/2F4A4B366285" Ref="C22"  Part="1" 
AR Path="/D1C3384B366285" Ref="C22"  Part="1" 
F 0 "C22" H 38800 21150 50  0000 L CNN
F 1 "0.1 uF" H 38800 20950 50  0000 L CNN
F 2 "C2" H 38750 21050 60  0001 C CNN
F 3 "" H 38750 21050 60  0001 C CNN
	1    38750 21050
	1    0    0    -1  
$EndComp
$Comp
L C C23
U 1 1 4B366284
P 39200 21050
AR Path="/4B366284" Ref="C23"  Part="1" 
AR Path="/94B366284" Ref="C23"  Part="1" 
AR Path="/5AD7153D4B366284" Ref="C23"  Part="1" 
AR Path="/23D9304B366284" Ref="C23"  Part="1" 
AR Path="/6FE901F74B366284" Ref="C23"  Part="1" 
AR Path="/3FE224DD4B366284" Ref="C23"  Part="1" 
AR Path="/3FEFFFFF4B366284" Ref="C23"  Part="1" 
AR Path="/23D8D44B366284" Ref="C23"  Part="1" 
AR Path="/14B366284" Ref="C23"  Part="1" 
AR Path="/6FF0DD404B366284" Ref="C23"  Part="1" 
AR Path="/FFFFFFF04B366284" Ref="C23"  Part="1" 
AR Path="/402955814B366284" Ref="C23"  Part="1" 
AR Path="/DCBAABCD4B366284" Ref="C23"  Part="1" 
AR Path="/A84B366284" Ref="C23"  Part="1" 
AR Path="/A4B366284" Ref="C23"  Part="1" 
AR Path="/402C08B44B366284" Ref="C23"  Part="1" 
AR Path="/23D2034B366284" Ref="C23"  Part="1" 
AR Path="/4E4B366284" Ref="C23"  Part="1" 
AR Path="/402BEF1A4B366284" Ref="C23"  Part="1" 
AR Path="/54B366284" Ref="C23"  Part="1" 
AR Path="/40293BE74B366284" Ref="C23"  Part="1" 
AR Path="/402C55814B366284" Ref="C23"  Part="1" 
AR Path="/40273BE74B366284" Ref="C23"  Part="1" 
AR Path="/2600004B366284" Ref="C23"  Part="1" 
AR Path="/3D8EA0004B366284" Ref="C23"  Part="1" 
AR Path="/402908B44B366284" Ref="C23"  Part="1" 
AR Path="/3D6CC0004B366284" Ref="C23"  Part="1" 
AR Path="/3D5A40004B366284" Ref="C23"  Part="1" 
AR Path="/402755814B366284" Ref="C23"  Part="1" 
AR Path="/4030AAC04B366284" Ref="C23"  Part="1" 
AR Path="/4031DDF34B366284" Ref="C23"  Part="1" 
AR Path="/3FE88B434B366284" Ref="C23"  Part="1" 
AR Path="/4032778D4B366284" Ref="C23"  Part="1" 
AR Path="/403091264B366284" Ref="C23"  Part="1" 
AR Path="/403051264B366284" Ref="C23"  Part="1" 
AR Path="/4032F78D4B366284" Ref="C23"  Part="1" 
AR Path="/403251264B366284" Ref="C23"  Part="1" 
AR Path="/4032AAC04B366284" Ref="C23"  Part="1" 
AR Path="/4030D1264B366284" Ref="C23"  Part="1" 
AR Path="/4031778D4B366284" Ref="C23"  Part="1" 
AR Path="/3FEA24DD4B366284" Ref="C23"  Part="1" 
AR Path="/6FE934E34B366284" Ref="C23"  Part="1" 
AR Path="/773F65F14B366284" Ref="C23"  Part="1" 
AR Path="/773F8EB44B366284" Ref="C23"  Part="1" 
AR Path="/23C9F04B366284" Ref="C23"  Part="1" 
AR Path="/24B366284" Ref="C23"  Part="1" 
AR Path="/23BC884B366284" Ref="C23"  Part="1" 
AR Path="/DC0C124B366284" Ref="C23"  Part="1" 
AR Path="/23C34C4B366284" Ref="C23"  Part="1" 
AR Path="/23CBC44B366284" Ref="C23"  Part="1" 
AR Path="/23C6504B366284" Ref="C23"  Part="1" 
AR Path="/39803EA4B366284" Ref="C23"  Part="1" 
AR Path="/FFFFFFFF4B366284" Ref="C23"  Part="1" 
AR Path="/6FE934344B366284" Ref="C23"  Part="1" 
AR Path="/7E0073254B366284" Ref="C23"  Part="1" 
AR Path="/73254B366284" Ref="C23"  Part="1" 
AR Path="/8CEE4B366284" Ref="C23"  Part="1" 
AR Path="/23D9004B366284" Ref="C23"  Part="1" 
AR Path="/91964B366284" Ref="C23"  Part="1" 
AR Path="/77F16B254B366284" Ref="C23"  Part="1" 
AR Path="/23D83C4B366284" Ref="C23"  Part="1" 
AR Path="/47907FA4B366284" Ref="C23"  Part="1" 
AR Path="/8D384B366284" Ref="C23"  Part="1" 
AR Path="/D058E04B366284" Ref="C23"  Part="1" 
AR Path="/3C64B366284" Ref="C23"  Part="1" 
AR Path="/2F4A4B366284" Ref="C23"  Part="1" 
AR Path="/D1C3384B366284" Ref="C23"  Part="1" 
F 0 "C23" H 39250 21150 50  0000 L CNN
F 1 "0.1 uF" H 39250 20950 50  0000 L CNN
F 2 "C2" H 39200 21050 60  0001 C CNN
F 3 "" H 39200 21050 60  0001 C CNN
	1    39200 21050
	1    0    0    -1  
$EndComp
$Comp
L C C29
U 1 1 4B366280
P 40550 21050
AR Path="/4B366280" Ref="C29"  Part="1" 
AR Path="/94B366280" Ref="C29"  Part="1" 
AR Path="/5AD7153D4B366280" Ref="C29"  Part="1" 
AR Path="/23D9304B366280" Ref="C29"  Part="1" 
AR Path="/6FE901F74B366280" Ref="C29"  Part="1" 
AR Path="/3FE224DD4B366280" Ref="C29"  Part="1" 
AR Path="/3FEFFFFF4B366280" Ref="C29"  Part="1" 
AR Path="/23D8D44B366280" Ref="C29"  Part="1" 
AR Path="/14B366280" Ref="C29"  Part="1" 
AR Path="/6FF0DD404B366280" Ref="C29"  Part="1" 
AR Path="/FFFFFFF04B366280" Ref="C29"  Part="1" 
AR Path="/402955814B366280" Ref="C29"  Part="1" 
AR Path="/DCBAABCD4B366280" Ref="C29"  Part="1" 
AR Path="/A84B366280" Ref="C29"  Part="1" 
AR Path="/A4B366280" Ref="C29"  Part="1" 
AR Path="/402C08B44B366280" Ref="C29"  Part="1" 
AR Path="/23D2034B366280" Ref="C29"  Part="1" 
AR Path="/4E4B366280" Ref="C29"  Part="1" 
AR Path="/402BEF1A4B366280" Ref="C29"  Part="1" 
AR Path="/54B366280" Ref="C29"  Part="1" 
AR Path="/40293BE74B366280" Ref="C29"  Part="1" 
AR Path="/402C55814B366280" Ref="C29"  Part="1" 
AR Path="/40273BE74B366280" Ref="C29"  Part="1" 
AR Path="/2600004B366280" Ref="C29"  Part="1" 
AR Path="/3D8EA0004B366280" Ref="C29"  Part="1" 
AR Path="/402908B44B366280" Ref="C29"  Part="1" 
AR Path="/3D6CC0004B366280" Ref="C29"  Part="1" 
AR Path="/3D5A40004B366280" Ref="C29"  Part="1" 
AR Path="/402755814B366280" Ref="C29"  Part="1" 
AR Path="/4030AAC04B366280" Ref="C29"  Part="1" 
AR Path="/4031DDF34B366280" Ref="C29"  Part="1" 
AR Path="/3FE88B434B366280" Ref="C29"  Part="1" 
AR Path="/4032778D4B366280" Ref="C29"  Part="1" 
AR Path="/403091264B366280" Ref="C29"  Part="1" 
AR Path="/403051264B366280" Ref="C29"  Part="1" 
AR Path="/4032F78D4B366280" Ref="C29"  Part="1" 
AR Path="/403251264B366280" Ref="C29"  Part="1" 
AR Path="/4032AAC04B366280" Ref="C29"  Part="1" 
AR Path="/4030D1264B366280" Ref="C29"  Part="1" 
AR Path="/4031778D4B366280" Ref="C29"  Part="1" 
AR Path="/3FEA24DD4B366280" Ref="C29"  Part="1" 
AR Path="/6FE934E34B366280" Ref="C29"  Part="1" 
AR Path="/773F65F14B366280" Ref="C29"  Part="1" 
AR Path="/773F8EB44B366280" Ref="C29"  Part="1" 
AR Path="/23C9F04B366280" Ref="C29"  Part="1" 
AR Path="/24B366280" Ref="C29"  Part="1" 
AR Path="/23BC884B366280" Ref="C29"  Part="1" 
AR Path="/DC0C124B366280" Ref="C29"  Part="1" 
AR Path="/23C34C4B366280" Ref="C29"  Part="1" 
AR Path="/23CBC44B366280" Ref="C29"  Part="1" 
AR Path="/23C6504B366280" Ref="C29"  Part="1" 
AR Path="/39803EA4B366280" Ref="C29"  Part="1" 
AR Path="/FFFFFFFF4B366280" Ref="C29"  Part="1" 
AR Path="/6FE934344B366280" Ref="C29"  Part="1" 
AR Path="/7E0073254B366280" Ref="C29"  Part="1" 
AR Path="/73254B366280" Ref="C29"  Part="1" 
AR Path="/8CEE4B366280" Ref="C29"  Part="1" 
AR Path="/23D9004B366280" Ref="C29"  Part="1" 
AR Path="/91964B366280" Ref="C29"  Part="1" 
AR Path="/77F16B254B366280" Ref="C29"  Part="1" 
AR Path="/23D83C4B366280" Ref="C29"  Part="1" 
AR Path="/47907FA4B366280" Ref="C29"  Part="1" 
AR Path="/8D384B366280" Ref="C29"  Part="1" 
AR Path="/D058E04B366280" Ref="C29"  Part="1" 
AR Path="/3C64B366280" Ref="C29"  Part="1" 
AR Path="/2F4A4B366280" Ref="C29"  Part="1" 
AR Path="/D1C3384B366280" Ref="C29"  Part="1" 
F 0 "C29" H 40600 21150 50  0000 L CNN
F 1 "0.1 uF" H 40600 20950 50  0000 L CNN
F 2 "C2" H 40550 21050 60  0001 C CNN
F 3 "" H 40550 21050 60  0001 C CNN
	1    40550 21050
	1    0    0    -1  
$EndComp
$Comp
L C C27
U 1 1 4B36627F
P 40100 21050
AR Path="/4B36627F" Ref="C27"  Part="1" 
AR Path="/94B36627F" Ref="C27"  Part="1" 
AR Path="/5AD7153D4B36627F" Ref="C27"  Part="1" 
AR Path="/23D9304B36627F" Ref="C27"  Part="1" 
AR Path="/6FE901F74B36627F" Ref="C27"  Part="1" 
AR Path="/3FE224DD4B36627F" Ref="C27"  Part="1" 
AR Path="/3FEFFFFF4B36627F" Ref="C27"  Part="1" 
AR Path="/23D8D44B36627F" Ref="C27"  Part="1" 
AR Path="/14B36627F" Ref="C27"  Part="1" 
AR Path="/6FF0DD404B36627F" Ref="C27"  Part="1" 
AR Path="/FFFFFFF04B36627F" Ref="C27"  Part="1" 
AR Path="/402955814B36627F" Ref="C27"  Part="1" 
AR Path="/DCBAABCD4B36627F" Ref="C27"  Part="1" 
AR Path="/A84B36627F" Ref="C27"  Part="1" 
AR Path="/A4B36627F" Ref="C27"  Part="1" 
AR Path="/402C08B44B36627F" Ref="C27"  Part="1" 
AR Path="/23D2034B36627F" Ref="C27"  Part="1" 
AR Path="/4E4B36627F" Ref="C27"  Part="1" 
AR Path="/402BEF1A4B36627F" Ref="C27"  Part="1" 
AR Path="/54B36627F" Ref="C27"  Part="1" 
AR Path="/40293BE74B36627F" Ref="C27"  Part="1" 
AR Path="/402C55814B36627F" Ref="C27"  Part="1" 
AR Path="/40273BE74B36627F" Ref="C27"  Part="1" 
AR Path="/2600004B36627F" Ref="C27"  Part="1" 
AR Path="/3D8EA0004B36627F" Ref="C27"  Part="1" 
AR Path="/402908B44B36627F" Ref="C27"  Part="1" 
AR Path="/3D6CC0004B36627F" Ref="C27"  Part="1" 
AR Path="/3D5A40004B36627F" Ref="C27"  Part="1" 
AR Path="/402755814B36627F" Ref="C27"  Part="1" 
AR Path="/4030AAC04B36627F" Ref="C27"  Part="1" 
AR Path="/4031DDF34B36627F" Ref="C27"  Part="1" 
AR Path="/3FE88B434B36627F" Ref="C27"  Part="1" 
AR Path="/4032778D4B36627F" Ref="C27"  Part="1" 
AR Path="/403091264B36627F" Ref="C27"  Part="1" 
AR Path="/403051264B36627F" Ref="C27"  Part="1" 
AR Path="/4032F78D4B36627F" Ref="C27"  Part="1" 
AR Path="/403251264B36627F" Ref="C27"  Part="1" 
AR Path="/4032AAC04B36627F" Ref="C27"  Part="1" 
AR Path="/4030D1264B36627F" Ref="C27"  Part="1" 
AR Path="/4031778D4B36627F" Ref="C27"  Part="1" 
AR Path="/3FEA24DD4B36627F" Ref="C27"  Part="1" 
AR Path="/6FE934E34B36627F" Ref="C27"  Part="1" 
AR Path="/773F65F14B36627F" Ref="C27"  Part="1" 
AR Path="/773F8EB44B36627F" Ref="C27"  Part="1" 
AR Path="/23C9F04B36627F" Ref="C27"  Part="1" 
AR Path="/24B36627F" Ref="C27"  Part="1" 
AR Path="/23BC884B36627F" Ref="C27"  Part="1" 
AR Path="/DC0C124B36627F" Ref="C27"  Part="1" 
AR Path="/23C34C4B36627F" Ref="C27"  Part="1" 
AR Path="/23CBC44B36627F" Ref="C27"  Part="1" 
AR Path="/23C6504B36627F" Ref="C27"  Part="1" 
AR Path="/39803EA4B36627F" Ref="C27"  Part="1" 
AR Path="/FFFFFFFF4B36627F" Ref="C27"  Part="1" 
AR Path="/6FE934344B36627F" Ref="C27"  Part="1" 
AR Path="/7E0073254B36627F" Ref="C27"  Part="1" 
AR Path="/73254B36627F" Ref="C27"  Part="1" 
AR Path="/8CEE4B36627F" Ref="C27"  Part="1" 
AR Path="/23D9004B36627F" Ref="C27"  Part="1" 
AR Path="/91964B36627F" Ref="C27"  Part="1" 
AR Path="/77F16B254B36627F" Ref="C27"  Part="1" 
AR Path="/23D83C4B36627F" Ref="C27"  Part="1" 
AR Path="/47907FA4B36627F" Ref="C27"  Part="1" 
AR Path="/8D384B36627F" Ref="C27"  Part="1" 
AR Path="/D058E04B36627F" Ref="C27"  Part="1" 
AR Path="/3C64B36627F" Ref="C27"  Part="1" 
AR Path="/2F4A4B36627F" Ref="C27"  Part="1" 
AR Path="/D1C3384B36627F" Ref="C27"  Part="1" 
F 0 "C27" H 40150 21150 50  0000 L CNN
F 1 "0.1 uF" H 40150 20950 50  0000 L CNN
F 2 "C2" H 40100 21050 60  0001 C CNN
F 3 "" H 40100 21050 60  0001 C CNN
	1    40100 21050
	1    0    0    -1  
$EndComp
$Comp
L C C28
U 1 1 4B36626C
P 40100 21650
AR Path="/4B36626C" Ref="C28"  Part="1" 
AR Path="/94B36626C" Ref="C28"  Part="1" 
AR Path="/5AD7153D4B36626C" Ref="C28"  Part="1" 
AR Path="/23D9304B36626C" Ref="C28"  Part="1" 
AR Path="/6FE901F74B36626C" Ref="C28"  Part="1" 
AR Path="/3FE224DD4B36626C" Ref="C28"  Part="1" 
AR Path="/3FEFFFFF4B36626C" Ref="C28"  Part="1" 
AR Path="/23D8D44B36626C" Ref="C28"  Part="1" 
AR Path="/14B36626C" Ref="C28"  Part="1" 
AR Path="/6FF0DD404B36626C" Ref="C28"  Part="1" 
AR Path="/FFFFFFF04B36626C" Ref="C28"  Part="1" 
AR Path="/402955814B36626C" Ref="C28"  Part="1" 
AR Path="/DCBAABCD4B36626C" Ref="C28"  Part="1" 
AR Path="/A84B36626C" Ref="C28"  Part="1" 
AR Path="/A4B36626C" Ref="C28"  Part="1" 
AR Path="/402C08B44B36626C" Ref="C28"  Part="1" 
AR Path="/23D2034B36626C" Ref="C28"  Part="1" 
AR Path="/4E4B36626C" Ref="C28"  Part="1" 
AR Path="/402BEF1A4B36626C" Ref="C28"  Part="1" 
AR Path="/54B36626C" Ref="C28"  Part="1" 
AR Path="/40293BE74B36626C" Ref="C28"  Part="1" 
AR Path="/402C55814B36626C" Ref="C28"  Part="1" 
AR Path="/40273BE74B36626C" Ref="C28"  Part="1" 
AR Path="/2600004B36626C" Ref="C28"  Part="1" 
AR Path="/3D8EA0004B36626C" Ref="C28"  Part="1" 
AR Path="/402908B44B36626C" Ref="C28"  Part="1" 
AR Path="/3D6CC0004B36626C" Ref="C28"  Part="1" 
AR Path="/3D5A40004B36626C" Ref="C28"  Part="1" 
AR Path="/402755814B36626C" Ref="C28"  Part="1" 
AR Path="/4030AAC04B36626C" Ref="C28"  Part="1" 
AR Path="/4031DDF34B36626C" Ref="C28"  Part="1" 
AR Path="/3FE88B434B36626C" Ref="C28"  Part="1" 
AR Path="/4032778D4B36626C" Ref="C28"  Part="1" 
AR Path="/403091264B36626C" Ref="C28"  Part="1" 
AR Path="/403051264B36626C" Ref="C28"  Part="1" 
AR Path="/4032F78D4B36626C" Ref="C28"  Part="1" 
AR Path="/403251264B36626C" Ref="C28"  Part="1" 
AR Path="/4032AAC04B36626C" Ref="C28"  Part="1" 
AR Path="/4030D1264B36626C" Ref="C28"  Part="1" 
AR Path="/4031778D4B36626C" Ref="C28"  Part="1" 
AR Path="/3FEA24DD4B36626C" Ref="C28"  Part="1" 
AR Path="/6FE934E34B36626C" Ref="C28"  Part="1" 
AR Path="/773F65F14B36626C" Ref="C28"  Part="1" 
AR Path="/773F8EB44B36626C" Ref="C28"  Part="1" 
AR Path="/23C9F04B36626C" Ref="C28"  Part="1" 
AR Path="/24B36626C" Ref="C28"  Part="1" 
AR Path="/23BC884B36626C" Ref="C28"  Part="1" 
AR Path="/DC0C124B36626C" Ref="C28"  Part="1" 
AR Path="/23C34C4B36626C" Ref="C28"  Part="1" 
AR Path="/23CBC44B36626C" Ref="C28"  Part="1" 
AR Path="/23C6504B36626C" Ref="C28"  Part="1" 
AR Path="/39803EA4B36626C" Ref="C28"  Part="1" 
AR Path="/FFFFFFFF4B36626C" Ref="C28"  Part="1" 
AR Path="/6FE934344B36626C" Ref="C28"  Part="1" 
AR Path="/7E0073254B36626C" Ref="C28"  Part="1" 
AR Path="/73254B36626C" Ref="C28"  Part="1" 
AR Path="/8CEE4B36626C" Ref="C28"  Part="1" 
AR Path="/23D9004B36626C" Ref="C28"  Part="1" 
AR Path="/91964B36626C" Ref="C28"  Part="1" 
AR Path="/77F16B254B36626C" Ref="C28"  Part="1" 
AR Path="/23D83C4B36626C" Ref="C28"  Part="1" 
AR Path="/47907FA4B36626C" Ref="C28"  Part="1" 
AR Path="/8D384B36626C" Ref="C28"  Part="1" 
AR Path="/D058E04B36626C" Ref="C28"  Part="1" 
AR Path="/3C64B36626C" Ref="C28"  Part="1" 
AR Path="/2F4A4B36626C" Ref="C28"  Part="1" 
AR Path="/D1C3384B36626C" Ref="C28"  Part="1" 
F 0 "C28" H 40150 21750 50  0000 L CNN
F 1 "0.1 uF" H 40150 21550 50  0000 L CNN
F 2 "C2" H 40100 21650 60  0001 C CNN
F 3 "" H 40100 21650 60  0001 C CNN
	1    40100 21650
	1    0    0    -1  
$EndComp
$Comp
L C C26
U 1 1 4B36626A
P 39650 21650
AR Path="/4B36626A" Ref="C26"  Part="1" 
AR Path="/94B36626A" Ref="C26"  Part="1" 
AR Path="/5AD7153D4B36626A" Ref="C26"  Part="1" 
AR Path="/23D9304B36626A" Ref="C26"  Part="1" 
AR Path="/6FE901F74B36626A" Ref="C26"  Part="1" 
AR Path="/3FE224DD4B36626A" Ref="C26"  Part="1" 
AR Path="/3FEFFFFF4B36626A" Ref="C26"  Part="1" 
AR Path="/23D8D44B36626A" Ref="C26"  Part="1" 
AR Path="/14B36626A" Ref="C26"  Part="1" 
AR Path="/6FF0DD404B36626A" Ref="C26"  Part="1" 
AR Path="/FFFFFFF04B36626A" Ref="C26"  Part="1" 
AR Path="/402955814B36626A" Ref="C26"  Part="1" 
AR Path="/DCBAABCD4B36626A" Ref="C26"  Part="1" 
AR Path="/A84B36626A" Ref="C26"  Part="1" 
AR Path="/A4B36626A" Ref="C26"  Part="1" 
AR Path="/402C08B44B36626A" Ref="C26"  Part="1" 
AR Path="/23D2034B36626A" Ref="C26"  Part="1" 
AR Path="/4E4B36626A" Ref="C26"  Part="1" 
AR Path="/402BEF1A4B36626A" Ref="C26"  Part="1" 
AR Path="/54B36626A" Ref="C26"  Part="1" 
AR Path="/40293BE74B36626A" Ref="C26"  Part="1" 
AR Path="/402C55814B36626A" Ref="C26"  Part="1" 
AR Path="/40273BE74B36626A" Ref="C26"  Part="1" 
AR Path="/2600004B36626A" Ref="C26"  Part="1" 
AR Path="/3D8EA0004B36626A" Ref="C26"  Part="1" 
AR Path="/402908B44B36626A" Ref="C26"  Part="1" 
AR Path="/3D6CC0004B36626A" Ref="C26"  Part="1" 
AR Path="/3D5A40004B36626A" Ref="C26"  Part="1" 
AR Path="/402755814B36626A" Ref="C26"  Part="1" 
AR Path="/4030AAC04B36626A" Ref="C26"  Part="1" 
AR Path="/4031DDF34B36626A" Ref="C26"  Part="1" 
AR Path="/3FE88B434B36626A" Ref="C26"  Part="1" 
AR Path="/4032778D4B36626A" Ref="C26"  Part="1" 
AR Path="/403091264B36626A" Ref="C26"  Part="1" 
AR Path="/403051264B36626A" Ref="C26"  Part="1" 
AR Path="/4032F78D4B36626A" Ref="C26"  Part="1" 
AR Path="/403251264B36626A" Ref="C26"  Part="1" 
AR Path="/4032AAC04B36626A" Ref="C26"  Part="1" 
AR Path="/4030D1264B36626A" Ref="C26"  Part="1" 
AR Path="/4031778D4B36626A" Ref="C26"  Part="1" 
AR Path="/3FEA24DD4B36626A" Ref="C26"  Part="1" 
AR Path="/6FE934E34B36626A" Ref="C26"  Part="1" 
AR Path="/773F65F14B36626A" Ref="C26"  Part="1" 
AR Path="/773F8EB44B36626A" Ref="C26"  Part="1" 
AR Path="/23C9F04B36626A" Ref="C26"  Part="1" 
AR Path="/24B36626A" Ref="C26"  Part="1" 
AR Path="/23BC884B36626A" Ref="C26"  Part="1" 
AR Path="/DC0C124B36626A" Ref="C26"  Part="1" 
AR Path="/23C34C4B36626A" Ref="C26"  Part="1" 
AR Path="/23CBC44B36626A" Ref="C26"  Part="1" 
AR Path="/23C6504B36626A" Ref="C26"  Part="1" 
AR Path="/39803EA4B36626A" Ref="C26"  Part="1" 
AR Path="/FFFFFFFF4B36626A" Ref="C26"  Part="1" 
AR Path="/6FE934344B36626A" Ref="C26"  Part="1" 
AR Path="/7E0073254B36626A" Ref="C26"  Part="1" 
AR Path="/73254B36626A" Ref="C26"  Part="1" 
AR Path="/8CEE4B36626A" Ref="C26"  Part="1" 
AR Path="/23D9004B36626A" Ref="C26"  Part="1" 
AR Path="/91964B36626A" Ref="C26"  Part="1" 
AR Path="/77F16B254B36626A" Ref="C26"  Part="1" 
AR Path="/23D83C4B36626A" Ref="C26"  Part="1" 
AR Path="/47907FA4B36626A" Ref="C26"  Part="1" 
AR Path="/8D384B36626A" Ref="C26"  Part="1" 
AR Path="/D058E04B36626A" Ref="C26"  Part="1" 
AR Path="/3C64B36626A" Ref="C26"  Part="1" 
AR Path="/2F4A4B36626A" Ref="C26"  Part="1" 
AR Path="/D1C3384B36626A" Ref="C26"  Part="1" 
F 0 "C26" H 39700 21750 50  0000 L CNN
F 1 "0.1 uF" H 39700 21550 50  0000 L CNN
F 2 "C2" H 39650 21650 60  0001 C CNN
F 3 "" H 39650 21650 60  0001 C CNN
	1    39650 21650
	1    0    0    -1  
$EndComp
$Comp
L C C20
U 1 1 4B36625D
P 37850 21050
AR Path="/4B36625D" Ref="C20"  Part="1" 
AR Path="/94B36625D" Ref="C20"  Part="1" 
AR Path="/5AD7153D4B36625D" Ref="C20"  Part="1" 
AR Path="/23D9304B36625D" Ref="C20"  Part="1" 
AR Path="/6FE901F74B36625D" Ref="C20"  Part="1" 
AR Path="/3FE224DD4B36625D" Ref="C20"  Part="1" 
AR Path="/3FEFFFFF4B36625D" Ref="C20"  Part="1" 
AR Path="/23D8D44B36625D" Ref="C20"  Part="1" 
AR Path="/14B36625D" Ref="C20"  Part="1" 
AR Path="/6FF0DD404B36625D" Ref="C20"  Part="1" 
AR Path="/FFFFFFF04B36625D" Ref="C20"  Part="1" 
AR Path="/402955814B36625D" Ref="C20"  Part="1" 
AR Path="/DCBAABCD4B36625D" Ref="C20"  Part="1" 
AR Path="/A84B36625D" Ref="C20"  Part="1" 
AR Path="/A4B36625D" Ref="C20"  Part="1" 
AR Path="/402C08B44B36625D" Ref="C20"  Part="1" 
AR Path="/23D2034B36625D" Ref="C20"  Part="1" 
AR Path="/4E4B36625D" Ref="C20"  Part="1" 
AR Path="/402BEF1A4B36625D" Ref="C20"  Part="1" 
AR Path="/54B36625D" Ref="C20"  Part="1" 
AR Path="/40293BE74B36625D" Ref="C20"  Part="1" 
AR Path="/402C55814B36625D" Ref="C20"  Part="1" 
AR Path="/40273BE74B36625D" Ref="C20"  Part="1" 
AR Path="/2600004B36625D" Ref="C20"  Part="1" 
AR Path="/3D8EA0004B36625D" Ref="C20"  Part="1" 
AR Path="/402908B44B36625D" Ref="C20"  Part="1" 
AR Path="/3D6CC0004B36625D" Ref="C20"  Part="1" 
AR Path="/3D5A40004B36625D" Ref="C20"  Part="1" 
AR Path="/402755814B36625D" Ref="C20"  Part="1" 
AR Path="/4030AAC04B36625D" Ref="C20"  Part="1" 
AR Path="/4031DDF34B36625D" Ref="C20"  Part="1" 
AR Path="/3FE88B434B36625D" Ref="C20"  Part="1" 
AR Path="/4032778D4B36625D" Ref="C20"  Part="1" 
AR Path="/403091264B36625D" Ref="C20"  Part="1" 
AR Path="/403051264B36625D" Ref="C20"  Part="1" 
AR Path="/4032F78D4B36625D" Ref="C20"  Part="1" 
AR Path="/403251264B36625D" Ref="C20"  Part="1" 
AR Path="/4032AAC04B36625D" Ref="C20"  Part="1" 
AR Path="/4030D1264B36625D" Ref="C20"  Part="1" 
AR Path="/4031778D4B36625D" Ref="C20"  Part="1" 
AR Path="/3FEA24DD4B36625D" Ref="C20"  Part="1" 
AR Path="/6FE934E34B36625D" Ref="C20"  Part="1" 
AR Path="/773F65F14B36625D" Ref="C20"  Part="1" 
AR Path="/773F8EB44B36625D" Ref="C20"  Part="1" 
AR Path="/23C9F04B36625D" Ref="C20"  Part="1" 
AR Path="/24B36625D" Ref="C20"  Part="1" 
AR Path="/23BC884B36625D" Ref="C20"  Part="1" 
AR Path="/DC0C124B36625D" Ref="C20"  Part="1" 
AR Path="/23C34C4B36625D" Ref="C20"  Part="1" 
AR Path="/23CBC44B36625D" Ref="C20"  Part="1" 
AR Path="/23C6504B36625D" Ref="C20"  Part="1" 
AR Path="/39803EA4B36625D" Ref="C20"  Part="1" 
AR Path="/FFFFFFFF4B36625D" Ref="C20"  Part="1" 
AR Path="/6FE934344B36625D" Ref="C20"  Part="1" 
AR Path="/7E0073254B36625D" Ref="C20"  Part="1" 
AR Path="/73254B36625D" Ref="C20"  Part="1" 
AR Path="/8CEE4B36625D" Ref="C20"  Part="1" 
AR Path="/23D9004B36625D" Ref="C20"  Part="1" 
AR Path="/91964B36625D" Ref="C20"  Part="1" 
AR Path="/77F16B254B36625D" Ref="C20"  Part="1" 
AR Path="/23D83C4B36625D" Ref="C20"  Part="1" 
AR Path="/47907FA4B36625D" Ref="C20"  Part="1" 
AR Path="/8D384B36625D" Ref="C20"  Part="1" 
AR Path="/D058E04B36625D" Ref="C20"  Part="1" 
AR Path="/3C64B36625D" Ref="C20"  Part="1" 
AR Path="/2F4A4B36625D" Ref="C20"  Part="1" 
AR Path="/D1C3384B36625D" Ref="C20"  Part="1" 
F 0 "C20" H 37900 21150 50  0000 L CNN
F 1 "0.1 uF" H 37900 20950 50  0000 L CNN
F 2 "C2" H 37850 21050 60  0001 C CNN
F 3 "" H 37850 21050 60  0001 C CNN
	1    37850 21050
	1    0    0    -1  
$EndComp
$Comp
L C C19
U 1 1 4B36624E
P 37400 21050
AR Path="/4B36624E" Ref="C19"  Part="1" 
AR Path="/94B36624E" Ref="C19"  Part="1" 
AR Path="/5AD7153D4B36624E" Ref="C19"  Part="1" 
AR Path="/23D9304B36624E" Ref="C19"  Part="1" 
AR Path="/6FE901F74B36624E" Ref="C19"  Part="1" 
AR Path="/3FE224DD4B36624E" Ref="C19"  Part="1" 
AR Path="/3FEFFFFF4B36624E" Ref="C19"  Part="1" 
AR Path="/23D8D44B36624E" Ref="C19"  Part="1" 
AR Path="/14B36624E" Ref="C19"  Part="1" 
AR Path="/6FF0DD404B36624E" Ref="C19"  Part="1" 
AR Path="/FFFFFFF04B36624E" Ref="C19"  Part="1" 
AR Path="/402955814B36624E" Ref="C19"  Part="1" 
AR Path="/DCBAABCD4B36624E" Ref="C19"  Part="1" 
AR Path="/A84B36624E" Ref="C19"  Part="1" 
AR Path="/A4B36624E" Ref="C19"  Part="1" 
AR Path="/402C08B44B36624E" Ref="C19"  Part="1" 
AR Path="/23D2034B36624E" Ref="C19"  Part="1" 
AR Path="/4E4B36624E" Ref="C19"  Part="1" 
AR Path="/402BEF1A4B36624E" Ref="C19"  Part="1" 
AR Path="/54B36624E" Ref="C19"  Part="1" 
AR Path="/40293BE74B36624E" Ref="C19"  Part="1" 
AR Path="/402C55814B36624E" Ref="C19"  Part="1" 
AR Path="/40273BE74B36624E" Ref="C19"  Part="1" 
AR Path="/2600004B36624E" Ref="C19"  Part="1" 
AR Path="/3D8EA0004B36624E" Ref="C19"  Part="1" 
AR Path="/402908B44B36624E" Ref="C19"  Part="1" 
AR Path="/3D6CC0004B36624E" Ref="C19"  Part="1" 
AR Path="/3D5A40004B36624E" Ref="C19"  Part="1" 
AR Path="/402755814B36624E" Ref="C19"  Part="1" 
AR Path="/4030AAC04B36624E" Ref="C19"  Part="1" 
AR Path="/4031DDF34B36624E" Ref="C19"  Part="1" 
AR Path="/3FE88B434B36624E" Ref="C19"  Part="1" 
AR Path="/4032778D4B36624E" Ref="C19"  Part="1" 
AR Path="/403091264B36624E" Ref="C19"  Part="1" 
AR Path="/403051264B36624E" Ref="C19"  Part="1" 
AR Path="/4032F78D4B36624E" Ref="C19"  Part="1" 
AR Path="/403251264B36624E" Ref="C19"  Part="1" 
AR Path="/4032AAC04B36624E" Ref="C19"  Part="1" 
AR Path="/4030D1264B36624E" Ref="C19"  Part="1" 
AR Path="/4031778D4B36624E" Ref="C19"  Part="1" 
AR Path="/3FEA24DD4B36624E" Ref="C19"  Part="1" 
AR Path="/6FE934E34B36624E" Ref="C19"  Part="1" 
AR Path="/773F65F14B36624E" Ref="C19"  Part="1" 
AR Path="/773F8EB44B36624E" Ref="C19"  Part="1" 
AR Path="/23C9F04B36624E" Ref="C19"  Part="1" 
AR Path="/24B36624E" Ref="C19"  Part="1" 
AR Path="/23BC884B36624E" Ref="C19"  Part="1" 
AR Path="/DC0C124B36624E" Ref="C19"  Part="1" 
AR Path="/23C34C4B36624E" Ref="C19"  Part="1" 
AR Path="/23CBC44B36624E" Ref="C19"  Part="1" 
AR Path="/23C6504B36624E" Ref="C19"  Part="1" 
AR Path="/39803EA4B36624E" Ref="C19"  Part="1" 
AR Path="/FFFFFFFF4B36624E" Ref="C19"  Part="1" 
AR Path="/6FE934344B36624E" Ref="C19"  Part="1" 
AR Path="/7E0073254B36624E" Ref="C19"  Part="1" 
AR Path="/73254B36624E" Ref="C19"  Part="1" 
AR Path="/8CEE4B36624E" Ref="C19"  Part="1" 
AR Path="/23D9004B36624E" Ref="C19"  Part="1" 
AR Path="/91964B36624E" Ref="C19"  Part="1" 
AR Path="/77F16B254B36624E" Ref="C19"  Part="1" 
AR Path="/23D83C4B36624E" Ref="C19"  Part="1" 
AR Path="/47907FA4B36624E" Ref="C19"  Part="1" 
AR Path="/8D384B36624E" Ref="C19"  Part="1" 
AR Path="/D058E04B36624E" Ref="C19"  Part="1" 
AR Path="/3C64B36624E" Ref="C19"  Part="1" 
AR Path="/2F4A4B36624E" Ref="C19"  Part="1" 
AR Path="/D1C3384B36624E" Ref="C19"  Part="1" 
F 0 "C19" H 37450 21150 50  0000 L CNN
F 1 "0.1 uF" H 37450 20950 50  0000 L CNN
F 2 "C2" H 37400 21050 60  0001 C CNN
F 3 "" H 37400 21050 60  0001 C CNN
	1    37400 21050
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 4B366239
P 38300 21050
AR Path="/23D9D84B366239" Ref="C?"  Part="1" 
AR Path="/394433324B366239" Ref="C?"  Part="1" 
AR Path="/74B366239" Ref="C?"  Part="1" 
AR Path="/4B366239" Ref="C21"  Part="1" 
AR Path="/94B366239" Ref="C21"  Part="1" 
AR Path="/5AD7153D4B366239" Ref="C21"  Part="1" 
AR Path="/23D9304B366239" Ref="C21"  Part="1" 
AR Path="/6FE901F74B366239" Ref="C21"  Part="1" 
AR Path="/3FE224DD4B366239" Ref="C21"  Part="1" 
AR Path="/3FEFFFFF4B366239" Ref="C21"  Part="1" 
AR Path="/23D8D44B366239" Ref="C21"  Part="1" 
AR Path="/6FF0DD404B366239" Ref="C21"  Part="1" 
AR Path="/FFFFFFF04B366239" Ref="C21"  Part="1" 
AR Path="/402955814B366239" Ref="C21"  Part="1" 
AR Path="/DCBAABCD4B366239" Ref="C21"  Part="1" 
AR Path="/A84B366239" Ref="C21"  Part="1" 
AR Path="/A4B366239" Ref="C21"  Part="1" 
AR Path="/402C08B44B366239" Ref="C21"  Part="1" 
AR Path="/23D2034B366239" Ref="C21"  Part="1" 
AR Path="/4E4B366239" Ref="C21"  Part="1" 
AR Path="/402BEF1A4B366239" Ref="C21"  Part="1" 
AR Path="/54B366239" Ref="C21"  Part="1" 
AR Path="/40293BE74B366239" Ref="C21"  Part="1" 
AR Path="/402C55814B366239" Ref="C21"  Part="1" 
AR Path="/40273BE74B366239" Ref="C21"  Part="1" 
AR Path="/2600004B366239" Ref="C21"  Part="1" 
AR Path="/3D8EA0004B366239" Ref="C21"  Part="1" 
AR Path="/402908B44B366239" Ref="C21"  Part="1" 
AR Path="/3D6CC0004B366239" Ref="C21"  Part="1" 
AR Path="/3D5A40004B366239" Ref="C21"  Part="1" 
AR Path="/402755814B366239" Ref="C21"  Part="1" 
AR Path="/4030AAC04B366239" Ref="C21"  Part="1" 
AR Path="/4031DDF34B366239" Ref="C21"  Part="1" 
AR Path="/3FE88B434B366239" Ref="C21"  Part="1" 
AR Path="/4032778D4B366239" Ref="C21"  Part="1" 
AR Path="/403091264B366239" Ref="C21"  Part="1" 
AR Path="/403051264B366239" Ref="C21"  Part="1" 
AR Path="/4032F78D4B366239" Ref="C21"  Part="1" 
AR Path="/403251264B366239" Ref="C21"  Part="1" 
AR Path="/4032AAC04B366239" Ref="C21"  Part="1" 
AR Path="/4030D1264B366239" Ref="C21"  Part="1" 
AR Path="/4031778D4B366239" Ref="C21"  Part="1" 
AR Path="/3FEA24DD4B366239" Ref="C21"  Part="1" 
AR Path="/6FE934E34B366239" Ref="C21"  Part="1" 
AR Path="/773F65F14B366239" Ref="C21"  Part="1" 
AR Path="/773F8EB44B366239" Ref="C21"  Part="1" 
AR Path="/23C9F04B366239" Ref="C21"  Part="1" 
AR Path="/24B366239" Ref="C21"  Part="1" 
AR Path="/23BC884B366239" Ref="C21"  Part="1" 
AR Path="/DC0C124B366239" Ref="C21"  Part="1" 
AR Path="/23C34C4B366239" Ref="C21"  Part="1" 
AR Path="/23CBC44B366239" Ref="C21"  Part="1" 
AR Path="/23C6504B366239" Ref="C21"  Part="1" 
AR Path="/39803EA4B366239" Ref="C21"  Part="1" 
AR Path="/FFFFFFFF4B366239" Ref="C21"  Part="1" 
AR Path="/6FE934344B366239" Ref="C21"  Part="1" 
AR Path="/7E0073254B366239" Ref="C21"  Part="1" 
AR Path="/73254B366239" Ref="C21"  Part="1" 
AR Path="/8CEE4B366239" Ref="C21"  Part="1" 
AR Path="/23D9004B366239" Ref="C21"  Part="1" 
AR Path="/91964B366239" Ref="C21"  Part="1" 
AR Path="/77F16B254B366239" Ref="C21"  Part="1" 
AR Path="/23D83C4B366239" Ref="C21"  Part="1" 
AR Path="/47907FA4B366239" Ref="C21"  Part="1" 
AR Path="/14B366239" Ref="C21"  Part="1" 
AR Path="/8D384B366239" Ref="C21"  Part="1" 
AR Path="/D058E04B366239" Ref="C21"  Part="1" 
AR Path="/3C64B366239" Ref="C21"  Part="1" 
AR Path="/2F4A4B366239" Ref="C21"  Part="1" 
AR Path="/D1C3384B366239" Ref="C21"  Part="1" 
F 0 "C21" H 38350 21150 50  0000 L CNN
F 1 "0.1 uF" H 38350 20950 50  0000 L CNN
F 2 "C2" H 38300 21050 60  0001 C CNN
F 3 "" H 38300 21050 60  0001 C CNN
	1    38300 21050
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 4B2EC5DA
P 36500 22250
AR Path="/69698AFC4B2EC5DA" Ref="C?"  Part="1" 
AR Path="/393639364B2EC5DA" Ref="C?"  Part="1" 
AR Path="/4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23CF784B2EC5DA" Ref="C?"  Part="1" 
AR Path="/94B2EC5DA" Ref="C18"  Part="1" 
AR Path="/FFFFFFF04B2EC5DA" Ref="C18"  Part="1" 
AR Path="/5AD7153D4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/DCBAABCD4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/6FE901F74B2EC5DA" Ref="C18"  Part="1" 
AR Path="/3FEFFFFF4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23D2034B2EC5DA" Ref="C18"  Part="1" 
AR Path="/A84B2EC5DA" Ref="C18"  Part="1" 
AR Path="/A4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/755D912A4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/6FF0DD404B2EC5DA" Ref="C18"  Part="1" 
AR Path="/14B2EC5DA" Ref="C18"  Part="1" 
AR Path="/3FE224DD4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23D8D44B2EC5DA" Ref="C18"  Part="1" 
AR Path="/402955814B2EC5DA" Ref="C18"  Part="1" 
AR Path="/402C08B44B2EC5DA" Ref="C18"  Part="1" 
AR Path="/402BEF1A4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/40293BE74B2EC5DA" Ref="C18"  Part="1" 
AR Path="/402C55814B2EC5DA" Ref="C18"  Part="1" 
AR Path="/40273BE74B2EC5DA" Ref="C18"  Part="1" 
AR Path="/2600004B2EC5DA" Ref="C18"  Part="1" 
AR Path="/3D8EA0004B2EC5DA" Ref="C18"  Part="1" 
AR Path="/402908B44B2EC5DA" Ref="C18"  Part="1" 
AR Path="/3D6CC0004B2EC5DA" Ref="C18"  Part="1" 
AR Path="/3D5A40004B2EC5DA" Ref="C18"  Part="1" 
AR Path="/402755814B2EC5DA" Ref="C18"  Part="1" 
AR Path="/4030AAC04B2EC5DA" Ref="C18"  Part="1" 
AR Path="/4031DDF34B2EC5DA" Ref="C18"  Part="1" 
AR Path="/3FE88B434B2EC5DA" Ref="C18"  Part="1" 
AR Path="/4032778D4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/403091264B2EC5DA" Ref="C18"  Part="1" 
AR Path="/403051264B2EC5DA" Ref="C18"  Part="1" 
AR Path="/4032F78D4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/403251264B2EC5DA" Ref="C18"  Part="1" 
AR Path="/4032AAC04B2EC5DA" Ref="C18"  Part="1" 
AR Path="/4030D1264B2EC5DA" Ref="C18"  Part="1" 
AR Path="/4031778D4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/3FEA24DD4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/6FE934E34B2EC5DA" Ref="C18"  Part="1" 
AR Path="/773F65F14B2EC5DA" Ref="C18"  Part="1" 
AR Path="/773F8EB44B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23C9F04B2EC5DA" Ref="C18"  Part="1" 
AR Path="/24B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23BC884B2EC5DA" Ref="C18"  Part="1" 
AR Path="/DC0C124B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23C34C4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23CBC44B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23C6504B2EC5DA" Ref="C18"  Part="1" 
AR Path="/39803EA4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/FFFFFFFF4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/6FE934344B2EC5DA" Ref="C18"  Part="1" 
AR Path="/7E0073254B2EC5DA" Ref="C18"  Part="1" 
AR Path="/73254B2EC5DA" Ref="C18"  Part="1" 
AR Path="/8CEE4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23D9004B2EC5DA" Ref="C18"  Part="1" 
AR Path="/91964B2EC5DA" Ref="C18"  Part="1" 
AR Path="/77F16B254B2EC5DA" Ref="C18"  Part="1" 
AR Path="/23D83C4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/47907FA4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/8D384B2EC5DA" Ref="C18"  Part="1" 
AR Path="/D058E04B2EC5DA" Ref="C18"  Part="1" 
AR Path="/3C64B2EC5DA" Ref="C18"  Part="1" 
AR Path="/2F4A4B2EC5DA" Ref="C18"  Part="1" 
AR Path="/D1C3384B2EC5DA" Ref="C18"  Part="1" 
F 0 "C18" H 36550 22350 50  0000 L CNN
F 1 "0.1 uF" H 36550 22150 50  0000 L CNN
F 2 "C2" H 36500 22250 60  0001 C CNN
F 3 "" H 36500 22250 60  0001 C CNN
	1    36500 22250
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 4AEDE3A8
P 36950 22250
AR Path="/23D9B04AEDE3A8" Ref="C?"  Part="1" 
AR Path="/394433324AEDE3A8" Ref="C?"  Part="1" 
AR Path="/23D6B44AEDE3A8" Ref="C?"  Part="1" 
AR Path="/7E44048F4AEDE3A8" Ref="0.1"  Part="0" 
AR Path="/2306E64AEDE3A8" Ref="C"  Part="1" 
AR Path="/23D6544AEDE3A8" Ref="0.1"  Part="0" 
AR Path="/5D06BC4AEDE3A8" Ref="C"  Part="1" 
AR Path="/4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/94AEDE3A8" Ref="C17"  Part="1" 
AR Path="/5AD7153D4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/DCBAABCD4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/755D912A4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/6FE901F74AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402688B44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/A4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3FEFFFFF4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23D9304AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3FE441894AEDE3A8" Ref="C17"  Part="1" 
AR Path="/2600004AEDE3A8" Ref="C17"  Part="1" 
AR Path="/B84AEDE3A8" Ref="C17"  Part="1" 
AR Path="/FFFFFFF04AEDE3A8" Ref="C17"  Part="1" 
AR Path="/6FF0DD404AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3DA360004AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4029D5814AEDE3A8" Ref="C17"  Part="1" 
AR Path="/40256F1A4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23D8D44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3D8EA0004AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3D7E00004AEDE3A8" Ref="C17"  Part="1" 
AR Path="/40286F1A4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402A08B44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402588B44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23D2034AEDE3A8" Ref="C17"  Part="1" 
AR Path="/A84AEDE3A8" Ref="C17"  Part="1" 
AR Path="/14AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3FE224DD4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402955814AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402C08B44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402BEF1A4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/40293BE74AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402C55814AEDE3A8" Ref="C17"  Part="1" 
AR Path="/40273BE74AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402908B44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3D6CC0004AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3D5A40004AEDE3A8" Ref="C17"  Part="1" 
AR Path="/402755814AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4030AAC04AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4031DDF34AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3FE88B434AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4032778D4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/403091264AEDE3A8" Ref="C17"  Part="1" 
AR Path="/403051264AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4032F78D4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/403251264AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4032AAC04AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4030D1264AEDE3A8" Ref="C17"  Part="1" 
AR Path="/4031778D4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3FEA24DD4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/6FE934E34AEDE3A8" Ref="C17"  Part="1" 
AR Path="/773F8EB44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23C9F04AEDE3A8" Ref="C17"  Part="1" 
AR Path="/24AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23BC884AEDE3A8" Ref="C17"  Part="1" 
AR Path="/DC0C124AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23C34C4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23CBC44AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23C6504AEDE3A8" Ref="C17"  Part="1" 
AR Path="/39803EA4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/FFFFFFFF4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/6FE934344AEDE3A8" Ref="C17"  Part="1" 
AR Path="/7E0073254AEDE3A8" Ref="C17"  Part="1" 
AR Path="/73254AEDE3A8" Ref="C17"  Part="1" 
AR Path="/8CEE4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23D9004AEDE3A8" Ref="C17"  Part="1" 
AR Path="/91964AEDE3A8" Ref="C17"  Part="1" 
AR Path="/77F16B254AEDE3A8" Ref="C17"  Part="1" 
AR Path="/23D83C4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/47907FA4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/8D384AEDE3A8" Ref="C17"  Part="1" 
AR Path="/D058E04AEDE3A8" Ref="C17"  Part="1" 
AR Path="/3C64AEDE3A8" Ref="C17"  Part="1" 
AR Path="/2F4A4AEDE3A8" Ref="C17"  Part="1" 
AR Path="/D1C3384AEDE3A8" Ref="C17"  Part="1" 
F 0 "C17" H 37000 22350 50  0000 L CNN
F 1 "0.1 uF" H 37000 22150 50  0000 L CNN
F 2 "C2" H 36950 22250 60  0001 C CNN
F 3 "" H 36950 22250 60  0001 C CNN
	1    36950 22250
	1    0    0    -1  
$EndComp
NoConn ~ 41500 11750
NoConn ~ 41500 6750
NoConn ~ 41500 13650
NoConn ~ 41500 13450
NoConn ~ 41500 9350
NoConn ~ 41500 8650
Text Label 40350 13550 0    60   ~ 0
GND
$Comp
L JUMPER JP3
U 1 1 4ADA2D9F
P 41200 13550
AR Path="/4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/94ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/DCBAABCD4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/755D912A4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23D9304ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/5AD7153D4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/A4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/6FE901F74ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3D6CC0004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/6FF0DD404ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/14ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/FFFFFFF04ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3FEFFFFF4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23C2344ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3D7E00004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3D8EA0004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/5AD746F64ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4027EF1A4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402A6F1A4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4029BBE74ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402A224D4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23D8D44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402A55814ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4026224D4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3D6220004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3D9720004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/2600004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402C3BE74ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402955814ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/40293BE74ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402555814ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3FE88B434ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23D2034ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402988B44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3FED58104ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402688B44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3FE441894ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/B84ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3DA360004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4029D5814ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/40256F1A4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/40286F1A4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402A08B44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402588B44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/A84ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3FE224DD4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402C08B44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402BEF1A4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402C55814ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/40273BE74ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402908B44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3D5A40004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/402755814ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4030AAC04ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4031DDF34ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4032778D4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/403091264ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/403051264ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4032F78D4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/403251264ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4032AAC04ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4030D1264ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/4031778D4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3FEA24DD4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/6FE934E34ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/773F65F14ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/773F8EB44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23C9F04ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/24ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23BC884ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/DC0C124ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23C34C4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23CBC44ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23C6504ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/39803EA4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/FFFFFFFF4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/6FE934344ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/7E0073254ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/73254ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/8CEE4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23D9004ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/91964ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/77F16B254ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/23D83C4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/47907FA4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/8D384ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/D058E04ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/3C64ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/2F4A4ADA2D9F" Ref="JP3"  Part="1" 
AR Path="/D1C3384ADA2D9F" Ref="JP3"  Part="1" 
F 0 "JP3" H 41200 13700 60  0000 C CNN
F 1 "JUMPER" H 41200 13470 40  0000 C CNN
F 2 "SIL-2" H 41200 13550 60  0001 C CNN
F 3 "" H 41200 13550 60  0001 C CNN
	1    41200 13550
	1    0    0    -1  
$EndComp
$Comp
L JUMPER JP2
U 1 1 4ADA2D99
P 41200 11850
AR Path="/4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/94ADA2D99" Ref="JP2"  Part="1" 
AR Path="/755D912A4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/5AD7153D4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/A4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/6FE901F74ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3D6CC0004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/6FF0DD404ADA2D99" Ref="JP2"  Part="1" 
AR Path="/14ADA2D99" Ref="JP2"  Part="1" 
AR Path="/FFFFFFF04ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3FEFFFFF4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23C2344ADA2D99" Ref="JP2"  Part="1" 
AR Path="/DCBAABCD4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3D7E00004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3D8EA0004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/5AD746F64ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4027EF1A4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402A6F1A4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4029BBE74ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402A224D4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23D8D44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402A55814ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4026224D4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3D6220004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3D9720004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/2600004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402C3BE74ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402955814ADA2D99" Ref="JP2"  Part="1" 
AR Path="/40293BE74ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402555814ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3FE88B434ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23D2034ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402988B44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3FED58104ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402688B44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3FE441894ADA2D99" Ref="JP2"  Part="1" 
AR Path="/B84ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3DA360004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4029D5814ADA2D99" Ref="JP2"  Part="1" 
AR Path="/40256F1A4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/40286F1A4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402A08B44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402588B44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/A84ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3FE224DD4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402C08B44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402BEF1A4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402C55814ADA2D99" Ref="JP2"  Part="1" 
AR Path="/40273BE74ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402908B44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3D5A40004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/402755814ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4030AAC04ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4031DDF34ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4032778D4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/403091264ADA2D99" Ref="JP2"  Part="1" 
AR Path="/403051264ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4032F78D4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/403251264ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4032AAC04ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4030D1264ADA2D99" Ref="JP2"  Part="1" 
AR Path="/4031778D4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3FEA24DD4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/6FE934E34ADA2D99" Ref="JP2"  Part="1" 
AR Path="/773F65F14ADA2D99" Ref="JP2"  Part="1" 
AR Path="/773F8EB44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23C9F04ADA2D99" Ref="JP2"  Part="1" 
AR Path="/24ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23BC884ADA2D99" Ref="JP2"  Part="1" 
AR Path="/DC0C124ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23C34C4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23CBC44ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23C6504ADA2D99" Ref="JP2"  Part="1" 
AR Path="/39803EA4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/FFFFFFFF4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/6FE934344ADA2D99" Ref="JP2"  Part="1" 
AR Path="/7E0073254ADA2D99" Ref="JP2"  Part="1" 
AR Path="/73254ADA2D99" Ref="JP2"  Part="1" 
AR Path="/8CEE4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23D9004ADA2D99" Ref="JP2"  Part="1" 
AR Path="/91964ADA2D99" Ref="JP2"  Part="1" 
AR Path="/77F16B254ADA2D99" Ref="JP2"  Part="1" 
AR Path="/23D83C4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/47907FA4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/8D384ADA2D99" Ref="JP2"  Part="1" 
AR Path="/D058E04ADA2D99" Ref="JP2"  Part="1" 
AR Path="/3C64ADA2D99" Ref="JP2"  Part="1" 
AR Path="/2F4A4ADA2D99" Ref="JP2"  Part="1" 
AR Path="/D1C3384ADA2D99" Ref="JP2"  Part="1" 
F 0 "JP2" H 41200 12000 60  0000 C CNN
F 1 "JUMPER" H 41200 11770 40  0000 C CNN
F 2 "SIL-2" H 41200 11850 60  0001 C CNN
F 3 "" H 41200 11850 60  0001 C CNN
	1    41200 11850
	1    0    0    -1  
$EndComp
Text Label 40350 11850 0    60   ~ 0
GND
Text Label 40350 8550 0    60   ~ 0
GND
$Comp
L JUMPER JP?
U 1 1 4ADA2D5E
P 41200 8550
AR Path="/23D9B04ADA2D5E" Ref="JP?"  Part="1" 
AR Path="/394433324ADA2D5E" Ref="JP?"  Part="1" 
AR Path="/4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/94ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/755D912A4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/5AD7153D4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/A4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/6FE901F74ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3D6CC0004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/6FF0DD404ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/14ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/FFFFFFF04ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3FEFFFFF4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23C2344ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/DCBAABCD4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3D7E00004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3D8EA0004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/5AD746F64ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4027EF1A4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402A6F1A4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4029BBE74ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402A224D4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23D8D44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402A55814ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4026224D4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3D6220004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3D9720004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/2600004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402C3BE74ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402955814ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/40293BE74ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402555814ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3FE88B434ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23D2034ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402988B44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3FED58104ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402688B44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3FE441894ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/B84ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3DA360004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4029D5814ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/40256F1A4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/40286F1A4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402A08B44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402588B44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/A84ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3FE224DD4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402C08B44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402BEF1A4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402C55814ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/40273BE74ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402908B44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3D5A40004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/402755814ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4030AAC04ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4031DDF34ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4032778D4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/403091264ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/403051264ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4032F78D4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/403251264ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4032AAC04ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4030D1264ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/4031778D4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3FEA24DD4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/6FE934E34ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/773F65F14ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/773F8EB44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23C9F04ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/24ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23BC884ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/DC0C124ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23C34C4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23CBC44ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23C6504ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/39803EA4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/FFFFFFFF4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/6FE934344ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/7E0073254ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/73254ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/8CEE4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23D9004ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/91964ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/77F16B254ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/23D83C4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/47907FA4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/8D384ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/D058E04ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/3C64ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/2F4A4ADA2D5E" Ref="JP1"  Part="1" 
AR Path="/D1C3384ADA2D5E" Ref="JP1"  Part="1" 
F 0 "JP1" H 41200 8700 60  0000 C CNN
F 1 "JUMPER" H 41200 8470 40  0000 C CNN
F 2 "SIL-2" H 41200 8550 60  0001 C CNN
F 3 "" H 41200 8550 60  0001 C CNN
	1    41200 8550
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 4AD0EE1B
P 36950 21050
AR Path="/7A4AD0EE1B" Ref="C?"  Part="1" 
AR Path="/23D1104AD0EE1B" Ref="C16"  Part="1" 
AR Path="/314433324AD0EE1B" Ref="C16"  Part="1" 
AR Path="/5AD7153D4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/DCBAABCD4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/6FE901F74AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402988B44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/94AD0EE1B" Ref="C16"  Part="1" 
AR Path="/755D912A4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/A4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3D6CC0004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/6FF0DD404AD0EE1B" Ref="C16"  Part="1" 
AR Path="/14AD0EE1B" Ref="C16"  Part="1" 
AR Path="/FFFFFFF04AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3FEFFFFF4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23C2344AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3D7E00004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3D8EA0004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/5AD746F64AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4027EF1A4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402A6F1A4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4029BBE74AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402A224D4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23D8D44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402A55814AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4026224D4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3D6220004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3D9720004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/2600004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402C3BE74AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402955814AD0EE1B" Ref="C16"  Part="1" 
AR Path="/40293BE74AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402555814AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3FE88B434AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23D2034AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3FED58104AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402688B44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3FE441894AD0EE1B" Ref="C16"  Part="1" 
AR Path="/B84AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3DA360004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4029D5814AD0EE1B" Ref="C16"  Part="1" 
AR Path="/40256F1A4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/40286F1A4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402A08B44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402588B44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/A84AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3FE224DD4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402C08B44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402BEF1A4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402C55814AD0EE1B" Ref="C16"  Part="1" 
AR Path="/40273BE74AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402908B44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3D5A40004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/402755814AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4030AAC04AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4031DDF34AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4032778D4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/403091264AD0EE1B" Ref="C16"  Part="1" 
AR Path="/403051264AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4032F78D4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/403251264AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4032AAC04AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4030D1264AD0EE1B" Ref="C16"  Part="1" 
AR Path="/4031778D4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3FEA24DD4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/6FE934E34AD0EE1B" Ref="C16"  Part="1" 
AR Path="/773F65F14AD0EE1B" Ref="C16"  Part="1" 
AR Path="/773F8EB44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23C9F04AD0EE1B" Ref="C16"  Part="1" 
AR Path="/24AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23BC884AD0EE1B" Ref="C16"  Part="1" 
AR Path="/DC0C124AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23C34C4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23CBC44AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23C6504AD0EE1B" Ref="C16"  Part="1" 
AR Path="/39803EA4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/FFFFFFFF4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/6FE934344AD0EE1B" Ref="C16"  Part="1" 
AR Path="/7E0073254AD0EE1B" Ref="C16"  Part="1" 
AR Path="/73254AD0EE1B" Ref="C16"  Part="1" 
AR Path="/8CEE4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23D9004AD0EE1B" Ref="C16"  Part="1" 
AR Path="/91964AD0EE1B" Ref="C16"  Part="1" 
AR Path="/77F16B254AD0EE1B" Ref="C16"  Part="1" 
AR Path="/23D83C4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/47907FA4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/8D384AD0EE1B" Ref="C16"  Part="1" 
AR Path="/D058E04AD0EE1B" Ref="C16"  Part="1" 
AR Path="/3C64AD0EE1B" Ref="C16"  Part="1" 
AR Path="/2F4A4AD0EE1B" Ref="C16"  Part="1" 
AR Path="/D1C3384AD0EE1B" Ref="C16"  Part="1" 
F 0 "C16" H 37000 21150 50  0000 L CNN
F 1 "0.1 uF" H 37000 20950 50  0000 L CNN
F 2 "C2" H 36950 21050 60  0001 C CNN
F 3 "" H 36950 21050 60  0001 C CNN
	1    36950 21050
	1    0    0    -1  
$EndComp
Text Label 41550 11650 0    60   ~ 0
+8V
$Comp
L VCC #PWR?
U 1 1 4AC79AEE
P 36500 20850
AR Path="/69698AFC4AC79AEE" Ref="#PWR?"  Part="1" 
AR Path="/393639364AC79AEE" Ref="#PWR?"  Part="1" 
AR Path="/4AC79AEE" Ref="#PWR039"  Part="1" 
AR Path="/94AC79AEE" Ref="#PWR25"  Part="1" 
AR Path="/25E4AC79AEE" Ref="#PWR036"  Part="1" 
AR Path="/6FF0DD404AC79AEE" Ref="#PWR055"  Part="1" 
AR Path="/DCBAABCD4AC79AEE" Ref="#PWR026"  Part="1" 
AR Path="/755D912A4AC79AEE" Ref="#PWR014"  Part="1" 
AR Path="/6FE901F74AC79AEE" Ref="#PWR011"  Part="1" 
AR Path="/3FE08B434AC79AEE" Ref="#PWR01"  Part="1" 
AR Path="/3DA360004AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/5AD7153D4AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/A4AC79AEE" Ref="#PWR050"  Part="1" 
AR Path="/23D9304AC79AEE" Ref="#PWR01"  Part="1" 
AR Path="/3FEFFFFF4AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/23D8D44AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/3D8EA0004AC79AEE" Ref="#PWR026"  Part="1" 
AR Path="/402B08B44AC79AEE" Ref="#PWR01"  Part="1" 
AR Path="/4026A24D4AC79AEE" Ref="#PWR01"  Part="1" 
AR Path="/23C7004AC79AEE" Ref="#PWR012"  Part="1" 
AR Path="/3FE88B434AC79AEE" Ref="#PWR028"  Part="1" 
AR Path="/3D7E00004AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/402988B44AC79AEE" Ref="#PWR011"  Part="1" 
AR Path="/3D6CC0004AC79AEE" Ref="#PWR019"  Part="1" 
AR Path="/14AC79AEE" Ref="#PWR068"  Part="1" 
AR Path="/363030314AC79AEE" Ref="#PWR036"  Part="1" 
AR Path="/FFFFFFF04AC79AEE" Ref="#PWR62"  Part="1" 
AR Path="/23C2344AC79AEE" Ref="#PWR023"  Part="1" 
AR Path="/5AD746F64AC79AEE" Ref="#PWR024"  Part="1" 
AR Path="/4027EF1A4AC79AEE" Ref="#PWR024"  Part="1" 
AR Path="/402A6F1A4AC79AEE" Ref="#PWR024"  Part="1" 
AR Path="/4029BBE74AC79AEE" Ref="#PWR024"  Part="1" 
AR Path="/402A224D4AC79AEE" Ref="#PWR024"  Part="1" 
AR Path="/402A55814AC79AEE" Ref="#PWR023"  Part="1" 
AR Path="/4026224D4AC79AEE" Ref="#PWR011"  Part="1" 
AR Path="/3D6220004AC79AEE" Ref="#PWR023"  Part="1" 
AR Path="/3D9720004AC79AEE" Ref="#PWR024"  Part="1" 
AR Path="/2600004AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/402C3BE74AC79AEE" Ref="#PWR011"  Part="1" 
AR Path="/402955814AC79AEE" Ref="#PWR018"  Part="1" 
AR Path="/40293BE74AC79AEE" Ref="#PWR022"  Part="1" 
AR Path="/402555814AC79AEE" Ref="#PWR011"  Part="1" 
AR Path="/23D2034AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/3FED58104AC79AEE" Ref="#PWR011"  Part="1" 
AR Path="/402688B44AC79AEE" Ref="#PWR011"  Part="1" 
AR Path="/3FE441894AC79AEE" Ref="#PWR012"  Part="1" 
AR Path="/B84AC79AEE" Ref="#PWR12"  Part="1" 
AR Path="/4029D5814AC79AEE" Ref="#PWR012"  Part="1" 
AR Path="/40256F1A4AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/40286F1A4AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/402A08B44AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/402588B44AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/A84AC79AEE" Ref="#PWR24"  Part="1" 
AR Path="/3FE224DD4AC79AEE" Ref="#PWR018"  Part="1" 
AR Path="/402C08B44AC79AEE" Ref="#PWR019"  Part="1" 
AR Path="/314AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/402BEF1A4AC79AEE" Ref="#PWR020"  Part="1" 
AR Path="/402C55814AC79AEE" Ref="#PWR025"  Part="1" 
AR Path="/40273BE74AC79AEE" Ref="#PWR026"  Part="1" 
AR Path="/402908B44AC79AEE" Ref="#PWR022"  Part="1" 
AR Path="/3D5A40004AC79AEE" Ref="#PWR019"  Part="1" 
AR Path="/402755814AC79AEE" Ref="#PWR027"  Part="1" 
AR Path="/4030AAC04AC79AEE" Ref="#PWR027"  Part="1" 
AR Path="/4031DDF34AC79AEE" Ref="#PWR028"  Part="1" 
AR Path="/4032778D4AC79AEE" Ref="#PWR034"  Part="1" 
AR Path="/403091264AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/403051264AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/4032F78D4AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/403251264AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/4032AAC04AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/4030D1264AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/4031778D4AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/3FEA24DD4AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/6FE934E34AC79AEE" Ref="#PWR053"  Part="1" 
AR Path="/773F65F14AC79AEE" Ref="#PWR055"  Part="1" 
AR Path="/773F8EB44AC79AEE" Ref="#PWR053"  Part="1" 
AR Path="/23C9F04AC79AEE" Ref="#PWR42"  Part="1" 
AR Path="/23BC884AC79AEE" Ref="#PWR055"  Part="1" 
AR Path="/DC0C124AC79AEE" Ref="#PWR037"  Part="1" 
AR Path="/6684D64AC79AEE" Ref="#PWR053"  Part="1" 
AR Path="/23C34C4AC79AEE" Ref="#PWR054"  Part="1" 
AR Path="/23CBC44AC79AEE" Ref="#PWR053"  Part="1" 
AR Path="/39803EA4AC79AEE" Ref="#PWR038"  Part="1" 
AR Path="/FFFFFFFF4AC79AEE" Ref="#PWR055"  Part="1" 
AR Path="/6FE934344AC79AEE" Ref="#PWR55"  Part="1" 
AR Path="/7E0073254AC79AEE" Ref="#PWR08"  Part="1" 
AR Path="/73254AC79AEE" Ref="#PWR081"  Part="1" 
AR Path="/8CEE4AC79AEE" Ref="#PWR081"  Part="1" 
AR Path="/23D9004AC79AEE" Ref="#PWR053"  Part="1" 
AR Path="/23C6504AC79AEE" Ref="#PWR065"  Part="1" 
AR Path="/91964AC79AEE" Ref="#PWR061"  Part="1" 
AR Path="/77F16B254AC79AEE" Ref="#PWR048"  Part="1" 
AR Path="/23D83C4AC79AEE" Ref="#PWR59"  Part="1" 
AR Path="/47907FA4AC79AEE" Ref="#PWR08"  Part="1" 
AR Path="/8D384AC79AEE" Ref="#PWR068"  Part="1" 
AR Path="/D058E04AC79AEE" Ref="#PWR055"  Part="1" 
AR Path="/3C64AC79AEE" Ref="#PWR069"  Part="1" 
AR Path="/2F4A4AC79AEE" Ref="#PWR069"  Part="1" 
AR Path="/24AC79AEE" Ref="#PWR44"  Part="1" 
AR Path="/D1C3384AC79AEE" Ref="#PWR055"  Part="1" 
F 0 "#PWR039" H 36500 20950 30  0001 C CNN
F 1 "VCC" H 36500 20950 30  0000 C CNN
F 2 "" H 36500 20850 60  0001 C CNN
F 3 "" H 36500 20850 60  0001 C CNN
	1    36500 20850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 4AC79AE6
P 36950 21300
AR Path="/69698AFC4AC79AE6" Ref="#PWR?"  Part="1" 
AR Path="/393639364AC79AE6" Ref="#PWR?"  Part="1" 
AR Path="/4AC79AE6" Ref="#PWR040"  Part="1" 
AR Path="/94AC79AE6" Ref="#PWR28"  Part="1" 
AR Path="/25E4AC79AE6" Ref="#PWR037"  Part="1" 
AR Path="/6FF0DD404AC79AE6" Ref="#PWR056"  Part="1" 
AR Path="/DCBAABCD4AC79AE6" Ref="#PWR027"  Part="1" 
AR Path="/755D912A4AC79AE6" Ref="#PWR015"  Part="1" 
AR Path="/6FE901F74AC79AE6" Ref="#PWR012"  Part="1" 
AR Path="/3FE08B434AC79AE6" Ref="#PWR02"  Part="1" 
AR Path="/3DA360004AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/5AD7153D4AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/A4AC79AE6" Ref="#PWR051"  Part="1" 
AR Path="/23D9304AC79AE6" Ref="#PWR02"  Part="1" 
AR Path="/3FEFFFFF4AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/23D8D44AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/3D8EA0004AC79AE6" Ref="#PWR027"  Part="1" 
AR Path="/402B08B44AC79AE6" Ref="#PWR02"  Part="1" 
AR Path="/4026A24D4AC79AE6" Ref="#PWR02"  Part="1" 
AR Path="/23C7004AC79AE6" Ref="#PWR013"  Part="1" 
AR Path="/3FE88B434AC79AE6" Ref="#PWR029"  Part="1" 
AR Path="/3D7E00004AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/402988B44AC79AE6" Ref="#PWR012"  Part="1" 
AR Path="/3D6CC0004AC79AE6" Ref="#PWR020"  Part="1" 
AR Path="/14AC79AE6" Ref="#PWR069"  Part="1" 
AR Path="/363030314AC79AE6" Ref="#PWR037"  Part="1" 
AR Path="/FFFFFFF04AC79AE6" Ref="#PWR67"  Part="1" 
AR Path="/23C2344AC79AE6" Ref="#PWR024"  Part="1" 
AR Path="/5AD746F64AC79AE6" Ref="#PWR025"  Part="1" 
AR Path="/4027EF1A4AC79AE6" Ref="#PWR025"  Part="1" 
AR Path="/402A6F1A4AC79AE6" Ref="#PWR025"  Part="1" 
AR Path="/4029BBE74AC79AE6" Ref="#PWR025"  Part="1" 
AR Path="/402A224D4AC79AE6" Ref="#PWR025"  Part="1" 
AR Path="/402A55814AC79AE6" Ref="#PWR024"  Part="1" 
AR Path="/4026224D4AC79AE6" Ref="#PWR012"  Part="1" 
AR Path="/3D6220004AC79AE6" Ref="#PWR024"  Part="1" 
AR Path="/3D9720004AC79AE6" Ref="#PWR025"  Part="1" 
AR Path="/2600004AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/402C3BE74AC79AE6" Ref="#PWR012"  Part="1" 
AR Path="/402955814AC79AE6" Ref="#PWR019"  Part="1" 
AR Path="/40293BE74AC79AE6" Ref="#PWR023"  Part="1" 
AR Path="/402555814AC79AE6" Ref="#PWR012"  Part="1" 
AR Path="/23D2034AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/3FED58104AC79AE6" Ref="#PWR012"  Part="1" 
AR Path="/402688B44AC79AE6" Ref="#PWR012"  Part="1" 
AR Path="/3FE441894AC79AE6" Ref="#PWR013"  Part="1" 
AR Path="/B84AC79AE6" Ref="#PWR13"  Part="1" 
AR Path="/4029D5814AC79AE6" Ref="#PWR013"  Part="1" 
AR Path="/40256F1A4AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/40286F1A4AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/402A08B44AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/402588B44AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/A84AC79AE6" Ref="#PWR27"  Part="1" 
AR Path="/3FE224DD4AC79AE6" Ref="#PWR019"  Part="1" 
AR Path="/402C08B44AC79AE6" Ref="#PWR020"  Part="1" 
AR Path="/314AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/402BEF1A4AC79AE6" Ref="#PWR021"  Part="1" 
AR Path="/402C55814AC79AE6" Ref="#PWR026"  Part="1" 
AR Path="/40273BE74AC79AE6" Ref="#PWR027"  Part="1" 
AR Path="/402908B44AC79AE6" Ref="#PWR023"  Part="1" 
AR Path="/3D5A40004AC79AE6" Ref="#PWR020"  Part="1" 
AR Path="/402755814AC79AE6" Ref="#PWR028"  Part="1" 
AR Path="/4030AAC04AC79AE6" Ref="#PWR028"  Part="1" 
AR Path="/4031DDF34AC79AE6" Ref="#PWR029"  Part="1" 
AR Path="/74AC79AE6" Ref="#PWR20"  Part="1" 
AR Path="/4032778D4AC79AE6" Ref="#PWR035"  Part="1" 
AR Path="/403091264AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/403051264AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/4032F78D4AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/403251264AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/4032AAC04AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/4030D1264AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/4031778D4AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/3FEA24DD4AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/6FE934E34AC79AE6" Ref="#PWR054"  Part="1" 
AR Path="/773F65F14AC79AE6" Ref="#PWR056"  Part="1" 
AR Path="/773F8EB44AC79AE6" Ref="#PWR054"  Part="1" 
AR Path="/23C9F04AC79AE6" Ref="#PWR49"  Part="1" 
AR Path="/23BC884AC79AE6" Ref="#PWR056"  Part="1" 
AR Path="/DC0C124AC79AE6" Ref="#PWR038"  Part="1" 
AR Path="/6684D64AC79AE6" Ref="#PWR054"  Part="1" 
AR Path="/23C34C4AC79AE6" Ref="#PWR055"  Part="1" 
AR Path="/23CBC44AC79AE6" Ref="#PWR054"  Part="1" 
AR Path="/39803EA4AC79AE6" Ref="#PWR039"  Part="1" 
AR Path="/FFFFFFFF4AC79AE6" Ref="#PWR056"  Part="1" 
AR Path="/6FE934344AC79AE6" Ref="#PWR60"  Part="1" 
AR Path="/7E0073254AC79AE6" Ref="#PWR09"  Part="1" 
AR Path="/73254AC79AE6" Ref="#PWR082"  Part="1" 
AR Path="/8CEE4AC79AE6" Ref="#PWR082"  Part="1" 
AR Path="/23D9004AC79AE6" Ref="#PWR054"  Part="1" 
AR Path="/23C6504AC79AE6" Ref="#PWR066"  Part="1" 
AR Path="/91964AC79AE6" Ref="#PWR062"  Part="1" 
AR Path="/77F16B254AC79AE6" Ref="#PWR049"  Part="1" 
AR Path="/23D83C4AC79AE6" Ref="#PWR63"  Part="1" 
AR Path="/47907FA4AC79AE6" Ref="#PWR09"  Part="1" 
AR Path="/8D384AC79AE6" Ref="#PWR069"  Part="1" 
AR Path="/D058E04AC79AE6" Ref="#PWR056"  Part="1" 
AR Path="/3C64AC79AE6" Ref="#PWR070"  Part="1" 
AR Path="/2F4A4AC79AE6" Ref="#PWR070"  Part="1" 
AR Path="/24AC79AE6" Ref="#PWR49"  Part="1" 
AR Path="/D1C3384AC79AE6" Ref="#PWR056"  Part="1" 
F 0 "#PWR040" H 36950 21300 30  0001 C CNN
F 1 "GND" H 36950 21230 30  0001 C CNN
F 2 "" H 36950 21300 60  0001 C CNN
F 3 "" H 36950 21300 60  0001 C CNN
	1    36950 21300
	1    0    0    -1  
$EndComp
$Comp
L C C15
U 1 1 4AC79AB8
P 36500 21050
AR Path="/4AC79AB8" Ref="C15"  Part="1" 
AR Path="/94AC79AB8" Ref="C15"  Part="1" 
AR Path="/14AC79AB8" Ref="C15"  Part="1" 
AR Path="/6FF0DD404AC79AB8" Ref="C15"  Part="1" 
AR Path="/DCBAABCD4AC79AB8" Ref="C15"  Part="1" 
AR Path="/755D912A4AC79AB8" Ref="C15"  Part="1" 
AR Path="/6FE901F74AC79AB8" Ref="C15"  Part="1" 
AR Path="/3FE08B434AC79AB8" Ref="C15"  Part="1" 
AR Path="/3DA360004AC79AB8" Ref="C15"  Part="1" 
AR Path="/5AD7153D4AC79AB8" Ref="C15"  Part="1" 
AR Path="/A4AC79AB8" Ref="C15"  Part="1" 
AR Path="/23D9304AC79AB8" Ref="C15"  Part="1" 
AR Path="/3FEFFFFF4AC79AB8" Ref="C15"  Part="1" 
AR Path="/23D8D44AC79AB8" Ref="C15"  Part="1" 
AR Path="/3D8EA0004AC79AB8" Ref="C15"  Part="1" 
AR Path="/402B08B44AC79AB8" Ref="C15"  Part="1" 
AR Path="/4026A24D4AC79AB8" Ref="C15"  Part="1" 
AR Path="/3FE88B434AC79AB8" Ref="C15"  Part="1" 
AR Path="/3D7E00004AC79AB8" Ref="C15"  Part="1" 
AR Path="/402988B44AC79AB8" Ref="C15"  Part="1" 
AR Path="/3D6CC0004AC79AB8" Ref="C15"  Part="1" 
AR Path="/FFFFFFF04AC79AB8" Ref="C15"  Part="1" 
AR Path="/23C2344AC79AB8" Ref="C15"  Part="1" 
AR Path="/5AD746F64AC79AB8" Ref="C15"  Part="1" 
AR Path="/4027EF1A4AC79AB8" Ref="C15"  Part="1" 
AR Path="/402A6F1A4AC79AB8" Ref="C15"  Part="1" 
AR Path="/4029BBE74AC79AB8" Ref="C15"  Part="1" 
AR Path="/402A224D4AC79AB8" Ref="C15"  Part="1" 
AR Path="/402A55814AC79AB8" Ref="C15"  Part="1" 
AR Path="/4026224D4AC79AB8" Ref="C15"  Part="1" 
AR Path="/3D6220004AC79AB8" Ref="C15"  Part="1" 
AR Path="/3D9720004AC79AB8" Ref="C15"  Part="1" 
AR Path="/2600004AC79AB8" Ref="C15"  Part="1" 
AR Path="/402C3BE74AC79AB8" Ref="C15"  Part="1" 
AR Path="/402955814AC79AB8" Ref="C15"  Part="1" 
AR Path="/40293BE74AC79AB8" Ref="C15"  Part="1" 
AR Path="/402555814AC79AB8" Ref="C15"  Part="1" 
AR Path="/23D2034AC79AB8" Ref="C15"  Part="1" 
AR Path="/3FED58104AC79AB8" Ref="C15"  Part="1" 
AR Path="/402688B44AC79AB8" Ref="C15"  Part="1" 
AR Path="/3FE441894AC79AB8" Ref="C15"  Part="1" 
AR Path="/B84AC79AB8" Ref="C15"  Part="1" 
AR Path="/4029D5814AC79AB8" Ref="C15"  Part="1" 
AR Path="/40256F1A4AC79AB8" Ref="C15"  Part="1" 
AR Path="/40286F1A4AC79AB8" Ref="C15"  Part="1" 
AR Path="/402A08B44AC79AB8" Ref="C15"  Part="1" 
AR Path="/402588B44AC79AB8" Ref="C15"  Part="1" 
AR Path="/A84AC79AB8" Ref="C15"  Part="1" 
AR Path="/3FE224DD4AC79AB8" Ref="C15"  Part="1" 
AR Path="/402C08B44AC79AB8" Ref="C15"  Part="1" 
AR Path="/402BEF1A4AC79AB8" Ref="C15"  Part="1" 
AR Path="/402C55814AC79AB8" Ref="C15"  Part="1" 
AR Path="/40273BE74AC79AB8" Ref="C15"  Part="1" 
AR Path="/402908B44AC79AB8" Ref="C15"  Part="1" 
AR Path="/3D5A40004AC79AB8" Ref="C15"  Part="1" 
AR Path="/402755814AC79AB8" Ref="C15"  Part="1" 
AR Path="/4030AAC04AC79AB8" Ref="C15"  Part="1" 
AR Path="/4031DDF34AC79AB8" Ref="C15"  Part="1" 
AR Path="/4032778D4AC79AB8" Ref="C15"  Part="1" 
AR Path="/403091264AC79AB8" Ref="C15"  Part="1" 
AR Path="/403051264AC79AB8" Ref="C15"  Part="1" 
AR Path="/4032F78D4AC79AB8" Ref="C15"  Part="1" 
AR Path="/403251264AC79AB8" Ref="C15"  Part="1" 
AR Path="/4032AAC04AC79AB8" Ref="C15"  Part="1" 
AR Path="/4030D1264AC79AB8" Ref="C15"  Part="1" 
AR Path="/4031778D4AC79AB8" Ref="C15"  Part="1" 
AR Path="/3FEA24DD4AC79AB8" Ref="C15"  Part="1" 
AR Path="/6FE934E34AC79AB8" Ref="C15"  Part="1" 
AR Path="/773F65F14AC79AB8" Ref="C15"  Part="1" 
AR Path="/773F8EB44AC79AB8" Ref="C15"  Part="1" 
AR Path="/23C9F04AC79AB8" Ref="C15"  Part="1" 
AR Path="/24AC79AB8" Ref="C15"  Part="1" 
AR Path="/23BC884AC79AB8" Ref="C15"  Part="1" 
AR Path="/DC0C124AC79AB8" Ref="C15"  Part="1" 
AR Path="/23C34C4AC79AB8" Ref="C15"  Part="1" 
AR Path="/23CBC44AC79AB8" Ref="C15"  Part="1" 
AR Path="/23C6504AC79AB8" Ref="C15"  Part="1" 
AR Path="/39803EA4AC79AB8" Ref="C15"  Part="1" 
AR Path="/FFFFFFFF4AC79AB8" Ref="C15"  Part="1" 
AR Path="/6FE934344AC79AB8" Ref="C15"  Part="1" 
AR Path="/7E0073254AC79AB8" Ref="C15"  Part="1" 
AR Path="/73254AC79AB8" Ref="C15"  Part="1" 
AR Path="/8CEE4AC79AB8" Ref="C15"  Part="1" 
AR Path="/23D9004AC79AB8" Ref="C15"  Part="1" 
AR Path="/91964AC79AB8" Ref="C15"  Part="1" 
AR Path="/77F16B254AC79AB8" Ref="C15"  Part="1" 
AR Path="/23D83C4AC79AB8" Ref="C15"  Part="1" 
AR Path="/47907FA4AC79AB8" Ref="C15"  Part="1" 
AR Path="/8D384AC79AB8" Ref="C15"  Part="1" 
AR Path="/D058E04AC79AB8" Ref="C15"  Part="1" 
AR Path="/3C64AC79AB8" Ref="C15"  Part="1" 
AR Path="/2F4A4AC79AB8" Ref="C15"  Part="1" 
AR Path="/D1C3384AC79AB8" Ref="C15"  Part="1" 
F 0 "C15" H 36550 21150 50  0000 L CNN
F 1 "0.1 uF" H 36550 20950 50  0000 L CNN
F 2 "C2" H 36500 21050 60  0001 C CNN
F 3 "" H 36500 21050 60  0001 C CNN
	1    36500 21050
	1    0    0    -1  
$EndComp
$Comp
L C C11
U 1 1 4AC79AA4
P 39200 22250
AR Path="/4AC79AA4" Ref="C11"  Part="1" 
AR Path="/94AC79AA4" Ref="C11"  Part="1" 
AR Path="/14AC79AA4" Ref="C11"  Part="1" 
AR Path="/6FF0DD404AC79AA4" Ref="C11"  Part="1" 
AR Path="/755D912A4AC79AA4" Ref="C11"  Part="1" 
AR Path="/6FE901F74AC79AA4" Ref="C11"  Part="1" 
AR Path="/3FE08B434AC79AA4" Ref="C11"  Part="1" 
AR Path="/3DA360004AC79AA4" Ref="C11"  Part="1" 
AR Path="/5AD7153D4AC79AA4" Ref="C11"  Part="1" 
AR Path="/A4AC79AA4" Ref="C11"  Part="1" 
AR Path="/23D9304AC79AA4" Ref="C11"  Part="1" 
AR Path="/3FEFFFFF4AC79AA4" Ref="C11"  Part="1" 
AR Path="/23D8D44AC79AA4" Ref="C11"  Part="1" 
AR Path="/3D8EA0004AC79AA4" Ref="C11"  Part="1" 
AR Path="/402B08B44AC79AA4" Ref="C11"  Part="1" 
AR Path="/4026A24D4AC79AA4" Ref="C11"  Part="1" 
AR Path="/3FE88B434AC79AA4" Ref="C11"  Part="1" 
AR Path="/3D7E00004AC79AA4" Ref="C11"  Part="1" 
AR Path="/402988B44AC79AA4" Ref="C11"  Part="1" 
AR Path="/3D6CC0004AC79AA4" Ref="C11"  Part="1" 
AR Path="/FFFFFFF04AC79AA4" Ref="C11"  Part="1" 
AR Path="/23C2344AC79AA4" Ref="C11"  Part="1" 
AR Path="/DCBAABCD4AC79AA4" Ref="C11"  Part="1" 
AR Path="/5AD746F64AC79AA4" Ref="C11"  Part="1" 
AR Path="/4027EF1A4AC79AA4" Ref="C11"  Part="1" 
AR Path="/402A6F1A4AC79AA4" Ref="C11"  Part="1" 
AR Path="/4029BBE74AC79AA4" Ref="C11"  Part="1" 
AR Path="/402A224D4AC79AA4" Ref="C11"  Part="1" 
AR Path="/402A55814AC79AA4" Ref="C11"  Part="1" 
AR Path="/4026224D4AC79AA4" Ref="C11"  Part="1" 
AR Path="/3D6220004AC79AA4" Ref="C11"  Part="1" 
AR Path="/3D9720004AC79AA4" Ref="C11"  Part="1" 
AR Path="/2600004AC79AA4" Ref="C11"  Part="1" 
AR Path="/402C3BE74AC79AA4" Ref="C11"  Part="1" 
AR Path="/402955814AC79AA4" Ref="C11"  Part="1" 
AR Path="/40293BE74AC79AA4" Ref="C11"  Part="1" 
AR Path="/402555814AC79AA4" Ref="C11"  Part="1" 
AR Path="/23D2034AC79AA4" Ref="C11"  Part="1" 
AR Path="/3FED58104AC79AA4" Ref="C11"  Part="1" 
AR Path="/402688B44AC79AA4" Ref="C11"  Part="1" 
AR Path="/3FE441894AC79AA4" Ref="C11"  Part="1" 
AR Path="/B84AC79AA4" Ref="C11"  Part="1" 
AR Path="/4029D5814AC79AA4" Ref="C11"  Part="1" 
AR Path="/40256F1A4AC79AA4" Ref="C11"  Part="1" 
AR Path="/40286F1A4AC79AA4" Ref="C11"  Part="1" 
AR Path="/402A08B44AC79AA4" Ref="C11"  Part="1" 
AR Path="/402588B44AC79AA4" Ref="C11"  Part="1" 
AR Path="/A84AC79AA4" Ref="C11"  Part="1" 
AR Path="/3FE224DD4AC79AA4" Ref="C11"  Part="1" 
AR Path="/402C08B44AC79AA4" Ref="C11"  Part="1" 
AR Path="/402BEF1A4AC79AA4" Ref="C11"  Part="1" 
AR Path="/402C55814AC79AA4" Ref="C11"  Part="1" 
AR Path="/40273BE74AC79AA4" Ref="C11"  Part="1" 
AR Path="/402908B44AC79AA4" Ref="C11"  Part="1" 
AR Path="/3D5A40004AC79AA4" Ref="C11"  Part="1" 
AR Path="/402755814AC79AA4" Ref="C11"  Part="1" 
AR Path="/4030AAC04AC79AA4" Ref="C11"  Part="1" 
AR Path="/4031DDF34AC79AA4" Ref="C11"  Part="1" 
AR Path="/4032778D4AC79AA4" Ref="C11"  Part="1" 
AR Path="/403091264AC79AA4" Ref="C11"  Part="1" 
AR Path="/403051264AC79AA4" Ref="C11"  Part="1" 
AR Path="/4032F78D4AC79AA4" Ref="C11"  Part="1" 
AR Path="/403251264AC79AA4" Ref="C11"  Part="1" 
AR Path="/4032AAC04AC79AA4" Ref="C11"  Part="1" 
AR Path="/4030D1264AC79AA4" Ref="C11"  Part="1" 
AR Path="/4031778D4AC79AA4" Ref="C11"  Part="1" 
AR Path="/3FEA24DD4AC79AA4" Ref="C11"  Part="1" 
AR Path="/6FE934E34AC79AA4" Ref="C11"  Part="1" 
AR Path="/773F65F14AC79AA4" Ref="C11"  Part="1" 
AR Path="/773F8EB44AC79AA4" Ref="C11"  Part="1" 
AR Path="/23C9F04AC79AA4" Ref="C11"  Part="1" 
AR Path="/24AC79AA4" Ref="C11"  Part="1" 
AR Path="/23BC884AC79AA4" Ref="C11"  Part="1" 
AR Path="/DC0C124AC79AA4" Ref="C11"  Part="1" 
AR Path="/23C34C4AC79AA4" Ref="C11"  Part="1" 
AR Path="/23CBC44AC79AA4" Ref="C11"  Part="1" 
AR Path="/23C6504AC79AA4" Ref="C11"  Part="1" 
AR Path="/39803EA4AC79AA4" Ref="C11"  Part="1" 
AR Path="/FFFFFFFF4AC79AA4" Ref="C11"  Part="1" 
AR Path="/6FE934344AC79AA4" Ref="C11"  Part="1" 
AR Path="/7E0073254AC79AA4" Ref="C11"  Part="1" 
AR Path="/73254AC79AA4" Ref="C11"  Part="1" 
AR Path="/8CEE4AC79AA4" Ref="C11"  Part="1" 
AR Path="/23D9004AC79AA4" Ref="C11"  Part="1" 
AR Path="/91964AC79AA4" Ref="C11"  Part="1" 
AR Path="/77F16B254AC79AA4" Ref="C11"  Part="1" 
AR Path="/23D83C4AC79AA4" Ref="C11"  Part="1" 
AR Path="/47907FA4AC79AA4" Ref="C11"  Part="1" 
AR Path="/8D384AC79AA4" Ref="C11"  Part="1" 
AR Path="/D058E04AC79AA4" Ref="C11"  Part="1" 
AR Path="/3C64AC79AA4" Ref="C11"  Part="1" 
AR Path="/2F4A4AC79AA4" Ref="C11"  Part="1" 
AR Path="/D1C3384AC79AA4" Ref="C11"  Part="1" 
F 0 "C11" H 39250 22350 50  0000 L CNN
F 1 "0.1 uF" H 39250 22150 50  0000 L CNN
F 2 "C2" H 39200 22250 60  0001 C CNN
F 3 "" H 39200 22250 60  0001 C CNN
	1    39200 22250
	1    0    0    -1  
$EndComp
$Comp
L C C12
U 1 1 4AC79AA3
P 39650 22250
AR Path="/4AC79AA3" Ref="C12"  Part="1" 
AR Path="/94AC79AA3" Ref="C12"  Part="1" 
AR Path="/14AC79AA3" Ref="C12"  Part="1" 
AR Path="/6FF0DD404AC79AA3" Ref="C12"  Part="1" 
AR Path="/755D912A4AC79AA3" Ref="C12"  Part="1" 
AR Path="/6FE901F74AC79AA3" Ref="C12"  Part="1" 
AR Path="/3FE08B434AC79AA3" Ref="C12"  Part="1" 
AR Path="/3DA360004AC79AA3" Ref="C12"  Part="1" 
AR Path="/5AD7153D4AC79AA3" Ref="C12"  Part="1" 
AR Path="/A4AC79AA3" Ref="C12"  Part="1" 
AR Path="/23D9304AC79AA3" Ref="C12"  Part="1" 
AR Path="/3FEFFFFF4AC79AA3" Ref="C12"  Part="1" 
AR Path="/23D8D44AC79AA3" Ref="C12"  Part="1" 
AR Path="/3D8EA0004AC79AA3" Ref="C12"  Part="1" 
AR Path="/402B08B44AC79AA3" Ref="C12"  Part="1" 
AR Path="/4026A24D4AC79AA3" Ref="C12"  Part="1" 
AR Path="/3FE88B434AC79AA3" Ref="C12"  Part="1" 
AR Path="/3D7E00004AC79AA3" Ref="C12"  Part="1" 
AR Path="/402988B44AC79AA3" Ref="C12"  Part="1" 
AR Path="/3D6CC0004AC79AA3" Ref="C12"  Part="1" 
AR Path="/FFFFFFF04AC79AA3" Ref="C12"  Part="1" 
AR Path="/23C2344AC79AA3" Ref="C12"  Part="1" 
AR Path="/DCBAABCD4AC79AA3" Ref="C12"  Part="1" 
AR Path="/5AD746F64AC79AA3" Ref="C12"  Part="1" 
AR Path="/4027EF1A4AC79AA3" Ref="C12"  Part="1" 
AR Path="/402A6F1A4AC79AA3" Ref="C12"  Part="1" 
AR Path="/4029BBE74AC79AA3" Ref="C12"  Part="1" 
AR Path="/402A224D4AC79AA3" Ref="C12"  Part="1" 
AR Path="/402A55814AC79AA3" Ref="C12"  Part="1" 
AR Path="/4026224D4AC79AA3" Ref="C12"  Part="1" 
AR Path="/3D6220004AC79AA3" Ref="C12"  Part="1" 
AR Path="/3D9720004AC79AA3" Ref="C12"  Part="1" 
AR Path="/2600004AC79AA3" Ref="C12"  Part="1" 
AR Path="/402C3BE74AC79AA3" Ref="C12"  Part="1" 
AR Path="/402955814AC79AA3" Ref="C12"  Part="1" 
AR Path="/40293BE74AC79AA3" Ref="C12"  Part="1" 
AR Path="/402555814AC79AA3" Ref="C12"  Part="1" 
AR Path="/23D2034AC79AA3" Ref="C12"  Part="1" 
AR Path="/3FED58104AC79AA3" Ref="C12"  Part="1" 
AR Path="/402688B44AC79AA3" Ref="C12"  Part="1" 
AR Path="/3FE441894AC79AA3" Ref="C12"  Part="1" 
AR Path="/B84AC79AA3" Ref="C12"  Part="1" 
AR Path="/4029D5814AC79AA3" Ref="C12"  Part="1" 
AR Path="/40256F1A4AC79AA3" Ref="C12"  Part="1" 
AR Path="/40286F1A4AC79AA3" Ref="C12"  Part="1" 
AR Path="/402A08B44AC79AA3" Ref="C12"  Part="1" 
AR Path="/402588B44AC79AA3" Ref="C12"  Part="1" 
AR Path="/A84AC79AA3" Ref="C12"  Part="1" 
AR Path="/3FE224DD4AC79AA3" Ref="C12"  Part="1" 
AR Path="/402C08B44AC79AA3" Ref="C12"  Part="1" 
AR Path="/402BEF1A4AC79AA3" Ref="C12"  Part="1" 
AR Path="/402C55814AC79AA3" Ref="C12"  Part="1" 
AR Path="/40273BE74AC79AA3" Ref="C12"  Part="1" 
AR Path="/402908B44AC79AA3" Ref="C12"  Part="1" 
AR Path="/3D5A40004AC79AA3" Ref="C12"  Part="1" 
AR Path="/402755814AC79AA3" Ref="C12"  Part="1" 
AR Path="/4030AAC04AC79AA3" Ref="C12"  Part="1" 
AR Path="/4031DDF34AC79AA3" Ref="C12"  Part="1" 
AR Path="/4032778D4AC79AA3" Ref="C12"  Part="1" 
AR Path="/403091264AC79AA3" Ref="C12"  Part="1" 
AR Path="/403051264AC79AA3" Ref="C12"  Part="1" 
AR Path="/4032F78D4AC79AA3" Ref="C12"  Part="1" 
AR Path="/403251264AC79AA3" Ref="C12"  Part="1" 
AR Path="/4032AAC04AC79AA3" Ref="C12"  Part="1" 
AR Path="/4030D1264AC79AA3" Ref="C12"  Part="1" 
AR Path="/4031778D4AC79AA3" Ref="C12"  Part="1" 
AR Path="/3FEA24DD4AC79AA3" Ref="C12"  Part="1" 
AR Path="/6FE934E34AC79AA3" Ref="C12"  Part="1" 
AR Path="/773F65F14AC79AA3" Ref="C12"  Part="1" 
AR Path="/773F8EB44AC79AA3" Ref="C12"  Part="1" 
AR Path="/23C9F04AC79AA3" Ref="C12"  Part="1" 
AR Path="/24AC79AA3" Ref="C12"  Part="1" 
AR Path="/23BC884AC79AA3" Ref="C12"  Part="1" 
AR Path="/DC0C124AC79AA3" Ref="C12"  Part="1" 
AR Path="/23C34C4AC79AA3" Ref="C12"  Part="1" 
AR Path="/23CBC44AC79AA3" Ref="C12"  Part="1" 
AR Path="/23C6504AC79AA3" Ref="C12"  Part="1" 
AR Path="/39803EA4AC79AA3" Ref="C12"  Part="1" 
AR Path="/FFFFFFFF4AC79AA3" Ref="C12"  Part="1" 
AR Path="/6FE934344AC79AA3" Ref="C12"  Part="1" 
AR Path="/7E0073254AC79AA3" Ref="C12"  Part="1" 
AR Path="/73254AC79AA3" Ref="C12"  Part="1" 
AR Path="/8CEE4AC79AA3" Ref="C12"  Part="1" 
AR Path="/23D9004AC79AA3" Ref="C12"  Part="1" 
AR Path="/91964AC79AA3" Ref="C12"  Part="1" 
AR Path="/77F16B254AC79AA3" Ref="C12"  Part="1" 
AR Path="/23D83C4AC79AA3" Ref="C12"  Part="1" 
AR Path="/47907FA4AC79AA3" Ref="C12"  Part="1" 
AR Path="/8D384AC79AA3" Ref="C12"  Part="1" 
AR Path="/D058E04AC79AA3" Ref="C12"  Part="1" 
AR Path="/3C64AC79AA3" Ref="C12"  Part="1" 
AR Path="/2F4A4AC79AA3" Ref="C12"  Part="1" 
AR Path="/D1C3384AC79AA3" Ref="C12"  Part="1" 
F 0 "C12" H 39700 22350 50  0000 L CNN
F 1 "0.1 uF" H 39700 22150 50  0000 L CNN
F 2 "C2" H 39650 22250 60  0001 C CNN
F 3 "" H 39650 22250 60  0001 C CNN
	1    39650 22250
	1    0    0    -1  
$EndComp
$Comp
L C C14
U 1 1 4AC79AA2
P 38300 21650
AR Path="/4AC79AA2" Ref="C14"  Part="1" 
AR Path="/94AC79AA2" Ref="C14"  Part="1" 
AR Path="/14AC79AA2" Ref="C14"  Part="1" 
AR Path="/6FF0DD404AC79AA2" Ref="C14"  Part="1" 
AR Path="/755D912A4AC79AA2" Ref="C14"  Part="1" 
AR Path="/6FE901F74AC79AA2" Ref="C14"  Part="1" 
AR Path="/3FE08B434AC79AA2" Ref="C14"  Part="1" 
AR Path="/3DA360004AC79AA2" Ref="C14"  Part="1" 
AR Path="/5AD7153D4AC79AA2" Ref="C14"  Part="1" 
AR Path="/A4AC79AA2" Ref="C14"  Part="1" 
AR Path="/23D9304AC79AA2" Ref="C14"  Part="1" 
AR Path="/3FEFFFFF4AC79AA2" Ref="C14"  Part="1" 
AR Path="/23D8D44AC79AA2" Ref="C14"  Part="1" 
AR Path="/3D8EA0004AC79AA2" Ref="C14"  Part="1" 
AR Path="/402B08B44AC79AA2" Ref="C14"  Part="1" 
AR Path="/4026A24D4AC79AA2" Ref="C14"  Part="1" 
AR Path="/3FE88B434AC79AA2" Ref="C14"  Part="1" 
AR Path="/3D7E00004AC79AA2" Ref="C14"  Part="1" 
AR Path="/402988B44AC79AA2" Ref="C14"  Part="1" 
AR Path="/3D6CC0004AC79AA2" Ref="C14"  Part="1" 
AR Path="/FFFFFFF04AC79AA2" Ref="C14"  Part="1" 
AR Path="/23C2344AC79AA2" Ref="C14"  Part="1" 
AR Path="/DCBAABCD4AC79AA2" Ref="C14"  Part="1" 
AR Path="/5AD746F64AC79AA2" Ref="C14"  Part="1" 
AR Path="/4027EF1A4AC79AA2" Ref="C14"  Part="1" 
AR Path="/402A6F1A4AC79AA2" Ref="C14"  Part="1" 
AR Path="/4029BBE74AC79AA2" Ref="C14"  Part="1" 
AR Path="/402A224D4AC79AA2" Ref="C14"  Part="1" 
AR Path="/402A55814AC79AA2" Ref="C14"  Part="1" 
AR Path="/4026224D4AC79AA2" Ref="C14"  Part="1" 
AR Path="/3D6220004AC79AA2" Ref="C14"  Part="1" 
AR Path="/3D9720004AC79AA2" Ref="C14"  Part="1" 
AR Path="/2600004AC79AA2" Ref="C14"  Part="1" 
AR Path="/402C3BE74AC79AA2" Ref="C14"  Part="1" 
AR Path="/402955814AC79AA2" Ref="C14"  Part="1" 
AR Path="/40293BE74AC79AA2" Ref="C14"  Part="1" 
AR Path="/402555814AC79AA2" Ref="C14"  Part="1" 
AR Path="/23D2034AC79AA2" Ref="C14"  Part="1" 
AR Path="/3FED58104AC79AA2" Ref="C14"  Part="1" 
AR Path="/402688B44AC79AA2" Ref="C14"  Part="1" 
AR Path="/3FE441894AC79AA2" Ref="C14"  Part="1" 
AR Path="/B84AC79AA2" Ref="C14"  Part="1" 
AR Path="/4029D5814AC79AA2" Ref="C14"  Part="1" 
AR Path="/40256F1A4AC79AA2" Ref="C14"  Part="1" 
AR Path="/40286F1A4AC79AA2" Ref="C14"  Part="1" 
AR Path="/402A08B44AC79AA2" Ref="C14"  Part="1" 
AR Path="/402588B44AC79AA2" Ref="C14"  Part="1" 
AR Path="/A84AC79AA2" Ref="C14"  Part="1" 
AR Path="/3FE224DD4AC79AA2" Ref="C14"  Part="1" 
AR Path="/402C08B44AC79AA2" Ref="C14"  Part="1" 
AR Path="/402BEF1A4AC79AA2" Ref="C14"  Part="1" 
AR Path="/402C55814AC79AA2" Ref="C14"  Part="1" 
AR Path="/40273BE74AC79AA2" Ref="C14"  Part="1" 
AR Path="/402908B44AC79AA2" Ref="C14"  Part="1" 
AR Path="/3D5A40004AC79AA2" Ref="C14"  Part="1" 
AR Path="/402755814AC79AA2" Ref="C14"  Part="1" 
AR Path="/4030AAC04AC79AA2" Ref="C14"  Part="1" 
AR Path="/4031DDF34AC79AA2" Ref="C14"  Part="1" 
AR Path="/4032778D4AC79AA2" Ref="C14"  Part="1" 
AR Path="/403091264AC79AA2" Ref="C14"  Part="1" 
AR Path="/403051264AC79AA2" Ref="C14"  Part="1" 
AR Path="/4032F78D4AC79AA2" Ref="C14"  Part="1" 
AR Path="/403251264AC79AA2" Ref="C14"  Part="1" 
AR Path="/4032AAC04AC79AA2" Ref="C14"  Part="1" 
AR Path="/4030D1264AC79AA2" Ref="C14"  Part="1" 
AR Path="/4031778D4AC79AA2" Ref="C14"  Part="1" 
AR Path="/3FEA24DD4AC79AA2" Ref="C14"  Part="1" 
AR Path="/6FE934E34AC79AA2" Ref="C14"  Part="1" 
AR Path="/773F65F14AC79AA2" Ref="C14"  Part="1" 
AR Path="/773F8EB44AC79AA2" Ref="C14"  Part="1" 
AR Path="/23C9F04AC79AA2" Ref="C14"  Part="1" 
AR Path="/24AC79AA2" Ref="C14"  Part="1" 
AR Path="/23BC884AC79AA2" Ref="C14"  Part="1" 
AR Path="/DC0C124AC79AA2" Ref="C14"  Part="1" 
AR Path="/23C34C4AC79AA2" Ref="C14"  Part="1" 
AR Path="/23CBC44AC79AA2" Ref="C14"  Part="1" 
AR Path="/23C6504AC79AA2" Ref="C14"  Part="1" 
AR Path="/39803EA4AC79AA2" Ref="C14"  Part="1" 
AR Path="/FFFFFFFF4AC79AA2" Ref="C14"  Part="1" 
AR Path="/6FE934344AC79AA2" Ref="C14"  Part="1" 
AR Path="/7E0073254AC79AA2" Ref="C14"  Part="1" 
AR Path="/73254AC79AA2" Ref="C14"  Part="1" 
AR Path="/8CEE4AC79AA2" Ref="C14"  Part="1" 
AR Path="/23D9004AC79AA2" Ref="C14"  Part="1" 
AR Path="/91964AC79AA2" Ref="C14"  Part="1" 
AR Path="/77F16B254AC79AA2" Ref="C14"  Part="1" 
AR Path="/23D83C4AC79AA2" Ref="C14"  Part="1" 
AR Path="/47907FA4AC79AA2" Ref="C14"  Part="1" 
AR Path="/8D384AC79AA2" Ref="C14"  Part="1" 
AR Path="/D058E04AC79AA2" Ref="C14"  Part="1" 
AR Path="/3C64AC79AA2" Ref="C14"  Part="1" 
AR Path="/2F4A4AC79AA2" Ref="C14"  Part="1" 
AR Path="/D1C3384AC79AA2" Ref="C14"  Part="1" 
F 0 "C14" H 38350 21750 50  0000 L CNN
F 1 "0.1 uF" H 38350 21550 50  0000 L CNN
F 2 "C2" H 38300 21650 60  0001 C CNN
F 3 "" H 38300 21650 60  0001 C CNN
	1    38300 21650
	1    0    0    -1  
$EndComp
$Comp
L C C13
U 1 1 4AC79AA1
P 40100 22250
AR Path="/4AC79AA1" Ref="C13"  Part="1" 
AR Path="/94AC79AA1" Ref="C13"  Part="1" 
AR Path="/14AC79AA1" Ref="C13"  Part="1" 
AR Path="/6FF0DD404AC79AA1" Ref="C13"  Part="1" 
AR Path="/755D912A4AC79AA1" Ref="C13"  Part="1" 
AR Path="/6FE901F74AC79AA1" Ref="C13"  Part="1" 
AR Path="/3FE08B434AC79AA1" Ref="C13"  Part="1" 
AR Path="/3DA360004AC79AA1" Ref="C13"  Part="1" 
AR Path="/5AD7153D4AC79AA1" Ref="C13"  Part="1" 
AR Path="/A4AC79AA1" Ref="C13"  Part="1" 
AR Path="/23D9304AC79AA1" Ref="C13"  Part="1" 
AR Path="/3FEFFFFF4AC79AA1" Ref="C13"  Part="1" 
AR Path="/23D8D44AC79AA1" Ref="C13"  Part="1" 
AR Path="/3D8EA0004AC79AA1" Ref="C13"  Part="1" 
AR Path="/402B08B44AC79AA1" Ref="C13"  Part="1" 
AR Path="/4026A24D4AC79AA1" Ref="C13"  Part="1" 
AR Path="/3FE88B434AC79AA1" Ref="C13"  Part="1" 
AR Path="/3D7E00004AC79AA1" Ref="C13"  Part="1" 
AR Path="/402988B44AC79AA1" Ref="C13"  Part="1" 
AR Path="/3D6CC0004AC79AA1" Ref="C13"  Part="1" 
AR Path="/FFFFFFF04AC79AA1" Ref="C13"  Part="1" 
AR Path="/23C2344AC79AA1" Ref="C13"  Part="1" 
AR Path="/DCBAABCD4AC79AA1" Ref="C13"  Part="1" 
AR Path="/5AD746F64AC79AA1" Ref="C13"  Part="1" 
AR Path="/4027EF1A4AC79AA1" Ref="C13"  Part="1" 
AR Path="/402A6F1A4AC79AA1" Ref="C13"  Part="1" 
AR Path="/4029BBE74AC79AA1" Ref="C13"  Part="1" 
AR Path="/402A224D4AC79AA1" Ref="C13"  Part="1" 
AR Path="/402A55814AC79AA1" Ref="C13"  Part="1" 
AR Path="/4026224D4AC79AA1" Ref="C13"  Part="1" 
AR Path="/3D6220004AC79AA1" Ref="C13"  Part="1" 
AR Path="/3D9720004AC79AA1" Ref="C13"  Part="1" 
AR Path="/2600004AC79AA1" Ref="C13"  Part="1" 
AR Path="/402C3BE74AC79AA1" Ref="C13"  Part="1" 
AR Path="/402955814AC79AA1" Ref="C13"  Part="1" 
AR Path="/40293BE74AC79AA1" Ref="C13"  Part="1" 
AR Path="/402555814AC79AA1" Ref="C13"  Part="1" 
AR Path="/23D2034AC79AA1" Ref="C13"  Part="1" 
AR Path="/3FED58104AC79AA1" Ref="C13"  Part="1" 
AR Path="/402688B44AC79AA1" Ref="C13"  Part="1" 
AR Path="/3FE441894AC79AA1" Ref="C13"  Part="1" 
AR Path="/B84AC79AA1" Ref="C13"  Part="1" 
AR Path="/4029D5814AC79AA1" Ref="C13"  Part="1" 
AR Path="/40256F1A4AC79AA1" Ref="C13"  Part="1" 
AR Path="/40286F1A4AC79AA1" Ref="C13"  Part="1" 
AR Path="/402A08B44AC79AA1" Ref="C13"  Part="1" 
AR Path="/402588B44AC79AA1" Ref="C13"  Part="1" 
AR Path="/A84AC79AA1" Ref="C13"  Part="1" 
AR Path="/3FE224DD4AC79AA1" Ref="C13"  Part="1" 
AR Path="/402C08B44AC79AA1" Ref="C13"  Part="1" 
AR Path="/402BEF1A4AC79AA1" Ref="C13"  Part="1" 
AR Path="/402C55814AC79AA1" Ref="C13"  Part="1" 
AR Path="/40273BE74AC79AA1" Ref="C13"  Part="1" 
AR Path="/402908B44AC79AA1" Ref="C13"  Part="1" 
AR Path="/3D5A40004AC79AA1" Ref="C13"  Part="1" 
AR Path="/402755814AC79AA1" Ref="C13"  Part="1" 
AR Path="/4030AAC04AC79AA1" Ref="C13"  Part="1" 
AR Path="/4031DDF34AC79AA1" Ref="C13"  Part="1" 
AR Path="/4032778D4AC79AA1" Ref="C13"  Part="1" 
AR Path="/403091264AC79AA1" Ref="C13"  Part="1" 
AR Path="/403051264AC79AA1" Ref="C13"  Part="1" 
AR Path="/4032F78D4AC79AA1" Ref="C13"  Part="1" 
AR Path="/403251264AC79AA1" Ref="C13"  Part="1" 
AR Path="/4032AAC04AC79AA1" Ref="C13"  Part="1" 
AR Path="/4030D1264AC79AA1" Ref="C13"  Part="1" 
AR Path="/4031778D4AC79AA1" Ref="C13"  Part="1" 
AR Path="/3FEA24DD4AC79AA1" Ref="C13"  Part="1" 
AR Path="/6FE934E34AC79AA1" Ref="C13"  Part="1" 
AR Path="/773F65F14AC79AA1" Ref="C13"  Part="1" 
AR Path="/773F8EB44AC79AA1" Ref="C13"  Part="1" 
AR Path="/23C9F04AC79AA1" Ref="C13"  Part="1" 
AR Path="/24AC79AA1" Ref="C13"  Part="1" 
AR Path="/23BC884AC79AA1" Ref="C13"  Part="1" 
AR Path="/DC0C124AC79AA1" Ref="C13"  Part="1" 
AR Path="/23C34C4AC79AA1" Ref="C13"  Part="1" 
AR Path="/23CBC44AC79AA1" Ref="C13"  Part="1" 
AR Path="/23C6504AC79AA1" Ref="C13"  Part="1" 
AR Path="/39803EA4AC79AA1" Ref="C13"  Part="1" 
AR Path="/FFFFFFFF4AC79AA1" Ref="C13"  Part="1" 
AR Path="/6FE934344AC79AA1" Ref="C13"  Part="1" 
AR Path="/7E0073254AC79AA1" Ref="C13"  Part="1" 
AR Path="/73254AC79AA1" Ref="C13"  Part="1" 
AR Path="/8CEE4AC79AA1" Ref="C13"  Part="1" 
AR Path="/23D9004AC79AA1" Ref="C13"  Part="1" 
AR Path="/91964AC79AA1" Ref="C13"  Part="1" 
AR Path="/77F16B254AC79AA1" Ref="C13"  Part="1" 
AR Path="/23D83C4AC79AA1" Ref="C13"  Part="1" 
AR Path="/47907FA4AC79AA1" Ref="C13"  Part="1" 
AR Path="/8D384AC79AA1" Ref="C13"  Part="1" 
AR Path="/D058E04AC79AA1" Ref="C13"  Part="1" 
AR Path="/3C64AC79AA1" Ref="C13"  Part="1" 
AR Path="/2F4A4AC79AA1" Ref="C13"  Part="1" 
AR Path="/D1C3384AC79AA1" Ref="C13"  Part="1" 
F 0 "C13" H 40150 22350 50  0000 L CNN
F 1 "0.1 uF" H 40150 22150 50  0000 L CNN
F 2 "C2" H 40100 22250 60  0001 C CNN
F 3 "" H 40100 22250 60  0001 C CNN
	1    40100 22250
	1    0    0    -1  
$EndComp
$Comp
L C C9
U 1 1 4AC79A7D
P 38300 22250
AR Path="/4AC79A7D" Ref="C9"  Part="1" 
AR Path="/94AC79A7D" Ref="C9"  Part="1" 
AR Path="/14AC79A7D" Ref="C9"  Part="1" 
AR Path="/6FF0DD404AC79A7D" Ref="C9"  Part="1" 
AR Path="/755D912A4AC79A7D" Ref="C9"  Part="1" 
AR Path="/6FE901F74AC79A7D" Ref="C9"  Part="1" 
AR Path="/3FE08B434AC79A7D" Ref="C9"  Part="1" 
AR Path="/3DA360004AC79A7D" Ref="C9"  Part="1" 
AR Path="/5AD7153D4AC79A7D" Ref="C9"  Part="1" 
AR Path="/A4AC79A7D" Ref="C9"  Part="1" 
AR Path="/23D9304AC79A7D" Ref="C9"  Part="1" 
AR Path="/3FEFFFFF4AC79A7D" Ref="C9"  Part="1" 
AR Path="/23D8D44AC79A7D" Ref="C9"  Part="1" 
AR Path="/3D8EA0004AC79A7D" Ref="C9"  Part="1" 
AR Path="/402B08B44AC79A7D" Ref="C9"  Part="1" 
AR Path="/4026A24D4AC79A7D" Ref="C9"  Part="1" 
AR Path="/3FE88B434AC79A7D" Ref="C9"  Part="1" 
AR Path="/3D7E00004AC79A7D" Ref="C9"  Part="1" 
AR Path="/402988B44AC79A7D" Ref="C9"  Part="1" 
AR Path="/3D6CC0004AC79A7D" Ref="C9"  Part="1" 
AR Path="/FFFFFFF04AC79A7D" Ref="C9"  Part="1" 
AR Path="/23C2344AC79A7D" Ref="C9"  Part="1" 
AR Path="/DCBAABCD4AC79A7D" Ref="C9"  Part="1" 
AR Path="/5AD746F64AC79A7D" Ref="C9"  Part="1" 
AR Path="/4027EF1A4AC79A7D" Ref="C9"  Part="1" 
AR Path="/402A6F1A4AC79A7D" Ref="C9"  Part="1" 
AR Path="/4029BBE74AC79A7D" Ref="C9"  Part="1" 
AR Path="/402A224D4AC79A7D" Ref="C9"  Part="1" 
AR Path="/402A55814AC79A7D" Ref="C9"  Part="1" 
AR Path="/4026224D4AC79A7D" Ref="C9"  Part="1" 
AR Path="/3D6220004AC79A7D" Ref="C9"  Part="1" 
AR Path="/3D9720004AC79A7D" Ref="C9"  Part="1" 
AR Path="/2600004AC79A7D" Ref="C9"  Part="1" 
AR Path="/402C3BE74AC79A7D" Ref="C9"  Part="1" 
AR Path="/402955814AC79A7D" Ref="C9"  Part="1" 
AR Path="/40293BE74AC79A7D" Ref="C9"  Part="1" 
AR Path="/402555814AC79A7D" Ref="C9"  Part="1" 
AR Path="/23D2034AC79A7D" Ref="C9"  Part="1" 
AR Path="/3FED58104AC79A7D" Ref="C9"  Part="1" 
AR Path="/402688B44AC79A7D" Ref="C9"  Part="1" 
AR Path="/3FE441894AC79A7D" Ref="C9"  Part="1" 
AR Path="/B84AC79A7D" Ref="C9"  Part="1" 
AR Path="/4029D5814AC79A7D" Ref="C9"  Part="1" 
AR Path="/40256F1A4AC79A7D" Ref="C9"  Part="1" 
AR Path="/40286F1A4AC79A7D" Ref="C9"  Part="1" 
AR Path="/402A08B44AC79A7D" Ref="C9"  Part="1" 
AR Path="/402588B44AC79A7D" Ref="C9"  Part="1" 
AR Path="/A84AC79A7D" Ref="C9"  Part="1" 
AR Path="/3FE224DD4AC79A7D" Ref="C9"  Part="1" 
AR Path="/402C08B44AC79A7D" Ref="C9"  Part="1" 
AR Path="/402BEF1A4AC79A7D" Ref="C9"  Part="1" 
AR Path="/402C55814AC79A7D" Ref="C9"  Part="1" 
AR Path="/40273BE74AC79A7D" Ref="C9"  Part="1" 
AR Path="/402908B44AC79A7D" Ref="C9"  Part="1" 
AR Path="/3D5A40004AC79A7D" Ref="C9"  Part="1" 
AR Path="/402755814AC79A7D" Ref="C9"  Part="1" 
AR Path="/4030AAC04AC79A7D" Ref="C9"  Part="1" 
AR Path="/4031DDF34AC79A7D" Ref="C9"  Part="1" 
AR Path="/4032778D4AC79A7D" Ref="C9"  Part="1" 
AR Path="/403091264AC79A7D" Ref="C9"  Part="1" 
AR Path="/403051264AC79A7D" Ref="C9"  Part="1" 
AR Path="/4032F78D4AC79A7D" Ref="C9"  Part="1" 
AR Path="/403251264AC79A7D" Ref="C9"  Part="1" 
AR Path="/4032AAC04AC79A7D" Ref="C9"  Part="1" 
AR Path="/4030D1264AC79A7D" Ref="C9"  Part="1" 
AR Path="/4031778D4AC79A7D" Ref="C9"  Part="1" 
AR Path="/3FEA24DD4AC79A7D" Ref="C9"  Part="1" 
AR Path="/6FE934E34AC79A7D" Ref="C9"  Part="1" 
AR Path="/773F65F14AC79A7D" Ref="C9"  Part="1" 
AR Path="/773F8EB44AC79A7D" Ref="C9"  Part="1" 
AR Path="/23C9F04AC79A7D" Ref="C9"  Part="1" 
AR Path="/24AC79A7D" Ref="C9"  Part="1" 
AR Path="/23BC884AC79A7D" Ref="C9"  Part="1" 
AR Path="/DC0C124AC79A7D" Ref="C9"  Part="1" 
AR Path="/23C34C4AC79A7D" Ref="C9"  Part="1" 
AR Path="/23CBC44AC79A7D" Ref="C9"  Part="1" 
AR Path="/23C6504AC79A7D" Ref="C9"  Part="1" 
AR Path="/39803EA4AC79A7D" Ref="C9"  Part="1" 
AR Path="/FFFFFFFF4AC79A7D" Ref="C9"  Part="1" 
AR Path="/6FE934344AC79A7D" Ref="C9"  Part="1" 
AR Path="/7E0073254AC79A7D" Ref="C9"  Part="1" 
AR Path="/73254AC79A7D" Ref="C9"  Part="1" 
AR Path="/8CEE4AC79A7D" Ref="C9"  Part="1" 
AR Path="/23D9004AC79A7D" Ref="C9"  Part="1" 
AR Path="/91964AC79A7D" Ref="C9"  Part="1" 
AR Path="/77F16B254AC79A7D" Ref="C9"  Part="1" 
AR Path="/23D83C4AC79A7D" Ref="C9"  Part="1" 
AR Path="/47907FA4AC79A7D" Ref="C9"  Part="1" 
AR Path="/8D384AC79A7D" Ref="C9"  Part="1" 
AR Path="/D058E04AC79A7D" Ref="C9"  Part="1" 
AR Path="/3C64AC79A7D" Ref="C9"  Part="1" 
AR Path="/2F4A4AC79A7D" Ref="C9"  Part="1" 
AR Path="/D1C3384AC79A7D" Ref="C9"  Part="1" 
F 0 "C9" H 38350 22350 50  0000 L CNN
F 1 "0.1 uF" H 38350 22150 50  0000 L CNN
F 2 "C2" H 38300 22250 60  0001 C CNN
F 3 "" H 38300 22250 60  0001 C CNN
	1    38300 22250
	1    0    0    -1  
$EndComp
$Comp
L C C10
U 1 1 4AC79A7C
P 38750 22250
AR Path="/4AC79A7C" Ref="C10"  Part="1" 
AR Path="/94AC79A7C" Ref="C10"  Part="1" 
AR Path="/14AC79A7C" Ref="C10"  Part="1" 
AR Path="/6FF0DD404AC79A7C" Ref="C10"  Part="1" 
AR Path="/755D912A4AC79A7C" Ref="C10"  Part="1" 
AR Path="/6FE901F74AC79A7C" Ref="C10"  Part="1" 
AR Path="/3FE08B434AC79A7C" Ref="C10"  Part="1" 
AR Path="/3DA360004AC79A7C" Ref="C10"  Part="1" 
AR Path="/5AD7153D4AC79A7C" Ref="C10"  Part="1" 
AR Path="/A4AC79A7C" Ref="C10"  Part="1" 
AR Path="/23D9304AC79A7C" Ref="C10"  Part="1" 
AR Path="/3FEFFFFF4AC79A7C" Ref="C10"  Part="1" 
AR Path="/23D8D44AC79A7C" Ref="C10"  Part="1" 
AR Path="/3D8EA0004AC79A7C" Ref="C10"  Part="1" 
AR Path="/402B08B44AC79A7C" Ref="C10"  Part="1" 
AR Path="/4026A24D4AC79A7C" Ref="C10"  Part="1" 
AR Path="/3FE88B434AC79A7C" Ref="C10"  Part="1" 
AR Path="/3D7E00004AC79A7C" Ref="C10"  Part="1" 
AR Path="/402988B44AC79A7C" Ref="C10"  Part="1" 
AR Path="/3D6CC0004AC79A7C" Ref="C10"  Part="1" 
AR Path="/FFFFFFF04AC79A7C" Ref="C10"  Part="1" 
AR Path="/23C2344AC79A7C" Ref="C10"  Part="1" 
AR Path="/DCBAABCD4AC79A7C" Ref="C10"  Part="1" 
AR Path="/5AD746F64AC79A7C" Ref="C10"  Part="1" 
AR Path="/4027EF1A4AC79A7C" Ref="C10"  Part="1" 
AR Path="/402A6F1A4AC79A7C" Ref="C10"  Part="1" 
AR Path="/4029BBE74AC79A7C" Ref="C10"  Part="1" 
AR Path="/402A224D4AC79A7C" Ref="C10"  Part="1" 
AR Path="/402A55814AC79A7C" Ref="C10"  Part="1" 
AR Path="/4026224D4AC79A7C" Ref="C10"  Part="1" 
AR Path="/3D6220004AC79A7C" Ref="C10"  Part="1" 
AR Path="/3D9720004AC79A7C" Ref="C10"  Part="1" 
AR Path="/2600004AC79A7C" Ref="C10"  Part="1" 
AR Path="/402C3BE74AC79A7C" Ref="C10"  Part="1" 
AR Path="/402955814AC79A7C" Ref="C10"  Part="1" 
AR Path="/40293BE74AC79A7C" Ref="C10"  Part="1" 
AR Path="/402555814AC79A7C" Ref="C10"  Part="1" 
AR Path="/23D2034AC79A7C" Ref="C10"  Part="1" 
AR Path="/3FED58104AC79A7C" Ref="C10"  Part="1" 
AR Path="/402688B44AC79A7C" Ref="C10"  Part="1" 
AR Path="/3FE441894AC79A7C" Ref="C10"  Part="1" 
AR Path="/B84AC79A7C" Ref="C10"  Part="1" 
AR Path="/4029D5814AC79A7C" Ref="C10"  Part="1" 
AR Path="/40256F1A4AC79A7C" Ref="C10"  Part="1" 
AR Path="/40286F1A4AC79A7C" Ref="C10"  Part="1" 
AR Path="/402A08B44AC79A7C" Ref="C10"  Part="1" 
AR Path="/402588B44AC79A7C" Ref="C10"  Part="1" 
AR Path="/A84AC79A7C" Ref="C10"  Part="1" 
AR Path="/3FE224DD4AC79A7C" Ref="C10"  Part="1" 
AR Path="/402C08B44AC79A7C" Ref="C10"  Part="1" 
AR Path="/402BEF1A4AC79A7C" Ref="C10"  Part="1" 
AR Path="/402C55814AC79A7C" Ref="C10"  Part="1" 
AR Path="/40273BE74AC79A7C" Ref="C10"  Part="1" 
AR Path="/402908B44AC79A7C" Ref="C10"  Part="1" 
AR Path="/3D5A40004AC79A7C" Ref="C10"  Part="1" 
AR Path="/402755814AC79A7C" Ref="C10"  Part="1" 
AR Path="/4030AAC04AC79A7C" Ref="C10"  Part="1" 
AR Path="/4031DDF34AC79A7C" Ref="C10"  Part="1" 
AR Path="/4032778D4AC79A7C" Ref="C10"  Part="1" 
AR Path="/403091264AC79A7C" Ref="C10"  Part="1" 
AR Path="/403051264AC79A7C" Ref="C10"  Part="1" 
AR Path="/4032F78D4AC79A7C" Ref="C10"  Part="1" 
AR Path="/403251264AC79A7C" Ref="C10"  Part="1" 
AR Path="/4032AAC04AC79A7C" Ref="C10"  Part="1" 
AR Path="/4030D1264AC79A7C" Ref="C10"  Part="1" 
AR Path="/4031778D4AC79A7C" Ref="C10"  Part="1" 
AR Path="/3FEA24DD4AC79A7C" Ref="C10"  Part="1" 
AR Path="/6FE934E34AC79A7C" Ref="C10"  Part="1" 
AR Path="/773F65F14AC79A7C" Ref="C10"  Part="1" 
AR Path="/773F8EB44AC79A7C" Ref="C10"  Part="1" 
AR Path="/23C9F04AC79A7C" Ref="C10"  Part="1" 
AR Path="/24AC79A7C" Ref="C10"  Part="1" 
AR Path="/23BC884AC79A7C" Ref="C10"  Part="1" 
AR Path="/DC0C124AC79A7C" Ref="C10"  Part="1" 
AR Path="/23C34C4AC79A7C" Ref="C10"  Part="1" 
AR Path="/23CBC44AC79A7C" Ref="C10"  Part="1" 
AR Path="/23C6504AC79A7C" Ref="C10"  Part="1" 
AR Path="/39803EA4AC79A7C" Ref="C10"  Part="1" 
AR Path="/FFFFFFFF4AC79A7C" Ref="C10"  Part="1" 
AR Path="/6FE934344AC79A7C" Ref="C10"  Part="1" 
AR Path="/7E0073254AC79A7C" Ref="C10"  Part="1" 
AR Path="/73254AC79A7C" Ref="C10"  Part="1" 
AR Path="/8CEE4AC79A7C" Ref="C10"  Part="1" 
AR Path="/23D9004AC79A7C" Ref="C10"  Part="1" 
AR Path="/91964AC79A7C" Ref="C10"  Part="1" 
AR Path="/77F16B254AC79A7C" Ref="C10"  Part="1" 
AR Path="/23D83C4AC79A7C" Ref="C10"  Part="1" 
AR Path="/47907FA4AC79A7C" Ref="C10"  Part="1" 
AR Path="/8D384AC79A7C" Ref="C10"  Part="1" 
AR Path="/D058E04AC79A7C" Ref="C10"  Part="1" 
AR Path="/3C64AC79A7C" Ref="C10"  Part="1" 
AR Path="/2F4A4AC79A7C" Ref="C10"  Part="1" 
AR Path="/D1C3384AC79A7C" Ref="C10"  Part="1" 
F 0 "C10" H 38800 22350 50  0000 L CNN
F 1 "0.1 uF" H 38800 22150 50  0000 L CNN
F 2 "C2" H 38750 22250 60  0001 C CNN
F 3 "" H 38750 22250 60  0001 C CNN
	1    38750 22250
	1    0    0    -1  
$EndComp
$Comp
L C C8
U 1 1 4AC79A72
P 37850 22250
AR Path="/4AC79A72" Ref="C8"  Part="1" 
AR Path="/94AC79A72" Ref="C8"  Part="1" 
AR Path="/14AC79A72" Ref="C8"  Part="1" 
AR Path="/6FF0DD404AC79A72" Ref="C8"  Part="1" 
AR Path="/755D912A4AC79A72" Ref="C8"  Part="1" 
AR Path="/6FE901F74AC79A72" Ref="C8"  Part="1" 
AR Path="/3FE08B434AC79A72" Ref="C8"  Part="1" 
AR Path="/3DA360004AC79A72" Ref="C8"  Part="1" 
AR Path="/5AD7153D4AC79A72" Ref="C8"  Part="1" 
AR Path="/A4AC79A72" Ref="C8"  Part="1" 
AR Path="/23D9304AC79A72" Ref="C8"  Part="1" 
AR Path="/3FEFFFFF4AC79A72" Ref="C8"  Part="1" 
AR Path="/23D8D44AC79A72" Ref="C8"  Part="1" 
AR Path="/3D8EA0004AC79A72" Ref="C8"  Part="1" 
AR Path="/402B08B44AC79A72" Ref="C8"  Part="1" 
AR Path="/4026A24D4AC79A72" Ref="C8"  Part="1" 
AR Path="/3FE88B434AC79A72" Ref="C8"  Part="1" 
AR Path="/3D7E00004AC79A72" Ref="C8"  Part="1" 
AR Path="/402988B44AC79A72" Ref="C8"  Part="1" 
AR Path="/3D6CC0004AC79A72" Ref="C8"  Part="1" 
AR Path="/FFFFFFF04AC79A72" Ref="C8"  Part="1" 
AR Path="/23C2344AC79A72" Ref="C8"  Part="1" 
AR Path="/DCBAABCD4AC79A72" Ref="C8"  Part="1" 
AR Path="/5AD746F64AC79A72" Ref="C8"  Part="1" 
AR Path="/4027EF1A4AC79A72" Ref="C8"  Part="1" 
AR Path="/402A6F1A4AC79A72" Ref="C8"  Part="1" 
AR Path="/4029BBE74AC79A72" Ref="C8"  Part="1" 
AR Path="/402A224D4AC79A72" Ref="C8"  Part="1" 
AR Path="/402A55814AC79A72" Ref="C8"  Part="1" 
AR Path="/4026224D4AC79A72" Ref="C8"  Part="1" 
AR Path="/3D6220004AC79A72" Ref="C8"  Part="1" 
AR Path="/3D9720004AC79A72" Ref="C8"  Part="1" 
AR Path="/2600004AC79A72" Ref="C8"  Part="1" 
AR Path="/402C3BE74AC79A72" Ref="C8"  Part="1" 
AR Path="/402955814AC79A72" Ref="C8"  Part="1" 
AR Path="/40293BE74AC79A72" Ref="C8"  Part="1" 
AR Path="/402555814AC79A72" Ref="C8"  Part="1" 
AR Path="/23D2034AC79A72" Ref="C8"  Part="1" 
AR Path="/3FED58104AC79A72" Ref="C8"  Part="1" 
AR Path="/402688B44AC79A72" Ref="C8"  Part="1" 
AR Path="/3FE441894AC79A72" Ref="C8"  Part="1" 
AR Path="/B84AC79A72" Ref="C8"  Part="1" 
AR Path="/4029D5814AC79A72" Ref="C8"  Part="1" 
AR Path="/40256F1A4AC79A72" Ref="C8"  Part="1" 
AR Path="/40286F1A4AC79A72" Ref="C8"  Part="1" 
AR Path="/402A08B44AC79A72" Ref="C8"  Part="1" 
AR Path="/402588B44AC79A72" Ref="C8"  Part="1" 
AR Path="/A84AC79A72" Ref="C8"  Part="1" 
AR Path="/3FE224DD4AC79A72" Ref="C8"  Part="1" 
AR Path="/402C08B44AC79A72" Ref="C8"  Part="1" 
AR Path="/402BEF1A4AC79A72" Ref="C8"  Part="1" 
AR Path="/402C55814AC79A72" Ref="C8"  Part="1" 
AR Path="/40273BE74AC79A72" Ref="C8"  Part="1" 
AR Path="/402908B44AC79A72" Ref="C8"  Part="1" 
AR Path="/3D5A40004AC79A72" Ref="C8"  Part="1" 
AR Path="/402755814AC79A72" Ref="C8"  Part="1" 
AR Path="/4030AAC04AC79A72" Ref="C8"  Part="1" 
AR Path="/4031DDF34AC79A72" Ref="C8"  Part="1" 
AR Path="/4032778D4AC79A72" Ref="C8"  Part="1" 
AR Path="/403091264AC79A72" Ref="C8"  Part="1" 
AR Path="/403051264AC79A72" Ref="C8"  Part="1" 
AR Path="/4032F78D4AC79A72" Ref="C8"  Part="1" 
AR Path="/403251264AC79A72" Ref="C8"  Part="1" 
AR Path="/4032AAC04AC79A72" Ref="C8"  Part="1" 
AR Path="/4030D1264AC79A72" Ref="C8"  Part="1" 
AR Path="/4031778D4AC79A72" Ref="C8"  Part="1" 
AR Path="/3FEA24DD4AC79A72" Ref="C8"  Part="1" 
AR Path="/6FE934E34AC79A72" Ref="C8"  Part="1" 
AR Path="/773F65F14AC79A72" Ref="C8"  Part="1" 
AR Path="/773F8EB44AC79A72" Ref="C8"  Part="1" 
AR Path="/23C9F04AC79A72" Ref="C8"  Part="1" 
AR Path="/24AC79A72" Ref="C8"  Part="1" 
AR Path="/23BC884AC79A72" Ref="C8"  Part="1" 
AR Path="/DC0C124AC79A72" Ref="C8"  Part="1" 
AR Path="/23C34C4AC79A72" Ref="C8"  Part="1" 
AR Path="/23CBC44AC79A72" Ref="C8"  Part="1" 
AR Path="/23C6504AC79A72" Ref="C8"  Part="1" 
AR Path="/39803EA4AC79A72" Ref="C8"  Part="1" 
AR Path="/FFFFFFFF4AC79A72" Ref="C8"  Part="1" 
AR Path="/6FE934344AC79A72" Ref="C8"  Part="1" 
AR Path="/7E0073254AC79A72" Ref="C8"  Part="1" 
AR Path="/73254AC79A72" Ref="C8"  Part="1" 
AR Path="/8CEE4AC79A72" Ref="C8"  Part="1" 
AR Path="/23D9004AC79A72" Ref="C8"  Part="1" 
AR Path="/91964AC79A72" Ref="C8"  Part="1" 
AR Path="/77F16B254AC79A72" Ref="C8"  Part="1" 
AR Path="/23D83C4AC79A72" Ref="C8"  Part="1" 
AR Path="/47907FA4AC79A72" Ref="C8"  Part="1" 
AR Path="/8D384AC79A72" Ref="C8"  Part="1" 
AR Path="/D058E04AC79A72" Ref="C8"  Part="1" 
AR Path="/3C64AC79A72" Ref="C8"  Part="1" 
AR Path="/2F4A4AC79A72" Ref="C8"  Part="1" 
AR Path="/D1C3384AC79A72" Ref="C8"  Part="1" 
F 0 "C8" H 37900 22350 50  0000 L CNN
F 1 "0.1 uF" H 37900 22150 50  0000 L CNN
F 2 "C2" H 37850 22250 60  0001 C CNN
F 3 "" H 37850 22250 60  0001 C CNN
	1    37850 22250
	1    0    0    -1  
$EndComp
$Comp
L C C7
U 1 1 4AC79A4A
P 37400 22250
AR Path="/4AC79A4A" Ref="C7"  Part="1" 
AR Path="/7C9114604AC79A4A" Ref="C?"  Part="1" 
AR Path="/404AC79A4A" Ref="C?"  Part="1" 
AR Path="/94AC79A4A" Ref="C7"  Part="1" 
AR Path="/14AC79A4A" Ref="C7"  Part="1" 
AR Path="/6FF0DD404AC79A4A" Ref="C7"  Part="1" 
AR Path="/755D912A4AC79A4A" Ref="C7"  Part="1" 
AR Path="/6FE901F74AC79A4A" Ref="C7"  Part="1" 
AR Path="/3FE08B434AC79A4A" Ref="C7"  Part="1" 
AR Path="/3DA360004AC79A4A" Ref="C7"  Part="1" 
AR Path="/5AD7153D4AC79A4A" Ref="C7"  Part="1" 
AR Path="/A4AC79A4A" Ref="C7"  Part="1" 
AR Path="/23D9304AC79A4A" Ref="C7"  Part="1" 
AR Path="/3FEFFFFF4AC79A4A" Ref="C7"  Part="1" 
AR Path="/23D8D44AC79A4A" Ref="C7"  Part="1" 
AR Path="/3D8EA0004AC79A4A" Ref="C7"  Part="1" 
AR Path="/402B08B44AC79A4A" Ref="C7"  Part="1" 
AR Path="/4026A24D4AC79A4A" Ref="C7"  Part="1" 
AR Path="/3FE88B434AC79A4A" Ref="C7"  Part="1" 
AR Path="/3D7E00004AC79A4A" Ref="C7"  Part="1" 
AR Path="/402988B44AC79A4A" Ref="C7"  Part="1" 
AR Path="/3D6CC0004AC79A4A" Ref="C7"  Part="1" 
AR Path="/FFFFFFF04AC79A4A" Ref="C7"  Part="1" 
AR Path="/23C2344AC79A4A" Ref="C7"  Part="1" 
AR Path="/DCBAABCD4AC79A4A" Ref="C7"  Part="1" 
AR Path="/5AD746F64AC79A4A" Ref="C7"  Part="1" 
AR Path="/4027EF1A4AC79A4A" Ref="C7"  Part="1" 
AR Path="/402A6F1A4AC79A4A" Ref="C7"  Part="1" 
AR Path="/4029BBE74AC79A4A" Ref="C7"  Part="1" 
AR Path="/402A224D4AC79A4A" Ref="C7"  Part="1" 
AR Path="/402A55814AC79A4A" Ref="C7"  Part="1" 
AR Path="/4026224D4AC79A4A" Ref="C7"  Part="1" 
AR Path="/3D6220004AC79A4A" Ref="C7"  Part="1" 
AR Path="/3D9720004AC79A4A" Ref="C7"  Part="1" 
AR Path="/2600004AC79A4A" Ref="C7"  Part="1" 
AR Path="/402C3BE74AC79A4A" Ref="C7"  Part="1" 
AR Path="/402955814AC79A4A" Ref="C7"  Part="1" 
AR Path="/40293BE74AC79A4A" Ref="C7"  Part="1" 
AR Path="/402555814AC79A4A" Ref="C7"  Part="1" 
AR Path="/23D2034AC79A4A" Ref="C7"  Part="1" 
AR Path="/3FED58104AC79A4A" Ref="C7"  Part="1" 
AR Path="/402688B44AC79A4A" Ref="C7"  Part="1" 
AR Path="/3FE441894AC79A4A" Ref="C7"  Part="1" 
AR Path="/B84AC79A4A" Ref="C7"  Part="1" 
AR Path="/4029D5814AC79A4A" Ref="C7"  Part="1" 
AR Path="/40256F1A4AC79A4A" Ref="C7"  Part="1" 
AR Path="/40286F1A4AC79A4A" Ref="C7"  Part="1" 
AR Path="/402A08B44AC79A4A" Ref="C7"  Part="1" 
AR Path="/402588B44AC79A4A" Ref="C7"  Part="1" 
AR Path="/A84AC79A4A" Ref="C7"  Part="1" 
AR Path="/3FE224DD4AC79A4A" Ref="C7"  Part="1" 
AR Path="/402C08B44AC79A4A" Ref="C7"  Part="1" 
AR Path="/402BEF1A4AC79A4A" Ref="C7"  Part="1" 
AR Path="/402C55814AC79A4A" Ref="C7"  Part="1" 
AR Path="/40273BE74AC79A4A" Ref="C7"  Part="1" 
AR Path="/402908B44AC79A4A" Ref="C7"  Part="1" 
AR Path="/3D5A40004AC79A4A" Ref="C7"  Part="1" 
AR Path="/402755814AC79A4A" Ref="C7"  Part="1" 
AR Path="/4030AAC04AC79A4A" Ref="C7"  Part="1" 
AR Path="/4031DDF34AC79A4A" Ref="C7"  Part="1" 
AR Path="/4032778D4AC79A4A" Ref="C7"  Part="1" 
AR Path="/403091264AC79A4A" Ref="C7"  Part="1" 
AR Path="/403051264AC79A4A" Ref="C7"  Part="1" 
AR Path="/4032F78D4AC79A4A" Ref="C7"  Part="1" 
AR Path="/403251264AC79A4A" Ref="C7"  Part="1" 
AR Path="/4032AAC04AC79A4A" Ref="C7"  Part="1" 
AR Path="/4030D1264AC79A4A" Ref="C7"  Part="1" 
AR Path="/4031778D4AC79A4A" Ref="C7"  Part="1" 
AR Path="/3FEA24DD4AC79A4A" Ref="C7"  Part="1" 
AR Path="/6FE934E34AC79A4A" Ref="C7"  Part="1" 
AR Path="/773F65F14AC79A4A" Ref="C7"  Part="1" 
AR Path="/773F8EB44AC79A4A" Ref="C7"  Part="1" 
AR Path="/23C9F04AC79A4A" Ref="C7"  Part="1" 
AR Path="/24AC79A4A" Ref="C7"  Part="1" 
AR Path="/23BC884AC79A4A" Ref="C7"  Part="1" 
AR Path="/DC0C124AC79A4A" Ref="C7"  Part="1" 
AR Path="/23C34C4AC79A4A" Ref="C7"  Part="1" 
AR Path="/23CBC44AC79A4A" Ref="C7"  Part="1" 
AR Path="/23C6504AC79A4A" Ref="C7"  Part="1" 
AR Path="/39803EA4AC79A4A" Ref="C7"  Part="1" 
AR Path="/FFFFFFFF4AC79A4A" Ref="C7"  Part="1" 
AR Path="/6FE934344AC79A4A" Ref="C7"  Part="1" 
AR Path="/7E0073254AC79A4A" Ref="C7"  Part="1" 
AR Path="/73254AC79A4A" Ref="C7"  Part="1" 
AR Path="/8CEE4AC79A4A" Ref="C7"  Part="1" 
AR Path="/23D9004AC79A4A" Ref="C7"  Part="1" 
AR Path="/91964AC79A4A" Ref="C7"  Part="1" 
AR Path="/77F16B254AC79A4A" Ref="C7"  Part="1" 
AR Path="/23D83C4AC79A4A" Ref="C7"  Part="1" 
AR Path="/47907FA4AC79A4A" Ref="C7"  Part="1" 
AR Path="/8D384AC79A4A" Ref="C7"  Part="1" 
AR Path="/D058E04AC79A4A" Ref="C7"  Part="1" 
AR Path="/3C64AC79A4A" Ref="C7"  Part="1" 
AR Path="/2F4A4AC79A4A" Ref="C7"  Part="1" 
AR Path="/D1C3384AC79A4A" Ref="C7"  Part="1" 
F 0 "C7" H 37450 22350 50  0000 L CNN
F 1 "0.1 uF" H 37450 22150 50  0000 L CNN
F 2 "C2" H 37400 22250 60  0001 C CNN
F 3 "" H 37400 22250 60  0001 C CNN
	1    37400 22250
	1    0    0    -1  
$EndComp
Text Label 39600 26300 0    60   ~ 0
VCC
Text Label 41550 16550 0    60   ~ 0
0V
Text Label 41550 16450 0    60   ~ 0
POC*
Text Label 41550 16350 0    60   ~ 0
ERROR*
Text Label 41550 16250 0    60   ~ 0
sWO*
Text Label 41550 16150 0    60   ~ 0
sINTA
Text Label 41550 16050 0    60   ~ 0
DI0
Text Label 41550 15950 0    60   ~ 0
DI1
Text Label 41550 15850 0    60   ~ 0
DI6
Text Label 41550 15750 0    60   ~ 0
DI5
Text Label 41550 15650 0    60   ~ 0
DI4
Text Label 41550 15550 0    60   ~ 0
DO7
Text Label 41550 15450 0    60   ~ 0
DO3
Text Label 41550 15350 0    60   ~ 0
DO2
Text Label 41550 15250 0    60   ~ 0
A11
Text Label 41550 15150 0    60   ~ 0
A14
Text Label 41550 15050 0    60   ~ 0
A13
Text Label 41550 14950 0    60   ~ 0
A8
Text Label 41550 14850 0    60   ~ 0
A7
Text Label 41550 14750 0    60   ~ 0
A6
Text Label 41550 14650 0    60   ~ 0
A2
Text Label 41550 14550 0    60   ~ 0
A1
Text Label 41550 14450 0    60   ~ 0
A0
Text Label 41550 14350 0    60   ~ 0
pDBIN
Text Label 41550 14250 0    60   ~ 0
pWR*
Text Label 41550 14150 0    60   ~ 0
pSYNC
Text Label 41550 14050 0    60   ~ 0
RESET*
Text Label 41550 13950 0    60   ~ 0
HOLD*
Text Label 41550 13850 0    60   ~ 0
INT*
Text Label 41550 13750 0    60   ~ 0
RDY
Text Label 41550 13650 0    60   ~ 0
RFU4
Text Label 41550 13550 0    60   ~ 0
0V_C
Text Label 41550 13450 0    60   ~ 0
RFU3
Text Label 41550 13350 0    60   ~ 0
MWRT
Text Label 41550 13250 0    60   ~ 0
PHANTOM*
Text Label 41550 13150 0    60   ~ 0
NDEF3
Text Label 41550 13050 0    60   ~ 0
NDEF2
Text Label 41550 12950 0    60   ~ 0
A23
Text Label 41550 12850 0    60   ~ 0
A22
Text Label 41550 12750 0    60   ~ 0
A21
Text Label 41550 12650 0    60   ~ 0
A20
Text Label 41550 12550 0    60   ~ 0
SIXTN*
Text Label 41550 12450 0    60   ~ 0
A19
Text Label 41550 12350 0    60   ~ 0
sXTRQ*
Text Label 41550 12250 0    60   ~ 0
TMA2*
Text Label 41550 12150 0    60   ~ 0
TMA1*
Text Label 41550 12050 0    60   ~ 0
TMA0*
Text Label 41550 11950 0    60   ~ 0
SLAVE_CLR*
Text Label 41550 11850 0    60   ~ 0
0V_B
Text Label 41550 11750 0    60   ~ 0
-16V
Text Label 41550 11550 0    60   ~ 0
0V
Text Label 41550 11450 0    60   ~ 0
CLOCK*
Text Label 41550 11350 0    60   ~ 0
sHLTA
Text Label 41550 11250 0    60   ~ 0
sMEMR
Text Label 41550 11150 0    60   ~ 0
sINP
Text Label 41550 11050 0    60   ~ 0
sOUT
Text Label 41550 10950 0    60   ~ 0
sM1
Text Label 41550 10850 0    60   ~ 0
DI7
Text Label 41550 10750 0    60   ~ 0
DI3
Text Label 41550 10650 0    60   ~ 0
DI2
Text Label 41550 10550 0    60   ~ 0
DO6
Text Label 41550 10450 0    60   ~ 0
DO5
Text Label 41550 10350 0    60   ~ 0
DO4
Text Label 41550 10250 0    60   ~ 0
A10
Text Label 41550 10150 0    60   ~ 0
DO0
Text Label 41550 10050 0    60   ~ 0
DO1
Text Label 41550 9950 0    60   ~ 0
A9
Text Label 41550 9850 0    60   ~ 0
A12
Text Label 41550 9750 0    60   ~ 0
A15
Text Label 41550 9650 0    60   ~ 0
A3
Text Label 41550 9550 0    60   ~ 0
A4
Text Label 41550 9450 0    60   ~ 0
A5
Text Label 41550 9350 0    60   ~ 0
RFU2
Text Label 41550 9250 0    60   ~ 0
RFU1
Text Label 41550 9150 0    60   ~ 0
pHLDA
Text Label 41550 9050 0    60   ~ 0
pSTVAL*
Text Label 41550 8950 0    60   ~ 0
PHI
Text Label 41550 8850 0    60   ~ 0
DODSB*
Text Label 41550 8750 0    60   ~ 0
ADSB*
Text Label 41550 8650 0    60   ~ 0
NDEF1
Text Label 41550 8550 0    60   ~ 0
0V_A
Text Label 41550 8450 0    60   ~ 0
CDSB*
Text Label 41550 8350 0    60   ~ 0
SDSB*
Text Label 41550 8250 0    60   ~ 0
A17
Text Label 41550 8150 0    60   ~ 0
A16
Text Label 41550 8050 0    60   ~ 0
A18
Text Label 41550 7950 0    60   ~ 0
TMA3*
Text Label 41550 7850 0    60   ~ 0
PWRFAIL*
Text Label 41550 7750 0    60   ~ 0
NMI*
Text Label 41550 7650 0    60   ~ 0
VI7*
Text Label 41550 7550 0    60   ~ 0
VI6*
Text Label 41550 7450 0    60   ~ 0
VI5*
Text Label 41550 7350 0    60   ~ 0
VI4*
Text Label 41550 7250 0    60   ~ 0
VI3*
Text Label 41550 7150 0    60   ~ 0
VI2*
Text Label 41550 7050 0    60   ~ 0
VI1*
Text Label 41550 6950 0    60   ~ 0
VI0*
Text Label 41550 6850 0    60   ~ 0
XRDY
Text Label 41550 6750 0    60   ~ 0
+16V
Text Label 41550 6650 0    60   ~ 0
+8V
Text Notes 38250 27350 0    60   ~ 0
Card Ejector Mounting Holes
$Comp
L CONN_1 P34
U 1 1 4A0CC099
P 38800 27500
AR Path="/FFFFFFFF4A0CC099" Ref="P34"  Part="1" 
AR Path="/4A0CC099" Ref="P34"  Part="1" 
AR Path="/94A0CC099" Ref="P34"  Part="1" 
AR Path="/6FF0DD404A0CC099" Ref="P34"  Part="1" 
AR Path="/23D6E04A0CC099" Ref="P34"  Part="1" 
AR Path="/5AD7153D4A0CC099" Ref="P34"  Part="1" 
AR Path="/A4A0CC099" Ref="P34"  Part="1" 
AR Path="/755D912A4A0CC099" Ref="P34"  Part="1" 
AR Path="/3FE08B434A0CC099" Ref="P34"  Part="1" 
AR Path="/3DA360004A0CC099" Ref="P34"  Part="1" 
AR Path="/23D9304A0CC099" Ref="P34"  Part="1" 
AR Path="/3FEFFFFF4A0CC099" Ref="P34"  Part="1" 
AR Path="/23D8D44A0CC099" Ref="P34"  Part="1" 
AR Path="/3D8EA0004A0CC099" Ref="P34"  Part="1" 
AR Path="/402B08B44A0CC099" Ref="P34"  Part="1" 
AR Path="/4026A24D4A0CC099" Ref="P34"  Part="1" 
AR Path="/3FE88B434A0CC099" Ref="P34"  Part="1" 
AR Path="/3D7E00004A0CC099" Ref="P34"  Part="1" 
AR Path="/402988B44A0CC099" Ref="P34"  Part="1" 
AR Path="/6FE901F74A0CC099" Ref="P34"  Part="1" 
AR Path="/3D6CC0004A0CC099" Ref="P34"  Part="1" 
AR Path="/14A0CC099" Ref="P34"  Part="1" 
AR Path="/FFFFFFF04A0CC099" Ref="P34"  Part="1" 
AR Path="/23C2344A0CC099" Ref="P34"  Part="1" 
AR Path="/DCBAABCD4A0CC099" Ref="P34"  Part="1" 
AR Path="/5AD746F64A0CC099" Ref="P34"  Part="1" 
AR Path="/4027EF1A4A0CC099" Ref="P34"  Part="1" 
AR Path="/402A6F1A4A0CC099" Ref="P34"  Part="1" 
AR Path="/4029BBE74A0CC099" Ref="P34"  Part="1" 
AR Path="/402A224D4A0CC099" Ref="P34"  Part="1" 
AR Path="/402A55814A0CC099" Ref="P34"  Part="1" 
AR Path="/4026224D4A0CC099" Ref="P34"  Part="1" 
AR Path="/3D6220004A0CC099" Ref="P34"  Part="1" 
AR Path="/3D9720004A0CC099" Ref="P34"  Part="1" 
AR Path="/2600004A0CC099" Ref="P34"  Part="1" 
AR Path="/402C3BE74A0CC099" Ref="P34"  Part="1" 
AR Path="/402955814A0CC099" Ref="P34"  Part="1" 
AR Path="/40293BE74A0CC099" Ref="P34"  Part="1" 
AR Path="/402555814A0CC099" Ref="P34"  Part="1" 
AR Path="/3FED58104A0CC099" Ref="P34"  Part="1" 
AR Path="/402688B44A0CC099" Ref="P34"  Part="1" 
AR Path="/3FE441894A0CC099" Ref="P34"  Part="1" 
AR Path="/B84A0CC099" Ref="P34"  Part="1" 
AR Path="/4029D5814A0CC099" Ref="P34"  Part="1" 
AR Path="/6FF405304A0CC099" Ref="P34"  Part="1" 
AR Path="/40256F1A4A0CC099" Ref="P34"  Part="1" 
AR Path="/40286F1A4A0CC099" Ref="P34"  Part="1" 
AR Path="/402A08B44A0CC099" Ref="P34"  Part="1" 
AR Path="/402588B44A0CC099" Ref="P34"  Part="1" 
AR Path="/23D2034A0CC099" Ref="P34"  Part="1" 
AR Path="/A84A0CC099" Ref="P34"  Part="1" 
AR Path="/3FE224DD4A0CC099" Ref="P34"  Part="1" 
AR Path="/402C08B44A0CC099" Ref="P34"  Part="1" 
AR Path="/402BEF1A4A0CC099" Ref="P34"  Part="1" 
AR Path="/402C55814A0CC099" Ref="P34"  Part="1" 
AR Path="/40273BE74A0CC099" Ref="P34"  Part="1" 
AR Path="/402908B44A0CC099" Ref="P34"  Part="1" 
AR Path="/3D5A40004A0CC099" Ref="P34"  Part="1" 
AR Path="/402755814A0CC099" Ref="P34"  Part="1" 
AR Path="/4030AAC04A0CC099" Ref="P34"  Part="1" 
AR Path="/4031DDF34A0CC099" Ref="P34"  Part="1" 
AR Path="/4032778D4A0CC099" Ref="P34"  Part="1" 
AR Path="/403091264A0CC099" Ref="P34"  Part="1" 
AR Path="/403051264A0CC099" Ref="P34"  Part="1" 
AR Path="/4032F78D4A0CC099" Ref="P34"  Part="1" 
AR Path="/403251264A0CC099" Ref="P34"  Part="1" 
AR Path="/4032AAC04A0CC099" Ref="P34"  Part="1" 
AR Path="/4030D1264A0CC099" Ref="P34"  Part="1" 
AR Path="/4031778D4A0CC099" Ref="P34"  Part="1" 
AR Path="/3FEA24DD4A0CC099" Ref="P34"  Part="1" 
AR Path="/6FE934E34A0CC099" Ref="P34"  Part="1" 
AR Path="/773F65F14A0CC099" Ref="P34"  Part="1" 
AR Path="/773F8EB44A0CC099" Ref="P34"  Part="1" 
AR Path="/23C9F04A0CC099" Ref="P34"  Part="1" 
AR Path="/24A0CC099" Ref="P34"  Part="1" 
AR Path="/23BC884A0CC099" Ref="P34"  Part="1" 
AR Path="/DC0C124A0CC099" Ref="P34"  Part="1" 
AR Path="/23C34C4A0CC099" Ref="P34"  Part="1" 
AR Path="/23CBC44A0CC099" Ref="P34"  Part="1" 
AR Path="/23C6504A0CC099" Ref="P34"  Part="1" 
AR Path="/39803EA4A0CC099" Ref="P34"  Part="1" 
AR Path="/6FE934344A0CC099" Ref="P34"  Part="1" 
AR Path="/7E0073254A0CC099" Ref="P34"  Part="1" 
AR Path="/73254A0CC099" Ref="P34"  Part="1" 
AR Path="/8CEE4A0CC099" Ref="P34"  Part="1" 
AR Path="/23D9004A0CC099" Ref="P34"  Part="1" 
AR Path="/91964A0CC099" Ref="P34"  Part="1" 
AR Path="/77F16B254A0CC099" Ref="P34"  Part="1" 
AR Path="/23D83C4A0CC099" Ref="P34"  Part="1" 
AR Path="/8D384A0CC099" Ref="P34"  Part="1" 
AR Path="/D058E04A0CC099" Ref="P34"  Part="1" 
AR Path="/3C64A0CC099" Ref="P34"  Part="1" 
AR Path="/2F4A4A0CC099" Ref="P34"  Part="1" 
AR Path="/D1C3384A0CC099" Ref="P34"  Part="1" 
F 0 "P34" H 38880 27500 40  0000 C CNN
F 1 "CONN_1" H 38750 27540 30  0001 C CNN
F 2 "1pin" H 38800 27500 60  0001 C CNN
F 3 "" H 38800 27500 60  0001 C CNN
	1    38800 27500
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P35
U 1 1 4A0CC090
P 38800 27400
AR Path="/FFFFFFFF4A0CC090" Ref="P35"  Part="1" 
AR Path="/4A0CC090" Ref="P35"  Part="1" 
AR Path="/94A0CC090" Ref="P35"  Part="1" 
AR Path="/6FF0DD404A0CC090" Ref="P35"  Part="1" 
AR Path="/23D6E04A0CC090" Ref="P35"  Part="1" 
AR Path="/5AD7153D4A0CC090" Ref="P35"  Part="1" 
AR Path="/A4A0CC090" Ref="P35"  Part="1" 
AR Path="/755D912A4A0CC090" Ref="P35"  Part="1" 
AR Path="/3FE08B434A0CC090" Ref="P35"  Part="1" 
AR Path="/3DA360004A0CC090" Ref="P35"  Part="1" 
AR Path="/23D9304A0CC090" Ref="P35"  Part="1" 
AR Path="/3FEFFFFF4A0CC090" Ref="P35"  Part="1" 
AR Path="/23D8D44A0CC090" Ref="P35"  Part="1" 
AR Path="/3D8EA0004A0CC090" Ref="P35"  Part="1" 
AR Path="/402B08B44A0CC090" Ref="P35"  Part="1" 
AR Path="/4026A24D4A0CC090" Ref="P35"  Part="1" 
AR Path="/3FE88B434A0CC090" Ref="P35"  Part="1" 
AR Path="/3D7E00004A0CC090" Ref="P35"  Part="1" 
AR Path="/402988B44A0CC090" Ref="P35"  Part="1" 
AR Path="/6FE901F74A0CC090" Ref="P35"  Part="1" 
AR Path="/3D6CC0004A0CC090" Ref="P35"  Part="1" 
AR Path="/14A0CC090" Ref="P35"  Part="1" 
AR Path="/FFFFFFF04A0CC090" Ref="P35"  Part="1" 
AR Path="/23C2344A0CC090" Ref="P35"  Part="1" 
AR Path="/DCBAABCD4A0CC090" Ref="P35"  Part="1" 
AR Path="/5AD746F64A0CC090" Ref="P35"  Part="1" 
AR Path="/4027EF1A4A0CC090" Ref="P35"  Part="1" 
AR Path="/402A6F1A4A0CC090" Ref="P35"  Part="1" 
AR Path="/4029BBE74A0CC090" Ref="P35"  Part="1" 
AR Path="/402A224D4A0CC090" Ref="P35"  Part="1" 
AR Path="/402A55814A0CC090" Ref="P35"  Part="1" 
AR Path="/4026224D4A0CC090" Ref="P35"  Part="1" 
AR Path="/3D6220004A0CC090" Ref="P35"  Part="1" 
AR Path="/3D9720004A0CC090" Ref="P35"  Part="1" 
AR Path="/2600004A0CC090" Ref="P35"  Part="1" 
AR Path="/402C3BE74A0CC090" Ref="P35"  Part="1" 
AR Path="/402955814A0CC090" Ref="P35"  Part="1" 
AR Path="/40293BE74A0CC090" Ref="P35"  Part="1" 
AR Path="/402555814A0CC090" Ref="P35"  Part="1" 
AR Path="/3FED58104A0CC090" Ref="P35"  Part="1" 
AR Path="/402688B44A0CC090" Ref="P35"  Part="1" 
AR Path="/3FE441894A0CC090" Ref="P35"  Part="1" 
AR Path="/B84A0CC090" Ref="P35"  Part="1" 
AR Path="/4029D5814A0CC090" Ref="P35"  Part="1" 
AR Path="/6FF405304A0CC090" Ref="P35"  Part="1" 
AR Path="/40256F1A4A0CC090" Ref="P35"  Part="1" 
AR Path="/40286F1A4A0CC090" Ref="P35"  Part="1" 
AR Path="/402A08B44A0CC090" Ref="P35"  Part="1" 
AR Path="/402588B44A0CC090" Ref="P35"  Part="1" 
AR Path="/23D2034A0CC090" Ref="P35"  Part="1" 
AR Path="/A84A0CC090" Ref="P35"  Part="1" 
AR Path="/3FE224DD4A0CC090" Ref="P35"  Part="1" 
AR Path="/402C08B44A0CC090" Ref="P35"  Part="1" 
AR Path="/402BEF1A4A0CC090" Ref="P35"  Part="1" 
AR Path="/402C55814A0CC090" Ref="P35"  Part="1" 
AR Path="/40273BE74A0CC090" Ref="P35"  Part="1" 
AR Path="/402908B44A0CC090" Ref="P35"  Part="1" 
AR Path="/3D5A40004A0CC090" Ref="P35"  Part="1" 
AR Path="/402755814A0CC090" Ref="P35"  Part="1" 
AR Path="/4030AAC04A0CC090" Ref="P35"  Part="1" 
AR Path="/4031DDF34A0CC090" Ref="P35"  Part="1" 
AR Path="/4032778D4A0CC090" Ref="P35"  Part="1" 
AR Path="/403091264A0CC090" Ref="P35"  Part="1" 
AR Path="/403051264A0CC090" Ref="P35"  Part="1" 
AR Path="/4032F78D4A0CC090" Ref="P35"  Part="1" 
AR Path="/403251264A0CC090" Ref="P35"  Part="1" 
AR Path="/4032AAC04A0CC090" Ref="P35"  Part="1" 
AR Path="/4030D1264A0CC090" Ref="P35"  Part="1" 
AR Path="/4031778D4A0CC090" Ref="P35"  Part="1" 
AR Path="/3FEA24DD4A0CC090" Ref="P35"  Part="1" 
AR Path="/6FE934E34A0CC090" Ref="P35"  Part="1" 
AR Path="/773F65F14A0CC090" Ref="P35"  Part="1" 
AR Path="/773F8EB44A0CC090" Ref="P35"  Part="1" 
AR Path="/23C9F04A0CC090" Ref="P35"  Part="1" 
AR Path="/24A0CC090" Ref="P35"  Part="1" 
AR Path="/23BC884A0CC090" Ref="P35"  Part="1" 
AR Path="/DC0C124A0CC090" Ref="P35"  Part="1" 
AR Path="/23C34C4A0CC090" Ref="P35"  Part="1" 
AR Path="/23CBC44A0CC090" Ref="P35"  Part="1" 
AR Path="/23C6504A0CC090" Ref="P35"  Part="1" 
AR Path="/39803EA4A0CC090" Ref="P35"  Part="1" 
AR Path="/6FE934344A0CC090" Ref="P35"  Part="1" 
AR Path="/7E0073254A0CC090" Ref="P35"  Part="1" 
AR Path="/73254A0CC090" Ref="P35"  Part="1" 
AR Path="/8CEE4A0CC090" Ref="P35"  Part="1" 
AR Path="/23D9004A0CC090" Ref="P35"  Part="1" 
AR Path="/91964A0CC090" Ref="P35"  Part="1" 
AR Path="/77F16B254A0CC090" Ref="P35"  Part="1" 
AR Path="/23D83C4A0CC090" Ref="P35"  Part="1" 
AR Path="/8D384A0CC090" Ref="P35"  Part="1" 
AR Path="/D058E04A0CC090" Ref="P35"  Part="1" 
AR Path="/3C64A0CC090" Ref="P35"  Part="1" 
AR Path="/2F4A4A0CC090" Ref="P35"  Part="1" 
AR Path="/D1C3384A0CC090" Ref="P35"  Part="1" 
F 0 "P35" H 38880 27400 40  0000 C CNN
F 1 "CONN_1" H 38750 27440 30  0001 C CNN
F 2 "1pin" H 38800 27400 60  0001 C CNN
F 3 "" H 38800 27400 60  0001 C CNN
	1    38800 27400
	1    0    0    -1  
$EndComp
$Comp
L S100_MALE P1
U 1 1 4A06E881
P 42750 11600
AR Path="/4A06E881" Ref="P1"  Part="1" 
AR Path="/94A06E881" Ref="P1"  Part="1" 
AR Path="/6FF0DD404A06E881" Ref="P1"  Part="1" 
AR Path="/14A06E881" Ref="P1"  Part="1" 
AR Path="/23D6E04A06E881" Ref="P1"  Part="1" 
AR Path="/5AD7153D4A06E881" Ref="P1"  Part="1" 
AR Path="/A4A06E881" Ref="P1"  Part="1" 
AR Path="/755D912A4A06E881" Ref="P1"  Part="1" 
AR Path="/3FE08B434A06E881" Ref="P1"  Part="1" 
AR Path="/3DA360004A06E881" Ref="P1"  Part="1" 
AR Path="/3FEFFFFF4A06E881" Ref="P1"  Part="1" 
AR Path="/23D8D44A06E881" Ref="P1"  Part="1" 
AR Path="/3D8EA0004A06E881" Ref="P1"  Part="1" 
AR Path="/402B08B44A06E881" Ref="P1"  Part="1" 
AR Path="/4026A24D4A06E881" Ref="P1"  Part="1" 
AR Path="/3FE88B434A06E881" Ref="P1"  Part="1" 
AR Path="/3D7E00004A06E881" Ref="P1"  Part="1" 
AR Path="/402988B44A06E881" Ref="P1"  Part="1" 
AR Path="/3D6CC0004A06E881" Ref="P1"  Part="1" 
AR Path="/FFFFFFF04A06E881" Ref="P1"  Part="1" 
AR Path="/23C2344A06E881" Ref="P1"  Part="1" 
AR Path="/DCBAABCD4A06E881" Ref="P1"  Part="1" 
AR Path="/5AD746F64A06E881" Ref="P1"  Part="1" 
AR Path="/4027EF1A4A06E881" Ref="P1"  Part="1" 
AR Path="/402A6F1A4A06E881" Ref="P1"  Part="1" 
AR Path="/4029BBE74A06E881" Ref="P1"  Part="1" 
AR Path="/402A224D4A06E881" Ref="P1"  Part="1" 
AR Path="/402A55814A06E881" Ref="P1"  Part="1" 
AR Path="/4026224D4A06E881" Ref="P1"  Part="1" 
AR Path="/3D6220004A06E881" Ref="P1"  Part="1" 
AR Path="/3D9720004A06E881" Ref="P1"  Part="1" 
AR Path="/2600004A06E881" Ref="P1"  Part="1" 
AR Path="/402C3BE74A06E881" Ref="P1"  Part="1" 
AR Path="/402955814A06E881" Ref="P1"  Part="1" 
AR Path="/40293BE74A06E881" Ref="P1"  Part="1" 
AR Path="/402555814A06E881" Ref="P1"  Part="1" 
AR Path="/3FED58104A06E881" Ref="P1"  Part="1" 
AR Path="/402688B44A06E881" Ref="P1"  Part="1" 
AR Path="/3FE441894A06E881" Ref="P1"  Part="1" 
AR Path="/B84A06E881" Ref="P1"  Part="1" 
AR Path="/990A17DE4A06E881" Ref="P1"  Part="1" 
AR Path="/4029D5814A06E881" Ref="P1"  Part="1" 
AR Path="/40256F1A4A06E881" Ref="P1"  Part="1" 
AR Path="/40286F1A4A06E881" Ref="P1"  Part="1" 
AR Path="/402A08B44A06E881" Ref="P1"  Part="1" 
AR Path="/402588B44A06E881" Ref="P1"  Part="1" 
AR Path="/A84A06E881" Ref="P1"  Part="1" 
AR Path="/3FE224DD4A06E881" Ref="P1"  Part="1" 
AR Path="/402C08B44A06E881" Ref="P1"  Part="1" 
AR Path="/402BEF1A4A06E881" Ref="P1"  Part="1" 
AR Path="/402C55814A06E881" Ref="P1"  Part="1" 
AR Path="/40273BE74A06E881" Ref="P1"  Part="1" 
AR Path="/402908B44A06E881" Ref="P1"  Part="1" 
AR Path="/3D5A40004A06E881" Ref="P1"  Part="1" 
AR Path="/402755814A06E881" Ref="P1"  Part="1" 
AR Path="/4030AAC04A06E881" Ref="P1"  Part="1" 
AR Path="/4031DDF34A06E881" Ref="P1"  Part="1" 
AR Path="/4032778D4A06E881" Ref="P1"  Part="1" 
AR Path="/403091264A06E881" Ref="P1"  Part="1" 
AR Path="/403051264A06E881" Ref="P1"  Part="1" 
AR Path="/4032F78D4A06E881" Ref="P1"  Part="1" 
AR Path="/403251264A06E881" Ref="P1"  Part="1" 
AR Path="/4032AAC04A06E881" Ref="P1"  Part="1" 
AR Path="/4030D1264A06E881" Ref="P1"  Part="1" 
AR Path="/4031778D4A06E881" Ref="P1"  Part="1" 
AR Path="/3FEA24DD4A06E881" Ref="P1"  Part="1" 
AR Path="/6FE934E34A06E881" Ref="P1"  Part="1" 
AR Path="/773F65F14A06E881" Ref="P1"  Part="1" 
AR Path="/773F8EB44A06E881" Ref="P1"  Part="1" 
AR Path="/23C9F04A06E881" Ref="P1"  Part="1" 
AR Path="/24A06E881" Ref="P1"  Part="1" 
AR Path="/23BC884A06E881" Ref="P1"  Part="1" 
AR Path="/DC0C124A06E881" Ref="P1"  Part="1" 
AR Path="/23C34C4A06E881" Ref="P1"  Part="1" 
AR Path="/23CBC44A06E881" Ref="P1"  Part="1" 
AR Path="/23C6504A06E881" Ref="P1"  Part="1" 
AR Path="/39803EA4A06E881" Ref="P1"  Part="1" 
AR Path="/FFFFFFFF4A06E881" Ref="P1"  Part="1" 
AR Path="/6FE934344A06E881" Ref="P1"  Part="1" 
AR Path="/7E0073254A06E881" Ref="P1"  Part="1" 
AR Path="/8CEE4A06E881" Ref="P1"  Part="1" 
AR Path="/23D9004A06E881" Ref="P1"  Part="1" 
AR Path="/91964A06E881" Ref="P1"  Part="1" 
AR Path="/77F16B254A06E881" Ref="P1"  Part="1" 
AR Path="/373041344A06E881" Ref="P1"  Part="1" 
AR Path="/23D83C4A06E881" Ref="P1"  Part="1" 
AR Path="/8D384A06E881" Ref="P1"  Part="1" 
AR Path="/D058E04A06E881" Ref="P1"  Part="1" 
AR Path="/3C64A06E881" Ref="P1"  Part="1" 
AR Path="/2F4A4A06E881" Ref="P1"  Part="1" 
AR Path="/D1C3384A06E881" Ref="P1"  Part="1" 
F 0 "P1" H 42750 16650 60  0000 C CNN
F 1 "S100_MALE" V 43150 11600 60  0000 C CNN
F 2 "S100_MALE" H 42750 11600 60  0001 C CNN
F 3 "" H 42750 11600 60  0001 C CNN
	1    42750 11600
	1    0    0    -1  
$EndComp
Text Label 14650 2800 0    60   ~ 0
VCC
Text Label 11500 13000 0    60   ~ 0
GND
Text Label 12050 11450 0    60   ~ 0
GND
$Comp
L CONN_4 P8
U 1 1 5275FFB2
P 11800 12450
F 0 "P8" V 11750 12450 50  0000 C CNN
F 1 "CONN_4" V 11850 12450 50  0000 C CNN
F 2 "SIL-4" V 11950 12450 50  0001 C CNN
F 3 "" H 11800 12450 60  0001 C CNN
	1    11800 12450
	1    0    0    -1  
$EndComp
$Comp
L CONN_4 P10
U 1 1 5275FFBF
P 12150 12450
F 0 "P10" V 12100 12450 50  0000 C CNN
F 1 "CONN_4" V 12200 12450 50  0000 C CNN
F 2 "SIL-4" V 12300 12450 50  0001 C CNN
F 3 "" H 12150 12450 60  0001 C CNN
	1    12150 12450
	-1   0    0    -1  
$EndComp
$Comp
L CONN_4 P9
U 1 1 5275FFD8
P 11900 11900
F 0 "P9" V 11850 11900 50  0000 C CNN
F 1 "CONN_4" V 11950 11900 50  0000 C CNN
F 2 "SIL-4" V 12050 11900 50  0001 C CNN
F 3 "" H 11900 11900 60  0001 C CNN
	1    11900 11900
	-1   0    0    -1  
$EndComp
Text Label 11250 11600 0    60   ~ 0
GND
$Comp
L LED D8
U 1 1 527650CF
P 13250 25600
F 0 "D8" H 13250 25700 50  0000 C CNN
F 1 "LED" H 13250 25500 50  0000 C CNN
F 2 "LEDV" H 13250 25600 50  0001 C CNN
F 3 "" H 13250 25600 60  0001 C CNN
	1    13250 25600
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 527650D5
P 13700 25600
F 0 "R2" V 13780 25600 50  0000 C CNN
F 1 "220" V 13700 25600 50  0000 C CNN
F 2 "R3" V 13800 25600 50  0001 C CNN
F 3 "" H 13700 25600 60  0001 C CNN
	1    13700 25600
	0    -1   -1   0   
$EndComp
Text Label 14000 25600 0    60   ~ 0
GND
Text Notes 13150 25850 0    60   ~ 0
Z80 ACTIVE
$Comp
L JUMPER JP9
U 1 1 52765551
P 14300 2800
F 0 "JP9" H 14300 2950 60  0000 C CNN
F 1 "JUMPER" H 14300 2720 40  0000 C CNN
F 2 "SIL-2" H 14300 2820 40  0001 C CNN
F 3 "" H 14300 2800 60  0001 C CNN
	1    14300 2800
	1    0    0    -1  
$EndComp
Text Label 13450 4800 0    60   ~ 0
PU5K-H
Text Label 13350 4300 0    60   ~ 0
A10
$Comp
L R R41
U 1 1 52765B5B
P 22600 24050
F 0 "R41" V 22680 24050 50  0000 C CNN
F 1 "4700" V 22600 24050 50  0000 C CNN
F 2 "R3" V 22700 24050 50  0001 C CNN
F 3 "" H 22600 24050 60  0001 C CNN
	1    22600 24050
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR041
U 1 1 52765B6A
P 22600 23800
F 0 "#PWR041" H 22600 23900 30  0001 C CNN
F 1 "VCC" H 22600 23900 30  0000 C CNN
F 2 "" H 22600 23800 60  0001 C CNN
F 3 "" H 22600 23800 60  0001 C CNN
	1    22600 23800
	1    0    0    -1  
$EndComp
$Comp
L JUMPER JP10
U 1 1 52ED38BC
P 12400 2150
F 0 "JP10" H 12400 2300 60  0000 C CNN
F 1 "JUMPER" H 12400 2070 40  0000 C CNN
F 2 "SIL-2" H 12400 2170 40  0001 C CNN
F 3 "" H 12400 2150 60  0001 C CNN
	1    12400 2150
	1    0    0    -1  
$EndComp
Text Notes 11550 2400 0    60   ~ 0
ONBOARD ROM ENABLE (INSTALL JUMPER)\nEXTERNAL ROM ENABLE (NO JUMPER)
$Comp
L CONN_3 K101
U 1 1 55F17EC0
P 8350 7900
F 0 "K101" V 8300 7900 50  0000 C CNN
F 1 "CONN_3" V 8400 7900 40  0000 C CNN
F 2 "" H 8350 7900 60  0000 C CNN
F 3 "" H 8350 7900 60  0000 C CNN
	1    8350 7900
	0    -1   -1   0   
$EndComp
$Comp
L CONN_3X2 P101
U 1 1 55F18E8D
P 12650 4300
F 0 "P101" H 12650 4550 50  0000 C CNN
F 1 "CONN_3X2" V 12650 4350 40  0000 C CNN
F 2 "" H 12650 4300 60  0000 C CNN
F 3 "" H 12650 4300 60  0000 C CNN
	1    12650 4300
	1    0    0    -1  
$EndComp
$Comp
L RR9 RR101
U 1 1 55F18E9A
P 18700 15050
F 0 "RR101" H 18750 15650 70  0000 C CNN
F 1 "1000" V 18730 15050 70  0000 C CNN
F 2 "r_pack9" V 18830 15050 70  0001 C CNN
F 3 "" H 18700 15050 60  0001 C CNN
	1    18700 15050
	1    0    0    -1  
$EndComp
Text Label 18150 14650 2    60   ~ 0
PU470-1
$Comp
L VCC #PWR042
U 1 1 55F1986D
P 18150 14500
F 0 "#PWR042" H 18150 14600 30  0001 C CNN
F 1 "VCC" H 18150 14600 30  0000 C CNN
F 2 "" H 18150 14500 60  0000 C CNN
F 3 "" H 18150 14500 60  0000 C CNN
	1    18150 14500
	1    0    0    -1  
$EndComp
Text Label 8650 8350 0    60   ~ 0
PU470-1
Text Label 26750 3650 0    60   ~ 0
DO0
Text Label 26750 3750 0    60   ~ 0
DO1
Text Label 26750 3950 0    60   ~ 0
DO3
Text Label 26750 3850 0    60   ~ 0
DO2
Text Label 26750 4250 0    60   ~ 0
DO6
Text Label 26750 4350 0    60   ~ 0
DO7
Text Label 26750 4150 0    60   ~ 0
DO5
Text Label 26750 4050 0    60   ~ 0
DO4
Text Label 32400 4050 0    60   ~ 0
DI4
Text Label 32400 4150 0    60   ~ 0
DI5
Text Label 32400 4350 0    60   ~ 0
DI7
Text Label 32400 4250 0    60   ~ 0
DI6
Text Label 32400 3850 0    60   ~ 0
DI2
Text Label 32400 3950 0    60   ~ 0
DI3
Text Label 32400 3750 0    60   ~ 0
DI1
Text Label 32400 3650 0    60   ~ 0
DI0
Text Label 28450 4350 0    60   ~ 0
PD7
Text Label 28450 4250 0    60   ~ 0
PD6
Text Label 28450 4150 0    60   ~ 0
PD5
Text Label 28450 4050 0    60   ~ 0
PD4
Text Label 28450 3950 0    60   ~ 0
PD3
Text Label 28450 3850 0    60   ~ 0
PD2
Text Label 28450 3750 0    60   ~ 0
PD1
Text Label 28450 3650 0    60   ~ 0
PD0
$Comp
L 74LS04 U25
U 1 1 55F0B585
P 23150 3650
F 0 "U25" H 23345 3765 60  0000 C CNN
F 1 "74LS04" H 23340 3525 60  0000 C CNN
F 2 "14dip300" H 23150 3650 60  0001 C CNN
F 3 "" H 23150 3650 60  0001 C CNN
	1    23150 3650
	1    0    0    -1  
$EndComp
Text Label 26300 2450 0    60   ~ 0
INPUT_ENABLE*
Text Label 23250 2350 0    60   ~ 0
BUS_SELECT*
NoConn ~ 25950 19850
$Comp
L GND #PWR043
U 1 1 55F0B709
P 24550 21450
F 0 "#PWR043" H 24550 21450 30  0001 C CNN
F 1 "GND" H 24550 21380 30  0001 C CNN
F 2 "" H 24550 21450 60  0001 C CNN
F 3 "" H 24550 21450 60  0001 C CNN
	1    24550 21450
	1    0    0    -1  
$EndComp
$Comp
L DIPS_08 SW101
U 1 1 55F0B710
P 24750 20900
F 0 "SW101" V 24300 20900 60  0000 C CNN
F 1 "DIPS_08" V 25200 20900 60  0000 C CNN
F 2 "SWDIP8" H 24750 20900 60  0001 C CNN
F 3 "" H 24750 20900 60  0001 C CNN
	1    24750 20900
	0    1    1    0   
$EndComp
Text Label 24400 19650 0    60   ~ 0
bA7
Text Label 24400 19750 0    60   ~ 0
bA6
Text Label 24400 19950 0    60   ~ 0
bA4
Text Label 24400 19850 0    60   ~ 0
bA5
Text Label 24400 20050 0    60   ~ 0
bA3
$Comp
L 74LS682N IC101
U 1 1 55F0B71D
P 25450 20450
F 0 "IC101" H 25150 21375 50  0000 L BNN
F 1 "74LS682N" H 25150 19450 50  0000 L BNN
F 2 "20dip300" H 25450 20600 50  0001 C CNN
F 3 "" H 25450 20450 60  0001 C CNN
	1    25450 20450
	1    0    0    -1  
$EndComp
$Comp
L 74LS00 U104
U 1 1 55F0B75E
P 24200 3550
F 0 "U104" H 24200 3600 60  0000 C CNN
F 1 "74LS00" H 24200 3500 60  0000 C CNN
F 2 "14dip300" H 24200 3550 60  0001 C CNN
F 3 "" H 24200 3550 60  0001 C CNN
	1    24200 3550
	1    0    0    -1  
$EndComp
Text Label 24050 3050 0    60   ~ 0
S100_RD*
Text Label 22050 3650 0    60   ~ 0
bpWR*
Text Label 22050 3350 0    60   ~ 0
bsOUT
Text Label 22050 3000 0    60   ~ 0
bsINP
Text Label 22050 2800 0    60   ~ 0
bpDBIN
$Comp
L 74LS00 U28
U 4 1 55F0B771
P 23200 2900
F 0 "U28" H 23200 2950 60  0000 C CNN
F 1 "74LS00" H 23200 2850 60  0000 C CNN
F 2 "14dip300" H 23200 2900 60  0001 C CNN
F 3 "" H 23200 2900 60  0001 C CNN
	4    23200 2900
	1    0    0    -1  
$EndComp
Text Label 21950 22650 0    60   ~ 0
bpWR*
Text Label 21950 22400 0    60   ~ 0
bpDBIN
Text Label 10950 16600 0    60   ~ 0
bsOUT
Text Label 10950 16750 0    60   ~ 0
bsINP
NoConn ~ 18350 15450
NoConn ~ 38650 27400
NoConn ~ 38650 27500
$Comp
L 628128 U111
U 1 1 55F0E78C
P 10600 6500
F 0 "U111" H 10650 6500 70  0000 C CNN
F 1 "628128" H 10900 5300 70  0000 C CNN
F 2 "" H 10600 6500 60  0000 C CNN
F 3 "" H 10600 6500 60  0000 C CNN
	1    10600 6500
	1    0    0    -1  
$EndComp
Text Label 9750 6950 2    60   ~ 0
bA15
Text Label 9750 5450 2    60   ~ 0
bA0
Text Label 9750 5550 2    60   ~ 0
bA1
Text Label 9750 5650 2    60   ~ 0
bA2
Text Label 9750 5750 2    60   ~ 0
bA3
Text Label 9750 5850 2    60   ~ 0
bA4
Text Label 9750 5950 2    60   ~ 0
bA5
Text Label 9750 6050 2    60   ~ 0
bA6
Text Label 9750 6150 2    60   ~ 0
bA7
Text Label 9750 6250 2    60   ~ 0
bA8
Text Label 9750 6350 2    60   ~ 0
bA9
Text Label 9750 6450 2    60   ~ 0
bA10
Text Label 9750 6550 2    60   ~ 0
bA11
Text Label 9750 6650 2    60   ~ 0
bA12
Text Label 9750 6750 2    60   ~ 0
bA13
Text Label 9750 6850 2    60   ~ 0
bA14
$Comp
L CONN_3 K105
U 1 1 55F0E79E
P 8950 7050
F 0 "K105" V 8900 7050 50  0000 C CNN
F 1 "CONN_3" V 9000 7050 40  0000 C CNN
F 2 "" H 8950 7050 60  0000 C CNN
F 3 "" H 8950 7050 60  0000 C CNN
	1    8950 7050
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR044
U 1 1 55F0EE1C
P 9300 7300
F 0 "#PWR044" H 9300 7300 30  0001 C CNN
F 1 "GND" H 9300 7230 30  0001 C CNN
F 2 "" H 9300 7300 60  0000 C CNN
F 3 "" H 9300 7300 60  0000 C CNN
	1    9300 7300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR045
U 1 1 55F0FB1B
P 9750 7300
F 0 "#PWR045" H 9750 7300 30  0001 C CNN
F 1 "GND" H 9750 7230 30  0001 C CNN
F 2 "" H 9750 7300 60  0000 C CNN
F 3 "" H 9750 7300 60  0000 C CNN
	1    9750 7300
	1    0    0    -1  
$EndComp
Text Notes 10850 5200 2    60   ~ 0
128K RAM\n(use 64Kx2)
Text Label 14050 9800 2    60   ~ 0
bA8
Text Label 14050 9900 2    60   ~ 0
bA9
Text Label 14050 10000 2    60   ~ 0
bA10
Text Label 14050 10100 2    60   ~ 0
bA11
Text Label 14050 10200 2    60   ~ 0
bA12
Text Label 14050 10300 2    60   ~ 0
bA13
Text Label 14050 10400 2    60   ~ 0
bA14
Text Label 14050 10500 2    60   ~ 0
bA15
$Comp
L 74LS373 U?
U 1 1 4BB29CDE
P 6150 9650
AR Path="/23D62C4BB29CDE" Ref="U?"  Part="1" 
AR Path="/679E884BB29CDE" Ref="U?"  Part="1" 
AR Path="/2606084BB29CDE" Ref="U2"  Part="1" 
AR Path="/394344454BB29CDE" Ref="U2"  Part="1" 
AR Path="/4BB29CDE" Ref="U2"  Part="1" 
AR Path="/FFFFFFFF4BB29CDE" Ref="U2"  Part="1" 
AR Path="/755D912A4BB29CDE" Ref="U2"  Part="1" 
AR Path="/23D83C4BB29CDE" Ref="U2"  Part="1" 
AR Path="/47907FA4BB29CDE" Ref="U2"  Part="1" 
AR Path="/23C34C4BB29CDE" Ref="U2"  Part="1" 
AR Path="/14BB29CDE" Ref="U2"  Part="1" 
AR Path="/23BC884BB29CDE" Ref="U2"  Part="1" 
AR Path="/773F8EB44BB29CDE" Ref="U2"  Part="1" 
AR Path="/94BB29CDE" Ref="U"  Part="1" 
AR Path="/23C9F04BB29CDE" Ref="U2"  Part="1" 
AR Path="/FFFFFFF04BB29CDE" Ref="U2"  Part="1" 
AR Path="/7E4188DA4BB29CDE" Ref="U2"  Part="1" 
AR Path="/8D384BB29CDE" Ref="U2"  Part="1" 
AR Path="/24BB29CDE" Ref="U2"  Part="1" 
AR Path="/773F65F14BB29CDE" Ref="U2"  Part="1" 
AR Path="/23C6504BB29CDE" Ref="U2"  Part="1" 
AR Path="/D058E04BB29CDE" Ref="U2"  Part="1" 
AR Path="/3C64BB29CDE" Ref="U2"  Part="1" 
AR Path="/23CBC44BB29CDE" Ref="U2"  Part="1" 
AR Path="/2F4A4BB29CDE" Ref="U2"  Part="1" 
AR Path="/23D9004BB29CDE" Ref="U2"  Part="1" 
AR Path="/64BB29CDE" Ref="U2"  Part="1" 
AR Path="/6FE934344BB29CDE" Ref="U2"  Part="1" 
AR Path="/6FE934E34BB29CDE" Ref="U2"  Part="1" 
AR Path="/2824BB29CDE" Ref="U2"  Part="1" 
AR Path="/D1C3384BB29CDE" Ref="U2"  Part="1" 
F 0 "U2" H 6150 9650 60  0000 C CNN
F 1 "74LS373" H 6200 9300 60  0000 C CNN
F 2 "20dip300" H 6200 9400 60  0001 C CNN
F 3 "" H 6150 9650 60  0001 C CNN
	1    6150 9650
	-1   0    0    -1  
$EndComp
Text Label 7000 9150 0    60   ~ 0
bA0
Text Label 7000 9250 0    60   ~ 0
bA1
Text Label 7000 9350 0    60   ~ 0
bA2
Text Label 7000 9450 0    60   ~ 0
bA3
Text Label 7000 9550 0    60   ~ 0
bA4
Text Label 7000 9650 0    60   ~ 0
bA5
Text Label 7000 9750 0    60   ~ 0
bA6
Text Label 7000 9850 0    60   ~ 0
bA7
Text Label 7050 10750 0    60   ~ 0
bA8
Text Label 7050 10850 0    60   ~ 0
bA9
Text Label 7050 10950 0    60   ~ 0
bA10
Text Label 7050 11050 0    60   ~ 0
bA11
Text Label 7050 11150 0    60   ~ 0
bA12
Text Label 7050 11250 0    60   ~ 0
bA13
Text Label 7050 11350 0    60   ~ 0
bA14
Text Label 7050 11450 0    60   ~ 0
bA15
Text Label 5250 9150 0    60   ~ 0
A0
Text Label 5250 9250 0    60   ~ 0
A1
Text Label 5250 9350 0    60   ~ 0
A2
Text Label 5250 9450 0    60   ~ 0
A3
Text Label 5250 9550 0    60   ~ 0
A4
Text Label 5250 9650 0    60   ~ 0
A5
Text Label 5250 9750 0    60   ~ 0
A6
Text Label 5250 9850 0    60   ~ 0
A7
Text Label 8850 10700 0    60   ~ 0
PARTIAL_LATCH
$Comp
L CONN_3 K106
U 1 1 55F36929
P 10300 8300
F 0 "K106" V 10250 8300 50  0000 C CNN
F 1 "CONN_3" V 10350 8300 40  0000 C CNN
F 2 "" H 10300 8300 60  0000 C CNN
F 3 "" H 10300 8300 60  0000 C CNN
	1    10300 8300
	0    -1   -1   0   
$EndComp
Text Label 9950 8700 2    60   ~ 0
PU470-2
Text Label 11850 8750 0    60   ~ 0
ROM_SELECT
$Comp
L CONN_3 K107
U 1 1 55F3A56E
P 17800 8850
F 0 "K107" V 17750 8850 50  0000 C CNN
F 1 "CONN_3" V 17850 8850 40  0000 C CNN
F 2 "" H 17800 8850 60  0000 C CNN
F 3 "" H 17800 8850 60  0000 C CNN
	1    17800 8850
	-1   0    0    1   
$EndComp
Text Label 18100 14750 2    60   ~ 0
PU470-2
Text Label 18100 14850 2    60   ~ 0
PU470-3
Text Label 12000 8550 0    60   ~ 0
MREQ*
Text Label 17700 8550 2    60   ~ 0
PU470-3
Text Label 24200 24450 0    60   ~ 0
GND
$Comp
L 74LS27 U41
U 3 1 55F11418
P 25450 24450
F 0 "U41" H 25450 24500 60  0000 C CNN
F 1 "74LS27" H 25450 24400 60  0000 C CNN
F 2 "14dip300" H 25450 24450 60  0001 C CNN
F 3 "" H 25450 24450 60  0001 C CNN
	3    25450 24450
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR046
U 1 1 55F11434
P 25850 25400
F 0 "#PWR046" H 25850 25500 30  0001 C CNN
F 1 "VCC" H 25850 25500 30  0000 C CNN
F 2 "" H 25850 25400 60  0001 C CNN
F 3 "" H 25850 25400 60  0001 C CNN
	1    25850 25400
	1    0    0    -1  
$EndComp
Text Label 24200 24300 0    60   ~ 0
s100_WR*
Text Label 24200 25200 0    60   ~ 0
S100_RD*
Text Label 24700 24800 2    60   ~ 0
BASE_PORT_(4+5)*
$Comp
L 74LS139 U113
U 1 1 55F1100D
P 27150 19100
F 0 "U113" H 27150 19200 60  0000 C CNN
F 1 "74LS139" H 27150 19000 60  0000 C CNN
F 2 "~" H 27150 19100 60  0000 C CNN
F 3 "~" H 27150 19100 60  0000 C CNN
	1    27150 19100
	1    0    0    -1  
$EndComp
Text Label 25900 19000 2    60   ~ 0
bA1
Text Label 25900 18850 2    60   ~ 0
bA2
$Comp
L 74LS08 U115
U 1 1 55F1533D
P 26850 23950
F 0 "U115" H 26850 24000 60  0000 C CNN
F 1 "74LS08" H 26850 23900 60  0000 C CNN
F 2 "~" H 26850 23950 60  0000 C CNN
F 3 "~" H 26850 23950 60  0000 C CNN
	1    26850 23950
	1    0    0    -1  
$EndComp
Text Label 28150 18800 0    60   ~ 0
BASE_PORT_(0+1)*
Text Label 28100 19000 0    60   ~ 0
BASE_PORT_(2+3)*
Text Label 25000 3850 0    60   ~ 0
S100_WR*
Text Label 28100 19200 0    60   ~ 0
BASE_PORT_(4+5)*
$Comp
L LED D103
U 1 1 55F11424
P 27950 23950
F 0 "D103" H 27950 24050 50  0000 C CNN
F 1 "LED" H 27950 23850 50  0000 C CNN
F 2 "LEDV" H 27950 23950 60  0001 C CNN
F 3 "" H 27950 23950 60  0001 C CNN
	1    27950 23950
	-1   0    0    1   
$EndComp
Text Notes 27500 23750 0    60   ~ 0
USB PORT SELECT
Text Label 17950 15350 2    60   ~ 0
PU470_8
Text Label 28300 23950 0    60   ~ 0
PU470_8
$Comp
L 74LS04 U114
U 1 1 55F249F9
P 25450 23850
F 0 "U114" H 25645 23965 60  0000 C CNN
F 1 "74LS04" H 25640 23725 60  0000 C CNN
F 2 "~" H 25450 23850 60  0000 C CNN
F 3 "~" H 25450 23850 60  0000 C CNN
	1    25450 23850
	1    0    0    -1  
$EndComp
Text Label 39350 26850 0    60   ~ 0
0V
$Comp
L GND #PWR047
U 1 1 55F131DA
P 39600 26900
F 0 "#PWR047" H 39600 26900 30  0001 C CNN
F 1 "GND" H 39600 26830 30  0001 C CNN
F 2 "" H 39600 26900 60  0001 C CNN
F 3 "" H 39600 26900 60  0001 C CNN
	1    39600 26900
	1    0    0    -1  
$EndComp
Text Label 37700 26450 0    60   ~ 0
+8V
Text Label 39300 26450 0    60   ~ 0
+5V
$Comp
L CP C105
U 1 1 55F131E2
P 39100 26650
F 0 "C105" H 39150 26750 50  0000 L CNN
F 1 "1 uF" H 39150 26550 50  0000 L CNN
F 2 "C1V5" H 39100 26650 60  0001 C CNN
F 3 "" H 39100 26650 60  0001 C CNN
	1    39100 26650
	1    0    0    -1  
$EndComp
$Comp
L CP C102
U 1 1 55F131E8
P 38200 26650
F 0 "C102" H 38250 26750 50  0000 L CNN
F 1 "33 uF" H 38250 26550 50  0000 L CNN
F 2 "C1V5" H 38200 26650 60  0001 C CNN
F 3 "" H 38200 26650 60  0001 C CNN
	1    38200 26650
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG048
U 1 1 55F131EE
P 39600 26850
F 0 "#FLG048" H 39600 27120 30  0001 C CNN
F 1 "PWR_FLAG" H 39600 27080 30  0000 C CNN
F 2 "" H 39600 26850 60  0001 C CNN
F 3 "" H 39600 26850 60  0001 C CNN
	1    39600 26850
	1    0    0    -1  
$EndComp
$Comp
L CONN_5 VReg-101
U 1 1 55F131FA
P 38650 26000
F 0 "VReg-101" V 38600 26000 50  0000 C CNN
F 1 "D24V25F5" V 38700 26000 50  0000 C CNN
F 2 "" H 38650 26000 60  0000 C CNN
F 3 "" H 38650 26000 60  0000 C CNN
	1    38650 26000
	0    -1   -1   0   
$EndComp
Text Notes 38400 25700 0    60   ~ 0
Palolu 5V 2.5A\nRegulator 
$Comp
L PWR_FLAG #FLG049
U 1 1 55F12B55
P 40150 26250
F 0 "#FLG049" H 40150 26345 30  0001 C CNN
F 1 "PWR_FLAG" H 40150 26430 30  0000 C CNN
F 2 "" H 40150 26250 60  0000 C CNN
F 3 "" H 40150 26250 60  0000 C CNN
	1    40150 26250
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG050
U 1 1 55F16124
P 38000 26100
F 0 "#FLG050" H 38000 26195 30  0001 C CNN
F 1 "PWR_FLAG" H 38000 26280 30  0000 C CNN
F 2 "" H 38000 26100 60  0000 C CNN
F 3 "" H 38000 26100 60  0000 C CNN
	1    38000 26100
	1    0    0    -1  
$EndComp
NoConn ~ 26750 25700
Text Label 26500 25600 0    60   ~ 0
VCC
Text Label 26500 25500 0    60   ~ 0
VCC
$Comp
L R R101
U 1 1 55F144B7
P 26100 25400
F 0 "R101" V 26180 25400 50  0000 C CNN
F 1 "10K" V 26100 25400 50  0000 C CNN
F 2 "R3" H 26100 25400 60  0001 C CNN
F 3 "" H 26100 25400 60  0001 C CNN
	1    26100 25400
	0    1    1    0   
$EndComp
Text Label 27500 25400 0    60   ~ 0
PD6
Text Label 26500 25200 0    60   ~ 0
PD5
Text Label 27500 25000 0    60   ~ 0
PD4
Text Label 26500 25300 0    60   ~ 0
PD3
Text Label 26500 25000 0    60   ~ 0
PD2
Text Label 27500 25100 0    60   ~ 0
PD1
Text Label 27500 24900 0    60   ~ 0
PD0
Text Label 26500 25100 0    60   ~ 0
PD7
Text Label 27500 25700 0    60   ~ 0
GND
Text Label 26500 24900 0    60   ~ 0
GND
Text Label 27500 25200 0    60   ~ 0
PC7
Text Label 27500 25300 0    60   ~ 0
PC6
Text Notes 26700 24650 0    60   ~ 0
DLP-USB1232H
$Comp
L DIL18 P103
U 1 1 55F144C9
P 27100 25300
F 0 "P103" H 27100 25850 70  0000 C CNN
F 1 "DIL18" H 27100 24750 70  0000 C CNN
F 2 "18dip300" H 27100 25300 60  0001 C CNN
F 3 "" H 27100 25300 60  0001 C CNN
	1    27100 25300
	1    0    0    -1  
$EndComp
NoConn ~ 38450 26400
$Comp
L 74LS32 U105
U 1 1 55F1EA25
P 11100 8650
F 0 "U105" H 11100 8700 60  0000 C CNN
F 1 "74LS32" H 11100 8600 60  0000 C CNN
F 2 "~" H 11100 8650 60  0000 C CNN
F 3 "~" H 11100 8650 60  0000 C CNN
	1    11100 8650
	-1   0    0    1   
$EndComp
$Comp
L 74LS32 U105
U 2 1 55F1EA34
P 25450 25100
F 0 "U105" H 25450 25150 60  0000 C CNN
F 1 "74LS32" H 25450 25050 60  0000 C CNN
F 2 "~" H 25450 25100 60  0000 C CNN
F 3 "~" H 25450 25100 60  0000 C CNN
	2    25450 25100
	1    0    0    -1  
$EndComp
$Comp
L 74LS32 U105
U 3 1 55F1EA43
P 25650 2900
F 0 "U105" H 25650 2950 60  0000 C CNN
F 1 "74LS32" H 25650 2850 60  0000 C CNN
F 2 "~" H 25650 2900 60  0000 C CNN
F 3 "~" H 25650 2900 60  0000 C CNN
	3    25650 2900
	1    0    0    -1  
$EndComp
$Comp
L 74LS32 U105
U 4 1 55F1EA52
P 25650 3450
F 0 "U105" H 25650 3500 60  0000 C CNN
F 1 "74LS32" H 25650 3400 60  0000 C CNN
F 2 "~" H 25650 3450 60  0000 C CNN
F 3 "~" H 25650 3450 60  0000 C CNN
	4    25650 3450
	1    0    0    -1  
$EndComp
Text Label 28100 19400 0    60   ~ 0
BASE_PORT_(6+7)*
Text Notes 27400 26050 2    60   ~ 0
Note Chip is\nplaced with \nPin 1 on Lower RHS
$Comp
L CONN_8X2 P113
U 1 1 55F54DE3
P 12950 29400
F 0 "P113" H 12950 29850 60  0000 C CNN
F 1 "CONN_8X2" V 12950 29400 50  0000 C CNN
F 2 "" H 12950 29400 60  0000 C CNN
F 3 "" H 12950 29400 60  0000 C CNN
	1    12950 29400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR051
U 1 1 55F57F96
P 12750 30000
F 0 "#PWR051" H 12750 30000 30  0001 C CNN
F 1 "GND" H 12750 29930 30  0001 C CNN
F 2 "" H 12750 30000 60  0000 C CNN
F 3 "" H 12750 30000 60  0000 C CNN
	1    12750 30000
	1    0    0    -1  
$EndComp
$Comp
L CONN_8X2 P112
U 1 1 55F5865C
P 5900 29400
F 0 "P112" H 5900 29850 60  0000 C CNN
F 1 "CONN_8X2" V 5900 29400 50  0000 C CNN
F 2 "" H 5900 29400 60  0000 C CNN
F 3 "" H 5900 29400 60  0000 C CNN
	1    5900 29400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR052
U 1 1 55F58669
P 5700 30000
F 0 "#PWR052" H 5700 30000 30  0001 C CNN
F 1 "GND" H 5700 29930 30  0001 C CNN
F 2 "" H 5700 30000 60  0000 C CNN
F 3 "" H 5700 30000 60  0000 C CNN
	1    5700 30000
	1    0    0    -1  
$EndComp
NoConn ~ 18350 15050
$Comp
L CONN_8X2 P114
U 1 1 55F5EB73
P 9350 29400
F 0 "P114" H 9350 29850 60  0000 C CNN
F 1 "CONN_8X2" V 9350 29400 50  0000 C CNN
F 2 "" H 9350 29400 60  0000 C CNN
F 3 "" H 9350 29400 60  0000 C CNN
	1    9350 29400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR053
U 1 1 55F5EB80
P 9150 30000
F 0 "#PWR053" H 9150 30000 30  0001 C CNN
F 1 "GND" H 9150 29930 30  0001 C CNN
F 2 "" H 9150 30000 60  0000 C CNN
F 3 "" H 9150 30000 60  0000 C CNN
	1    9150 30000
	1    0    0    -1  
$EndComp
$Comp
L C C106
U 1 1 55F5EC44
P 38750 21650
F 0 "C106" H 38800 21750 50  0000 L CNN
F 1 "0.1 uF" H 38800 21550 50  0000 L CNN
F 2 "C2" H 38750 21650 60  0001 C CNN
F 3 "" H 38750 21650 60  0001 C CNN
	1    38750 21650
	1    0    0    -1  
$EndComp
$Comp
L C C107
U 1 1 55F5EC56
P 39200 21650
F 0 "C107" H 39250 21750 50  0000 L CNN
F 1 "0.1 uF" H 39250 21550 50  0000 L CNN
F 2 "C2" H 39200 21650 60  0001 C CNN
F 3 "" H 39200 21650 60  0001 C CNN
	1    39200 21650
	1    0    0    -1  
$EndComp
$Comp
L DIPS_08 SW102
U 1 1 55F5B46E
P 29250 24100
F 0 "SW102" V 28800 24100 60  0000 C CNN
F 1 "DIPS_08" V 29750 24150 60  0000 C CNN
F 2 "16dip300" V 29800 24100 60  0001 C CNN
F 3 "" H 29250 24100 60  0001 C CNN
	1    29250 24100
	0    1    1    0   
$EndComp
$Comp
L GND #PWR054
U 1 1 55F5B474
P 29050 24600
F 0 "#PWR054" H 29050 24600 30  0001 C CNN
F 1 "GND" H 29050 24530 30  0001 C CNN
F 2 "" H 29050 24600 60  0001 C CNN
F 3 "" H 29050 24600 60  0001 C CNN
	1    29050 24600
	1    0    0    -1  
$EndComp
Text Notes 31400 24950 0    60   ~ 0
IOBYTE PORT 
Text Label 32400 24450 0    60   ~ 0
DI0
Text Label 32400 24350 0    60   ~ 0
DI1
Text Label 32400 24250 0    60   ~ 0
DI2
Text Label 32400 24150 0    60   ~ 0
DI3
Text Label 32400 24050 0    60   ~ 0
DI4
Text Label 32400 23950 0    60   ~ 0
DI5
Text Label 32400 23850 0    60   ~ 0
DI6
Text Label 32400 23750 0    60   ~ 0
DI7
$Comp
L 74LS241 U110
U 1 1 55F5B483
P 31650 24250
F 0 "U110" H 31700 24050 60  0000 C CNN
F 1 "74LS244" H 31750 23850 60  0000 C CNN
F 2 "20dip300" H 31750 23950 60  0001 C CNN
F 3 "" H 31650 24250 60  0001 C CNN
	1    31650 24250
	1    0    0    -1  
$EndComp
Text Label 29400 25050 2    60   ~ 0
BASE_PORT_(6+7)*
$Comp
L 74LS32 U102
U 1 1 55F5C857
P 30150 25150
F 0 "U102" H 30150 25200 60  0000 C CNN
F 1 "74LS32" H 30150 25100 60  0000 C CNN
F 2 "14dip300" H 30150 25150 60  0001 C CNN
F 3 "" H 30150 25150 60  0001 C CNN
	1    30150 25150
	1    0    0    -1  
$EndComp
Text Label 29400 25250 2    60   ~ 0
S100_RD*
NoConn ~ 29450 23850
NoConn ~ 29450 23750
Text Label 30500 23850 2    60   ~ 0
PC6
Text Label 30500 23750 2    60   ~ 0
PC7
$Comp
L CONN_3 K103
U 1 1 56039868
P 20500 8850
F 0 "K103" V 20450 8850 50  0000 C CNN
F 1 "CONN_3" V 20550 8850 40  0000 C CNN
F 2 "" H 20500 8850 60  0000 C CNN
F 3 "" H 20500 8850 60  0000 C CNN
	1    20500 8850
	1    0    0    -1  
$EndComp
Text Label 20150 8400 0    60   ~ 0
MREQ*
$Comp
L RR8 RR103
U 1 1 5603B535
P 31250 22900
F 0 "RR103" H 31300 23450 70  0000 C CNN
F 1 "1K" V 31280 22900 70  0000 C CNN
F 2 "~" H 31250 22900 60  0000 C CNN
F 3 "~" H 31250 22900 60  0000 C CNN
	1    31250 22900
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR055
U 1 1 56042675
P 30550 22450
F 0 "#PWR055" H 30550 22550 30  0001 C CNN
F 1 "VCC" H 30550 22550 30  0000 C CNN
F 2 "" H 30550 22450 60  0000 C CNN
F 3 "" H 30550 22450 60  0000 C CNN
	1    30550 22450
	1    0    0    -1  
$EndComp
NoConn ~ 18350 15150
NoConn ~ 18350 15250
Text Label 30800 13850 0    60   ~ 0
DRV_VCC2
$Comp
L 74LS03 U103
U 1 1 560611E4
P 27900 11500
F 0 "U103" H 27900 11550 60  0000 C CNN
F 1 "7403" H 27900 11450 60  0000 C CNN
F 2 "14dip300" H 27900 11500 60  0001 C CNN
F 3 "" H 27900 11500 60  0001 C CNN
	1    27900 11500
	0    1    1    0   
$EndComp
$Comp
L 74LS03 U103
U 2 1 560611EA
P 31850 11250
F 0 "U103" H 31850 11300 60  0000 C CNN
F 1 "7403" H 31850 11200 60  0000 C CNN
F 2 "14dip300" H 31850 11250 60  0001 C CNN
F 3 "" H 31850 11250 60  0001 C CNN
	2    31850 11250
	0    1    1    0   
$EndComp
Text Label 28450 12950 0    60   ~ 0
/RESETA
Text Label 30750 13050 0    60   ~ 0
ID8
$Comp
L R R104
U 1 1 560611F6
P 31200 14450
F 0 "R104" V 31280 14450 50  0000 C CNN
F 1 "470" V 31200 14450 50  0000 C CNN
F 2 "R3" H 31200 14450 60  0001 C CNN
F 3 "" H 31200 14450 60  0001 C CNN
	1    31200 14450
	0    1    1    0   
$EndComp
Text Label 31500 14450 0    60   ~ 0
GND
Text Label 30800 14450 0    60   ~ 0
32
Text Label 30800 14250 0    60   ~ 0
28
$Comp
L R R103
U 1 1 560611FF
P 31200 14250
F 0 "R103" V 31280 14250 50  0000 C CNN
F 1 "470" V 31200 14250 50  0000 C CNN
F 2 "R3" H 31200 14250 60  0001 C CNN
F 3 "" H 31200 14250 60  0001 C CNN
	1    31200 14250
	0    1    1    0   
$EndComp
Text Label 30800 14750 0    60   ~ 0
ICS1
Text Label 30800 14650 0    60   ~ 0
IA2
Text Label 30750 13750 0    60   ~ 0
ID15
Text Label 30750 13650 0    60   ~ 0
ID14
Text Label 30750 13550 0    60   ~ 0
ID13
Text Label 30750 13450 0    60   ~ 0
ID12
Text Label 30750 13350 0    60   ~ 0
ID11
Text Label 30750 13250 0    60   ~ 0
ID10
Text Label 30750 13150 0    60   ~ 0
ID9
Text Label 29700 14750 0    60   ~ 0
ICS0
Text Label 29700 14650 0    60   ~ 0
IA0
Text Label 29700 14550 0    60   ~ 0
IA1
Text Label 29550 14150 0    60   ~ 0
/IORDA
Text Label 29550 14050 0    60   ~ 0
/IOWRA
Text Label 29800 13750 0    60   ~ 0
ID0
Text Label 29800 13650 0    60   ~ 0
ID1
Text Label 29800 13550 0    60   ~ 0
ID2
Text Label 29800 13450 0    60   ~ 0
ID3
Text Label 29800 13350 0    60   ~ 0
ID4
Text Label 29800 13250 0    60   ~ 0
ID5
Text Label 29800 13150 0    60   ~ 0
ID6
Text Label 29850 13050 0    60   ~ 0
ID7
$Comp
L GND #PWR056
U 1 1 5606121B
P 31900 15050
F 0 "#PWR056" H 31900 15050 30  0001 C CNN
F 1 "GND" H 31900 14980 30  0001 C CNN
F 2 "" H 31900 15050 60  0001 C CNN
F 3 "" H 31900 15050 60  0001 C CNN
	1    31900 15050
	1    0    0    -1  
$EndComp
Text Notes 27750 15050 0    60   ~ 0
HARD DISK ACTIVITY
NoConn ~ 30750 14550
NoConn ~ 29950 14350
NoConn ~ 29950 14250
NoConn ~ 29950 13950
$Comp
L VCC #PWR057
U 1 1 5606122C
P 27650 14850
F 0 "#PWR057" H 27650 14950 30  0001 C CNN
F 1 "VCC" H 27650 14950 30  0000 C CNN
F 2 "" H 27650 14850 60  0001 C CNN
F 3 "" H 27650 14850 60  0001 C CNN
	1    27650 14850
	1    0    0    -1  
$EndComp
$Comp
L LED D101
U 1 1 56061232
P 28100 14850
F 0 "D101" H 27825 14900 50  0000 C CNN
F 1 "LED" H 28300 14900 50  0000 C CNN
F 2 "LEDV" H 28100 14850 60  0001 C CNN
F 3 "" H 28100 14850 60  0001 C CNN
	1    28100 14850
	1    0    0    -1  
$EndComp
$Comp
L R R102
U 1 1 56061238
P 28550 14850
F 0 "R102" V 28630 14850 50  0000 C CNN
F 1 "470" V 28550 14850 50  0000 C CNN
F 2 "R3" H 28550 14850 60  0001 C CNN
F 3 "" H 28550 14850 60  0001 C CNN
	1    28550 14850
	0    1    1    0   
$EndComp
$Comp
L CONN_20X2 P102
U 1 1 5606123E
P 30350 13900
F 0 "P102" H 30350 14950 60  0000 C CNN
F 1 "CONN_20X2" V 30350 13900 50  0000 C CNN
F 2 "PIN_ARRAY_20X2" H 30350 13900 60  0001 C CNN
F 3 "" H 30350 13900 60  0001 C CNN
	1    30350 13900
	1    0    0    -1  
$EndComp
$Comp
L 8255 U106
U 1 1 56061244
P 30350 9300
F 0 "U106" H 30350 10700 60  0000 C CNN
F 1 "8255" V 30400 9450 60  0000 C CNN
F 2 "40dip600" H 30350 9300 60  0001 C CNN
F 3 "" H 30350 9300 60  0001 C CNN
	1    30350 9300
	0    -1   1    0   
$EndComp
$Comp
L 74LS244 U107
U 1 1 5606124A
P 31700 4150
F 0 "U107" H 31750 3950 60  0000 C CNN
F 1 "74LS244" H 31800 3750 60  0000 C CNN
F 2 "20dip300" H 31700 4150 60  0001 C CNN
F 3 "" H 31700 4150 60  0001 C CNN
	1    31700 4150
	1    0    0    -1  
$EndComp
$Comp
L 74LS244 U101
U 1 1 56061250
P 27700 4150
F 0 "U101" H 27750 3950 60  0000 C CNN
F 1 "74LS244" H 27800 3750 60  0000 C CNN
F 2 "20dip300" H 27700 4150 60  0001 C CNN
F 3 "" H 27700 4150 60  0001 C CNN
	1    27700 4150
	1    0    0    -1  
$EndComp
Text Label 33650 12400 0    60   ~ 0
ICS1
Text Label 33650 12300 0    60   ~ 0
ICS0
$Comp
L VCC #PWR058
U 1 1 56061342
P 34050 11600
F 0 "#PWR058" H 34050 11700 30  0001 C CNN
F 1 "VCC" H 34050 11700 30  0000 C CNN
F 2 "" H 34050 11600 60  0001 C CNN
F 3 "" H 34050 11600 60  0001 C CNN
	1    34050 11600
	1    0    0    -1  
$EndComp
Text Label 33650 11800 0    60   ~ 0
/IORDA
Text Label 33650 12000 0    60   ~ 0
/IOWRA
Text Label 33650 12200 0    60   ~ 0
/RESETA
$Comp
L RR9 RR102
U 1 1 5606134E
P 34400 12100
F 0 "RR102" H 34450 12700 70  0000 C CNN
F 1 "680" V 34430 12100 70  0000 C CNN
F 2 "r_pack9" H 34400 12100 60  0001 C CNN
F 3 "" H 34400 12100 60  0001 C CNN
	1    34400 12100
	1    0    0    -1  
$EndComp
Text Label 34500 9250 0    60   ~ 0
/RESETA
$Comp
L 74LS03 U103
U 3 1 56061357
P 33700 9250
F 0 "U103" H 33700 9300 60  0000 C CNN
F 1 "7403" H 33700 9200 60  0000 C CNN
F 2 "14dip300" H 33700 9250 60  0001 C CNN
F 3 "" H 33700 9250 60  0001 C CNN
	3    33700 9250
	1    0    0    1   
$EndComp
$Comp
L 74LS03 U108
U 1 1 56061369
P 33700 10650
F 0 "U108" H 33700 10700 60  0000 C CNN
F 1 "7403" H 33700 10600 60  0000 C CNN
F 2 "14dip300" H 33700 10650 60  0001 C CNN
F 3 "" H 33700 10650 60  0001 C CNN
	1    33700 10650
	1    0    0    -1  
$EndComp
$Comp
L 74LS03 U103
U 4 1 56061375
P 33700 9950
F 0 "U103" H 33700 10000 60  0000 C CNN
F 1 "7403" H 33700 9900 60  0000 C CNN
F 2 "14dip300" H 33700 9950 60  0001 C CNN
F 3 "" H 33700 9950 60  0001 C CNN
	4    33700 9950
	1    0    0    -1  
$EndComp
Text Label 34450 10650 0    60   ~ 0
/IOWRA
Text Label 34400 9950 0    60   ~ 0
/IORDA
Text Label 31300 18000 0    60   ~ 0
8255_SEL*
Text Label 30800 8250 0    60   ~ 0
8255_SEL*
Text Label 30800 7500 0    60   ~ 0
bA0
Text Label 30850 7600 0    60   ~ 0
bA1
Text Label 30500 7150 0    60   ~ 0
S100_WR*
Text Label 30500 6900 0    60   ~ 0
S100_RD*
$Comp
L 74LS04 U25
U 3 1 56077D30
P 31900 7700
F 0 "U25" H 32095 7815 60  0000 C CNN
F 1 "74LS04" H 32090 7575 60  0000 C CNN
F 2 "~" H 31900 7700 60  0000 C CNN
F 3 "~" H 31900 7700 60  0000 C CNN
	3    31900 7700
	-1   0    0    1   
$EndComp
Text Label 32550 7700 0    60   ~ 0
BOARD_RESET*
Text Label 9400 1100 0    60   ~ 0
BOARD_RESET*
$Comp
L CONN_3 K102
U 1 1 56079264
P 32750 13800
F 0 "K102" V 32700 13800 50  0000 C CNN
F 1 "CONN_3" V 32800 13800 40  0000 C CNN
F 2 "" H 32750 13800 60  0000 C CNN
F 3 "" H 32750 13800 60  0000 C CNN
	1    32750 13800
	0    -1   -1   0   
$EndComp
$Comp
L VCC #PWR059
U 1 1 56079273
P 33100 14050
F 0 "#PWR059" H 33100 14150 30  0001 C CNN
F 1 "VCC" H 33100 14150 30  0000 C CNN
F 2 "" H 33100 14050 60  0000 C CNN
F 3 "" H 33100 14050 60  0000 C CNN
	1    33100 14050
	1    0    0    -1  
$EndComp
NoConn ~ 29950 14450
$Comp
L CONN_2 P104
U 1 1 56082011
P 32700 13150
F 0 "P104" V 32650 13150 40  0000 C CNN
F 1 "CONN_2" V 32750 13150 40  0000 C CNN
F 2 "" H 32700 13150 60  0000 C CNN
F 3 "" H 32700 13150 60  0000 C CNN
	1    32700 13150
	0    -1   -1   0   
$EndComp
NoConn ~ 34050 11700
NoConn ~ 34050 11900
NoConn ~ 34050 12100
NoConn ~ 24300 24750
$Comp
L 74LS11 U43
U 2 1 56083262
P 30350 18950
F 0 "U43" H 30350 19000 60  0000 C CNN
F 1 "74LS11" H 30350 18900 60  0000 C CNN
F 2 "~" H 30350 18950 60  0000 C CNN
F 3 "~" H 30350 18950 60  0000 C CNN
	2    30350 18950
	1    0    0    -1  
$EndComp
Text Label 31100 18950 0    60   ~ 0
BUS_SELECT*
$Comp
L C C103
U 1 1 560632AD
P 40550 22250
F 0 "C103" H 40600 22350 50  0000 L CNN
F 1 "0.1 uF" H 40600 22150 50  0000 L CNN
F 2 "C2" H 40550 22250 60  0001 C CNN
F 3 "" H 40550 22250 60  0001 C CNN
	1    40550 22250
	1    0    0    -1  
$EndComp
$Comp
L C C101
U 1 1 560632BF
P 36500 21650
F 0 "C101" H 36550 21750 50  0000 L CNN
F 1 "0.1 uF" H 36550 21550 50  0000 L CNN
F 2 "C2" H 36500 21650 60  0001 C CNN
F 3 "" H 36500 21650 60  0001 C CNN
	1    36500 21650
	1    0    0    -1  
$EndComp
$Comp
L 74LS74 U116
U 1 1 56067120
P 5750 6050
F 0 "U116" H 5900 6350 60  0000 C CNN
F 1 "74LS74" H 6050 5755 60  0000 C CNN
F 2 "~" H 5750 6050 60  0000 C CNN
F 3 "~" H 5750 6050 60  0000 C CNN
	1    5750 6050
	1    0    0    -1  
$EndComp
$Comp
L 74LS04 U24
U 3 1 5606712D
P 6150 5050
F 0 "U24" H 6345 5165 60  0000 C CNN
F 1 "74LS04" H 6340 4925 60  0000 C CNN
F 2 "~" H 6150 5050 60  0000 C CNN
F 3 "~" H 6150 5050 60  0000 C CNN
	3    6150 5050
	1    0    0    -1  
$EndComp
Text Label 3400 5950 2    60   ~ 0
BASE_PORT_(6+7)*
Text Label 3400 6150 2    60   ~ 0
S100_WR*
Text Label 4200 5400 2    60   ~ 0
DO0
Text Label 5500 5050 2    60   ~ 0
bA15
$Comp
L VCC #PWR060
U 1 1 5606C09A
P 5750 5400
F 0 "#PWR060" H 5750 5500 30  0001 C CNN
F 1 "VCC" H 5750 5500 30  0000 C CNN
F 2 "" H 5750 5400 60  0000 C CNN
F 3 "" H 5750 5400 60  0000 C CNN
	1    5750 5400
	1    0    0    -1  
$EndComp
Text Label 5450 6650 2    60   ~ 0
BOARD_RESET*
$Comp
L 74LS08 U109
U 1 1 5606D482
P 7400 5250
F 0 "U109" H 7400 5300 60  0000 C CNN
F 1 "74LS08" H 7400 5200 60  0000 C CNN
F 2 "~" H 7400 5250 60  0000 C CNN
F 3 "~" H 7400 5250 60  0000 C CNN
	1    7400 5250
	1    0    0    -1  
$EndComp
$Comp
L 74LS08 U109
U 2 1 5606D491
P 30450 18000
F 0 "U109" H 30450 18050 60  0000 C CNN
F 1 "74LS08" H 30450 17950 60  0000 C CNN
F 2 "~" H 30450 18000 60  0000 C CNN
F 3 "~" H 30450 18000 60  0000 C CNN
	2    30450 18000
	1    0    0    -1  
$EndComp
$Comp
L 74LS02 U117
U 1 1 5607097F
P 4300 6050
F 0 "U117" H 4300 6100 60  0000 C CNN
F 1 "74LS02" H 4350 6000 60  0000 C CNN
F 2 "~" H 4300 6050 60  0000 C CNN
F 3 "~" H 4300 6050 60  0000 C CNN
	1    4300 6050
	1    0    0    -1  
$EndComp
$Comp
L R R106
U 1 1 56066718
P 7250 6800
F 0 "R106" V 7150 6850 40  0000 C CNN
F 1 "470" V 7257 6801 40  0000 C CNN
F 2 "~" V 7180 6800 30  0000 C CNN
F 3 "~" H 7250 6800 30  0000 C CNN
	1    7250 6800
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR061
U 1 1 56066736
P 7500 7000
F 0 "#PWR061" H 7500 7000 30  0001 C CNN
F 1 "GND" H 7500 6930 30  0001 C CNN
F 2 "" H 7500 7000 60  0000 C CNN
F 3 "" H 7500 7000 60  0000 C CNN
	1    7500 7000
	1    0    0    -1  
$EndComp
Text Notes 6900 6100 0    60   ~ 0
RED/GREEN \nLED
NoConn ~ 34050 12500
$Comp
L 74LS04 U24
U 2 1 560688DE
P 7900 6250
F 0 "U24" H 8095 6365 60  0000 C CNN
F 1 "74LS04" H 8090 6125 60  0000 C CNN
F 2 "~" H 7900 6250 60  0000 C CNN
F 3 "~" H 7900 6250 60  0000 C CNN
	2    7900 6250
	-1   0    0    1   
$EndComp
$Comp
L C C109
U 1 1 5606AACD
P 41000 21050
F 0 "C109" H 41050 21150 50  0000 L CNN
F 1 "0.1 uF" H 41050 20950 50  0000 L CNN
F 2 "C2" H 41000 21050 60  0001 C CNN
F 3 "" H 41000 21050 60  0001 C CNN
	1    41000 21050
	1    0    0    -1  
$EndComp
$Comp
L C C108
U 1 1 5606B169
P 40550 22850
F 0 "C108" H 40600 22950 50  0000 L CNN
F 1 "0.1 uF" H 40600 22750 50  0000 L CNN
F 2 "C2" H 40550 22850 60  0001 C CNN
F 3 "" H 40550 22850 60  0001 C CNN
	1    40550 22850
	1    0    0    -1  
$EndComp
$Comp
L C C104
U 1 1 5606B17B
P 38300 23450
F 0 "C104" H 38350 23550 50  0000 L CNN
F 1 "0.1 uF" H 38350 23350 50  0000 L CNN
F 2 "C2" H 38300 23450 60  0001 C CNN
F 3 "" H 38300 23450 60  0001 C CNN
	1    38300 23450
	1    0    0    -1  
$EndComp
$Comp
L LED D102
U 1 1 56066A7C
P 6900 6450
F 0 "D102" H 7050 6300 50  0000 C CNN
F 1 "LED" H 6850 6300 50  0000 C CNN
F 2 "~" H 6900 6450 60  0000 C CNN
F 3 "~" H 6900 6450 60  0000 C CNN
	1    6900 6450
	0    1    1    0   
$EndComp
$Comp
L LED D104
U 1 1 56066A8B
P 7100 6450
F 0 "D104" H 7200 6550 50  0000 C CNN
F 1 "LED" H 7000 6550 50  0000 C CNN
F 2 "~" H 7100 6450 60  0000 C CNN
F 3 "~" H 7100 6450 60  0000 C CNN
	1    7100 6450
	0    1    1    0   
$EndComp
$Comp
L 74LS04 U24
U 4 1 561743C4
P 11550 7850
F 0 "U24" H 11745 7965 60  0000 C CNN
F 1 "74LS04" H 11740 7725 60  0000 C CNN
F 2 "~" H 11550 7850 60  0000 C CNN
F 3 "~" H 11550 7850 60  0000 C CNN
	4    11550 7850
	-1   0    0    -1  
$EndComp
Text Label 12150 7850 0    60   ~ 0
LOCAL_MWRT
Text Label 21500 25150 0    60   ~ 0
LOCAL_MWRT
NoConn ~ 41500 13150
NoConn ~ 18350 14950
$Comp
L VCC #PWR062
U 1 1 561A0322
P 8700 7400
F 0 "#PWR062" H 8700 7500 30  0001 C CNN
F 1 "VCC" H 8700 7500 30  0000 C CNN
F 2 "" H 8700 7400 60  0000 C CNN
F 3 "" H 8700 7400 60  0000 C CNN
	1    8700 7400
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR063
U 1 1 561A0A71
P 33300 13600
F 0 "#PWR063" H 33300 13700 30  0001 C CNN
F 1 "VCC" H 33300 13700 30  0000 C CNN
F 2 "" H 33300 13600 60  0000 C CNN
F 3 "" H 33300 13600 60  0000 C CNN
	1    33300 13600
	1    0    0    -1  
$EndComp
$Comp
L 74LS74 U116
U 2 1 5625B430
P 8800 3800
F 0 "U116" H 8950 4100 60  0000 C CNN
F 1 "74LS74" H 9100 3505 60  0000 C CNN
F 2 "~" H 8800 3800 60  0000 C CNN
F 3 "~" H 8800 3800 60  0000 C CNN
	2    8800 3800
	1    0    0    -1  
$EndComp
Text Label 8550 4500 2    60   ~ 0
BOARD_RESET*
$Comp
L VCC #PWR064
U 1 1 5625BACE
P 8800 3050
F 0 "#PWR064" H 8800 3150 30  0001 C CNN
F 1 "VCC" H 8800 3150 30  0000 C CNN
F 2 "" H 8800 3050 60  0000 C CNN
F 3 "" H 8800 3050 60  0000 C CNN
	1    8800 3050
	1    0    0    -1  
$EndComp
Text Label 7650 3600 2    60   ~ 0
DO1
Wire Wire Line
	12950 3050 13800 3050
Wire Wire Line
	2050 1550 2050 1850
Wire Wire Line
	2050 1850 2150 1850
Wire Wire Line
	7750 18500 10250 18500
Connection ~ 7750 17500
Wire Wire Line
	7750 18500 7750 17500
Wire Wire Line
	7350 16750 7650 16750
Wire Wire Line
	7250 1500 7250 1700
Wire Wire Line
	13900 5100 13900 1600
Wire Wire Line
	13900 5100 14150 5100
Wire Wire Line
	14000 2800 14000 4600
Wire Wire Line
	14000 4600 14150 4600
Wire Wire Line
	14150 4700 14000 4700
Wire Wire Line
	14000 4700 14000 5250
Wire Wire Line
	14000 5250 15450 5250
Wire Wire Line
	15450 5250 15450 5100
Connection ~ 13050 4350
Connection ~ 13050 4250
Connection ~ 13050 4150
Wire Wire Line
	13050 4150 13050 4500
Wire Wire Line
	13050 4500 14150 4500
Wire Wire Line
	4650 2350 5100 2350
Connection ~ 3900 2000
Wire Wire Line
	3900 1850 3900 2900
Wire Wire Line
	3900 1850 3050 1850
Connection ~ 3900 2500
Wire Wire Line
	3900 2900 3000 2900
Wire Wire Line
	13350 5750 13350 11100
Wire Wire Line
	13150 5550 13150 11300
Wire Wire Line
	16700 12100 17400 12100
Wire Wire Line
	7800 19050 8600 19050
Wire Wire Line
	7000 18950 6850 18950
Wire Wire Line
	6850 18950 6850 18800
Connection ~ 5500 21500
Wire Wire Line
	9000 20950 9000 21300
Wire Wire Line
	9000 21300 5500 21300
Wire Wire Line
	5500 21300 5500 21700
Wire Wire Line
	10200 20850 10200 21550
Wire Wire Line
	3250 21550 3250 18500
Wire Wire Line
	3250 21550 3100 21550
Wire Wire Line
	3100 21550 3100 25150
Wire Wire Line
	3100 18700 3100 21050
Wire Wire Line
	13700 27550 10900 27550
Wire Wire Line
	10100 27650 10100 27800
Wire Wire Line
	15950 29150 19700 29150
Wire Wire Line
	12350 29150 12450 29150
Wire Wire Line
	14000 22350 14000 21950
Wire Wire Line
	14000 21950 12600 21950
Wire Wire Line
	12600 21950 12600 22300
Wire Wire Line
	19850 13450 20850 13450
Wire Wire Line
	19850 13550 20850 13550
Wire Wire Line
	19850 13350 20850 13350
Wire Wire Line
	19900 14400 20850 14400
Wire Wire Line
	19900 14500 20850 14500
Wire Wire Line
	19900 14700 20850 14700
Wire Wire Line
	19900 14600 20850 14600
Wire Wire Line
	19900 14200 20850 14200
Wire Wire Line
	19900 14300 20850 14300
Wire Wire Line
	19900 14100 20850 14100
Wire Wire Line
	19900 14000 20850 14000
Wire Wire Line
	17700 13450 18300 13450
Wire Wire Line
	17700 13550 18300 13550
Wire Wire Line
	17700 13750 18300 13750
Wire Wire Line
	17700 13650 18300 13650
Wire Wire Line
	17700 14050 18300 14050
Wire Wire Line
	17700 14150 18300 14150
Wire Wire Line
	17700 13950 18300 13950
Wire Wire Line
	17700 13850 18300 13850
Wire Wire Line
	17700 14250 18300 14250
Connection ~ 13500 22300
Connection ~ 11400 22300
Wire Wire Line
	13500 22300 13500 24100
Connection ~ 11400 22150
Wire Wire Line
	21300 19450 20750 19450
Wire Wire Line
	16250 15400 16850 15400
Wire Wire Line
	16250 15000 16850 15000
Wire Wire Line
	16250 15100 16850 15100
Wire Wire Line
	16250 15300 16850 15300
Wire Wire Line
	16250 15200 16850 15200
Wire Wire Line
	16250 14800 16850 14800
Wire Wire Line
	16250 14900 16850 14900
Wire Wire Line
	16250 14700 16850 14700
Wire Wire Line
	16250 14600 16850 14600
Connection ~ 20750 18800
Wire Wire Line
	20750 18950 20750 18800
Connection ~ 20750 19250
Wire Wire Line
	20750 19450 20750 19250
Connection ~ 20750 17800
Connection ~ 3400 19650
Connection ~ 5100 24450
Wire Wire Line
	5650 24300 5100 24300
Wire Wire Line
	5100 24300 5100 24450
Connection ~ 3600 22700
Connection ~ 3600 21800
Wire Wire Line
	3600 21800 3600 23450
Wire Wire Line
	3300 2000 3900 2000
Wire Wire Line
	16050 2700 15400 2700
Wire Wire Line
	8150 18850 9500 18850
Wire Wire Line
	19350 18250 18800 18250
Wire Wire Line
	18800 18250 18800 18550
Wire Wire Line
	1200 700  1750 700 
Wire Wire Line
	1750 700  1750 950 
Wire Wire Line
	12300 24950 11650 24950
Wire Wire Line
	18050 12100 18600 12100
Wire Wire Line
	18450 18600 17950 18600
Wire Wire Line
	17950 18600 17950 19450
Wire Wire Line
	21300 18500 20750 18500
Wire Wire Line
	20750 18950 21300 18950
Connection ~ 19400 25300
Wire Wire Line
	19950 25300 19400 25300
Connection ~ 18500 16050
Wire Wire Line
	19050 16250 18500 16250
Wire Wire Line
	18500 16250 18500 16050
Wire Wire Line
	19800 26150 19200 26150
Wire Wire Line
	19200 26150 19200 25950
Wire Wire Line
	14850 14300 15450 14300
Wire Wire Line
	14850 13900 15450 13900
Wire Wire Line
	14850 14000 15450 14000
Wire Wire Line
	14850 14200 15450 14200
Wire Wire Line
	14850 14100 15450 14100
Wire Wire Line
	14850 13700 15450 13700
Wire Wire Line
	14850 13800 15450 13800
Wire Wire Line
	14850 13600 15450 13600
Wire Wire Line
	14850 13500 15450 13500
Wire Wire Line
	7800 20800 7800 20550
Wire Wire Line
	7800 20550 6600 20550
Wire Wire Line
	6600 20550 6600 20150
Connection ~ 6700 21600
Connection ~ 5500 21700
Wire Wire Line
	16050 22150 15950 22150
Connection ~ 15950 22150
Wire Wire Line
	10200 21550 15950 21550
Connection ~ 18950 31050
Connection ~ 19200 25950
Wire Wire Line
	14500 18800 14500 18100
Wire Wire Line
	14500 18100 16450 18100
Connection ~ 16450 16650
Wire Wire Line
	16450 18100 16450 16650
Wire Wire Line
	19700 29150 19700 29750
Wire Wire Line
	8800 31150 8800 29150
Wire Wire Line
	8800 31150 16250 31150
Wire Wire Line
	16250 31150 16250 30250
Wire Wire Line
	16250 30250 20800 30250
Wire Wire Line
	5350 30850 14450 30850
Connection ~ 5350 30850
Connection ~ 7250 30850
Wire Wire Line
	10700 28600 10700 29900
Wire Wire Line
	10700 28600 17100 28600
Wire Wire Line
	17100 28600 17100 27450
Wire Wire Line
	10700 29900 10950 29900
Wire Wire Line
	10950 30050 10850 30050
Wire Wire Line
	10850 30050 10850 30850
Connection ~ 10600 29050
Connection ~ 10400 29250
Wire Wire Line
	10400 30100 10400 29250
Connection ~ 10200 29450
Wire Wire Line
	10200 30100 10200 29450
Connection ~ 10000 29650
Wire Wire Line
	10000 30100 10000 29650
Wire Wire Line
	9750 29450 10950 29450
Wire Wire Line
	9750 29550 10950 29550
Wire Wire Line
	9750 29750 10950 29750
Wire Wire Line
	9750 29650 10950 29650
Wire Wire Line
	9750 29250 10950 29250
Wire Wire Line
	9750 29350 10950 29350
Wire Wire Line
	9750 29150 10950 29150
Wire Wire Line
	9750 29050 10950 29050
Wire Wire Line
	9900 30100 9900 29750
Connection ~ 9900 29750
Wire Wire Line
	10100 30100 10100 29550
Connection ~ 10100 29550
Wire Wire Line
	10300 30100 10300 29350
Connection ~ 10300 29350
Wire Wire Line
	10500 30100 10500 29150
Connection ~ 10500 29150
Wire Wire Line
	10600 30100 10600 29050
Wire Wire Line
	10950 30150 10950 30400
Wire Wire Line
	10950 30400 11250 30400
Wire Wire Line
	14300 29900 14300 28700
Wire Wire Line
	14850 30400 14550 30400
Wire Wire Line
	14550 30400 14550 30150
Wire Wire Line
	14200 30100 14200 29050
Connection ~ 14100 29150
Wire Wire Line
	14100 30100 14100 29150
Connection ~ 13900 29350
Wire Wire Line
	13900 30100 13900 29350
Connection ~ 13700 29550
Wire Wire Line
	13700 30100 13700 29550
Connection ~ 13500 29750
Wire Wire Line
	13500 30100 13500 29750
Wire Wire Line
	13350 29050 14550 29050
Wire Wire Line
	13350 29150 14550 29150
Wire Wire Line
	13350 29350 14550 29350
Wire Wire Line
	13350 29250 14550 29250
Wire Wire Line
	13350 29650 14550 29650
Wire Wire Line
	13350 29750 14550 29750
Wire Wire Line
	13350 29550 14550 29550
Wire Wire Line
	13350 29450 14550 29450
Wire Wire Line
	13600 30100 13600 29650
Connection ~ 13600 29650
Wire Wire Line
	13800 30100 13800 29450
Connection ~ 13800 29450
Wire Wire Line
	14000 30100 14000 29250
Connection ~ 14000 29250
Connection ~ 14200 29050
Wire Wire Line
	14450 30850 14450 30050
Wire Wire Line
	14450 30050 14550 30050
Wire Wire Line
	14300 29900 14550 29900
Wire Wire Line
	7350 29900 7100 29900
Wire Wire Line
	7350 30050 7250 30050
Wire Wire Line
	7250 30050 7250 30850
Connection ~ 7000 29050
Connection ~ 6800 29250
Wire Wire Line
	6800 30100 6800 29250
Connection ~ 6600 29450
Wire Wire Line
	6600 30100 6600 29450
Connection ~ 6400 29650
Wire Wire Line
	6400 30100 6400 29650
Wire Wire Line
	6300 30100 6300 29750
Connection ~ 6300 29750
Wire Wire Line
	6500 30100 6500 29550
Connection ~ 6500 29550
Wire Wire Line
	6700 30100 6700 29350
Connection ~ 6700 29350
Wire Wire Line
	6900 30100 6900 29150
Connection ~ 6900 29150
Wire Wire Line
	7000 30100 7000 29050
Wire Wire Line
	7350 30150 7350 30400
Wire Wire Line
	7350 30400 7650 30400
Wire Wire Line
	6450 16750 6050 16750
Wire Wire Line
	5800 26650 8000 26650
Wire Wire Line
	5100 26350 5100 26800
Wire Wire Line
	6000 26050 5800 26050
Connection ~ 3100 18850
Wire Wire Line
	21150 31150 20500 31150
Wire Wire Line
	21150 30950 20500 30950
Wire Wire Line
	1450 20050 1450 31050
Wire Wire Line
	18450 27000 18450 27350
Connection ~ 13700 27000
Wire Wire Line
	13700 27350 13700 27000
Wire Wire Line
	15650 28050 2300 28050
Wire Wire Line
	15650 28050 15650 27550
Wire Wire Line
	2300 28050 2300 20300
Wire Wire Line
	15650 27550 15900 27550
Connection ~ 4100 19050
Connection ~ 2450 21150
Wire Wire Line
	2450 19850 2450 27800
Connection ~ 2600 20000
Wire Wire Line
	19400 16650 19400 16200
Wire Wire Line
	16050 16650 19400 16650
Wire Wire Line
	16050 16650 16050 17250
Connection ~ 18200 21150
Wire Wire Line
	18850 21150 18850 26700
Wire Wire Line
	11400 21150 18850 21150
Wire Wire Line
	18600 24400 18600 25900
Wire Wire Line
	18600 24400 19800 24400
Wire Wire Line
	19400 16200 21300 16200
Wire Wire Line
	17400 15900 21300 15900
Wire Wire Line
	21300 17800 20750 17800
Wire Wire Line
	19850 20350 19850 20150
Wire Wire Line
	10350 20350 19850 20350
Connection ~ 19850 18900
Wire Wire Line
	19850 18900 19400 18900
Wire Wire Line
	19400 19300 19400 20700
Wire Wire Line
	19400 20700 14500 20700
Wire Wire Line
	14500 20700 14500 19000
Wire Wire Line
	18200 18900 17950 18900
Wire Wire Line
	10350 20350 10350 16250
Wire Wire Line
	10350 16250 1650 16250
Wire Wire Line
	1650 16250 1650 18250
Wire Wire Line
	1650 18250 850  18250
Wire Wire Line
	22200 24500 22600 24500
Wire Wire Line
	21450 23950 21300 23950
Wire Wire Line
	21300 23950 21300 24100
Wire Wire Line
	21300 24100 21200 24100
Wire Wire Line
	21450 23550 21200 23550
Wire Wire Line
	21200 22400 21200 23900
Wire Wire Line
	13900 24750 13900 24950
Wire Wire Line
	13900 24750 17400 24750
Wire Wire Line
	17400 24750 17400 19300
Wire Wire Line
	17400 19300 17250 19300
Connection ~ 9950 25600
Wire Wire Line
	9950 25800 11850 25800
Wire Wire Line
	9950 24300 9950 25800
Wire Wire Line
	12450 26350 11650 26350
Wire Wire Line
	11650 26350 11650 25600
Wire Wire Line
	11650 25600 11850 25600
Wire Wire Line
	13900 24950 12500 24950
Wire Wire Line
	19400 25600 19800 25600
Wire Wire Line
	19400 24800 19400 25600
Wire Wire Line
	19400 24800 19800 24800
Wire Wire Line
	20850 20450 10250 20450
Wire Wire Line
	20850 20450 20850 22700
Wire Wire Line
	19800 23900 17850 23900
Wire Wire Line
	17850 23900 17850 22150
Wire Wire Line
	17850 22150 17250 22150
Connection ~ 11400 24000
Wire Wire Line
	11400 22450 11400 27000
Wire Wire Line
	19800 24000 11400 24000
Wire Wire Line
	9650 24200 19800 24200
Connection ~ 17650 19850
Wire Wire Line
	17650 17450 17650 24300
Connection ~ 8050 24100
Wire Wire Line
	7600 24100 8450 24100
Connection ~ 4800 23900
Wire Wire Line
	4800 23900 4800 25600
Wire Wire Line
	17650 17450 16050 17450
Wire Wire Line
	17650 19850 16050 19850
Wire Wire Line
	16050 19850 16050 19100
Wire Wire Line
	15700 18900 16050 18900
Wire Wire Line
	850  18050 1550 18050
Wire Wire Line
	1550 18050 1550 16150
Wire Wire Line
	1550 16150 14850 16150
Wire Wire Line
	14850 16150 14850 17250
Wire Wire Line
	15950 22900 16650 22900
Wire Wire Line
	8400 19600 5150 19600
Wire Wire Line
	8400 19600 8400 20750
Wire Wire Line
	9000 22100 10200 22100
Wire Wire Line
	10200 22100 10200 22300
Wire Wire Line
	8050 24100 8050 22550
Wire Wire Line
	8050 22550 9000 22550
Wire Wire Line
	8000 18300 7000 18300
Wire Wire Line
	9500 19950 8750 19950
Wire Wire Line
	8750 19950 8750 18850
Connection ~ 8750 18850
Wire Wire Line
	9400 18000 9950 18000
Wire Wire Line
	9400 17800 9950 17800
Wire Wire Line
	9400 17400 9950 17400
Connection ~ 3950 21150
Wire Wire Line
	3950 21600 3950 21150
Wire Wire Line
	5500 22600 5250 22600
Wire Wire Line
	5250 22600 5250 22700
Wire Wire Line
	5250 22700 4900 22700
Wire Wire Line
	6700 22050 6700 21600
Wire Wire Line
	6700 22050 5500 22050
Wire Wire Line
	5500 22050 5500 22300
Wire Wire Line
	5500 21700 5150 21700
Connection ~ 3500 19500
Wire Wire Line
	3500 19500 3500 20950
Wire Wire Line
	3500 20950 4950 20950
Wire Wire Line
	4950 20950 4950 20800
Wire Wire Line
	4950 20800 5400 20800
Wire Wire Line
	8000 17900 6650 17900
Wire Wire Line
	6650 17900 6650 19450
Wire Wire Line
	6650 19450 8150 19450
Wire Wire Line
	8150 19450 8150 21150
Wire Wire Line
	8150 21150 2450 21150
Wire Wire Line
	2450 19850 2350 19850
Connection ~ 6450 19900
Wire Wire Line
	6250 19900 7100 19900
Wire Wire Line
	1550 19950 1550 25600
Wire Wire Line
	1550 19950 1400 19950
Wire Wire Line
	1400 19950 1400 18450
Wire Wire Line
	1400 18450 850  18450
Wire Wire Line
	3400 24850 3400 19650
Wire Wire Line
	850  19650 3750 19650
Wire Wire Line
	3750 19650 3750 20700
Wire Wire Line
	3750 20700 3900 20700
Connection ~ 5050 19500
Wire Wire Line
	4750 19500 5050 19500
Wire Wire Line
	3300 19500 3850 19500
Wire Wire Line
	3300 19500 3300 19250
Wire Wire Line
	3300 19250 850  19250
Wire Wire Line
	8000 17600 6350 17600
Wire Wire Line
	6350 17600 6350 19350
Wire Wire Line
	6350 19350 6250 19350
Wire Wire Line
	6250 17500 8000 17500
Wire Wire Line
	6250 17500 6250 18800
Wire Wire Line
	3100 18700 4000 18700
Wire Wire Line
	850  18850 3100 18850
Wire Wire Line
	8000 17400 6150 17400
Wire Wire Line
	6150 17400 6150 18300
Wire Wire Line
	6150 18300 4900 18300
Wire Wire Line
	1450 19850 850  19850
Wire Wire Line
	1450 19450 850  19450
Wire Wire Line
	850  17850 1450 17850
Wire Wire Line
	1450 20050 850  20050
Wire Wire Line
	16800 11100 16300 11100
Wire Wire Line
	16800 10600 16300 10600
Wire Wire Line
	16800 10800 16300 10800
Wire Wire Line
	16800 10900 16300 10900
Wire Wire Line
	16800 10400 16300 10400
Wire Wire Line
	16800 10300 16300 10300
Wire Wire Line
	16800 10100 16300 10100
Wire Wire Line
	16800 9700 16300 9700
Wire Wire Line
	16800 9900 16300 9900
Wire Wire Line
	16800 9400 16300 9400
Wire Wire Line
	16800 9500 16300 9500
Wire Wire Line
	16800 9300 16300 9300
Wire Wire Line
	16800 9200 16300 9200
Wire Wire Line
	16800 9000 16300 9000
Wire Wire Line
	18150 6750 19300 6750
Connection ~ 18400 8250
Connection ~ 16950 6850
Connection ~ 16950 6350
Connection ~ 16950 6450
Wire Wire Line
	16950 6350 16950 6850
Wire Wire Line
	13250 3150 12950 3150
Wire Wire Line
	14150 3300 13300 3300
Connection ~ 10750 2150
Wire Wire Line
	10750 2150 12100 2150
Wire Wire Line
	12700 2150 15450 2150
Connection ~ 18350 4300
Connection ~ 18350 4200
Wire Wire Line
	16650 1950 18500 1950
Wire Wire Line
	18500 1950 18500 4200
Wire Wire Line
	18500 4200 18350 4200
Wire Wire Line
	16650 2350 17850 2350
Wire Wire Line
	19750 12550 15800 12550
Connection ~ 18400 9200
Connection ~ 18400 8350
Connection ~ 16450 4000
Wire Wire Line
	15550 4000 16950 4000
Connection ~ 16250 3800
Wire Wire Line
	15550 3800 16950 3800
Connection ~ 16050 3600
Wire Wire Line
	15550 3600 16950 3600
Connection ~ 15850 3400
Wire Wire Line
	15550 3400 16950 3400
Wire Wire Line
	16450 4000 16450 7350
Wire Wire Line
	16250 3800 16250 7550
Wire Wire Line
	16050 3600 16050 7750
Wire Wire Line
	15850 3400 15850 7950
Connection ~ 15750 6150
Wire Wire Line
	15750 8050 17000 8050
Connection ~ 15950 5950
Wire Wire Line
	15950 7850 17000 7850
Connection ~ 16150 5750
Wire Wire Line
	16150 7650 17000 7650
Connection ~ 16350 5550
Wire Wire Line
	16350 7450 17000 7450
Connection ~ 13750 6150
Wire Wire Line
	11300 6150 16950 6150
Connection ~ 13550 5950
Wire Wire Line
	11300 5950 16950 5950
Connection ~ 13350 5750
Wire Wire Line
	11300 5750 16950 5750
Connection ~ 13150 5550
Wire Wire Line
	11300 5550 16950 5550
Wire Wire Line
	18350 5450 18800 5450
Wire Wire Line
	18350 5550 18800 5550
Wire Wire Line
	18350 5750 18800 5750
Wire Wire Line
	18350 5650 18800 5650
Wire Wire Line
	18350 6050 18800 6050
Wire Wire Line
	18350 6150 18800 6150
Wire Wire Line
	18350 5950 18800 5950
Wire Wire Line
	18350 5850 18800 5850
Wire Wire Line
	18400 7750 18850 7750
Wire Wire Line
	18400 7850 18850 7850
Wire Wire Line
	18400 8050 18850 8050
Wire Wire Line
	18400 7950 18850 7950
Wire Wire Line
	18400 7550 18850 7550
Wire Wire Line
	18400 7650 18850 7650
Wire Wire Line
	18400 7450 18850 7450
Wire Wire Line
	18400 7350 18850 7350
Wire Wire Line
	10450 11900 10350 11900
Wire Wire Line
	10350 11900 10350 13900
Wire Wire Line
	10350 13900 13250 13900
Wire Wire Line
	13250 13900 13250 12450
Wire Wire Line
	13250 12450 13700 12450
Wire Wire Line
	10750 1600 10750 2150
Wire Wire Line
	12500 12500 12650 12500
Wire Wire Line
	12650 12500 12650 10300
Wire Wire Line
	12550 10500 12550 12300
Wire Wire Line
	12550 12300 12500 12300
Wire Wire Line
	13050 11400 14400 11400
Wire Wire Line
	13050 5450 13050 11400
Wire Wire Line
	13250 11200 14400 11200
Wire Wire Line
	13450 11000 14400 11000
Wire Wire Line
	13450 5850 13450 11000
Wire Wire Line
	13650 10800 14400 10800
Wire Wire Line
	13650 6050 13650 10800
Wire Wire Line
	5000 7200 7950 7200
Wire Wire Line
	6800 8400 7650 8400
Wire Wire Line
	5400 7900 3850 7900
Wire Wire Line
	5400 8000 3850 8000
Wire Wire Line
	5400 8200 3850 8200
Wire Wire Line
	5400 8100 3850 8100
Wire Wire Line
	5400 7700 3850 7700
Wire Wire Line
	5400 7800 3850 7800
Wire Wire Line
	5400 7600 3850 7600
Wire Wire Line
	5400 7500 3850 7500
Connection ~ 6200 2500
Connection ~ 6200 2200
Connection ~ 6200 1600
Connection ~ 1750 3800
Connection ~ 1450 3800
Wire Wire Line
	800  3800 1750 3800
Wire Wire Line
	36500 22050 41000 22050
Wire Wire Line
	36500 21250 41000 21250
Wire Wire Line
	36500 21850 41000 21850
Connection ~ 40550 21850
Connection ~ 41000 21850
Connection ~ 41000 21450
Connection ~ 40550 21450
Connection ~ 41000 21250
Connection ~ 41000 20850
Connection ~ 40550 20850
Connection ~ 40550 21250
Connection ~ 40100 21250
Connection ~ 40100 20850
Connection ~ 39650 20850
Connection ~ 39650 21250
Connection ~ 39200 21250
Connection ~ 39200 20850
Connection ~ 38750 20850
Connection ~ 38750 21250
Connection ~ 38300 21250
Connection ~ 38300 20850
Connection ~ 37850 20850
Connection ~ 37850 21250
Connection ~ 37400 21250
Connection ~ 37400 20850
Connection ~ 36500 21850
Connection ~ 37850 21850
Connection ~ 36500 21450
Connection ~ 39200 21850
Connection ~ 36950 21850
Connection ~ 37400 21850
Connection ~ 37400 21450
Connection ~ 36950 21450
Connection ~ 37850 21450
Connection ~ 38300 21450
Connection ~ 38750 21450
Connection ~ 38750 21850
Connection ~ 38300 21850
Connection ~ 39650 21850
Connection ~ 40100 21850
Connection ~ 40100 21450
Connection ~ 39650 21450
Connection ~ 39200 21450
Connection ~ 36950 21250
Wire Wire Line
	40300 13550 40900 13550
Wire Wire Line
	40300 11850 40900 11850
Wire Wire Line
	40300 8550 40900 8550
Connection ~ 36500 21250
Connection ~ 36500 20850
Wire Wire Line
	41500 6650 42100 6650
Wire Wire Line
	41500 6850 42100 6850
Wire Wire Line
	41500 6750 42100 6750
Wire Wire Line
	41500 7050 42100 7050
Wire Wire Line
	41500 6950 42100 6950
Wire Wire Line
	41500 7450 42100 7450
Wire Wire Line
	41500 7550 42100 7550
Wire Wire Line
	41500 7750 42100 7750
Wire Wire Line
	41500 7650 42100 7650
Wire Wire Line
	41500 7250 42100 7250
Wire Wire Line
	41500 7350 42100 7350
Wire Wire Line
	41500 7150 42100 7150
Wire Wire Line
	41500 7850 42100 7850
Wire Wire Line
	41500 7950 42100 7950
Wire Wire Line
	41500 8150 42100 8150
Wire Wire Line
	41500 8050 42100 8050
Wire Wire Line
	41500 8450 42100 8450
Wire Wire Line
	41500 8550 42100 8550
Wire Wire Line
	41500 8350 42100 8350
Wire Wire Line
	41500 8250 42100 8250
Wire Wire Line
	41500 9850 42100 9850
Wire Wire Line
	41500 9950 42100 9950
Wire Wire Line
	41500 10150 42100 10150
Wire Wire Line
	41500 10050 42100 10050
Wire Wire Line
	41500 9650 42100 9650
Wire Wire Line
	41500 9750 42100 9750
Wire Wire Line
	41500 9550 42100 9550
Wire Wire Line
	41500 9450 42100 9450
Wire Wire Line
	41500 8650 42100 8650
Wire Wire Line
	41500 8750 42100 8750
Wire Wire Line
	41500 8950 42100 8950
Wire Wire Line
	41500 8850 42100 8850
Wire Wire Line
	41500 9250 42100 9250
Wire Wire Line
	41500 9350 42100 9350
Wire Wire Line
	41500 9150 42100 9150
Wire Wire Line
	41500 9050 42100 9050
Wire Wire Line
	41500 10450 42100 10450
Wire Wire Line
	41500 10550 42100 10550
Wire Wire Line
	41500 10750 42100 10750
Wire Wire Line
	41500 10650 42100 10650
Wire Wire Line
	41500 10250 42100 10250
Wire Wire Line
	41500 10350 42100 10350
Wire Wire Line
	41500 10850 42100 10850
Wire Wire Line
	41500 10950 42100 10950
Wire Wire Line
	41500 11150 42100 11150
Wire Wire Line
	41500 11050 42100 11050
Wire Wire Line
	41500 11450 42100 11450
Wire Wire Line
	41500 11550 42100 11550
Wire Wire Line
	41500 11350 42100 11350
Wire Wire Line
	41500 11250 42100 11250
Wire Wire Line
	42100 11750 41500 11750
Wire Wire Line
	41500 11650 42100 11650
Wire Wire Line
	41500 16450 42100 16450
Wire Wire Line
	42100 16550 41500 16550
Wire Wire Line
	41500 16050 42100 16050
Wire Wire Line
	41500 16150 42100 16150
Wire Wire Line
	41500 16350 42100 16350
Wire Wire Line
	41500 16250 42100 16250
Wire Wire Line
	41500 15850 42100 15850
Wire Wire Line
	41500 15950 42100 15950
Wire Wire Line
	41500 15750 42100 15750
Wire Wire Line
	41500 15650 42100 15650
Wire Wire Line
	41500 15150 42100 15150
Wire Wire Line
	41500 15050 42100 15050
Wire Wire Line
	41500 15450 42100 15450
Wire Wire Line
	41500 15550 42100 15550
Wire Wire Line
	41500 15350 42100 15350
Wire Wire Line
	41500 15250 42100 15250
Wire Wire Line
	41500 13850 42100 13850
Wire Wire Line
	41500 13950 42100 13950
Wire Wire Line
	41500 14150 42100 14150
Wire Wire Line
	41500 14050 42100 14050
Wire Wire Line
	41500 13650 42100 13650
Wire Wire Line
	41500 13750 42100 13750
Wire Wire Line
	41500 13550 42100 13550
Wire Wire Line
	41500 13450 42100 13450
Wire Wire Line
	41500 14250 42100 14250
Wire Wire Line
	41500 14350 42100 14350
Wire Wire Line
	41500 14550 42100 14550
Wire Wire Line
	41500 14450 42100 14450
Wire Wire Line
	41500 14850 42100 14850
Wire Wire Line
	41500 14950 42100 14950
Wire Wire Line
	41500 14750 42100 14750
Wire Wire Line
	41500 14650 42100 14650
Wire Wire Line
	41500 13050 42100 13050
Wire Wire Line
	41500 13150 42100 13150
Wire Wire Line
	41500 13350 42100 13350
Wire Wire Line
	41500 13250 42100 13250
Wire Wire Line
	41500 12850 42100 12850
Wire Wire Line
	41500 12950 42100 12950
Wire Wire Line
	41500 12750 42100 12750
Wire Wire Line
	41500 12650 42100 12650
Wire Wire Line
	41500 11850 42100 11850
Wire Wire Line
	41500 11950 42100 11950
Wire Wire Line
	41500 12150 42100 12150
Wire Wire Line
	41500 12050 42100 12050
Wire Wire Line
	41500 12450 42100 12450
Wire Wire Line
	41500 12550 42100 12550
Wire Wire Line
	41500 12350 42100 12350
Wire Wire Line
	41500 12250 42100 12250
Wire Wire Line
	36950 21300 36950 21250
Connection ~ 37850 22050
Connection ~ 38300 22050
Connection ~ 38750 22050
Connection ~ 39200 22050
Connection ~ 39650 22050
Connection ~ 40100 22050
Connection ~ 40100 22450
Connection ~ 39650 22450
Connection ~ 39200 22450
Connection ~ 38750 22450
Connection ~ 38300 22450
Connection ~ 37850 22450
Connection ~ 36950 20850
Connection ~ 37400 22050
Connection ~ 37400 22450
Connection ~ 36950 22050
Connection ~ 36950 22450
Connection ~ 38650 27500
Connection ~ 38650 27400
Wire Wire Line
	36500 21450 41000 21450
Wire Wire Line
	36500 20850 41000 20850
Wire Wire Line
	36950 21850 36950 21900
Wire Wire Line
	36950 22500 36950 22450
Wire Wire Line
	36500 22450 41000 22450
Connection ~ 40550 22050
Connection ~ 40550 22450
Wire Wire Line
	1100 950  6200 950 
Connection ~ 2050 950 
Connection ~ 1750 950 
Wire Wire Line
	1100 2500 2400 2500
Connection ~ 2400 2500
Connection ~ 3000 2500
Connection ~ 1750 2500
Connection ~ 5400 2700
Wire Wire Line
	6200 2500 6200 2200
Wire Wire Line
	2850 7200 4100 7200
Wire Wire Line
	6800 8500 8350 8500
Connection ~ 6950 8400
Wire Wire Line
	13750 10700 14400 10700
Wire Wire Line
	13750 6150 13750 10700
Wire Wire Line
	13550 10900 14400 10900
Wire Wire Line
	13550 5950 13550 10900
Wire Wire Line
	13350 11100 14400 11100
Wire Wire Line
	13150 11300 14400 11300
Wire Wire Line
	12250 13200 12250 13600
Connection ~ 12250 13300
Connection ~ 12250 13400
Connection ~ 12250 13500
Wire Wire Line
	12500 12400 12600 12400
Wire Wire Line
	12600 12400 12600 10400
Wire Wire Line
	12500 12600 12700 12600
Wire Wire Line
	12700 12600 12700 10200
Wire Wire Line
	8450 1600 10750 1600
Wire Wire Line
	14600 12650 13350 12650
Wire Wire Line
	15800 12550 15800 12100
Connection ~ 15800 12550
Connection ~ 15800 12100
Wire Wire Line
	11300 5450 16950 5450
Connection ~ 13050 5450
Wire Wire Line
	11300 5650 16950 5650
Connection ~ 13250 5650
Wire Wire Line
	11300 5850 16950 5850
Connection ~ 13450 5850
Wire Wire Line
	11300 6050 16950 6050
Connection ~ 13650 6050
Wire Wire Line
	16450 7350 17000 7350
Connection ~ 16450 5450
Wire Wire Line
	16250 7550 17000 7550
Connection ~ 16250 5650
Wire Wire Line
	16050 7750 17000 7750
Connection ~ 16050 5850
Wire Wire Line
	15850 7950 17000 7950
Connection ~ 15850 6050
Wire Wire Line
	15750 3300 15750 8050
Wire Wire Line
	15950 3500 15950 7850
Wire Wire Line
	16150 3700 16150 7650
Wire Wire Line
	16350 3900 16350 7450
Wire Wire Line
	15550 3300 16950 3300
Connection ~ 15750 3300
Wire Wire Line
	15550 3500 16950 3500
Connection ~ 15950 3500
Wire Wire Line
	15550 3700 16950 3700
Connection ~ 16150 3700
Wire Wire Line
	15550 3900 16950 3900
Connection ~ 16350 3900
Wire Wire Line
	18350 4000 18850 4000
Wire Wire Line
	18850 4000 18850 4450
Wire Wire Line
	18350 3300 18350 4000
Connection ~ 18350 3300
Connection ~ 18350 3400
Connection ~ 18350 3500
Connection ~ 18350 3600
Connection ~ 18350 3700
Connection ~ 18350 3800
Connection ~ 18350 3900
Connection ~ 18350 4000
Wire Wire Line
	19600 9300 20650 9300
Wire Wire Line
	19750 1600 19750 12550
Wire Wire Line
	18350 4200 18350 4300
Wire Wire Line
	15000 1950 15450 1950
Wire Wire Line
	13900 1600 19750 1600
Connection ~ 16050 1600
Wire Wire Line
	18150 6950 19300 6950
Wire Wire Line
	19300 6950 19300 8250
Wire Wire Line
	19300 8250 18400 8250
Connection ~ 18150 6750
Wire Wire Line
	850  18650 2900 18650
Wire Wire Line
	2900 18650 2900 18300
Wire Wire Line
	2900 18300 4000 18300
Wire Wire Line
	5050 18700 4900 18700
Wire Wire Line
	3200 19050 850  19050
Wire Wire Line
	4800 19050 4100 19050
Wire Wire Line
	5050 18900 4800 18900
Wire Wire Line
	4800 19250 5050 19250
Wire Wire Line
	4800 18900 4800 19250
Connection ~ 4800 19050
Wire Wire Line
	5050 19450 5050 19800
Wire Wire Line
	2350 19450 2600 19450
Wire Wire Line
	2600 20000 5050 20000
Wire Wire Line
	3900 20500 3900 20000
Connection ~ 3900 20000
Wire Wire Line
	6450 19900 6450 17700
Wire Wire Line
	6450 17700 8000 17700
Wire Wire Line
	6550 19550 7800 19550
Wire Wire Line
	6550 19550 6550 17800
Wire Wire Line
	6550 17800 8000 17800
Wire Wire Line
	5100 20600 5400 20600
Wire Wire Line
	6600 20900 6600 21050
Wire Wire Line
	6600 21050 3100 21050
Wire Wire Line
	5250 20600 5250 22450
Connection ~ 5250 20600
Wire Wire Line
	5250 22450 5500 22450
Wire Wire Line
	3700 22900 3700 23900
Wire Wire Line
	3700 23900 6400 23900
Wire Wire Line
	6700 23300 6700 22450
Wire Wire Line
	6700 23300 6400 23300
Wire Wire Line
	6400 23300 6400 23700
Connection ~ 6700 22450
Wire Wire Line
	6700 21600 8250 21600
Wire Wire Line
	8250 21600 8250 19400
Wire Wire Line
	8250 19400 6750 19400
Wire Wire Line
	6750 19400 6750 18000
Wire Wire Line
	6750 18000 8000 18000
Wire Wire Line
	9400 17500 9950 17500
Wire Wire Line
	9400 17700 9950 17700
Wire Wire Line
	9400 17900 9950 17900
Wire Wire Line
	10100 19200 10100 18600
Wire Wire Line
	8900 19200 8650 19200
Wire Wire Line
	8650 19200 8650 22450
Wire Wire Line
	8650 22450 6700 22450
Connection ~ 7000 22450
Wire Wire Line
	9000 22350 8650 22350
Connection ~ 8650 22350
Wire Wire Line
	10200 22600 10200 22900
Wire Wire Line
	10200 22900 8150 22900
Wire Wire Line
	8150 22900 8150 24850
Wire Wire Line
	8150 24850 4300 24850
Wire Wire Line
	8900 19400 8900 21800
Wire Wire Line
	8900 21800 11400 21800
Wire Wire Line
	16200 16050 1450 16050
Wire Wire Line
	1450 16050 1450 17850
Wire Wire Line
	15450 18000 16350 18000
Wire Wire Line
	16350 18000 16350 16900
Wire Wire Line
	16350 16900 15450 16900
Connection ~ 15450 16900
Wire Wire Line
	15850 18550 16650 18550
Wire Wire Line
	15850 18550 15850 19650
Wire Wire Line
	15850 19650 16650 19650
Wire Wire Line
	1550 25600 3600 25600
Connection ~ 1750 25600
Wire Wire Line
	4500 25600 9950 25600
Connection ~ 4800 25600
Wire Wire Line
	3100 25150 8450 25150
Wire Wire Line
	8450 25150 8450 24300
Connection ~ 11400 22450
Wire Wire Line
	13500 24100 19800 24100
Wire Wire Line
	9950 24300 19800 24300
Connection ~ 17650 24300
Wire Wire Line
	10250 20450 10250 18500
Wire Wire Line
	19650 22800 19400 22800
Wire Wire Line
	19400 22800 19400 24500
Wire Wire Line
	19400 24500 19800 24500
Wire Wire Line
	20700 25600 21150 25600
Wire Wire Line
	12450 25250 12950 25250
Wire Wire Line
	11850 25600 11850 25100
Wire Wire Line
	11850 25100 12400 25100
Wire Wire Line
	12400 25100 12400 24950
Connection ~ 11850 25600
Wire Wire Line
	19800 24900 14300 24900
Wire Wire Line
	14300 24900 14300 26000
Wire Wire Line
	14300 26000 13050 26000
Wire Wire Line
	19800 24600 19100 24600
Wire Wire Line
	19100 24600 19100 25950
Wire Wire Line
	19100 25950 21150 25950
Wire Wire Line
	21950 23550 22400 23550
Wire Wire Line
	21950 23750 22400 23750
Wire Wire Line
	22400 24150 21950 24150
Wire Wire Line
	21950 23950 22400 23950
Wire Wire Line
	21200 24000 21250 24000
Wire Wire Line
	21250 24000 21250 23750
Wire Wire Line
	21250 23750 21450 23750
Wire Wire Line
	21350 24200 21200 24200
Wire Wire Line
	21350 22650 21350 24200
Wire Wire Line
	21350 24150 21450 24150
Wire Wire Line
	21200 24600 21200 24750
Wire Wire Line
	21200 24750 22400 24750
Wire Wire Line
	17950 19450 18300 19450
Wire Wire Line
	18300 19450 18300 19650
Wire Wire Line
	18300 19650 18800 19650
Wire Wire Line
	11400 21150 11400 22150
Wire Wire Line
	18200 19100 18200 21150
Connection ~ 11400 21800
Wire Wire Line
	19850 18350 19850 19250
Connection ~ 19850 18800
Wire Wire Line
	21300 18350 20750 18350
Wire Wire Line
	20750 18800 21300 18800
Wire Wire Line
	20750 19250 21300 19250
Wire Wire Line
	21300 20150 20750 20150
Connection ~ 20750 20150
Wire Wire Line
	17250 18900 17250 17800
Wire Wire Line
	17250 17800 19850 17800
Wire Wire Line
	17400 16050 21300 16050
Wire Wire Line
	20900 15100 19900 15100
Wire Wire Line
	20900 15200 19900 15200
Wire Wire Line
	20900 15400 19900 15400
Wire Wire Line
	20900 15300 19900 15300
Wire Wire Line
	20900 15500 19900 15500
Wire Wire Line
	18100 25300 17900 25300
Wire Wire Line
	17200 25600 17200 26050
Wire Wire Line
	18600 25900 17900 25900
Wire Wire Line
	18850 26700 9950 26700
Wire Wire Line
	11400 27000 18450 27000
Wire Wire Line
	17400 16200 18150 16200
Wire Wire Line
	18150 16200 18150 17500
Wire Wire Line
	18150 17500 23000 17500
Wire Wire Line
	23000 17500 23000 29950
Wire Wire Line
	2600 27550 10100 27550
Wire Wire Line
	2600 19450 2600 27550
Wire Wire Line
	2450 27800 10100 27800
Wire Wire Line
	18450 27550 18250 27550
Wire Wire Line
	18250 27550 18250 28050
Wire Wire Line
	18250 28050 17400 28050
Wire Wire Line
	15900 27000 15900 27350
Connection ~ 15900 27000
Wire Wire Line
	1450 31050 21150 31050
Wire Wire Line
	8900 25600 8900 26650
Connection ~ 8900 25600
Connection ~ 41000 22450
Connection ~ 41000 22050
Wire Wire Line
	36500 22650 41000 22650
Wire Wire Line
	36500 23050 41000 23050
Connection ~ 41000 22650
Connection ~ 41000 23050
Connection ~ 40550 23050
Connection ~ 40550 22650
Wire Wire Line
	36950 23050 36950 23100
Connection ~ 36950 23050
Connection ~ 36950 22650
Connection ~ 37400 23050
Connection ~ 37400 22650
Connection ~ 37850 23050
Connection ~ 38300 23050
Connection ~ 38750 23050
Connection ~ 39200 23050
Connection ~ 39650 23050
Connection ~ 40100 23050
Connection ~ 40100 22650
Connection ~ 39650 22650
Connection ~ 39200 22650
Connection ~ 38750 22650
Connection ~ 38300 22650
Connection ~ 37850 22650
Wire Wire Line
	14150 3400 13300 3400
Wire Wire Line
	14150 3600 13300 3600
Wire Wire Line
	14150 3500 13300 3500
Wire Wire Line
	14150 3900 13300 3900
Wire Wire Line
	14150 4000 13300 4000
Wire Wire Line
	14150 3800 13300 3800
Wire Wire Line
	14150 3700 13300 3700
Wire Wire Line
	14150 4100 13300 4100
Wire Wire Line
	13300 4300 14150 4300
Wire Wire Line
	14150 4200 13300 4200
Wire Wire Line
	14300 28700 19650 28700
Wire Wire Line
	19650 28700 19650 27450
Wire Wire Line
	14900 27450 14900 28500
Wire Wire Line
	14900 28500 7100 28500
Wire Wire Line
	7100 28500 7100 29900
Wire Wire Line
	9950 26700 9950 27200
Wire Wire Line
	9950 27200 5350 27200
Wire Wire Line
	5350 27200 5350 30850
Connection ~ 10850 30850
Wire Wire Line
	20900 30050 20800 30050
Wire Wire Line
	20800 30050 20800 30250
Wire Wire Line
	19100 30250 19100 30250
Wire Wire Line
	19700 29950 16050 29950
Wire Wire Line
	16050 29950 16050 30950
Wire Wire Line
	16050 30950 12450 30950
Wire Wire Line
	12450 30950 12450 29150
Wire Wire Line
	16000 11500 16000 11650
Connection ~ 17950 20350
Wire Wire Line
	15950 21550 15950 22900
Wire Wire Line
	2850 20300 2850 21800
Connection ~ 37850 23250
Connection ~ 38300 23250
Connection ~ 38750 23250
Connection ~ 39200 23250
Connection ~ 39650 23250
Connection ~ 40100 23250
Connection ~ 40100 23650
Connection ~ 39650 23650
Connection ~ 39200 23650
Connection ~ 38750 23650
Connection ~ 38300 23650
Connection ~ 37850 23650
Connection ~ 37400 23250
Connection ~ 37400 23650
Connection ~ 36950 23250
Connection ~ 36950 23650
Wire Wire Line
	36950 23650 36950 23700
Connection ~ 40550 23250
Connection ~ 40550 23650
Connection ~ 41000 23650
Connection ~ 41000 23250
Wire Wire Line
	36500 23650 41000 23650
Wire Wire Line
	36500 23250 41000 23250
Wire Wire Line
	7800 19550 7800 20250
Connection ~ 6600 20350
Connection ~ 6600 20150
Connection ~ 7800 20800
Connection ~ 7800 20250
Connection ~ 17950 19450
Connection ~ 17950 18900
Connection ~ 7000 23350
Wire Wire Line
	7000 23350 7600 23350
Wire Wire Line
	19900 16350 19400 16350
Connection ~ 19400 16350
Wire Wire Line
	18500 15900 18500 15750
Connection ~ 18500 15900
Wire Wire Line
	18500 15750 19050 15750
Wire Wire Line
	1450 3800 1450 3550
Wire Wire Line
	1450 3550 900  3550
Wire Wire Line
	16250 13500 16850 13500
Wire Wire Line
	16250 13600 16850 13600
Wire Wire Line
	16250 13800 16850 13800
Wire Wire Line
	16250 13700 16850 13700
Wire Wire Line
	16250 14100 16850 14100
Wire Wire Line
	16250 14200 16850 14200
Wire Wire Line
	16250 14000 16850 14000
Wire Wire Line
	16250 13900 16850 13900
Wire Wire Line
	16250 14300 16850 14300
Wire Wire Line
	20750 20150 20750 19900
Wire Wire Line
	20750 19900 21250 19900
Wire Wire Line
	18950 31050 18950 30850
Wire Wire Line
	18950 30850 19500 30850
Wire Wire Line
	1750 2500 1750 2200
Wire Wire Line
	1750 2200 1200 2200
Wire Wire Line
	6200 2200 6750 2200
Wire Wire Line
	14850 14600 15450 14600
Wire Wire Line
	14850 14700 15450 14700
Wire Wire Line
	14850 14900 15450 14900
Wire Wire Line
	14850 14800 15450 14800
Wire Wire Line
	14850 15200 15450 15200
Wire Wire Line
	14850 15300 15450 15300
Wire Wire Line
	14850 15100 15450 15100
Wire Wire Line
	14850 15000 15450 15000
Wire Wire Line
	14850 15400 15450 15400
Wire Wire Line
	15450 16900 15450 16500
Wire Wire Line
	15450 16500 15950 16500
Wire Wire Line
	16650 18550 16650 18450
Wire Wire Line
	16650 18450 17100 18450
Connection ~ 16650 18550
Wire Wire Line
	16650 21800 17250 21800
Wire Wire Line
	3700 22700 3600 22700
Wire Wire Line
	3600 23450 4300 23450
Wire Wire Line
	4300 22350 3500 22350
Wire Wire Line
	3500 22350 3500 24450
Wire Wire Line
	3500 24450 7000 24450
Wire Wire Line
	20750 17800 20750 18000
Wire Wire Line
	20750 18350 20750 18500
Connection ~ 20750 18350
Wire Wire Line
	20750 18000 21300 18000
Wire Wire Line
	15800 22350 16050 22350
Connection ~ 15800 22350
Connection ~ 16050 22350
Connection ~ 14000 22350
Connection ~ 12600 22300
Wire Wire Line
	8800 29150 8750 29150
Wire Wire Line
	10100 27450 9550 27450
Wire Wire Line
	10900 27450 10900 27650
Connection ~ 10900 27550
Wire Wire Line
	22600 24400 22200 24400
Wire Wire Line
	2300 20300 3650 20300
Wire Wire Line
	3650 20300 3650 19350
Wire Wire Line
	3650 19350 4100 19350
Wire Wire Line
	4100 19350 4100 19050
Connection ~ 2850 20300
Wire Wire Line
	1750 21800 3950 21800
Connection ~ 2850 21800
Wire Wire Line
	3250 18500 4900 18500
Wire Wire Line
	4900 18500 4900 18700
Connection ~ 4900 18700
Wire Wire Line
	7650 16750 7650 18400
Wire Wire Line
	7650 18400 8000 18400
Wire Wire Line
	5150 19600 5150 19700
Wire Wire Line
	5150 19700 3850 19700
Wire Wire Line
	3850 19700 3850 19500
Connection ~ 3850 19500
Wire Wire Line
	8400 20750 9000 20750
Wire Wire Line
	7000 18300 7000 19050
Connection ~ 7000 18950
Wire Wire Line
	7800 18950 7800 18600
Wire Wire Line
	7800 18600 10100 18600
Wire Wire Line
	17650 12100 17650 12550
Connection ~ 17650 12550
Wire Wire Line
	13250 5650 13250 11200
Wire Wire Line
	3900 2500 5100 2500
Wire Wire Line
	5100 2500 5100 2350
Connection ~ 5100 2500
Wire Wire Line
	6200 950  6200 1600
Wire Wire Line
	13400 4800 14150 4800
Wire Wire Line
	12250 4350 11800 4350
Wire Wire Line
	10400 4250 12250 4250
Wire Wire Line
	12250 4150 11800 4150
Wire Wire Line
	14150 4400 13800 4400
Wire Wire Line
	13800 4400 13800 3050
Wire Wire Line
	14150 5000 13600 5000
Wire Wire Line
	6200 1600 7250 1600
Connection ~ 7250 1600
Wire Wire Line
	19600 24200 19600 23200
Connection ~ 19600 24200
Wire Wire Line
	19600 23200 20850 23200
Wire Wire Line
	20850 23200 20850 22900
Wire Wire Line
	1750 3200 1750 2900
Wire Wire Line
	1750 2900 2100 2900
Connection ~ 9950 25800
Connection ~ 9950 26700
Wire Wire Line
	13250 2950 12950 2950
Wire Wire Line
	5800 26350 5800 26650
Connection ~ 5100 26650
Connection ~ 5100 26550
Connection ~ 5100 26450
Connection ~ 5800 26650
Connection ~ 5800 26550
Connection ~ 5800 26450
Connection ~ 17200 25700
Connection ~ 17200 25800
Connection ~ 17200 25900
Connection ~ 17900 25700
Connection ~ 17900 25800
Connection ~ 17900 25900
Wire Wire Line
	17900 25900 17900 25600
Connection ~ 13050 4450
Wire Wire Line
	14900 2800 14600 2800
Wire Wire Line
	11450 13000 11700 13000
Wire Wire Line
	12250 11450 12250 12050
Wire Wire Line
	11450 11600 11450 12200
Connection ~ 11450 12100
Connection ~ 11450 12000
Connection ~ 11450 11900
Wire Wire Line
	11450 12800 11450 13100
Connection ~ 11450 12900
Connection ~ 11450 13000
Wire Wire Line
	12000 11450 12250 11450
Connection ~ 12250 11750
Connection ~ 12250 11850
Connection ~ 12250 11950
Wire Wire Line
	11200 11600 11450 11600
Wire Wire Line
	14250 25600 13950 25600
Wire Wire Line
	21200 24300 22600 24300
Connection ~ 5800 26050
Wire Wire Line
	6800 7450 6800 8200
Connection ~ 6800 7600
Connection ~ 6800 7700
Connection ~ 6800 7800
Connection ~ 6800 7900
Connection ~ 6800 8000
Connection ~ 6800 8100
Wire Wire Line
	7950 8250 8250 8250
Wire Wire Line
	8350 8500 8350 8250
Wire Wire Line
	18150 14650 18350 14650
Wire Wire Line
	18150 14500 18150 14550
Wire Wire Line
	18150 14550 18350 14550
Wire Wire Line
	8450 8250 8450 8350
Wire Wire Line
	8450 8350 8650 8350
Wire Wire Line
	31000 4650 28650 4650
Wire Wire Line
	28650 4650 28650 2450
Wire Wire Line
	26250 4550 27000 4550
Wire Wire Line
	26700 3650 27000 3650
Wire Wire Line
	26700 3750 27000 3750
Wire Wire Line
	26700 3950 27000 3950
Wire Wire Line
	26700 3850 27000 3850
Wire Wire Line
	26700 4250 27000 4250
Wire Wire Line
	26700 4350 27000 4350
Wire Wire Line
	26700 4150 27000 4150
Wire Wire Line
	26700 4050 27000 4050
Connection ~ 24650 3000
Wire Wire Line
	24650 2350 24650 3350
Wire Wire Line
	24650 2350 23200 2350
Wire Wire Line
	24350 19650 24950 19650
Wire Wire Line
	24350 19750 24950 19750
Wire Wire Line
	24350 19950 24950 19950
Wire Wire Line
	24350 19850 24950 19850
Wire Wire Line
	24350 20050 24950 20050
Wire Wire Line
	26250 2900 26250 2450
Wire Wire Line
	24800 3550 25050 3550
Wire Wire Line
	25050 3000 24650 3000
Wire Wire Line
	23800 2900 24050 2900
Wire Wire Line
	22000 3350 23250 3350
Wire Wire Line
	23250 3350 23250 3450
Wire Wire Line
	22000 3650 22700 3650
Wire Wire Line
	23250 3450 23600 3450
Wire Wire Line
	22000 3000 22600 3000
Wire Wire Line
	22000 2800 22600 2800
Wire Wire Line
	24050 2900 24050 2800
Wire Wire Line
	24050 2800 25050 2800
Wire Wire Line
	24650 3350 25050 3350
Wire Wire Line
	24550 20250 24550 21450
Connection ~ 24550 20550
Connection ~ 24550 20650
Connection ~ 24550 20750
Connection ~ 24550 20850
Connection ~ 24550 20950
Connection ~ 24550 21050
Connection ~ 24550 21150
Connection ~ 24550 21250
Wire Wire Line
	23600 3650 23600 3650
Wire Wire Line
	26250 2450 28650 2450
Connection ~ 27000 4650
Wire Wire Line
	21200 22400 22350 22400
Wire Wire Line
	21350 22650 22350 22650
Connection ~ 21200 23550
Connection ~ 21350 24150
Wire Wire Line
	10950 16600 9750 16600
Wire Wire Line
	9750 16600 9750 17500
Connection ~ 9750 17500
Wire Wire Line
	9800 17600 9800 16750
Wire Wire Line
	9800 16750 10950 16750
Connection ~ 9800 17600
Wire Wire Line
	9400 17600 9950 17600
Wire Wire Line
	24050 3050 23900 3050
Wire Wire Line
	23900 3050 23900 2900
Connection ~ 23900 2900
Connection ~ 24950 3550
Wire Wire Line
	9300 7050 9900 7050
Wire Wire Line
	9300 5250 9300 6950
Wire Wire Line
	9300 7150 9300 7300
Wire Wire Line
	9750 7300 9750 7250
Wire Wire Line
	9750 7250 9900 7250
Wire Wire Line
	8700 7400 9850 7400
Wire Wire Line
	9850 7400 9850 7350
Wire Wire Line
	9850 7350 9900 7350
Wire Wire Line
	9750 5450 9900 5450
Wire Wire Line
	9750 5550 9900 5550
Wire Wire Line
	9750 5650 9900 5650
Wire Wire Line
	9750 5750 9900 5750
Wire Wire Line
	9750 5850 9900 5850
Wire Wire Line
	9750 5950 9900 5950
Wire Wire Line
	9750 6050 9900 6050
Wire Wire Line
	9750 6150 9900 6150
Wire Wire Line
	9750 6250 9900 6250
Wire Wire Line
	9750 6350 9900 6350
Wire Wire Line
	9750 6450 9900 6450
Wire Wire Line
	9750 6550 9900 6550
Wire Wire Line
	9750 6650 9900 6650
Wire Wire Line
	9750 6750 9900 6750
Wire Wire Line
	9750 6850 9900 6850
Wire Wire Line
	9750 6950 9900 6950
Connection ~ 5350 7400
Wire Wire Line
	14100 9000 14400 9000
Wire Wire Line
	14100 9100 14400 9100
Wire Wire Line
	14100 9200 14400 9200
Wire Wire Line
	14100 9300 14400 9300
Wire Wire Line
	14100 9400 14400 9400
Wire Wire Line
	14100 9500 14400 9500
Wire Wire Line
	14100 9600 14400 9600
Wire Wire Line
	14100 9700 14400 9700
Wire Wire Line
	14050 9800 14400 9800
Wire Wire Line
	14050 9900 14400 9900
Wire Wire Line
	14050 10000 14400 10000
Wire Wire Line
	14050 10100 14400 10100
Wire Wire Line
	12700 10200 14400 10200
Wire Wire Line
	12650 10300 14400 10300
Wire Wire Line
	12600 10400 14400 10400
Wire Wire Line
	12550 10500 14400 10500
Wire Wire Line
	7000 9150 6850 9150
Wire Wire Line
	6850 9250 7000 9250
Wire Wire Line
	6850 9350 7000 9350
Wire Wire Line
	6850 9450 7000 9450
Wire Wire Line
	6850 9550 7000 9550
Wire Wire Line
	6850 9650 7000 9650
Wire Wire Line
	6850 9750 7000 9750
Wire Wire Line
	6850 9850 7000 9850
Wire Wire Line
	6900 10750 7050 10750
Wire Wire Line
	6900 10850 7050 10850
Wire Wire Line
	6900 10950 7050 10950
Wire Wire Line
	6900 11050 7050 11050
Wire Wire Line
	6900 11150 7050 11150
Wire Wire Line
	6900 11250 7050 11250
Wire Wire Line
	6900 11350 7050 11350
Wire Wire Line
	6900 11450 7050 11450
Wire Wire Line
	5100 10750 5500 10750
Wire Wire Line
	5100 10850 5500 10850
Wire Wire Line
	5100 10950 5500 10950
Wire Wire Line
	5100 11050 5500 11050
Wire Wire Line
	5100 11150 5500 11150
Wire Wire Line
	5100 11250 5500 11250
Wire Wire Line
	5100 11350 5500 11350
Wire Wire Line
	5100 11450 5500 11450
Wire Wire Line
	5250 9150 5450 9150
Wire Wire Line
	5250 9250 5450 9250
Wire Wire Line
	5250 9350 5450 9350
Wire Wire Line
	5250 9450 5450 9450
Wire Wire Line
	5250 9550 5450 9550
Wire Wire Line
	5250 9650 5450 9650
Wire Wire Line
	5250 9750 5450 9750
Wire Wire Line
	5250 9850 5450 9850
Wire Wire Line
	6850 7450 6800 7450
Connection ~ 6800 7500
Wire Wire Line
	7950 7200 7950 11750
Connection ~ 7950 8250
Wire Wire Line
	7650 8400 7650 11650
Wire Wire Line
	7650 10700 8850 10700
Wire Wire Line
	6850 10050 7650 10050
Connection ~ 7650 10050
Wire Wire Line
	7650 11650 6900 11650
Connection ~ 7650 10700
Wire Wire Line
	9900 7450 9500 7450
Wire Wire Line
	9500 7450 9500 8950
Wire Wire Line
	11700 8550 12000 8550
Wire Wire Line
	11700 8750 11850 8750
Wire Wire Line
	10400 8650 10500 8650
Wire Wire Line
	9950 8700 10200 8700
Wire Wire Line
	10200 8700 10200 8650
Wire Wire Line
	9500 8950 10300 8950
Wire Wire Line
	10300 8950 10300 8650
Wire Wire Line
	9900 7550 9650 7550
Wire Wire Line
	9650 7550 9650 7850
Wire Wire Line
	9650 7850 11100 7850
Wire Wire Line
	18150 8550 18150 8750
Wire Wire Line
	18400 8850 18150 8850
Wire Wire Line
	18400 8250 18400 8850
Wire Wire Line
	18150 8950 18400 8950
Wire Wire Line
	18400 8950 18400 9200
Wire Wire Line
	18100 14750 18350 14750
Wire Wire Line
	18100 14850 18350 14850
Wire Wire Line
	17700 8550 18150 8550
Wire Wire Line
	25900 23850 26250 23850
Connection ~ 26050 24450
Wire Wire Line
	24850 24450 24150 24450
Wire Wire Line
	26050 24550 26050 25100
Wire Wire Line
	24850 24300 24150 24300
Wire Wire Line
	24850 25200 24150 25200
Wire Wire Line
	26050 24450 26050 24150
Wire Wire Line
	26050 24150 24850 24150
Wire Wire Line
	26250 24050 26250 24550
Connection ~ 26250 24550
Connection ~ 26250 23850
Wire Wire Line
	26300 19650 25950 19650
Wire Wire Line
	26300 19350 26300 19650
Wire Wire Line
	25900 19000 26300 19000
Wire Wire Line
	25900 18850 26300 18850
Wire Wire Line
	24950 3550 24950 3850
Wire Wire Line
	24950 3850 25000 3850
Wire Wire Line
	17950 15350 18350 15350
Wire Wire Line
	28150 23950 28300 23950
Wire Wire Line
	27450 23950 27750 23950
Wire Wire Line
	25000 23850 24850 23850
Wire Wire Line
	24850 23850 24850 24150
Wire Wire Line
	24850 24600 24850 25000
Wire Wire Line
	24700 24800 24850 24800
Connection ~ 24850 24800
Connection ~ 38200 26450
Wire Wire Line
	39600 26850 39600 26900
Wire Wire Line
	37650 26450 38550 26450
Connection ~ 39100 26450
Connection ~ 39600 26850
Connection ~ 39100 26850
Connection ~ 39600 26450
Wire Wire Line
	38850 26450 40700 26450
Wire Wire Line
	39600 26300 39600 26450
Wire Wire Line
	40150 26450 40150 26250
Connection ~ 40150 26450
Wire Wire Line
	38000 26100 38000 26450
Connection ~ 38000 26450
Wire Wire Line
	26350 25400 26750 25400
Wire Wire Line
	26450 25600 26750 25600
Wire Wire Line
	26450 25500 26750 25500
Wire Wire Line
	27450 25600 27850 25600
Wire Wire Line
	27750 25300 27450 25300
Wire Wire Line
	27450 25200 27750 25200
Wire Wire Line
	27950 25500 27450 25500
Wire Wire Line
	26450 24900 26750 24900
Wire Wire Line
	27450 25700 27750 25700
Wire Wire Line
	26450 25100 26750 25100
Wire Wire Line
	27450 24900 27750 24900
Wire Wire Line
	27450 25100 27750 25100
Wire Wire Line
	26450 25000 26750 25000
Wire Wire Line
	26450 25300 26750 25300
Wire Wire Line
	27450 25000 27750 25000
Wire Wire Line
	26450 25200 26750 25200
Wire Wire Line
	27450 25400 27750 25400
Wire Wire Line
	27950 24450 27950 25500
Wire Wire Line
	27950 24450 26050 24450
Wire Wire Line
	26050 24550 27850 24550
Wire Wire Line
	27850 24550 27850 25600
Wire Wire Line
	38850 26400 38850 26450
Wire Wire Line
	38650 26400 38750 26400
Wire Wire Line
	38700 26400 38700 26850
Connection ~ 38700 26850
Connection ~ 38700 26400
Wire Wire Line
	38550 26450 38550 26400
Wire Wire Line
	12550 29050 12550 29850
Connection ~ 12550 29150
Connection ~ 12550 29250
Connection ~ 12550 29350
Connection ~ 12550 29450
Connection ~ 12550 29550
Connection ~ 12550 29650
Wire Wire Line
	12550 29850 12750 29850
Wire Wire Line
	12750 29850 12750 30000
Connection ~ 12550 29750
Wire Wire Line
	5500 29050 5500 29850
Connection ~ 5500 29150
Connection ~ 5500 29250
Connection ~ 5500 29350
Connection ~ 5500 29450
Connection ~ 5500 29550
Connection ~ 5500 29650
Wire Wire Line
	5500 29850 5700 29850
Wire Wire Line
	5700 29850 5700 30000
Connection ~ 5500 29750
Wire Wire Line
	6300 29050 7350 29050
Wire Wire Line
	6300 29150 7350 29150
Wire Wire Line
	6300 29250 7350 29250
Wire Wire Line
	6300 29350 7350 29350
Wire Wire Line
	6300 29450 7350 29450
Wire Wire Line
	6300 29550 7350 29550
Wire Wire Line
	6300 29650 7350 29650
Wire Wire Line
	6300 29750 7350 29750
Wire Wire Line
	8950 29050 8950 29850
Connection ~ 8950 29150
Connection ~ 8950 29250
Connection ~ 8950 29350
Connection ~ 8950 29450
Connection ~ 8950 29550
Connection ~ 8950 29650
Wire Wire Line
	8950 29850 9150 29850
Wire Wire Line
	9150 29850 9150 30000
Connection ~ 8950 29750
Wire Wire Line
	28000 18800 29750 18800
Wire Wire Line
	28000 19200 29600 19200
Connection ~ 29050 24450
Connection ~ 29050 24350
Connection ~ 29050 24250
Connection ~ 29050 24150
Connection ~ 29050 24050
Connection ~ 29050 23950
Connection ~ 29050 23850
Connection ~ 29050 23750
Wire Wire Line
	29050 23750 29050 24600
Wire Wire Line
	32350 24450 32700 24450
Wire Wire Line
	32350 24350 32700 24350
Wire Wire Line
	32350 24150 32700 24150
Wire Wire Line
	32350 24250 32700 24250
Wire Wire Line
	32350 23850 32700 23850
Wire Wire Line
	32350 23750 32700 23750
Wire Wire Line
	32350 23950 32700 23950
Wire Wire Line
	32350 24050 32700 24050
Connection ~ 30950 24750
Connection ~ 30950 24650
Wire Wire Line
	30950 24650 30950 25150
Wire Wire Line
	29400 25250 29550 25250
Wire Wire Line
	29400 25050 29550 25050
Wire Wire Line
	30950 25150 30750 25150
Wire Wire Line
	29450 24450 30950 24450
Wire Wire Line
	29450 24350 30950 24350
Wire Wire Line
	29450 24250 30950 24250
Wire Wire Line
	29450 24150 30950 24150
Wire Wire Line
	29450 24050 30950 24050
Wire Wire Line
	29450 23950 30950 23950
Wire Wire Line
	30500 23850 30950 23850
Wire Wire Line
	30500 23750 30950 23750
Wire Wire Line
	29500 19000 28000 19000
Wire Wire Line
	20150 8400 20000 8400
Wire Wire Line
	20000 8400 20000 8750
Wire Wire Line
	20000 8750 20150 8750
Wire Wire Line
	20150 8850 19600 8850
Wire Wire Line
	19600 8850 19600 9100
Wire Wire Line
	20150 8950 19750 8950
Connection ~ 19750 8950
Wire Wire Line
	30900 23250 30850 23250
Wire Wire Line
	30850 23250 30850 23750
Connection ~ 30850 23750
Wire Wire Line
	30750 23150 30900 23150
Wire Wire Line
	30750 23150 30750 23850
Connection ~ 30750 23850
Wire Wire Line
	30900 23050 30250 23050
Wire Wire Line
	30900 22950 30150 22950
Wire Wire Line
	30050 22850 30900 22850
Wire Wire Line
	30900 22750 29950 22750
Wire Wire Line
	29850 22650 30900 22650
Wire Wire Line
	30550 22450 30900 22450
Wire Wire Line
	27000 4550 27000 4650
Wire Wire Line
	38200 26850 39600 26850
Connection ~ 27900 10900
Wire Wire Line
	27800 10900 28000 10900
Wire Wire Line
	30750 13650 31500 13650
Wire Wire Line
	31500 13650 31500 11850
Wire Wire Line
	30750 13450 31300 13450
Wire Wire Line
	30750 13050 30900 13050
Wire Wire Line
	30900 13050 30900 12450
Wire Wire Line
	30750 12950 31900 12950
Wire Wire Line
	30750 14250 30950 14250
Connection ~ 31900 14950
Connection ~ 31900 14850
Connection ~ 31900 14350
Connection ~ 31900 14150
Connection ~ 31900 14050
Connection ~ 31900 13950
Wire Wire Line
	31900 12950 31900 15050
Wire Wire Line
	31800 12300 31800 14750
Wire Wire Line
	28800 14750 28800 14250
Wire Wire Line
	31700 14650 30750 14650
Wire Wire Line
	29950 14850 28800 14850
Wire Wire Line
	31900 14850 30750 14850
Wire Wire Line
	30750 14150 32650 14150
Wire Wire Line
	31900 13950 30750 13950
Wire Wire Line
	29950 14150 29500 14150
Wire Wire Line
	29950 14050 29500 14050
Wire Wire Line
	31800 14750 30750 14750
Wire Wire Line
	31700 14650 31700 11650
Wire Wire Line
	31700 11650 31100 11650
Wire Wire Line
	31100 11650 31100 10200
Wire Wire Line
	28900 14650 29950 14650
Wire Wire Line
	28900 10300 28900 14650
Wire Wire Line
	28900 10300 30900 10300
Wire Wire Line
	30900 10300 30900 10200
Wire Wire Line
	30750 13150 31000 13150
Wire Wire Line
	31500 11850 30600 11850
Wire Wire Line
	30600 11850 30600 10200
Wire Wire Line
	30500 10200 30500 11950
Wire Wire Line
	30500 11950 31400 11950
Wire Wire Line
	30750 13250 31100 13250
Wire Wire Line
	30300 10200 30300 12150
Wire Wire Line
	30300 12150 31200 12150
Wire Wire Line
	30100 10200 30100 12350
Wire Wire Line
	30100 12350 31000 12350
Wire Wire Line
	29950 13050 29800 13050
Wire Wire Line
	29800 13050 29800 10200
Wire Wire Line
	29950 13250 29600 13250
Wire Wire Line
	29600 13250 29600 10200
Wire Wire Line
	29950 13450 29400 13450
Wire Wire Line
	29400 13450 29400 10200
Wire Wire Line
	29950 13650 29200 13650
Wire Wire Line
	29200 13650 29200 10200
Wire Wire Line
	30400 8400 30400 7700
Wire Wire Line
	30200 7500 30200 8400
Wire Wire Line
	30000 6900 30000 8400
Wire Wire Line
	29700 4250 29700 8400
Wire Wire Line
	29500 4050 29500 8400
Wire Wire Line
	29300 3850 29300 8400
Wire Wire Line
	29100 3650 29100 8400
Wire Wire Line
	29200 3750 29200 8400
Wire Wire Line
	29400 3950 29400 8400
Wire Wire Line
	29600 4150 29600 8400
Wire Wire Line
	29800 4350 29800 8400
Wire Wire Line
	30100 7150 30100 8400
Wire Wire Line
	30300 7600 30300 8400
Wire Wire Line
	29100 10200 29100 13750
Wire Wire Line
	29100 13750 29950 13750
Wire Wire Line
	29300 10200 29300 13550
Wire Wire Line
	29300 13550 29950 13550
Wire Wire Line
	29500 10200 29500 13350
Wire Wire Line
	29500 13350 29950 13350
Wire Wire Line
	29700 10200 29700 13150
Wire Wire Line
	29700 13150 29950 13150
Wire Wire Line
	30000 10200 30000 12450
Wire Wire Line
	30000 12450 30900 12450
Wire Wire Line
	30200 10200 30200 12250
Wire Wire Line
	30200 12250 31100 12250
Wire Wire Line
	30400 10200 30400 12050
Wire Wire Line
	30400 12050 31300 12050
Wire Wire Line
	30750 13350 31200 13350
Wire Wire Line
	30700 11750 31600 11750
Wire Wire Line
	30700 11750 30700 10200
Wire Wire Line
	31000 10200 31000 10400
Wire Wire Line
	31000 10400 29000 10400
Wire Wire Line
	29000 10400 29000 14550
Wire Wire Line
	29000 14550 29950 14550
Wire Wire Line
	31300 10200 31300 10500
Wire Wire Line
	31900 14050 30750 14050
Wire Wire Line
	31900 14350 30750 14350
Wire Wire Line
	31900 14950 29100 14950
Wire Wire Line
	29100 14950 29100 13850
Wire Wire Line
	29100 13850 29950 13850
Wire Wire Line
	27900 14850 27650 14850
Wire Wire Line
	31200 10500 27900 10500
Wire Wire Line
	27900 10500 27900 10900
Wire Wire Line
	28800 14750 29950 14750
Wire Wire Line
	29950 12950 28400 12950
Wire Wire Line
	31300 10500 31800 10500
Wire Wire Line
	31200 10200 31200 10500
Wire Wire Line
	27900 12100 27900 14250
Wire Wire Line
	27900 14250 28800 14250
Wire Wire Line
	31450 14450 31650 14450
Wire Wire Line
	30750 14450 30950 14450
Wire Wire Line
	31600 11750 31600 13750
Wire Wire Line
	31600 13750 30750 13750
Wire Wire Line
	31000 12350 31000 13150
Wire Wire Line
	31100 12250 31100 13250
Wire Wire Line
	31200 13350 31200 12150
Wire Wire Line
	31300 12050 31300 13450
Wire Wire Line
	30750 13550 31400 13550
Wire Wire Line
	31400 13550 31400 11950
Wire Wire Line
	31450 14250 32750 14250
Wire Wire Line
	30750 13850 32200 13850
Wire Wire Line
	34050 12000 33600 12000
Wire Wire Line
	34050 11800 33600 11800
Wire Wire Line
	34050 12200 33600 12200
Wire Wire Line
	34050 12400 33600 12400
Wire Wire Line
	34050 12300 33600 12300
Wire Wire Line
	31000 4550 31000 4650
Wire Wire Line
	26250 3450 26250 4550
Connection ~ 29100 3650
Connection ~ 29200 3750
Connection ~ 29300 3850
Connection ~ 29400 3950
Connection ~ 29500 4050
Connection ~ 29600 4150
Connection ~ 29700 4250
Connection ~ 29800 4350
Wire Wire Line
	28400 3650 31000 3650
Wire Wire Line
	28400 3750 31000 3750
Wire Wire Line
	28400 3850 31000 3850
Wire Wire Line
	28400 3950 31000 3950
Wire Wire Line
	28400 4050 31000 4050
Wire Wire Line
	28400 4150 31000 4150
Wire Wire Line
	28400 4250 31000 4250
Wire Wire Line
	28400 4350 31000 4350
Wire Wire Line
	24950 20150 24950 20350
Wire Wire Line
	24950 20250 24550 20250
Connection ~ 24950 20250
Wire Wire Line
	30800 8250 30500 8250
Wire Wire Line
	30500 8250 30500 8400
Wire Wire Line
	30300 7600 30850 7600
Wire Wire Line
	30200 7500 30800 7500
Wire Wire Line
	30500 7150 30100 7150
Wire Wire Line
	30500 6900 30000 6900
Wire Wire Line
	30400 7700 31450 7700
Wire Wire Line
	32350 7700 32550 7700
Wire Wire Line
	9400 1100 9400 1600
Connection ~ 9400 1600
Wire Wire Line
	32750 14250 32750 14150
Wire Wire Line
	32850 14150 32850 14250
Wire Wire Line
	32850 14250 33100 14250
Wire Wire Line
	33100 14250 33100 14050
Wire Wire Line
	31750 10650 31950 10650
Wire Wire Line
	31800 10500 31800 10650
Connection ~ 31800 10650
Wire Wire Line
	31850 11850 31850 12300
Wire Wire Line
	31850 12300 31800 12300
Wire Wire Line
	31400 10200 31400 10450
Wire Wire Line
	31400 10450 32300 10450
Wire Wire Line
	31500 10200 31500 10300
Wire Wire Line
	31500 10300 32800 10300
Wire Wire Line
	31600 10200 32550 10200
Wire Wire Line
	32300 10450 32300 10550
Wire Wire Line
	32300 10550 33100 10550
Wire Wire Line
	34450 10650 34300 10650
Wire Wire Line
	34300 9950 34400 9950
Wire Wire Line
	33100 10550 33100 10750
Wire Wire Line
	33100 9850 33100 10050
Wire Wire Line
	32800 10300 32800 9950
Wire Wire Line
	32800 9950 33100 9950
Connection ~ 33100 9950
Wire Wire Line
	34300 9250 34500 9250
Wire Wire Line
	33100 9150 33100 9350
Wire Wire Line
	32550 10200 32550 9250
Wire Wire Line
	32550 9250 33100 9250
Connection ~ 33100 9250
Wire Wire Line
	32800 13500 32800 13600
Wire Wire Line
	32800 13600 33300 13600
Wire Wire Line
	32200 13850 32200 13500
Wire Wire Line
	32200 13500 32600 13500
Wire Wire Line
	29500 18100 29500 19000
Wire Wire Line
	29500 18950 29750 18950
Wire Wire Line
	29600 19200 29600 19100
Wire Wire Line
	29600 19100 29750 19100
Wire Wire Line
	31100 18950 30950 18950
Wire Wire Line
	29500 18100 29850 18100
Connection ~ 29700 18100
Connection ~ 29500 18950
Connection ~ 29250 18800
Wire Wire Line
	28000 19400 28350 19400
Wire Wire Line
	3400 5950 3700 5950
Wire Wire Line
	3400 6150 3700 6150
Wire Wire Line
	4900 6050 5150 6050
Wire Wire Line
	4200 5400 5100 5400
Wire Wire Line
	5100 5400 5100 5850
Wire Wire Line
	5100 5850 5150 5850
Wire Wire Line
	6600 5350 6600 5850
Wire Wire Line
	6600 5850 6350 5850
Wire Wire Line
	8000 5250 9300 5250
Wire Wire Line
	5500 5050 5700 5050
Wire Wire Line
	5750 5400 5750 5500
Wire Wire Line
	5450 6650 5750 6650
Wire Wire Line
	5750 6650 5750 6600
Wire Wire Line
	31050 18000 31300 18000
Wire Wire Line
	29850 17900 29250 17900
Wire Wire Line
	29250 17900 29250 18800
Wire Wire Line
	6600 5350 6800 5350
Wire Wire Line
	6600 5050 6600 5150
Wire Wire Line
	6600 5150 6800 5150
Wire Wire Line
	6350 6250 6900 6250
Wire Wire Line
	7000 6650 7000 6800
Wire Wire Line
	7500 6800 7500 7000
Wire Wire Line
	6700 6250 6700 5900
Wire Wire Line
	6700 5900 8350 5900
Wire Wire Line
	8350 5900 8350 6250
Connection ~ 6700 6250
Wire Wire Line
	7450 6250 7100 6250
Wire Wire Line
	6900 6650 7100 6650
Connection ~ 7000 6650
Wire Wire Line
	30250 23050 30250 23950
Connection ~ 30250 23950
Wire Wire Line
	30150 22950 30150 24050
Connection ~ 30150 24050
Wire Wire Line
	30050 22850 30050 24150
Connection ~ 30050 24150
Wire Wire Line
	29950 22750 29950 24250
Connection ~ 29950 24250
Wire Wire Line
	29850 22650 29850 24350
Connection ~ 29850 24350
Wire Wire Line
	30900 22550 29750 22550
Wire Wire Line
	29750 22550 29750 24450
Connection ~ 29750 24450
Wire Wire Line
	12000 7850 12150 7850
Wire Wire Line
	21200 24400 21400 24400
Wire Wire Line
	21200 24500 21400 24500
Wire Wire Line
	21300 24500 21300 25150
Wire Wire Line
	21300 25150 21500 25150
Connection ~ 21300 24500
Wire Wire Line
	7950 10150 6850 10150
Wire Wire Line
	7950 11750 6900 11750
Connection ~ 7950 10150
Wire Wire Line
	8550 4500 8800 4500
Wire Wire Line
	8800 4500 8800 4350
Wire Wire Line
	8800 3050 8800 3250
Wire Wire Line
	4950 6050 4950 3800
Wire Wire Line
	4950 3800 8200 3800
Connection ~ 4950 6050
Wire Wire Line
	10400 4250 10400 3600
Wire Wire Line
	10400 3600 9400 3600
Wire Wire Line
	7650 3600 8200 3600
NoConn ~ 9400 4000
$EndSCHEMATC
