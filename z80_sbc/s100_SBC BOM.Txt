SBC Z80 Board           Creation date: 17/10/2015 08:38:21

| C7         0.1 uF      
| C8         0.1 uF      
| C9         0.1 uF      
| C10        0.1 uF      
| C11        0.1 uF      
| C12        0.1 uF      
| C13        0.1 uF      
| C14        0.1 uF      
| C15        0.1 uF      
| C16        0.1 uF      
| C17        0.1 uF      
| C18        0.1 uF      
| C19        0.1 uF      
| C20        0.1 uF      
| C21        0.1 uF      
| C22        0.1 uF      
| C23        0.1 uF      
| C25        0.1 uF      
| C26        0.1 uF      
| C27        0.1 uF      
| C28        0.1 uF      
| C29        0.1 uF      
| C38        0.1 uF      
| C39        0.1 uF      
| C40        0.1 uF      
| C41        0.1 uF      
| C42        22 uF       
| C43        33 uF       
| C44        0.1 uF      
| C45        0.1 uF      
| C46        0.1 uF      
| C47        0.1 uF      
| C48        0.1 uF      
| C49        0.1 uF      
| C50        0.1 uF      
| C51        0.1 uF      
| C52        0.1 uF      
| C53        0.1 uF      
| C54        0.1 uF      
| C57        0.1 uF      
| C58        0.1 uF      
| C59        0.1 uF      
| C60        0.1 uF      
| C61        0.1 uF      
| C62        0.1 uF      
| C63        0.1 uF      
| C101       0.1 uF      
| C102       33 uF       
| C103       0.1 uF      
| C104       0.1 uF      
| C105       1 uF        
| C106       0.1 uF      
| C107       0.1 uF      
| C108       0.1 uF      
| C109       0.1 uF      
| D5         LED         
| D6         1N4148      
| D8         LED         
| D101       LED         
| D102       LED         
| D103       LED         
| D104       LED         
| IC101      74LS682N    
| JP1        2 Pin Jumper      
| JP2        2 Pin Jumper      
| JP3        2 Pin Jumper      
| JP4        2 Pin Jumper      
| JP5        2 Pin Jumper      
| JP6        2 Pin Jumper      
| JP7        2 Pin Jumper      
| JP8        3 Pin Jumper     
| JP9        2 Pin Jumper      
| JP10       2 Pin Jumper      
| K1         3 Pin Jumper     
| K2         3 Pin Jumper     
| K101       3 Pin Jumper     
| K102       3 Pin Jumper     
| K103       3 Pin Jumper     
| K105       3 Pin Jumper     
| K106       3 Pin Jumper     
| K107       3 Pin Jumper     
| P1         S100_MALE   
| P3         CONN_4X2    
| P4         CONN_2X2    
| P8         CONN_4      
| P9         CONN_4      
| P10        CONN_4      
| P36        CONN_3X2    
| P37        CONN_2X2    
| P101       CONN_3X2    
| P102       CONN_20X2   
| P103       DLP-USB1232H    (Mouser 626-DLP-USB1232H)      
| P104       2 Pin Jumper       
| P112       CONN_8X2    
| P113       CONN_8X2    
| P114       CONN_8X2    
| Q1         2N3904      
| R2         220         
| R5         330         
| R11        4700        
| R35        10          
| R36        10          
| R37        10          
| R38        10          
| R39        4700        
| R40        1000        
| R41        4700        
| R101       10K         
| R102       470         
| R103       470         
| R104       470         
| R106       470         
| RR2        1000        
| RR3        1000        
| RR4        1000        
| RR5        1000        
| RR6        1000        
| RR7        4700        
| RR8        1000        
| RR9        1000        
| RR101      1000        
| RR102      680         
| RR103      1K          
| SW101      DIPS_08     
| SW102      DIPS_08     
| U1         Z80A       
| U2         74LS373     
| U3         74LS373     
| U9         74LS165     
| U10        74LS165     
| U11        74LS165     
| U12        74LS373     
| U13        27C64       
| U15        74LS682N    
| U18        74LS244     
| U19        74LS244     
| U20        74LS244     
| U21        74LS244     
| U22        74LS373     
| U24        74LS04      
| U25        74LS04      
| U26        74LS05      
| U28        74LS00      
| U29        74LS74      
| U30        74LS74      
| U31        74LS00      
| U32        74LS74      
| U33        74LS74      
| U34        74LS74      
| U35        74LS32      
| U36        74LS04      
| U37        74LS05      
| U38        74F04       
| U39        74S08       
| U40        74S00       
| U41        74LS27      
| U42        2.0000 MHz  
| U43        74LS11      
| U45        74LS02      
| U46        4.0000 MHz  
| U101       74LS244     
| U102       74LS32      
| U103       7403        
| U104       74LS00      
| U105       74LS32      
| U106       82C55A         
| U107       74LS244     
| U108       7403        
| U109       74LS08      
| U110       74LS244     
| U111       628128      
| U113       74LS139     
| U114       74LS04      
| U115       74LS08      
| U116       74LS74      
| U117       74LS02      
| VReg-101   D24V25F5  2.5 Amp (https://www.pololu.com/category/131/step-down-voltage-regulators)  

Note: Pin jumpers can be made from Jameco #160882 & #117197 "Headers"
