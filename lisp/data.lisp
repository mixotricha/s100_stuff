; Damiens Lisp Keyboard Generator 

(defvar kb_data 
  (list

    (list "FUNCTION"     2 1 )
    (list "ESCAPE"       2 1 )
    (list "REFRESH"      2 1 )
    (list "&#9632;"      2 1 )
    (list "&#9679;"      2 1 )
    (list "&#9650;"      2 1 )
    (list "CLEAR INPUT"  2 1 )
    (list "SUSPEND"      2 1 )
    (list "RESUME"       2 1 )
    (list "ABORT"        2 1 )  
 
    (list 0 0 0)
 
    (list "NETWORK"  2  1)
    (list "<b>:</b>" 1  1)
    (list "!\n1"     1  1)
    (list "@\n2"     1  1)
    (list "#\n3"     1  1)
    (list "$\n4"     1  1)
    (list "%\n5"     1  1)
    (list "^\n6"     1  1)
    (list "&\n7"     1  1)
    (list "*\n8"     1  1)
    (list "(\n9"     1  1)
    (list ")\n0"     1  1)
    (list "&mdash;\n&ndash;" 1  1)
    (list "+\n="     1   1)
    (list "~\n'"     1   1)
    (list "(\n\\"    1   1)
    (list ")\n|"     1   1)
    (list "HELP"     2   1)
 
    (list 0 0 0)   
 
    (list "LOCAL"  2 1)
    (list "TAB"    1.5 1)
    (list "Q" 1  1)
    (list "W" 1  1)
    (list "E" 1  1)
    (list "R" 1  1)
    (list "T" 1  1)
    (list "Y" 1  1)
    (list "U" 1  1)
    (list "I" 1  1)
    (list "O" 1  1)
    (list "P" 1  1)
    (list "[\n(" 1 1)
    (list "]\n)" 1 1)
    (list "BACK SPACE"  1 1)
    (list "PAGE"  1.5 1)
    (list "COMPLETE" 2 1)
     
    (list 0 0 0)   
 
    (list "SELECT"   2 1)
    (list "RUB OUT"  1.75 1)
    (list "A" 1  1)
    (list "S" 1  1)
    (list "D" 1  1)
    (list "F" 1  1)
    (list "G" 1  1)
    (list "H" 1  1)
    (list "J" 1  1)
    (list "K" 1  1)
    (list "L" 1  1)
    (list ":\n;" 1  1)
    (list "\"\n'"  1 1)
    (list "RETURN"  2 1)
    (list "LINE"  1.25 1)
    (list "END"   2 1)
  
    (list 0 0 0)   
 
    (list "CAPS LOCK"   1       1)
    (list "SYMBOL"     1.25    1)
    (list "SHIFT"      2       1)
    (list "Z" 1  1)
    (list "X" 1  1)
    (list "C" 1  1)
    (list "V" 1  1)
    (list "B" 1  1)
    (list "N" 1  1)
    (list "M" 1  1)
    (list "<\n" 1   1)
    (list ">\n." 1  1)
    (list "?\n/" 1  1)
    (list "SHIFT" 2 1)
    (list "SYMBOL"  1.25 1)
    (list "REPEAT"  1.25 1)
    (list "MODE LOCK"  1.25 1)
  
    (list 0 0 0)   
 
    (list "HYPER"    1.5  1)
    (list "SUPER"    1.5  1)
    (list "META"     1.25 1)
    (list "CONTROL"  1.75 1)
    (list "SPACE"    8    1)
    (list "CONTROL"  1.75 1)
    (list "META"     1.25 1)
    (list "SUPER"    1    1)
    (list "HYPER"    1    1)
    (list "SCROLL"   1    1)
  
))


; Ugly learned nothing way 
#|(defvar x_row 0)
(defvar x_pos 0)
(defvar y_pos 0)
(defvar x_pos_prev 0)
(defvar y_pos_prev 0)
(defvar key_size 0.725)

(defun mullet(lst) 
 (mapcar (lambda(i)  

    (setq x_pos (+ (* (second i) key_size) x_pos_prev))
    (setq y_pos (+ (* (third  i) key_size) y_pos_prev))
    (format t "polygon([ [~a,~a] , [~a,~a], [~a,~a] , [~a,~a] ], paths=[[0,1,2,3]]); ~C ~C" 
      x_pos_prev y_pos_prev x_pos y_pos_prev x_pos y_pos x_pos_prev y_pos #\return #\linefeed ) 
  
    (setq x_pos_prev (+ x_pos 0.001))  
    (if (= (third i) 0) (progn 
      (setq x_pos 0)
      (setq x_pos_prev 0)  
      (setq y_pos_prev (+ y_pos 0.001))
      (print "dropping next row")
    ) )

 ) lst ))

(mullet kb_data)

(exit)



;(defun my-mapcar* (func arglist)
;  (mapcar 
;     (lambda (args)
;       (apply func args))
;     arglist))

;(my-mapcar* 
;  #'+ 
;  '((1) (1 1) (1 1 1) (1 1 1 1))
  
|#


; **************************************************************************
; Damiens KeyBoard Generating Lisp Thing 
; **************************************************************************
(defun generate-grid (list x_pos y_pos x_pos_prev y_pos_prev key_size size cx cy)
  (when list                            
    (setq key_size (second (car list)))   
      (if (= key_size 0) 
        (progn 
          (setq cx 0)
          (setq x_pos 0)
          (setq x_pos_prev 0)  
          (setq y_pos  (+ y_pos size)  )
          (setq y_pos_prev (+ y_pos_prev size))
          (setq cy (+ cy 0.01))

          ;(format t ";;;;;;;;;;;;;;;;;; ~C ~C" #\return #\linefeed ) 
          ;(format t "offset=0.0; ~C ~C" #\return #\linefeed ) 
        )
        (progn 
          (setq x_pos (+ (* key_size size) x_pos_prev))

          (format t "translate([~$,~$,0.0]) { ~C" 
            cx cy #\linefeed ) 
          
          ;(format t "polygon([ [~$,~$] , [~$,~$], [~$,~$] , [~$,~$] ], paths=[[0,1,2,3]]); ~C ~C" 
          ;  x_pos_prev y_pos_prev x_pos y_pos_prev x_pos y_pos x_pos_prev y_pos #\return #\linefeed ) 

          (format t "color([1,0,0]) ~C ~C" #\return #\linefeed )  

          (format t "polygon([ [~$,~$] , [~$,~$], [~$,~$] , [~$,~$] ], paths=[[0,1,2,3]]); } ~C ~C" 
            (+ x_pos_prev 0.5) 
            (- y_pos_prev 0.5)
            (- x_pos 0.5)
            (- y_pos_prev 0.5)
            (- x_pos 0.5)
            (+ y_pos 0.5)
            (+ x_pos_prev 0.5)
            (+ y_pos 0.5) 
            #\return 
            #\linefeed 
          ) 


          (setq x_pos_prev x_pos )
          (setq cx (+ cx 0.01))  
        )
      )
    (generate-grid (cdr list) x_pos y_pos x_pos_prev y_pos_prev key_size size cx cy)
  )
)       

(setq ks (+ 18.415 1)) ; 0.725" in mm

;(format t ";;;;;;;;;;;;;;;;;; ~C ~C" #\return #\linefeed ) 
;(format t "offset=0.0; ~C ~C" #\return #\linefeed ) 

(generate-grid kb_data 0 0 0 ks 0 ks 0 0)
(exit) 




 