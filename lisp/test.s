	.text
	.intel_syntax noprefix
	.file	"test.lisp"
	.p2align	4, 0x90                         # -- Begin function RUN-ALL/home/mixotricha/workspace/s100_stuff/lisp/test.lisp
	.type	"RUN-ALL/home/mixotricha/workspace/s100_stuff/lisp/test.lisp",@function
"RUN-ALL/home/mixotricha/workspace/s100_stuff/lisp/test.lisp": # @"RUN-ALL/home/mixotricha/workspace/s100_stuff/lisp/test.lisp"
.Lfunc_begin0:
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	sub	rsp, 32
	movabs	rdi, offset __clasp_gcroots_in_module_4
	movabs	rsi, offset __clasp_literals_6
	mov	edx, 19
	xor	eax, eax
	mov	ecx, eax
	lea	r8, [rbp - 8]
	mov	r9d, 70
	movabs	rax, offset "function-vector-0"
	mov	qword ptr [rsp], 27
	mov	qword ptr [rsp + 8], rax
	call	cc_initialize_gcroots_in_module@PLT
	movabs	rdi, offset __clasp_gcroots_in_module_4
	movabs	rsi, offset "startup-byte-code-0"
	mov	edx, 1278
	call	cc_invoke_byte_code_interpreter@PLT
	movabs	rdi, offset __clasp_gcroots_in_module_4
	call	cc_finish_gcroots_in_module@PLT
	xor	eax, eax
                                        # kill: def $rax killed $eax
	add	rsp, 32
	pop	rbp
	.cfi_def_cfa rsp, 8
	ret
.Lfunc_end0:
	.size	"RUN-ALL/home/mixotricha/workspace/s100_stuff/lisp/test.lisp", .Lfunc_end0-"RUN-ALL/home/mixotricha/workspace/s100_stuff/lisp/test.lisp"
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90                         # -- Begin function LAMBDA^COMMON-LISP^FN^^-lcl
	.type	"LAMBDA^COMMON-LISP^FN^^-lcl",@function
"LAMBDA^COMMON-LISP^FN^^-lcl":          # @"LAMBDA^COMMON-LISP^FN^^-lcl"
.Lfunc_begin1:
	.file	1 "/home/mixotricha/workspace/s100_stuff/lisp" "test.lisp" source "difile-source"
	.loc	1 1320 0                        # test.lisp:1320:0
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	sub	rsp, 16
	mov	rcx, rdi
.Ltmp0:
	.loc	1 1320 0 prologue_end           # test.lisp:1320:0
	mov	rdx, qword ptr [rip + __clasp_literals_6+80]
	mov	rax, rdx
	sub	rax, rsi
	cmove	rcx, rdx
	mov	qword ptr [rbp - 8], rcx        # 8-byte Spill
	mov	eax, ecx
	and	eax, 7
	sub	eax, 3
.Ltmp1:
	.loc	1 0 0 is_stmt 0                 # test.lisp:0:0
	mov	rsi, qword ptr [rip + __clasp_literals_6+120]
	mov	rax, rdx
	cmove	rax, rsi
.Ltmp2:
	.loc	1 1320 0                        # test.lisp:1320:0
	sub	rax, rdx
	setne	al
	sub	rcx, rdx
	sete	cl
	or	al, cl
	test	al, 1
	jne	.LBB1_2
# %bb.1:                                # %if-then3
	.loc	1 0 0                           # test.lisp:0:0
	mov	rdi, qword ptr [rbp - 8]        # 8-byte Reload
	.loc	1 1320 0                        # test.lisp:1320:0
	mov	rsi, qword ptr [__clasp_literals_6+104]
	call	cc_error_type_error@PLT
.LBB1_2:                                # %if-else4
	.loc	1 0 0                           # test.lisp:0:0
	mov	rax, qword ptr [rbp - 8]        # 8-byte Reload
	.loc	1 1320 0                        # test.lisp:1320:0
	add	rsp, 16
	pop	rbp
	.cfi_def_cfa rsp, 8
	ret
.Ltmp3:
.Lfunc_end1:
	.size	"LAMBDA^COMMON-LISP^FN^^-lcl", .Lfunc_end1-"LAMBDA^COMMON-LISP^FN^^-lcl"
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90                         # -- Begin function LAMBDA^COMMON-LISP^FN^^-lcl.1
	.type	"LAMBDA^COMMON-LISP^FN^^-lcl.1",@function
"LAMBDA^COMMON-LISP^FN^^-lcl.1":        # @"LAMBDA^COMMON-LISP^FN^^-lcl.1"
.Lfunc_begin2:
	.loc	1 48 0 is_stmt 1                # test.lisp:48:0
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	sub	rsp, 80
	mov	qword ptr [rbp - 40], rdi       # 8-byte Spill
.Ltmp4:
	.loc	1 48 21 prologue_end            # test.lisp:48:21
	mov	rax, qword ptr [rip + __clasp_literals_6+72]
	mov	rax, qword ptr [rax + 31]
	mov	qword ptr [rbp - 32], rax       # 8-byte Spill
	.loc	1 48 27 is_stmt 0               # test.lisp:48:27
	mov	rax, qword ptr [rip + __clasp_literals_6+96]
	mov	rax, qword ptr [rax + 31]
	mov	qword ptr [rbp - 24], rax       # 8-byte Spill
.Ltmp5:
	.loc	1 1319 0 is_stmt 1              # test.lisp:1319:0
	mov	rsi, qword ptr [__clasp_literals_6+120]
                                        # implicit-def: $rdx
	call	"LAMBDA^COMMON-LISP^FN^^-lcl.3"
	mov	qword ptr [rbp - 16], rax       # 8-byte Spill
	cmp	rax, qword ptr [__clasp_literals_6+80]
	mov	qword ptr [rbp - 8], rax        # 8-byte Spill
	je	.LBB2_2
# %bb.1:                                # %if-then
	.loc	1 0 0 is_stmt 0                 # test.lisp:0:0
	mov	rax, qword ptr [rbp - 16]       # 8-byte Reload
	.loc	1 1319 0                        # test.lisp:1319:0
	mov	rax, qword ptr [rax - 3]
	mov	qword ptr [rbp - 8], rax        # 8-byte Spill
.Ltmp6:
.LBB2_2:                                # %merge
	.loc	1 0 0                           # test.lisp:0:0
	mov	rdi, qword ptr [rbp - 40]       # 8-byte Reload
	mov	rax, qword ptr [rbp - 8]        # 8-byte Reload
	mov	qword ptr [rbp - 56], rax       # 8-byte Spill
.Ltmp7:
	.loc	1 1320 0 is_stmt 1              # test.lisp:1320:0
	mov	rsi, qword ptr [__clasp_literals_6+120]
                                        # implicit-def: $rdx
	call	"LAMBDA^COMMON-LISP^FN^^-lcl.2"
	cmp	rax, qword ptr [__clasp_literals_6+80]
	mov	qword ptr [rbp - 48], rax       # 8-byte Spill
	je	.LBB2_4
# %bb.3:                                # %if-then5
	.loc	1 0 0 is_stmt 0                 # test.lisp:0:0
	mov	rax, qword ptr [rbp - 40]       # 8-byte Reload
	.loc	1 1320 0                        # test.lisp:1320:0
	mov	rax, qword ptr [rax + 5]
	mov	qword ptr [rbp - 48], rax       # 8-byte Spill
.LBB2_4:                                # %iblock8
	.loc	1 0 0                           # test.lisp:0:0
	mov	rdi, qword ptr [rbp - 48]       # 8-byte Reload
	.loc	1 1320 0                        # test.lisp:1320:0
	mov	rsi, qword ptr [__clasp_literals_6+120]
                                        # implicit-def: $rdx
	call	"LAMBDA^COMMON-LISP^FN^^-lcl"
	mov	qword ptr [rbp - 72], rax       # 8-byte Spill
	cmp	rax, qword ptr [__clasp_literals_6+80]
	mov	qword ptr [rbp - 64], rax       # 8-byte Spill
	je	.LBB2_6
# %bb.5:                                # %if-then11
	.loc	1 0 0                           # test.lisp:0:0
	mov	rax, qword ptr [rbp - 72]       # 8-byte Reload
	.loc	1 1320 0                        # test.lisp:1320:0
	mov	rax, qword ptr [rax - 3]
	mov	qword ptr [rbp - 64], rax       # 8-byte Spill
.Ltmp8:
.LBB2_6:                                # %merge12
	.loc	1 0 0                           # test.lisp:0:0
	mov	rsi, qword ptr [rbp - 56]       # 8-byte Reload
	mov	rdi, qword ptr [rbp - 24]       # 8-byte Reload
	mov	rdx, qword ptr [rbp - 64]       # 8-byte Reload
	.loc	1 48 27 is_stmt 1               # test.lisp:48:27
	mov	rax, qword ptr [rdi + 7]
	mov	rax, qword ptr [rax + 47]
	call	rax
	mov	rdi, qword ptr [rbp - 32]       # 8-byte Reload
	mov	rsi, rax
	.loc	1 48 20 is_stmt 0               # test.lisp:48:20
	mov	rax, qword ptr [rdi + 7]
	mov	rax, qword ptr [rax + 39]
	add	rsp, 80
	pop	rbp
	.cfi_def_cfa rsp, 8
	jmp	rax                             # TAILCALL
.Ltmp9:
.Lfunc_end2:
	.size	"LAMBDA^COMMON-LISP^FN^^-lcl.1", .Lfunc_end2-"LAMBDA^COMMON-LISP^FN^^-lcl.1"
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90                         # -- Begin function LAMBDA^COMMON-LISP^FN^^-xep
	.type	"LAMBDA^COMMON-LISP^FN^^-xep",@function
"LAMBDA^COMMON-LISP^FN^^-xep":          # @"LAMBDA^COMMON-LISP^FN^^-xep"
.Lfunc_begin3:
	.loc	1 48 0 is_stmt 1                # test.lisp:48:0
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	sub	rsp, 48
	mov	qword ptr [rbp - 48], rdi       # 8-byte Spill
.Ltmp10:
	#DEBUG_VALUE: LAMBDA^COMMON-LISP^FN^^-xep:rsa-closure <- [DW_OP_constu 48, DW_OP_minus] [$rbp+0]
	mov	qword ptr [rbp - 40], rsi       # 8-byte Spill
	mov	qword ptr [rbp - 32], rdx       # 8-byte Spill
.Ltmp11:
	.loc	1 48 9 prologue_end             # test.lisp:48:9
.Ltmp12:
	mov	qword ptr [rbp - 24], rdi
.Ltmp13:
	#DEBUG_VALUE: LAMBDA^COMMON-LISP^FN^^-xep:rsa-closure <- $rdi
	mov	qword ptr [rbp - 16], rsi
	mov	qword ptr [rbp - 8], rdx
	cmp	rsi, 1
.Ltmp14:
	#DEBUG_VALUE: LAMBDA^COMMON-LISP^FN^^-xep:rsa-closure <- [DW_OP_constu 48, DW_OP_minus] [$rbp+0]
	je	.LBB3_2
.Ltmp15:
# %bb.1:                                # %wrong-num-args
	#DEBUG_VALUE: LAMBDA^COMMON-LISP^FN^^-xep:rsa-closure <- [DW_OP_constu 48, DW_OP_minus] [$rbp+0]
	.loc	1 0 9 is_stmt 0                 # test.lisp:0:9
	mov	rsi, qword ptr [rbp - 40]       # 8-byte Reload
	mov	rdi, qword ptr [rbp - 48]       # 8-byte Reload
	mov	ecx, 1
	mov	rdx, rcx
	call	cc_wrong_number_of_arguments@PLT
.Ltmp16:
.LBB3_2:                                # %enough-arguments3
	#DEBUG_VALUE: LAMBDA^COMMON-LISP^FN^^-xep:rsa-closure <- [DW_OP_constu 48, DW_OP_minus] [$rbp+0]
	mov	rax, qword ptr [rbp - 32]       # 8-byte Reload
	.loc	1 48 9                          # test.lisp:48:9
	mov	rdi, qword ptr [rax]
	call	"LAMBDA^COMMON-LISP^FN^^-lcl.1"
	add	rsp, 48
	pop	rbp
	.cfi_def_cfa rsp, 8
	ret
.Ltmp17:
.Lfunc_end3:
	.size	"LAMBDA^COMMON-LISP^FN^^-xep", .Lfunc_end3-"LAMBDA^COMMON-LISP^FN^^-xep"
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90                         # -- Begin function LAMBDA^COMMON-LISP^FN^^-xep1
	.type	"LAMBDA^COMMON-LISP^FN^^-xep1",@function
"LAMBDA^COMMON-LISP^FN^^-xep1":         # @"LAMBDA^COMMON-LISP^FN^^-xep1"
.Lfunc_begin4:
	.loc	1 48 0 is_stmt 1                # test.lisp:48:0
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	sub	rsp, 32
	mov	qword ptr [rbp - 24], rsi       # 8-byte Spill
	mov	rax, rdi
	mov	rdi, qword ptr [rbp - 24]       # 8-byte Reload
.Ltmp18:
	#DEBUG_VALUE: LAMBDA^COMMON-LISP^FN^^-xep1:rsa-arg0 <- $rdi
	.loc	1 48 9 prologue_end             # test.lisp:48:9
.Ltmp19:
	mov	qword ptr [rbp - 16], rax
	mov	qword ptr [rbp - 8], rdi
	call	"LAMBDA^COMMON-LISP^FN^^-lcl.1"
.Ltmp20:
	#DEBUG_VALUE: LAMBDA^COMMON-LISP^FN^^-xep1:rsa-closure <- undef
	add	rsp, 32
	pop	rbp
	.cfi_def_cfa rsp, 8
	ret
.Ltmp21:
.Lfunc_end4:
	.size	"LAMBDA^COMMON-LISP^FN^^-xep1", .Lfunc_end4-"LAMBDA^COMMON-LISP^FN^^-xep1"
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90                         # -- Begin function LAMBDA^COMMON-LISP^FN^^-lcl.2
	.type	"LAMBDA^COMMON-LISP^FN^^-lcl.2",@function
"LAMBDA^COMMON-LISP^FN^^-lcl.2":        # @"LAMBDA^COMMON-LISP^FN^^-lcl.2"
.Lfunc_begin5:
	.loc	1 1320 0                        # test.lisp:1320:0
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	sub	rsp, 16
	mov	rcx, rdi
.Ltmp22:
	.loc	1 1320 0 prologue_end           # test.lisp:1320:0
	mov	rdx, qword ptr [rip + __clasp_literals_6+80]
	mov	rax, rdx
	sub	rax, rsi
	cmove	rcx, rdx
	mov	qword ptr [rbp - 8], rcx        # 8-byte Spill
	mov	eax, ecx
	and	eax, 7
	sub	eax, 3
.Ltmp23:
	.loc	1 0 0 is_stmt 0                 # test.lisp:0:0
	mov	rsi, qword ptr [rip + __clasp_literals_6+120]
	mov	rax, rdx
	cmove	rax, rsi
.Ltmp24:
	.loc	1 1320 0                        # test.lisp:1320:0
	sub	rax, rdx
	setne	al
	sub	rcx, rdx
	sete	cl
	or	al, cl
	test	al, 1
	jne	.LBB5_2
# %bb.1:                                # %if-then3
	.loc	1 0 0                           # test.lisp:0:0
	mov	rdi, qword ptr [rbp - 8]        # 8-byte Reload
	.loc	1 1320 0                        # test.lisp:1320:0
	mov	rsi, qword ptr [__clasp_literals_6+104]
	call	cc_error_type_error@PLT
.LBB5_2:                                # %if-else4
	.loc	1 0 0                           # test.lisp:0:0
	mov	rax, qword ptr [rbp - 8]        # 8-byte Reload
	.loc	1 1320 0                        # test.lisp:1320:0
	add	rsp, 16
	pop	rbp
	.cfi_def_cfa rsp, 8
	ret
.Ltmp25:
.Lfunc_end5:
	.size	"LAMBDA^COMMON-LISP^FN^^-lcl.2", .Lfunc_end5-"LAMBDA^COMMON-LISP^FN^^-lcl.2"
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90                         # -- Begin function MULLET^COMMON-LISP-USER^FN^^-lcl
	.type	"MULLET^COMMON-LISP-USER^FN^^-lcl",@function
"MULLET^COMMON-LISP-USER^FN^^-lcl":     # @"MULLET^COMMON-LISP-USER^FN^^-lcl"
.Lfunc_begin6:
	.loc	1 47 0 is_stmt 1                # test.lisp:47:0
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	mov	rdx, rdi
.Ltmp26:
	.loc	1 48 2 prologue_end             # test.lisp:48:2
	mov	rax, qword ptr [rip + __clasp_literals_6+88]
	mov	rdi, qword ptr [rax + 31]
	.loc	1 48 9 is_stmt 0                # test.lisp:48:9
	mov	rsi, qword ptr [rip + __clasp_literals_6+136]
	.loc	1 48 1                          # test.lisp:48:1
	mov	rax, qword ptr [rdi + 7]
	mov	rax, qword ptr [rax + 47]
	pop	rbp
	.cfi_def_cfa rsp, 8
	jmp	rax                             # TAILCALL
.Ltmp27:
.Lfunc_end6:
	.size	"MULLET^COMMON-LISP-USER^FN^^-lcl", .Lfunc_end6-"MULLET^COMMON-LISP-USER^FN^^-lcl"
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90                         # -- Begin function MULLET^COMMON-LISP-USER^FN^^-xep
	.type	"MULLET^COMMON-LISP-USER^FN^^-xep",@function
"MULLET^COMMON-LISP-USER^FN^^-xep":     # @"MULLET^COMMON-LISP-USER^FN^^-xep"
.Lfunc_begin7:
	.loc	1 47 0 is_stmt 1                # test.lisp:47:0
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	sub	rsp, 48
	mov	qword ptr [rbp - 48], rdi       # 8-byte Spill
.Ltmp28:
	#DEBUG_VALUE: MULLET^COMMON-LISP-USER^FN^^-xep:rsa-closure <- [DW_OP_constu 48, DW_OP_minus] [$rbp+0]
	mov	qword ptr [rbp - 40], rsi       # 8-byte Spill
	mov	qword ptr [rbp - 32], rdx       # 8-byte Spill
.Ltmp29:
	.loc	1 47 0 prologue_end             # test.lisp:47:0
.Ltmp30:
	mov	qword ptr [rbp - 24], rdi
.Ltmp31:
	#DEBUG_VALUE: MULLET^COMMON-LISP-USER^FN^^-xep:rsa-closure <- $rdi
	mov	qword ptr [rbp - 16], rsi
	mov	qword ptr [rbp - 8], rdx
	cmp	rsi, 1
.Ltmp32:
	#DEBUG_VALUE: MULLET^COMMON-LISP-USER^FN^^-xep:rsa-closure <- [DW_OP_constu 48, DW_OP_minus] [$rbp+0]
	je	.LBB7_2
.Ltmp33:
# %bb.1:                                # %wrong-num-args
	#DEBUG_VALUE: MULLET^COMMON-LISP-USER^FN^^-xep:rsa-closure <- [DW_OP_constu 48, DW_OP_minus] [$rbp+0]
	.loc	1 0 0 is_stmt 0                 # test.lisp:0:0
	mov	rsi, qword ptr [rbp - 40]       # 8-byte Reload
	mov	rdi, qword ptr [rbp - 48]       # 8-byte Reload
	mov	ecx, 1
	mov	rdx, rcx
	call	cc_wrong_number_of_arguments@PLT
.Ltmp34:
.LBB7_2:                                # %enough-arguments3
	#DEBUG_VALUE: MULLET^COMMON-LISP-USER^FN^^-xep:rsa-closure <- [DW_OP_constu 48, DW_OP_minus] [$rbp+0]
	mov	rax, qword ptr [rbp - 32]       # 8-byte Reload
	.loc	1 47 0                          # test.lisp:47:0
	mov	rdi, qword ptr [rax]
	call	"MULLET^COMMON-LISP-USER^FN^^-lcl"
	add	rsp, 48
	pop	rbp
	.cfi_def_cfa rsp, 8
	ret
.Ltmp35:
.Lfunc_end7:
	.size	"MULLET^COMMON-LISP-USER^FN^^-xep", .Lfunc_end7-"MULLET^COMMON-LISP-USER^FN^^-xep"
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90                         # -- Begin function MULLET^COMMON-LISP-USER^FN^^-xep1
	.type	"MULLET^COMMON-LISP-USER^FN^^-xep1",@function
"MULLET^COMMON-LISP-USER^FN^^-xep1":    # @"MULLET^COMMON-LISP-USER^FN^^-xep1"
.Lfunc_begin8:
	.loc	1 47 0 is_stmt 1                # test.lisp:47:0
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	sub	rsp, 32
	mov	qword ptr [rbp - 24], rsi       # 8-byte Spill
	mov	rax, rdi
	mov	rdi, qword ptr [rbp - 24]       # 8-byte Reload
.Ltmp36:
	#DEBUG_VALUE: MULLET^COMMON-LISP-USER^FN^^-xep1:rsa-arg0 <- $rdi
	.loc	1 47 0 prologue_end             # test.lisp:47:0
.Ltmp37:
	mov	qword ptr [rbp - 16], rax
	mov	qword ptr [rbp - 8], rdi
	call	"MULLET^COMMON-LISP-USER^FN^^-lcl"
.Ltmp38:
	#DEBUG_VALUE: MULLET^COMMON-LISP-USER^FN^^-xep1:rsa-closure <- undef
	add	rsp, 32
	pop	rbp
	.cfi_def_cfa rsp, 8
	ret
.Ltmp39:
.Lfunc_end8:
	.size	"MULLET^COMMON-LISP-USER^FN^^-xep1", .Lfunc_end8-"MULLET^COMMON-LISP-USER^FN^^-xep1"
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90                         # -- Begin function LAMBDA^COMMON-LISP^FN^^-lcl.3
	.type	"LAMBDA^COMMON-LISP^FN^^-lcl.3",@function
"LAMBDA^COMMON-LISP^FN^^-lcl.3":        # @"LAMBDA^COMMON-LISP^FN^^-lcl.3"
.Lfunc_begin9:
	.loc	1 1319 0                        # test.lisp:1319:0
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	sub	rsp, 16
	mov	rcx, rdi
.Ltmp40:
	.loc	1 1319 0 prologue_end           # test.lisp:1319:0
	mov	rdx, qword ptr [rip + __clasp_literals_6+80]
	mov	rax, rdx
	sub	rax, rsi
	cmove	rcx, rdx
	mov	qword ptr [rbp - 8], rcx        # 8-byte Spill
	mov	eax, ecx
	and	eax, 7
	sub	eax, 3
.Ltmp41:
	.loc	1 0 0 is_stmt 0                 # test.lisp:0:0
	mov	rsi, qword ptr [rip + __clasp_literals_6+120]
	mov	rax, rdx
	cmove	rax, rsi
.Ltmp42:
	.loc	1 1319 0                        # test.lisp:1319:0
	sub	rax, rdx
	setne	al
	sub	rcx, rdx
	sete	cl
	or	al, cl
	test	al, 1
	jne	.LBB9_2
# %bb.1:                                # %if-then3
	.loc	1 0 0                           # test.lisp:0:0
	mov	rdi, qword ptr [rbp - 8]        # 8-byte Reload
	.loc	1 1319 0                        # test.lisp:1319:0
	mov	rsi, qword ptr [__clasp_literals_6+104]
	call	cc_error_type_error@PLT
.LBB9_2:                                # %if-else4
	.loc	1 0 0                           # test.lisp:0:0
	mov	rax, qword ptr [rbp - 8]        # 8-byte Reload
	.loc	1 1319 0                        # test.lisp:1319:0
	add	rsp, 16
	pop	rbp
	.cfi_def_cfa rsp, 8
	ret
.Ltmp43:
.Lfunc_end9:
	.size	"LAMBDA^COMMON-LISP^FN^^-lcl.3", .Lfunc_end9-"LAMBDA^COMMON-LISP^FN^^-lcl.3"
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90                         # -- Begin function ___LAMBDA___test.lisp-^1^7-lcl
	.type	"___LAMBDA___test.lisp-^1^7-lcl",@function
"___LAMBDA___test.lisp-^1^7-lcl":       # @"___LAMBDA___test.lisp-^1^7-lcl"
.Lfunc_begin10:
	.loc	1 47 0 is_stmt 1                # test.lisp:47:0
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
.Ltmp44:
	.loc	1 47 0 prologue_end             # test.lisp:47:0
	mov	rax, qword ptr [rip + __clasp_literals_6+112]
	mov	rdi, qword ptr [rax + 39]
	mov	rsi, qword ptr [__clasp_literals_6+144]
	.loc	1 47 7 is_stmt 0                # test.lisp:47:7
	mov	rdx, qword ptr [__clasp_literals_6+128]
	.loc	1 47 0                          # test.lisp:47:0
	mov	rax, qword ptr [rdi + 7]
	call	qword ptr [rax + 47]
	.loc	1 47 7                          # test.lisp:47:7
	mov	rax, qword ptr [__clasp_literals_6+128]
	.loc	1 47 0                          # test.lisp:47:0
	pop	rbp
	.cfi_def_cfa rsp, 8
	ret
.Ltmp45:
.Lfunc_end10:
	.size	"___LAMBDA___test.lisp-^1^7-lcl", .Lfunc_end10-"___LAMBDA___test.lisp-^1^7-lcl"
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90                         # -- Begin function ___LAMBDA___test.lisp-^1^7-xep
	.type	"___LAMBDA___test.lisp-^1^7-xep",@function
"___LAMBDA___test.lisp-^1^7-xep":       # @"___LAMBDA___test.lisp-^1^7-xep"
.Lfunc_begin11:
	.loc	1 47 0 is_stmt 1                # test.lisp:47:0
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	sub	rsp, 48
	mov	qword ptr [rbp - 40], rdi       # 8-byte Spill
.Ltmp46:
	#DEBUG_VALUE: ___LAMBDA___test.lisp-^1^7-xep:rsa-closure <- [DW_OP_constu 40, DW_OP_minus] [$rbp+0]
	mov	qword ptr [rbp - 32], rsi       # 8-byte Spill
.Ltmp47:
	.loc	1 47 0 prologue_end             # test.lisp:47:0
.Ltmp48:
	mov	qword ptr [rbp - 24], rdi
.Ltmp49:
	#DEBUG_VALUE: ___LAMBDA___test.lisp-^1^7-xep:rsa-closure <- $rdi
	mov	qword ptr [rbp - 16], rsi
	mov	qword ptr [rbp - 8], rdx
	cmp	rsi, 0
.Ltmp50:
	#DEBUG_VALUE: ___LAMBDA___test.lisp-^1^7-xep:rsa-closure <- [DW_OP_constu 40, DW_OP_minus] [$rbp+0]
	je	.LBB11_2
.Ltmp51:
# %bb.1:                                # %wrong-num-args
	#DEBUG_VALUE: ___LAMBDA___test.lisp-^1^7-xep:rsa-closure <- [DW_OP_constu 40, DW_OP_minus] [$rbp+0]
	.loc	1 0 0 is_stmt 0                 # test.lisp:0:0
	mov	rsi, qword ptr [rbp - 32]       # 8-byte Reload
	mov	rdi, qword ptr [rbp - 40]       # 8-byte Reload
	xor	eax, eax
	mov	ecx, eax
	mov	rdx, rcx
	call	cc_wrong_number_of_arguments@PLT
.Ltmp52:
.LBB11_2:                               # %enough-arguments
	#DEBUG_VALUE: ___LAMBDA___test.lisp-^1^7-xep:rsa-closure <- [DW_OP_constu 40, DW_OP_minus] [$rbp+0]
	.loc	1 47 0                          # test.lisp:47:0
	call	"___LAMBDA___test.lisp-^1^7-lcl"
	mov	edx, 1
	add	rsp, 48
	pop	rbp
	.cfi_def_cfa rsp, 8
	ret
.Ltmp53:
.Lfunc_end11:
	.size	"___LAMBDA___test.lisp-^1^7-xep", .Lfunc_end11-"___LAMBDA___test.lisp-^1^7-xep"
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90                         # -- Begin function ___LAMBDA___test.lisp-^1^7-xep0
	.type	"___LAMBDA___test.lisp-^1^7-xep0",@function
"___LAMBDA___test.lisp-^1^7-xep0":      # @"___LAMBDA___test.lisp-^1^7-xep0"
.Lfunc_begin12:
	.loc	1 47 0 is_stmt 1                # test.lisp:47:0
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	sub	rsp, 16
.Ltmp54:
	.loc	1 47 0 prologue_end             # test.lisp:47:0
.Ltmp55:
	mov	qword ptr [rbp - 8], rdi
	call	"___LAMBDA___test.lisp-^1^7-lcl"
	mov	edx, 1
	#DEBUG_VALUE: ___LAMBDA___test.lisp-^1^7-xep0:rsa-closure <- undef
	add	rsp, 16
	pop	rbp
	.cfi_def_cfa rsp, 8
	ret
.Ltmp56:
.Lfunc_end12:
	.size	"___LAMBDA___test.lisp-^1^7-xep0", .Lfunc_end12-"___LAMBDA___test.lisp-^1^7-xep0"
	.cfi_endproc
                                        # -- End function
	.section	".text.<<clasp_startup>>_1","axR",@progbits
	.globl	"<<clasp_startup>>_1"           # -- Begin function <<clasp_startup>>_1
	.p2align	4, 0x90
	.type	"<<clasp_startup>>_1",@function
"<<clasp_startup>>_1":                  # @"<<clasp_startup>>_1"
.Lfunc_begin13:
	.cfi_startproc
# %bb.0:                                # %entry
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset rbp, -16
	mov	rbp, rsp
	.cfi_def_cfa_register rbp
	mov	esi, offset "RUN-ALL/home/mixotricha/workspace/s100_stuff/lisp/test.lisp"
	mov	edi, 1
	pop	rbp
	.cfi_def_cfa rsp, 8
	jmp	cc_register_startup_function@PLT # TAILCALL
.Lfunc_end13:
	.size	"<<clasp_startup>>_1", .Lfunc_end13-"<<clasp_startup>>_1"
	.cfi_endproc
                                        # -- End function
	.type	__clasp_gcroots_in_module_4,@object # @__clasp_gcroots_in_module_4
	.local	__clasp_gcroots_in_module_4
	.comm	__clasp_gcroots_in_module_4,48,16
	.type	"startup-byte-code-0",@object   # @startup-byte-code-0
	.section	.rodata,"a",@progbits
	.p2align	4
"startup-byte-code-0":
	.asciz	"Al1\nHt01\013Tt1\0011\bABSOLUTETt1\0021\007KEYWORDOt1\003t1\002Rt1\004t1\001t1\003Tt1\0051\004homeTt1\0061\nmixotrichaTt1\0071\tworkspaceTt1\b1\ns100_stuffTt1\t1\004lispTt1\n1\005claspTt1\0131\003srcTt1\f1\006kernelTt1\r1\007cleavirIt01\013t1\004t1\005t1\006t1\007t1\bt1\tt1\nt1\013t1\tt1\ft1\rTt1\0161\006inlineTt1\0171\006NEWESTRt1\020t1\017t1\003Ut1\021l1\nl1\nt0t1\016t1\tt1\020Ht1\0221\002Tt1\0231\006LAMBDATt1\0241\013COMMON-LISPOt1\025t1\024Rt1\026t1\023t1\025Ht1\0271\004Tt1\0301\t&OPTIONALRt1\031t1\030t1\025Tt1\0321\005VALUETt1\0331\rCLASP-CLEAVIROt1\034t1\033Rt1\035t1\032t1\034Tt1\0361\005&RESTRt1\037t1\036t1\025Tt1 1\003IGNRt1!t1 t1\034It1\0271\004t1\031t1\035t1\037t1!It1\0221\002t1\026t1\027Ht1\"1\004It1\"1\004t1\031t1\035t1\037t1!Vt1#t1\021t1\022t1\"l1\nl1\n2(\0051\0013 \n\001Xl00t1#Ht1$1\006It1$1\006t1\004t1\005t1\006t1\007t1\bt1\tTt1%1\004testUt1&l1\nl1\nt1$t1%t1\tt1\020Ht1'1\002Ht1(1\001Tt1)1\001XTt1*1\020COMMON-LISP-USEROt1+t1*Rt1,t1)t1+It1(1\001t1,It1'1\002t1\026t1(Ht1-1\001It1-1\001t1,Vt1.t1&t1't1-l1\nl1\n101\n2\032\004Xl1\0011\001t1.Wl1\0021\002t1.1\001Ht1/1\002Ht101\004It101\004t1\031t1\035t1\037t1!It1/1\002t1\026t10Ht111\004It111\004t1\031t1\035t1\037t1!Vt12t1\021t1/t11l1\nl1\n2(\0051\0013 \n\001Xl1\0031\tt12Tt131\006MULLETRl1\020t13t1+Ht141\001Tt151\003LSTRt16t15t1+It141\001t16Vt17t1&l1\020t14l1\nl1\n1/1\0012\375\003Xl1\0041\nt17Wl1\0051\013t171\004Ht181\002Ht191\004It191\004t1\031t1\035t1\037t1!It181\002t1\026t19Ht1:1\004It1:1\004t1\031t1\035t1\037t1!Vt1;t1\021t18t1:l1\nl1\n2'\0051\0013\f\n\001Xl1\0061\022t1;Tt1<1\tTOP-LEVELTt1=1\004COREOt1>t1=Rt1?t1<t1>Vt1@t1&t1?l1\nl1\nl1\n1/1\0012\375\003Xl1\0071\023t1@Wl1\b1\024t1@1\007Tt1A1\005PRINTRl1\tt1At1\025Tt1B1\006MAPCARRl1\013t1Bt1\025Tt1C1\tTWO-ARG-*Rl1\ft1Ct1>Tt1D1\004LISTRl1\rt1Dt1\025Tt1E1\013FDEFINITIONRl1\016t1Et1\025Bl1\017\\l1\0211\002\\l1\0221\005b1\b1\032___LAMBDA___test.lisp-^1^7\000"
	.size	"startup-byte-code-0", 1279

	.type	__clasp_literals_6,@object      # @__clasp_literals_6
	.local	__clasp_literals_6
	.comm	__clasp_literals_6,152,16
	.type	"function-vector-0",@object     # @function-vector-0
	.data
	.p2align	4
"function-vector-0":
	.quad	"LAMBDA^COMMON-LISP^FN^^-lcl"
	.quad	"LAMBDA^COMMON-LISP^FN^^-lcl.1"
	.quad	"LAMBDA^COMMON-LISP^FN^^-xep"
	.quad	general_entry_point_redirect_0
	.quad	"LAMBDA^COMMON-LISP^FN^^-xep1"
	.quad	general_entry_point_redirect_2
	.quad	general_entry_point_redirect_3
	.quad	general_entry_point_redirect_4
	.quad	general_entry_point_redirect_5
	.quad	"LAMBDA^COMMON-LISP^FN^^-lcl.2"
	.quad	"MULLET^COMMON-LISP-USER^FN^^-lcl"
	.quad	"MULLET^COMMON-LISP-USER^FN^^-xep"
	.quad	general_entry_point_redirect_0
	.quad	"MULLET^COMMON-LISP-USER^FN^^-xep1"
	.quad	general_entry_point_redirect_2
	.quad	general_entry_point_redirect_3
	.quad	general_entry_point_redirect_4
	.quad	general_entry_point_redirect_5
	.quad	"LAMBDA^COMMON-LISP^FN^^-lcl.3"
	.quad	"___LAMBDA___test.lisp-^1^7-lcl"
	.quad	"___LAMBDA___test.lisp-^1^7-xep"
	.quad	"___LAMBDA___test.lisp-^1^7-xep0"
	.quad	general_entry_point_redirect_1
	.quad	general_entry_point_redirect_2
	.quad	general_entry_point_redirect_3
	.quad	general_entry_point_redirect_4
	.quad	general_entry_point_redirect_5
	.size	"function-vector-0", 216

	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
	.quad	.Ltmp10-.Lfunc_begin0
	.quad	.Ltmp13-.Lfunc_begin0
	.short	2                               # Loc expr size
	.byte	118                             # DW_OP_breg6
	.byte	80                              # -48
	.quad	.Ltmp13-.Lfunc_begin0
	.quad	.Ltmp14-.Lfunc_begin0
	.short	1                               # Loc expr size
	.byte	85                              # DW_OP_reg5
	.quad	.Ltmp14-.Lfunc_begin0
	.quad	.Lfunc_end3-.Lfunc_begin0
	.short	2                               # Loc expr size
	.byte	118                             # DW_OP_breg6
	.byte	80                              # -48
	.quad	0
	.quad	0
.Ldebug_loc1:
	.quad	.Ltmp18-.Lfunc_begin0
	.quad	.Ltmp20-.Lfunc_begin0
	.short	1                               # Loc expr size
	.byte	85                              # DW_OP_reg5
	.quad	0
	.quad	0
.Ldebug_loc2:
	.quad	.Ltmp28-.Lfunc_begin0
	.quad	.Ltmp31-.Lfunc_begin0
	.short	2                               # Loc expr size
	.byte	118                             # DW_OP_breg6
	.byte	80                              # -48
	.quad	.Ltmp31-.Lfunc_begin0
	.quad	.Ltmp32-.Lfunc_begin0
	.short	1                               # Loc expr size
	.byte	85                              # DW_OP_reg5
	.quad	.Ltmp32-.Lfunc_begin0
	.quad	.Lfunc_end7-.Lfunc_begin0
	.short	2                               # Loc expr size
	.byte	118                             # DW_OP_breg6
	.byte	80                              # -48
	.quad	0
	.quad	0
.Ldebug_loc3:
	.quad	.Ltmp36-.Lfunc_begin0
	.quad	.Ltmp38-.Lfunc_begin0
	.short	1                               # Loc expr size
	.byte	85                              # DW_OP_reg5
	.quad	0
	.quad	0
.Ldebug_loc4:
	.quad	.Ltmp46-.Lfunc_begin0
	.quad	.Ltmp49-.Lfunc_begin0
	.short	2                               # Loc expr size
	.byte	118                             # DW_OP_breg6
	.byte	88                              # -40
	.quad	.Ltmp49-.Lfunc_begin0
	.quad	.Ltmp50-.Lfunc_begin0
	.short	1                               # Loc expr size
	.byte	85                              # DW_OP_reg5
	.quad	.Ltmp50-.Lfunc_begin0
	.quad	.Lfunc_end11-.Lfunc_begin0
	.short	2                               # Loc expr size
	.byte	118                             # DW_OP_breg6
	.byte	88                              # -40
	.quad	0
	.quad	0
	.section	.debug_abbrev,"",@progbits
	.byte	1                               # Abbreviation Code
	.byte	17                              # DW_TAG_compile_unit
	.byte	1                               # DW_CHILDREN_yes
	.byte	37                              # DW_AT_producer
	.byte	14                              # DW_FORM_strp
	.byte	19                              # DW_AT_language
	.byte	5                               # DW_FORM_data2
	.byte	3                               # DW_AT_name
	.byte	14                              # DW_FORM_strp
	.byte	16                              # DW_AT_stmt_list
	.byte	23                              # DW_FORM_sec_offset
	.byte	27                              # DW_AT_comp_dir
	.byte	14                              # DW_FORM_strp
	.ascii	"\264B"                         # DW_AT_GNU_pubnames
	.byte	25                              # DW_FORM_flag_present
	.byte	17                              # DW_AT_low_pc
	.byte	1                               # DW_FORM_addr
	.byte	18                              # DW_AT_high_pc
	.byte	6                               # DW_FORM_data4
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	2                               # Abbreviation Code
	.byte	46                              # DW_TAG_subprogram
	.byte	0                               # DW_CHILDREN_no
	.byte	17                              # DW_AT_low_pc
	.byte	1                               # DW_FORM_addr
	.byte	18                              # DW_AT_high_pc
	.byte	6                               # DW_FORM_data4
	.byte	64                              # DW_AT_frame_base
	.byte	24                              # DW_FORM_exprloc
	.byte	110                             # DW_AT_linkage_name
	.byte	14                              # DW_FORM_strp
	.byte	3                               # DW_AT_name
	.byte	14                              # DW_FORM_strp
	.byte	73                              # DW_AT_type
	.byte	19                              # DW_FORM_ref4
	.byte	63                              # DW_AT_external
	.byte	25                              # DW_FORM_flag_present
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	3                               # Abbreviation Code
	.byte	46                              # DW_TAG_subprogram
	.byte	0                               # DW_CHILDREN_no
	.byte	110                             # DW_AT_linkage_name
	.byte	14                              # DW_FORM_strp
	.byte	3                               # DW_AT_name
	.byte	14                              # DW_FORM_strp
	.byte	58                              # DW_AT_decl_file
	.byte	11                              # DW_FORM_data1
	.byte	59                              # DW_AT_decl_line
	.byte	5                               # DW_FORM_data2
	.byte	73                              # DW_AT_type
	.byte	19                              # DW_FORM_ref4
	.byte	63                              # DW_AT_external
	.byte	25                              # DW_FORM_flag_present
	.byte	32                              # DW_AT_inline
	.byte	11                              # DW_FORM_data1
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	4                               # Abbreviation Code
	.byte	36                              # DW_TAG_base_type
	.byte	0                               # DW_CHILDREN_no
	.byte	3                               # DW_AT_name
	.byte	14                              # DW_FORM_strp
	.byte	62                              # DW_AT_encoding
	.byte	11                              # DW_FORM_data1
	.byte	11                              # DW_AT_byte_size
	.byte	11                              # DW_FORM_data1
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	5                               # Abbreviation Code
	.byte	46                              # DW_TAG_subprogram
	.byte	1                               # DW_CHILDREN_yes
	.byte	17                              # DW_AT_low_pc
	.byte	1                               # DW_FORM_addr
	.byte	18                              # DW_AT_high_pc
	.byte	6                               # DW_FORM_data4
	.byte	64                              # DW_AT_frame_base
	.byte	24                              # DW_FORM_exprloc
	.byte	49                              # DW_AT_abstract_origin
	.byte	19                              # DW_FORM_ref4
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	6                               # Abbreviation Code
	.byte	29                              # DW_TAG_inlined_subroutine
	.byte	0                               # DW_CHILDREN_no
	.byte	49                              # DW_AT_abstract_origin
	.byte	19                              # DW_FORM_ref4
	.byte	85                              # DW_AT_ranges
	.byte	23                              # DW_FORM_sec_offset
	.byte	88                              # DW_AT_call_file
	.byte	11                              # DW_FORM_data1
	.byte	89                              # DW_AT_call_line
	.byte	11                              # DW_FORM_data1
	.byte	87                              # DW_AT_call_column
	.byte	11                              # DW_FORM_data1
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	7                               # Abbreviation Code
	.byte	46                              # DW_TAG_subprogram
	.byte	0                               # DW_CHILDREN_no
	.byte	110                             # DW_AT_linkage_name
	.byte	14                              # DW_FORM_strp
	.byte	3                               # DW_AT_name
	.byte	14                              # DW_FORM_strp
	.byte	58                              # DW_AT_decl_file
	.byte	11                              # DW_FORM_data1
	.byte	59                              # DW_AT_decl_line
	.byte	11                              # DW_FORM_data1
	.byte	73                              # DW_AT_type
	.byte	19                              # DW_FORM_ref4
	.byte	63                              # DW_AT_external
	.byte	25                              # DW_FORM_flag_present
	.byte	32                              # DW_AT_inline
	.byte	11                              # DW_FORM_data1
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	8                               # Abbreviation Code
	.byte	29                              # DW_TAG_inlined_subroutine
	.byte	0                               # DW_CHILDREN_no
	.byte	49                              # DW_AT_abstract_origin
	.byte	19                              # DW_FORM_ref4
	.byte	17                              # DW_AT_low_pc
	.byte	1                               # DW_FORM_addr
	.byte	18                              # DW_AT_high_pc
	.byte	6                               # DW_FORM_data4
	.byte	88                              # DW_AT_call_file
	.byte	11                              # DW_FORM_data1
	.byte	89                              # DW_AT_call_line
	.byte	11                              # DW_FORM_data1
	.byte	87                              # DW_AT_call_column
	.byte	11                              # DW_FORM_data1
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	9                               # Abbreviation Code
	.byte	46                              # DW_TAG_subprogram
	.byte	1                               # DW_CHILDREN_yes
	.byte	17                              # DW_AT_low_pc
	.byte	1                               # DW_FORM_addr
	.byte	18                              # DW_AT_high_pc
	.byte	6                               # DW_FORM_data4
	.byte	64                              # DW_AT_frame_base
	.byte	24                              # DW_FORM_exprloc
	.byte	110                             # DW_AT_linkage_name
	.byte	14                              # DW_FORM_strp
	.byte	3                               # DW_AT_name
	.byte	14                              # DW_FORM_strp
	.byte	58                              # DW_AT_decl_file
	.byte	11                              # DW_FORM_data1
	.byte	59                              # DW_AT_decl_line
	.byte	11                              # DW_FORM_data1
	.byte	73                              # DW_AT_type
	.byte	19                              # DW_FORM_ref4
	.byte	63                              # DW_AT_external
	.byte	25                              # DW_FORM_flag_present
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	10                              # Abbreviation Code
	.byte	5                               # DW_TAG_formal_parameter
	.byte	0                               # DW_CHILDREN_no
	.byte	2                               # DW_AT_location
	.byte	23                              # DW_FORM_sec_offset
	.byte	3                               # DW_AT_name
	.byte	14                              # DW_FORM_strp
	.byte	58                              # DW_AT_decl_file
	.byte	11                              # DW_FORM_data1
	.byte	59                              # DW_AT_decl_line
	.byte	11                              # DW_FORM_data1
	.byte	73                              # DW_AT_type
	.byte	19                              # DW_FORM_ref4
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	11                              # Abbreviation Code
	.byte	5                               # DW_TAG_formal_parameter
	.byte	0                               # DW_CHILDREN_no
	.byte	3                               # DW_AT_name
	.byte	14                              # DW_FORM_strp
	.byte	58                              # DW_AT_decl_file
	.byte	11                              # DW_FORM_data1
	.byte	59                              # DW_AT_decl_line
	.byte	11                              # DW_FORM_data1
	.byte	73                              # DW_AT_type
	.byte	19                              # DW_FORM_ref4
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	12                              # Abbreviation Code
	.byte	46                              # DW_TAG_subprogram
	.byte	0                               # DW_CHILDREN_no
	.byte	17                              # DW_AT_low_pc
	.byte	1                               # DW_FORM_addr
	.byte	18                              # DW_AT_high_pc
	.byte	6                               # DW_FORM_data4
	.byte	64                              # DW_AT_frame_base
	.byte	24                              # DW_FORM_exprloc
	.byte	110                             # DW_AT_linkage_name
	.byte	14                              # DW_FORM_strp
	.byte	3                               # DW_AT_name
	.byte	14                              # DW_FORM_strp
	.byte	58                              # DW_AT_decl_file
	.byte	11                              # DW_FORM_data1
	.byte	59                              # DW_AT_decl_line
	.byte	11                              # DW_FORM_data1
	.byte	73                              # DW_AT_type
	.byte	19                              # DW_FORM_ref4
	.byte	63                              # DW_AT_external
	.byte	25                              # DW_FORM_flag_present
	.byte	0                               # EOM(1)
	.byte	0                               # EOM(2)
	.byte	0                               # EOM(3)
	.section	.debug_info,"",@progbits
.Lcu_begin0:
	.long	.Ldebug_info_end0-.Ldebug_info_start0 # Length of Unit
.Ldebug_info_start0:
	.short	4                               # DWARF version number
	.long	.debug_abbrev                   # Offset Into Abbrev. Section
	.byte	8                               # Address Size (in bytes)
	.byte	1                               # Abbrev [1] 0xb:0x2c4 DW_TAG_compile_unit
	.long	.Linfo_string0                  # DW_AT_producer
	.short	4                               # DW_AT_language
	.long	.Linfo_string1                  # DW_AT_name
	.long	.Lline_table_start0             # DW_AT_stmt_list
	.long	.Linfo_string2                  # DW_AT_comp_dir
                                        # DW_AT_GNU_pubnames
	.quad	.Lfunc_begin0                   # DW_AT_low_pc
	.long	.Lfunc_end12-.Lfunc_begin0      # DW_AT_high_pc
	.byte	2                               # Abbrev [2] 0x2a:0x1b DW_TAG_subprogram
	.quad	.Lfunc_begin0                   # DW_AT_low_pc
	.long	.Lfunc_end0-.Lfunc_begin0       # DW_AT_high_pc
	.byte	1                               # DW_AT_frame_base
	.byte	86
	.long	.Linfo_string8                  # DW_AT_linkage_name
	.long	.Linfo_string8                  # DW_AT_name
	.long	86                              # DW_AT_type
                                        # DW_AT_external
	.byte	3                               # Abbrev [3] 0x45:0x11 DW_TAG_subprogram
	.long	.Linfo_string3                  # DW_AT_linkage_name
	.long	.Linfo_string3                  # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.short	1320                            # DW_AT_decl_line
	.long	86                              # DW_AT_type
                                        # DW_AT_external
	.byte	1                               # DW_AT_inline
	.byte	4                               # Abbrev [4] 0x56:0x7 DW_TAG_base_type
	.long	.Linfo_string4                  # DW_AT_name
	.byte	13                              # DW_AT_encoding
	.byte	8                               # DW_AT_byte_size
	.byte	5                               # Abbrev [5] 0x5d:0x20 DW_TAG_subprogram
	.quad	.Lfunc_begin1                   # DW_AT_low_pc
	.long	.Lfunc_end1-.Lfunc_begin1       # DW_AT_high_pc
	.byte	1                               # DW_AT_frame_base
	.byte	86
	.long	69                              # DW_AT_abstract_origin
	.byte	6                               # Abbrev [6] 0x70:0xc DW_TAG_inlined_subroutine
	.long	69                              # DW_AT_abstract_origin
	.long	.Ldebug_ranges0                 # DW_AT_ranges
	.byte	1                               # DW_AT_call_file
	.byte	48                              # DW_AT_call_line
	.byte	41                              # DW_AT_call_column
	.byte	0                               # End Of Children Mark
	.byte	7                               # Abbrev [7] 0x7d:0x10 DW_TAG_subprogram
	.long	.Linfo_string5                  # DW_AT_linkage_name
	.long	.Linfo_string5                  # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	48                              # DW_AT_decl_line
	.long	86                              # DW_AT_type
                                        # DW_AT_external
	.byte	1                               # DW_AT_inline
	.byte	5                               # Abbrev [5] 0x8d:0x3c DW_TAG_subprogram
	.quad	.Lfunc_begin2                   # DW_AT_low_pc
	.long	.Lfunc_end2-.Lfunc_begin2       # DW_AT_high_pc
	.byte	1                               # DW_AT_frame_base
	.byte	86
	.long	125                             # DW_AT_abstract_origin
	.byte	8                               # Abbrev [8] 0xa0:0x14 DW_TAG_inlined_subroutine
	.long	125                             # DW_AT_abstract_origin
	.quad	.Ltmp5                          # DW_AT_low_pc
	.long	.Ltmp6-.Ltmp5                   # DW_AT_high_pc
	.byte	1                               # DW_AT_call_file
	.byte	48                              # DW_AT_call_line
	.byte	31                              # DW_AT_call_column
	.byte	8                               # Abbrev [8] 0xb4:0x14 DW_TAG_inlined_subroutine
	.long	125                             # DW_AT_abstract_origin
	.quad	.Ltmp7                          # DW_AT_low_pc
	.long	.Ltmp8-.Ltmp7                   # DW_AT_high_pc
	.byte	1                               # DW_AT_call_file
	.byte	48                              # DW_AT_call_line
	.byte	41                              # DW_AT_call_column
	.byte	0                               # End Of Children Mark
	.byte	9                               # Abbrev [9] 0xc9:0x43 DW_TAG_subprogram
	.quad	.Lfunc_begin3                   # DW_AT_low_pc
	.long	.Lfunc_end3-.Lfunc_begin3       # DW_AT_high_pc
	.byte	1                               # DW_AT_frame_base
	.byte	86
	.long	.Linfo_string9                  # DW_AT_linkage_name
	.long	.Linfo_string9                  # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	48                              # DW_AT_decl_line
	.long	86                              # DW_AT_type
                                        # DW_AT_external
	.byte	10                              # Abbrev [10] 0xe6:0xf DW_TAG_formal_parameter
	.long	.Ldebug_loc0                    # DW_AT_location
	.long	.Linfo_string17                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	48                              # DW_AT_decl_line
	.long	711                             # DW_AT_type
	.byte	11                              # Abbrev [11] 0xf5:0xb DW_TAG_formal_parameter
	.long	.Linfo_string19                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	48                              # DW_AT_decl_line
	.long	711                             # DW_AT_type
	.byte	11                              # Abbrev [11] 0x100:0xb DW_TAG_formal_parameter
	.long	.Linfo_string20                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	48                              # DW_AT_decl_line
	.long	711                             # DW_AT_type
	.byte	0                               # End Of Children Mark
	.byte	9                               # Abbrev [9] 0x10c:0x38 DW_TAG_subprogram
	.quad	.Lfunc_begin4                   # DW_AT_low_pc
	.long	.Lfunc_end4-.Lfunc_begin4       # DW_AT_high_pc
	.byte	1                               # DW_AT_frame_base
	.byte	86
	.long	.Linfo_string10                 # DW_AT_linkage_name
	.long	.Linfo_string10                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	48                              # DW_AT_decl_line
	.long	86                              # DW_AT_type
                                        # DW_AT_external
	.byte	11                              # Abbrev [11] 0x129:0xb DW_TAG_formal_parameter
	.long	.Linfo_string17                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	48                              # DW_AT_decl_line
	.long	711                             # DW_AT_type
	.byte	10                              # Abbrev [10] 0x134:0xf DW_TAG_formal_parameter
	.long	.Ldebug_loc1                    # DW_AT_location
	.long	.Linfo_string21                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	48                              # DW_AT_decl_line
	.long	711                             # DW_AT_type
	.byte	0                               # End Of Children Mark
	.byte	3                               # Abbrev [3] 0x144:0x11 DW_TAG_subprogram
	.long	.Linfo_string6                  # DW_AT_linkage_name
	.long	.Linfo_string6                  # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.short	1320                            # DW_AT_decl_line
	.long	86                              # DW_AT_type
                                        # DW_AT_external
	.byte	1                               # DW_AT_inline
	.byte	5                               # Abbrev [5] 0x155:0x20 DW_TAG_subprogram
	.quad	.Lfunc_begin5                   # DW_AT_low_pc
	.long	.Lfunc_end5-.Lfunc_begin5       # DW_AT_high_pc
	.byte	1                               # DW_AT_frame_base
	.byte	86
	.long	324                             # DW_AT_abstract_origin
	.byte	6                               # Abbrev [6] 0x168:0xc DW_TAG_inlined_subroutine
	.long	324                             # DW_AT_abstract_origin
	.long	.Ldebug_ranges1                 # DW_AT_ranges
	.byte	1                               # DW_AT_call_file
	.byte	48                              # DW_AT_call_line
	.byte	41                              # DW_AT_call_column
	.byte	0                               # End Of Children Mark
	.byte	12                              # Abbrev [12] 0x175:0x1d DW_TAG_subprogram
	.quad	.Lfunc_begin6                   # DW_AT_low_pc
	.long	.Lfunc_end6-.Lfunc_begin6       # DW_AT_high_pc
	.byte	1                               # DW_AT_frame_base
	.byte	86
	.long	.Linfo_string11                 # DW_AT_linkage_name
	.long	.Linfo_string11                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	47                              # DW_AT_decl_line
	.long	86                              # DW_AT_type
                                        # DW_AT_external
	.byte	9                               # Abbrev [9] 0x192:0x43 DW_TAG_subprogram
	.quad	.Lfunc_begin7                   # DW_AT_low_pc
	.long	.Lfunc_end7-.Lfunc_begin7       # DW_AT_high_pc
	.byte	1                               # DW_AT_frame_base
	.byte	86
	.long	.Linfo_string12                 # DW_AT_linkage_name
	.long	.Linfo_string12                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	47                              # DW_AT_decl_line
	.long	86                              # DW_AT_type
                                        # DW_AT_external
	.byte	10                              # Abbrev [10] 0x1af:0xf DW_TAG_formal_parameter
	.long	.Ldebug_loc2                    # DW_AT_location
	.long	.Linfo_string17                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	47                              # DW_AT_decl_line
	.long	711                             # DW_AT_type
	.byte	11                              # Abbrev [11] 0x1be:0xb DW_TAG_formal_parameter
	.long	.Linfo_string19                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	47                              # DW_AT_decl_line
	.long	711                             # DW_AT_type
	.byte	11                              # Abbrev [11] 0x1c9:0xb DW_TAG_formal_parameter
	.long	.Linfo_string20                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	47                              # DW_AT_decl_line
	.long	711                             # DW_AT_type
	.byte	0                               # End Of Children Mark
	.byte	9                               # Abbrev [9] 0x1d5:0x38 DW_TAG_subprogram
	.quad	.Lfunc_begin8                   # DW_AT_low_pc
	.long	.Lfunc_end8-.Lfunc_begin8       # DW_AT_high_pc
	.byte	1                               # DW_AT_frame_base
	.byte	86
	.long	.Linfo_string13                 # DW_AT_linkage_name
	.long	.Linfo_string13                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	47                              # DW_AT_decl_line
	.long	86                              # DW_AT_type
                                        # DW_AT_external
	.byte	11                              # Abbrev [11] 0x1f2:0xb DW_TAG_formal_parameter
	.long	.Linfo_string17                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	47                              # DW_AT_decl_line
	.long	711                             # DW_AT_type
	.byte	10                              # Abbrev [10] 0x1fd:0xf DW_TAG_formal_parameter
	.long	.Ldebug_loc3                    # DW_AT_location
	.long	.Linfo_string21                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	47                              # DW_AT_decl_line
	.long	711                             # DW_AT_type
	.byte	0                               # End Of Children Mark
	.byte	3                               # Abbrev [3] 0x20d:0x11 DW_TAG_subprogram
	.long	.Linfo_string7                  # DW_AT_linkage_name
	.long	.Linfo_string7                  # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.short	1319                            # DW_AT_decl_line
	.long	86                              # DW_AT_type
                                        # DW_AT_external
	.byte	1                               # DW_AT_inline
	.byte	5                               # Abbrev [5] 0x21e:0x20 DW_TAG_subprogram
	.quad	.Lfunc_begin9                   # DW_AT_low_pc
	.long	.Lfunc_end9-.Lfunc_begin9       # DW_AT_high_pc
	.byte	1                               # DW_AT_frame_base
	.byte	86
	.long	525                             # DW_AT_abstract_origin
	.byte	6                               # Abbrev [6] 0x231:0xc DW_TAG_inlined_subroutine
	.long	525                             # DW_AT_abstract_origin
	.long	.Ldebug_ranges2                 # DW_AT_ranges
	.byte	1                               # DW_AT_call_file
	.byte	48                              # DW_AT_call_line
	.byte	31                              # DW_AT_call_column
	.byte	0                               # End Of Children Mark
	.byte	12                              # Abbrev [12] 0x23e:0x1d DW_TAG_subprogram
	.quad	.Lfunc_begin10                  # DW_AT_low_pc
	.long	.Lfunc_end10-.Lfunc_begin10     # DW_AT_high_pc
	.byte	1                               # DW_AT_frame_base
	.byte	86
	.long	.Linfo_string14                 # DW_AT_linkage_name
	.long	.Linfo_string14                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	47                              # DW_AT_decl_line
	.long	86                              # DW_AT_type
                                        # DW_AT_external
	.byte	9                               # Abbrev [9] 0x25b:0x43 DW_TAG_subprogram
	.quad	.Lfunc_begin11                  # DW_AT_low_pc
	.long	.Lfunc_end11-.Lfunc_begin11     # DW_AT_high_pc
	.byte	1                               # DW_AT_frame_base
	.byte	86
	.long	.Linfo_string15                 # DW_AT_linkage_name
	.long	.Linfo_string15                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	47                              # DW_AT_decl_line
	.long	86                              # DW_AT_type
                                        # DW_AT_external
	.byte	10                              # Abbrev [10] 0x278:0xf DW_TAG_formal_parameter
	.long	.Ldebug_loc4                    # DW_AT_location
	.long	.Linfo_string17                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	47                              # DW_AT_decl_line
	.long	711                             # DW_AT_type
	.byte	11                              # Abbrev [11] 0x287:0xb DW_TAG_formal_parameter
	.long	.Linfo_string19                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	47                              # DW_AT_decl_line
	.long	711                             # DW_AT_type
	.byte	11                              # Abbrev [11] 0x292:0xb DW_TAG_formal_parameter
	.long	.Linfo_string20                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	47                              # DW_AT_decl_line
	.long	711                             # DW_AT_type
	.byte	0                               # End Of Children Mark
	.byte	9                               # Abbrev [9] 0x29e:0x29 DW_TAG_subprogram
	.quad	.Lfunc_begin12                  # DW_AT_low_pc
	.long	.Lfunc_end12-.Lfunc_begin12     # DW_AT_high_pc
	.byte	1                               # DW_AT_frame_base
	.byte	86
	.long	.Linfo_string16                 # DW_AT_linkage_name
	.long	.Linfo_string16                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	47                              # DW_AT_decl_line
	.long	86                              # DW_AT_type
                                        # DW_AT_external
	.byte	11                              # Abbrev [11] 0x2bb:0xb DW_TAG_formal_parameter
	.long	.Linfo_string17                 # DW_AT_name
	.byte	1                               # DW_AT_decl_file
	.byte	47                              # DW_AT_decl_line
	.long	711                             # DW_AT_type
	.byte	0                               # End Of Children Mark
	.byte	4                               # Abbrev [4] 0x2c7:0x7 DW_TAG_base_type
	.long	.Linfo_string18                 # DW_AT_name
	.byte	1                               # DW_AT_encoding
	.byte	8                               # DW_AT_byte_size
	.byte	0                               # End Of Children Mark
.Ldebug_info_end0:
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.quad	.Ltmp0-.Lfunc_begin0
	.quad	.Ltmp1-.Lfunc_begin0
	.quad	.Ltmp2-.Lfunc_begin0
	.quad	.Ltmp3-.Lfunc_begin0
	.quad	0
	.quad	0
.Ldebug_ranges1:
	.quad	.Ltmp22-.Lfunc_begin0
	.quad	.Ltmp23-.Lfunc_begin0
	.quad	.Ltmp24-.Lfunc_begin0
	.quad	.Ltmp25-.Lfunc_begin0
	.quad	0
	.quad	0
.Ldebug_ranges2:
	.quad	.Ltmp40-.Lfunc_begin0
	.quad	.Ltmp41-.Lfunc_begin0
	.quad	.Ltmp42-.Lfunc_begin0
	.quad	.Ltmp43-.Lfunc_begin0
	.quad	0
	.quad	0
	.section	.debug_str,"MS",@progbits,1
.Linfo_string0:
	.asciz	"clasp Common Lisp compiler -v" # string offset=0
.Linfo_string1:
	.asciz	"test.lisp"                     # string offset=30
.Linfo_string2:
	.asciz	"/home/mixotricha/workspace/s100_stuff/lisp" # string offset=40
.Linfo_string3:
	.asciz	"LAMBDA^COMMON-LISP^FN^^-lcl"   # string offset=83
.Linfo_string4:
	.asciz	"int"                           # string offset=111
.Linfo_string5:
	.asciz	"LAMBDA^COMMON-LISP^FN^^-lcl.1" # string offset=115
.Linfo_string6:
	.asciz	"LAMBDA^COMMON-LISP^FN^^-lcl.2" # string offset=145
.Linfo_string7:
	.asciz	"LAMBDA^COMMON-LISP^FN^^-lcl.3" # string offset=175
.Linfo_string8:
	.asciz	"RUN-ALL/home/mixotricha/workspace/s100_stuff/lisp/test.lisp" # string offset=205
.Linfo_string9:
	.asciz	"LAMBDA^COMMON-LISP^FN^^-xep"   # string offset=265
.Linfo_string10:
	.asciz	"LAMBDA^COMMON-LISP^FN^^-xep1"  # string offset=293
.Linfo_string11:
	.asciz	"MULLET^COMMON-LISP-USER^FN^^-lcl" # string offset=322
.Linfo_string12:
	.asciz	"MULLET^COMMON-LISP-USER^FN^^-xep" # string offset=355
.Linfo_string13:
	.asciz	"MULLET^COMMON-LISP-USER^FN^^-xep1" # string offset=388
.Linfo_string14:
	.asciz	"___LAMBDA___test.lisp-^1^7-lcl" # string offset=422
.Linfo_string15:
	.asciz	"___LAMBDA___test.lisp-^1^7-xep" # string offset=453
.Linfo_string16:
	.asciz	"___LAMBDA___test.lisp-^1^7-xep0" # string offset=484
.Linfo_string17:
	.asciz	"rsa-closure"                   # string offset=516
.Linfo_string18:
	.asciz	"T_O*"                          # string offset=528
.Linfo_string19:
	.asciz	"rsa-nargs"                     # string offset=533
.Linfo_string20:
	.asciz	"rsa-args"                      # string offset=543
.Linfo_string21:
	.asciz	"rsa-arg0"                      # string offset=552
	.section	.debug_pubnames,"",@progbits
	.long	.LpubNames_end0-.LpubNames_start0 # Length of Public Names Info
.LpubNames_start0:
	.short	2                               # DWARF Version
	.long	.Lcu_begin0                     # Offset of Compilation Unit Info
	.long	719                             # Compilation Unit Length
	.long	373                             # DIE offset
	.asciz	"MULLET^COMMON-LISP-USER^FN^^-lcl" # External Name
	.long	125                             # DIE offset
	.asciz	"LAMBDA^COMMON-LISP^FN^^-lcl.1" # External Name
	.long	69                              # DIE offset
	.asciz	"LAMBDA^COMMON-LISP^FN^^-lcl"   # External Name
	.long	324                             # DIE offset
	.asciz	"LAMBDA^COMMON-LISP^FN^^-lcl.2" # External Name
	.long	525                             # DIE offset
	.asciz	"LAMBDA^COMMON-LISP^FN^^-lcl.3" # External Name
	.long	268                             # DIE offset
	.asciz	"LAMBDA^COMMON-LISP^FN^^-xep1"  # External Name
	.long	469                             # DIE offset
	.asciz	"MULLET^COMMON-LISP-USER^FN^^-xep1" # External Name
	.long	603                             # DIE offset
	.asciz	"___LAMBDA___test.lisp-^1^7-xep" # External Name
	.long	42                              # DIE offset
	.asciz	"RUN-ALL/home/mixotricha/workspace/s100_stuff/lisp/test.lisp" # External Name
	.long	402                             # DIE offset
	.asciz	"MULLET^COMMON-LISP-USER^FN^^-xep" # External Name
	.long	201                             # DIE offset
	.asciz	"LAMBDA^COMMON-LISP^FN^^-xep"   # External Name
	.long	574                             # DIE offset
	.asciz	"___LAMBDA___test.lisp-^1^7-lcl" # External Name
	.long	670                             # DIE offset
	.asciz	"___LAMBDA___test.lisp-^1^7-xep0" # External Name
	.long	0                               # End Mark
.LpubNames_end0:
	.section	.debug_pubtypes,"",@progbits
	.long	.LpubTypes_end0-.LpubTypes_start0 # Length of Public Types Info
.LpubTypes_start0:
	.short	2                               # DWARF Version
	.long	.Lcu_begin0                     # Offset of Compilation Unit Info
	.long	719                             # Compilation Unit Length
	.long	86                              # DIE offset
	.asciz	"int"                           # External Name
	.long	711                             # DIE offset
	.asciz	"T_O*"                          # External Name
	.long	0                               # End Mark
.LpubTypes_end0:
	.ident	"Clasp"
	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym "RUN-ALL/home/mixotricha/workspace/s100_stuff/lisp/test.lisp"
	.addrsig_sym __gxx_personality_v0
	.addrsig_sym "LAMBDA^COMMON-LISP^FN^^-lcl"
	.addrsig_sym "LAMBDA^COMMON-LISP^FN^^-lcl.1"
	.addrsig_sym "LAMBDA^COMMON-LISP^FN^^-xep"
	.addrsig_sym "LAMBDA^COMMON-LISP^FN^^-xep1"
	.addrsig_sym general_entry_point_redirect_0
	.addrsig_sym general_entry_point_redirect_2
	.addrsig_sym general_entry_point_redirect_3
	.addrsig_sym general_entry_point_redirect_4
	.addrsig_sym general_entry_point_redirect_5
	.addrsig_sym "LAMBDA^COMMON-LISP^FN^^-lcl.2"
	.addrsig_sym "MULLET^COMMON-LISP-USER^FN^^-lcl"
	.addrsig_sym "MULLET^COMMON-LISP-USER^FN^^-xep"
	.addrsig_sym "MULLET^COMMON-LISP-USER^FN^^-xep1"
	.addrsig_sym "LAMBDA^COMMON-LISP^FN^^-lcl.3"
	.addrsig_sym "___LAMBDA___test.lisp-^1^7-lcl"
	.addrsig_sym "___LAMBDA___test.lisp-^1^7-xep"
	.addrsig_sym "___LAMBDA___test.lisp-^1^7-xep0"
	.addrsig_sym general_entry_point_redirect_1
	.addrsig_sym "<<clasp_startup>>_1"
	.addrsig_sym __clasp_gcroots_in_module_4
	.addrsig_sym __clasp_literals_6
	.addrsig_sym "function-vector-0"
	.section	.llvm_stackmaps,"a",@progbits
__LLVM_StackMaps:
	.byte	3
	.byte	0
	.short	0
	.long	6
	.long	0
	.long	6
	.quad	"LAMBDA^COMMON-LISP^FN^^-xep"
	.quad	56
	.quad	1
	.quad	"LAMBDA^COMMON-LISP^FN^^-xep1"
	.quad	40
	.quad	1
	.quad	"MULLET^COMMON-LISP-USER^FN^^-xep"
	.quad	56
	.quad	1
	.quad	"MULLET^COMMON-LISP-USER^FN^^-xep1"
	.quad	40
	.quad	1
	.quad	"___LAMBDA___test.lisp-^1^7-xep"
	.quad	56
	.quad	1
	.quad	"___LAMBDA___test.lisp-^1^7-xep0"
	.quad	24
	.quad	1
	.quad	3735879680
	.long	.Ltmp12-"LAMBDA^COMMON-LISP^FN^^-xep"
	.short	0
	.short	1
	.byte	2
	.byte	0
	.short	8
	.short	6
	.short	0
	.long	-24
	.p2align	3
	.short	0
	.short	0
	.p2align	3
	.quad	3735879682
	.long	.Ltmp19-"LAMBDA^COMMON-LISP^FN^^-xep1"
	.short	0
	.short	1
	.byte	2
	.byte	0
	.short	8
	.short	6
	.short	0
	.long	-16
	.p2align	3
	.short	0
	.short	0
	.p2align	3
	.quad	3735879680
	.long	.Ltmp30-"MULLET^COMMON-LISP-USER^FN^^-xep"
	.short	0
	.short	1
	.byte	2
	.byte	0
	.short	8
	.short	6
	.short	0
	.long	-24
	.p2align	3
	.short	0
	.short	0
	.p2align	3
	.quad	3735879682
	.long	.Ltmp37-"MULLET^COMMON-LISP-USER^FN^^-xep1"
	.short	0
	.short	1
	.byte	2
	.byte	0
	.short	8
	.short	6
	.short	0
	.long	-16
	.p2align	3
	.short	0
	.short	0
	.p2align	3
	.quad	3735879680
	.long	.Ltmp48-"___LAMBDA___test.lisp-^1^7-xep"
	.short	0
	.short	1
	.byte	2
	.byte	0
	.short	8
	.short	6
	.short	0
	.long	-24
	.p2align	3
	.short	0
	.short	0
	.p2align	3
	.quad	3735879681
	.long	.Ltmp55-"___LAMBDA___test.lisp-^1^7-xep0"
	.short	0
	.short	1
	.byte	2
	.byte	0
	.short	8
	.short	6
	.short	0
	.long	-8
	.p2align	3
	.short	0
	.short	0
	.p2align	3

	.section	.debug_line,"",@progbits
.Lline_table_start0:
