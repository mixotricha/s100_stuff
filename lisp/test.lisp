;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Damiens first lisp code in (30?) years
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|
;(load "~/quicklisp/setup.lisp")
(ql:quickload "cl-json")
;(ql:quickload "cl-uiop")

;(print "What Is Your Name")
;(defvar *name* (file) )
(defun hello-you (*name*) (format t "Hello ~a! ~%" *name*) )
;(setq *print-case* :capitalize) 

;;; Read in a file 

(defun read-file (infile)
  (with-open-file (instream infile :direction :input :if-does-not-exist nil)
    (when instream 
      (let ((string (make-string (file-length instream))))
        (read-sequence string instream)
        string))))

;;; string to json object  

(defvar jsn (with-input-from-string
    (s (read-file "test.json") )
  (json:decode-json s)))


(defun spud (z)  
  (format t "[~a,~a]" (* (cdr (second z)) 0.725) wibble ) 
) 

;;; double depth map to iterate through json 
(defvar wibble 0)
(mapcar #'(
	lambda(x) (
		mapcar #'(lambda(y) (spud y)) x )
    (incf wibble 1)  
) jsn ) 

(print "hello") 

|#

(defun mullet(lst) 
 (mapcar (lambda(x) (print (* (first x) (second x) ))) lst ))
